import React from "react";
import { storiesOf } from "@storybook/react";

import LogoImage from "../src/designSystem/atoms/LogoImage";
import Heading from "../src/designSystem/atoms/Heading";
import Title from "../src/designSystem/atoms/Title";
import Text from "../src/designSystem/atoms/Text";
import Button from "../src/designSystem/atoms/Button";
import Input from "../src/designSystem/atoms/Input";
import Select from "../src/designSystem/molecules/Select";

storiesOf("Брэндбук", module).add("Логотип", () => (
  <>
    <LogoImage />
  </>
));

storiesOf("Типография", module)
  .add("Заголовок", () => (
    <>
      <Heading>Съешь ещё этих мягких французских булок.</Heading>
      <Heading alter>Съешь ещё этих мягких французских булок.</Heading>
    </>
  ))
  .add("Подзаголовок", () => (
    <>
      <Title>Съешь ещё этих мягких французских булок.</Title>
      <Title alter>Съешь ещё этих мягких французских булок.</Title>
    </>
  ))
  .add("Текст", () => (
    <>
      <Text>Съешь ещё этих мягких французских булок.</Text>
      <Text alter>Съешь ещё этих мягких французских булок.</Text>
    </>
  ));

storiesOf("Кнопки", module)
  .add("Обычная", () => (
    <>
      <Button mr="20px">Главная</Button>
      <Button disabled mr="20px">
        Отключенная
      </Button>
      <Button secondary mr="20px">
        Вторичная
      </Button>
      <Button white mr="20px">
        Белая
      </Button>
      <Button transparent mr="20px">
        Прозрачная
      </Button>
      <Button simple mr="20px">
        Простая
      </Button>
    </>
  ))
  .add("С иконкой", () => (
    <>
      {/*
        <IconButton mr="20px">Главная</IconButton>
        <IconButton disabled mr="20px">
          Отключенная
        </IconButton>
        <IconButton secondary mr="20px">
          Вторичная
        </IconButton>
        <IconButton white mr="20px">
          Белая
        </IconButton>
        <IconButton transparent mr="20px">
          Прозрачная
        </IconButton>
        <IconButton simple mr="20px">
          Простая
        </IconButton>
      */}
      Lorem ipsum dolor sit amet consectetur adipisicing elit. A cum qui fugiat
      numquam eligendi? Non, voluptatum quidem omnis ullam distinctio architecto
      repellat dignissimos maxime, nesciunt dolor ab, ipsam qui eligendi?
    </>
  ));

storiesOf("Формы", module)
  .add("Input", () => (
    <>
      <Input placeholder="Введите значение" />
      <Input placeholder="Введите значение" disabled />
    </>
  ))
  .add("Textarea", () => (
    <>
      <Input placeholder="Введите значение" type="textarea" />
      <Input placeholder="Введите значение" type="textarea" disabled />
    </>
  ))
  .add("Select", () => (
    <>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate illum
      doloribus neque provident, ea saepe porro at ipsam voluptates eius? Aut
      officia molestiae dicta incidunt totam vitae quod sequi ad?
    </>
  ));
