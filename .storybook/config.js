import React from "react";
import { configure, addDecorator, addParameters } from "@storybook/react";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import yourTheme from "./yourTheme";
import theme from "../src/designSystem/theme";

function loadStories() {
  require("../stories/index.js");
  const req = require.context("../src/designSystem", true, /\.stories\.js$/);
  req.keys().forEach(filename => req(filename));
}

addParameters({
  options: {
    theme: yourTheme
  }
});

addDecorator(story => (
  <BrowserRouter>
    <ThemeProvider theme={theme}>{story()}</ThemeProvider>
  </BrowserRouter>
));

configure(loadStories, module);
