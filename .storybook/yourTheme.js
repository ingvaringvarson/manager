import { create } from "@storybook/theming";

export default create({
  base: "light",
  colorPrimary: "#eb0028",
  colorSecondary: "#c30028",
  appBg: "#f1f1f1",
  appContentBg: "#ffffff",
  appBorderColor: "#e0e0e0",
  appBorderRadius: 3,
  textColor: "#333333",
  brandTitle: "LAF24 - Design System"
});
