import App from "./components/App";
import ClientsPage from "./components/routes/ClientsPage";
import ClientInfoPage from "./components/routes/ClientInfoPage";
import OrdersPage from "./components/routes/OrdersPage";
import OrderInfoPage from "./components/routes/OrderInfoPage";
import PaymentsPage from "./components/routes/PaymentsPage";
import NotFoundPage from "./components/routes/NotFoundPage";
import SearchPage from "./components/routes/search/SearchPage";
import SearchResultPage from "./components/routes/search/SearchResultPage";
import ProductListPage from "./components/routes/product/ProductListPage";
import TasksPage from "./components/routes/TasksPage";
import BillInfoPage from "./components/routes/BillInfoPage";
import PaymentInfoPage from "./components/routes/PaymentInfoPage";
import PurchaseReturns from "./components/routes/PurchaseReturns";
import PurchaseReturnInfoPage from "./components/routes/PurchaseReturnInfoPage";
import ReturnInfoPage from "./components/routes/ReturnInfoPage";
import WarehousePage from "./components/routes/WarehousePage";
import StockPage from "./components/routes/StockPage";
import CreateClientPage from "./components/routes/CreateClientPage";
import ReturnsPage from "./components/routes/ReturnsPage";
import PurchaseOrders from "./components/routes/PurchaseOrders";
import PurchaseOrderInfoPage from "./components/routes/PurchaseOrderInfoPage";

import { getUrl } from "./helpers/urls";

const routes = [
  {
    component: App,
    routes: [
      {
        path: "/",
        exact: true,
        component: ClientsPage
      },
      {
        path: getUrl("agents/create"),
        exact: true,
        component: CreateClientPage
      },
      {
        path: getUrl("clients"),
        exact: true,
        component: ClientsPage
      },
      {
        path: getUrl("clients", { id: ":id" }),
        exact: true,
        component: ClientInfoPage
      },
      {
        path: getUrl("bills", { id: ":id" }),
        exact: true,
        component: BillInfoPage
      },
      {
        path: getUrl("orders"),
        exact: true,
        component: OrdersPage
      },
      {
        path: getUrl("orders", { id: ":id" }),
        exact: true,
        component: OrderInfoPage
      },
      {
        path: getUrl("returns"),
        exact: true,
        component: ReturnsPage
      },
      {
        path: getUrl("returns", { id: ":id" }),
        exact: true,
        component: ReturnInfoPage
      },
      {
        path: getUrl("purchaseOrders"),
        exact: true,
        component: PurchaseOrders
      },
      {
        path: getUrl("purchaseOrders", { id: ":id" }),
        exact: true,
        component: PurchaseOrderInfoPage
      },
      {
        path: getUrl("purchaseReturns"),
        exact: true,
        component: PurchaseReturns
      },
      {
        path: getUrl("purchaseReturns", { id: ":id" }),
        exact: true,
        component: PurchaseReturnInfoPage
      },
      {
        path: getUrl("payments"),
        exact: true,
        component: PaymentsPage
      },
      {
        path: getUrl("payments", { id: ":id" }),
        exact: true,
        component: PaymentInfoPage
      },
      {
        path: getUrl("search"),
        component: SearchPage,
        routes: [
          {
            path: getUrl("search"),
            exact: true,
            component: SearchResultPage
          },
          {
            path: getUrl("products", {
              brandKey: ":brandKey",
              vendorCode: ":vendorCode"
            }),
            exact: true,
            component: ProductListPage
          }
        ]
      },
      {
        path: getUrl("tasks"),
        exact: true,
        component: TasksPage
      },
      {
        path: getUrl("warehouse"),
        exact: true,
        component: WarehousePage
      },
      {
        path: getUrl("stock"),
        exact: true,
        component: StockPage
      },
      {
        path: "*",
        component: NotFoundPage
      }
    ]
  }
];

export default routes;
