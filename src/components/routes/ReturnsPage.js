import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import nanoid from "nanoid";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import {
  requestParamsReturnPageSelector,
  moduleName as moduleNameReturn
} from "../../ducks/returns";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import {
  keyRouterSelector,
  pathnameRouterSelector
} from "../../redux/selectors/router";
import { moduleName as moduleNamePositions } from "../../ducks/positions";

import Grid from "../../designSystem/templates/Grid";
import PageWrapper from "../../designSystem/molecules/PageWrapper";
import { ContainerContext } from "../common/formControls";

import ReturnsTableBlock from "../blocks/returns/ReturnsTableBlock";
import FilterComponent from "../blocks/returns/FilterComponent";
import PreLoader from "../common/PreLoader";

import { fields as returnsFields } from "../../fields/formFields/returns/fieldsForReturnList";
import { fields as filtersFields } from "../../fields/formFields/returns/fieldsForFilters";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../fields/helpers";
import { generateUrlWithQuery } from "../../utils";

const enumerationNames = [
  "agentType",
  "returnReason",
  "returnPurpose",
  "returnState",
  "orderPositionGoodState"
];

const moduleNames = [
  moduleNameEnumerations,
  moduleNameReturn,
  moduleNamePositions
];

class ReturnsPage extends PureComponent {
  static propTypes = {
    enumerations: PropTypes.object,
    requestParams: PropTypes.object,
    pathname: PropTypes.string,
    push: PropTypes.func.isRequired,
    keyRouter: PropTypes.string,
    isLoading: PropTypes.bool,
    fetchEnumerations: PropTypes.func.isRequired
  };
  componentDidMount = () => {
    this.props.fetchEnumerations(enumerationNames);
  };

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  renderCells = memoizeOne(enumerations => {
    const { requestParams } = this.props;

    const cellsForTable = returnsFields.reduce(
      reduceValuesToFormFields(requestParams, enumerations),
      []
    );
    const fieldsForFilters = filtersFields.reduce(
      reduceValuesToFormFields(requestParams, enumerations),
      []
    );
    const fields = cellsForTable.concat(fieldsForFilters);

    return {
      cellsForTable,
      fieldsForFilters,
      fields,
      key: nanoid()
    };
  });

  render() {
    const { requestParams, enumerations, keyRouter, pathname } = this.props;

    const { cellsForTable, fieldsForFilters, fields, key } = this.renderCells(
      enumerations,
      requestParams
    );

    return (
      <PageWrapper>
        <ContainerContext
          key={key}
          onSort={this.handleSort}
          fields={fields}
          onSubmitForm={this.handleSubmitForm}
          autoSubmit
          submitValidation
        >
          <Grid filterRight>
            <ReturnsTableBlock
              requestParams={requestParams}
              enumerations={enumerations}
              fields={cellsForTable}
              keyRouter={keyRouter}
              pathname={pathname}
            />
            <FilterComponent fields={fieldsForFilters} />
          </Grid>
        </ContainerContext>
        {this.props.isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      requestParams: requestParamsReturnPageSelector(state, props),
      enumerations: enumerationsSelector(state, { enumerationNames }),
      isLoading: modulesIsLoadingSelector(state, { moduleNames }),
      pathname: pathnameRouterSelector(state, props),
      keyRouter: keyRouterSelector(state, props)
    };
  },
  {
    fetchEnumerations,
    push
  }
)(ReturnsPage);
