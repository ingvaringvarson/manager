import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  clientListSelector,
  fetchClients,
  requestParamsSelector,
  createClient,
  maxPagesSelector,
  moduleName as moduleNameClients
} from "../../ducks/clients";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations,
  agentTypesByClientTabsSelector
} from "../../ducks/enumerations";
import {
  keyRouterSelector,
  pathnameRouterSelector
} from "../../redux/selectors/router";
import { push } from "connected-react-router";

import Grid from "../../designSystem/templates/Grid";
import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Block from "../../designSystem/organisms/Block";
import BlockContent from "../../designSystem/organisms/BlockContent";
import Heading from "../../designSystem/molecules/Heading";
import {
  fieldsForFiltersByType,
  fieldsForTableByType
} from "../../fields/formFields/clients/clientsPage";
import Pagination from "../blocks/Pagination";
import CreateClientBlock from "../blocks/clients/CreateClientBlock";
import PreLoader from "../common/PreLoader";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import EmptyResult from "../common/EmptyResult";
import { ContainerContext, FormContext } from "../common/formControls";
import {
  handleChangeTab,
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields
} from "../../helpers/component/handlers";
import TableWithLinks from "../common/table/TableWithLinks";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../common/tabControls";
import FieldForm from "../common/form/FieldForm";
import memoizeOne from "memoize-one";
import ClearButtonHeading from "../common/styled/ClearButtonHeading";
import { getStateByRequestInfo } from "../../helpers/component/staticMethods";

const enumerationNames = ["contactType", "agentType"];

class ClientsPage extends Component {
  static propTypes = {
    requestParams: PropTypes.object.isRequired,
    clients: PropTypes.array.isRequired,
    enumerations: PropTypes.object.isRequired,
    createClient: PropTypes.func.isRequired,
    pathname: PropTypes.string.isRequired,
    push: PropTypes.func.isRequired,
    maxPages: PropTypes.number.isRequired,
    queryKeyByTab: PropTypes.string.isRequired,
    agentTypes: PropTypes.array.isRequired,
    fetchClients: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    keyRouter: PropTypes.string.isRequired,
    typeEntity: PropTypes.string.isRequired,
    valuesForFields: PropTypes.object.isRequired,
    fieldsForTable: PropTypes.array.isRequired,
    fieldsForFilters: PropTypes.array.isRequired,
    selectedTab: PropTypes.number.isRequired,
    isLoading: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      isLoaded: false
    };

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleChangeTab = handleChangeTab.bind(this);
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    return getStateByRequestInfo(props, state);
  }

  componentDidMount() {
    this.props.fetchClients(this.props.requestParams);
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.props.fetchClients(this.props.requestParams);
    }
  }

  render() {
    const {
      requestParams,
      clients,
      enumerations,
      createClient,
      pathname,
      push,
      maxPages,
      isLoading,
      agentTypes,
      selectedTab
    } = this.props;
    const {
      fieldsForTable,
      fieldsForFilters,
      allFields,
      key
    } = this.mapValuesToFields(enumerations, clients);

    return (
      <PageWrapper>
        <ContainerContext
          key={key}
          onSort={this.handleTableSort}
          fields={allFields}
          onSubmitForm={this.handleSubmitForm}
          autoSubmit
          externalForm
          submitValidation
        >
          <Grid filterRight>
            <Block>
              <Heading title="Список клиентов">
                <ClearButtonHeading
                  title="Сбросить все"
                  fields={this.props.fieldsForTable}
                />
              </Heading>
              <BlockContent table>
                <FormContext>
                  <TableWithLinks
                    key={key}
                    cells={fieldsForTable}
                    entities={clients}
                    enumerations={enumerations}
                    hasFormControl
                    typeEntity="client"
                  />
                </FormContext>
                {this.state.isLoaded && !clients.length && (
                  <EmptyResult>
                    <CreateClientBlock onSubmitForm={createClient} />
                  </EmptyResult>
                )}
                <Pagination
                  pathname={pathname}
                  push={push}
                  requestParams={requestParams}
                  page={+requestParams.page}
                  maxPages={maxPages}
                />
              </BlockContent>
            </Block>
            <Block mb="middle">
              <Heading title="Фильтрация">
                <ClearButtonHeading
                  title="Сбросить все"
                  fields={this.props.fieldsForFilters}
                />
              </Heading>
              <TabContainer
                selectedTabId={selectedTab}
                onChangeSelected={this.handleChangeTab}
              >
                <TabHeadContainer>
                  {agentTypes.map(item => (
                    <TabHead id={item.id} key={item.id}>
                      {item.name}
                    </TabHead>
                  ))}
                </TabHeadContainer>
                <TabBodyContainer>
                  {agentTypes.map(item => {
                    return (
                      <TabBody id={item.id} key={item.id}>
                        <BlockContent>
                          <FormContext>
                            {fieldsForFilters.map((filter, index) => (
                              <FieldForm
                                field={filter}
                                index={index}
                                key={index}
                              />
                            ))}
                          </FormContext>
                        </BlockContent>
                      </TabBody>
                    );
                  })}
                </TabBodyContainer>
              </TabContainer>
            </Block>
          </Grid>
        </ContainerContext>
        {isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

const moduleNames = [moduleNameClients, moduleNameEnumerations];

function mapStateToProps(state, ownProps) {
  const requestParams = requestParamsSelector(state, ownProps);
  const queryKeyByTab = "type";
  const selectedTab = +requestParams[queryKeyByTab];

  return {
    clients: clientListSelector(state, ownProps),
    requestParams,
    keyRouter: keyRouterSelector(state, ownProps),
    pathname: pathnameRouterSelector(state, ownProps),
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    maxPages: maxPagesSelector(state, ownProps),
    enumerations: enumerationsSelector(state, { enumerationNames }),
    queryKeyByTab,
    agentTypes: agentTypesByClientTabsSelector(state, { name: "agentType" }),
    typeEntity: "clients",
    valuesForFields: requestParams,
    fieldsForTable: fieldsForTableByType[selectedTab],
    fieldsForFilters: fieldsForFiltersByType[selectedTab],
    selectedTab
  };
}

export default connect(
  mapStateToProps,
  { fetchClients, push, createClient, fetchEnumerations }
)(ClientsPage);
