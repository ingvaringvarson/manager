import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import memoizeOne from "memoize-one";

import { fields as fieldsForTable } from "../../fields/formFields/purchaseOrders/fieldsForTable";
import { fieldsForFilters } from "../../fields/formFields/purchaseOrders/fieldsForFilter";

import {
  requestParamsSelector,
  pathnameRouterSelector,
  keyRouterSelector
} from "../../redux/selectors/router";
import { moduleName as moduleNamePositions } from "../../ducks/positions";
import { moduleName as moduleNamePurchaseReturns } from "../../ducks/purchaseReturns";
import {
  fetchPurchaseOrders,
  purchaseOrdersSelector,
  maxPagesSelector,
  moduleName as moduleNamePurchaseOrders
} from "../../ducks/purchaseOrders";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import { modulesIsLoadingSelector } from "../../ducks/logger";

import {
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields,
  getSelectedEntities
} from "../../helpers/component/handlers";
import { getStateByRequestInfo } from "../../helpers/component/staticMethods";
import { purchaseOrdersFields as positionsFields } from "../../fields/formFields/positions/fieldsForList";

import Pagination from "../blocks/Pagination";
import TableWithLinks from "../common/table/TableWithLinks";
import { ContainerContext, FormContext } from "../common/formControls";
import {
  TableContainer,
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore,
  renderRowFormControlChildrenAfter,
  ToggleOpenRow,
  RenderContext
} from "../common/tableControls";
import EmptyResult from "../common/EmptyResult";
import PreLoader from "../common/PreLoader";
import FieldForm from "../common/form/FieldForm";
import ClearButtonHeading from "../common/styled/ClearButtonHeading";

import Grid from "../../designSystem/templates/Grid";
import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Block from "../../designSystem/organisms/Block";
import Heading from "../../designSystem/molecules/Heading";
import BlockContent from "../../designSystem/organisms/BlockContent";
import Box from "../../designSystem/organisms/Box";
import TableRow from "../../designSystem/molecules/TableRow";
import TableCell from "../../designSystem/molecules/TableCell";
import Title from "../../designSystem/atoms/Title";
import PurchaseTabs from "../blocks/helperBlocks/PurchaseTabs";

import NestedTableForEntity from "../blocks/positions/NestedTableForEntity";

import PurchaseOrderMenu from "../blocks/purchaseOrders/PurchaseOrderMenu";
import PurchaseOrderPositionsMenu from "../blocks/purchaseOrders/PurchaseOrderPositionsMenu";

import { memoizeToArray } from "../../helpers/cache";

const enumerationNames = [
  "agentType",
  "orderSupplierState",
  "pricelistDeliveryType",
  "orderPositionGoodState",
  "productUnitState",
  "orderState"
];

const moduleNames = [
  moduleNameEnumerations,
  moduleNamePurchaseOrders,
  moduleNamePositions,
  moduleNamePurchaseReturns
];

class PurchaseOrders extends PureComponent {
  static propTypes = {
    fieldsForTable: PropTypes.array,
    fieldsForFilter: PropTypes.array,
    keyRouter: PropTypes.string,
    pathname: PropTypes.string,
    requestParams: PropTypes.object,
    isLoading: PropTypes.bool,
    enumerations: PropTypes.object,
    purchaseOrders: PropTypes.arrayOf(PropTypes.object),
    maxPages: PropTypes.number,
    push: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    fetchPurchaseOrders: PropTypes.func.isRequired
  };
  constructor(props) {
    super(props);

    this.state = {
      isLoaded: false,
      isLoading: false
    };

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
    this.getSelectedEntities = memoizeOne(getSelectedEntities);
  }

  static getDerivedStateFromProps(props, state) {
    return getStateByRequestInfo(props, state);
  }

  fetchEntities = () => {
    this.props.fetchPurchaseOrders({
      ...this.props.requestParams,
      isForRepeat: true
    });
  };

  componentDidMount = () => {
    this.fetchEntities();
    this.props.fetchEnumerations(enumerationNames);
  };

  componentDidUpdate = prevProps => {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.fetchEntities();
    }
  };

  renderTableHead = () => {
    return (
      <TableRow>
        <TableCell flex="0 1 80px" />
        <TableCell flex="2 1 0">
          <Title>Поставщик</Title>
        </TableCell>
        <TableCell flex="1 1 0">
          <Title>Заказчик</Title>
        </TableCell>
        <TableCell flex="4 1 0">
          <Title>Заказ</Title>
        </TableCell>
      </TableRow>
    );
  };

  renderPositionsMenu = purchaseOrder => ({ forList, positions }) => (
    <PurchaseOrderPositionsMenu
      forList={forList}
      positions={positions}
      purchaseOrder={purchaseOrder}
      fetchEntities={this.fetchEntities}
    />
  );

  renderNestedTable = ({ cells, entity, entities, enumerations }) => {
    const { positions } = entity.order;
    if (!(positions && Array.isArray(positions) && positions.length))
      return (
        <ToggleOpenRow id={entity._id}>У возврата нет позиций</ToggleOpenRow>
      );
    return (
      <NestedTableForEntity
        renderPositionsMenu={this.renderPositionsMenu(entity)}
        entity={entity}
        enumerations={enumerations}
        fields={positionsFields}
        positions={positions}
      />
    );
  };

  renderMainTableHeadAfterContext = ({ selected }, { entities }) => {
    const purchaseOrders = this.getSelectedEntities(entities, selected);
    return (
      <PurchaseOrderMenu
        forList
        purchaseOrders={purchaseOrders}
        fetchEntities={this.fetchEntities}
      />
    );
  };

  renderMainTableHeadAfter = headProps => {
    return (
      <RenderContext {...headProps}>
        {this.renderMainTableHeadAfterContext}
      </RenderContext>
    );
  };

  renderMainTableRowAfter = ({ entity }) => {
    return (
      <PurchaseOrderMenu
        purchaseOrders={memoizeToArray(entity)}
        fetchEntities={this.fetchEntities}
      />
    );
  };

  render() {
    const {
      isLoading,
      requestParams,
      enumerations,
      pathname,
      push,
      maxPages,
      purchaseOrders
    } = this.props;

    const {
      fieldsForTable,
      fieldsForFilters,
      allFields,
      key
    } = this.mapValuesToFields(enumerations, purchaseOrders);

    return (
      <PageWrapper>
        <ContainerContext
          key={key}
          fields={allFields}
          autoSubmit
          externalForm
          submitValidation
          onSort={this.handleTableSort}
          onSubmitForm={this.handleSubmitForm}
        >
          <Grid filterRight>
            <Block>
              <Box position="relative">
                <PurchaseTabs />
                <ClearButtonHeading
                  title="Сбросить все"
                  fields={fieldsForTable}
                />
              </Box>
              <BlockContent table>
                <FormContext>
                  <TableContainer
                    key={key}
                    entities={purchaseOrders}
                    canBeSelected
                    hasNestedTable
                  >
                    <TableWithLinks
                      cells={fieldsForTable}
                      entities={purchaseOrders}
                      enumerations={enumerations}
                      hasFormControl
                      hasChildrenBefore
                      hasChildrenAfter
                      renderBeforeContent={this.renderTableHead}
                      renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
                      renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
                      renderRowFormControlChildrenBefore={
                        renderRowFormControlChildrenBefore
                      }
                      renderRowFormControlChildrenAfter={
                        renderRowFormControlChildrenAfter
                      }
                      renderRowBodyAfter={this.renderNestedTable}
                      renderRowHeadChildrenAfter={this.renderMainTableHeadAfter}
                      renderRowBodyChildrenAfter={this.renderMainTableRowAfter}
                      typeEntity="purchaseOrder"
                    />
                  </TableContainer>
                </FormContext>
                {this.state.isLoaded && !purchaseOrders.length && (
                  <EmptyResult />
                )}
                <Pagination
                  requestParams={requestParams}
                  pathname={pathname}
                  push={push}
                  page={+requestParams.page}
                  maxPages={maxPages}
                />
              </BlockContent>
            </Block>
            <Block md="middle">
              <Heading title="Фильтрация">
                <ClearButtonHeading
                  title="Сбросить все"
                  fields={fieldsForFilters}
                />
              </Heading>
              <BlockContent>
                <FormContext>
                  {fieldsForFilters.map((field, i) => {
                    return <FieldForm key={i} field={field} index={i} />;
                  })}
                </FormContext>
              </BlockContent>
            </Block>
          </Grid>
        </ContainerContext>
        {isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

export default connect(
  (state, props) => {
    const requestParams = requestParamsSelector(state, props);
    return {
      fieldsForTable,
      fieldsForFilters,
      requestParams,
      valuesForFields: requestParams,
      keyRouter: keyRouterSelector(state),
      pathname: pathnameRouterSelector(state),
      isLoading: modulesIsLoadingSelector(state, { moduleNames }),
      enumerations: enumerationsSelector(state, { enumerationNames }),
      purchaseOrders: purchaseOrdersSelector(state),
      maxPages: maxPagesSelector(state)
    };
  },
  {
    push,
    fetchEnumerations,
    fetchPurchaseOrders
  }
)(PurchaseOrders);
