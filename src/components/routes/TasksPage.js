import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import { connect } from "react-redux";
import { push } from "connected-react-router";
import {
  tasksSelector,
  fetchTasks,
  moduleName as moduleNameTasks,
  maxPagesSelector,
  defaultValuesForFieldsSelector,
  requestParamsSelector
} from "../../ducks/tasks";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import {
  enumerationsSelector,
  fetchEnumerations
} from "../../ducks/enumerations";
import {
  keyRouterSelector,
  pathnameRouterSelector
} from "../../redux/selectors/router";

import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Block from "../../designSystem/organisms/Block";
import Heading from "../../designSystem/molecules/Heading";
import BlockContent from "../../designSystem/organisms/BlockContent";
import Row from "../../designSystem/templates/Row";
import Column from "../../designSystem/templates/Column";
import Grid from "../../designSystem/templates/Grid";
import PreLoader from "../common/PreLoader";

import {
  fieldsForTable,
  fieldsForFilters
} from "../../fields/formFields/tasks/tasksPage";
import FieldForm from "../common/form/FieldForm";
import { ContainerContext, FormContext } from "../common/formControls";
import Pagination from "../blocks/Pagination";
import { Table } from "../common/tableView";
import {
  TableContainer,
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore
} from "../common/tableControls";
import {
  handleChangeTab,
  handleMoveToEntityPage,
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields
} from "../../helpers/component/handlers";
import {
  getStateByDefaultValuesForFields,
  getStateByRequestInfo
} from "../../helpers/component/staticMethods";
import EmptyResult from "../common/EmptyResult";

import CommandsInList from "../common/menus/CommandsInList";
import tasksCommands from "../../commands/tasks";
import { Modal as CreateTask } from "../blocks/tasks/commands/CreateTask";

const enumerationNames = ["taskType", "taskState", "taskReason"];

class TasksPage extends PureComponent {
  static propTypes = {
    enumerations: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      isLoaded: false,
      valuesForFields: null,
      prevValuesForFields: null
    };

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleChangeTab = handleChangeTab.bind(this);
    this.handleMoveToEntityPage = handleMoveToEntityPage.bind(this);
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
    this.rowBodyHandlers = {
      onClick: this.handleMoveToEntityPage
    };
  }

  static getDerivedStateFromProps(props, state) {
    let newState = null;

    newState = getStateByRequestInfo(props, state, newState);
    newState = getStateByDefaultValuesForFields(props, state, newState);

    return newState;
  }

  fetchEntities = () => this.props.fetchTasks(this.state.valuesForFields);

  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
    this.fetchEntities();
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.fetchEntities();
    }
  }

  commands = [
    {
      ...tasksCommands.createTaskPrepareToMoveCommand,
      renderModal: (handleClose, command) => (
        <CreateTask
          isOpen
          handleClose={handleClose}
          defaultValues={command.defaultValues}
          title={command.title}
          fetchEntities={this.fetchEntities}
        />
      )
    }
  ];

  render() {
    const {
      requestParams,
      tasks,
      enumerations,
      pathname,
      push,
      maxPages,
      isLoading
    } = this.props;
    const {
      fieldsForTable,
      fieldsForFilters,
      allFields,
      key
    } = this.mapValuesToFields(enumerations, tasks);

    return (
      <PageWrapper>
        <ContainerContext
          key={key}
          fields={allFields}
          onSort={this.handleTableSort}
          onSubmitForm={this.handleSubmitForm}
          autoSubmit
          externalForm
          submitValidation
        >
          <Row>
            <Column grow="2" basis="1200px" mr="middle" />
            <Column mb="10px" grow="2" basis="350px" maxWidth="350px">
              <CommandsInList
                commands={this.commands}
                buttonTitle="Создать задачу"
              />
            </Column>
          </Row>
          <Grid filterRight>
            <Block>
              <Heading title="Список задач" />
              <BlockContent table>
                <TableContainer key={key} entities={tasks} canBeSelected>
                  <FormContext>
                    <Table
                      hasFormControl
                      entities={tasks}
                      cells={fieldsForTable}
                      enumerations={enumerations}
                      hasChildrenBefore
                      renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
                      renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
                      renderRowFormControlChildrenBefore={
                        renderRowFormControlChildrenBefore
                      }
                      rowBodyHandlers={this.rowBodyHandlers}
                    />
                  </FormContext>
                </TableContainer>
                {this.state.isLoaded && !tasks.length && <EmptyResult />}
                <Pagination
                  requestParams={this.state.valuesForFields}
                  pathname={pathname}
                  push={push}
                  page={+requestParams.page}
                  maxPages={maxPages}
                />
              </BlockContent>
            </Block>
            <Block mb="middle">
              <Heading title="Фильтрация" />
              <BlockContent>
                <FormContext>
                  {fieldsForFilters.map((filter, index) => (
                    <FieldForm field={filter} index={index} />
                  ))}
                </FormContext>
              </BlockContent>
            </Block>
          </Grid>
        </ContainerContext>

        {isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

const moduleNames = [moduleNameTasks];

function mapStateToProps(state, props) {
  const requestParams = requestParamsSelector(state, props);

  return {
    typeEntity: "tasks",
    valuesForFields: requestParams,
    fieldsForTable,
    fieldsForFilters,
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    requestParams,
    keyRouter: keyRouterSelector(state),
    pathname: pathnameRouterSelector(state, props),
    enumerations: enumerationsSelector(state, { enumerationNames }),
    tasks: tasksSelector(state),
    maxPages: maxPagesSelector(state),
    defaultValuesForFields: defaultValuesForFieldsSelector(state, props)
  };
}

export default connect(
  mapStateToProps,
  {
    fetchEnumerations,
    fetchTasks,
    push
  }
)(TasksPage);
