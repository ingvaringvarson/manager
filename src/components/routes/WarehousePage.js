import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  entityIdByWarehousePageSelector,
  requestParamsByWarehousePageSelector,
  tasksWithProductForEntitySelector,
  fetchTasksForEntity,
  defaultSelectedFilterGroupSelector,
  moduleName as moduleNameTasks
} from "../../ducks/tasks";
import { employeeInfoByUserSelector } from "../../ducks/user";

import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Container from "../../designSystem/templates/Container";
import Row from "../../designSystem/templates/Row";
import Column from "../../designSystem/templates/Column";
import Grid from "../../designSystem/templates/Grid";
import PointOfSaleTitle from "../blocks/stock/PointOfSaleTitle";
import GroupFilterByTaskType from "../blocks/tasks/GroupFilterByTaskType";
import {
  groupFieldsForFilter,
  groupTaskType,
  getFieldsForProduct
} from "../../fields/formFields/tasks/fieldsForWarehousePage";
import memoizeOne from "memoize-one";
import {
  actionResultsByIdSelector,
  modulesIsLoadingSelector
} from "../../ducks/logger";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import { moduleName as moduleProductsInStock } from "../../ducks/productsInStock";
import { pathnameRouterSelector } from "../../redux/selectors/router";
import {
  mapFormFieldsToQuery,
  reduceValuesToFormFields
} from "../../fields/helpers";
import { getUrl } from "../../helpers/urls";
import { generateId, generateUrlWithQuery, getValueInDepth } from "../../utils";
import { push } from "connected-react-router";
import Block from "../../designSystem/organisms/Block";
import Box from "../../designSystem/organisms/Box";
import Heading from "../../designSystem/molecules/Heading";
import BlockContent from "../../designSystem/organisms/BlockContent";
import WideButton from "../../designSystem/molecules/WideButton";
import PreLoader from "../common/PreLoader";
import FieldForm from "../common/form/FieldForm";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../common/tabControls";
import { ContainerContext } from "../common/formControls";
import GroupProductByTaskType from "../blocks/tasks/GroupProductByTaskType";
import styled from "styled-components";
import HeadControlsForTasks from "../blocks/tasks/HeadControlsForTasks";
import SearchBarCodeForm from "../blocks/tasks/SearchBarCodeForm";
import PrintingForms from "../blocks/tasks/PrintingForms";
import { mapTasksToProducts } from "../blocks/tasks/helpers";

const StyledRow = styled(Row)`
  padding: 16px 20px;
  background-color: #fff;
  border-radius: 5px;
`;
const typeEntity = "warehouse";
const enumerationNames = ["taskType", "taskState"];

class WarehousePage extends Component {
  static propTypes = {
    fetchTasksForEntity: PropTypes.func.isRequired,
    idEntity: PropTypes.string.isRequired,
    requestParams: PropTypes.object.isRequired,
    employeeInfo: PropTypes.object,
    push: PropTypes.func.isRequired,
    pathname: PropTypes.string.isRequired,
    enumerations: PropTypes.object.isRequired,
    actionResult: PropTypes.object,
    defaultSelectedFilterGroup: PropTypes.array.isRequired
  };

  state = {
    selectedFilterGroup: [],
    prevTasks: [],
    selectedTaskIds: [],
    productsFound: [],
    prevActionResult: null,
    selectedProductIds: []
  };

  static getDerivedStateFromProps(props, state) {
    const newState = {};

    if (state.prevTasks !== props.tasks) {
      newState.prevTasks = props.tasks;
      newState.selectedFilterGroup = props.defaultSelectedFilterGroup;
      newState.selectedTaskIds = [];
      newState.productsFound = [];
      newState.selectedProductIds = [];
    }
    if (state.prevActionResult !== props.actionResult) {
      newState.prevActionResult = props.actionResult;
      const ids = getValueInDepth(props.actionResult, ["payload", "ids"]);
      if (Array.isArray(ids) && ids.length) {
        newState.selectedTaskIds = ids;
      }
    }

    if (Object.keys(newState).length) return newState;

    return null;
  }

  componentDidMount() {
    this.props.fetchTasksForEntity(
      typeEntity,
      this.props.idEntity,
      this.props.requestParams
    );
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.requestParams !== prevProps.requestParams) {
      this.props.fetchTasksForEntity(
        typeEntity,
        this.props.idEntity,
        this.props.requestParams
      );
    }
  }

  handleChangeFilterByProducts = (newFilter, reset) => {
    const { selectedFilterGroup } = this.state;

    if (reset) this.setState({ selectedFilterGroup: [] });

    const index = selectedFilterGroup.findIndex(
      item => item.key === newFilter.key && item.value === newFilter.value
    );

    if (!~index) {
      this.setState({ selectedFilterGroup: [newFilter] });
    } else {
      this.setState({ selectedFilterGroup: [] });
    }
  };

  mapProductsFoundToFilterTasks = memoizeOne((tasks, productsFound) => {
    return {
      key: generateId(),
      tasksWithProducts: tasks.reduce((filteredTasks, task) => {
        if (productsFound.length) {
          const productFound = productsFound.find(
            product => product[0] === task.id
          );

          if (productFound) {
            filteredTasks.push({
              ...task,
              product_unit_id: productFound[1].id,
              ean_13: productFound[1].ean_13
            });
          } else {
            filteredTasks.push(task);
          }
        } else {
          filteredTasks.push(task);
        }

        return filteredTasks;
      }, [])
    };
  });

  getKeyStateByTasks = memoizeOne(() => generateId());

  filterTasks = memoizeOne((tasks, filters) => {
    const filteredTasks = tasks.filter(task => {
      return !filters.some(item => task[item.key] !== item.value);
    });

    return {
      filteredTasks,
      key: generateId()
    };
  });

  mapFields = memoizeOne(enumerations => {
    const { requestParams } = this.props;

    const fields = groupFieldsForFilter[requestParams._tab]
      ? groupFieldsForFilter[requestParams._tab].reduce(
          reduceValuesToFormFields(requestParams, enumerations),
          []
        )
      : [];

    return {
      fields,
      key: generateId()
    };
  });

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleChangeTab = id => {
    const { pathname } = this.props;
    const queryForTab = id === "1" ? "" : `?_tab=${id}`;
    this.props.push(`${pathname}${queryForTab}`);
  };

  tabsByFilter = [{ id: "1", name: "Выдача" }, { id: "2", name: "Приёмка" }];

  handleSelectedProducts = (
    selectedTaskIds,
    productsFound,
    selectedProductIds
  ) => {
    this.setState(prevState => {
      return {
        selectedTaskIds,
        productsFound: prevState.productsFound.concat(productsFound),
        selectedProductIds: Array.isArray(selectedProductIds)
          ? selectedProductIds
          : prevState.selectedProductIds
      };
    });
  };

  handleChangeSelected = selectedProductIds => {
    this.setState({ selectedProductIds });
  };

  getKeyStatePrintingForms = memoizeOne(() => generateId());

  mapTasksToProducts = memoizeOne(mapTasksToProducts);

  render() {
    const {
      employeeInfo,
      requestParams,
      tasks,
      enumerations,
      actionResult,
      selectedGroup
    } = this.props;
    const {
      selectedFilterGroup,
      selectedTaskIds,
      productsFound,
      selectedProductIds
    } = this.state;
    const { fields, key: keyForm } = this.mapFields(
      enumerations,
      requestParams
    );
    const { key: keyFilteredTaskList, filteredTasks } = this.filterTasks(
      tasks.entities,
      selectedFilterGroup
    );
    const {
      key: keyTaskList,
      tasksWithProducts
    } = this.mapProductsFoundToFilterTasks(filteredTasks, productsFound);
    const keyState = this.getKeyStateByTasks(tasks.entities);
    const products = this.mapTasksToProducts(selectedGroup, tasksWithProducts);

    return (
      <PageWrapper>
        <Container>
          <Row alignCenter justifyBetween mb="20px">
            <Column>
              <PointOfSaleTitle employeeInfo={employeeInfo} />
            </Column>
          </Row>
        </Container>
        <Grid filterLeft>
          <GroupFilterByTaskType
            key={keyState}
            tasks={tasks.entities}
            selectedFilter={selectedFilterGroup}
            groupTaskType={groupTaskType}
            selectedGroup={selectedGroup}
            onChangeFilter={this.handleChangeFilterByProducts}
          />
          <Box>
            <Container>
              <StyledRow mb="16px">
                <Column basis="900px" grow="2">
                  <SearchBarCodeForm
                    selectedProductIds={selectedProductIds}
                    id={keyFilteredTaskList}
                    tasks={tasksWithProducts}
                    selectedGroup={selectedGroup}
                    onSelectedProducts={this.handleSelectedProducts}
                    warehouseId={employeeInfo ? employeeInfo.warehouse_id : ""}
                    products={products}
                  />
                </Column>
                <Column basis="350px" maxWidth="350px" pl="50px">
                  <HeadControlsForTasks
                    selectedGroup={selectedGroup}
                    tasks={tasksWithProducts}
                    products={products}
                    selectedProductIds={selectedProductIds}
                  />
                </Column>
              </StyledRow>
            </Container>
            <Grid subGrid>
              <Block>
                <Heading title="Товары" />
                <BlockContent table>
                  <GroupProductByTaskType
                    handleChangeSelected={this.handleChangeSelected}
                    selectedProductIds={selectedProductIds}
                    selectedTaskIds={selectedTaskIds}
                    tasks={tasksWithProducts}
                    enumerations={enumerations}
                    fields={getFieldsForProduct(selectedGroup)}
                    onSelectedProducts={this.handleSelectedProducts}
                    selectedGroup={selectedGroup}
                    warehouseId={employeeInfo ? employeeInfo.warehouse_id : ""}
                    products={products}
                  />
                </BlockContent>
              </Block>
              {/* mr="8px" */}
              <Box>
                <Block mb="middle">
                  <Heading title="Фильтрация" />
                  <TabContainer
                    selectedTabId={requestParams._tab}
                    onChangeSelected={this.handleChangeTab}
                  >
                    <TabHeadContainer>
                      {this.tabsByFilter.map(item => {
                        return (
                          <TabHead key={item.id} id={item.id}>
                            {item.name}
                          </TabHead>
                        );
                      })}
                    </TabHeadContainer>
                    <TabBodyContainer>
                      {this.tabsByFilter.map(item => {
                        return (
                          <TabBody key={item.id} id={item.id}>
                            <BlockContent>
                              <ContainerContext
                                key={keyForm}
                                fields={fields}
                                onSubmitForm={this.handleSubmitForm}
                                autoSubmit
                                submitValidation
                              >
                                {fields.map((filter, index) => (
                                  <FieldForm
                                    field={filter}
                                    index={index}
                                    key={index}
                                  />
                                ))}
                              </ContainerContext>
                            </BlockContent>
                          </TabBody>
                        );
                      })}
                    </TabBodyContainer>
                  </TabContainer>
                </Block>
                <WideButton
                  icon="filter_9"
                  type="spa-link"
                  href={getUrl("stock")}
                >
                  Товары на складе
                </WideButton>
              </Box>
            </Grid>
          </Box>
        </Grid>

        <PrintingForms
          key={this.getKeyStatePrintingForms(actionResult)}
          selectedGroup={selectedGroup}
          actionResult={actionResult}
          actionResultMod={true}
        />
        {this.props.isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

const moduleNames = [
  moduleNameTasks,
  moduleNameEnumerations,
  moduleProductsInStock
];

function mapStateToProps(state, props) {
  const idEntity = entityIdByWarehousePageSelector(state, props);
  const requestParams = requestParamsByWarehousePageSelector(state, props);

  return {
    selectedGroup: requestParams._type_group,
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    idEntity,
    tasks: tasksWithProductForEntitySelector(state, { idEntity, typeEntity }),
    requestParams,
    actionResult: actionResultsByIdSelector(state, { id: idEntity }),
    employeeInfo: employeeInfoByUserSelector(state, props),
    pathname: pathnameRouterSelector(state, props),
    enumerations: enumerationsSelector(state, { enumerationNames }),
    defaultSelectedFilterGroup: defaultSelectedFilterGroupSelector(state, props)
  };
}

export default connect(
  mapStateToProps,
  { fetchTasksForEntity, fetchEnumerations, push }
)(WarehousePage);
