import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import PageWrapper from "../../../designSystem/molecules/PageWrapper";
import styled from "styled-components";
import { space } from "styled-system";
import { palette } from "styled-tools";
import memoizeOne from "memoize-one";
import {
  fieldsForBasketForm,
  fieldsForSearchForm,
  fieldsForClientForm
} from "../../../fields/formFields/search/fieldsForSearchPage";
import { ContainerContext, FieldContext } from "../../common/formControls";
import { push } from "connected-react-router";

import Icon from "../../../designSystem/atoms/Icon";
import Button from "../../../designSystem/mixins/Button";
import Text from "../../../designSystem/mixins/Text";
import Block from "../../../designSystem/organisms/Block";
import Box from "../../../designSystem/organisms/Box";
import { renderRoutes } from "react-router-config";
import { reduceValuesToFormFields } from "../../../fields/helpers";
import { getUrl } from "../../../helpers/urls";
import { generateId, generateUrlWithQuery } from "../../../utils";
import { queryRouterSelector } from "../../../redux/selectors/router";
import {
  fetchEnumerations,
  enumerationStateSelector
} from "../../../ducks/enumerations";
import { basketOptionsSelector } from "../../../ducks/baskets";
import ChangeCityBlock from "../../blocks/search/ChangeCityBlock";
import {
  pageFiltersSelector,
  resetPageFilters,
  changeAgentAttachedPage,
  changeBasketAttachedPage,
  updatePageFilters
} from "../../../ducks/search";

const enumerationNames = ["city"];

const CatalogueTxt = styled.span`
  ${Text};
  line-height: 1;

  :hover {
    color: #fff;
  }
`;

const CatalogueBtn = styled.a`
  ${Button};
  display: flex;
  align-items: center;
  width: 200px;
  min-height: 36px;
  line-height: 36px;
  padding: 6px 14px;
  border-radius: 5px;
  background: ${palette("grayscale", 2)};
  outline: none;
  transition: background 0.3s;
  color: #3b3b3b;

  :hover {
    background: ${palette("grayscale", 1)};
    color: #fff;

    & ${CatalogueTxt} {
      color: #fff;
    }
  }

  ${space};
`;

const CatalogueIcon = styled(Icon)`
  width: 18px;
  margin-right: 10px;
`;

const HeaderNavLink = styled.a`
  ${Text};
  font-size: 18px;
  white-space: nowrap;
  text-decoration: none;
`;

class SearchPage extends Component {
  static propTypes = {
    route: PropTypes.object,
    query: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    basketOptions: PropTypes.array.isRequired,
    cities: PropTypes.shape({
      map: PropTypes.object.isRequired,
      list: PropTypes.array.isRequired
    }).isRequired,
    changeAgentAttachedPage: PropTypes.func.isRequired,
    resetPageFilters: PropTypes.func.isRequired,
    pageFilters: PropTypes.object.isRequired,
    changeBasketAttachedPage: PropTypes.func.isRequired,
    updatePageFilters: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
  }

  componentWillUnmount() {
    this.props.resetPageFilters();
  }

  mapSearchFields = memoizeOne((searchString, fields) => {
    return {
      fields: fields.reduce(reduceValuesToFormFields({ searchString }), []),
      key: generateId()
    };
  });

  mapBasketFields = memoizeOne((basketId, basketOptions, fields) => {
    const enumerations = { basketList: { list: basketOptions } };

    return {
      fields: fields.reduce(
        reduceValuesToFormFields({ basketId }, enumerations),
        []
      ),
      key: generateId()
    };
  });

  mapClientFields = memoizeOne((client, fields) => {
    return {
      fields: fields.reduce(reduceValuesToFormFields(client), []),
      key: generateId()
    };
  });

  handleSubmitSearchForm = values => {
    this.props.push(
      generateUrlWithQuery({ pathname: getUrl("search"), newQuery: values })
    );
  };

  handleSubmitClientForm = values => {
    this.props.changeAgentAttachedPage(values.clientCode);
  };

  handleSubmitBasketForm = ({ basketId }) => {
    this.props.changeBasketAttachedPage(basketId);
  };

  handleChangeCity = (_, { id }) => {
    this.props.updatePageFilters({ cityId: id });
  };

  render() {
    const { route, query, cities, basketOptions, pageFilters } = this.props;
    const { fields: searchFields, key: searchKey } = this.mapSearchFields(
      query.searchString,
      fieldsForSearchForm
    );
    const { fields: basketFields, key: basketKey } = this.mapBasketFields(
      pageFilters.basketId,
      basketOptions,
      fieldsForBasketForm
    );
    const { fields: clientFields, key: clientKey } = this.mapClientFields(
      pageFilters.agent,
      fieldsForClientForm
    );

    return (
      <PageWrapper>
        <Block>
          <Box
            display="grid"
            gridTemplateColumns="200px 375px 1fr 1fr 1fr"
            gridColumnGap="24px"
            alignItems="center"
            py="20px"
            px="24px"
          >
            <CatalogueBtn mr="24px" target="_blank" href="//laf24.ru">
              <CatalogueIcon icon="menu" />
              <CatalogueTxt>Каталог товаров</CatalogueTxt>
            </CatalogueBtn>
            <ContainerContext
              onSubmitForm={this.handleSubmitSearchForm}
              fields={searchFields}
              key={searchKey}
            >
              <FieldContext name={searchFields[0].id} />
            </ContainerContext>
            <HeaderNavLink target="_blank" href="//laf24.ru/cars">
              Поиск по автомобилю
            </HeaderNavLink>
            <HeaderNavLink target="_blank" href="//laf24.ru/client">
              О компании
            </HeaderNavLink>
            <Box
              display="grid"
              gridTemplateColumns="225px 110px 175px"
              gridColumn={5}
              gridColumnGap="20px"
              alignItems="center"
              ml="auto"
            >
              <ContainerContext
                onSubmitForm={this.handleSubmitClientForm}
                fields={clientFields}
                key={clientKey}
                autoSubmit
              >
                <FieldContext name={clientFields[0].id} />
              </ContainerContext>
              <ContainerContext
                onSubmitForm={this.handleSubmitBasketForm}
                fields={basketFields}
                key={basketKey}
                autoSubmit
              >
                <FieldContext name={basketFields[0].id} />
              </ContainerContext>
              <ChangeCityBlock
                onChangeCity={this.handleChangeCity}
                selectedCityId={pageFilters.cityId}
                cities={cities}
              />
            </Box>
          </Box>
        </Block>
        {renderRoutes(route.routes)}
      </PageWrapper>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    cities: enumerationStateSelector(state, { name: "city" }),
    query: queryRouterSelector(state, props),
    basketOptions: basketOptionsSelector(state, props),
    pageFilters: pageFiltersSelector(state, props)
  };
}

export default connect(
  mapStateToProps,
  {
    push,
    fetchEnumerations,
    changeAgentAttachedPage,
    resetPageFilters,
    changeBasketAttachedPage,
    updatePageFilters
  }
)(SearchPage);
