import React from "react";

export const DefaultState = {
  cityId: 2,
  agent: null,
  basketId: null
};

export const ContextSearch = React.createContext(DefaultState);
