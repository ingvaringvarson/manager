import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  brandsSelector,
  searchBrands,
  moduleName,
  brandRequestIdSelector
} from "../../../ducks/search";
import { moduleName as productsModuleName } from "../../../ducks/products";
import { push } from "connected-react-router";
import { queryRouterSelector } from "../../../redux/selectors/router";
import Grid from "../../../designSystem/templates/Grid";
import Block from "../../../designSystem/organisms/Block";
import Title from "../../../designSystem/atoms/Title";
import Button from "../../../designSystem/atoms/Button";
import styled from "styled-components";
import Table from "../../common/tableView/Table";
import { fieldsForTable } from "../../../fields/formFields/search/fieldsForSearchResultPage";
import { getUrl } from "../../../helpers/urls";
import { modulesIsLoadingSelector } from "../../../ducks/logger";
import PreLoader from "../../common/PreLoader";

const StartBlock = styled.div`
  font-size: 18px;
  color: #787878;
  margin-top: 30px;
  margin-bottom: 30px;
  text-align: center;
`;

const TitleResult = styled(Title)`
  font-size: 24px;
`;

const TableResult = styled(Table)`
  width: 30%;
  min-width: 500px;
  margin-top: 30px;
`;

class SearchResultPage extends Component {
  static propTypes = {
    brands: PropTypes.object,
    query: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    searchBrands: PropTypes.func.isRequired,
    brandRequestId: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired
  };

  componentDidMount() {
    if (this.props.query.searchString) {
      this.props.searchBrands(
        this.props.query.searchString,
        this.props.brandRequestId
      );
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.query.searchString !== prevProps.query.searchString) {
      this.props.searchBrands(
        this.props.query.searchString,
        this.props.brandRequestId
      );
    }
  }

  handleMoveToProductList = ({ entity }) => () => {
    const { mainVendorCode, brandKey } = entity;

    if (!mainVendorCode) return false;

    this.props.push(
      getUrl("products", { brandKey, vendorCode: mainVendorCode.vendorCodeKey })
    );
  };

  handlers = {
    onClick: this.handleMoveToProductList
  };

  renderStartBlock() {
    return (
      <StartBlock>
        Введите артикул или VIN-номер для получения
        <br />
        списка предложений товаров
      </StartBlock>
    );
  }

  rowBodyHandlers = {
    onClick: this.handleMoveToProductList
  };

  renderTable() {
    const { brands, query, brandRequestId } = this.props;
    const result = brands[brandRequestId];

    return (
      <>
        <TitleResult mb="30px">
          По запросу "{query.searchString}" было найдено несколько совпадений в
          каталогах разных брендов
        </TitleResult>
        <Title>Выберите из списка интересующий вас бренд</Title>
        <TableResult
          rowBodyHandlers={this.rowBodyHandlers}
          cells={fieldsForTable}
          entities={result.entities}
        />
      </>
    );
  }

  renderEmptyResult() {
    const { query } = this.props;

    return (
      <>
        <TitleResult mb="30px">
          По запросу "{query.searchString}" не было найдено ни одного товара
        </TitleResult>
        <Title>
          Пожалуйста, проверьте правильно ли введён артикул или{" "}
          <Button transparent mr="10px" ml="10px">
            Создайте запрос
          </Button>{" "}
          менеджеру по закупке для поиска оффера товара
        </Title>
      </>
    );
  }

  renderSearchResult() {
    const { brands, query, brandRequestId } = this.props;

    if (
      !brands.hasOwnProperty(brandRequestId) ||
      !query.hasOwnProperty("searchString")
    ) {
      return this.renderStartBlock();
    }

    const result = brands[brandRequestId];

    switch (true) {
      case !result.isLoaded: {
        return null;
      }
      case !result.entities.length: {
        return this.renderEmptyResult();
      }
      case !!result.entities.length: {
        return this.renderTable();
      }
      default: {
        return null;
      }
    }
  }

  render() {
    return (
      <Grid mt="16px">
        {this.props.isLoading && <PreLoader />}
        <Block p="32px">{this.renderSearchResult()}</Block>
      </Grid>
    );
  }
}

const moduleNames = [moduleName, productsModuleName];

function mapDerivedStateFromProps(state, props) {
  return {
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    brands: brandsSelector(state, props),
    query: queryRouterSelector(state, props),
    brandRequestId: brandRequestIdSelector(state, props)
  };
}
export default connect(
  mapDerivedStateFromProps,
  { push, searchBrands }
)(SearchResultPage);
