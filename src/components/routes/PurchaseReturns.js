import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { generateId } from "../../utils";

import { push } from "connected-react-router";
import { connect } from "react-redux";
import {
  fetchPurchaseReturns,
  purchaseReturnsSelector,
  requestParamsForListSelector,
  moduleName as moduleNamePurchaseReturns,
  maxPagesSelector
} from "../../ducks/purchaseReturns";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import { moduleName as moduleNamePositions } from "../../ducks/positions";
import {
  pathnameRouterSelector,
  keyRouterSelector
} from "../../redux/selectors/router";

import Grid from "../../designSystem/templates/Grid";
import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Block from "../../designSystem/organisms/Block";
import Heading from "../../designSystem/molecules/Heading";
import BlockContent from "../../designSystem/organisms/BlockContent";

import ClearButtonHeading from "../common/styled/ClearButtonHeading";
import PreLoader from "../common/PreLoader";
import FieldForm from "../common/form/FieldForm";
import { ContainerContext, FormContext } from "../common/formControls";

import { modulesIsLoadingSelector } from "../../ducks/logger";
import { generateUrlWithQuery } from "../../utils";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../fields/helpers";
import { fields as fieldsForList } from "../../fields/formFields/purchaseReturns/fieldsForList";
import { fields as fieldsForFilters } from "../../fields/formFields/purchaseReturns/fieldsForFilters";
import PurchaseReturnsTable from "../blocks/purchaseReturns/PurchaseReturnsTable";

const enumerationNames = [
  "returnReason",
  "returnPurpose",
  "purchaseReturnState",
  "productType",
  "orderPositionGoodState",
  "productUnitState",
  "orderState",
  "returnState"
];

const moduleNames = [
  moduleNamePurchaseReturns,
  moduleNameEnumerations,
  moduleNamePositions
];

const propTypes = {
  enumerations: PropTypes.object,
  requestParams: PropTypes.object,
  isLoading: PropTypes.bool
};

class PurchaseReturns extends PureComponent {
  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
    this.fetchEntities();
  }

  componentDidUpdate = prevProps => {
    if (prevProps.keyRouter !== this.props.keyRouter) {
      this.fetchEntities();
    }
  };

  fetchEntities = () => {
    this.props.fetchPurchaseReturns(this.props.requestParams);
  };

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  renderCells = memoizeOne((requestParams, enumerations) => {
    const cellsForTable = fieldsForList.reduce(
      reduceValuesToFormFields(requestParams, enumerations),
      []
    );
    const fieldsForFiltersMapped = fieldsForFilters.reduce(
      reduceValuesToFormFields(requestParams, enumerations),
      []
    );
    const fields = cellsForTable.concat(fieldsForFiltersMapped);

    return {
      cellsForTable,
      fieldsForFilters: fieldsForFiltersMapped,
      fields,
      key: generateId()
    };
  });

  render() {
    const { requestParams, enumerations, purchaseReturns } = this.props;
    const { cellsForTable, fieldsForFilters, fields, key } = this.renderCells(
      requestParams,
      enumerations,
      purchaseReturns
    );

    return (
      <PageWrapper>
        <ContainerContext
          key={key}
          autoSubmit
          onSort={this.handleSort}
          fields={fields}
          onSubmitForm={this.handleSubmitForm}
          externalForm
          submitValidation
        >
          <Grid filterRight>
            <PurchaseReturnsTable
              requestParams={this.props.requestParams}
              pathname={this.props.pathname}
              keyRouter={this.props.keyRouter}
              enumerations={this.props.enumerations}
              purchaseReturns={this.props.purchaseReturns}
              push={this.props.push}
              maxPages={this.props.maxPages}
              fields={cellsForTable}
              fetchEntities={this.fetchEntities}
            />
            <Block>
              <Heading title="Фильтрация">
                <ClearButtonHeading
                  title="Сбросить все"
                  fields={fieldsForFilters}
                />
              </Heading>
              <BlockContent>
                <FormContext>
                  {fieldsForFilters.map((field, i) => {
                    return <FieldForm key={i} field={field} index={i} />;
                  })}
                </FormContext>
              </BlockContent>
            </Block>
          </Grid>
        </ContainerContext>
        {!!this.props.isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

PurchaseReturns.propTypes = propTypes;

export default connect(
  state => {
    return {
      enumerations: enumerationsSelector(state, { enumerationNames }),
      keyRouter: keyRouterSelector(state),
      isLoading: modulesIsLoadingSelector(state, { moduleNames }),
      requestParams: requestParamsForListSelector(state),
      purchaseReturns: purchaseReturnsSelector(state),
      maxPages: maxPagesSelector(state),
      pathname: pathnameRouterSelector(state)
    };
  },
  {
    fetchEnumerations,
    fetchPurchaseReturns,
    push
  }
)(PurchaseReturns);
