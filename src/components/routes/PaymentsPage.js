import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  paymentListSelector,
  maxPagesSelector,
  fetchPayments,
  moduleName as moduleNamePayments,
  requestParamsSelector
} from "../../ducks/payments";
import {
  pathnameRouterSelector,
  keyRouterSelector
} from "../../redux/selectors/router";
import Pagination from "../blocks/Pagination";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations,
  enumerationListWithoutZeroIdSelector
} from "../../ducks/enumerations";
import { push } from "connected-react-router";
import { ContainerContext, FormContext } from "../common/formControls";
import memoizeOne from "memoize-one";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import Container from "../../designSystem/templates/Container";
import Row from "../../designSystem/templates/Row";
import Column from "../../designSystem/templates/Column";
import Grid from "../../designSystem/templates/Grid";
import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Block from "../../designSystem/organisms/Block";
import Heading from "../../designSystem/molecules/Heading";
import BlockContent from "../../designSystem/organisms/BlockContent";
import EmptyResult from "../common/EmptyResult";
import PreLoader from "../common/PreLoader";
import FieldForm from "../common/form/FieldForm";
import HeadingCard from "../common/headlines/HeadingCard";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../common/tabControls";
import TableWithLinks from "../common/table/TableWithLinks";
import CashCommands from "../blocks/CashCommands";
import TableRow from "../../designSystem/molecules/TableRow";
import TableCell from "../../designSystem/molecules/TableCell";
import Title from "../../designSystem/atoms/Title";
import {
  getStateByDefaultValuesForFields,
  getStateByRequestInfo
} from "../../helpers/component/staticMethods";
import {
  handleChangeTab,
  handleMoveToEntityPage,
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields
} from "../../helpers/component/handlers";
import {
  fieldsForFilters,
  fieldsForTableByType
} from "../../fields/formFields/payments/paymentsPage";
import ClearButtonHeading from "../common/styled/ClearButtonHeading";
import { defaultValuesForFieldsSelector } from "../../helpers/selectors";
import { employeeInfoByUserSelector } from "../../ducks/user";

const enumerationNames = ["paymentMethod", "productType", "paymentState"];

class PaymentsPage extends Component {
  static propTypes = {
    requestParams: PropTypes.object.isRequired,
    payments: PropTypes.array.isRequired,
    enumerations: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,
    push: PropTypes.func.isRequired,
    maxPages: PropTypes.number.isRequired,
    queryKeyByTab: PropTypes.string.isRequired,
    paymentMethods: PropTypes.array.isRequired,
    fetchPayments: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    keyRouter: PropTypes.string.isRequired,
    typeEntity: PropTypes.string.isRequired,
    valuesForFields: PropTypes.object.isRequired,
    fieldsForTable: PropTypes.array.isRequired,
    fieldsForFilters: PropTypes.array.isRequired,
    selectedTab: PropTypes.number.isRequired,
    isLoading: PropTypes.bool.isRequired,
    employee: PropTypes.object,
    defaultValuesForFields: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      isLoaded: false,
      valuesForFields: null,
      prevValuesForFields: null
    };

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleChangeTab = handleChangeTab.bind(this);
    this.handleMoveToEntityPage = handleMoveToEntityPage.bind(this);
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
    this.rowBodyHandlers = {
      onClick: this.handleMoveToEntityPage
    };
  }

  static getDerivedStateFromProps(props, state) {
    let newState = null;

    newState = getStateByRequestInfo(props, state, newState);
    newState = getStateByDefaultValuesForFields(props, state, newState);

    return newState;
  }

  fetchEntities = () => {
    this.props.fetchPayments(this.state.valuesForFields);
  };

  componentDidMount() {
    this.fetchEntities();
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.fetchEntities();
    }
  }

  renderTableHead = () => {
    const { selectedTab } = this.props;
    return (
      <TableRow>
        <TableCell flex="2 1 0">
          <Title>Клиент</Title>
        </TableCell>
        <TableCell flex={`${selectedTab === 3 ? "6" : "7"} 1 0`}>
          <Title>Платёж</Title>
        </TableCell>
      </TableRow>
    );
  };

  render() {
    const {
      requestParams,
      payments,
      enumerations,
      pathname,
      push,
      maxPages,
      isLoading,
      paymentMethods,
      selectedTab,
      employee
    } = this.props;
    const {
      fieldsForTable,
      fieldsForFilters,
      allFields,
      key
    } = this.mapValuesToFields(enumerations, payments);

    return (
      <PageWrapper>
        <Container>
          <Row>
            <Column grow="2" basis="1200px" mr="middle">
              <HeadingCard
                title="Торговая точка"
                name={employee ? employee.warehouse_name : ""}
              />
            </Column>
            <Column mb="10px" grow="2" basis="350px" maxWidth="350px">
              <CashCommands fetchEntities={this.fetchEntities} />
            </Column>
          </Row>
        </Container>
        <ContainerContext
          key={key}
          onSort={this.handleTableSort}
          fields={allFields}
          onSubmitForm={this.handleSubmitForm}
          autoSubmit
          externalForm
          submitValidation
        >
          <Grid filterRight>
            <Block>
              <Heading title="Список клиентов">
                <ClearButtonHeading
                  title="Сбросить все"
                  fields={this.props.fieldsForTable}
                />
              </Heading>
              <BlockContent table>
                <FormContext>
                  <TableWithLinks
                    key={key}
                    cells={fieldsForTable}
                    entities={payments}
                    enumerations={enumerations}
                    hasFormControl
                    rowBodyHandlers={this.rowBodyHandlers}
                    renderBeforeContent={this.renderTableHead}
                    typeEntity="payment"
                  />
                </FormContext>
                {this.state.isLoaded && !payments.length && <EmptyResult />}
                <Pagination
                  pathname={pathname}
                  push={push}
                  requestParams={this.state.valuesForFields}
                  page={+requestParams.page}
                  maxPages={maxPages}
                />
              </BlockContent>
            </Block>
            <Block mb="middle">
              <Heading title="Фильтрация">
                <ClearButtonHeading
                  title="Сбросить все"
                  fields={this.props.fieldsForFilters}
                />
              </Heading>
              <TabContainer
                selectedTabId={selectedTab}
                onChangeSelected={this.handleChangeTab}
              >
                <TabHeadContainer>
                  {paymentMethods.map(item => {
                    return (
                      <TabHead key={item.id} id={item.id}>
                        {item.name}
                      </TabHead>
                    );
                  })}
                </TabHeadContainer>
                <TabBodyContainer>
                  {paymentMethods.map(item => {
                    return (
                      <TabBody key={item.id} id={item.id}>
                        <BlockContent>
                          <FormContext>
                            {fieldsForFilters.map((filter, index) => (
                              <FieldForm
                                key={index}
                                field={filter}
                                index={index}
                              />
                            ))}
                          </FormContext>
                        </BlockContent>
                      </TabBody>
                    );
                  })}
                </TabBodyContainer>
              </TabContainer>
            </Block>
          </Grid>
        </ContainerContext>
        {isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

const moduleNames = [moduleNamePayments, moduleNameEnumerations];

function mapStateToProps(state, props) {
  const requestParams = requestParamsSelector(state, props);
  const queryKeyByTab = "payment_method";
  const selectedTab = +requestParams[queryKeyByTab];

  return {
    requestParams,
    keyRouter: keyRouterSelector(state, props),
    pathname: pathnameRouterSelector(state, props),
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    maxPages: maxPagesSelector(state, props),
    enumerations: enumerationsSelector(state, { enumerationNames }),
    queryKeyByTab,
    typeEntity: "payments",
    valuesForFields: requestParams,
    fieldsForTable: fieldsForTableByType[selectedTab],
    fieldsForFilters,
    selectedTab,
    payments: paymentListSelector(state, props),
    paymentMethods: enumerationListWithoutZeroIdSelector(state, {
      name: "paymentMethod"
    }),
    defaultValuesForFields: defaultValuesForFieldsSelector(state, props),
    employee: employeeInfoByUserSelector(state, props)
  };
}

export default connect(
  mapStateToProps,
  {
    fetchPayments,
    push,
    fetchEnumerations
  }
)(PaymentsPage);
