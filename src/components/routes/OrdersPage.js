import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";
import { push } from "connected-react-router";

import {
  orderListWithBillIdSelector,
  maxPagesSelector,
  fetchOrders,
  defaultValuesForFieldsSelector,
  moduleName as moduleNameOrders
} from "../../ducks/orders";
import { moduleName as moduleNameReturns } from "../../ducks/returns";
import { moduleName as moduleNameBills } from "../../ducks/bills";
import { moduleName as moduleNameClients } from "../../ducks/clients";
import { moduleName as moduleNamePositions } from "../../ducks/positions";
import {
  requestParamsSelector,
  pathnameRouterSelector,
  keyRouterSelector
} from "../../redux/selectors/router";
import Pagination from "../blocks/Pagination";
import TableWithLinks from "../common/table/TableWithLinks";

import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import {
  fieldsForTable,
  fieldsForPositions,
  fieldsForFilters
} from "../../fields/formFields/orders/ordersPage";
import { ContainerContext, FormContext } from "../common/formControls";
import {
  TableContainer,
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore,
  renderRowFormControlChildrenAfter
} from "../common/tableControls";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import Grid from "../../designSystem/templates/Grid";
import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Block from "../../designSystem/organisms/Block";
import Heading from "../../designSystem/molecules/Heading";
import BlockContent from "../../designSystem/organisms/BlockContent";
import Box from "../../designSystem/organisms/Box";
import EmptyResult from "../common/EmptyResult";
import PreLoader from "../common/PreLoader";
import FieldForm from "../common/form/FieldForm";
import ClearButtonHeading from "../common/styled/ClearButtonHeading";
import TableRow from "../../designSystem/molecules/TableRow";
import TableCell from "../../designSystem/molecules/TableCell";
import Title from "../../designSystem/atoms/Title";
import {
  handleChangeTab,
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields
} from "../../helpers/component/handlers";

import OrdersTabLinks from "../blocks/helperBlocks/OrdersTabLinks";
import NestedTableForEntity from "../blocks/positions/NestedTableForEntity";

import {
  getStateByDefaultValuesForFields,
  getStateByRequestInfo
} from "../../helpers/component/staticMethods";
import {
  renderOrderControl,
  renderHeadOrderControl
} from "../../helpers/component/byEntity/orders";
import OrderPositionsMenu from "../blocks/orders/OrderPositionsMenu";

const enumerationNames = [
  "deliveryType",
  "orderState",
  "productType",
  "orderPositionGoodState",
  "orderPositionServiceState",
  "productUnitState",
  "returnReason",
  "returnPurpose"
];

class OrdersPage extends Component {
  static propTypes = {
    requestParams: PropTypes.object.isRequired,
    orders: PropTypes.array.isRequired,
    enumerations: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,
    push: PropTypes.func.isRequired,
    maxPages: PropTypes.number.isRequired,
    fetchOrders: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    keyRouter: PropTypes.string.isRequired,
    typeEntity: PropTypes.string.isRequired,
    valuesForFields: PropTypes.object.isRequired,
    fieldsForTable: PropTypes.array.isRequired,
    fieldsForFilters: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    defaultValuesForFields: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      isLoaded: false,
      valuesForFields: null,
      prevValuesForFields: null
    };

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleChangeTab = handleChangeTab.bind(this);
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    let newState = null;

    newState = getStateByRequestInfo(props, state, newState);
    newState = getStateByDefaultValuesForFields(props, state, newState);

    return newState;
  }

  componentDidMount() {
    this.fetchEntities();
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.fetchEntities();
    }
  }

  fetchEntities = () => {
    this.props.fetchOrders({
      ...this.state.valuesForFields,
      isForRepeat: true
    });
  };

  getRenderOrderControl = renderOrderControl({
    ...this.props,
    fetchEntities: this.fetchEntities
  });

  getRenderHeadOrderControl = () =>
    renderHeadOrderControl({
      ...this.props,
      fetchEntities: this.fetchEntities
    });

  renderPositionsMenu = order => ({ forList, positions }) => (
    <OrderPositionsMenu
      forList={forList}
      positions={positions}
      order={order}
      fetchEntities={this.fetchEntities}
    />
  );

  renderNestedTable = ({ entity, enumerations }) => {
    return (
      <NestedTableForEntity
        renderPositionsMenu={this.renderPositionsMenu(entity)}
        entity={entity}
        enumerations={enumerations}
        fields={fieldsForPositions}
        positions={entity.positions}
      />
    );
  };

  renderTableHead = () => {
    return (
      <TableRow head>
        <TableCell first />
        <TableCell flex="3 1 0">
          <Title>Клиент</Title>
        </TableCell>
        <TableCell flex="9 1 0">
          <Title>Заказ</Title>
        </TableCell>
        <TableCell control />
      </TableRow>
    );
  };

  render() {
    const {
      requestParams,
      orders,
      enumerations,
      pathname,
      push,
      maxPages,
      isLoading
    } = this.props;
    const {
      fieldsForTable,
      fieldsForFilters,
      allFields,
      key
    } = this.mapValuesToFields(enumerations, orders);

    return (
      <PageWrapper>
        <Grid filterRight>
          <ContainerContext
            key={key}
            onSort={this.handleTableSort}
            fields={allFields}
            onSubmitForm={this.handleSubmitForm}
            autoSubmit
            externalForm
            submitValidation
          >
            <Block>
              <Box position="relative">
                <OrdersTabLinks />
                <ClearButtonHeading
                  title="Сбросить все"
                  fields={fieldsForTable}
                />
              </Box>
              <BlockContent table>
                <FormContext>
                  <TableContainer
                    key={key}
                    entities={orders}
                    canBeSelected
                    hasNestedTable
                  >
                    <TableWithLinks
                      cells={fieldsForTable}
                      entities={orders}
                      enumerations={enumerations}
                      hasFormControl
                      hasChildrenBefore
                      hasChildrenAfter
                      renderBeforeContent={this.renderTableHead}
                      renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
                      renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
                      renderRowFormControlChildrenBefore={
                        renderRowFormControlChildrenBefore
                      }
                      renderRowBodyChildrenAfter={this.getRenderOrderControl}
                      renderRowHeadChildrenAfter={
                        this.getRenderHeadOrderControl
                      }
                      renderRowFormControlChildrenAfter={
                        renderRowFormControlChildrenAfter
                      }
                      renderRowBodyAfter={this.renderNestedTable}
                      typeEntity="order"
                    />
                  </TableContainer>
                </FormContext>
                {this.state.isLoaded && !orders.length && <EmptyResult />}
                <Pagination
                  requestParams={this.state.valuesForFields}
                  pathname={pathname}
                  push={push}
                  page={+requestParams.page}
                  maxPages={maxPages}
                />
              </BlockContent>
            </Block>
            <Block mb="middle">
              <Heading title="Фильтрация">
                <ClearButtonHeading
                  title="Сбросить все"
                  fields={fieldsForFilters}
                />
              </Heading>
              <BlockContent>
                <FormContext>
                  {fieldsForFilters.map((filter, i) => (
                    <FieldForm key={i} field={filter} index={i} />
                  ))}
                </FormContext>
              </BlockContent>
            </Block>
          </ContainerContext>
        </Grid>
        {isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

const moduleNames = [
  moduleNameOrders,
  moduleNameEnumerations,
  moduleNamePositions,
  moduleNameReturns,
  moduleNameBills,
  moduleNameClients
];

function mapStateToProps(state, props) {
  const requestParams = requestParamsSelector(state, props);

  return {
    typeEntity: "orders",
    valuesForFields: requestParams,
    fieldsForTable,
    fieldsForFilters,
    fieldsForPositions,
    enumerations: enumerationsSelector(state, { enumerationNames }),
    maxPages: maxPagesSelector(state, props),
    orders: orderListWithBillIdSelector(state, props),
    requestParams: requestParamsSelector(state, props),
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    pathname: pathnameRouterSelector(state, props),
    keyRouter: keyRouterSelector(state, props),
    defaultValuesForFields: defaultValuesForFieldsSelector(state, props)
  };
}

export default connect(
  mapStateToProps,
  {
    fetchOrders,
    push,
    fetchEnumerations
  }
)(OrdersPage);
