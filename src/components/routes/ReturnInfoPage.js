import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  updateReturn,
  fetchReturn,
  returnInfoSelector,
  moduleName as returnModuleName,
  requestParamsReturnPageSelector
} from "../../ducks/returns";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as enumerationModuleName
} from "../../ducks/enumerations";
import { moduleName as billsModuleName } from "../../ducks/bills";
import { moduleName as paymentsModuleName } from "../../ducks/payments";
import { moduleName as tasksModuleName } from "../../ducks/tasks";
import { moduleName as moduleNamePositions } from "../../ducks/positions";
import {
  fetchClient,
  updateClient,
  clientInfoSelector,
  moduleName as clientsModuleName
} from "../../ducks/clients";
import { modulesIsLoadingSelector } from "../../ducks/logger";

import Container from "../../designSystem/templates/Container";
import Grid from "../../designSystem/templates/Grid";
import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Block from "../../designSystem/organisms/Block";
import Box from "../../designSystem/organisms/Box";

import PreLoader from "../common/PreLoader";

import HeaderReturnPage from "../blocks/returns/HeaderReturnPage";
import ReturnInfoBlocks from "../blocks/returns/ReturnInfoBlocks";
import ReturnTabs from "../blocks/returns/ReturnTabs";

import InfoPageFooter from "../common/InfoPageFooter";

const enumerationNames = [
  "returnReason",
  "returnPurpose",
  "returnState",
  "productType",
  "productUnitState",
  "orderPositionGoodState",
  "personGender",
  "agentTag",
  "agentSegment",
  "paymentMethod",
  "taskState",
  "taskType",
  "taskReason",
  "contactType",
  "paymentState",
  "billType",
  "billState"
];

const moduleNames = [
  returnModuleName,
  enumerationModuleName,
  billsModuleName,
  paymentsModuleName,
  tasksModuleName,
  moduleNamePositions,
  clientsModuleName
];

class ReturnInfoPage extends PureComponent {
  static propTypes = {
    returnId: PropTypes.string,
    returnEntity: PropTypes.object,
    clientId: PropTypes.string,
    enumerations: PropTypes.object,
    isLoading: PropTypes.bool,
    requestParams: PropTypes.object,
    client: PropTypes.object
  };

  static getDerivedStateFromProps = (props, state) => {
    if (
      props.returnEntity &&
      props.returnEntity.return &&
      props.returnEntity.return.agent_id &&
      props.returnEntity !== state.returnEntity
    ) {
      return {
        returnEntity: props.returnEntity,
        isClientReadyToFetch: true
      };
    }
    return null;
  };

  state = {
    returnEntity: null,
    isClientReadyToFetch: false
  };

  fetchEntities = () => {
    this.props.fetchReturn(this.props.returnId, { id: this.props.returnId });
  };

  componentDidMount = () => {
    this.fetchEntities();
    this.props.fetchEnumerations(enumerationNames);
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (this.state.isClientReadyToFetch && !prevState.isClientReadyToFetch) {
      this.props.fetchClient(this.props.clientId, { id: this.props.clientId });
    }
  };

  handleSubmitEditComment = params => {
    this.props.updateReturn(this.props.returnId, {
      ...this.props.returnEntity.return,
      ...params
    });
  };

  handleSubmitEditForm = params => {
    this.props.updateClient(this.props.client.id, params, this.props.client);
  };

  render() {
    if (!this.props.returnEntity) {
      return this.props.isLoading ? (
        <PageWrapper>
          <PreLoader />
        </PageWrapper>
      ) : null;
    }

    const { returnEntity, client, enumerations } = this.props;

    return (
      <PageWrapper>
        <Container>
          <HeaderReturnPage
            returnEntity={returnEntity}
            enumerations={enumerations}
            fetchEntities={this.fetchEntities}
          />
        </Container>
        <Grid filterLeft>
          <Box>
            <ReturnInfoBlocks
              returnEntity={returnEntity}
              enumerations={enumerations}
              client={client}
              onSubmitForm={this.handleSubmitEditForm}
            />
          </Box>
          <Box>
            <Block mb="middle">
              <ReturnTabs
                returnEntity={returnEntity}
                enumerations={enumerations}
                client={client}
              />
            </Block>
            <InfoPageFooter
              onSubmitComment={this.handleSubmitEditComment}
              entity={returnEntity.return}
              client={client}
            />
          </Box>
        </Grid>
        {this.props.isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

export default connect(
  (state, props) => {
    const returnId = props.match.params.id;
    const returnEntity = returnInfoSelector(state, { id: returnId });
    const clientId =
      returnEntity && returnEntity.return ? returnEntity.return.agent_id : null;
    return {
      returnId,
      returnEntity,
      clientId,
      enumerations: enumerationsSelector(state, { enumerationNames }),
      isLoading: modulesIsLoadingSelector(state, { moduleNames }),
      requestParams: requestParamsReturnPageSelector(state, props),
      client: clientInfoSelector(state, { idEntity: returnId, id: clientId })
    };
  },
  { fetchReturn, fetchEnumerations, fetchClient, updateReturn, updateClient }
)(ReturnInfoPage);
