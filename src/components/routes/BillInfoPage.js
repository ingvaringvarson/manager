import React, { Component } from "react";
import PropTypes from "prop-types";
import { push } from "connected-react-router";
import BillInfoBlocks from "../blocks/bills/BillInfoBlocks";
import { connect } from "react-redux";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import {
  pathnameRouterSelector,
  queryRouterSelector,
  keyRouterSelector,
  entityIdForRouteSelector
} from "../../redux/selectors/router";
import {
  moduleName as moduleNameBills,
  billInfoSelector,
  fetchBill,
  requestParamsForBillPageSelector,
  updateBill
} from "../../ducks/bills";

import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Container from "../../designSystem/templates/Container";
import Row from "../../designSystem/templates/Row";
import Column from "../../designSystem/templates/Column";

import PreLoader from "../common/PreLoader";
import {
  clientInfoSelector,
  updateClient,
  moduleName as moduleNameClients
} from "../../ducks/clients";
import Block from "../../designSystem/organisms/Block";
import { getDateWithMonthName } from "../../utils";
import Icon from "../../designSystem/atoms/Icon";
import BillTabs from "../blocks/bills/BillTabs";
import EditComment from "../forms/EditComment";
import { GreyText, InfoText } from "../common/styled/text";
import HeaderBillPage from "../blocks/bills/HeaderBillPage";

const enumerationNames = [
  "productType",
  "billState",
  "billType",
  "paymentMethod",
  "paymentType",
  "paymentState",
  "contactType",
  "agentTag",
  "agentSegment",
  "paymentPlace",
  "personGender"
];

const moduleNames = [
  moduleNameBills,
  moduleNameEnumerations,
  moduleNameClients
];

class BillInfoPage extends Component {
  static propTypes = {
    fetchEnumerations: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    updateClient: PropTypes.func.isRequired,
    fetchBill: PropTypes.func.isRequired,
    billId: PropTypes.string.isRequired,
    bill: PropTypes.object.isRequired,
    keyRouter: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
    enumerations: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,
    queryRouter: PropTypes.object.isRequired,
    client: PropTypes.object.isRequired,
    requestParams: PropTypes.object.isRequired,
    updateBill: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
    this.props.fetchBill(this.props.billId, {
      ...this.props.requestParams,
      forInfoPage: true
    });
  }

  handleChangeTab = id => {
    const queryForTab = id === 1 ? "" : `?_tab=${id}`;
    this.props.push(`${this.props.pathname}${queryForTab}`);
  };

  handleSubmitEditForm = params => {
    this.props.updateClient(this.props.client.id, params, this.props.client);
  };

  handleSubmitEditComment = params => {
    this.props.updateBill(this.props.billId, this.props.bill, params);
  };

  render() {
    const { bill, enumerations, client, isLoading, queryRouter } = this.props;

    if (!this.props.bill) {
      return this.props.isLoading ? (
        <PageWrapper>
          <PreLoader />
        </PageWrapper>
      ) : null;
    }

    return (
      <PageWrapper>
        <Container>
          <HeaderBillPage
            bill={bill}
            client={client}
            enumerations={enumerations}
          />
          <Row>
            <Column basis="350px" maxWidth="350px" mr="16px">
              <BillInfoBlocks
                bill={bill}
                enumerations={enumerations}
                onSubmitForm={this.handleSubmitEditForm}
                client={client}
                push={this.props.push}
              />
            </Column>
            <Column basis="1200px" grow="2">
              <Block mb="middle">
                <BillTabs
                  billId={bill.id}
                  onChangeTab={this.handleChangeTab}
                  selectedTabId={queryRouter._tab ? +queryRouter._tab : 1}
                />
              </Block>
              <Row m="0px">
                <EditComment
                  onSubmitForm={this.handleSubmitEditComment}
                  comment={bill.comment ? bill.comment : ""}
                />
              </Row>
              <Row m="0px">
                <Column basis="250px" maxWidth="250px" mr="16px" mt="16px">
                  {!!bill.object_date_create && (
                    <InfoText>
                      <GreyText>Дата создания:</GreyText>{" "}
                      {getDateWithMonthName(bill.object_date_create)}
                    </InfoText>
                  )}
                  {!!bill.object_name_create && (
                    <InfoText>
                      <GreyText>Автор счета:</GreyText>{" "}
                      {bill.object_name_create}
                    </InfoText>
                  )}
                </Column>
                <Column basis="250px" maxWidth="250px" mr="16px" mt="16px">
                  {!!bill.contract_number && (
                    <InfoText>
                      <GreyText>Договор:</GreyText> {bill.contract_number}{" "}
                      <Icon icon="info" width="15px" />
                    </InfoText>
                  )}
                  {bill.history_id && (
                    <InfoText>
                      <GreyText>Старый ID:</GreyText> {bill.history_id}
                    </InfoText>
                  )}
                </Column>
                <Column basis="250px" maxWidth="250px" mr="16px" mt="16px">
                  {!!bill.firm_name && (
                    <InfoText>
                      <GreyText>Фирма:</GreyText> {bill.firm_name}{" "}
                    </InfoText>
                  )}
                </Column>
              </Row>
            </Column>
          </Row>
        </Container>
        {isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

export default connect(
  (state, props) => {
    const billId = entityIdForRouteSelector(state, props);
    const bill = billInfoSelector(state, { id: billId });
    const client = clientInfoSelector(state, {
      id: bill ? bill.agent_id : null
    });

    return {
      requestParams: requestParamsForBillPageSelector(state, props),
      billId,
      bill,
      keyRouter: keyRouterSelector(state),
      isLoading: modulesIsLoadingSelector(state, { moduleNames }),
      enumerations: enumerationsSelector(state, { enumerationNames }),
      pathname: pathnameRouterSelector(state),
      queryRouter: queryRouterSelector(state),
      client
    };
  },
  {
    fetchEnumerations,
    push,
    updateClient,
    fetchBill,
    updateBill
  }
)(BillInfoPage);
