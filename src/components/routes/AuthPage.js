import React, { Component } from "react";
import { connect } from "react-redux";
import PropsTypes from "prop-types";
import { logIn, moduleName } from "../../ducks/user";
import LogInForm from "../forms/LogInForm";
import styled from "styled-components";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import PreLoader from "../common/PreLoader";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100%;
  background: #ffffff;
`;

class AuthPage extends Component {
  static propTypes = {
    logIn: PropsTypes.func.isRequired,
    isLoading: PropsTypes.bool.isRequired
  };

  render() {
    return (
      <Wrapper>
        <LogInForm onSubmitForm={this.props.logIn} />
        {this.props.isLoading && <PreLoader />}
      </Wrapper>
    );
  }
}

const moduleNames = [moduleName];

function mapStateToProps(state) {
  return {
    isLoading: modulesIsLoadingSelector(state, { moduleNames })
  };
}

export default connect(
  mapStateToProps,
  { logIn }
)(AuthPage);
