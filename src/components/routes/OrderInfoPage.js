import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { push } from "connected-react-router";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";

import OrderInfoBlocks from "../blocks/orders/OrderInfoBlocks";
import OrderTabs from "../blocks/orders/OrderTabs";

import {
  moduleName as moduleNameOrders,
  fetchOrderWithClient,
  orderInfoWithBillIdSelector,
  orderInfoSelector,
  updateOrder,
  updateOrderWithEntity,
  mapValuesToOrderByDeliveryChangeSaga
} from "../../ducks/orders";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import {
  pathnameRouterSelector,
  queryRouterSelector,
  keyRouterSelector,
  entityIdForRouteSelector
} from "../../redux/selectors/router";
import { moduleName as moduleNamePayments } from "../../ducks/payments";
import { moduleName as moduleNameBills } from "../../ducks/bills";
import { moduleName as moduleNameReturns } from "../../ducks/returns";
import { moduleName as moduleNamePositions } from "../../ducks/positions";

import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Container from "../../designSystem/templates/Container";
import Grid from "../../designSystem/templates/Grid";
import Box from "../../designSystem/organisms/Box";

import PreLoader from "../common/PreLoader";
import {
  clientInfoSelector,
  updateClient,
  moduleName as clientModuleName
} from "../../ducks/clients";
import Block from "../../designSystem/organisms/Block";
import { generateId } from "../../utils";
import HeaderOrderPage from "../blocks/orders/HeaderOrderPage";

import InfoPageFooter from "../common/InfoPageFooter";

const enumerationNames = [
  "deliveryType",
  "productType",
  "orderPositionGoodState",
  "productUnitState",
  "orderState",
  "paymentMethod",
  "paymentType",
  "paymentState",
  "contactType",
  "agentTag",
  "agentSegment",
  "returnReason",
  "returnPurpose",
  "deliveryTemplate"
];

const moduleNames = [
  moduleNameOrders,
  moduleNameEnumerations,
  moduleNamePayments,
  moduleNameBills,
  clientModuleName,
  moduleNameReturns,
  moduleNamePositions
];

class OrderInfoPage extends PureComponent {
  static propTypes = {
    info: PropTypes.object,
    orderId: PropTypes.string,
    order: PropTypes.object,
    keyRouter: PropTypes.string,
    isLoading: PropTypes.bool,
    enumerations: PropTypes.object,
    pathname: PropTypes.string,
    queryRouter: PropTypes.object,
    client: PropTypes.object,
    fetchEnumerations: PropTypes.func.isRequired,
    fetchOrderWithClient: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    updateClient: PropTypes.func.isRequired,
    updateOrder: PropTypes.func.isRequired,
    updateOrderWithEntity: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
    this.fetchEntities();
  }

  fetchEntities = () => {
    this.props.fetchOrderWithClient(this.props.orderId);
  };

  getStateKey = memoizeOne(() => generateId());

  handleSubmitClientEditForm = params => {
    this.props.updateClient(this.props.client.id, params, this.props.client);
  };

  handleSubmitOrderEditForm = values => {
    this.props.updateOrder(
      this.props.orderId,
      this.props.order,
      values,
      mapValuesToOrderByDeliveryChangeSaga,
      {
        successParams: {
          moveToEntityPage: true
        }
      }
    );
  };

  handleSubmitEditComment = params => {
    this.props.updateOrder(this.props.paymentId, {
      ...this.props.info,
      order: {
        ...this.props.info.order,
        ...params
      }
    });
  };

  handleChangeTab = id => {
    const queryForTab = id === 1 ? "" : `?_tab=${id}`;
    this.props.push(`${this.props.pathname}${queryForTab}`);
  };

  handleConfirmClicked = () => {
    const orderState = this.props.order.state;
    if (orderState === 12) {
      this.props.updateOrderWithEntity(this.props.order.id, this.props.order, {
        state: 1
      });
    }
  };

  render() {
    const { order, enumerations, queryRouter } = this.props;
    const stateKey = this.getStateKey(this.props.client, this.props.order);

    if (!this.props.order) {
      return this.props.isLoading ? (
        <PageWrapper>
          <PreLoader />
        </PageWrapper>
      ) : null;
    }

    return (
      <PageWrapper>
        <Container>
          <HeaderOrderPage
            order={order}
            enumerations={enumerations}
            onConfirmClick={this.handleConfirmClicked}
            fetchEntities={this.fetchEntities}
          />
        </Container>
        <Grid filterLeft>
          <Box>
            <OrderInfoBlocks
              key={stateKey}
              enumerations={this.props.enumerations}
              onSubmitClientForm={this.handleSubmitClientEditForm}
              onSubmitOrderForm={this.handleSubmitOrderEditForm}
              order={this.props.order}
              client={this.props.client}
            />
          </Box>
          <Box>
            <Block mb="middle">
              <OrderTabs
                orderId={this.props.order.id}
                fetchEntities={this.fetchEntities}
                onChangeTab={this.handleChangeTab}
                selectedTabId={queryRouter._tab ? +queryRouter._tab : 1}
              />
            </Block>
            <InfoPageFooter
              client={this.props.client}
              entity={order}
              onSubmitComment={this.handleSubmitEditComment}
            />
          </Box>
        </Grid>
        {this.props.isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

const noClientObject = {};

export default connect(
  (state, props) => {
    const orderId = entityIdForRouteSelector(state, props);
    const order = orderInfoWithBillIdSelector(state, { id: orderId });

    return {
      info: orderInfoSelector(state, { id: orderId }),
      orderId,
      order,
      keyRouter: keyRouterSelector(state),
      isLoading: modulesIsLoadingSelector(state, { moduleNames }),
      enumerations: enumerationsSelector(state, { enumerationNames }),
      pathname: pathnameRouterSelector(state),
      queryRouter: queryRouterSelector(state),
      client:
        clientInfoSelector(state, { id: order ? order.agent_id : null }) ||
        noClientObject
    };
  },
  {
    fetchEnumerations,
    fetchOrderWithClient,
    push,
    updateClient,
    updateOrder,
    updateOrderWithEntity
  }
)(OrderInfoPage);
