import React from "react";
import Container from "../../designSystem/templates/Container";
import PageWrapper from "../../designSystem/molecules/PageWrapper";

function NotFoundPage() {
  return (
    <PageWrapper>
      <Container />
    </PageWrapper>
  );
}

export default NotFoundPage;
