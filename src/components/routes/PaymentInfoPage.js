import React, { Component } from "react";
import PropTypes from "prop-types";
import { push } from "connected-react-router";
import PaymentInfoBlocks from "../blocks/payments/PaymentInfoBlocks";
import {
  fieldsForTable_1,
  fieldsForTable_2,
  fieldsForTable_3
} from "../../fields/formFields/payments/fieldsForPaymentPage";
import { connect } from "react-redux";
import {
  moduleName as moduleNameOrders,
  ordersWithBillIdForEntitySelector
} from "../../ducks/orders";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import {
  pathnameRouterSelector,
  queryRouterSelector,
  keyRouterSelector,
  entityIdForRouteSelector
} from "../../redux/selectors/router";
import {
  moduleName as moduleNamePayments,
  paymentInfoSelector,
  fetchPayment,
  requestParamsForPaymentPageSelector,
  updatePayment
} from "../../ducks/payments";
import {
  billsForEntitySelector,
  moduleName as moduleNameBills
} from "../../ducks/bills";

import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Container from "../../designSystem/templates/Container";
import Row from "../../designSystem/templates/Row";
import Column from "../../designSystem/templates/Column";
import Grid from "../../designSystem/templates/Grid";

import PreLoader from "../common/PreLoader";
import {
  clientInfoSelector,
  updateClient,
  moduleName as clientModuleName
} from "../../ducks/clients";
import HeadingCard from "../common/headlines/HeadingCard";
import HeadingConfirm from "../common/headlines/HeadingConfirm";
import HeadingInfo from "../common/headlines/HeadingInfo";
import Block from "../../designSystem/organisms/Block";
import Box from "../../designSystem/organisms/Box";
import BlockContent from "../../designSystem/organisms/BlockContent";
import { digitsForNumbers, getDateWithMonthName } from "../../utils";
import {
  reduceValuesToFormFields,
  renderEnumerationValue
} from "../../fields/helpers";
import Heading from "../../designSystem/molecules/Heading";
import memoizeOne from "memoize-one";
import nanoId from "nanoid";
import Table from "../common/tableView/Table";
import Icon from "../../designSystem/atoms/Icon";
import EditComment from "../forms/EditComment";
import { RedText, GreyText, InfoText } from "../common/styled/text";

const enumerationNames = [
  "paymentMethod",
  "paymentType",
  "paymentState",
  "contactType",
  "agentTag",
  "agentSegment",
  "paymentPlace",
  "personGender"
];

const moduleNames = [
  moduleNameOrders,
  moduleNameEnumerations,
  moduleNamePayments,
  moduleNameBills,
  clientModuleName
];
const typeEntity = "payments";

class PaymentInfoPage extends Component {
  static propTypes = {
    fetchEnumerations: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    updateClient: PropTypes.func.isRequired,
    fetchPayment: PropTypes.func.isRequired,
    paymentId: PropTypes.string.isRequired,
    orders: PropTypes.arrayOf(PropTypes.object).isRequired,
    bills: PropTypes.arrayOf(PropTypes.object).isRequired,
    payment: PropTypes.object.isRequired,
    keyRouter: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
    enumerations: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,
    queryRouter: PropTypes.object.isRequired,
    client: PropTypes.object.isRequired,
    requestParams: PropTypes.object.isRequired,
    updatePayment: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
    this.props.fetchPayment(this.props.paymentId, {
      ...this.props.requestParams,
      forInfoPage: true
    });
  }

  handleSubmitEditForm = params => {
    this.props.updateClient(this.props.client.id, params, this.props.client);
  };

  handleSubmitEditComment = params => {
    this.props.updatePayment(this.props.paymentId, this.props.payment, params);
  };

  renderHeader() {
    const { payment, enumerations } = this.props;

    const paymentTypeName = renderEnumerationValue(
      "paymentType",
      "payment_type"
    )({ enumerations, entity: payment });
    const paymentStateName = renderEnumerationValue(
      "paymentState",
      "payment_state"
    )({ enumerations, entity: payment });

    return (
      <Row>
        {paymentTypeName && (
          <Column mr="30px">
            <HeadingInfo subTitle="Тип платежа" info={paymentTypeName} />
          </Column>
        )}
        {paymentStateName && (
          <Column mr="30px">
            <HeadingInfo subTitle="Тип транзакции" info={paymentStateName} />
          </Column>
        )}
        <Column mr="30px">
          <HeadingInfo
            subTitle="Сумма платежа"
            info={<RedText>{digitsForNumbers(payment.sum)} руб.</RedText>}
          />
        </Column>
      </Row>
    );
  }

  renderCells = memoizeOne((enumerations, payment) => {
    const entities = [payment];

    if (payment.payment_state === 3) {
      return {
        cells: fieldsForTable_3.reduce(
          reduceValuesToFormFields(payment, enumerations),
          []
        ),
        entities,
        key: nanoId()
      };
    } else if (payment.payment_method === 3) {
      return {
        cells: fieldsForTable_2.reduce(
          reduceValuesToFormFields(payment, enumerations),
          []
        ),
        entities,
        key: nanoId()
      };
    }

    return {
      cells: fieldsForTable_1.reduce(
        reduceValuesToFormFields(payment, enumerations),
        []
      ),
      entities,
      key: nanoId()
    };
  });

  render() {
    const {
      payment,
      enumerations,
      orders,
      bills,
      client,
      isLoading
    } = this.props;

    if (!payment) {
      return isLoading ? (
        <PageWrapper>
          <PreLoader />
        </PageWrapper>
      ) : null;
    }

    const paymentMethodName = renderEnumerationValue(
      "paymentMethod",
      "payment_method"
    )({ enumerations, entity: payment });

    const { cells, entities } = this.renderCells(enumerations, payment);

    return (
      <PageWrapper>
        <Container>
          <Row alignCenter justifyBetween mb="20px">
            <Column>
              <HeadingCard
                title="Карточка платежа"
                name={`id ${payment.code}`}
                status={
                  paymentMethodName ? (
                    <RedText>{paymentMethodName}</RedText>
                  ) : null
                }
              />
            </Column>
            <Column>
              <Row>{this.renderHeader()}</Row>
            </Column>
            <Column>
              <HeadingConfirm />
            </Column>
          </Row>
        </Container>
        <Grid filterLeft>
          <Box>
            <PaymentInfoBlocks
              payment={payment}
              bills={bills}
              enumerations={enumerations}
              onSubmitForm={this.handleSubmitEditForm}
              orders={orders}
              client={client}
              push={this.props.push}
            />
          </Box>
          <Box>
            <Block mb="middle">
              <Heading title="Платёж" />
              <BlockContent>
                <Table
                  entities={entities}
                  cells={cells}
                  enumerations={enumerations}
                />
              </BlockContent>
            </Block>
            <Row m="0px">
              <EditComment
                onSubmitForm={this.handleSubmitEditComment}
                comment={payment.comment ? payment.comment : ""}
              />
            </Row>
            <Row m="0px">
              <Column basis="250px" maxWidth="250px" mr="16px" mt="16px">
                {!!payment.object_date_create && (
                  <InfoText>
                    <GreyText>Дата создания:</GreyText>{" "}
                    {getDateWithMonthName(payment.object_date_create)}
                  </InfoText>
                )}
                {!!payment.object_name_create && (
                  <InfoText>
                    <GreyText>Автор платежа:</GreyText>{" "}
                    {payment.object_name_create}
                  </InfoText>
                )}
              </Column>
              <Column basis="250px" maxWidth="250px" mr="16px" mt="16px">
                {!!payment.contract_number && (
                  <InfoText>
                    <GreyText>Договор:</GreyText> {payment.contract_number}{" "}
                    <Icon icon="info" width="15px" />
                  </InfoText>
                )}
                {!!payment.firm_name && (
                  <InfoText>
                    <GreyText>Фирма:</GreyText> {payment.firm_name}
                  </InfoText>
                )}
              </Column>
            </Row>
          </Box>
        </Grid>

        {this.props.isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

export default connect(
  (state, props) => {
    const paymentId = entityIdForRouteSelector(state, props);
    const idEntity = paymentId;
    const payment = paymentInfoSelector(state, { id: paymentId });
    const orders = ordersWithBillIdForEntitySelector(state, {
      typeEntity,
      idEntity
    });
    const bills = billsForEntitySelector(state, {
      typeEntity,
      idEntity
    });
    const client = clientInfoSelector(state, {
      id: payment ? payment.agent_id : null
    });

    return {
      requestParams: requestParamsForPaymentPageSelector(state, props),
      paymentId,
      orders: orders.entities,
      bills: bills.entities,
      payment,
      keyRouter: keyRouterSelector(state),
      isLoading: modulesIsLoadingSelector(state, { moduleNames }),
      enumerations: enumerationsSelector(state, { enumerationNames }),
      pathname: pathnameRouterSelector(state),
      queryRouter: queryRouterSelector(state),
      client
    };
  },
  {
    fetchEnumerations,
    push,
    updateClient,
    fetchPayment,
    updatePayment
  }
)(PaymentInfoPage);
