import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  fetchClient,
  updateClient,
  clientInfoSelector,
  clientIdSelector,
  clientChangedSelector,
  moduleName as moduleNameClients,
  requestParamsForClientPageSelector,
  clientIsProviderSelector
} from "../../ducks/clients";
import { moduleName as moduleNameOrders } from "../../ducks/orders";
import { moduleName as moduleNameDiscountGroup } from "../../ducks/discountGroup";
import { moduleName as moduleNameBills } from "../../ducks/bills";
import { moduleName as moduleNameReturns } from "../../ducks/returns";
import { moduleName as moduleNameBalance } from "../../ducks/balances";
import { moduleName as moduleNamePositions } from "../../ducks/positions";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import { moduleName as moduleNamePayments } from "../../ducks/payments";
import { moduleName as moduleNamePurchaseOrders } from "../../ducks/purchaseOrders";
import { moduleName as moduleNamePurchaseReturns } from "../../ducks/purchaseReturns";

import ReactTooltip from "react-tooltip";
import Icon from "../../designSystem/atoms/Icon";
import Container from "../../designSystem/templates/Container";
import Row from "../../designSystem/templates/Row";
import Column from "../../designSystem/templates/Column";
import Grid from "../../designSystem/templates/Grid";
import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Block from "../../designSystem/organisms/Block";
import Box from "../../designSystem/organisms/Box";
import ClientTabs from "../blocks/clients/ClientTabs";
import ClientInfoBlocks from "../blocks/clients/ClientInfoBlocks";
import styled from "styled-components";
import PreLoader from "../common/PreLoader";
import {
  pathnameRouterSelector,
  stateRouterSelector,
  queryRouterSelector
} from "../../redux/selectors/router";
import clientInfoTypes from "../../helpers/propTypes/clientInfo";
import { push } from "connected-react-router";
import HeadingCard from "../common/headlines/HeadingCard";

import HeadingCardModalWrapper from "../blocks/clients/HeadingCardModalWrapper";

export const Header = styled.div`
  padding-bottom: 16px;
  font-size: 24px;
  font-family: "Roboto", sans-serif;
  display: flex;
  align-items: center;
`;

export const HeaderId = styled.span`
  color: #787878;
  margin-left: 10px;
`;

const IconRedByHover = styled(Icon)`
  cursor: pointer;
  &:hover {
    color: red;
  }
`;
const enumerationNames = [
  "personGender",
  "contactType",
  "addressType",
  "documentType",
  "companyType",
  "companySno",
  "agentTag",
  "agentSegment",
  "agentType",
  "returnReason",
  "returnPurpose",
  "orderPositionGoodState",
  "returnState"
];

class ClientInfoPage extends Component {
  static propTypes = {
    ...clientInfoTypes,
    id: PropTypes.string.isRequired,
    client: PropTypes.object,
    fetchClient: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    stateRouter: PropTypes.object.isRequired,
    queryRouter: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    pathname: PropTypes.string.isRequired,
    requestParams: PropTypes.object.isRequired
  };

  state = {
    // provider mode - isProviderMode
    isProviderMode: false
  };

  componentDidMount() {
    if (
      !this.props.client ||
      (this.props.client && !this.props.stateRouter.isLoaded)
    ) {
      this.fetchEntities();
    }
    this.props.fetchEnumerations(enumerationNames);
  }

  fetchEntities = () =>
    this.props.fetchClient(this.props.id, this.props.requestParams);

  handleSubmitEditForm = params => {
    this.props.updateClient(this.props.id, params, this.props.client);
  };

  handleChangeTab = id => {
    const queryForTab = id === 1 ? "" : `?_tab=${id}`;
    this.props.push(`${this.props.pathname}${queryForTab}`);
  };

  handleIconClick = () => {
    if (this.props.isAgentProvider) {
      this.setState(prevState => {
        return {
          isProviderMode: !prevState.isProviderMode
        };
      });
    }
  };

  renderHeadingIcon = () => {
    const { isAgentProvider } = this.props;
    const { isProviderMode } = this.state;
    if (isAgentProvider) {
      return (
        <div>
          <IconRedByHover
            onClick={this.handleIconClick}
            icon={isProviderMode ? "person" : "worker"}
            data-for="iconTooltip"
            data-tip="tooltip"
            data-place="right"
            data-effect="solid"
          />
          <ReactTooltip
            key={isProviderMode ? "person" : "worker"}
            id="iconTooltip"
          >
            {isProviderMode
              ? "Перейти в карточку клиента"
              : "Перейти в карточку поставщика"}
          </ReactTooltip>
        </div>
      );
    }
    return null;
  };

  renderHeadingCardIcon = () => {
    return (
      <HeadingCardModalWrapper
        enumerations={this.props.enumerations}
        client={this.props.client}
        fetchEntities={this.fetchEntities}
      />
    );
  };

  render() {
    const { enumerations, client, clientStateKey, selectedTab } = this.props;
    const { isProviderMode } = this.state;

    if (!this.props.client) {
      return this.props.isLoading ? (
        <PageWrapper>
          <PreLoader />
        </PageWrapper>
      ) : null;
    }

    return (
      <PageWrapper>
        <Container>
          <Row alignCenter justifyBetween>
            <Column>
              <Header>
                {isProviderMode ? "Карточка поставщика" : "Карточка клиента"}
                <HeaderId>id {client.code}</HeaderId>
                {this.renderHeadingIcon()}
              </Header>
            </Column>
            <Column>
              <HeadingCard
                title="Менеджер клиента"
                name={client.manager_name}
                renderIcon={this.renderHeadingCardIcon}
              />
            </Column>
          </Row>
        </Container>
        <Grid filterLeft>
          <Box>
            <ClientInfoBlocks
              key={clientStateKey}
              enumerations={enumerations}
              client={client}
              onSubmitForm={this.handleSubmitEditForm}
            />
          </Box>
          <Block>
            <ClientTabs
              agentId={client.id}
              onChangeTab={this.handleChangeTab}
              selectedTabId={selectedTab || 1}
              isProviderMode={isProviderMode}
            />
          </Block>
        </Grid>
        {this.props.isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

const moduleNames = [
  moduleNameClients,
  moduleNameEnumerations,
  moduleNamePayments,
  moduleNameOrders,
  moduleNameBills,
  moduleNameBalance,
  moduleNameReturns,
  moduleNamePositions,
  moduleNamePurchaseOrders,
  moduleNamePurchaseReturns,
  moduleNameDiscountGroup
];

function mapStateToProps(state, ownProps) {
  const queryRouter = queryRouterSelector(state, ownProps);
  const queryKeyByTab = "_tab";
  const selectedTab = +queryRouter[queryKeyByTab];

  return {
    queryKeyByTab,
    selectedTab,
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    client: clientInfoSelector(state, ownProps.match.params),
    id: clientIdSelector(state, ownProps),
    clientStateKey: clientChangedSelector(state, ownProps.match.params),
    stateRouter: stateRouterSelector(state),
    queryRouter,
    pathname: pathnameRouterSelector(state),
    enumerations: enumerationsSelector(state, { enumerationNames }),
    requestParams: requestParamsForClientPageSelector(state, ownProps),
    // isAgentProvider shows if agent.roles = [1]
    isAgentProvider: clientIsProviderSelector(state, ownProps.match.params)
  };
}

export default connect(
  mapStateToProps,
  { fetchClient, fetchEnumerations, updateClient, push }
)(ClientInfoPage);
