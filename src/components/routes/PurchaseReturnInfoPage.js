import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import {
  putPurchaseReturn,
  fetchPurchaseReturn,
  purchaseReturnInfoSelector,
  moduleName as moduleNamePurchaseReturns,
  requestParamsForListSelector
} from "../../ducks/purchaseReturns";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import { moduleName as billsModuleName } from "../../ducks/bills";
import { moduleName as paymentsModuleName } from "../../ducks/payments";
import { moduleName as tasksModuleName } from "../../ducks/tasks";
import { moduleName as tasksModulePositions } from "../../ducks/positions";
import {
  updateClient,
  fetchClient,
  clientInfoSelector,
  moduleName as moduleNameClient
} from "../../ducks/clients";
import { modulesIsLoadingSelector } from "../../ducks/logger";

import Container from "../../designSystem/templates/Container";
import Grid from "../../designSystem/templates/Grid";
import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Block from "../../designSystem/organisms/Block";
import Box from "../../designSystem/organisms/Box";

import PreLoader from "../common/PreLoader";

import HeaderPurchaseReturnPage from "../blocks/purchaseReturns/HeaderPurchaseReturnPage";
import PurchaseReturnInfoBlocks from "../blocks/purchaseReturns/PurchaseReturnInfoBlocks";
import Tabs from "../blocks/purchaseReturns/Tabs";

import InfoPageFooter from "../common/InfoPageFooter";

const enumerationNames = [
  "returnReason",
  "returnPurpose",
  "purchaseReturnState",
  "productType",
  "orderPositionGoodState",
  "productUnitState",
  "orderState",
  "returnReason",
  "returnState",
  "agentSegment",
  "agentTag",
  "paymentMethod",
  "billState",
  "billType",
  "paymentState",
  "taskState",
  "taskType",
  "taskReason",
  "contactType",
  "personGender"
];

const moduleNames = [
  moduleNamePurchaseReturns,
  moduleNameEnumerations,
  moduleNameClient,
  billsModuleName,
  paymentsModuleName,
  tasksModuleName,
  tasksModulePositions
];

const propTypes = {
  enumerations: PropTypes.object,
  purchaseReturn: PropTypes.object,
  fetchEnumerations: PropTypes.func.isRequired,
  fetchPurchaseReturn: PropTypes.func.isRequired
};

class PurchaseReturnInfoPage extends PureComponent {
  fetchEntities = () => {
    this.props.fetchPurchaseReturn(this.props.entityId, {
      id: this.props.entityId
    });
  };

  componentDidMount = () => {
    this.props.fetchEnumerations(enumerationNames);
    this.fetchEntities();
  };

  componentDidUpdate = prevProps => {
    if (!prevProps.clientId && this.props.clientId) {
      this.props.fetchClient(this.props.clientId, { id: this.props.clientId });
    }
  };

  fetchPurchaseReturn = () => {
    this.props.fetchPurchaseReturn(this.props.entityId, {
      id: this.props.entityId
    });
  };

  handleSubmitClientEditForm = params => {
    this.props.updateClient(this.props.client.id, params, this.props.client);
  };
  handleSubmitEditComment = params => {
    this.props.putPurchaseReturn(
      {
        ...this.props.purchaseReturn.purchase_return,
        ...params
      },
      {
        fetchEntities: this.fetchEntities
      }
    );
  };

  render() {
    if (!this.props.purchaseReturn) {
      return this.props.isLoading ? (
        <PageWrapper>
          <PreLoader />
        </PageWrapper>
      ) : null;
    }

    const { purchaseReturn, enumerations, client } = this.props;

    return (
      <PageWrapper>
        <Container>
          <HeaderPurchaseReturnPage
            enumerations={enumerations}
            purchaseReturn={purchaseReturn}
            fetchPurchaseReturn={this.fetchPurchaseReturn}
            memoizeToArray={this.memoizeToArray}
          />
        </Container>
        <Grid filterLeft>
          <Box>
            <PurchaseReturnInfoBlocks
              purchaseReturn={purchaseReturn}
              enumerations={enumerations}
              client={client}
              onSubmitClientForm={this.handleSubmitClientEditForm}
            />
          </Box>
          <Box>
            <Block mb="16px">
              <Tabs
                purchaseReturn={purchaseReturn}
                enumerations={enumerations}
              />
            </Block>
            <InfoPageFooter
              client={client}
              entity={purchaseReturn.purchase_return}
              onSubmitComment={this.handleSubmitEditComment}
            />
          </Box>
        </Grid>
        {!!this.props.isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

PurchaseReturnInfoPage.propTypes = propTypes;

export default connect(
  (state, props) => {
    const entityId = props.match.params.id;
    const purchaseReturn = purchaseReturnInfoSelector(state, { id: entityId });
    const clientId =
      purchaseReturn && purchaseReturn.purchase_return
        ? purchaseReturn.purchase_return.supplier_id
        : null;
    return {
      entityId,
      clientId,
      enumerations: enumerationsSelector(state, { enumerationNames }),
      purchaseReturn,
      isLoading: modulesIsLoadingSelector(state, { moduleNames }),
      requestParams: requestParamsForListSelector(state, props),
      client: clientInfoSelector(state, { idEntity: entityId, id: clientId })
    };
  },
  {
    fetchEnumerations,
    fetchPurchaseReturn,
    updateClient,
    fetchClient,
    putPurchaseReturn
  }
)(PurchaseReturnInfoPage);
