import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import CreateClientForm from "../forms/CreateClientForm";
import Container from "../../designSystem/templates/Container";
import Row from "../../designSystem/templates/Row";
import Column from "../../designSystem/templates/Column";
import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Block from "../../designSystem/organisms/Block";
import BlockContent from "../../designSystem/organisms/BlockContent";
import Heading from "../../designSystem/molecules/Heading";
import {
  createClient,
  moduleName as moduleNameClients
} from "../../ducks/clients";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import PreLoader from "../common/PreLoader";

class CreateClientPage extends Component {
  static propTypes = {
    createClient: PropTypes.func.isRequired
  };

  render() {
    return (
      <PageWrapper>
        <Container>
          <Row>
            <Column basis="500px" mr="middle">
              <Block>
                <Heading title="Создание клиента" />
                <BlockContent>
                  <CreateClientForm onSubmitForm={this.props.createClient} />
                </BlockContent>
              </Block>
            </Column>
          </Row>
        </Container>
        {this.props.isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

const moduleNames = [moduleNameClients];

function mapStateToProps(state) {
  return {
    isLoading: modulesIsLoadingSelector(state, { moduleNames })
  };
}
export default connect(
  mapStateToProps,
  { createClient }
)(CreateClientPage);
