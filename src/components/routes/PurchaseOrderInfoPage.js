import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import { connect } from "react-redux";
import { push } from "connected-react-router";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import {
  fetchPurchaseOrder,
  requestParamsOrderPageSelector,
  purchaseOrderInfoSelector,
  moduleName as moduleNamePurchaseOrders,
  updatePurchaseOrder,
  mapPurchaseOrderUpdateComment
} from "../../ducks/purchaseOrders";
import {
  fetchClient,
  updateClient,
  clientInfoSelector,
  moduleName as moduleNameClient
} from "../../ducks/clients";
import { moduleName as moduleNameBills } from "../../ducks/bills";
import { moduleName as moduleNamePurchaseReturns } from "../../ducks/purchaseReturns";
import { moduleName as moduleNamePayments } from "../../ducks/payments";
import { moduleName as moduleNameTasks } from "../../ducks/tasks";
import { moduleName as moduleNamePositions } from "../../ducks/positions";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import {
  pathnameRouterSelector,
  keyRouterSelector,
  entityIdForRouteSelector
} from "../../redux/selectors/router";

import PageWrapper from "../../designSystem/molecules/PageWrapper";
import Container from "../../designSystem/templates/Container";
import Grid from "../../designSystem/templates/Grid";
import PreLoader from "../common/PreLoader";
import Block from "../../designSystem/organisms/Block";
import Box from "../../designSystem/organisms/Box";
import BlockContent from "../../designSystem/organisms/BlockContent";

import PurchaseOrderInfoBlocks from "../blocks/purchaseOrders/PurchaseOrderInfoBlocks";
import HeaderPurchaseOrder from "../blocks/purchaseOrders/HeaderPurchaseOrder";
import PurchaseOrderTabs from "../blocks/purchaseOrders/Tabs";

import InfoPageFooter from "../common/InfoPageFooter";

const enumerationNames = [
  "agentType",
  "orderSupplierState",
  "pricelistDeliveryType",
  "orderPositionGoodState",
  "productUnitState",
  "taskState",
  "taskType",
  "taskReason",
  "orderState",
  "agentTag",
  "agentSegment",
  "contactType",
  "paymentMethod",
  "paymentState",
  "billState",
  "billType",
  "personGender"
];

const moduleNames = [
  moduleNameEnumerations,
  moduleNamePurchaseOrders,
  moduleNameClient,
  moduleNameBills,
  moduleNamePayments,
  moduleNameTasks,
  moduleNamePositions,
  moduleNamePurchaseReturns
];

class PurchaseOrderInfoPage extends PureComponent {
  static propTypes = {
    purchaseOrderId: PropTypes.string,
    purchaseOrder: PropTypes.object,
    clientId: PropTypes.string,
    enumerations: PropTypes.object,
    keyRouter: PropTypes.string,
    isLoading: PropTypes.bool,
    pathname: PropTypes.string,
    requestParams: PropTypes.object,
    client: PropTypes.object,

    updateClient: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    fetchPurchaseOrder: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    fetchClient: PropTypes.func.isRequired,
    updatePurchaseOrder: PropTypes.func.isRequired
  };

  fetchEntities = () => {
    const { fetchPurchaseOrder, purchaseOrderId } = this.props;
    fetchPurchaseOrder(purchaseOrderId, { id: purchaseOrderId });
  };

  componentDidMount = () => {
    const { fetchEnumerations } = this.props;

    fetchEnumerations(enumerationNames);
    this.fetchEntities();
  };

  componentDidUpdate = prevProps => {
    if (!prevProps.clientId && this.props.clientId) {
      this.props.fetchClient(this.props.clientId, { id: this.props.clientId });
    }
  };

  handleSubmitClientEditForm = params => {
    const { updateClient, client } = this.props;
    updateClient(client.id, params, client);
  };

  handleSubmitEditComment = params => {
    this.props.updatePurchaseOrder(
      params,
      {
        purchaseOrder: this.props.purchaseOrder.order,
        successCallback: this.fetchEntities
      },
      mapPurchaseOrderUpdateComment
    );
  };

  render() {
    const {
      purchaseOrder,
      isLoading,
      enumerations,
      push,
      client,
      requestParams,
      pathname,
      keyRouter
    } = this.props;

    if (!this.props.purchaseOrder) {
      return this.props.isLoading ? (
        <PageWrapper>
          <PreLoader />
        </PageWrapper>
      ) : null;
    }

    return (
      <PageWrapper>
        <Container>
          <HeaderPurchaseOrder
            purchaseOrder={purchaseOrder}
            enumerations={enumerations}
            fetchEntities={this.fetchEntities}
          />
        </Container>
        <Grid filterLeft>
          <Box>
            <PurchaseOrderInfoBlocks
              purchaseOrder={purchaseOrder}
              enumerations={enumerations}
              client={client}
              onSubmitClientForm={this.handleSubmitClientEditForm}
            />
          </Box>
          <Box>
            <Block mb="16px">
              <BlockContent>
                <PurchaseOrderTabs
                  purchaseOrder={purchaseOrder}
                  enumerations={enumerations}
                  push={push}
                  client={client}
                  requestParams={requestParams}
                  pathname={pathname}
                  keyRouter={keyRouter}
                />
              </BlockContent>
            </Block>
            <InfoPageFooter
              client={client}
              entity={this.props.purchaseOrder.order}
              onSubmitComment={this.handleSubmitEditComment}
            />
          </Box>
        </Grid>
        {isLoading && <PreLoader />}
      </PageWrapper>
    );
  }
}

export default connect(
  (state, props) => {
    const purchaseOrderId = entityIdForRouteSelector(state, props);
    const purchaseOrder = purchaseOrderInfoSelector(state, {
      id: purchaseOrderId
    });
    const clientId =
      purchaseOrder && purchaseOrder.order
        ? purchaseOrder.order.agent_id
        : null;
    return {
      purchaseOrderId,
      purchaseOrder,
      clientId,
      enumerations: enumerationsSelector(state, { enumerationNames }),
      keyRouter: keyRouterSelector(state),
      isLoading: modulesIsLoadingSelector(state, { moduleNames }),
      pathname: pathnameRouterSelector(state),
      requestParams: requestParamsOrderPageSelector(state),
      client: clientInfoSelector(state, {
        idEntity: purchaseOrderId,
        id: clientId
      })
    };
  },
  {
    fetchEnumerations,
    fetchPurchaseOrder,
    push,
    updateClient,
    fetchClient,
    updatePurchaseOrder
  }
)(PurchaseOrderInfoPage);
