import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import memoizeOne from "memoize-one";

import PageWrapper from "../../designSystem/molecules/PageWrapper";
import WideButton from "../../designSystem/molecules/WideButton";
import Container from "../../designSystem/templates/Container";
import Heading from "../../designSystem/molecules/Heading";
import Block from "../../designSystem/organisms/Block";
import Box from "../../designSystem/organisms/Box";
import Row from "../../designSystem/templates/Row";
import Grid from "../../designSystem/templates/Grid";

import HeaderStockPage from "../blocks/stock/HeaderStockPage";
import StockFilter from "../blocks/stock/StockFilter";
import StockTable from "../blocks/stock/StockTable";
import Pagination from "../blocks/Pagination";

import EmptyResult from "../common/EmptyResult";
import PreLoader from "../common/PreLoader";

import { ContainerContext, FormContext } from "../common/formControls";
import { pathnameRouterSelector } from "../../redux/selectors/router";

import { employeeInfoByUserSelector } from "../../ducks/user";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../ducks/enumerations";
import {
  requestParamsInStockSelector,
  fetchProductsInStockList,
  productsInStockListSelector,
  maxPagesSelector,
  defaultValuesForFieldsSelector,
  moduleName as moduleProductsInStock
} from "../../ducks/productsInStock";

import {
  getStateByDefaultValuesForFields,
  getStateByRequestInfo
} from "../../helpers/component/staticMethods";
import {
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields
} from "../../helpers/component/handlers";

import {
  fieldsForStockTable,
  fieldsForStockFilter
} from "../../fields/formFields/stock";

import { mapFormFieldsToQuery } from "../../fields/helpers";
import { generateUrlWithQuery } from "../../utils";
import { getUrl } from "../../helpers/urls";

const enumerationNames = [
  "agentType",
  "orderSupplierState",
  "pricelistDeliveryType",
  "orderPositionGoodState",
  "productUnitState",
  "productType"
];

class StockPage extends Component {
  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
    productsInStock: PropTypes.arrayOf(PropTypes.object).isRequired,
    valuesForFields: PropTypes.object.isRequired,
    defaultValuesForFields: PropTypes.object.isRequired,
    requestParams: PropTypes.object.isRequired,
    fieldsForTable: PropTypes.arrayOf(PropTypes.object).isRequired,
    fieldsForFilters: PropTypes.arrayOf(PropTypes.object).isRequired,
    employeeInfo: PropTypes.object,
    push: PropTypes.func.isRequired,
    pathname: PropTypes.string.isRequired,
    enumerations: PropTypes.object.isRequired,
    maxPages: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      valuesForFields: null,
      prevValuesForFields: null,
      pagingSubEntity: null
    };

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.rowBodyHandlers = {
      onClick: this.handleMoveToEntityPage
    };
  }

  static getDerivedStateFromProps(props, state) {
    let newState = null;

    newState = getStateByRequestInfo(props, state, newState);
    newState = getStateByDefaultValuesForFields(props, state, newState);

    return newState;
  }

  componentDidMount() {
    this.callFetchProductsInStockList();
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.requestParams !== prevProps.requestParams) {
      this.callFetchProductsInStockList();
    }
  }

  callFetchProductsInStockList = () =>
    this.props.fetchProductsInStockList(this.state.valuesForFields);

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    debugger;
    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleMoveToPage = pageNumber =>
    this.props.push(
      generateUrlWithQuery({
        pathname: this.props.pathname,
        oldQuery: this.state.valuesForFields,
        newQuery: {
          page_product: pageNumber,
          page_product_unit: 1
        }
      })
    );

  handleSubPageOpen = ({ pagingSubEntity, pageProductUnit }) => {
    this.setState({ pagingSubEntity });

    return this.props.push(
      generateUrlWithQuery({
        pathname: this.props.pathname,
        oldQuery: this.state.valuesForFields,
        newQuery: { page_product_unit: pageProductUnit }
      })
    );
  };

  handleOpen = openedId => {
    const { requestParams, productsInStock } = this.props;
    const pageProductUnit = +requestParams.page_product_unit;

    if (
      !pageProductUnit ||
      pageProductUnit === 1 ||
      !this.state.pagingSubEntity
    ) {
      return;
    }

    this.handleSubPageOpen({
      pagingSubEntity: productsInStock.find(p => p._id === openedId),
      pageProductUnit: 1
    });
  };

  renderFooter = productsInStock => {
    const { isLoading, pathname, requestParams, maxPages } = this.props;

    return (
      <>
        {isLoading ? <PreLoader /> : !productsInStock.length && <EmptyResult />}
        <Pagination
          pathname={pathname}
          moveToPage={this.handleMoveToPage}
          requestParams={this.state.valuesForFields}
          page={+requestParams.page_product}
          maxPages={maxPages || 1}
        />
      </>
    );
  };

  render() {
    const { employeeInfo, productsInStock, enumerations } = this.props;

    const {
      fieldsForTable,
      fieldsForFilters,
      allFields,
      key
    } = this.mapValuesToFields(enumerations, productsInStock);

    return (
      <PageWrapper>
        <Container>
          <HeaderStockPage employeeInfo={employeeInfo} />
        </Container>
        <ContainerContext
          key={key}
          fields={allFields}
          onSubmitForm={this.handleSubmitForm}
          autoSubmit
          submitValidation
          externalForm
        >
          <Grid filterRight mt="16px">
            <Block>
              <Heading title="Список товаров" />
              <FormContext>
                <StockTable
                  productsInStock={productsInStock}
                  pagingSubEntity={this.state.pagingSubEntity}
                  handleOpen={this.handleOpen}
                  handleSubPageOpen={this.handleSubPageOpen}
                  enumerations={enumerations}
                  fields={fieldsForTable}
                  footerElement={this.renderFooter(productsInStock)}
                />
              </FormContext>
            </Block>
            <FormContext>
              <Row mb="8px">
                <StockFilter fields={fieldsForFilters} />
              </Row>
            </FormContext>
            <Box mt="16px">
              <Container>
                <Row>
                  <WideButton
                    icon="store_mall_directory"
                    type="spa-link"
                    href={getUrl("warehouse")}
                  >
                    Задачи для склада
                  </WideButton>
                </Row>
              </Container>
            </Box>
          </Grid>
        </ContainerContext>
      </PageWrapper>
    );
  }
}

const moduleNames = [moduleNameEnumerations, moduleProductsInStock];

function mapStateToProps(state, props) {
  const requestParams = requestParamsInStockSelector(state, props);

  return {
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    productsInStock: productsInStockListSelector(state, props).entities,

    valuesForFields: requestParams,
    defaultValuesForFields: defaultValuesForFieldsSelector(state, props),
    requestParams,
    fieldsForTable: fieldsForStockTable,
    fieldsForFilters: fieldsForStockFilter,

    employeeInfo: employeeInfoByUserSelector(state, props),
    pathname: pathnameRouterSelector(state, props),
    enumerations: enumerationsSelector(state, { enumerationNames }),
    maxPages: maxPagesSelector(state, props)
  };
}

export default connect(
  mapStateToProps,
  {
    fetchProductsInStockList,
    fetchEnumerations,
    push
  }
)(StockPage);
