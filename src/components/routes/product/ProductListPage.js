import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  fetchProductsByArticle,
  moduleName,
  productsSelector,
  requestParamsForProductListPageSelector,
  addToBasket
} from "../../../ducks/products";
import { modulesIsLoadingSelector } from "../../../ducks/logger";
import {
  queryRouterSelector,
  keyRouterSelector,
  pathnameRouterSelector
} from "../../../redux/selectors/router";
import PreLoader from "../../common/PreLoader";
import Container from "../../../designSystem/templates/Container";
import Row from "../../../designSystem/templates/Row";
import Column from "../../../designSystem/templates/Column";
import Block from "../../../designSystem/organisms/Block";
import memoizeOne from "memoize-one";
import ProductList from "../../blocks/products/ProductList";
import ProductFilters from "../../blocks/products/ProductFilters";
import { push } from "connected-react-router";
import { generateId, objectToArray } from "../../../utils";
import {
  basketAttachedInfoSelector,
  pageFiltersSelector
} from "../../../ducks/search";

class ProductListPage extends Component {
  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
    fetchProductsByArticle: PropTypes.func.isRequired,
    query: PropTypes.object.isRequired,
    products: PropTypes.object.isRequired,
    requestParams: PropTypes.object.isRequired,
    keyRouter: PropTypes.string.isRequired,
    push: PropTypes.func.isRequired,
    pathname: PropTypes.string.isRequired,
    addToBasket: PropTypes.func.isRequired,
    pageFilters: PropTypes.object.isRequired,
    basketAttached: PropTypes.object
  };

  requestId = generateId();

  componentDidMount() {
    this.props.fetchProductsByArticle(
      this.requestId,
      this.props.query,
      this.props.requestParams
    );
  }

  componentDidUpdate(prevProps) {
    const { keyRouter, pageFilters, requestParams } = this.props;

    if (
      prevProps.pageFilters.cityId !== pageFilters.cityId ||
      prevProps.pageFilters.discountGroupId !== pageFilters.discountGroupId ||
      keyRouter !== prevProps.keyRouter
    ) {
      this.props.fetchProductsByArticle(
        this.requestId,
        this.props.query,
        requestParams
      );
    }
  }

  getKeyStateProduct = memoizeOne(() => generateId());

  getFilters = memoizeOne(filters => {
    const filterList = objectToArray(filters);
    const selectedFilters = filterList.filter(
      entity => entity.selectedItems.length
    );
    return {
      filterList,
      selectedFilters,
      filtersIsSelected: !!selectedFilters.length
    };
  });

  handleAddOfferToBasket = item => {
    this.props.addToBasket([item]);
  };

  renderFilters() {
    const { products, pathname, query, isLoading } = this.props;

    if (!products[this.requestId]) return null;

    const { filterList, selectedFilters } = this.getFilters(
      products[this.requestId].filters
    );

    return (
      <ProductFilters
        push={this.props.push}
        filters={filterList}
        selectedFilters={selectedFilters}
        pathname={pathname}
        query={query}
        isLoading={isLoading}
        filterKeyList={products[this.requestId].filters}
      />
    );
  }

  render() {
    const { products, isLoading, basketAttached } = this.props;
    const productsInfo = products[this.requestId];
    const keyStateProduct = this.getKeyStateProduct(productsInfo);

    return (
      <Container>
        {isLoading && <PreLoader />}
        <Row mt="20px">
          <Column basis="1200px" grow="2" mr="middle">
            <Block>
              {!!productsInfo && (
                <>
                  {!!productsInfo.searchProduct.length && (
                    <ProductList
                      key={`1_${keyStateProduct}`}
                      goodsInBasket={{}}
                      title="Запрошенный артикул"
                      showSubTitle
                      products={productsInfo.searchProduct}
                      addOfferToBasket={this.handleAddOfferToBasket}
                      brandLevels={productsInfo.brandLevels}
                      basketAttached={basketAttached}
                    />
                  )}
                  {!!productsInfo.originalProducts.length && (
                    <ProductList
                      key={`2_${keyStateProduct}`}
                      goodsInBasket={{}}
                      title="Оригинальный артикул"
                      isOriginal
                      products={productsInfo.originalProducts}
                      addOfferToBasket={this.handleAddOfferToBasket}
                      brandLevels={productsInfo.brandLevels}
                      basketAttached={basketAttached}
                    />
                  )}
                  {!!productsInfo.analogProducts.length && (
                    <ProductList
                      key={`3_${keyStateProduct}`}
                      goodsInBasket={{}}
                      title="Аналоги"
                      products={productsInfo.analogProducts}
                      addOfferToBasket={this.handleAddOfferToBasket}
                      brandLevels={productsInfo.brandLevels}
                      basketAttached={basketAttached}
                    />
                  )}
                </>
              )}
            </Block>
          </Column>
          <Column basis="350px" maxWidth="350px">
            <Block mb="middle">{this.renderFilters()}</Block>
          </Column>
        </Row>
      </Container>
    );
  }
}

const moduleNames = [moduleName];

function mapStateToProps(state, props) {
  return {
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    query: queryRouterSelector(state, props),
    products: productsSelector(state, props),
    requestParams: requestParamsForProductListPageSelector(state, props),
    keyRouter: keyRouterSelector(state, props),
    pathname: pathnameRouterSelector(state, props),
    pageFilters: pageFiltersSelector(state, props),
    basketAttached: basketAttachedInfoSelector(state, props)
  };
}
export default connect(
  mapStateToProps,
  { fetchProductsByArticle, push, addToBasket }
)(ProductListPage);
