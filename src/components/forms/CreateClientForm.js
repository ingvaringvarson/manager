import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../common/tabControls";
import Button from "../../designSystem/atoms/Button";
import BlockContent from "../../designSystem/organisms/BlockContent";
import ContainerContext, { FieldContext } from "../common/formControls";
import BtnBlock from "../common/styled/BtnBlock";
import { fields } from "../../fields/formFields/clients/fieldsForCreateForm";
import { reduceValuesToFormFields } from "./../../fields/helpers/mapMethods";
import { hasPermissionSelector } from "../../ducks/user";
import { pm_registration_supplier } from "../../constants/permissions";

const fieldStyle = {
  mb: "small"
};

const firstFieldStyle = {
  mb: "small",
  mt: "middle"
};

class CreateClientForm extends PureComponent {
  static propTypes = {
    onSubmitForm: PropTypes.func.isRequired,
    createSupplierAvailable: PropTypes.bool.isRequired
  };

  state = { type: 1 };

  handleChangeTab = id => this.setState({ type: id });

  renderCells = type => {
    const { createSupplierAvailable } = this.props;
    const cellsFields = fields[type].reduce(
      reduceValuesToFormFields(createSupplierAvailable),
      []
    );
    return cellsFields.map(this.renderField);
  };

  renderField = (field, index) =>
    field.isActive && (
      <FieldContext
        key={`1_${field.id}`}
        fieldStyle={index ? fieldStyle : firstFieldStyle}
        name={field.id}
      />
    );

  render() {
    return (
      <BlockContent>
        <ContainerContext
          key={this.state.type}
          fields={fields[this.state.type]}
          onSubmitForm={this.props.onSubmitForm}
          submitValidation
        >
          <TabContainer
            onChangeSelected={this.handleChangeTab}
            selectedTabId={this.state.type}
          >
            <TabHeadContainer>
              <TabHead id={1}>Физическое лицо</TabHead>
              <TabHead id={2}>Юридическое лицо</TabHead>
            </TabHeadContainer>
            <TabBodyContainer>
              <TabBody id={1}>{this.renderCells(1)}</TabBody>
              <TabBody id={2}>{this.renderCells(2)}</TabBody>
            </TabBodyContainer>
          </TabContainer>
          <BtnBlock>
            <Button type="submit">Создать</Button>
          </BtnBlock>
        </ContainerContext>
      </BlockContent>
    );
  }
}

function mapStateToProps(state) {
  return {
    createSupplierAvailable: hasPermissionSelector(state, {
      name: pm_registration_supplier
    })
  };
}

export default connect(mapStateToProps)(CreateClientForm);
