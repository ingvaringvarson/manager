import React, { PureComponent } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

import Input from "../../designSystem/atoms/Input";
import Row from "../../designSystem/templates/Row";
import Column from "../../designSystem/templates/Column";
import ButtonMixin from "../../designSystem/mixins/Button";
import Icon from "../../designSystem/atoms/Icon";

const CommentInput = styled(Input)`
  background-color: #e5e5e5;
  height: 56px;
`;

const SubmitButton = styled.button`
  ${ButtonMixin};
  display: flex;
  align-items: center;
  width: 100%;
  height: 56px;
  text-transform: none;
`;

const ButtonIcon = styled(Icon)`
  margin-right: 6px;
  width: 36px;
`;

const StyledForm = styled.form`
  width: 100%;
`;

class EditComment extends PureComponent {
  static propTypes = {
    onSubmitForm: PropTypes.func.isRequired,
    comment: PropTypes.string
  };

  state = {
    comment: this.props.comment
  };

  handleChangeComment = event => {
    event.preventDefault();
    this.setState({
      comment: event.target.value
    });
  };

  handleSubmitForm = event => {
    event.preventDefault();
    const { comment } = this.state;

    this.props.onSubmitForm({ comment });
  };

  render() {
    const { comment } = this.state;

    return (
      <StyledForm onSubmit={this.handleSubmitForm}>
        <Row m="0px">
          <Column basis="800px" mr="30px" grow="2">
            <CommentInput
              onChange={this.handleChangeComment}
              fullWidth
              value={comment}
              type="textarea"
              placeholder="Введите комментарий"
            />
          </Column>
          <Column basis="180px" maxWidth="180px">
            <SubmitButton type="submit" white>
              <ButtonIcon icon="done" />
              Сохранить комментарий
            </SubmitButton>
          </Column>
        </Row>
      </StyledForm>
    );
  }
}

export default EditComment;
