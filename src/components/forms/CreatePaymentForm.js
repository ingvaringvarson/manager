import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import styled from "styled-components";
import ReactTooltip from "react-tooltip";
import memoizeOne from "memoize-one";

import Title from "../../designSystem/atoms/Title";
import Text from "../../designSystem/atoms/Text";
import Icon from "../../designSystem/atoms/Icon";
import Box from "../../designSystem/organisms/Box";
import BlockContent from "../../designSystem/organisms/BlockContent";
import FieldForm from "../common/form/FieldForm";
import {
  fieldsForCreatePaymentForm as fieldsForForm,
  balanceField,
  billIdField_v2 as billIdField,
  paymentMethodField_1,
  paymentMethodField_2,
  paymentTypeField_1,
  paymentTypeField_2,
  paymentTypeField_3,
  paymentTypeField_4,
  receiptField,
  sumField,
  receiptAddressField
} from "../../fields/formFields/payments/returnPayment";
import { ContainerContext } from "../common/formControls";
import {
  fetchBillsForEntity,
  billsForEntitySelector,
  moduleName as billsModuleName
} from "../../ducks/bills";
import {
  fetchClient,
  clientInfoSelector,
  moduleName as clientsModuleName
} from "../../ducks/clients";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import { reduceValuesToFormFields } from "../../fields/helpers";
import { generateId, getValueInDepth } from "../../utils";
import { createPaymentCommand, commandsLoaderName } from "../../ducks/payments";
import Button from "../../designSystem/atoms/Button";
import BtnBlock from "../common/styled/BtnBlock";
import PreLoader from "../common/PreLoader";
import { hasPermissionSelector } from "../../ducks/user";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../ducks/enumerations";
import {
  pm_payments_create_return_internet_acquiring,
  pm_payments_create_without_check
} from "../../constants/permissions";
import { getUrl } from "../../helpers/urls";

const StyledIcon = styled(Icon)`
  justify-self: center;
  width: 24px;
  margin-top: 8px;
  color: #000;
  cursor: pointer;
`;

const BlockContentForm = styled(BlockContent)`
  position: static;
  max-height: 650px;
  width: 460px;
  overflow-y: auto;
`;

const PreloaderContainer = styled.div`
  position: relative;
`;

const ToolTipContainer = styled.div`
  width: 300px;
  padding: 5px 0;
`;

const typeEntity = "returns";

const enumerationNames = ["billType", "billState"];

class CreatePaymentForm extends Component {
  static propTypes = {
    order: PropTypes.object.isRequired,
    bill: PropTypes.object, // когда будем использовать?
    afterSubmitCallback: PropTypes.func.isRequired,

    internetAcquiringIsAvailable: PropTypes.bool.isRequired,
    withoutCheckIsAvailable: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    bills: PropTypes.shape({
      entities: PropTypes.array.isRequired
    }).isRequired,
    client: PropTypes.object.isRequired,
    enumerations: PropTypes.object.isRequired,
    fetchBillsForEntity: PropTypes.func.isRequired,
    createPaymentCommand: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    fetchClient: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
    const { id, agent_id } = this.props.order;
    this.props.fetchBillsForEntity(typeEntity, id, {
      order_id: id,
      state: [1, 2, 3]
    });

    if (!this.props.client) {
      this.props.fetchClient(agent_id, { id: agent_id });
    }
  }

  getSelectedBill = memoizeOne((id, bills) => {
    return bills.entities.find(e => e.id === id);
  });

  getBillInfo = memoizeOne((id, bills) => {
    const selectedBill = this.getSelectedBill(id, bills);

    const info = {
      sum_prepaid: null,
      sum: null,
      prepaid: null
    };

    if (selectedBill) {
      if (selectedBill.type === 4) info.sum_prepaid = selectedBill.fact_sum;
      if (selectedBill.type !== 4) info.sum = selectedBill.fact_sum;
      info.prepaid = selectedBill.fact_sum - selectedBill.receipt_sum;
    } else {
      bills.entities.forEach(b => {
        if (b.type === 4) info.sum_prepaid += b.fact_sum;
        if (b.type === 1) info.sum += b.fact_sum;
        info.prepaid += b.fact_sum - b.receipt_sum;
      });
    }

    return info;
  });

  renderBillInfo = values => {
    const { bills, enumerations } = this.props;
    const billId = getValueInDepth(values, ["bill_id", 0]);
    const info = this.getSelectedBill(billId, bills);

    if (!info) return "Нет информации по счёту.";

    const billType = getValueInDepth(enumerations, [
      "billType",
      "map",
      info.type,
      "name"
    ]);
    const billState = getValueInDepth(enumerations, [
      "billState",
      "map",
      info.state,
      "name"
    ]);

    return (
      <ToolTipContainer>
        {!!billType && (
          <Text mb="10px" fontSize="12px">
            Тип счета: {billType}
          </Text>
        )}
        {!!billState && (
          <Text mb="10px" fontSize="12px">
            Статус счета: {billState}
          </Text>
        )}
        {!!(info.positions && info.positions.length) && (
          <>
            <Text mb="5px" fontSize="12px">
              Позиции в счете:
            </Text>
            {info.positions.map(p => {
              let name = "";

              if (p.product) {
                if (p.product.article)
                  name = name
                    ? `${name} ${p.product.article}`
                    : `${p.product.article}`;
                if (p.product.brand)
                  name = name
                    ? `${name} ${p.product.brand}`
                    : `${p.product.brand}`;
                if (p.product.name)
                  name = name
                    ? `${name} ${p.product.name}`
                    : `${p.product.name}`;
              }

              return name ? (
                <Text mb="5px" pl="5px" fontSize="10px">
                  {" "}
                  - {name}
                </Text>
              ) : (
                name
              );
            })}
          </>
        )}
      </ToolTipContainer>
    );
  };

  renderForm = ({ values, fields, invalidFields }) => {
    const { bills } = this.props;
    const billId = getValueInDepth(values, ["bill_id", 0]);
    const billInfo = this.getBillInfo(billId, bills);

    return (
      <>
        <Box
          display="grid"
          gridTemplateColumns="1fr 24px 24px"
          gridColumnGap="8px"
          pb="8px"
          borderBottom="1px solid"
          borderColor="grayscale.1"
        >
          <FieldForm field={fields[billIdField.id]} />
          {!!billId && (
            <>
              <StyledIcon
                icon="error_outline"
                data-tip="tooltip"
                data-for={`bill_info_${billId}`}
                data-place="right"
                data-effect="solid"
              />
              <Link to={getUrl("bills", { id: billId })} target="_blank">
                <StyledIcon icon="edit" />
              </Link>
            </>
          )}
        </Box>
        <ReactTooltip id={`bill_info_${billId}`}>
          {this.renderBillInfo(values)}
        </ReactTooltip>
        <Box
          display="grid"
          gridRowGap="8px"
          py={16}
          borderBottom="1px solid"
          borderColor="grayscale.1"
        >
          <Box display="grid" gridTemplateColumns="1fr 1fr" alignItems="center">
            <FieldForm field={fields[paymentTypeField_2.id]} />
            <FieldForm field={fields[paymentTypeField_3.id]} />
          </Box>
          <Box display="grid" gridTemplateColumns="1fr 1fr" alignItems="center">
            <FieldForm field={fields[paymentTypeField_1.id]} />
            <FieldForm field={fields[paymentTypeField_4.id]} />
          </Box>
          {!!invalidFields.payment_type && (
            <Text fontSize="10px" color="#eb0028">
              Не выбран тип платежа
            </Text>
          )}
          <Box display="grid" gridTemplateColumns="35% 65%" alignItems="center">
            <Text>Отправить электронный чек на</Text>
            <Box
              display="grid"
              gridTemplateColumns="1fr 24px"
              alignItems="center"
              gridColumnGap="8px"
            >
              <FieldForm field={fields[receiptAddressField.id]} />
            </Box>
          </Box>
        </Box>
        {billInfo.sum_prepaid !== null && (
          <Box
            display="grid"
            gridRowGap="10px"
            py={16}
            borderBottom="1px solid"
            borderColor="grayscale.1"
          >
            <Box
              display="grid"
              gridTemplateColumns="1fr 1fr"
              alignItems="center"
            >
              <Text>Сумма предоплаты</Text>
              <Text>
                <Box display="inline-block" mr="5px" color="primary.0">
                  {billInfo.sum_prepaid}
                </Box>
                руб.
              </Text>
            </Box>
          </Box>
        )}
        {billInfo.sum !== null && (
          <Box
            display="grid"
            gridRowGap="10px"
            py={16}
            borderBottom="1px solid"
            borderColor="grayscale.1"
          >
            <Box
              display="grid"
              gridTemplateColumns="1fr 1fr"
              alignItems="center"
            >
              <Text>Сумма</Text>
              <Text>
                <Box display="inline-block" mr="5px" color="primary.0">
                  {billInfo.sum}
                </Box>
                руб.
              </Text>
            </Box>
          </Box>
        )}
        {billInfo.prepaid !== null && (
          <Box
            display="grid"
            gridRowGap="10px"
            py={16}
            borderBottom="1px solid"
            borderColor="grayscale.1"
          >
            <Box
              display="grid"
              gridTemplateColumns="1fr 1fr"
              alignItems="center"
            >
              <Text>Предоплачено</Text>
              <Text>
                <Box display="inline-block" mr="5px" color="primary.0">
                  {billInfo.prepaid}
                </Box>
                руб.
              </Text>
            </Box>
          </Box>
        )}
        <Box
          py="16px"
          borderBottom="1px solid"
          borderColor="grayscale.1"
          textAlign="center"
        >
          <Title>
            К оплате ИТОГО{" "}
            <Box display="inline-block" color="primary.0">
              {values.sum}
            </Box>{" "}
            руб.
          </Title>
        </Box>
        <Box display="grid" gridRowGap="10px" pt={16} pb={0}>
          <Box display="grid" gridTemplateColumns="35% 65%" alignItems="center">
            <Text>Сумма оплаты</Text>
            <FieldForm field={fields[sumField.id]} />
          </Box>
          <Box display="grid" gridTemplateColumns="35% 65%" alignItems="center">
            <Text>Сдача с</Text>
            <Box
              display="grid"
              gridTemplateColumns="1fr 1fr"
              gridColumnGap="8px"
            >
              <FieldForm field={fields[receiptField.id]} />
              <FieldForm field={fields[balanceField.id]} />
            </Box>
          </Box>
        </Box>
        <Box
          display="grid"
          gridTemplateColumns="1fr 1fr"
          gridColumnGap="16px"
          py={16}
          mb="16px"
        >
          <FieldForm field={fields[paymentMethodField_1.id]} />
          <FieldForm field={fields[paymentMethodField_2.id]} />
          {!!invalidFields.payment_method && (
            <Text fontSize="10px" color="#eb0028" mt="15px">
              Не выбран способ оплаты
            </Text>
          )}
        </Box>
        <BtnBlock>
          <Button type="submit">Создать</Button>
        </BtnBlock>
      </>
    );
  };

  mapValuesToFields = memoizeOne((bill, order, client, bills) => {
    const enumerations = {};
    const values = {
      bill,
      order,
      client,
      bill_id: [],
      sum: 0,
      receipt: 0,
      balance: 0,
      payment_state: 1,
      payment_place: 2
    };

    if (order) {
      enumerations.bills = {
        list: bills.entities.map(entity => ({
          id: entity.id,
          name: entity.code,
          entity
        }))
      };

      values.agent_id = order.agent_id;
      values.order_id = order.id;
      values.contract_id = order.contract_id;
      values.sum = order.sum_payment;
      values.receipt = order.sum_payment;
      values.firm_id = order.firm_id;
      values.firm_name = order.firm_name;
    } else if (bill) {
      enumerations.bills = {
        list: [
          {
            id: bill.id,
            name: bill.code,
            entity: bill
          }
        ]
      };
      values.agent_id = bill.agent_id;
      values.order_id = getValueInDepth(bill, ["return_id", 0]);
      values.contract_id = bill.contract_id;
      values.bill_id = [bill.id];
      values.sum = bill.bill_sum;
      values.receipt = bill.bill_sum;
    }

    return {
      fields: fieldsForForm.reduce(
        reduceValuesToFormFields(values, enumerations),
        []
      ),
      key: generateId()
    };
  });

  handleSubmitForm = values => {
    this.props.createPaymentCommand(
      values,
      this.props.client,
      this.props.order,
      this.props.bills.entities
    );
    if (this.props.afterSubmitCallback) this.props.afterSubmitCallback(values);
  };

  render() {
    const { bill, order, client, bills, isLoading } = this.props;
    const { fields, key } = this.mapValuesToFields(bill, order, client, bills);
    return (
      <PreloaderContainer>
        <BlockContentForm>
          <ContainerContext
            key={key}
            fields={fields}
            renderForm={this.renderForm}
            onSubmitForm={this.handleSubmitForm}
            submitValidation
          />
          {isLoading && <PreLoader viewType="content" />}
        </BlockContentForm>
      </PreloaderContainer>
    );
  }
}

const moduleNames = [billsModuleName, commandsLoaderName, clientsModuleName];

function mapStateToProps(state, props) {
  return {
    internetAcquiringIsAvailable: hasPermissionSelector(state, {
      name: pm_payments_create_return_internet_acquiring
    }),
    withoutCheckIsAvailable: hasPermissionSelector(state, {
      name: pm_payments_create_without_check
    }),
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    bills: billsForEntitySelector(state, {
      idEntity: props.order ? props.order.id : null,
      typeEntity
    }),
    client: clientInfoSelector(state, {
      id: props.order ? props.order.agent_id : null
    }),
    enumerations: enumerationsSelector(state, { enumerationNames })
  };
}

export default connect(
  mapStateToProps,
  { fetchBillsForEntity, createPaymentCommand, fetchEnumerations, fetchClient }
)(CreatePaymentForm);
