import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import styled from "styled-components";
import Modal from "../blocks/helperBlocks/Modal";
import ContainerContext, { FieldContext } from "../common/formControls";
import memoizeOne from "memoize-one";
import { objectToArray } from "../../utils";
import ButtonPrimary from "../../designSystem/atoms/Button";
import { updateClient, moduleName } from "../../ducks/clients";
import { modulesIsLoadingSelector } from "../../ducks/logger";
import {
  agentInfoByUserSelector,
  logOut,
  workPlaceChanged
} from "../../ducks/user";
import { fields as fieldsForWorkPlaces } from "../../fields/formFields/tasks/fieldsForWorkPlaces";
import PreLoader from "../common/PreLoader";

const fieldStyle = {
  mb: "small"
};

const FormWrapper = styled.div`
  margin-bottom: 12px;
`;

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

class AddWorkPlace extends PureComponent {
  static propTypes = {
    logOut: PropTypes.func.isRequired,
    onSubmitForm: PropTypes.func.isRequired,
    updateClient: PropTypes.func.isRequired,
    workPlaceChanged: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired
  };

  handleSubmitForm = params => {
    this.props.updateClient(
      this.props.agentInfo.id,
      params,
      this.props.agentInfo,
      {
        successCallback: this.props.workPlaceChanged
      }
    );
  };

  renderField = (fieldList, field) => {
    if (!field.isActive) return fieldList;
    return fieldList.concat(
      <FieldContext key={field.id} name={field.id} fieldStyle={fieldStyle} />
    );
  };

  fieldsToFieldList = memoizeOne(fields => objectToArray(fields));

  renderForm = ({ fields }) => {
    const fieldList = this.fieldsToFieldList(fields);
    return (
      <>
        <FormWrapper>{fieldList.reduce(this.renderField, [])}</FormWrapper>
        <ButtonWrapper>
          <ButtonPrimary type="submit">Сохранить</ButtonPrimary>
        </ButtonWrapper>
      </>
    );
  };

  render() {
    const { logOut, isLoading } = this.props;

    return (
      <Modal
        title="Добавление текущего места работы"
        handleClose={logOut}
        isOpen
        width={400}
      >
        <ContainerContext
          renderForm={this.renderForm}
          fields={fieldsForWorkPlaces}
          onSubmitForm={this.handleSubmitForm}
          submitValidation
        />
        {isLoading && <PreLoader viewType="content" />}
      </Modal>
    );
  }
}

const moduleNames = [moduleName];

export default connect(
  state => {
    return {
      isLoading: modulesIsLoadingSelector(state, { moduleNames }),
      agentInfo: agentInfoByUserSelector(state)
    };
  },
  { updateClient, workPlaceChanged, logOut }
)(AddWorkPlace);
