import React, { PureComponent } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

import Button from "../../designSystem/atoms/Button";
import Input from "../../designSystem/atoms/Input";
import LogoImage from "../../designSystem/atoms/LogoImage";

const StyledInput = styled(Input)`
  width: 100%;
`;

const StyledLogo = styled(LogoImage)`
  margin-bottom: 20px;
`;

const Inner = styled.div`
  position: relative;
  top: 20%;
  width: 300px;
  text-align: center;
`;

const StyledForm = styled.form`
  input {
    margin-bottom: 20px;
  }
`;

class LogInForm extends PureComponent {
  static propTypes = {
    onSubmitForm: PropTypes.func.isRequired
  };

  state = {
    login: "",
    password: ""
  };

  handleChangeField = event => {
    event.preventDefault();
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSubmitForm = event => {
    event.preventDefault();
    const { login, password } = this.state;

    this.props.onSubmitForm(login, password);
  };

  render() {
    const { login, password } = this.state;

    return (
      <Inner>
        <StyledLogo />
        <StyledForm onSubmit={this.handleSubmitForm}>
          <StyledInput
            onChange={this.handleChangeField}
            type="text"
            name="login"
            placeholder="Введите логин"
            value={login}
          />
          <StyledInput
            onChange={this.handleChangeField}
            name="password"
            type="password"
            placeholder="Введите пароль"
            value={password}
          />
          <Button type="submit">Авторизация</Button>
        </StyledForm>
      </Inner>
    );
  }
}

export default LogInForm;
