import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import BlockContent from "../../designSystem/organisms/BlockContent";
import Button from "../../designSystem/atoms/Button";
import ContainerContext from "../common/formControls";
import BtnBlock from "../common/styled/BtnBlock";
import memoizeOne from "memoize-one";
import { objectToArray } from "../../utils";
import FieldForm from "../common/form/FieldForm";

const BlockContentForm = styled(BlockContent)`
  max-height: 650px;
  overflow-y: auto;
`;

class EditClientForm extends PureComponent {
  static propTypes = {
    typeForm: PropTypes.string.isRequired,
    onSubmitForm: PropTypes.func.isRequired,
    fields: PropTypes.array.isRequired,
    btnProps: PropTypes.object
  };

  renderField = (fieldList, field, index) => {
    return fieldList.concat(
      <FieldForm
        key={Array.isArray(field) ? index : field.id}
        field={field}
        index={index}
      />
    );
  };

  renderForm = () => {
    return (
      <>
        {this.props.fields.reduce(this.renderField, [])}
        <BtnBlock>
          <Button type="submit" {...this.props.btnProps}>
            {this.textButton}
          </Button>
        </BtnBlock>
      </>
    );
  };

  textButton = this.props.typeForm === "add" ? "Создать" : "Изменить";

  render() {
    if (!this.props.fields) return null;
    return (
      <BlockContentForm>
        <ContainerContext
          renderForm={this.renderForm}
          fields={this.props.fields}
          onSubmitForm={this.props.onSubmitForm}
          submitValidation
        />
      </BlockContentForm>
    );
  }
}

export default EditClientForm;
