import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropsTypes from "prop-types";
import {
  isAuthSelector,
  testAuth,
  userNameSelector,
  logOut,
  showWorkPlaceFormSelector,
  permissionsIsLoadedSelector
} from "../ducks/user";
import AuthPage from "./routes/AuthPage";
import { renderRoutes } from "react-router-config";
import Header from "../components/blocks/Header";
import { LeftMenu, RightMenu } from "./blocks/menu";
import Notification from "./blocks/Notification";
import { pathnameRouterSelector } from "../redux/selectors/router";
import AddWorkPlace from "../components/forms/AddWorkPlace";

class App extends Component {
  static propTypes = {
    isAuth: PropsTypes.bool.isRequired,
    testAuth: PropsTypes.func.isRequired,
    logOut: PropsTypes.func.isRequired,
    route: PropsTypes.shape({
      routes: PropsTypes.array.isRequired
    }).isRequired,
    pathname: PropsTypes.string.isRequired,
    showWorkPlaceForm: PropsTypes.bool.isRequired,
    permissionsIsLoaded: PropsTypes.bool.isRequired
  };

  componentDidMount() {
    this.props.testAuth();
  }

  render() {
    if (!this.props.isAuth) {
      return (
        <>
          <AuthPage />
          <Notification />
        </>
      );
    } else {
      if (this.props.showWorkPlaceForm) {
        return (
          <>
            <AddWorkPlace />
            <Notification />
          </>
        );
      } else if (this.props.permissionsIsLoaded) {
        return (
          <Fragment>
            <Header onLogOut={this.props.logOut} name={this.props.name} />
            <LeftMenu pathname={this.props.pathname} />
            <RightMenu />
            {renderRoutes(this.props.route.routes)}
            <Notification />
          </Fragment>
        );
      }
    }

    return (
      <>
        <Notification />
      </>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    isAuth: isAuthSelector(state),
    name: userNameSelector(state),
    pathname: pathnameRouterSelector(state, props),
    showWorkPlaceForm: showWorkPlaceFormSelector(state, props),
    permissionsIsLoaded: permissionsIsLoadedSelector(state, props)
  };
}

export default connect(
  mapStateToProps,
  { testAuth, logOut }
)(App);
