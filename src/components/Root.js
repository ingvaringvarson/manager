import React from "react";
import { Provider } from "react-redux";
import store from "../redux";
import history from "../history";
import { renderRoutes } from "react-router-config";
import routes from "../routesConfig";
import { ConnectedRouter as Router } from "connected-react-router";
import { injectGlobal, ThemeProvider } from "styled-components";
import reset from "styled-reset";
import { palette } from "styled-tools";

import theme from "../designSystem/theme";

injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Roboto:400,500&subset=cyrillic');

  ${reset};

  html,
  body,
  #root {
    height: 100%;
    min-height: 100%;
  }

  html {
    box-sizing: border-box;
  }

  body {
    font-family: "Roboto", sans-serif;
    color: ${palette("general", 0)};
    background: #f1f1f1;
  }

  *,
  *::before,
  *::after {
    box-sizing: inherit;
  }

  .ReactModal__Overlay--after-open {
    overflow-y: scroll;
  }
  .ReactModal__Content {
    overflow: visible !important;
  }
  .react-datepicker-popper {
    z-index: 1000 !important;
  }
`;

const Root = () => {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <Router history={history}>{renderRoutes(routes)}</Router>
      </Provider>
    </ThemeProvider>
  );
};

export default Root;
