import Text from "../../../designSystem/atoms/Text";
import styled from "styled-components";

export const RedText = styled.span`
  color: #eb0028;
`;

export const GreyText = styled.span`
  color: #787878;
`;

export const PreSpaceText = styled.span`
  white-space: pre;
`;

export const InfoText = styled(Text)`
  font-size: 12px;
  line-height: 18px;
  margin-bottom: 5px;
`;

export const HeaderWithLine = styled.h3`
  width: 100%;
  text-align: center;
  border-bottom: 1px solid #e0e0e0;
  line-height: 0.1em;
  margin: 10px 0 20px;
`;

export const TextBetween = styled.span`
  background: #fff;
  padding: 0 10px;
  color: #787878;
`;
