import styled from "styled-components";

export const VerticalCenter = styled.div`
  display: flex;
  justify-content: center;
`;
