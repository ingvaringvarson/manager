import styled, { css } from "styled-components";
import { ifProp, palette } from "styled-tools";
import { space, right } from "styled-system";
import ButtonMixin from "../../../designSystem/mixins/Button";
import Button from "../../../designSystem/atoms/Button";
import Icon from "../../../designSystem/atoms/Icon";

export const IconClearWrap = styled.button`
  position: absolute;
  top: ${ifProp("iconFix", "10px", "8px")};
  right: 10px;
  width: 18px;
  height: 18px;
  cursor: pointer;
  border: none;
  background: transparent;
  padding: 0;

  ${ifProp(
    "hide",
    css`
      display: none;
    `
  )};

  :disabled {
    cursor: not-allowed;
  }

  ${space}
  ${right}
`;

export const fieldStyle = {
  mb: "small"
};

export const PaymentIconWrap = styled.div`
  display: flex;
  flex-shrink: 0;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 54px;
  height: 54px;
  margin-bottom: 6px;
  border-radius: 50%;
  background: #eb0028;
`;

export const PaymentIcon = styled(Icon)`
  width: 20px;
  height: 16px;
  color: #ffffff;
`;

export const PaymentTitle = styled.p`
  font-size: 24px;
`;

export const PaymentKind = styled.button`
  ${ButtonMixin.white};
  display: inline-flex;
  flex-direction: column;
  align-items: center;
  padding: 16px;
  overflow: hidden;
  font-size: 24px;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.05);
  border: 1px solid rgba(0, 0, 0, 0.05);
  border-radius: 5px;
  transition: background 0.3s;
  cursor: pointer;
  color: ${palette("general", 1)};
  background: white;
  outline: none;

  :not(:disabled):hover {
    color: white;
    background: ${palette("general", 1)};
  }

  ${ifProp(
    "active",
    css`
      background: #e5e5e5;

      ${PaymentTitle} {
        color: #eb0028;
      }
    `
  )}
`;

export const CheckBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 16px;
  padding: 8px;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.05);
  border: 1px solid rgba(0, 0, 0, 0.05);
  border-radius: 5px;
`;

export const TransparentButton = styled(Button)`
  ${space};
  background: transparent;
  color: #444;
  font-size: 14px;
  font-weight: 500;
  cursor: pointer;
  padding: 0 15px;
  height: 36px;
  border: none;
  :disabled {
    color: #777;
    cursor: not-allowed;
  }
  :not(:disabled):hover {
    background: #eee;
  }
`;

export const GridFieldsTemplate = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 44px;
  row-gap: 1em;
  margin-bottom: 20px;
`;
