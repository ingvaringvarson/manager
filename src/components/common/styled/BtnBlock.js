import styled from "styled-components";

const BtnBlock = styled.div`
  text-align: right;
  margin-top: 16px;
`;

export default BtnBlock;
