import Box from "../../../designSystem/organisms/Box";
import styled from "styled-components";

export const PointerBox = styled(Box)`
  cursor: pointer;
`;
