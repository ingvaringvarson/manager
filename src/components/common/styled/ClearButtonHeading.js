import styled from "styled-components";
import ClearButtonContext from "../formControls/ClearButtonContext";

const ClearButtonHeading = styled(ClearButtonContext)`
  position: absolute;
  top: 16px;
  right: 16px;
`;

export default ClearButtonHeading;
