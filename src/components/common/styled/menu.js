import styled, { css } from "styled-components";
import { ifProp } from "styled-tools";

import Menu from "../../../designSystem/molecules/Menu";
import Text from "../../../designSystem/atoms/Text";

export const StyledText = styled(Text)`
  display: block;
  padding: 10px 0;
  text-align: center;
  cursor: pointer;

  ${ifProp(
    "isDisabled",
    css`
      opacity: 0.3;
    `
  )};

  :nth-child(odd) {
    padding-top: 15px;
  }

  :nth-child(even) {
    padding-bottom: 15px;
  }

  :nth-child(even):not(:last-of-type) {
    border-bottom: 1px solid #e4e4e4;
  }
`;

export const MenuStyled = styled(Menu)`
  display: ${ifProp("isOpen", "block", "none")};
  position: absolute;
  width: 100%;
  right: 0;
  left: 0;
  top: 100%;
  padding-top: 20px;
  z-index: 3;
`;

export const Container = styled.div`
  position: relative;
  width: 100%;
`;
