import styled from "styled-components";

export const modalStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "340px",
    border: "none",
    padding: "0",
    overflow: "visible"
  }
};

export const overlayBackgroundColor = "rgba(0, 0, 0, 0.5)";
export const overlayZIndex = "3";

export const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  cursor: default;
  z-index: 3;
`;
