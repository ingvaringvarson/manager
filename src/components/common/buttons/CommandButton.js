import React, { memo } from "react";
import styled from "styled-components";

import Icon from "../../../designSystem/atoms/Icon";
import Button from "../../../designSystem/atoms/Button";

const StyledButton = styled(Button)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 36px;
  padding: 0;
`;

const CommandButton = memo(props => {
  return (
    <StyledButton type="button" simple {...props}>
      <Icon icon="baseline_more" />
    </StyledButton>
  );
});

export default CommandButton;
