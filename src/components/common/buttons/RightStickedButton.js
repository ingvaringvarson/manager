import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Button from "../../../designSystem/atoms/Button";

const ButtonWrapper = styled.div`
  padding-top: 16px;
  text-align: right;
`;

const propTypes = {
  hasOwnButton: PropTypes.bool
};

const defaultProps = {
  hasOwnButton: false
};

const RightStickedButton = props => {
  const { children, hasOwnButton } = props;
  return (
    <ButtonWrapper>
      {hasOwnButton ? children : <Button {...props} />}
    </ButtonWrapper>
  );
};

RightStickedButton.defaultProps = defaultProps;
RightStickedButton.propTypes = propTypes;

export default RightStickedButton;
