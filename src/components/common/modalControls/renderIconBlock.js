import React from "react";
import styled from "styled-components";

import Icon from "../../../designSystem/atoms/Icon";

export const IconControl = styled(Icon)`
  width: 24px;
  cursor: pointer;
`;

export default icon => (state, modalProps, blockProps, iconProps) => {
  return <IconControl {...iconProps} icon={icon} />;
};
