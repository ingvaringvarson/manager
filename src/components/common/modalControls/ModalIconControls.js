import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import ReactModal from "react-modal";
import memoizeOne from "memoize-one";
import { customStyles } from "../../../constants/defaultStyles/reactModalStyles";

class ModalIconControls extends PureComponent {
  static propTypes = {
    children: PropTypes.func.isRequired,
    renderContent: PropTypes.func.isRequired,
    iconProps: PropTypes.object,
    modalStyles: PropTypes.object
  };

  static defaultProps = {
    valuesForFields: {},
    iconProps: {},
    modalStyles: {}
  };

  constructor(props) {
    super(props);

    this.state = {
      modalIsOpen: false,
      iconIsShow: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (state.modalIsOpen && state.iconIsShow) {
      return {
        iconIsShow: false
      };
    }

    return null;
  }

  handleCloseModal = () => {
    this.setState({ modalIsOpen: false });
  };

  handleOpenModal = () => {
    this.setState({ modalIsOpen: true });
  };

  handleToggleStateModal = () => {
    this.setState(prevState => {
      return { modalIsOpen: !prevState.modalIsOpen };
    });
  };

  handleShowIcon = () => {
    if (!this.state.iconIsShow) this.setState({ iconIsShow: true });
  };

  handleHideIcon = () => {
    if (this.state.iconIsShow) this.setState({ iconIsShow: false });
  };

  defaultIconProps = {
    onClick: this.handleToggleStateModal,
    width: "24px",
    height: "24px"
  };

  blockProps = {
    onMouseEnter: this.handleShowIcon,
    onMouseLeave: this.handleHideIcon
  };

  modalProps = {
    onOpen: this.handleOpenModal,
    onClose: this.handleCloseModal
  };

  mapModalStyles = memoizeOne((modalStyles, defaultStyles) => {
    return {
      ...defaultStyles,
      ...modalStyles,
      content: {
        ...defaultStyles.content,
        ...modalStyles.content
      }
    };
  });

  render() {
    const stylesByModal = this.mapModalStyles(
      this.props.modalStyles,
      customStyles
    );

    const iconProps = {
      ...this.defaultIconProps,
      ...this.props.iconProps
    };

    return (
      <>
        {this.props.children(
          this.state,
          this.modalProps,
          this.blockProps,
          iconProps
        )}
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.handleCloseModal}
          style={stylesByModal}
        >
          {this.props.renderContent(
            this.state,
            this.modalProps,
            this.blockProps,
            iconProps
          )}
        </ReactModal>
      </>
    );
  }
}

export default ModalIconControls;
