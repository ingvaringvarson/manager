import React, { memo } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import { mapArrOfIdsWithEnumerations } from "../../../utils";

const SpanElement = styled.span`
  margin-right: 5px;
  padding: 2px 15px;
  border-radius: 5px;
  border: 1px solid #e0e0e0;
  font-size: 12px;
  color: #3b3b3b;
`;

const propTypes = {
  field: PropTypes.object,
  entity: PropTypes.object,
  enumerations: PropTypes.object,
  restHelperData: PropTypes.object
};

const MultiValue = memo(props => {
  const { field, entity, enumerations } = props;

  let elements = mapArrOfIdsWithEnumerations(field, entity, enumerations);
  if (!elements) {
    return null;
  }

  elements = elements.filter(Boolean);
  if (!elements.length) {
    return null;
  }

  return (
    <div>
      {elements.filter(Boolean).map(el => {
        return <SpanElement key={el.id}>{el.name}</SpanElement>;
      })}
    </div>
  );
});

MultiValue.propTypes = propTypes;

export default MultiValue;
