import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Input from "../../../designSystem/atoms/Input";
import Icon from "../../../designSystem/atoms/Icon";
import styled from "styled-components";
import { ifProp } from "styled-tools";

const Wrapper = styled.div`
  display: flex;
`;

const IconControl = styled(Icon)`
  width: 24px;
  cursor: pointer;
`;

const InputControl = styled(Input)`
  text-align: center;
  height: 24px;
  width: 50px;
`;

const ButtonControl = styled.button`
  border: none;
  background: none;
  padding: 0;
  margin: 0;
  outline: 0;

  opacity: ${ifProp("disabled", ".3", "1")};
`;

class UpdateCountControl extends PureComponent {
  static propTypes = {
    delay: PropTypes.number,
    onChangeCount: PropTypes.func.isRequired,
    count: PropTypes.number.isRequired,
    maxCount: PropTypes.number,
    minCount: PropTypes.number,
    entity: PropTypes.object
  };

  static defaultProps = {
    delay: 1000,
    minCount: 1
  };

  state = {
    value: this.props.count,
    prevValue: 0
  };

  counterDelay = null;

  handleChangeValue = (type, withDelay = false) => event => {
    event.preventDefault();
    const {
      maxCount,
      minCount,
      onChangeCount,
      entity,
      count,
      delay
    } = this.props;

    const { value } = this.state;
    const newState = { value: +value };

    switch (type) {
      case "custom":
        const numbers = event.target.value.match(/[0-9]/g);
        newState.value = numbers ? +numbers.join("") : "";
        break;
      case "increment":
        if (maxCount && newState.value + minCount > maxCount) return;
        newState.value += minCount;
        break;
      case "decrement":
        if (newState.value <= minCount) return;
        newState.value =
          newState.value - minCount < minCount
            ? minCount
            : newState.value - minCount;
        break;
      default:
        return;
    }

    if (this.counterDelay) clearTimeout(this.counterDelay);

    if (newState.value === count) {
      if (newState.value !== value) this.setState(newState);
      return;
    }

    const newValue =
      maxCount && newState.value > maxCount ? maxCount : newState.value;

    if (!withDelay) {
      onChangeCount(newValue, entity);
    } else if (newValue > 0) {
      this.counterDelay = setTimeout(() => {
        return onChangeCount(newValue, entity);
      }, delay);
    }

    this.setState(newState);
  };

  handleCustomChangeValue = this.handleChangeValue("custom", true);

  handleIncrementValue = this.handleChangeValue("increment");

  handleDecrementValue = this.handleChangeValue("decrement");

  handleBlurInputControl = () => {
    if (this.state.value === "" || this.state.value === 0) {
      this.setState(prevState => ({ value: prevState.prevValue }));
    }
  };

  handleFocusInputControl = () => {
    if (this.state.value !== "") {
      this.setState(prevState => ({ value: "", prevQty: prevState.value }));
    }
  };

  render() {
    const { maxCount } = this.props;
    const { value } = this.state;

    return (
      <Wrapper>
        <ButtonControl
          onClick={this.handleDecrementValue}
          disabled={value <= 1}
        >
          <IconControl icon="remove" />
        </ButtonControl>
        <InputControl
          type="text"
          value={value}
          onFocus={this.handleFocusInputControl}
          onBlur={this.handleBlurInputControl}
          onChange={this.handleCustomChangeValue}
        />
        <ButtonControl
          onClick={this.handleIncrementValue}
          disabled={maxCount && value >= maxCount}
        >
          <IconControl icon="add" />
        </ButtonControl>
      </Wrapper>
    );
  }
}

export default UpdateCountControl;
