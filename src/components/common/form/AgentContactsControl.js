import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateClient, moduleName } from "../../../ducks/clients";
import { addNotification } from "../../../ducks/logger";
import { modulesIsLoadingSelector } from "../../../ducks/logger";
import Field from "../../../designSystem/molecules/Field";
import Select from "../../../designSystem/molecules/Select";
import memoizeOne from "memoize-one";
import PreLoader from "../PreLoader";
import { generateId, getValueInDepth } from "../../../utils";
import Icon from "../../../designSystem/atoms/Icon";
import styled from "styled-components";

const StyledIcon = styled(Icon)`
  justify-self: center;
  width: 24px;
  color: #000;
  cursor: pointer;
`;

class AgentContactsControl extends Component {
  static propTypes = {
    updateClient: PropTypes.func.isRequired,
    addNotification: PropTypes.func.isRequired,
    value: PropTypes.number,
    agent: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired
  };

  formId = generateId();

  state = {
    value: this.props.value,
    key: generateId()
  };

  handleChangeContact = ([value]) => {
    const contacts = getValueInDepth(this.props.agent, ["contacts"]);

    if (contacts) {
      const contact = contacts.find(c => c._id === value);

      this.setState({
        key: generateId(),
        value: contact.value
      });
    }
  };

  availableContactTypes = [1, 2, 7];

  getOptions = memoizeOne(contacts => {
    return contacts
      ? contacts.reduce((opts, c) => {
          if (this.availableContactTypes.includes(c.type)) {
            opts.push({
              id: c._id,
              name: c.value
            });
          }

          return opts;
        }, [])
      : [];
  });

  handleChangeValue = event => {
    if (this.state.value !== event.target.value) {
      this.setState({
        value: event.target.value
      });
    }
  };

  handleAddContact = event => {
    event.preventDefault();
    const { value } = this.state;
    const { agent } = this.props;

    if (!value || !agent) return;

    const contacts = getValueInDepth(agent, ["contacts"]);
    const contact = contacts.find(c => c.value === value);

    if (contact) {
      this.props.addNotification({
        message: "Указанный контакт уже есть у клиента"
      });
      return;
    }

    const newContact = {
      type: 0,
      value,
      is_active: true
    };

    const newAgent = {
      ...agent,
      contacts: agent.contacts ? [...agent.contacts, newContact] : [newContact]
    };

    this.props.updateClient(agent.id, {}, newAgent);
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.value !== this.state.value) {
      this.props.onChange(this.state.value, this.props.name);
    }
  }

  render() {
    const { value, key } = this.state;
    const { isLoading, agent } = this.props;

    const contacts = agent ? this.getOptions(agent.contacts) : [];
    return (
      <>
        {isLoading && <PreLoader viewType="content" />}
        <Field>
          <Select
            key={key}
            placeholder=""
            name={`contacts_${this.formId}`}
            options={contacts}
            onChange={this.handleChangeContact}
            searchMode="custom"
            defaultSearchValue={value}
            onChangeSearchValue={this.handleChangeValue}
          />
        </Field>
        {!!agent && <StyledIcon icon="add" onClick={this.handleAddContact} />}
      </>
    );
  }
}

const moduleNames = [moduleName];

function mapStateToProps(state) {
  return {
    isLoading: modulesIsLoadingSelector(state, { moduleNames })
  };
}

export default connect(
  mapStateToProps,
  { updateClient, addNotification }
)(AgentContactsControl);
