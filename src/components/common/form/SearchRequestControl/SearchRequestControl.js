import { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { actionIsLoadingSelector } from "../../../../ducks/logger";

class SearchRequestControl extends Component {
  static propTypes = {
    children: PropTypes.func.isRequired,
    actionSearch: PropTypes.func.isRequired,
    selectorSearchResult: PropTypes.func.isRequired,
    selectorInitSearchResultIsLoaded: PropTypes.func.isRequired,
    fetchSearch: PropTypes.func.isRequired,
    initSearchResultIsLoaded: PropTypes.bool.isRequired,
    searchResult: PropTypes.any.isRequired,
    keyParam: PropTypes.string.isRequired,
    keyById: PropTypes.string.isRequired,
    selected: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    params: PropTypes.object,
    delayBeforeAction: PropTypes.number,
    minSizeSearchStringByRequest: PropTypes.number
  };

  static defaultProps = {
    delayBeforeAction: 500,
    minSizeSearchStringByRequest: 1
  };

  actionCounter = null;

  componentDidMount() {
    const {
      fetchSearch,
      params,
      selected,
      keyById,
      initSearchResultIsLoaded
    } = this.props;

    if (
      Array.isArray(selected) &&
      selected.length &&
      fetchSearch &&
      !initSearchResultIsLoaded
    ) {
      const requestParams = { ...params, [keyById]: selected[0] };
      fetchSearch(requestParams);
    }
  }

  fetchSearchResult = searchValue => {
    const {
      fetchSearch,
      params,
      keyParam,
      minSizeSearchStringByRequest,
      delayBeforeAction
    } = this.props;

    if (this.actionCounter) clearTimeout(this.actionCounter);

    if (searchValue.length >= minSizeSearchStringByRequest) {
      const requestParams = { ...params, [keyParam]: searchValue };

      this.actionCounter = setTimeout(
        () => fetchSearch(requestParams),
        delayBeforeAction
      );
    }
  };

  forceFetchSearchResult = isFocus => {
    const {
      fetchSearch,
      params,
      keyParam,
      minSizeSearchStringByRequest
    } = this.props;

    if (!isFocus || minSizeSearchStringByRequest !== 0) return;

    if (this.actionCounter) clearTimeout(this.actionCounter);

    const requestParams = { ...params, [keyParam]: "" };

    fetchSearch(requestParams);
  };

  render() {
    return this.props.children(
      this.fetchSearchResult,
      this.forceFetchSearchResult,
      this.props.searchResult,
      this.props.isLoading
    );
  }
}

function mapStateToProps(state, props) {
  return {
    isLoading: actionIsLoadingSelector(state, props),
    searchResult: props.selectorSearchResult(state, props),
    initSearchResultIsLoaded: props.selectorInitSearchResultIsLoaded(
      state,
      props
    )
  };
}
function mapDispatchToProps(dispatch, props) {
  return {
    fetchSearch: params => dispatch(props.actionSearch(params))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchRequestControl);
