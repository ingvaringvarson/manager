import {
  brandsVendorCodesSelector,
  searchBrands
} from "../../../../ducks/search";
import {
  warehousesToOptionsSelector,
  fetchWarehousesForEntity
} from "../../../../ducks/warehouses";
import {
  clientsToOptionsSelector,
  fetchClientsForEntity
} from "../../../../ducks/clients";
import {
  ordersToOptionsSelector,
  fetchOrdersForEntity
} from "../../../../ducks/orders";
import {
  pricelistOptionsSelector,
  fetchPriceLists
} from "../../../../ducks/pricelists";

//ToDo: http://jira.laf24.ru/browse/MAN-1745

import { generateId, getValueInDepth } from "../../../../utils";
import {
  getIdForEntity,
  getNameForEntity,
  getIdByEmployee,
  getOrderId,
  getOrderCode,
  getPersonFullName
} from "../../../../fields/helpers";

function mapProps(
  { id, type, key, ...initProps },
  { page = 1, pageSize = 25, ...initParams },
  actionSearch,
  selectorSearchResult
) {
  const actionKey = `${id}_${type}`;
  const props = {
    id,
    actionKey,
    idEntity: id,
    typeEntity: type,
    keyParam: key,
    ...initProps,
    params: {
      page,
      page_size: pageSize,
      ...initParams
    },
    actionSearch: () => {}, //экшэн для запроса сущностей при изменении поискового слова
    selectorSearchResult: () => {}, //селектор для получения сущностей в результате поиска
    selectorInitSearchResultIsLoaded: () => {} //селектор озвращает true, если в загруженных сущностях есть изначально установленное значение
  };

  //записываем в экшэн ключ для отслеживания загрузки и показа прелоадера
  if (actionSearch) {
    props.actionSearch = ({ ...requestParams }) => {
      if (!requestParams[key]) delete requestParams[key];

      return {
        ...actionSearch(type, id, requestParams),
        actionKey
      };
    };
  }

  if (selectorSearchResult) {
    props.selectorSearchResult = selectorSearchResult;
    props.selectorInitSearchResultIsLoaded = (state, props) => {
      const selectedOptionId = getValueInDepth(props, ["selected", 0]);
      const options = selectorSearchResult(state, props);

      if (selectedOptionId === null || !options.length) return false;

      return !!options.find(op => op.id === selectedOptionId);
    };
  }

  return props;
}

export function searchRequestPropsForAgents(props = {}) {
  const {
    id = generateId(),
    keyById = "employee_id",
    type = "agents",
    key = "full_name",
    getName = getPersonFullName,
    getId = getIdByEmployee,
    minSizeSearchStringByRequest = 1,
    params = {}
  } = props;

  return mapProps(
    { id, type, key, keyById, minSizeSearchStringByRequest },
    params,
    fetchClientsForEntity,
    clientsToOptionsSelector(getName, getId)
  );
}

export function searchRequestPropsForOrders(props = {}) {
  const {
    id = generateId(),
    keyById = "id",
    type = "orders",
    key = "code",
    getName = getOrderCode,
    getId = getOrderId,
    minSizeSearchStringByRequest = 1,
    params = {}
  } = props;

  return mapProps(
    { id, type, key, keyById, minSizeSearchStringByRequest },
    params,
    fetchOrdersForEntity,
    ordersToOptionsSelector(getName, getId)
  );
}

export function searchRequestPropsForWarehouses(props = {}) {
  const {
    id = generateId(),
    keyById = "id",
    type = "warehouse",
    key = "name",
    getName = getNameForEntity,
    getId = getIdForEntity,
    minSizeSearchStringByRequest = 0,
    params = {}
  } = props;

  return mapProps(
    { id, type, key, keyById, minSizeSearchStringByRequest },
    params,
    fetchWarehousesForEntity,
    warehousesToOptionsSelector(getName, getId)
  );
}

export function searchRequestPropsForPriceList(props = {}) {
  const {
    id = generateId(),
    keyById = "pricelist_id",
    type = "pricelist",
    key = "pricelist_code",
    minSizeSearchStringByRequest = 1,
    params = {}
  } = props;

  return mapProps(
    { id, type, key, keyById, minSizeSearchStringByRequest },
    params,
    fetchPriceLists,
    pricelistOptionsSelector
  );
}

export function searchRequestPropsForBrandVendorCode(props = {}) {
  const {
    id = generateId(),
    keyById = "vendorCode",
    type = "search",
    key = "vendorCode",
    minSizeSearchStringByRequest = 1,
    params = {}
  } = props;

  const searchRequestProps = mapProps(
    { id, type, key, keyById, minSizeSearchStringByRequest },
    params,
    null,
    brandsVendorCodesSelector
  );

  return {
    ...searchRequestProps,
    actionSearch: ({ vendorCode }) => {
      return {
        ...searchBrands(vendorCode, searchRequestProps.idEntity),
        actionKey: searchRequestProps.actionKey
      };
    }
  };
}
