import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { FieldContext } from "../../common/formControls";
import styled from "styled-components";
import { palette } from "styled-tools";
import TextMixin from "../../../designSystem/mixins/Text";
import Field from "../../../designSystem/molecules/Field";
import { ContextForm } from "../formControls/context";

const fieldStyle = {
  mb: "8px"
};

const StyledRange = styled.div`
  display: flex;
`;

const Range = styled.span`
  display: inline-flex;
  margin: 5px;
  font-size: 24px;
  color: ${palette("secondary", 0)};
`;

class FieldForm extends PureComponent {
  static propTypes = {
    field: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.arrayOf(PropTypes.any)
    ]).isRequired,
    index: PropTypes.number,
    style: PropTypes.object
  };
  static contextType = ContextForm;

  renderField(field) {
    const { fields = {}, ...rest } = this.context;
    const contextField = fields[field.id];

    if (!contextField || !contextField.isActive) return null;

    if (contextField.formControlProps.render) {
      return contextField.formControlProps.render({
        ...rest,
        field: contextField
      });
    }

    return (
      <FieldContext
        fieldStyle={fieldStyle}
        name={contextField.formControlProps.name}
      />
    );
  }

  renderFields() {
    const { field, style } = this.props;
    const fieldFirst = this.renderField(field[1][0]);
    const fieldSecond = this.renderField(field[1][1]);
    return fieldFirst || fieldSecond ? (
      <Field fieldStyle={fieldStyle} style={style} label={field[0].name}>
        <StyledRange>
          {fieldFirst}
          <Range>
            {field[0].hasOwnProperty("separator") ? field[0].separator : "-"}
          </Range>
          {fieldSecond}
        </StyledRange>
      </Field>
    ) : null;
  }

  render() {
    const { field } = this.props;
    return Array.isArray(field) ? this.renderFields() : this.renderField(field);
  }
}

export default FieldForm;
