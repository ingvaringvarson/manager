import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  fetchKladrInfo,
  stateInfoSelector,
  stateSuggestSelector,
  moduleName
} from "../../../ducks/kladr/index";
import {
  loadsStateSelector,
  modulesIsLoadingSelector
} from "../../../ducks/logger/index";
import Field from "../../../designSystem/molecules/Field/index";
import Select from "../../../designSystem/molecules/Select/index";
import memoizeOne from "memoize-one";
import PreLoader from "../PreLoader";
import { generateId, mapIdToSelectedIds } from "../../../utils";

class SearchKladrControl extends Component {
  static propTypes = {
    fetchKladrInfo: PropTypes.func.isRequired,
    value: PropTypes.number,
    suggest: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    fetchKladrSuggest: PropTypes.func.isRequired
  };

  formId = generateId();

  placeholders = {
    city: "Введите город",
    street_with_type: "Введите улицу",
    house: "Введите номер дома"
  };

  state = {
    cityName: "",
    cityInfo: null,
    requestCityId: generateId(),
    cityError: null,
    streetName: "",
    streetInfo: null,
    requestStreetId: generateId(),
    streetError: null,
    houseName: "",
    houseInfo: null,
    requestHouseId: generateId(),
    houseError: null,
    locationError: null,
    locationInfo: null,
    requestCurrentLocationId:
      this.props.value && this.props.value !== "0" ? generateId() : null,
    isLoadedCurrentLocation: !(this.props.value && this.props.value !== "0"),
    isLoading: false,
    loadKeys: PropTypes.arrayOf(PropTypes.string).isRequired
  };

  static getDerivedStateFromProps(props, state) {
    const newState = {};

    if (props.isLoading !== state.isLoading) {
      newState.isLoading = props.isLoading;
    }

    if (
      state.requestCurrentLocationId &&
      props.info[state.requestCurrentLocationId]
    ) {
      newState.cityName = "";
      newState.cityInfo = null;
      newState.streetName = "";
      newState.streetInfo = null;
      newState.houseName = "";
      newState.houseInfo = null;

      if (props.info[state.requestCurrentLocationId]) {
        const location = props.info[state.requestCurrentLocationId];

        newState.requestHouseId = generateId();
        newState.houseInfo = location;
        newState.houseName = location.data.house;
        newState.houseError = null;
        newState.requestCityId = generateId();
        newState.cityInfo = location;
        newState.cityName = location.data.city;
        newState.cityError = null;
        newState.requestStreetId = generateId();
        newState.streetInfo = location;
        newState.streetName = location.data.street_with_type;
        newState.streetError = null;
        newState.locationInfo = location.data;
      }

      newState.requestCurrentLocationId = null;
      newState.isLoadedCurrentLocation = true;
    } else if (
      !state.isLoadedCurrentLocation &&
      !props.isLoading &&
      state.isLoading
    ) {
      newState.isLoadedCurrentLocation = true;
    }

    if (Object.keys(newState)) return newState;

    return null;
  }

  componentDidMount() {
    const { value } = this.props;
    const { requestCurrentLocationId } = this.state;

    if (value && value !== "0") {
      this.props.fetchKladrInfo(requestCurrentLocationId, {
        query: value
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      cityInfo,
      streetInfo,
      houseInfo,
      requestCurrentLocationId,
      locationInfo
    } = this.state;

    switch (true) {
      case prevState.requestCurrentLocationId !== null &&
        requestCurrentLocationId === null &&
        houseInfo === null: {
        return this.props.onChange("0", locationInfo);
      }
      case prevState.locationInfo === locationInfo &&
        (prevState.cityInfo !== cityInfo ||
          prevState.streetInfo !== streetInfo): {
        return this.props.onChange(null, locationInfo);
      }
      case prevState.houseInfo !== houseInfo: {
        return this.props.onChange(
          houseInfo ? houseInfo.data.fias_id : null,
          locationInfo
        );
      }
    }
  }

  handleSearchLocation = (value, name) => {
    const {
      requestCityId,
      requestStreetId,
      requestHouseId,
      cityInfo,
      streetInfo
    } = this.state;
    const { fetchKladrSuggest } = this.props;

    const [type] = name.split("_");

    let requestId = "";
    let requestParams = "";

    switch (type) {
      case "city": {
        requestId = requestCityId;
        requestParams = {
          query: value,
          from_bound: { value: "city" },
          to_bound: { value: "city" }
        };

        break;
      }
      case "street": {
        requestId = requestStreetId;
        requestParams = cityInfo
          ? {
              query: value,
              from_bound: { value: "street" },
              to_bound: { value: "street" },
              locations: [
                {
                  kladr_id: cityInfo.data.city_kladr_id
                }
              ]
            }
          : null;

        break;
      }
      case "house": {
        requestId = requestHouseId;
        requestParams =
          cityInfo && streetInfo
            ? {
                query: value,
                from_bound: { value: "house" },
                to_bound: { value: "house" },
                locations: [
                  {
                    kladr_id: streetInfo.data.street_kladr_id
                  }
                ]
              }
            : null;

        break;
      }
    }

    if (!!value && requestId && requestParams) {
      fetchKladrSuggest(requestId, requestParams);
    }
  };

  handleChangeLocation = ([value], name) => {
    const [type] = name.split("_");
    const { suggest } = this.props;

    let propKeyVal = "";
    let requestId = "";
    let propKeyId = "";
    let propKeyError = "";
    let propKeyFias = "";
    let resetState = {};

    switch (type) {
      case "city": {
        propKeyVal = "cityName";
        requestId = "requestCityId";
        propKeyId = "cityInfo";
        propKeyError = "cityError";
        propKeyFias = "city_fias_id";
        resetState = {
          streetName: "",
          streetInfo: null,
          houseName: "",
          houseInfo: null,
          locationInfo: null,
          requestStreetId: generateId(),
          requestHouseId: generateId()
        };
        break;
      }
      case "street": {
        propKeyVal = "streetName";
        requestId = "requestStreetId";
        propKeyId = "streetInfo";
        propKeyError = "streetError";
        propKeyFias = "street_fias_id";
        resetState = {
          houseName: "",
          houseInfo: null,
          locationInfo: null,
          requestHouseId: generateId()
        };
        break;
      }
      case "house": {
        propKeyVal = "houseName";
        requestId = "requestHouseId";
        propKeyId = "houseInfo";
        propKeyError = "houseError";
        propKeyFias = "fias_id";
        break;
      }
    }

    const location = suggest[this.state[requestId]]
      ? suggest[this.state[requestId]].entities.find(
          entity => entity.data[propKeyFias] === value
        )
      : null;

    if (propKeyVal && propKeyId && location) {
      if (type === "house") resetState.locationInfo = location.data;

      this.setState({
        ...resetState,
        [requestId]: generateId(),
        [propKeyVal]: location.data[type],
        [propKeyId]: location,
        [propKeyError]: null
      });
    }
  };

  mapEntitiesToOptions = (suggest, id, key, info, keyById) => {
    const { value } = this.props;
    const { locationInfo } = this.state;
    const searchResult = [];

    if (value && value !== "0" && locationInfo && info) {
      searchResult.push({
        id: info.data[keyById],
        name: info.data[key]
      });
    }

    return suggest[id]
      ? suggest[id].entities.map(entity => ({
          id: entity.data[keyById],
          name: entity.data[key]
        }))
      : searchResult;
  };

  normalizeHouseName = data => {
    let name = "";

    if (data.house) {
      name = name
        ? `${name}, ${data.house_type_full} ${data.house}`
        : `${data.house_type_full} ${data.house}`;
    }

    if (data.block) {
      name = name
        ? `${name}, ${data.block_type_full} ${data.block}`
        : `${data.block_type_full} ${data.block}`;
    }

    if (data.flat) {
      name = name
        ? `${name}, ${data.flat_type_full} ${data.flat}`
        : `${data.flat_type_full} ${data.flat}`;
    }

    return name;
  };

  mapEntitiesToOptionsByHouse = (suggest, id, key, info) => {
    const { value } = this.props;
    const { locationInfo } = this.state;

    if (value && value !== "0" && locationInfo && info) {
      return [
        {
          id: info.data.fias_id,
          name: this.normalizeHouseName(info.data)
        }
      ];
    }

    return suggest[id]
      ? suggest[id].entities.map(entity => ({
          id: entity.data.fias_id,
          name: this.normalizeHouseName(entity.data)
        }))
      : [];
  };

  getOptions = {
    city: memoizeOne(this.mapEntitiesToOptions),
    street_with_type: memoizeOne(this.mapEntitiesToOptions),
    house: memoizeOne(this.mapEntitiesToOptionsByHouse)
  };

  getSelectedCity = memoizeOne(mapIdToSelectedIds);
  getSelectedStreet = memoizeOne(mapIdToSelectedIds);
  getSelectedHouse = memoizeOne(mapIdToSelectedIds);

  render() {
    const {
      cityError,
      cityInfo,
      cityName,
      streetInfo,
      streetName,
      streetError,
      houseInfo,
      houseName,
      houseError,
      requestCityId,
      requestHouseId,
      requestStreetId,
      isLoadedCurrentLocation
    } = this.state;
    const { suggest, loadKeys } = this.props;

    return (
      <>
        {!isLoadedCurrentLocation && <PreLoader viewType="content" />}
        <Field notice={cityError} invalid={!!cityError} mb="10px">
          <Select
            key={requestCityId}
            isLoading={loadKeys.includes(requestCityId)}
            placeholder={this.placeholders.city}
            name={`city_${this.formId}`}
            selected={this.getSelectedCity(
              cityInfo ? cityInfo.data.city_fias_id : null
            )}
            options={this.getOptions.city(
              suggest,
              requestCityId,
              "city",
              cityInfo,
              "city_fias_id"
            )}
            searchMode="custom"
            defaultSearchValue={cityName}
            onChangeSearchValue={this.handleSearchLocation}
            onChange={this.handleChangeLocation}
          />
        </Field>
        <Field notice={streetError} invalid={!!streetError} mb="10px">
          <Select
            key={requestStreetId}
            isLoading={loadKeys.includes(requestStreetId)}
            placeholder={this.placeholders.street_with_type}
            name={`street_${this.formId}`}
            selected={this.getSelectedStreet(
              streetInfo ? streetInfo.data.street_fias_id : null
            )}
            options={this.getOptions.street_with_type(
              suggest,
              requestStreetId,
              "street_with_type",
              streetInfo,
              "street_fias_id"
            )}
            searchMode="custom"
            defaultSearchValue={streetName}
            onChangeSearchValue={this.handleSearchLocation}
            onChange={this.handleChangeLocation}
          />
        </Field>
        <Field notice={houseError} invalid={!!houseError} mb="10px">
          <Select
            key={requestHouseId}
            isLoading={loadKeys.includes(requestHouseId)}
            placeholder={this.placeholders.house}
            name={`house_${this.formId}`}
            selected={this.getSelectedHouse(
              houseInfo ? houseInfo.data.fias_id : null
            )}
            options={this.getOptions.house(
              suggest,
              requestHouseId,
              "house",
              houseInfo
            )}
            searchMode="custom"
            defaultSearchValue={houseName}
            onChangeSearchValue={this.handleSearchLocation}
            onChange={this.handleChangeLocation}
          />
        </Field>
      </>
    );
  }
}

const moduleNames = [moduleName];

function mapStateToProps(state, props) {
  return {
    info: stateInfoSelector(state, props),
    suggest: stateSuggestSelector(state, props),
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    loadKeys: loadsStateSelector(state, props)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchKladrInfo: (...arg) => dispatch(fetchKladrInfo(...arg)),
    fetchKladrSuggest: (id, params) =>
      dispatch({ ...fetchKladrInfo(id, params), actionKey: id })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchKladrControl);
