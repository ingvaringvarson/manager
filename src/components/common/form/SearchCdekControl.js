import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  fetchCdekInfo,
  cdekInfoSelector,
  pointsSelector,
  requestParamsSelector,
  isLoadedSelector,
  moduleName
} from "../../../ducks/cdek/index";
import { modulesIsLoadingSelector } from "../../../ducks/logger/index";
import Field from "../../../designSystem/molecules/Field/index";
import Select from "../../../designSystem/molecules/Select/index";
import memoizeOne from "memoize-one";
import PreLoader from "../PreLoader";
import { generateId, mapIdToSelectedIds } from "../../../utils";

class SearchCdekControl extends Component {
  static propTypes = {
    fetchCdekInfo: PropTypes.func.isRequired,
    value: PropTypes.string,
    cityName: PropTypes.string,
    info: PropTypes.shape({
      cities: PropTypes.array.isRequired,
      pointsByCity: PropTypes.object.isRequired
    }).isRequired,
    onChange: PropTypes.func.isRequired,
    requestParams: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isLoaded: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired
  };

  formId = generateId();

  state = {
    cityId: null,
    cityName: "",
    hideCityControl: false,
    pointId: null,
    isLoaded: false
  };

  static getDerivedStateFromProps(props, state) {
    const newState = {};

    if (props.isLoaded !== state.isLoaded) {
      newState.isLoaded = props.isLoaded;
    }

    if (props.isLoaded && !state.isLoaded) {
      if (props.cityName) {
        const city = props.info.cities.find(c => c.name === props.cityName);

        if (city) {
          newState.cityId = city.id;
          newState.hideCityControl = true;
        }
      }

      if (props.value) {
        const point = props.points.find(p => p.code === props.value);

        if (point) {
          newState.hideCityControl = false;
          newState.cityId = point.cityCode;
          newState.cityName = point.city;
          newState.pointId = point.code;
        }
      }
    }

    if (Object.keys(newState)) return newState;

    return null;
  }

  componentDidMount() {
    this.props.fetchCdekInfo(this.props.requestParams);
  }

  componentDidUpdate(prevProps, prevState) {
    const { pointId, cityId } = this.state;
    const { value, onChange, name, info } = this.props;

    if (pointId !== prevState.pointId && pointId !== value) {
      const point = info.pointsByCity[cityId]
        ? info.pointsByCity[cityId].find(p => p.code === pointId)
        : null;
      onChange(pointId, point ? point.fullAddress : "", name);
    }
  }

  handleChangeCity = ([value]) => {
    if (this.state.cityId !== value) {
      const city = this.props.info.cities.find(c => c.id === value);

      this.setState({
        cityId: value,
        cityName: city ? city.name : ""
      });
    }
  };

  handleSearchCity = value => {
    if (this.state.cityName !== value) {
      this.setState({
        cityName: value
      });
    }
  };

  handleChangePoint = ([value]) => {
    if (this.state.pointId !== value) {
      this.setState({
        pointId: value
      });
    }
  };

  getCities() {
    const { cityName } = this.state;
    const { info } = this.props;
    return !cityName
      ? info.cities
      : info.cities.filter(c =>
          c.name.toLowerCase().includes(cityName.toLowerCase())
        );
  }

  getSelectedCity = memoizeOne(mapIdToSelectedIds);
  getSelectedPoint = memoizeOne(mapIdToSelectedIds);

  render() {
    const { hideCityControl, cityId, pointId, cityName } = this.state;
    const { info, isLoading } = this.props;
    const points = info.pointsByCity[cityId] || [];

    return (
      <>
        {isLoading && <PreLoader viewType="content" />}
        {!hideCityControl && (
          <Field mb="10px" label="Выберите город">
            <Select
              key={cityId || info.cities.length}
              placeholder="Выберите город"
              name={`city_${this.formId}`}
              selected={this.getSelectedCity(cityId)}
              options={this.getCities()}
              onChange={this.handleChangeCity}
              searchMode="custom"
              defaultSearchValue={cityName}
              onChangeSearchValue={this.handleSearchCity}
            />
          </Field>
        )}
        <Field mb="10px" label="Введите точку выдачи">
          <Select
            key={pointId ? pointId : cityId || this.formId}
            disabled={!cityId}
            placeholder="Введите точку выдачи"
            name={`point_${this.formId}`}
            selected={this.getSelectedPoint(pointId)}
            options={points}
            onChange={this.handleChangePoint}
          />
        </Field>
      </>
    );
  }
}

const moduleNames = [moduleName];

function mapStateToProps(state, props) {
  return {
    info: cdekInfoSelector(state, props),
    points: pointsSelector(state, props),
    requestParams: requestParamsSelector(state, props),
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    isLoaded: isLoadedSelector(state, props)
  };
}

export default connect(
  mapStateToProps,
  { fetchCdekInfo }
)(SearchCdekControl);
