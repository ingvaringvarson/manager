import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import styled from "styled-components";

import { ContextForm } from "./context";

const ClearButton = styled.button`
  background: transparent;
  border: none;
  cursor: pointer;
  color: #336ea8;
`;

class ClearButtonContext extends PureComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    type: PropTypes.string,
    // здесь тип не уточняем, так как могут быть вложенные массивы
    fields: PropTypes.array.isRequired
  };

  static defaultProps = {
    type: "button"
  };

  static contextType = ContextForm;

  mapFieldsToKeys = memoizeOne(function getAllFieldsId(fields) {
    return fields
      .map(field =>
        Array.isArray(field) ? getAllFieldsId(field) : field && field.id
      )
      .filter(id => id)
      .flat();
  });

  handleClickButton = event => {
    event.preventDefault();
    const { typeProps = {} } = this.context;

    if (typeProps.clear && typeProps.clear.onClearValues) {
      const fieldsArray = Array.isArray(this.props.fields)
        ? this.props.fields
        : [this.props.fields];
      const keys = this.mapFieldsToKeys(fieldsArray);

      typeProps.clear.onClearValues(keys);
    }
  };

  render() {
    const { type, title, ...props } = this.props;

    return (
      <ClearButton {...props} type={type} onClick={this.handleClickButton}>
        {title}
      </ClearButton>
    );
  }
}

export default ClearButtonContext;
