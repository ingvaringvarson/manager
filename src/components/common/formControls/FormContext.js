import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { ContextForm } from "./context";
import styled from "styled-components";

const StyledForm = styled.form`
  border: none;
`;

const StyledInput = styled.input`
  display: none;
`;

class FormContext extends PureComponent {
  static propTypes = {
    children: PropTypes.any.isRequired
  };

  static contextType = ContextForm;

  render() {
    const { typeProps = {} } = this.context;
    return (
      <StyledForm {...typeProps.form}>
        {this.props.children}
        <StyledInput type="submit" />
      </StyledForm>
    );
  }
}

export default FormContext;
