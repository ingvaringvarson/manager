import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { space, width } from "styled-system";
import { ifProp } from "styled-tools";
import styled, { css } from "styled-components";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { ContextForm } from "./context";
import { generateId } from "../../../utils";
import memoizeOne from "memoize-one";

import Input from "../../../designSystem/atoms/Input";
import Field from "../../../designSystem/molecules/Field";
import Checkbox from "../../../designSystem/molecules/Checkbox";
import WrapInputDatePicker from "./WrapInputDatePicker";
import Select from "../../../designSystem/molecules/Select";
import SearchRequestControl from "../form/SearchRequestControl";
import SearchKladrControl from "../form/SearchKladrControl";
import SearchCdekControl from "../form/SearchCdekControl";
import Icon from "../../../designSystem/atoms/Icon";
import { IconClearWrap } from "../../common/styled/form";

import datePickerDefaultProps from "../../../constants/datepickerProps";

const StyledInput = styled(Input)`
  ${ifProp(
    "forIcon",
    css`
      padding-right: 34px;
    `
  )};
`;

const FieldWithIconWrapper = styled.div`
  display: inline-block;
  vertical-align: middle;
  ${width}
`;

const IconClear = styled(Icon)`
  width: 16px;
  height: 16px;
`;

const PositionedField = styled(Field)`
  position: relative;
  display: block;

  .react-datepicker__input-container {
    display: block;
  }
`;

const Spacer = styled.div(space);

class FieldContext extends PureComponent {
  static contextType = ContextForm;

  id = generateId();

  static propTypes = {
    name: PropTypes.string.isRequired,
    fieldStyle: PropTypes.object,
    helperFieldData: PropTypes.object
  };

  getSelected = memoizeOne(value =>
    value === "" || value === null ? [] : Array.isArray(value) ? value : [value]
  );

  getAllInputProps = (typeProps, formControlProps) => ({
    ...typeProps[formControlProps.type],
    ...formControlProps.attrs,
    iconClearProps: {
      ...(typeProps[formControlProps.type]
        ? typeProps[formControlProps.type].iconClearProps
        : null),
      ...formControlProps.attrs.iconClearProps,
      disabled: formControlProps.attrs.disabled
    }
  });

  renderCheckbox() {
    const {
      typeProps = {},
      fields = {},
      values = {},
      invalidFields = {}
    } = this.context;
    const { name } = this.props;
    const { formControlProps } = fields[name];
    const allInputProps = this.getAllInputProps(typeProps, formControlProps);
    const invalid = invalidFields[name] ? invalidFields[name].join(", ") : "";
    const notice = formControlProps.notice || "";
    const value = values.hasOwnProperty(name)
      ? values[name]
      : formControlProps.defaultValue;
    const text = formControlProps.label || "";
    const checked = formControlProps.checkedByValue.get(value);

    return (
      <Spacer my="14px">
        <Checkbox
          type="checkbox"
          name={name}
          checked={!!checked}
          id={this.id}
          invalid={invalid}
          notice={notice}
          {...allInputProps}
          value={value}
          text={text}
        />
      </Spacer>
    );
  }

  renderRadio() {
    const {
      typeProps = {},
      fields = {},
      values = {},
      invalidFields = {}
    } = this.context;
    const { name } = this.props;
    const { formControlProps } = fields[name];
    const allInputProps = this.getAllInputProps(typeProps, formControlProps);
    const invalid = invalidFields[name] ? invalidFields[name].join(", ") : "";
    const notice = formControlProps.notice || "";
    const commonName = formControlProps.commonName
      ? formControlProps.commonName
      : name;
    const value = values.hasOwnProperty(commonName)
      ? values[commonName]
      : formControlProps.defaultValue;
    const text = formControlProps.label || "";
    const checked = formControlProps.checkedByValue.get(value);

    return (
      <Checkbox
        type="radio"
        name={name}
        checked={!!checked}
        id={this.id}
        invalid={invalid}
        notice={notice}
        {...allInputProps}
        value={value}
        text={text}
      />
    );
  }

  renderSelectSearchRequest = (allInputProps, name, value, key) => (
    onChangeSearchValue,
    onToggleFocusSearchControl,
    options,
    isLoading
  ) => {
    return (
      <Select
        key={key}
        {...allInputProps}
        name={name}
        selected={this.getSelected(value)}
        options={options}
        searchMode="searchRequestControl"
        isLoading={isLoading}
        onChangeSearchValue={onChangeSearchValue}
        onToggleFocusSearchControl={onToggleFocusSearchControl}
      />
    );
  };

  renderType() {
    const { typeProps = {}, fields = {}, values = {} } = this.context;
    const { name } = this.props;
    const { formControlProps } = fields[name];
    const allInputProps = this.getAllInputProps(typeProps, formControlProps);
    const value = values.hasOwnProperty(name)
      ? values[name]
      : formControlProps.defaultValue;

    switch (formControlProps.type) {
      case "date": {
        const { onChange, iconClearProps, ...rest } = allInputProps;
        return (
          <>
            <DatePicker
              {...datePickerDefaultProps}
              {...formControlProps.datePickerProps}
              customInput={
                <WrapInputDatePicker
                  key={`date_${value}`}
                  allInputProps={rest}
                  value={value}
                  onChangeDate={onChange(name)}
                />
              }
              onChange={onChange(name)}
              selected={Date.parse(value)}
            />
            {iconClearProps && !!value && (
              <IconClearWrap name={name} {...iconClearProps}>
                <IconClear icon="clear" />
              </IconClearWrap>
            )}
          </>
        );
      }
      case "select":
        return (
          <Select
            key={formControlProps.key}
            {...allInputProps}
            name={name}
            selected={this.getSelected(value)}
            selectedAll={formControlProps.selectedAll}
            isMultiple={formControlProps.isMultiple}
            options={formControlProps.options}
          />
        );
      case "select_search_request":
        return (
          <SearchRequestControl
            selected={this.getSelected(value)}
            {...formControlProps.searchRequestProps}
          >
            {this.renderSelectSearchRequest(
              allInputProps,
              name,
              value,
              formControlProps.key
            )}
          </SearchRequestControl>
        );
      case "kladr":
        return (
          <SearchKladrControl name={name} {...allInputProps} value={value} />
        );
      case "cdek":
        return (
          <SearchCdekControl name={name} {...allInputProps} value={value} />
        );
      default:
        const { iconClearProps, ...rest } = allInputProps;

        return (
          <>
            <StyledInput
              type={formControlProps.type}
              {...rest}
              name={name}
              value={value}
              forIcon={!!value}
            />
            {iconClearProps && !!value && !formControlProps.attrs.disabled && (
              <IconClearWrap name={name} {...iconClearProps}>
                <IconClear icon="clear" />
              </IconClearWrap>
            )}
          </>
        );
    }
  }

  render() {
    const { fields = {}, invalidFields = {}, typeProps } = this.context;
    const { name, fieldStyle } = this.props;

    const field = fields[name];
    if (!fields[name]) return null;

    const { formControlProps } = field;
    const { Component, type } = formControlProps;
    const allInputProps = this.getAllInputProps(typeProps, formControlProps);

    switch (true) {
      case type === "checkbox": {
        return this.renderCheckbox();
      }
      case type === "radio": {
        return this.renderRadio();
      }
      case type === "custom":
        return <Component {...this.props} {...allInputProps} />;
      default: {
        const label = formControlProps.label || "";
        const invalid = invalidFields[name]
          ? invalidFields[name].map((s, i) => {
              return (
                <>
                  {s}
                  {i !== invalidFields[name].length - 1 && ", "}
                </>
              );
            })
          : "";
        const notice = formControlProps.notice || "";

        return (
          <Field
            {...fieldStyle}
            label={label}
            notice={invalid || notice}
            invalid={!!invalid}
          >
            {fields[name].formControlProps.renderIconAfter === null ? (
              <PositionedField>{this.renderType()}</PositionedField>
            ) : (
              <PositionedField>
                <FieldWithIconWrapper width="calc(100% - 24px)">
                  {this.renderType()}
                </FieldWithIconWrapper>
                <FieldWithIconWrapper width="24px">
                  {fields[name].formControlProps.renderIconAfter()}
                </FieldWithIconWrapper>
              </PositionedField>
            )}
          </Field>
        );
      }
    }
  }
}

export default FieldContext;
