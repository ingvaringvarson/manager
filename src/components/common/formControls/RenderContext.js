import React from "react";
import { ContextForm } from "./context";

export default props => (
  <ContextForm.Consumer>{props.children}</ContextForm.Consumer>
);
