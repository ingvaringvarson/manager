import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { ContextForm, DefaultState } from "./context";
import FormContext from "./FormContext";
import moment from "moment";
import { generateId, getValueInDepth, mapInvalidFields } from "../../../utils";

class ContainerContext extends PureComponent {
  static propTypes = {
    fields: PropTypes.arrayOf(PropTypes.any).isRequired,
    onSubmitForm: PropTypes.func,
    submitValidation: PropTypes.bool,
    children: PropTypes.any,
    externalForm: PropTypes.bool,
    onSort: PropTypes.func,
    // autosubmit works for select, datapicker and checkbox
    autoSubmit: PropTypes.bool,
    renderForm: PropTypes.func,
    handleStateChanged: PropTypes.func,
    onValuesChange: PropTypes.func,
    onlyActiveFields: PropTypes.bool
  };

  static defaultProps = {
    submitValidation: false,
    externalForm: false,
    autoSubmit: false,
    onlyActiveFields: false
  };

  constructor(props) {
    super(props);

    const values = this.props.fields.reduce(this.mapFieldsToValues, {
      triggerSubmitValue: null
    });
    const fields = this.props.fields.reduce(this.renderFields, {});
    const invalidFields = mapInvalidFields(values, fields);
    const isFormValid = Object.keys(invalidFields).length === 0;
    const stateKey = generateId();

    this.state = {
      ...DefaultState,
      enumerations: this.props.enumerations,
      helperFieldsData: this.props.helperFieldsData,
      fields,
      values,
      prevValues: values,
      stateKey,
      prevStateKey: stateKey,
      invalidFields,
      isFormValid,
      typeProps: {
        text: {
          onChange: this.handleChangeTextInput,
          onBlur: this.handleBlurTextInput,
          iconClearProps: {
            type: "button",
            onClick: this.handleClearValueField
          }
        },
        select: {
          onChange: this.handleChangeSelect,
          iconClearProps: {
            type: "button",
            onClick: this.handleClearValueField
          }
        },
        select_search_request: {
          onChange: this.handleChangeSelect,
          iconClearProps: {
            type: "button",
            onClick: this.handleClearValueField
          }
        },
        date: {
          onChange: this.handleChangeDate,
          iconClearProps: {
            type: "button",
            onClick: this.handleClearValueField
          }
        },
        checkbox: {
          onChange: this.handleChangeCheckbox
        },
        radio: {
          onChange: this.handleChangeRadio
        },
        form: {
          onSubmit: this.handleSubmitForm
        },
        sort: {
          onClick: this.handleChangeSort
        },
        textarea: {
          onChange: this.handleChangeTextInput,
          onBlur: this.handleBlurTextInput,
          iconClearProps: {
            type: "button",
            onClick: this.handleClearValueField
          }
        },
        kladr: {
          onChange: this.handleChangeKladr
        },
        cdek: {
          onChange: this.handleChangeCdek
        },
        clear: {
          onClearValues: this.handleClearValues
        },
        button: {
          onClick: this.handleClickButton
        },
        custom: {
          onChange: this.handleChangeCustom,
          iconClearProps: {
            type: "button",
            onClick: this.handleClearValueField
          }
        }
      }
    };
  }

  static getDerivedStateFromProps(props, state) {
    const newState = {};

    let newValues = state.values;
    let newFields = state.fields;
    if (state.values !== state.prevValues) {
      newFields = Object.keys(state.fields).reduce(
        (fields, key) => {
          if (state.fields[key].formControlProps.afterChange) {
            const { values, ...newField } = state.fields[
              key
            ].formControlProps.afterChange({
              field: fields[key],
              fields,
              values: newValues,
              prevValues: state.prevValues
            });

            if (values) {
              newValues = values;
            }

            return {
              ...fields,
              [key]: newField
            };
          }

          return { ...fields, [key]: state.fields[key] };
        },
        { ...newFields }
      );
      newValues = Object.keys(newValues).reduce((values, key) => {
        let value = newValues[key];

        if (value === state.prevValues[key]) {
          const twinFieldName = Object.keys(newFields).find(fieldName => {
            const currentField = newFields[fieldName];
            if (key === currentField.id) return false;
            return (
              (currentField.queryKeys.includes(key) ||
                (newFields[key] &&
                  newFields[key].queryKeys.includes(currentField.id))) &&
              state.values[currentField.id] !==
                state.prevValues[currentField.id]
            );
          });

          if (twinFieldName) value = state.values[twinFieldName];
        }

        value =
          newFields[key] && newFields[key].formControlProps.mask
            ? newFields[key].formControlProps.mask(value, state.values)
            : value;

        if (value !== state.prevValues[key]) {
          newFields[key].formControlProps.key = generateId();
        }

        return { ...values, [key]: value };
      }, {});

      newState.prevValues = newValues;
      newState.values = newValues;
      newState.fields = newFields;

      const newInvalidFields = mapInvalidFields(newValues, newFields);
      newState.isFormValid = Object.keys(newInvalidFields).length === 0;
    }

    if (state.stateKey !== state.prevStateKey || !state.isFormValid) {
      const newInvalidFields = mapInvalidFields(newValues, newFields);
      newState.invalidFields = newInvalidFields;
    }

    if (Object.keys(newState).length) return newState;

    return null;
  }

  renderFields = (copyFields, field) => {
    if (!Array.isArray(field)) {
      return {
        ...copyFields,
        [field.id]: {
          ...field,
          formControlProps: { ...field.formControlProps, key: generateId() }
        }
      };
    } else if (Array.isArray(field[1])) {
      return field[1].reduce(this.renderFields, copyFields);
    }

    return copyFields;
  };

  mapFieldsToValues = (values, field) => {
    if (Array.isArray(field)) {
      if (Array.isArray(field[1])) {
        return field[1].reduce(this.mapFieldsToValues, values);
      }

      return values;
    }

    const { value, defaultValue } = field.formControlProps;
    let fieldValue = value;

    if (value === null) {
      if (defaultValue) {
        fieldValue = defaultValue;
      } else {
        fieldValue = "";
      }
    }
    return {
      ...values,
      [field.id]: fieldValue
    };
  };

  handleClearValues = keys => {
    const { fields = {} } = this.state;
    const { autoSubmit } = this.props;
    const resetValues = {};

    keys.forEach(key => {
      if (!fields[key]) return;

      resetValues[key] = fields[key].formControlProps.defaultValue;
    });

    this.setState(prevState => {
      const { values } = prevState;
      const newState = { values: { ...values, ...resetValues } };

      if (autoSubmit) {
        newState.stateKey = generateId();
      }

      return newState;
    });
  };

  handleBlurTextInput = event => {
    const { fields = {}, invalidFields = {}, values = {} } = this.state;
    const { name } = event.target;

    if (
      fields[name] &&
      Array.isArray(fields[name].formControlProps.validators)
    ) {
      const invalidMsg = [];

      fields[name].formControlProps.validators.forEach(func => {
        const correctValue = fields[name].formControlProps.revertMask
          ? fields[name].formControlProps.revertMask(event.target.value, values)
          : event.target.value;
        const result = func(correctValue, values);
        if (result) invalidMsg.push(result);
      });

      if (
        !(
          !invalidMsg.length &&
          (!invalidFields[name] || !invalidFields[name].length)
        )
      ) {
        this.setState(prevState => {
          const prevInvalidFields = { ...prevState.invalidFields };
          if (!invalidMsg.length) {
            delete prevInvalidFields[name];
            return {
              invalidFields: prevInvalidFields
            };
          }

          return {
            invalidFields: { ...prevState.invalidFields, [name]: invalidMsg }
          };
        });
      }
    }
  };

  handleChangeTextInput = event => {
    event.preventDefault();
    const { name } = event.target;

    const value = event.target.value;

    this.setState(prevState => {
      const { values } = prevState;

      return {
        values: { ...values, [name]: value }
      };
    });
  };

  handleClickButton = event => {
    event.preventDefault();
    const { name } = event.currentTarget;

    const value = event.currentTarget.value;

    this.setState(prevState => {
      const { values } = prevState;

      return {
        values: { ...values, [name]: value }
      };
    });
  };

  handleChangeCustom = (value, name) => {
    this.setState(prevState => {
      const { values } = prevState;

      return {
        values: { ...values, [name]: value }
      };
    });
  };

  handleClearValueField = event => {
    event.preventDefault();

    const { name } = event.currentTarget;
    const { fields = {} } = this.state;
    const { defaultValue } = fields[name].formControlProps;

    this.setState(prevState => {
      const newValues = {
        ...prevState.values,
        [name]: defaultValue
      };
      const newInvalidFields = mapInvalidFields(newValues, fields);
      return {
        values: newValues,
        invalidFields: newInvalidFields,
        stateKey: this.props.autoSubmit ? generateId() : prevState.stateKey
      };
    });
  };

  handleChangeCheckbox = event => {
    const { name, checked } = event.target;
    const { fields = {} } = this.state;
    const { valuesByChecked } = fields[name].formControlProps;
    const value = valuesByChecked.get(checked);

    const newState = {
      ...this.state,
      values: {
        ...this.state.values,
        [name]: value
      }
    };

    if (this.props.autoSubmit) {
      newState.stateKey = generateId();
    }

    this.setState(newState);
  };

  handleChangeRadio = event => {
    const { name, checked } = event.target;
    const { fields = {} } = this.state;
    const { valuesByChecked, commonName } = fields[name].formControlProps;
    const value = valuesByChecked.get(checked);

    const newState = {
      ...this.state,
      values: {
        ...this.state.values,
        [commonName ? commonName : name]: value
      }
    };

    if (this.props.autoSubmit) {
      newState.stateKey = generateId();
    }

    this.setState(newState);
  };

  handleChangeSelect = (value, name, options) => {
    const { fields } = this.state;
    const autoSubmitDrop = fields[name]
      ? fields[name].formControlProps.autoSubmitDrop
      : false;

    const newState = {
      ...this.state,
      values: {
        ...this.state.values,
        [name]: value
      },
      fields: {
        ...this.state.fields,
        [name]: {
          ...this.state.fields[name],
          formControlProps: {
            ...this.state.fields[name].formControlProps,
            options
          }
        }
      }
    };

    if (this.props.autoSubmit && !autoSubmitDrop) {
      newState.stateKey = generateId();
    }

    this.setState(newState);
  };

  handleChangeSort = (event, name, sortBy) => {
    const { values } = this.state;

    let value = `${name} desc`;

    if (sortBy) {
      value = `${name} ${sortBy}`;
    } else if (values.sort_by) {
      const selectedSortArr = String(values.sort_by).split(" ");
      if (selectedSortArr[0] === name && selectedSortArr[1] === "desc") {
        value = `${name} asc`;
      }
    }

    if (this.props.onSort) {
      this.props.onSort(
        { sort_by: value },
        this.state.fields,
        this.mapValues(this.state.values, this.state.fields)
      );
    } else {
      this.setState(prevState => {
        const { values } = prevState;

        return {
          values: { ...values, sort_by: value }
        };
      });
    }
  };

  handleChangeDate = name => value => {
    const date = value ? moment(value).format("YYYY-MM-DDTHH:mm:ss") : "";
    const newState = {
      values: {
        ...this.state.values,
        [name]: date
      }
    };

    if (this.props.autoSubmit) {
      newState.stateKey = generateId();
    }

    this.setState({ ...newState });
  };

  handleSubmitForm = event => {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    const value = getValueInDepth(event, ["currentTarget", "value"]);
    const stateKey = generateId();

    this.setState(prevState => {
      const { values } = prevState;

      return {
        stateKey,
        values: { ...values, triggerSubmitValue: value }
      };
    });
  };

  handleChangeKladr = (kladrId, locationInfo) => {
    this.setState(prevState => {
      const { values } = prevState;
      const prevKladrId = values["kladr"];
      const copyValues = { ...values };
      let location = "";

      if (locationInfo) {
        if (locationInfo.city) {
          location = location
            ? `${location}, ${locationInfo.postal_code}, ${locationInfo.city}`
            : `${locationInfo.postal_code}, ${locationInfo.city}`;
        }
        if (locationInfo.street) {
          location = location
            ? `${location}, ${locationInfo.street_with_type}`
            : `${locationInfo.street_with_type}`;
        }
        if (locationInfo.house) {
          location = location
            ? `${location}, ${locationInfo.house}`
            : `${locationInfo.house}`;
        }
        copyValues.location = location;
      }

      const newState = {
        values: { ...copyValues, kladr: kladrId }
      };

      if (this.props.autoSubmit && kladrId && prevKladrId !== kladrId) {
        newState.stateKey = generateId();
      }

      return newState;
    });
  };

  handleChangeCdek = (pointId, location_cdek, name) => {
    this.setState(prevState => {
      const { values } = prevState;
      const prevPointId = values[name];
      const newState = {
        values: { ...values, location_cdek, [name]: pointId || "" }
      };

      if (this.props.autoSubmit && prevPointId !== pointId) {
        newState.stateKey = generateId();
      }

      return newState;
    });
  };

  mapValues = (values, fields) => {
    return Object.keys(values).reduce((copyValues, key) => {
      const copyValue = values[key];

      switch (true) {
        case !fields[key] ||
          !!(this.props.onlyActiveFields && !fields[key].isActive):
          return copyValues;
        case (Array.isArray(copyValue) && !copyValue.length) ||
          copyValue === "":
          return { ...copyValues, [key]: null };
        case Array.isArray(copyValue) &&
          !fields[key].formControlProps.isMultiple:
          return { ...copyValues, [key]: copyValue[0] };
        default:
          return { ...copyValues, [key]: copyValue };
      }
    }, {});
  };

  componentDidUpdate(prevProps, prevState) {
    const { invalidFields, stateKey, values, fields } = this.state;
    const { submitValidation, onSubmitForm } = this.props;
    const fieldsIsValid = !Object.keys(invalidFields).length;

    if (
      (!submitValidation || (submitValidation && fieldsIsValid)) &&
      prevState.stateKey !== stateKey
    ) {
      onSubmitForm(this.mapValues(values, fields), fields, this.state);
    }

    if (prevState !== this.state && this.props.handleStateChanged) {
      this.props.handleStateChanged(this.state);
    }

    if (
      values !== prevState.values &&
      typeof this.props.onValuesChange === "function"
    ) {
      this.props.onValuesChange(values, fields, this.state);
    }
  }

  render() {
    return (
      <ContextForm.Provider value={this.state}>
        {this.props.externalForm ? (
          this.props.renderForm ? (
            this.props.renderForm(this.state)
          ) : (
            this.props.children
          )
        ) : (
          <FormContext>
            {this.props.renderForm
              ? this.props.renderForm(this.state)
              : this.props.children}
          </FormContext>
        )}
      </ContextForm.Provider>
    );
  }
}

export default ContainerContext;
