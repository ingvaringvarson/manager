import React from "react";

export const DefaultState = {
  fields: {},
  invalidFields: {},
  values: {},
  validatorFields: {},
  typeProps: {},
  isFormValid: true
};

export const ContextForm = React.createContext(DefaultState);
