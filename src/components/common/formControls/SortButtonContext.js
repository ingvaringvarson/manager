import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Text from "../../../designSystem/atoms/Text";
import SortButton from "../../../designSystem/molecules/SortButton";
import { ContextForm } from "./context";
import { isType } from "../../../utils";

class SortButtonContext extends PureComponent {
  static propTypes = {
    name: PropTypes.string.isRequired,
    sortButtonStyle: PropTypes.object,
    title: PropTypes.string,
    sortName: PropTypes.string
  };

  static contextType = ContextForm;

  handleClickSortDesc = event => {
    event.preventDefault();
    event.stopPropagation();
    const { typeProps = {} } = this.context;

    typeProps.sort.onClick(
      event,
      this.props.sortName || this.props.name,
      "desc"
    );
  };

  handleClickSortAsc = event => {
    event.preventDefault();
    event.stopPropagation();
    const { typeProps = {} } = this.context;

    typeProps.sort.onClick(
      event,
      this.props.sortName || this.props.name,
      "asc"
    );
  };

  handleClickTitle = event => {
    event.preventDefault();
    event.stopPropagation();
    const { typeProps = {} } = this.context;

    typeProps.sort.onClick(event, this.props.sortName || this.props.name);
  };

  iconUpProps = {
    onClick: this.handleClickSortDesc
  };

  iconDownProps = {
    onClick: this.handleClickSortAsc
  };

  getSortValue = (valueByForm, name) => {
    if (!isType(valueByForm, "string")) return "";

    const splitValue = valueByForm.split(" ");

    return name === splitValue[0] && splitValue[1] ? splitValue[1] : "";
  };

  render() {
    const { fields = {}, values = {} } = this.context;
    const { name, sortName, sortButtonStyle } = this.props;

    if (!fields[name]) return null;

    const activeSortBy = this.getSortValue(values["sort_by"], sortName || name);

    return (
      <SortButton
        type="button"
        {...sortButtonStyle}
        onClick={this.handleClickTitle}
        iconUpProps={this.iconUpProps}
        iconDownProps={this.iconDownProps}
        activeSortBy={activeSortBy}
      >
        <Text>{this.props.title}</Text>
      </SortButton>
    );
  }
}

export default SortButtonContext;
