import { isValidDate } from "./../../../utils";
import React, { Component } from "react";
import Input from "../../../designSystem/atoms/Input";
import {
  maskDatePicker,
  maskDatePickerRevertMonthDay
} from "../../../helpers/masks";

class WrapInputDatePicker extends Component {
  state = {
    value: this.props.value
  };

  handleChangeValue = event => {
    event.preventDefault();

    const value = maskDatePicker(event.target.value);

    if (value !== this.state.value) {
      this.setState({ value });
    }

    if (value.length === 10) {
      const invertedDate = maskDatePickerRevertMonthDay(value);

      if (isValidDate(invertedDate)) {
        this.props.onChangeDate(invertedDate);
      }
    }
  };

  render() {
    const { elemStyle, allInputProps, ...rest } = this.props;

    return (
      <Input
        {...rest}
        {...elemStyle}
        {...allInputProps}
        value={this.state.value}
        onChange={this.handleChangeValue}
      />
    );
  }
}

export default WrapInputDatePicker;
