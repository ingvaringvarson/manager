import React, { Component } from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import Item from "../../../designSystem/mixins/Item";
import Text from "../../../designSystem/atoms/Text";
import { ifProp, palette } from "styled-tools";
import { space } from "styled-system";

const StyledText = styled(Text)`
  padding-right: 30px;
  padding-left: 12px;
  overflow: hidden;
  line-height: 24px;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

const Wrap = styled.div`
  ${Item};
  padding: 3px 0;
  cursor: pointer;

  ${ifProp(
    "withBorder",
    css`
      margin-bottom: 10px;
      background: #ffffff;
      border: 1px solid ${palette("grayscale", 1)};
      border-radius: 5px;
    `
  )}

    ${ifProp(
      "textMode",
      css`
        cursor: normal;

        ${StyledText} {
          white-space: normal;
        }
      `
    )}

  ${ifProp(
    "active",
    css`
      background: rgba(241, 241, 241, 0.3);
      border-left: 2px solid #eb0028;
    `
  )}

  ${space}
`;

class ListItem extends Component {
  static propTypes = {
    children: PropTypes.any,
    active: PropTypes.bool,
    withBorder: PropTypes.bool,
    onClick: PropTypes.func,
    textMode: PropTypes.bool
  };

  handleClick = () => {
    if (this.props.onClick) {
      this.props.onClick(this.props);
    }
  };

  render() {
    const { children, ...rest } = this.props;
    return (
      <Wrap {...rest} onClick={this.handleClick}>
        <StyledText>{children}</StyledText>
      </Wrap>
    );
  }
}

export default ListItem;
