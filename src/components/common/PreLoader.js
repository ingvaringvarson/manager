import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { switchProp } from "styled-tools";

import Spinner from "../../designSystem/atoms/Spinner";

const Overlay = styled.div`
  position: fixed;
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
  z-index: 9999;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: rgba(255, 255, 255, 0.8);

  ${switchProp("viewType", {
    content: css`
      position: absolute;
    `
  })}
`;

const propTypes = {
  viewType: PropTypes.oneOf(["content"]),
  size: PropTypes.string
};

const PreLoader = ({ size, viewType, ...rest }) => {
  return (
    <Overlay viewType={viewType} {...rest}>
      <Spinner size={size} />
    </Overlay>
  );
};

PreLoader.propTypes = propTypes;

export default PreLoader;
