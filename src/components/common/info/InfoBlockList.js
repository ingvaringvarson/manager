import React, { Fragment } from "react";
import PropTypes from "prop-types";
import {
  ListDesc,
  ListRow,
  ListTitle
} from "../../../designSystem/organisms/List";
import styled from "styled-components";
import Icon from "../../../designSystem/atoms/Icon";
import { List } from "../../../designSystem/organisms/List";
import InfoRow from "./InfoRow";

const ListBorder = styled(List)`
  position: relative;
  &:not(:last-of-type) {
    border-bottom: 1px solid #e4e4e4;
    margin-bottom: 16px;
    padding-bottom: 16px;
  }
`;

export const IconMarkControl = styled(Icon)`
  position: absolute;
  top: 0;
  right: 0;
  width: 24px;
  cursor: pointer;
`;

const Mark = styled.span`
  display: inline-block;
  border: 1px solid #e0e0e0;
  border-radius: 5px;
  padding: 3px 5px;
  margin: 3px 5px;
  transform: translate(-5px, -3px);
`;

const propTypes = {
  fields: PropTypes.array,
  iconIsShow: PropTypes.bool,
  blockProps: PropTypes.object,
  iconControl: PropTypes.shape({
    onClick: PropTypes.func,
    width: PropTypes.string,
    height: PropTypes.string
  }),
  entity: PropTypes.object,
  enumerations: PropTypes.object,
  restHelperData: PropTypes.object
};

const InfoBlockList = React.memo(function InfoBlockList(props) {
  const {
    fields,
    iconControl,
    iconIsShow,
    blockProps,
    entity,
    enumerations,
    restHelperData
  } = props;

  const listProps = iconControl ? blockProps : null;

  return (
    <ListBorder {...listProps}>
      {!!iconControl && iconIsShow && (
        <IconMarkControl icon="edit" {...iconControl} />
      )}
      {fields.map(field => {
        if (field.renderRow) {
          return field.renderRow({ ...props, field, entity });
        }
        if (
          !field.renderValue &&
          (field.value === "" ||
            field.value === null ||
            field.value === undefined)
        ) {
          return null;
        }
        let value = field.value;

        if (Array.isArray(field.value) && !field.renderValue) {
          if (Array.isArray(field.value[0])) {
            return (
              <Fragment key={field.id}>
                {field.value.reduce((arr, group) => {
                  if (!Array.isArray(group[1])) return arr;

                  const rows = group[1].map(entity => {
                    return <InfoRow key={entity[0]} info={entity[1]} />;
                  });
                  return arr.concat(rows);
                }, [])}
              </Fragment>
            );
          } else {
            value = field.value.map((item, itemIndex) => (
              <Mark key={itemIndex}>{item}</Mark>
            ));
          }
        }

        return (
          <ListRow key={field.id}>
            <ListTitle>
              {field.renderTitle
                ? field.renderTitle(field, entity, enumerations, restHelperData)
                : field.title}
            </ListTitle>
            <ListDesc>
              {field.renderValue
                ? field.renderValue(field, entity, enumerations, restHelperData)
                : value}
            </ListDesc>
          </ListRow>
        );
      })}
    </ListBorder>
  );
});

InfoBlockList.propTypes = propTypes;

export default InfoBlockList;
