import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Text from "../../../designSystem/atoms/Text";
import Block from "../../../designSystem/organisms/Block";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import Heading from "../../../designSystem/molecules/Heading";
import InfoBlockList from "./InfoBlockList";
import InfoBlockNested from "./InfoBlockNested";
import InfoRow from "./InfoRow";

const InfoRowWithBRow = styled(InfoRow)`
  border-bottom: 1px solid #e0e0e0;
`;

class InfoBlock extends PureComponent {
  static propTypes = {
    iconIsShow: PropTypes.bool,
    info: PropTypes.object.isRequired,
    iconProps: PropTypes.object,
    blockProps: PropTypes.object.isRequired,
    entity: PropTypes.object,
    restHelperValues: PropTypes.object,
    enumerations: PropTypes.object,
    hasOwnContentView: PropTypes.bool,
    renderContent: PropTypes.func
  };

  static defaultProps = {
    iconProps: {},
    blockProps: {},
    iconIsShow: false,
    hasOwnContentView: false
  };

  getHeadingProps = () => {
    const { info, iconProps, iconIsShow } = this.props;
    const { title, subtitle, formMain } = info;
    const icon = iconIsShow && formMain ? formMain.icon : "";
    return { title, subtitle, icon, iconProps };
  };

  renderEmptyData = () => {
    return <Text>Нет данных</Text>;
  };

  renderDefaultView() {
    const {
      info: { infoFields },
      enumerations,
      entity,
      restHelperValues
    } = this.props;

    if (!infoFields.some(field => field.value !== null)) {
      return this.renderEmptyData();
    }

    return (
      <InfoBlockList
        {...this.props}
        fields={infoFields}
        enumerations={enumerations}
        entity={entity}
        restHelperValues={restHelperValues}
      />
    );
  }

  renderListView() {
    const {
      info,
      blockProps: { onSubmitForm }
    } = this.props;

    if (!info.infoFields.length) {
      return this.renderEmptyData();
    }

    return info.infoFields.map(entity => {
      if (info.formField) {
        return (
          <InfoBlockNested
            onSubmitForm={onSubmitForm}
            info={info}
            id={entity[0]}
            fields={entity[1]}
            key={entity[0]}
          />
        );
      }

      return <InfoBlockList key={entity[0]} fields={entity[1]} />;
    });
  }

  renderGroupedListView() {
    const {
      info,
      blockProps: { onSubmitForm }
    } = this.props;

    if (!info.infoFields.length) {
      return this.renderEmptyData();
    }

    return info.infoFields.reduce((groups, group) => {
      return groups.concat(
        group[1].map(entity => {
          if (info.formField) {
            return (
              <InfoBlockNested
                onSubmitForm={onSubmitForm}
                info={info}
                id={entity[0]}
                fields={entity[1]}
                key={entity[0]}
              />
            );
          }

          return (
            <InfoRow
              key={entity[0]}
              info={entity[1]}
              entity={this.props.entity}
              restHelperValues={this.props.restHelperValues}
            />
          );
        })
      );
    }, []);
  }

  renderDividedList = () => {
    /**
     * expecting model:
     *  info = {
     *    devidingFields: [
     *      [{id: '', value: '', title: ''}],
     *      [{id: '', value: '', title: ''}]
     *    ]
     *  }
     */
    const { info } = this.props;

    if (!(info.devidingFields && info.devidingFields.length)) {
      return this.renderEmptyData();
    }

    return info.devidingFields.reduce(
      (allResult, currentFieldBlock, blockIndex) => {
        currentFieldBlock.map((field, fieldIndex) => {
          if (
            blockIndex < info.devidingFields.length - 1 &&
            fieldIndex === currentFieldBlock.length - 1
          ) {
            allResult.push(
              <InfoRowWithBRow
                key={`${blockIndex} ${fieldIndex}`}
                info={field}
              />
            );
          } else {
            allResult.push(
              <InfoRow key={`${blockIndex} ${fieldIndex}`} info={field} />
            );
          }
        });
        return allResult;
      },
      []
    );
  };

  renderContent() {
    const {
      info: { view }
    } = this.props;

    if (this.props.hasOwnContentView && this.props.renderContent) {
      return this.props.renderContent(this.props.info);
    }

    switch (view) {
      case "list":
        return this.renderListView();
      case "groupedList":
        return this.renderGroupedListView();
      // нигде не используется пока?
      case "dividedList":
        return this.renderDividedList();
      default:
        return this.renderDefaultView();
    }
  }

  render() {
    const { info, blockProps } = this.props;

    if (!info) return null;

    const { helpersData, renderTitle } = info;

    const { subtitle, title, icon, iconProps } = this.getHeadingProps();

    return (
      <Block mb="middle" {...blockProps}>
        {title && renderTitle ? (
          renderTitle(helpersData, iconProps)
        ) : (
          <Heading
            subtitle={subtitle}
            title={title}
            icon={icon}
            iconProps={iconProps}
          />
        )}
        <BlockContent>{this.renderContent()}</BlockContent>
      </Block>
    );
  }
}

export default InfoBlock;
