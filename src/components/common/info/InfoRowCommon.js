import React, { memo } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import {
  ListDesc,
  ListRow,
  ListTitle
} from "../../../designSystem/organisms/List";

export const ListDescWithIcon = styled(ListDesc)`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const propTypes = {
  title: PropTypes.string,
  value: PropTypes.string,
  icon: PropTypes.element,
  tooltip: PropTypes.element
};

const InfoRowCommon = memo(props => {
  const { tooltip, value, title, icon } = props;
  return (
    <>
      <ListRow>
        <ListTitle>{title}</ListTitle>
        <ListDescWithIcon>
          {value}
          {icon && <div>{icon}</div>}
        </ListDescWithIcon>
      </ListRow>
      {tooltip}
    </>
  );
});

InfoRowCommon.propTypes = propTypes;

export default InfoRowCommon;
