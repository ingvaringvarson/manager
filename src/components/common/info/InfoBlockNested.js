import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import InfoBlockList from "./InfoBlockList";
import InfoRow from "./InfoRow";
import InfoModalControl from "./InfoModalControl";

class InfoBlockNested extends PureComponent {
  static propTypes = {
    onSubmitForm: PropTypes.func.isRequired,
    info: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    fields: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired
  };

  getFormFields = memoizeOne((formFields, id) => {
    const fields = formFields.find(entity => entity[0] === id);

    return fields ? fields[1] : [];
  });

  renderContent = (
    state = null,
    modalProps = null,
    blockProps = null,
    iconProps = null
  ) => {
    const {
      fields,
      info: { view }
    } = this.props;

    if (view === "list") {
      return (
        <InfoBlockList
          blockProps={blockProps}
          iconIsShow={state.iconIsShow}
          iconControl={iconProps}
          fields={fields}
        />
      );
    } else if (view === "groupedList") {
      return <InfoRow iconControl={iconProps} info={fields} />;
    }

    return null;
  };

  render() {
    const {
      info: { formField, formFields },
      id,
      onSubmitForm
    } = this.props;

    const fields = this.getFormFields(formFields, id);

    return (
      <InfoModalControl
        formProps={formField}
        onSubmitForm={onSubmitForm}
        fields={fields}
      >
        {this.renderContent}
      </InfoModalControl>
    );
  }
}

export default InfoBlockNested;
