import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import ReactModal from "react-modal";

import Block from "../../../designSystem/organisms/Block";
import Heading from "../../../designSystem/molecules/Heading";
import EditClientForm from "../../forms/EditClientForm";
import { customStyles as defaultStyles } from "../../../constants/defaultStyles/reactModalStyles";

const customStyles = {
  ...defaultStyles,
  content: {
    ...defaultStyles.content,
    width: "380px",
    overflow: "visible"
  }
};

class InfoModalControl extends PureComponent {
  static propTypes = {
    children: PropTypes.func.isRequired,
    fields: PropTypes.array.isRequired,
    onSubmitForm: PropTypes.func.isRequired,
    formProps: PropTypes.object.isRequired,
    btnProps: PropTypes.object
  };

  static defaultProps = {
    valuesForFields: {},
    btnProps: {}
  };

  constructor(props) {
    super(props);

    this.state = {
      modalIsOpen: false,
      iconIsShow: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (state.modalIsOpen && state.iconIsShow) {
      return {
        iconIsShow: false
      };
    }

    return null;
  }

  handleCloseModal = () => {
    this.setState({ modalIsOpen: false });
  };

  handleOpenModal = () => {
    this.setState({ modalIsOpen: true });
  };

  handleToggleStateModal = () => {
    this.setState(prevState => ({
      modalIsOpen: !prevState.modalIsOpen
    }));
  };

  handleShowIcon = () => {
    if (!this.state.iconIsShow) this.setState({ iconIsShow: true });
  };

  handleHideIcon = () => {
    if (this.state.iconIsShow) this.setState({ iconIsShow: false });
  };

  iconProps = {
    onClick: this.handleToggleStateModal,
    width: "24px",
    height: "24px"
  };

  getBlockProps(onSubmitForm) {
    return {
      onSubmitForm,
      onMouseEnter: this.handleShowIcon,
      onMouseLeave: this.handleHideIcon
    };
  }

  modalProps = {
    onOpen: this.handleOpenModal,
    onClose: this.handleCloseModal
  };

  render() {
    const { fields, onSubmitForm, formProps, btnProps } = this.props;
    const blockProps = this.getBlockProps(onSubmitForm);
    return (
      <>
        {this.props.children(
          this.state,
          this.modalProps,
          blockProps,
          this.iconProps
        )}
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.handleCloseModal}
          style={customStyles}
        >
          <Block>
            <Heading
              subtitle={formProps.subtitle}
              title={formProps.title}
              icon="close"
              iconProps={this.iconProps}
            />
            <EditClientForm
              fields={fields}
              onSubmitForm={onSubmitForm}
              typeForm={formProps.type}
              btnProps={btnProps}
            />
          </Block>
        </ReactModal>
      </>
    );
  }
}

export default InfoModalControl;
