import React from "react";
import PropTypes from "prop-types";
import {
  ListDesc,
  ListRow,
  ListTitle
} from "../../../designSystem/organisms/List";
import styled from "styled-components";
import Icon from "../../../designSystem/atoms/Icon";
import ReactTooltip from "react-tooltip";

export const IconMark = styled(Icon)`
  width: 24px;
`;

export const IconMarkControl = styled(Icon)`
  width: 24px;
  opacity: 0;
  cursor: pointer;
  transition: opacity 0.2s;
`;

export const ListDescWithIcon = styled(ListDesc)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  word-break: break-all;

  :hover {
    ${IconMarkControl} {
      opacity: 1;
    }
  }
`;

const propTypes = {
  iconControl: PropTypes.shape({
    onClick: PropTypes.func,
    width: PropTypes.string,
    height: PropTypes.string
  }),
  info: PropTypes.object,
  entity: PropTypes.object,
  restHelperValues: PropTypes.object
};

const InfoRow = React.memo(function InfoRow(props) {
  const { iconControl, info, entity, restHelperValues } = props;
  const icon = [];
  const tooltip = [];

  if (info.hasOwnProperty("is_active") && !info.is_active) {
    icon.push(
      <IconMark
        key={`mobile-off_${info._id}`}
        data-tip="tooltip"
        data-for={`mobile-off_${info._id}`}
        icon="mobile-off"
        width="24px"
        height="24px"
      />
    );
    tooltip.push(
      <ReactTooltip
        id={`mobile-off_${info._id}`}
        key={`mobile-off_${info._id}`}
      >
        Неактивный контакт
      </ReactTooltip>
    );
  } else if (info.hasOwnProperty("is_main") && info.is_main) {
    icon.push(
      <IconMark
        key={`done_${info._id}`}
        data-tip="tooltip"
        data-for={`done_${info._id}`}
        icon="done"
        width="24px"
        height="24px"
      />
    );
    tooltip.push(
      <ReactTooltip id={`done_${info._id}`} key={info._id}>
        Основной контакт
      </ReactTooltip>
    );
  }

  if (iconControl) {
    icon.push(
      <IconMarkControl
        icon="edit"
        key={`control_${info._id}`}
        {...iconControl}
      />
    );
  }

  return (
    <>
      <ListRow>
        <ListTitle>
          {info.renderTitle
            ? info.renderTitle(info, entity, restHelperValues)
            : info.title}
        </ListTitle>
        <ListDescWithIcon>
          {info.renderValue
            ? info.renderValue(info, entity, restHelperValues)
            : info.value}{" "}
          {!!icon.length && <div>{icon}</div>}
        </ListDescWithIcon>
      </ListRow>
      {tooltip}
    </>
  );
});

InfoRow.propTypes = propTypes;

export default InfoRow;
