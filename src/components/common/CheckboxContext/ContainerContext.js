import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Context, DefaultState } from "./context";

class ContainerContext extends PureComponent {
  static propTypes = {
    entities: PropTypes.array
  };

  constructor(props) {
    super(props);

    this.state = {
      ...DefaultState,
      entities: this.props.entities,
      handleSelect: this.handleSelect,
      handleSelectAll: this.handleSelectAll
    };
  }

  handleSelectAll = () => {
    this.setState(prevState => {
      return {
        selected: prevState.entities.map(entity => entity.id)
      };
    });
  };

  handleSelect = entity => {
    this.setState(prevState => {
      const elInSelected = this.state.selected.find(el => el === entity.id);
      const selectedCopy = [...prevState.selected];
      if (elInSelected) {
        selectedCopy.splice(selectedCopy.indexOf(elInSelected), 1);
      } else {
        selectedCopy.push(entity.id);
      }
      return {
        selected: selectedCopy
      };
    });
  };

  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    );
  }
}

export default ContainerContext;
