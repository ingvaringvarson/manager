import { createContext } from "react";

export const DefaultState = {
  selected: [],
  isAllSelected: false
};

export const Context = createContext(DefaultState);
