import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import ReactModal from "react-modal";
import Heading from "../../../designSystem/molecules/Heading";
import Block from "../../../designSystem/organisms/Block";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import { customStyles } from "../../../constants/defaultStyles/reactModalStyles";

class CommonModal extends PureComponent {
  static propTypes = {
    handleClose: PropTypes.func.isRequired,
    isOpen: PropTypes.bool,
    contentStyles: PropTypes.object,
    title: PropTypes.string,
    subtitle: PropTypes.string,
    children: PropTypes.any
  };

  customStyles = {
    ...customStyles,
    content: {
      ...customStyles.content,
      width: "400px",
      overflow: "visible",
      ...this.props.contentStyles
    }
  };

  iconProps = {
    height: "24px",
    width: "24px",
    onClick: this.props.handleClose
  };

  render() {
    return (
      <ReactModal
        ariaHideApp={false}
        style={this.customStyles}
        onRequestClose={this.props.handleClose}
        isOpen={this.props.isOpen}
      >
        <Block>
          <Heading
            title={this.props.title}
            subtitle={this.props.subtitle}
            icon="close"
            iconProps={this.iconProps}
          />
          <BlockContent>{this.props.children}</BlockContent>
        </Block>
      </ReactModal>
    );
  }
}

export default CommonModal;
