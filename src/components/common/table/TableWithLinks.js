import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { Table } from "../tableView";
import { routesToMove } from "../../../helpers/routesGenerate";
import memoizeOne from "memoize-one";
import { propTypes } from "../tableView/tablePropTypes";

class TableWithLinks extends PureComponent {
  static propTypes = {
    ...propTypes,
    typeEntity: PropTypes.string
  };

  mapRowBodyHandlers = memoizeOne(rowBodyHandlers => {
    return {
      onMouseDown: ({ entity }) => event => {
        if (
          (event.button === 1 || event.metaKey) &&
          this.props.typeEntity in routesToMove
        ) {
          const path = routesToMove[this.props.typeEntity](entity);
          window.open(path, "_blank");
        }
      },
      onClick: ({ entity }) => () => {
        if (this.props.typeEntity in routesToMove) {
          this.props.push(routesToMove[this.props.typeEntity](entity));
        }
      },
      ...rowBodyHandlers
    };
  });

  render() {
    return (
      <Table
        {...this.props}
        rowBodyHandlers={this.mapRowBodyHandlers(this.props.rowBodyHandlers)}
      />
    );
  }
}

export default connect(
  null,
  { push }
)(TableWithLinks);
