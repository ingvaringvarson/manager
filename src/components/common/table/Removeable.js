import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import memoizeOne from "memoize-one";
import { generateId } from "../../../utils";
import { Table, RowBody } from "../tableView";
import Icon from "../../../designSystem/atoms/Icon";

const StyledIcon = styled(Icon)`
  cursor: pointer;
`;

const ReturnButton = styled.div`
  text-align: right;
  font-size: 16px;
  padding: 5px 0;
  cursor: pointer;
  color: #4cace8;
`;

class Removeable extends PureComponent {
  static propTypes = {
    removedIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    entities: PropTypes.arrayOf(PropTypes.object).isRequired,
    handleRemovedChange: PropTypes.func.isRequired,
    cells: PropTypes.array.isRequired,
    enumerations: PropTypes.object
  };

  handleRowRemove = entity => {
    this.props.handleRemovedChange(this.props.removedIds.concat(entity._id));
  };

  handleReturnRow = entity => () => {
    const array = [...this.props.removedIds];
    const _id = array.indexOf(entity._id);
    array.splice(_id, 1);

    this.props.handleRemovedChange(array);
  };

  renderRowBodyChildrenAfter = ({ entity }) => {
    return (
      <StyledIcon icon="close" onClick={() => this.handleRowRemove(entity)} />
    );
  };

  renderTableBody = (entities, _1, _2, rowBodyProps) => {
    return entities.map(entity => {
      if (this.props.removedIds.some(id => id === entity._id)) {
        return (
          <ReturnButton key={entity._id} onClick={this.handleReturnRow(entity)}>
            Вернуть позицию
          </ReturnButton>
        );
      }
      return <RowBody {...rowBodyProps} key={entity._id} entity={entity} />;
    });
  };

  renderKey = memoizeOne(() => generateId());

  render() {
    const { cells, entities, removedIds, enumerations } = this.props;

    if (entities.length === 0) {
      return null;
    }

    return (
      <Table
        key={this.renderKey(removedIds, entities, enumerations)}
        hasExternalBody
        hasChildrenAfter
        cells={cells}
        entities={entities}
        enumerations={enumerations}
        renderBody={this.renderTableBody}
        renderRowBodyChildrenAfter={this.renderRowBodyChildrenAfter}
      />
    );
  }
}

export default Removeable;
