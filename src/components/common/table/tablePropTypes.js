import PropTypes from "prop-types";

export const propTypes = {
  entities: PropTypes.arrayOf(PropTypes.object).isRequired,
  cells: PropTypes.arrayOf(PropTypes.object).isRequired,
  hasFilters: PropTypes.bool,
  renderNestedTable: PropTypes.func,
  renderOptionControl: PropTypes.func,
  enumerations: PropTypes.object,
  tableParams: PropTypes.object,
  headlines: PropTypes.arrayOf(PropTypes.object),
  renderHeadOptionControl: PropTypes.func,
  renderHeadlineOptionControl: PropTypes.func,
  hasControls: PropTypes.bool
};

export const defaultProps = {
  hasFilters: false,
  hasControls: false
};
