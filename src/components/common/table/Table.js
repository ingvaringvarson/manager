import React, { PureComponent } from "react";
import memoizeOne from "memoize-one";

import TableWrap from "../../../designSystem/organisms/Table";
import Text from "../../../designSystem/atoms/Text";
import TableRow from "../../../designSystem/molecules/TableRow";
import TableCell from "../../../designSystem/molecules/TableCell";
import RowBody from "./RowBody";
import { FieldContext, SortButtonContext } from "../formControls";
import { RowHead } from "../tableControls";
import { propTypes, defaultProps } from "./tablePropTypes";

class Table extends PureComponent {
  static propTypes = propTypes;

  static defaultProps = defaultProps;

  renderHeadlines = memoizeOne(headlines => {
    return headlines.map(hesd => {
      return (
        <TableCell key={hesd.id} flex={hesd.flex}>
          {!!hesd.render && hesd.render()}
        </TableCell>
      );
    });
  });

  renderCells = memoizeOne(cells => {
    return cells.reduce(
      (rows, cell) => {
        if (!cell.isActive) return rows;

        return {
          titles: rows.titles.concat(
            <TableCell key={cell.id} {...cell.headCellProps.handlers}>
              {cell.headCellProps.sortBy ? (
                <SortButtonContext
                  name={cell.id}
                  title={cell.title}
                  {...cell.headCellProps}
                />
              ) : (
                <Text>
                  {cell.headCellProps.render
                    ? cell.headCellProps.render({ field: cell })
                    : cell.title}
                </Text>
              )}
            </TableCell>
          ),
          inputs: rows.inputs.concat(
            <TableCell key={cell.id}>
              <FieldContext name={cell.id} />
            </TableCell>
          )
        };
      },
      { titles: [], inputs: [] }
    );
  });

  renderEntities = memoizeOne((entities, cells, enumerations) => {
    return entities.reduce(
      (result, entity) => {
        const {
          renderNestedTable,
          hasControls,
          renderOptionControl
        } = this.props;
        return {
          ids: result.ids.concat(entity.id),
          rows: result.rows.concat(
            <RowBody
              hasControls={hasControls}
              enumerations={enumerations}
              entity={entity}
              renderNestedTable={renderNestedTable}
              renderOptionControl={renderOptionControl}
              key={entity.id}
              cells={cells}
            />
          )
        };
      },
      { ids: [], rows: [] }
    );
  });

  render() {
    const {
      cells,
      entities,
      hasFilters,
      enumerations,
      tableParams,
      headlines,
      renderHeadOptionControl,
      hasControls
    } = this.props;

    if (!cells.length || !entities) return null;

    const headRows = headlines ? this.renderHeadlines(headlines) : null;

    const { titles, inputs } = this.renderCells(cells);

    const { ids, rows } = this.renderEntities(entities, cells, enumerations);

    return (
      <TableWrap {...tableParams}>
        {headRows && (
          <TableRow>
            {headRows}
            {this.props.hasControls && (
              <TableCell>
                {this.props.renderHeadlineOptionControl &&
                  this.props.renderHeadlineOptionControl()}
              </TableCell>
            )}
          </TableRow>
        )}
        <RowHead
          hasControls={hasControls}
          renderHeadOptionControl={renderHeadOptionControl}
          ids={ids}
        >
          {titles}
        </RowHead>
        {hasFilters && (
          <TableRow head>
            <TableCell first />
            {inputs}
          </TableRow>
        )}
        {rows}
      </TableWrap>
    );
  }
}

export default Table;
