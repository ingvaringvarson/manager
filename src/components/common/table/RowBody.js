import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Text from "../../../designSystem/atoms/Text";
import TableCell from "../../../designSystem/molecules/TableCell";
import RowBodyWrap from "../tableControls/RowBody";

class RowBody extends PureComponent {
  static propTypes = {
    entity: PropTypes.object.isRequired,
    cells: PropTypes.arrayOf(PropTypes.object).isRequired,
    renderNestedTable: PropTypes.func,
    renderOptionControl: PropTypes.func,
    enumerations: PropTypes.object,
    onClickRow: PropTypes.func
  };

  renderCell = (cells, cell) => {
    if (!cell.isActive) {
      return cells;
    }

    const { entity, enumerations } = this.props;
    const value = cell.bodyCellProps.render({
      entity,
      enumerations,
      field: cell
    });
    const textValue = value === null || value === undefined ? "" : value;

    return cells.concat(
      <TableCell key={cell.id} {...cell.bodyCellProps.handlers} breakContent>
        {!!cell.bodyCellProps.render && <Text>{textValue}</Text>}
      </TableCell>
    );
  };

  render() {
    const {
      cells,
      entity,
      renderNestedTable,
      renderOptionControl,
      hasControls
    } = this.props;

    if (!entity) return null;

    const row = cells.reduce(this.renderCell, []);

    return (
      <RowBodyWrap
        hasControls={hasControls}
        entity={entity}
        renderNestedTable={renderNestedTable}
        renderOptionControl={renderOptionControl}
        id={entity.id}
      >
        {row}
      </RowBodyWrap>
    );
  }
}

export default RowBody;
