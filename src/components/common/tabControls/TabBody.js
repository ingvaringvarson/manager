import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Context } from "./context";

class TabBody extends PureComponent {
  static propTypes = {
    children: PropTypes.any,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
  };
  static contextType = Context;

  renderTab = () => {
    if (this.props.id !== this.context.selectedId) {
      return null;
    }

    return this.props.children;
  };

  render() {
    return this.renderTab();
  }
}

export default TabBody;
