import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Context } from "./context";
import Tab from "../../../designSystem/atoms/Tab";

class TabHead extends PureComponent {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
      .isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
  };
  static contextType = Context;

  handleClick = () => this.context.onChangeSelected(this.props.id);

  renderTab = () => {
    const { children, id, ...props } = this.props;

    if (this.context.defaultStyle) {
      return (
        <Tab
          active={id === this.context.selectedId}
          onClick={this.handleClick}
          title={children}
          {...props}
        />
      );
    }

    return (
      <div {...props} onClick={this.handleClick}>
        {children}
      </div>
    );
  };

  render() {
    return this.renderTab();
  }
}

export default TabHead;
