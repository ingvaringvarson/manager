import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Context } from "./context";

class TabContainer extends PureComponent {
  static propTypes = {
    defaultStyle: PropTypes.bool,
    onChangeSelected: PropTypes.func,
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.array])
      .isRequired,
    selectedTabId: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
      .isRequired
  };

  static defaultProps = {
    defaultStyle: true,
    selectedTabId: null
  };

  onChangeSelected = selectedId => {
    if (typeof this.props.onChangeSelected === "function") {
      this.props.onChangeSelected(selectedId);
    } else {
      this.setState({ selectedId });
    }
  };

  state = {
    defaultStyle: this.props.defaultStyle,
    selectedId: this.props.selectedTabId,
    prevSelectedId: this.props.selectedTabId,
    onChangeSelected: this.onChangeSelected
  };

  static getDerivedStateFromProps(props) {
    if (props.prevSelectedId !== props.selectedTabId) {
      return {
        selectedId: props.selectedTabId,
        prevSelectedId: props.selectedTabId
      };
    }

    return null;
  }

  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    );
  }
}

export default TabContainer;
