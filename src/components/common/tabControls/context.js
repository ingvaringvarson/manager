import React from "react";

const InitContext = {
  selectedId: null,
  defaultStyle: true,
  onChangeSelected: () => {}
};

export const Context = React.createContext(InitContext);
