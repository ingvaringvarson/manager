import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Context } from "./context";
import Tabs from "../../../designSystem/molecules/Tabs";
import styled from "styled-components";

const StyledTabs = styled(Tabs)`
  align-content: space-around;
`;

class TabHeadContainer extends PureComponent {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.array])
      .isRequired
  };
  static contextType = Context;

  renderTabs = () => {
    const { children, ...props } = this.props;

    if (this.context.defaultStyle) {
      return <StyledTabs {...props}>{children}</StyledTabs>;
    }

    return <div {...props}>{children}</div>;
  };

  render() {
    return this.renderTabs();
  }
}

export default TabHeadContainer;
