import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { space } from "styled-system";

const BodyContainer = styled.div`
  ${space};
`;

const propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]).isRequired
};

function TabBodyContainer(props) {
  return <BodyContainer {...props}>{props.children}</BodyContainer>;
}

TabBodyContainer.propTypes = propTypes;

export default TabBodyContainer;
