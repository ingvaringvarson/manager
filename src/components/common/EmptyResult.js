import React from "react";
import PropTypes from "prop-types";
import Text from "../../designSystem/atoms/Text";
import styled from "styled-components";

const propTypes = {
  children: PropTypes.any
};

const EmptyResultBlock = styled.div`
  text-align: center;
`;

const EmptyResultMsg = styled(Text)`
  font-size: 18px;
  color: #787878;
  margin-top: 30px;
  margin-bottom: 30px;
  text-align: center;
`;

const EmptyResult = React.memo(function EmptyResult(props) {
  return (
    <EmptyResultBlock {...props}>
      <EmptyResultMsg>
        Результаты не найдены. Попробуйте ввести
        <br /> другие данные или убрать фильтры.
      </EmptyResultMsg>
      {props.children}
    </EmptyResultBlock>
  );
});

EmptyResult.propTypes = propTypes;

export default EmptyResult;
