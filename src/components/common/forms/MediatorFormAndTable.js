import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import {
  ContainerContext,
  FieldContext,
  FormContext,
  RenderContext
} from "../formControls";
import RemoveableTable from "../table/Removeable";
import RightStickedButton from "../buttons/RightStickedButton";
import Button from "../../../designSystem/atoms/Button";

class MediatorFromAndTable extends PureComponent {
  static propTypes = {
    entities: PropTypes.array.isRequired,
    fieldsForContainer: PropTypes.array.isRequired,
    fieldsForTable: PropTypes.array.isRequired,
    fieldsForForm: PropTypes.array,
    handleFormSubmit: PropTypes.func.isRequired,
    isRenderDefault: PropTypes.bool,
    renderFormAfterTable: PropTypes.func,
    renderFormBeforeTable: PropTypes.func,
    buttonTitle: PropTypes.string,
    removedIds: PropTypes.array,
    renderButtonContext: PropTypes.func
  };

  static defaultProps = {
    isRenderDefault: true,
    buttonTitle: "СОХРАНИТЬ",
    renderFormBeforeTable: null,
    renderFormAfterTable: null,
    removedIds: [],
    fieldsForForm: []
  };

  state = {
    removedIds: this.props.removedIds
  };

  handleRemovedIdsChange = removedIds => this.setState({ removedIds });
  handleFormSubmit = (values, ...restAgrs) => {
    const entitiesObj = this.getAvailableEntities(
      this.props.entities,
      this.state.removedIds
    );

    this.props.handleFormSubmit(
      values,
      entitiesObj.entities,
      this.state.removedIds,
      ...restAgrs
    );
  };

  getAvailableEntities(entities, removedIds) {
    return {
      entities: entities.filter(e => !removedIds.some(id => id === e._id))
    };
  }

  getHasAccessToSubmit = () => {
    const availableEntities = this.getAvailableEntities(
      this.props.entities,
      this.state.removedIds
    );

    return !!availableEntities.entities.length;
  };

  render() {
    const {
      fieldsForContainer,
      fieldsForTable,
      fieldsForForm,
      entities,
      enumerations,
      isRenderDefault,
      renderFormAfterTable,
      renderFormBeforeTable,
      buttonTitle,
      renderButtonContext
    } = this.props;

    return (
      <ContainerContext
        fields={fieldsForContainer}
        onSubmitForm={this.handleFormSubmit}
        submitValidation
      >
        {typeof renderFormBeforeTable === "function" &&
          renderFormBeforeTable(this.state)}
        <RemoveableTable
          cells={fieldsForTable}
          entities={entities}
          enumerations={enumerations}
          removedIds={this.state.removedIds}
          handleRemovedChange={this.handleRemovedIdsChange}
        />
        {isRenderDefault && (
          <FormContext>
            {fieldsForForm.map(field => (
              <FieldContext name={field.id} key={field.id} />
            ))}
          </FormContext>
        )}
        {typeof renderFormAfterTable === "function" &&
          renderFormAfterTable(this.state)}
        <RightStickedButton hasOwnButton>
          <RenderContext>
            {({ isFormValid, ...restContext }) =>
              renderButtonContext ? (
                renderButtonContext({
                  ...restContext,
                  isFormValid: isFormValid && this.getHasAccessToSubmit()
                })
              ) : (
                <Button
                  disabled={!(isFormValid && this.getHasAccessToSubmit())}
                >
                  {buttonTitle}
                </Button>
              )
            }
          </RenderContext>
        </RightStickedButton>
      </ContainerContext>
    );
  }
}

export default MediatorFromAndTable;
