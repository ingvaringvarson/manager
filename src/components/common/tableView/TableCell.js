import React, { Component } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import TableCellView from "../../../designSystem/molecules/TableCell";
import Text from "../../../designSystem/atoms/Text";
import styled, { css } from "styled-components";
import { ifProp } from "styled-tools";

const propTypes = {
  cellProps: PropTypes.object,
  handlers: PropTypes.object,
  renderProps: PropTypes.func,
  field: PropTypes.object,
  enumerations: PropTypes.object,
  entity: PropTypes.object,
  parentEntity: PropTypes.object
};

const CellText = styled(Text)`
  ${ifProp(
    "breakContent",
    css`
      word-break: break-all;
    `
  )}
`;

class TableCell extends Component {
  mapHandlers = memoizeOne(
    (handlers, entity, field, enumerations, parentEntity) => {
      return handlers
        ? Object.keys(handlers).reduce((result, handlerKey) => {
            result[handlerKey] = handlers[handlerKey]({
              entity,
              field,
              enumerations,
              parentEntity
            });
            return result;
          }, {})
        : null;
    }
  );

  render() {
    const {
      cellProps,
      handlers,
      renderProps,
      field,
      enumerations,
      entity,
      parentEntity
    } = this.props;

    const cellHandlers = this.mapHandlers(
      handlers,
      entity,
      field,
      enumerations,
      parentEntity
    );

    const cellTextContent =
      renderProps && renderProps({ field, enumerations, entity, parentEntity });

    return (
      <TableCellView {...cellProps} {...cellHandlers}>
        {cellTextContent && (
          <CellText breakContent={cellProps ? cellProps.breakContent : false}>
            {cellTextContent}
          </CellText>
        )}
      </TableCellView>
    );
  }
}

TableCell.propTypes = propTypes;

export default TableCell;
