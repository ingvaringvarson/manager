import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import TableRow from "../../../designSystem/molecules/TableRow";
import TableCell from "./TableCell";

const StyledHead = styled(TableRow)`
  top: 0;
`;

class RowHead extends PureComponent {
  static propTypes = {
    entities: PropTypes.array,
    cells: PropTypes.arrayOf(
      PropTypes.shape({
        headCellProps: PropTypes.shape({
          render: PropTypes.func,
          handlers: PropTypes.object
        })
      })
    ),
    enumerations: PropTypes.object,
    rowProps: PropTypes.object,
    hasChildrenBefore: PropTypes.bool,
    hasChildrenAfter: PropTypes.bool,
    renderChildrenBefore: PropTypes.func,
    renderChildrenAfter: PropTypes.func,
    parentEntity: PropTypes.object,
    hasHeadFixed: PropTypes.bool
  };

  static defaultProps = {
    hasChildrenBefore: false,
    hasChildrenAfter: false,
    hasHeadFixed: true,
    rowProps: { head: true }
  };

  renderCells = cells => {
    return cells.map(cell => {
      return (
        <TableCell
          key={cell.id}
          handlers={cell.headCellProps.handlers}
          cellProps={cell.headCellProps.cellProps}
          renderProps={cell.headCellProps.render}
          field={cell}
          enumerations={this.props.enumerations}
          parentEntity={this.props.parentEntity}
        />
      );
    });
  };

  render() {
    const {
      rowProps,
      cells,
      hasChildrenBefore,
      hasChildrenAfter,
      renderChildrenBefore,
      renderChildrenAfter,
      entities,
      enumerations,
      parentEntity,
      hasHeadFixed
    } = this.props;

    return (
      <StyledHead {...rowProps} headFixed={hasHeadFixed}>
        {hasChildrenBefore &&
          renderChildrenBefore &&
          renderChildrenBefore({ cells, enumerations, entities, parentEntity })}
        {this.renderCells(cells)}
        {hasChildrenAfter &&
          renderChildrenAfter &&
          renderChildrenAfter({ cells, enumerations, entities, parentEntity })}
      </StyledHead>
    );
  }
}

export default RowHead;
