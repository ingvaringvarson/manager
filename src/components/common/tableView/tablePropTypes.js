import PropTypes from "prop-types";

export const propTypes = {
  // Main props
  cells: PropTypes.arrayOf(PropTypes.object),
  entities: PropTypes.arrayOf(PropTypes.object),
  enumerations: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.arrayOf(PropTypes.object)
  ]),
  hasChildrenBefore: PropTypes.bool,
  hasChildrenAfter: PropTypes.bool,
  // FormControl Props
  hasFormControl: PropTypes.bool,
  hasExternalFormControl: PropTypes.bool,
  renderFormControl: PropTypes.func,
  rowFormControlProps: PropTypes.object,
  renderRowFormControlChildrenAfter: PropTypes.func,
  renderRowFormControlChildrenBefore: PropTypes.func,
  // Head Props
  hasExternalHead: PropTypes.bool,
  renderHead: PropTypes.func,
  rowHeadProps: PropTypes.object,
  renderRowHeadChildrenAfter: PropTypes.func,
  renderRowHeadChildrenBefore: PropTypes.func,
  // Body Props
  hasExternalBody: PropTypes.bool,
  cellBodyProps: PropTypes.object,
  cellBodyHandlers: PropTypes.object,
  renderBody: PropTypes.func,
  rowBodyProps: PropTypes.object,
  rowBodyHandlers: PropTypes.object,
  renderRowBodyChildrenAfter: PropTypes.func,
  renderRowBodyChildrenBefore: PropTypes.func,
  renderRowBodyAfter: PropTypes.func,
  renderAfterContent: PropTypes.func,
  renderBeforeContent: PropTypes.func,
  className: PropTypes.string,
  tableProps: PropTypes.object,
  helperData: PropTypes.object,
  parentEntity: PropTypes.object,
  hasHeadFixed: PropTypes.bool
};

export const defaultProps = {
  hasChildrenBefore: false,
  hasChildrenAfter: false,
  hasFormControl: false,
  hasExternalFormControl: false,
  hasExternalHead: false,
  hasExternalBody: false,
  className: "",
  parentEntity: null,
  hasHeadFixed: true
};
