import React, { memo } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { ifProp } from "styled-tools";
import Button from "../../../designSystem/atoms/Button";

const Wrapper = styled.div`
  text-align: right;
`;

const WrapperContent = styled.div`
  display: inline-block;
`;

const DataBlock = styled.div`
  display: inline-block;
  margin-right: 20px;
`;

const DataLabel = styled.p`
  font-size: 11px;
  color: #787878;
  margin-bottom: 5px;
`;

const DataContent = styled.p`
  font-size: 18px;
  font-family: Roboto;
  color: ${ifProp("red", "#EB0028", "#444")};
`;

const propTypes = {
  sum_total: PropTypes.number,
  sum_payment: PropTypes.number,
  buttonProps: PropTypes.object
};

const NestedTableAfter = memo(props => {
  const { sum_total, sum_payment, buttonProps } = props;

  return (
    <Wrapper>
      <WrapperContent>
        <DataBlock>
          <DataLabel>Сумма всего</DataLabel>
          <DataContent>{sum_total} руб.</DataContent>
        </DataBlock>
        <DataBlock>
          <DataLabel>Сумма к возврату</DataLabel>
          <DataContent red>{sum_payment} руб.</DataContent>
        </DataBlock>
      </WrapperContent>
      <Button disabled {...buttonProps}>
        Вернуть оплату
      </Button>
    </Wrapper>
  );
});

NestedTableAfter.propTypes = propTypes;

export default NestedTableAfter;
