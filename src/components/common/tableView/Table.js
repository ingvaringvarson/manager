import React, { PureComponent } from "react";
import TableWrapper from "../../../designSystem/organisms/Table";
import memoizeOne from "memoize-one";

import RowBody from "./RowBody";
import RowHead from "./RowHead";
import RowFormControl from "./RowFormControl";
import { propTypes, defaultProps } from "./tablePropTypes";

class TableView extends PureComponent {
  static propTypes = propTypes;

  static defaultProps = defaultProps;

  filterCells = memoizeOne(cells => {
    return cells.filter(cell => cell.isActive);
  });

  render() {
    const {
      cells,
      entities,
      enumerations,
      hasFormControl,
      hasExternalFormControl,
      renderFormControl,
      rowFormControlProps,
      hasExternalHead,
      renderHead,
      rowHeadProps,
      hasExternalBody,
      renderBody,
      rowBodyProps,
      rowBodyHandlers,
      cellBodyProps,
      cellBodyHandlers,
      renderRowFormControlChildrenBefore,
      renderRowFormControlChildrenAfter,
      renderRowHeadChildrenAfter,
      renderRowHeadChildrenBefore,
      renderRowBodyChildrenAfter,
      renderRowBodyChildrenBefore,
      hasChildrenBefore,
      hasChildrenAfter,
      className,
      renderAfterContent,
      renderBeforeContent,
      renderRowBodyAfter,
      tableProps,
      helperData, // ToDo: к удалению. Лишнее свойство, которое можно использовать на уровне формирования renderAfterContent
      parentEntity,
      hasHeadFixed
    } = this.props;

    const activeCells = this.filterCells(cells);

    const mainProps = {
      entities,
      cells: activeCells,
      enumerations,
      hasChildrenBefore,
      hasChildrenAfter,
      parentEntity,
      hasHeadFixed
    };

    return (
      <TableWrapper className={className} {...tableProps}>
        {!!renderBeforeContent && renderBeforeContent()}
        {hasExternalHead ? (
          renderHead && renderHead(entities, activeCells, enumerations)
        ) : (
          <RowHead
            {...mainProps}
            rowProps={rowHeadProps}
            renderChildrenBefore={renderRowHeadChildrenBefore}
            renderChildrenAfter={renderRowHeadChildrenAfter}
          />
        )}
        {hasFormControl ? (
          hasExternalFormControl ? (
            renderFormControl &&
            renderFormControl(entities, activeCells, enumerations)
          ) : (
            <RowFormControl
              {...mainProps}
              rowProps={rowFormControlProps}
              renderChildrenBefore={renderRowFormControlChildrenBefore}
              renderChildrenAfter={renderRowFormControlChildrenAfter}
            />
          )
        ) : null}
        {hasExternalBody
          ? renderBody &&
            renderBody(
              entities,
              activeCells,
              enumerations,
              // here comes all row body props
              {
                ...mainProps,
                rowProps: rowBodyProps,
                renderChildrenBefore: renderRowBodyChildrenBefore,
                renderChildrenAfter: renderRowBodyChildrenAfter,
                cellProps: cellBodyProps,
                cellHandlers: cellBodyHandlers,
                rowHandlers: rowBodyHandlers,
                renderRowAfter: renderRowBodyAfter
              }
            )
          : entities.map((entity, index) => {
              return (
                <RowBody
                  {...mainProps}
                  key={entity.key || entity._id || index}
                  entity={entity}
                  rowProps={rowBodyProps}
                  renderChildrenBefore={renderRowBodyChildrenBefore}
                  renderChildrenAfter={renderRowBodyChildrenAfter}
                  cellProps={cellBodyProps}
                  cellHandlers={cellBodyHandlers}
                  rowHandlers={rowBodyHandlers}
                  renderRowAfter={renderRowBodyAfter}
                />
              );
            })}
        {!!renderAfterContent &&
          renderAfterContent(helperData, {
            entities,
            enumerations,
            cells: activeCells
          })}
      </TableWrapper>
    );
  }
}

export default TableView;
