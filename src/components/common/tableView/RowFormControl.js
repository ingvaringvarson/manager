import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import TableRow from "../../../designSystem/molecules/TableRow";
import TableCell from "./TableCell";

class RowFormControl extends PureComponent {
  static propTypes = {
    cells: PropTypes.arrayOf(
      PropTypes.shape({
        headLineCellProps: PropTypes.shape({
          render: PropTypes.func,
          handlers: PropTypes.object
        })
      })
    ),
    enumerations: PropTypes.object,
    rowProps: PropTypes.object,
    hasChildrenBefore: PropTypes.bool,
    hasChildrenAfter: PropTypes.bool,
    renderChildrenBefore: PropTypes.func,
    renderChildrenAfter: PropTypes.func,
    parentEntity: PropTypes.object,
    entities: PropTypes.array
  };

  static defaultProps = {
    hasChildrenBefore: false,
    hasChildrenAfter: false,
    rowProps: { head: true }
  };

  renderCells = memoizeOne((cells, enumerations, parentEntity) => {
    return cells.map(cell => {
      return (
        <TableCell
          key={cell.id}
          handlers={cell.headLineCellProps.handlers}
          cellProps={cell.headCellProps.cellProps}
          renderProps={cell.headLineCellProps.render}
          field={cell}
          enumerations={enumerations}
          parentEntity={parentEntity}
        />
      );
    });
  });

  render() {
    const {
      cells,
      enumerations,
      hasChildrenAfter,
      hasChildrenBefore,
      renderChildrenAfter,
      renderChildrenBefore,
      rowProps,
      parentEntity,
      entities
    } = this.props;

    const row = this.renderCells(cells, enumerations, parentEntity);

    return (
      <TableRow {...rowProps}>
        {hasChildrenBefore &&
          renderChildrenBefore &&
          renderChildrenBefore({ cells, enumerations, entities, parentEntity })}
        {row}
        {hasChildrenAfter &&
          renderChildrenAfter &&
          renderChildrenAfter({ cells, enumerations, entities, parentEntity })}
      </TableRow>
    );
  }
}

export default RowFormControl;
