import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { ContextTable } from "../tableControls/context";

import TableRow from "../../../designSystem/molecules/TableRow";
import TableCell from "./TableCell";

class RowBody extends PureComponent {
  static propTypes = {
    entity: PropTypes.object.isRequired,
    cells: PropTypes.arrayOf(
      PropTypes.shape({
        bodyCellProps: PropTypes.shape({
          handlers: PropTypes.object,
          render: PropTypes.func
        })
      })
    ),
    enumerations: PropTypes.object,
    rowProps: PropTypes.object,
    hasChildrenBefore: PropTypes.bool,
    hasChildrenAfter: PropTypes.bool,
    renderChildrenBefore: PropTypes.func,
    renderChildrenAfter: PropTypes.func,
    cellProps: PropTypes.object,
    cellHandlers: PropTypes.object,
    rowHandlers: PropTypes.object,
    renderRowAfter: PropTypes.func,
    parentEntity: PropTypes.object
  };

  static defaultProps = {
    hasChildrenBefore: false,
    hasChildrenAfter: false
  };

  static contextType = ContextTable;

  activeRow = memoizeOne((selected, open, entity) => {
    return {
      isSelected: selected.some(_id => {
        return _id === entity._id;
      }),
      isOpened: open.some(_id => {
        return _id === entity._id;
      })
    };
  });

  renderCells = memoizeOne((cells, entity, enumerations, parentEntity) => {
    return cells.map(cell => {
      return (
        <TableCell
          key={cell.id}
          handlers={cell.bodyCellProps.handlers || this.props.cellHandlers}
          cellProps={cell.bodyCellProps.cellProps || this.props.cellProps}
          renderProps={cell.bodyCellProps.render}
          field={cell}
          enumerations={enumerations}
          entity={entity}
          parentEntity={parentEntity}
        />
      );
    });
  });

  mapHandlers = memoizeOne((handlers, entity, enumerations, parentEntity) => {
    return handlers
      ? Object.keys(handlers).reduce((result, handlerKey) => {
          result[handlerKey] = handlers[handlerKey]({
            entity,
            enumerations,
            parentEntity
          });
          return result;
        }, {})
      : null;
  });

  normalizeArguments = memoizeOne(
    (entity, cells, enumerations, entities, parentEntity) => ({
      entity,
      cells,
      enumerations,
      entities,
      parentEntity
    })
  );

  render() {
    const {
      cells,
      entity,
      enumerations,
      rowProps,
      hasChildrenBefore,
      hasChildrenAfter,
      renderChildrenBefore,
      renderChildrenAfter,
      entities,
      rowHandlers,
      renderRowAfter,
      parentEntity
    } = this.props;

    const { selected, open } = this.context;

    const { isSelected, isOpened } = this.activeRow(selected, open, entity);

    const row = this.renderCells(cells, entity, enumerations, parentEntity);
    const handlers = this.mapHandlers(
      rowHandlers,
      entity,
      enumerations,
      parentEntity
    );
    const arg = this.normalizeArguments(
      entity,
      cells,
      enumerations,
      entities,
      parentEntity
    );

    return (
      <>
        <TableRow
          {...rowProps}
          {...handlers}
          active={isSelected || isOpened}
          trigger={!!handlers}
        >
          {hasChildrenBefore &&
            renderChildrenBefore &&
            renderChildrenBefore(arg)}
          {row}
          {hasChildrenAfter && renderChildrenAfter && renderChildrenAfter(arg)}
        </TableRow>
        {!!renderRowAfter && renderRowAfter(arg)}
      </>
    );
  }
}

export default RowBody;
