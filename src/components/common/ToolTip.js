import React from "react";
import PropTypes from "prop-types";
import ReactTooltip from "react-tooltip";

const propTypes = {
  children: PropTypes.any
};

function ToolTip(props) {
  const { children, ...ownProps } = props;

  return <ReactTooltip {...ownProps}>{children}</ReactTooltip>;
}

ToolTip.propTypes = propTypes;

export default ToolTip;
