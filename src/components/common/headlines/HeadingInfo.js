import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { space } from "styled-system";

import Title from "../../../designSystem/atoms/Title/index";
import Text from "../../../designSystem/atoms/Text/index";

const StyledInfo = styled.div`
  &:not(:last-of-type) {
    margin-right: 30px;
  }

  ${space};
`;

const StyledText = styled(Text)`
  font-size: 11px;
`;

const HeadingInfo = ({ subTitle, info, ...props }) => {
  return (
    <StyledInfo {...props}>
      <StyledText mb="5px" alter>
        {subTitle}
      </StyledText>
      <Title>{info}</Title>
    </StyledInfo>
  );
};

HeadingInfo.propsTypes = {
  subTitle: PropTypes.string.isRequired,
  info: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired
};

export default HeadingInfo;
