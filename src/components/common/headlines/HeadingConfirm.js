import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { space } from "styled-system";

import Button from "../../../designSystem/atoms/Button/index";
import IconButton from "../../../designSystem/molecules/IconButton";

const StyledConfirm = styled.div`
  display: flex;
  ${space};
`;

// ToDo: удалить. Использовать components\common\menus\CommandsByStatusMenu
const HeadingConfirm = ({
  confirm,
  buttonProps,
  onConfirmClick,
  renderConfirm,
  ...props
}) => {
  return (
    <StyledConfirm>
      {renderConfirm
        ? renderConfirm()
        : confirm && (
            <Button mr="small" onClick={onConfirmClick}>
              {confirm}
            </Button>
          )}
      {props.renderIconButton ? (
        props.renderIconButton()
      ) : (
        <IconButton icon="more_vert" transparent {...buttonProps} />
      )}
    </StyledConfirm>
  );
};

HeadingConfirm.propsTypes = {
  confirm: PropTypes.bool,
  renderIconButton: PropTypes.func,
  buttonProps: PropTypes.object,
  renderConfirm: PropTypes.func
};

export default HeadingConfirm;
