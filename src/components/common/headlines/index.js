import HeadingCard from "./HeadingCard";
import HeadingConfirm from "./HeadingConfirm";
import HeadingInfo from "./HeadingInfo";
import HeadingType from "./HeadingType";

export { HeadingCard, HeadingConfirm, HeadingInfo, HeadingType };
