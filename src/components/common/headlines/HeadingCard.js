import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { space } from "styled-system";

import Heading from "../../../designSystem/atoms/Heading";
import Text from "../../../designSystem/atoms/Text";

const HeadingIcon = styled(Heading)`
  visibility: hidden;
  cursor: pointer;
`;

const StyledCard = styled.div`
  &:hover {
    ${HeadingIcon} {
      visibility: visible;
    }
  }

  ${space};
`;

const CardRow = styled.div`
  display: flex;
`;

const HeadingCard = ({
  title,
  name,
  status,
  statusColor,
  renderIcon,
  ...props
}) => {
  return (
    <StyledCard {...props}>
      <CardRow>
        <Heading fontSize="18px" mr="10px">
          {title}
          {name ? ":" : ""}
        </Heading>
        <Heading fontSize="18px" alter>
          {name}
        </Heading>
        {!!renderIcon && <HeadingIcon>{renderIcon()}</HeadingIcon>}
      </CardRow>
      {status && (
        <Text color={statusColor} mt="small">
          {status}
        </Text>
      )}
    </StyledCard>
  );
};

HeadingCard.propsTypes = {
  clientNum: PropTypes.string.isRequired,
  status: PropTypes.string,
  renderIcon: PropTypes.func
};

export default HeadingCard;
