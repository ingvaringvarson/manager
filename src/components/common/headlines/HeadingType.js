import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { space } from "styled-system";

import Title from "../../../designSystem/atoms/Title/index";

const StyledType = styled.div`
  display: flex;
  ${space};
`;

const HeadingType = ({ title, name }) => {
  return (
    <StyledType>
      <Title mr="5px">{title}</Title>
      <Title alter>{name}</Title>
    </StyledType>
  );
};

HeadingType.propTypes = {
  title: PropTypes.string.isRequired,
  managerName: PropTypes.string.isRequired
};

export default HeadingType;
