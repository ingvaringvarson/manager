import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { ContextTable } from "./context";
import TableRow from "../../../designSystem/molecules/TableRow";
import TableCell from "../../../designSystem/molecules/TableCell";
import Checkbox from "../../../designSystem/molecules/Checkbox";
import Icon from "../../../designSystem/atoms/Icon";
import memoizeOne from "memoize-one";

const OpenButton = styled(Icon)`
  width: 24px;
  height: 24px;
  font-size: 24px;
  cursor: pointer;
`;

const StyledRow = styled.div`
  display: flex;
  align-items: center;
`;

class RowBody extends PureComponent {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element,
      PropTypes.arrayOf(PropTypes.element)
    ]).isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    entity: PropTypes.object.isRequired,
    renderNestedTable: PropTypes.func,
    renderOptionControl: PropTypes.func
  };

  static contextType = ContextTable;

  state = {
    showNestedTable: false
  };

  rowHandlers = memoizeOne((rowHandlers, entity) => {
    return rowHandlers
      ? Object.keys(rowHandlers).reduce((handlers, handlerName) => {
          return {
            ...handlers,
            [handlerName]: rowHandlers[handlerName](entity)
          };
        }, {})
      : null;
  });

  handleChangeSelected = () => {
    this.context.onChangeSelected(this.props.id);
  };

  handleToggleShowNestedTable = () => {
    this.setState(prevState => {
      return {
        showNestedTable: !prevState.showNestedTable
      };
    });
  };

  nestedTableHandlers = {
    onClick: this.handleToggleShowNestedTable
  };

  handleClickCheckbox = event => {
    if (event) event.stopPropagation();
  };

  renderControls = () => {
    if (!this.context.showControls) return null;

    const checkboxCtrl = this.context.canBeSelected ? (
      <Checkbox
        type="checkbox"
        checked={this.context.selected.includes(this.props.id)}
        onChange={this.handleChangeSelected}
        onClick={this.handleClickCheckbox}
      />
    ) : null;
    const nestedTableCtrl =
      this.context.hasNestedTable && this.props.renderNestedTable ? (
        <OpenButton
          icon={this.state.showNestedTable ? "expand_more" : "chevron_right"}
          {...this.nestedTableHandlers}
        />
      ) : null;

    return (
      <TableCell first key={`controls_${this.props.id}`}>
        <StyledRow>
          {checkboxCtrl}
          {nestedTableCtrl}
        </StyledRow>
      </TableCell>
    );
  };

  renderRow = () => {
    const {
      children,
      id,
      entity,
      renderNestedTable,
      renderOptionControl,
      hasControls
    } = this.props;
    const { rowBodyHandlers } = this.context;

    const handlers = this.rowHandlers(rowBodyHandlers, entity);

    return (
      <>
        <TableRow {...handlers} trigger={!!handlers}>
          {this.renderControls()}
          {children}
          {hasControls && (
            <TableCell control>
              {renderOptionControl && renderOptionControl(id, entity)}
            </TableCell>
          )}
        </TableRow>
        {renderNestedTable && this.state.showNestedTable
          ? renderNestedTable(id, entity)
          : null}
      </>
    );
  };

  render() {
    return this.renderRow();
  }
}

export default RowBody;
