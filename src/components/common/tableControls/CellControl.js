import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { ContextTable } from "./context";
import TableCell from "../../../designSystem/molecules/TableCell";
import Checkbox from "../../../designSystem/molecules/Checkbox";
import Icon from "../../../designSystem/atoms/Icon";

const OpenButton = styled(Icon)`
  width: 24px;
  height: 24px;
  font-size: 24px;
  cursor: pointer;
`;

const StyledRow = styled.div`
  display: flex;
  align-items: center;
`;

class CellControl extends PureComponent {
  static propTypes = {
    head: PropTypes.bool.isRequired,
    before: PropTypes.bool,
    after: PropTypes.bool,
    id: PropTypes.string
  };

  static defaultProps = {
    head: false,
    before: false,
    after: false
  };

  static contextType = ContextTable;

  handleHeadChangeSelected = () => {
    this.context.onChangeSelected(
      this.context.ids.length === this.context.selected.length
        ? []
        : this.context.ids
    );
  };

  handleBodyChangeSelected = () => {
    this.context.onChangeSelected(this.props.id);
  };

  handleToggleOpen = () => {
    this.context.toggleOpen(this.props.id);
  };

  renderHeadBefore = () => {
    if (!this.context.showControls) return null;

    const checkboxCtrl = this.context.canBeSelected ? (
      <Checkbox
        type="checkbox"
        checked={
          this.context.ids.length &&
          this.context.ids.length === this.context.selected.length
        }
        onChange={this.handleHeadChangeSelected}
        gray
      />
    ) : null;

    return checkboxCtrl;
  };

  handleStopLifting = event => {
    event.stopPropagation();
  };

  handlers = {
    onClick: this.handleStopLifting
  };

  renderBodyBefore = () => {
    if (!this.context.showControls) return null;

    const checkboxCtrl = this.context.canBeSelected ? (
      <Checkbox
        id={`rbb_${this.props.id}`}
        type="checkbox"
        checked={this.context.selected.includes(this.props.id)}
        onChange={this.handleBodyChangeSelected}
      />
    ) : null;
    const nestedTableCtrl = this.context.hasNestedTable ? (
      <OpenButton
        icon={
          this.context.open.includes(this.props.id)
            ? "expand_more"
            : "chevron_right"
        }
        onClick={this.handleToggleOpen}
      />
    ) : null;

    return (
      <StyledRow>
        {checkboxCtrl}
        {nestedTableCtrl}
      </StyledRow>
    );
  };

  renderBody() {
    const { head, before } = this.props;
    if (head) {
      if (before) {
        return this.renderHeadBefore();
      }
    } else {
      if (before) {
        return this.renderBodyBefore();
      }
    }
    return null;
  }

  render() {
    const { before, id } = this.props;
    const handlers = this.context.stopLiftingByControls ? this.handlers : null;

    return (
      <TableCell
        first={before}
        key={`controls_${id ? id : "main"}`}
        {...handlers}
      >
        {this.renderBody()}
      </TableCell>
    );
  }
}

export default CellControl;
