import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { ContextTable, DefaultState } from "./context";
import {
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore
} from "./helpers";

class TableContainer extends PureComponent {
  static propTypes = {
    canBeSelected: PropTypes.bool,
    hasNestedTable: PropTypes.bool,
    rowBodyHandlers: PropTypes.object,
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]),
    renderTable: PropTypes.func,
    stopLiftingByControls: PropTypes.bool,
    entities: PropTypes.array,
    selected: PropTypes.array,
    handleOpen: PropTypes.func,
    open: PropTypes.array,
    handleChangeSelected: PropTypes.func
  };

  static defaultProps = {
    canBeSelected: false,
    hasNestedTable: false,
    stopLiftingByControls: true,
    entities: []
  };

  constructor(props) {
    super(props);

    this.state = {
      ...DefaultState,
      canBeSelected: this.props.canBeSelected,
      hasNestedTable: this.props.hasNestedTable,
      onChangeSelected: this.handleChangeSelected,
      toggleOpen: this.handleOpen,
      showControls: this.props.canBeSelected || this.props.hasNestedTable,
      rowBodyHandlers: this.props.rowBodyHandlers || null,
      stopLiftingByControls: this.props.stopLiftingByControls,
      entities: props.entities,
      ids: props.entities.map(entity => entity._id)
    };

    if (this.props.selected) {
      this.state.selected = this.props.selected;
    }

    if (this.props.open) {
      this.state.open = this.props.open;
    }

    this.renderCallBacks = {
      renderRowFormControlChildrenBefore: renderRowFormControlChildrenBefore,
      renderRowHeadChildrenBefore: renderRowHeadChildrenBefore,
      renderRowBodyChildrenBefore: renderRowBodyChildrenBefore
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.handleChangeSelected &&
      this.state.selected !== prevState.selected
    ) {
      this.props.handleChangeSelected(this.state.selected);
    }
  }

  handleChangeSelected = selected => {
    if (Array.isArray(selected)) {
      this.setState({ selected });
    } else {
      this.setState(prevState => {
        const indexSelected = prevState.selected.findIndex(id => {
          return id === selected;
        });
        return {
          selected: ~indexSelected
            ? prevState.selected.filter((id, index) => {
                return index !== indexSelected;
              })
            : prevState.selected.concat(selected)
        };
      });
    }
  };

  handleOpen = openIds => {
    this.props.handleOpen && this.props.handleOpen(openIds);

    if (Array.isArray(openIds)) {
      this.setState({ open: openIds });
    } else {
      this.setState(prevState => {
        const indexSelected = prevState.open.findIndex(id => {
          return id === openIds;
        });
        return {
          open: ~indexSelected
            ? prevState.open.filter((id, index) => {
                return index !== indexSelected;
              })
            : prevState.open.concat(openIds)
        };
      });
    }
  };

  render() {
    return (
      <ContextTable.Provider value={this.state}>
        {this.props.renderTable
          ? this.props.renderTable(this.renderCallBacks, this.state)
          : this.props.children}
      </ContextTable.Provider>
    );
  }
}

export default TableContainer;
