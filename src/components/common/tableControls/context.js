import React from "react";

export const DefaultState = {
  selected: [],
  open: [],
  canBeSelected: false,
  hasNestedTable: false,
  showControls: false,
  onChangeSelected: () => {},
  toggleOpen: () => {},
  rowBodyHandlers: null,
  canBeControlled: false,
  stopLiftingByControls: true
};

export const ContextTable = React.createContext(DefaultState);
