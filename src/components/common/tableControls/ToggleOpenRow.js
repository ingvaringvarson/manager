import React, { Component } from "react";
import PropTypes from "prop-types";
import { ContextTable } from "./context";

class ToggleOpenRow extends Component {
  static contextType = ContextTable;

  static propTypes = {
    id: PropTypes.string.isRequired,
    children: PropTypes.any
  };

  render() {
    return this.context && this.context.open.includes(this.props.id)
      ? this.props.children
      : null;
  }
}

export default ToggleOpenRow;
