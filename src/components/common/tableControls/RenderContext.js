import React, { Component } from "react";
import PropTypes from "prop-types";
import { ContextTable } from "./context";

class RenderContext extends Component {
  static contextType = ContextTable;

  static propTypes = {
    children: PropTypes.func
  };

  render() {
    return this.props.children(this.context, this.props);
  }
}

export default RenderContext;
