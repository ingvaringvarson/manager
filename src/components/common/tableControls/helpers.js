import React from "react";
import CellControl from "./CellControl";
import TableCell from "../../../designSystem/molecules/TableCell";

export const renderRowFormControlChildrenBefore = () => {
  return <TableCell first key="controls_before" />;
};

export const renderRowFormControlChildrenAfter = () => {
  return <TableCell key="controls_after" control />;
};

export const renderRowHeadChildrenAfter = () => {
  return <TableCell control />;
};

export const renderRowBodyChildrenBefore = ({ entity }) => {
  return <CellControl before id={entity._id} />;
};

export const handleStopLifting = event => {
  event.stopPropagation();
};

export const renderRowHeadChildrenBefore = () => {
  return <CellControl head before />;
};
