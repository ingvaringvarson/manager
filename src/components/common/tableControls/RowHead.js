import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { ContextTable } from "./context";
import TableRow from "../../../designSystem/molecules/TableRow";
import TableCell from "../../../designSystem/molecules/TableCell";
import Checkbox from "../../../designSystem/molecules/Checkbox";

class RowHead extends PureComponent {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element,
      PropTypes.array
    ]).isRequired,
    ids: PropTypes.array.isRequired,
    renderHeadOptionControl: PropTypes.func
  };

  static contextType = ContextTable;

  handleChangeSelected = () => {
    this.context.onChangeSelected(
      this.props.ids.length === this.context.selected.length
        ? []
        : this.props.ids
    );
  };

  renderControls = () => {
    if (!this.context.showControls) return null;

    const checkboxCtrl = this.context.canBeSelected ? (
      <Checkbox
        type="checkbox"
        checked={
          this.props.ids.length &&
          this.props.ids.length === this.context.selected.length
        }
        onChange={this.handleChangeSelected}
        gray
      />
    ) : null;

    return (
      <TableCell first key="controls_main">
        {checkboxCtrl}
      </TableCell>
    );
  };

  render() {
    return (
      <TableRow>
        {this.renderControls()}
        {this.props.children}
        {this.props.hasControls && (
          <TableCell>
            {this.props.renderHeadOptionControl &&
              this.props.renderHeadOptionControl(this.context.selected)}
          </TableCell>
        )}
      </TableRow>
    );
  }
}

export default RowHead;
