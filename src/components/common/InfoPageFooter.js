import React, { memo } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Row from "../../designSystem/templates/Row";
import Column from "../../designSystem/templates/Column";
import EditComment from "../forms/EditComment";
import { WrappedWithModal as CreateUpdateForm } from "../blocks/contracts/CreateUpdateForm";
import { GreyText, InfoText } from "./styled/text";
import { VerticalCenter } from "./styled/flex";
import { getDateWithMonthName } from "../../utils";

const FooterWrapper = styled.div`
  margin-top: 20px;
`;

const infoIconProps = {
  width: "16px",
  height: "16px"
};

const columnProps = {
  basis: "250px",
  maxWidth: "250px",
  mr: "16px",
  mt: "16px"
};

function getContractByClient(contract_id, client) {
  if (!(contract_id && client && Array.isArray(client.contracts))) return null;

  return client.contracts.find(c => c.id === contract_id);
}

const propTypes = {
  entity: PropTypes.shape({
    history_id: PropTypes.string,
    comment: PropTypes.string,
    object_date_create: PropTypes.string,
    object_name_create: PropTypes.string,
    contract_id: PropTypes.string,
    contract_no: PropTypes.string,
    firm_name: PropTypes.string
  }).isRequired,
  client: PropTypes.object.isRequired,
  onSubmitComment: PropTypes.func.isRequired
};

const InfoPageFooter = memo(props => {
  const {
    entity: {
      history_id,
      comment,
      object_date_create,
      object_name_create,
      contract_id,
      contract_no,
      firm_name
    },
    onSubmitComment,
    client
  } = props;

  const contract = getContractByClient(contract_id, client);
  return (
    <FooterWrapper>
      <EditComment onSubmitForm={onSubmitComment} comment={comment} />
      <Row m="0px" justifyBetween>
        <Column {...columnProps}>
          {!!history_id && (
            <InfoText>
              <GreyText>Старый ID:</GreyText> {history_id}
            </InfoText>
          )}
        </Column>
        <Row>
          <Column {...columnProps}>
            {!!object_date_create && (
              <InfoText>
                <GreyText>Дата создания:</GreyText>{" "}
                {getDateWithMonthName(object_date_create)}
              </InfoText>
            )}
            {!!object_name_create && (
              <InfoText>
                <GreyText>Автор заказа:</GreyText> {object_name_create}
              </InfoText>
            )}
          </Column>
          <Column {...columnProps}>
            {!!contract_id && (
              <InfoText>
                <VerticalCenter>
                  <GreyText>Договор:</GreyText> {contract_no}{" "}
                  {!!contract && (
                    <CreateUpdateForm
                      iconName="info"
                      client={client}
                      contract={contract}
                      iconProps={infoIconProps}
                    />
                  )}
                </VerticalCenter>
              </InfoText>
            )}
            {!!firm_name && (
              <InfoText>
                <GreyText>Фирма:</GreyText> {firm_name}
              </InfoText>
            )}
          </Column>
        </Row>
      </Row>
    </FooterWrapper>
  );
});

InfoPageFooter.propTypes = propTypes;

export default InfoPageFooter;
