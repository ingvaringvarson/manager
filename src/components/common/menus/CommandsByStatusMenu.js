import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import styled from "styled-components";
import { palette } from "styled-tools";
import { space } from "styled-system";
import memoizeOne from "memoize-one";

import Button from "../../../designSystem/atoms/Button";

import CommonMenu from "./CommonMenu";
import CommandButton from "./CommandButton";

import commandMenuPropTypes from "./commandMenuPropTypes";
import { handleStopLifting } from "../tableControls";
import { permissionsArrSelector } from "./../../../ducks/user";

const StyledMainButton = styled.div`
  display: flex;
  ${space};
`;

const MenuButtonWrapper = styled.div`
  & > div button {
    background-color: transparent;
  }
`;

const MenuWrapper = styled.div`
  & > div > div p {
    color: ${palette("general", 0)};
  }
`;

const menuProps = {
  position: "absolute",
  zIndex: 1,
  top: "50px",
  right: true,
  left: "-198px",
  width: "250px"
};

class CommandsByStatusMenu extends PureComponent {
  static propTypes = {
    ...commandMenuPropTypes,
    permissions: PropTypes.arrayOf(PropTypes.string)
  };

  state = { openedModalCommandId: null };

  static noneFunc = () => {};

  handleModalOpen = command => {
    const { entityOrEntities } = this.props;
    const commandOnClick = command.onClick || CommandsByStatusMenu.noneFunc;
    return e => {
      this.setState({ openedModalCommandId: command.id });
      commandOnClick(entityOrEntities, command, e);
    };
  };
  handleModalClose = () => this.setState({ openedModalCommandId: null });

  getPatchedCommands = () => {
    const { commands, forList } = this.props;
    const filteredCommands = commands.filter(c => !forList || !!c.forList);
    filteredCommands.forEach(c => {
      c.onClick = this.handleModalOpen(c);
      c.isDisabled = this.isDisabledByPermissions(c);
    });
    return filteredCommands;
  };

  getMenuOptions = memoizeOne(menuOptions => menuOptions);

  isDisabledByPermissions = command => {
    const { permissions, entityOrEntities } = this.props;

    if (!command.permissionsMarker) {
      return false;
    }

    if (
      Array.isArray(command.permissionsMarker) &&
      command.permissionsMarker.length
    ) {
      return !command.permissionsMarker.every(p => permissions.includes(p));
    } else if (typeof command.permissionsMarker === "function") {
      return !command.permissionsMarker(permissions, entityOrEntities);
    }

    return false;
  };

  renderToggleButton = (menuOptions, withBorder) => ({ handleToggle }) => {
    const disabled = !menuOptions.length;
    if (typeof this.props.renderToggleButton === "function") {
      return this.props.renderToggleButton({ disabled, handleToggle });
    }
    return (
      <CommandButton
        simple={!withBorder}
        secondary={withBorder}
        onClick={handleToggle}
        disabled={disabled}
      />
    );
  };

  renderMenu = () => {
    const {
      mainCommandId,
      entityOrEntities,
      parentEntity,
      commandsToMenuOptionsFilter
    } = this.props;

    const patchedCommands = this.getPatchedCommands();

    const mainCommand = patchedCommands.find(c => c.id === mainCommandId);

    const entities = Array.isArray(entityOrEntities)
      ? entityOrEntities
      : [entityOrEntities];

    const menuOptions = commandsToMenuOptionsFilter(
      patchedCommands,
      entities,
      parentEntity
    );

    return mainCommand
      ? this.renderMainButtonAndMenu(mainCommand, menuOptions)
      : this.renderMenuOnly(menuOptions);
  };

  renderMainButtonAndMenu = (mainCommand, menuOptions) => {
    const mainOption = menuOptions.find(c => c.id === mainCommand.id);
    const otherMenuOptions = menuOptions.filter(o => o.id !== mainCommand.id);
    return (
      <StyledMainButton>
        <Button
          mr="small"
          disabled={!mainOption}
          onClick={mainOption && mainOption.onClick}
        >
          {mainCommand.title}
        </Button>
        <MenuButtonWrapper>
          {this.renderMenuOnly(otherMenuOptions, true)}
        </MenuButtonWrapper>
      </StyledMainButton>
    );
  };

  renderMenuOnly = (menuOptions, withBorder) => (
    <MenuWrapper>
      <CommonMenu
        menuProps={menuProps}
        menuOptions={this.getMenuOptions(menuOptions)}
      >
        {this.renderToggleButton(menuOptions, withBorder)}
      </CommonMenu>
    </MenuWrapper>
  );

  renderForm = () => {
    const { openedModalCommandId } = this.state;

    if (openedModalCommandId === null) {
      return null;
    }

    const { commands, entityOrEntities, parentEntity } = this.props;

    const command = commands.find(c => c.id === openedModalCommandId);

    if (!command || !command.renderModal) {
      return null;
    }

    return command.renderModal(
      this.handleModalClose,
      entityOrEntities,
      parentEntity,
      command
    );
  };

  render() {
    return (
      <div onClick={handleStopLifting}>
        {this.renderMenu()}
        {this.renderForm()}
      </div>
    );
  }
}

export default connect(state => ({
  permissions: permissionsArrSelector(state)
}))(CommandsByStatusMenu);
