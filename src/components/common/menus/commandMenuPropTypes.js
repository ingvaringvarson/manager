import PropTypes from "prop-types";

const commandShape = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  forList: PropTypes.bool,
  // либо renderModal, либо onClick
  // (handleClose, entities, parentEntity)
  renderModal: PropTypes.func,
  // (entityOrEntities, command, e)
  onClick: PropTypes.func,
  // либо blockedStates, либо availableStates
  blockedStates: PropTypes.arrayOf(PropTypes.number),
  availableStates: PropTypes.arrayOf(PropTypes.number),
  // (command, entity)
  customFilter: PropTypes.func,
  permissionsMarker: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    // (userPermissions, entity)
    PropTypes.func
  ]),
  renderToggleButton: PropTypes.func
};

export default {
  entityOrEntities: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.arrayOf(PropTypes.object)
  ]).isRequired,
  parentEntity: PropTypes.object,
  mainCommandId: PropTypes.string,
  commands: PropTypes.arrayOf(PropTypes.shape(commandShape)).isRequired,
  // (commands, entities, parentEntities)
  commandsToMenuOptionsFilter: PropTypes.func.isRequired
};
