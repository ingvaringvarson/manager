import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import styled from "styled-components";
import {
  position,
  zIndex,
  top,
  left,
  width,
  right,
  bottom
} from "styled-system";

import Menu from "../../../designSystem/molecules/Menu";

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  cursor: default;
`;

const MenuElement = styled.p`
  cursor: pointer;

  &[disabled] {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;

const MenuHolder = styled.div`
  position: relative;
`;

const MenuWrapper = styled.div`
  position: absolute;
  z-index: 3;
  right: -15px;
  top: 40px;
  width: 250px;
  ${position};
  ${zIndex};
  ${top};
  ${left};
  ${right};
  ${bottom};
  ${width};
`;

const menuContentProps = {
  right: true
};

class CommonMenu extends PureComponent {
  static propTypes = {
    menuOptions: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        id: PropTypes.string,
        onClick: PropTypes.func,
        isDisabled: PropTypes.bool,
        props: PropTypes.object
      })
    ),
    children: PropTypes.func.isRequired,
    menuProps: PropTypes.object,
    renderMenu: PropTypes.func
  };

  static defaultProps = {
    menuProps: {}
  };

  state = {
    isShow: false
  };

  handleToggle = () => {
    this.setState(prevState => ({ isShow: !prevState.isShow }));
  };

  handleOpen = () => {
    this.setState({ isShow: true });
  };

  handleClose = () => {
    this.setState({ isShow: false });
  };

  handleMenuElementClick = elementOnClick => e => {
    elementOnClick && elementOnClick(e);
    this.handleClose();
  };

  static noneFunc = () => {};

  getMenuElementProps = memoizeOne(option => {
    const optionProps = { ...option.props };

    const isDisabled =
      option.isDisabled === undefined ? false : option.isDisabled;

    if (isDisabled) {
      optionProps.disabled = true;
      optionProps.onClick = CommonMenu.noneFunc;
    } else {
      optionProps.onClick = this.handleMenuElementClick(option.onClick);
    }

    return optionProps;
  });

  getPropsByChildren = memoizeOne(isShow => {
    const { handleClose, handleOpen, handleToggle } = this;

    return { isShow, handleClose, handleOpen, handleToggle };
  });

  renederMenuElement = option => (
    <MenuElement key={option.id} {...this.getMenuElementProps(option)}>
      {option.title}
    </MenuElement>
  );

  render() {
    const { children, menuOptions, menuProps, renderMenu } = this.props;
    const { isShow } = this.state;
    const propsByChildren = this.getPropsByChildren(isShow);

    return (
      <MenuHolder>
        {children(propsByChildren)}
        {isShow && (
          <>
            {renderMenu ? (
              renderMenu(propsByChildren)
            ) : (
              <MenuWrapper {...menuProps}>
                <Menu {...menuContentProps}>
                  {menuOptions.map(option => this.renederMenuElement(option))}
                </Menu>
              </MenuWrapper>
            )}
            <Overlay onClick={this.handleClose} />
          </>
        )}
      </MenuHolder>
    );
  }
}

export default CommonMenu;
