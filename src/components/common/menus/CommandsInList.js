import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import StyledButton from "../../../designSystem/atoms/Button";
import MenuItem from "./MenuItem";
import { MenuStyled, Container } from "../styled/menu";

class CommandsInList extends PureComponent {
  static propTypes = {
    buttonTitle: PropTypes.string,
    commands: PropTypes.array.isRequired
  };

  state = {
    modalNameShow: "",
    isMenuShow: false
  };

  handleMenuOpen = () => this.setState({ isMenuShow: true });
  handleMenuClose = () => this.setState({ isMenuShow: false });

  handleModalClose = () => this.setState({ modalNameShow: "" });
  handleModalOpen = id => this.setState({ modalNameShow: id });
  handleElementClick = c => {
    if (c.onClick) {
      c.onClick(c);
    } else {
      this.handleModalOpen(c.id);
    }
    this.handleMenuClose();
  };

  renderForm = () => {
    const { modalNameShow } = this.state;

    const command = this.props.commands.find(c => c.id === modalNameShow);

    if (!command || !command.renderModal) {
      return null;
    }

    return command.renderModal(this.handleModalClose, command);
  };

  render() {
    return (
      <Container>
        <StyledButton fullWidth onClick={this.handleMenuOpen}>
          {this.props.buttonTitle}
        </StyledButton>
        <MenuStyled center isOpen={this.state.isMenuShow}>
          {this.props.commands.map(c => (
            <MenuItem
              {...c.attrs}
              command={c}
              onClick={this.handleElementClick}
            >
              {c.title}
            </MenuItem>
          ))}
        </MenuStyled>
        {this.renderForm()}
      </Container>
    );
  }
}

export default CommandsInList;
