import React, { memo } from "react";
import PropTypes from "prop-types";
import { StyledText as StyledMenuItem } from "../../common/styled/menu";

const propTypes = {
  children: PropTypes.any,
  command: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired
};

function MenuItem(props) {
  const { children, onClick, command, ...restProps } = props;
  const onClickBind = () => onClick(command, restProps);

  return (
    <StyledMenuItem {...restProps} onClick={onClickBind}>
      {children}
    </StyledMenuItem>
  );
}

MenuItem.propTypes = propTypes;

export default memo(MenuItem);
