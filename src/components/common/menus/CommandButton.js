import React, { memo } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Icon from "../../../designSystem/atoms/Icon";
import Button from "../../../designSystem/atoms/Button";

const propTypes = {
  onClick: PropTypes.func,
  disabled: PropTypes.bool
};

const defaultProps = {
  disabled: false
};

const StyledButton = styled(Button)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 36px;
  padding: 0;
`;

const CommandButton = memo(props => {
  const handleClick = event => {
    event.preventDefault();
    props.onClick();
  };

  const { disabled, ...restProps } = props;
  return (
    <StyledButton
      disabled={disabled}
      type="button"
      {...restProps}
      onClick={handleClick}
    >
      <Icon icon="baseline_more" />
    </StyledButton>
  );
});

CommandButton.propTypes = propTypes;
CommandButton.defaultProps = defaultProps;

export default CommandButton;
