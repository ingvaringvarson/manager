import React, { Component } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import nanoId from "nanoid";
import ReactModal from "react-modal";

import { connect } from "react-redux";
import { createTask } from "../../../ducks/tasks";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../ducks/enumerations";

import styled from "styled-components";
import { ContainerContext, FormContext } from "../../common/formControls/index";
import ModalIconControls from "../../common/modalControls/ModalIconControls";
import FieldForm from "../../common/form/FieldForm";

import Block from "../../../designSystem/organisms/Block";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import Heading from "../../../designSystem/molecules/Heading";
import Button from "../../../designSystem/atoms/Button";

import { reduceValuesToFormFields } from "../../../fields/helpers";
import {
  callToClientFields,
  prepareForIssueFields,
  prepareForMovementFields
} from "../../../fields/formFields/tasks/createTasksFields";
import { formFields } from "../../../fields/formFields/tasks/createTaskFields";

import OrderTable from "./OrderTable";

const AddButton = styled.div`
  color: #444;
  font-size: 24px;
  font-weight: 500;
  cursor: pointer;
`;

const ModalContentWrapper = styled.div`
  min-width: 630px;
`;

const SubmitButtonWrapper = styled.div`
  text-align: right;
`;

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "630px",
    border: "none",
    padding: "0",
    overflow: "visible"
  }
};

const enumerationNames = ["taskType", "taskState"];

class AddTask extends Component {
  static propTypes = {
    fetchEnumerations: PropTypes.func,
    enumerations: PropTypes.object,
    createTask: PropTypes.func
  };
  state = {
    mainFormStateValues: {},
    fields: callToClientFields,
    orders: [
      {
        _id: nanoId(),
        _index: 0
      }
    ],
    orderTableValues: {},
    isModalOpen: false
  };

  handleModalClose = () => {
    this.setState({ isModalOpen: false });
  };

  handleModalToggle = () => {
    this.setState(prevState => {
      return {
        isModalOpen: !prevState.isModalOpen
      };
    });
  };

  iconProps = {
    onClick: this.handleModalToggle,
    width: 24,
    height: 24
  };

  componentDidMount = () => {
    this.props.fetchEnumerations(enumerationNames);
  };

  onSubmitForm = (values, fields) => {
    this.props.createTask(values, { orderValues: this.state.orderTableValues });
    this.handleModalClose();
  };

  handleAddOrder = () => {
    this.setState(prevState => {
      const orders = [
        ...prevState.orders,
        {
          _id: nanoId(),
          _index: prevState.orders.length
        }
      ];

      return { orders };
    });
  };

  handleFormStateChange = state => {
    if (state.values !== this.state.mainFormStateValues) {
      this.setState(prevState => {
        const newState = { mainFormStateValues: state.values };
        switch (+state.values.type[0]) {
          case 1:
            newState.fields = prepareForIssueFields;
            break;
          case 2:
            newState.fields = prepareForMovementFields;
            break;
          default:
            newState.fields = callToClientFields;
            break;
        }
        return newState;
      });
    }
  };

  handleOrderTableChange = (order, orderId, formValues, removedIds) => {
    this.setState(prevState => {
      return {
        orderTableValues: {
          ...prevState.orderTableValues,
          [orderId]: {
            order,
            formValues,
            removedIds
          }
        }
      };
    });
  };

  renderCells = memoizeOne((cells, enumerations, values, orders) => {
    const newOrders = orders.map(order => {
      return {
        ...order,
        value: values[`order_id__${order._id}`]
      };
    });

    return {
      cells: cells.reduce(
        reduceValuesToFormFields(
          { ...values, orders: newOrders },
          enumerations
        ),
        []
      ),
      key: nanoId()
    };
  });

  renderKey = memoizeOne((...args) => {
    return {
      key: nanoId()
    };
  });

  render() {
    const { cells } = this.renderCells(
      formFields,
      this.props.enumerations,
      this.state.mainFormStateValues,
      this.state.orders
    );

    const { key } = this.renderKey(this.state.fields, this.state.orders);

    return (
      <>
        <AddButton {...this.iconProps}>+</AddButton>
        <ReactModal
          isOpen={this.state.isModalOpen}
          onRequestClose={this.handleModalClose}
          style={customStyles}
        >
          <Block>
            <Heading icon="close" iconProps={this.iconProps}>
              Создание задач
            </Heading>
            <BlockContent>
              <ContainerContext
                key={key}
                onSubmitForm={this.onSubmitForm}
                fields={cells}
                handleStateChanged={this.handleFormStateChange}
              >
                <FormContext>
                  {cells.map((cell, index) => {
                    const orderInState = this.state.orders.find(
                      order => `order_id__${order._id}` === cell.id
                    );

                    // START --- return order field
                    if (orderInState) {
                      const lastOrderId = `order_id__${this.state.orders[this.state.orders.length - 1]._id}`;

                      if (
                        lastOrderId === cell.id &&
                        this.state.mainFormStateValues[lastOrderId]
                      ) {
                        return (
                          <div key={index}>
                            <FieldForm index={index} field={cell} />
                            <OrderTable
                              field={cell}
                              orderId={
                                this.state.mainFormStateValues[cell.id][0]
                              }
                              handleChange={this.handleOrderTableChange}
                            />
                            <button onClick={this.handleAddOrder}>
                              Добавить еще заказ
                            </button>
                          </div>
                        );
                      } else if (
                        this.state.mainFormStateValues[cell.id] &&
                        Array.isArray(
                          this.state.mainFormStateValues[cell.id]
                        ) &&
                        this.state.mainFormStateValues[cell.id].length
                      ) {
                        return (
                          <div key={index}>
                            <FieldForm index={index} field={cell} />
                            <OrderTable
                              field={cell}
                              orderId={
                                this.state.mainFormStateValues[cell.id][0]
                              }
                              handleChange={this.handleOrderTableChange}
                            />
                          </div>
                        );
                      }
                    }
                    // END --- return order field

                    return <FieldForm field={cell} index={index} key={index} />;
                  })}
                </FormContext>
                <SubmitButtonWrapper>
                  <Button type="submit">Создать</Button>
                </SubmitButtonWrapper>
              </ContainerContext>
            </BlockContent>
          </Block>
        </ReactModal>
      </>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      enumerations: enumerationsSelector(state, { enumerationNames })
    };
  },
  {
    fetchEnumerations,
    createTask
  }
)(AddTask);
