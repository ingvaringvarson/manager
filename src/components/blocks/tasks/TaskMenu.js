import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import Button from "../../../designSystem/atoms/Button";
import CommandsByStatusMenu from "../../common/menus/CommandsByStatusMenu";

import tasksCommands from "../../../commands/tasks";

import { Modal as CreateTask } from "./commands/CreateTask";

class TaskMenu extends PureComponent {
  static propTypes = {
    tasks: PropTypes.array,
    forList: PropTypes.bool,
    useMainCommand: PropTypes.bool,
    useMainForListCommand: PropTypes.bool
  };
  static defaultProps = {
    forList: false,
    useMainForListCommand: false,
    useMainCommand: false
  };

  modalIsNotImplementedFunc(_, c) {
    alert(`Функциональность ${c.id} пока не добавлена`);
  }

  commands = [
    {
      ...tasksCommands.createTaskPrepareToMoveCommand,
      renderModal: handleClose => (
        <CreateTask
          handleClose={handleClose}
          title="Создание задачи подготовить к перемещению"
          defaultValues={
            tasksCommands.createTaskPrepareToMoveCommand.defaultValues
          }
        />
      )
    }
  ];

  getMainCommand() {
    return null;
  }

  getCommandsFiltered = memoizeOne(commands => commands);

  renderButtonForList({ disabled, handleToggle }) {
    return (
      <Button fullWidth onClick={handleToggle} disabled={disabled}>
        Создать задачу
      </Button>
    );
  }

  render() {
    const {
      tasks,
      forList,
      useMainCommand,
      useMainForListCommand
    } = this.props;
    const mainCommandId = this.getMainCommand(tasks, useMainCommand);
    return (
      <CommandsByStatusMenu
        forList={forList}
        entityOrEntities={tasks}
        commands={this.commands}
        mainCommandId={mainCommandId}
        commandsToMenuOptionsFilter={this.getCommandsFiltered}
        renderToggleButton={
          useMainForListCommand ? this.renderButtonForList : null
        }
      />
    );
  }
}

export default TaskMenu;
