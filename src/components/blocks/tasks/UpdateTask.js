import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import styled from "styled-components";

import OrderTableUpdateTask from "./OrderTableUpdateTask";

import Block from "../../../designSystem/organisms/Block";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import Heading from "../../../designSystem/molecules/Heading";
import Button from "../../../designSystem/atoms/Button";

import { formFields } from "../../../fields/formFields/tasks/updateTasksFields";
import { reduceValuesToFormFields } from "../../../fields/helpers";
import { ContainerContext, FormContext } from "../../common/formControls/index";
import FieldForm from "../../common/form/FieldForm";

import { connect } from "react-redux";
import { updateTask } from "../../../ducks/tasks";
import { enumerationsSelector } from "../../../ducks/enumerations";
import { generateId } from "../../../utils";

const SubmitButtonWrapper = styled.div`
  text-align: right;
`;

const enumerationNames = ["taskState", "taskType", "taskReason"];

class UpdateTask extends PureComponent {
  static propTypes = {
    entity: PropTypes.object,
    handleClose: PropTypes.object
  };

  iconProps = {
    onClick: this.props.handleClose,
    width: 24,
    height: 24
  };

  state = {
    formState: this.props.entity,
    productUnit: null
  };

  renderCells = memoizeOne((cells, enumerations, values, formState) => {
    return {
      cells: cells.reduce(
        reduceValuesToFormFields(
          {
            ...values,
            ...formState
          },
          enumerations
        ),
        []
      ),
      key: generateId()
    };
  });

  renderKey = memoizeOne((...args) => generateId());

  handleFormStateChange = state => {
    this.setState({ formState: state.values });
  };

  handleUnitChanged = productUnit => {
    this.setState({ productUnit });
  };

  onSubmitForm = (values, fields) => {
    this.props.updateTask(this.props.entity.id, values, {
      productUnit: this.state.productUnit,
      task: this.props.entity
    });
    this.props.handleClose();
  };

  render() {
    const { cells } = this.renderCells(
      formFields,
      this.props.enumerations,
      this.props.entity,
      this.state.formState
    );
    const key = this.renderKey(
      (Array.isArray(this.state.formState.type) &&
        this.state.formState.type.length &&
        +this.state.formState.type[0]) ||
        +this.state.formState.type,
      (Array.isArray(this.state.formState.state) &&
        this.state.formState.state.length &&
        +this.state.formState.state[0]) ||
        +this.state.formState.state
    );

    return (
      <Block>
        <Heading icon="close" iconProps={this.iconProps}>
          Редактировать
        </Heading>
        <BlockContent>
          <ContainerContext
            key={key}
            onSubmitForm={this.onSubmitForm}
            fields={cells}
            handleStateChanged={this.handleFormStateChange}
            externalForm
          >
            <FormContext>
              {cells.map((cell, index) => {
                if (cell.hasOwnRepresentation) {
                  if (cell.id === "product") {
                    return (
                      <OrderTableUpdateTask
                        field={cell}
                        entity={this.props.entity}
                        formValues={this.state.formState}
                        handleUnitChanged={this.handleUnitChanged}
                      />
                    );
                  }
                }

                return <FieldForm index={index} key={index} field={cell} />;
              })}
              <SubmitButtonWrapper>
                <Button type="submit">Редактировать</Button>
              </SubmitButtonWrapper>
            </FormContext>
          </ContainerContext>
        </BlockContent>
      </Block>
    );
  }
}

export default connect(
  state => {
    return {
      enumerations: enumerationsSelector(state, { enumerationNames })
    };
  },
  {
    updateTask
  }
)(UpdateTask);
