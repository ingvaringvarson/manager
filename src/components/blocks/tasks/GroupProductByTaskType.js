import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { generateId } from "../../../utils";
import {
  TableContainer,
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  ToggleOpenRow,
  renderRowHeadChildrenAfter,
  handleStopLifting
} from "../../common/tableControls";
import Table from "../../common/tableView/Table";
import memoizeOne from "memoize-one";
import TaskTableForProduct from "./TaskTableForProduct";
import TableCell from "../../../designSystem/molecules/TableCell";
import ProductCommandMenu from "./ProductCommandMenu";

class GroupProductByTaskType extends PureComponent {
  static propTypes = {
    tasks: PropTypes.array.isRequired,
    selectedGroup: PropTypes.string.isRequired,
    fields: PropTypes.array.isRequired,
    enumerations: PropTypes.object.isRequired,
    selectedTaskIds: PropTypes.array.isRequired,
    onSelectedProducts: PropTypes.func.isRequired,
    warehouseId: PropTypes.string.isRequired,
    handleChangeSelected: PropTypes.func,
    selectedProductIds: PropTypes.array.isRequired,
    products: PropTypes.array.isRequired
  };

  getOpen = memoizeOne((products, selectedTaskIds) => {
    const result = {
      open: [],
      key: generateId()
    };

    if (!selectedTaskIds.length || !products.length) return result;

    result.open = selectedTaskIds.reduce((ids, id) => {
      const product = products.find(item => {
        const isFind = item.tasks.find(task => task.id === id);
        return isFind;
      });

      if (product && !ids.includes(product._id)) {
        ids.push(product._id);
      }

      return ids;
    }, []);

    return result;
  });

  renderNestedTable = ({ entity }) => {
    return (
      <ToggleOpenRow id={entity._id}>
        <TaskTableForProduct
          onSelectedProducts={this.props.onSelectedProducts}
          product={entity}
          selectedGroup={this.props.selectedGroup}
          warehouseId={this.props.warehouseId}
        />
      </ToggleOpenRow>
    );
  };

  renderCommands = ({ entity }) => {
    return (
      <TableCell control onClick={handleStopLifting}>
        <ProductCommandMenu
          selectedGroup={this.props.selectedGroup}
          warehouseId={this.props.warehouseId}
          task={entity}
          onSelectedProducts={this.props.onSelectedProducts}
        />
      </TableCell>
    );
  };

  render() {
    const { open, key } = this.getOpen(
      this.props.products,
      this.props.selectedTaskIds
    );

    return (
      <TableContainer
        key={key}
        selected={this.props.selectedProductIds}
        open={open}
        canBeSelected
        hasNestedTable
        entities={this.props.products}
        handleChangeSelected={this.props.handleChangeSelected}
      >
        <Table
          key={key}
          cells={this.props.fields}
          entities={this.props.products}
          enumerations={this.props.enumerations}
          hasChildrenBefore
          renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
          renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
          renderRowBodyAfter={this.renderNestedTable}
          hasChildrenAfter
          renderRowBodyChildrenAfter={this.renderCommands}
          renderRowHeadChildrenAfter={renderRowHeadChildrenAfter}
        />
      </TableContainer>
    );
  }
}

export default GroupProductByTaskType;
