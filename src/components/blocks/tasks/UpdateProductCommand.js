import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled from "styled-components";
import {
  entityIdByWarehousePageSelector,
  requestParamsByWarehousePageSelector,
  updateProductTasks
} from "../../../ducks/tasks";

const Title = styled.p`
  cursor: pointer;
`;

class UpdateProductCommand extends Component {
  static propTypes = {
    tasks: PropTypes.array.isRequired,
    selectedGroup: PropTypes.string.isRequired,
    updateProductTasks: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    onCloseMenu: PropTypes.func,
    openForm: PropTypes.bool,
    needKey: PropTypes.bool,
    idEntity: PropTypes.string.isRequired,
    updateParams: PropTypes.object,
    requestParams: PropTypes.object.isRequired,
    typeEntity: PropTypes.string
  };

  static defaultProps = {
    openForm: false,
    needKey: false,
    typeEntity: "warehouse"
  };

  handleUpdateProduct = event => {
    event.preventDefault();
    const {
      selectedGroup,
      tasks,
      idEntity,
      requestParams,
      updateParams,
      openForm,
      needKey,
      onCloseMenu,
      typeEntity
    } = this.props;

    this.props.updateProductTasks(tasks, updateParams, {
      notification: {
        id: this.props.idEntity
      },
      actionResult: {
        id: this.props.idEntity
      },
      fetchTasks: {
        idEntity,
        params: requestParams,
        typeEntity
      },
      openForm,
      needKey,
      selectedGroup
    });

    if (onCloseMenu) onCloseMenu();
  };

  render() {
    return <Title onClick={this.handleUpdateProduct}>{this.props.title}</Title>;
  }
}

function mapStateToProps(state, props) {
  const idEntity = entityIdByWarehousePageSelector(state, props);
  const requestParams = requestParamsByWarehousePageSelector(state, props);

  return {
    idEntity,
    requestParams
  };
}

export default connect(
  mapStateToProps,
  { updateProductTasks }
)(UpdateProductCommand);
