import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp } from "styled-tools";
import CommonMenu from "../../common/menus/CommonMenu";
import Icon from "../../../designSystem/atoms/Icon";
import memoizeOne from "memoize-one";
import Menu from "../../../designSystem/molecules/Menu";
import SearchProductCommand from "./SearchProductCommand";
import UpdateProductCommand from "./UpdateProductCommand";
import SearchProductByOrderCommand from "./SearchProductByOrderCommand";
import { getValueInDepth } from "../../../utils";

const ModalWrapper = styled.div`
  position: relative;
`;

const IconCommands = styled(Icon)`
  position: relative;
  cursor: pointer;
  ${ifProp(
    "disabled",
    css`
      opacity: 0.5;
      cursor: not-allowed;
    `
  )}
`;

const MenuElement = styled.p`
  cursor: pointer;
`;

const MenuWrapper = styled(Menu)`
  position: absolute;
  z-index: 3;
  left: -24px;
  top: 42px;
  width: 250px;
  right: 0;
`;

// ToDo: выделить из CommandsByStatusMenu более общий компонент
// только с customFilter без статусов
class ProductCommandMenu extends PureComponent {
  static propTypes = {
    task: PropTypes.object.isRequired,
    onSelectedProducts: PropTypes.func.isRequired,
    selectedGroup: PropTypes.string.isRequired,
    warehouseId: PropTypes.string.isRequired
  };

  commands = [
    {
      id: "searchProduct",
      title: "Поиск товара",
      blockedGroup: ["11", "18"],
      validators: [
        task => task.tasks.find(item => !item.product_unit_id),
        task => getValueInDepth(task, ["product", "type"]) === 1
      ]
    },
    {
      id: "takeProductWithoutBarCode",
      title: "Принять товар без шк",
      validators: [task => getValueInDepth(task, ["product", "type"]) !== 1]
    },
    {
      id: "searchProductByOrder",
      title: "Принять товар без шк",
      availableGroup: ["18"],
      validators: [
        task => task.tasks.find(item => !item.product_unit_id),
        task => getValueInDepth(task, ["product", "type"]) === 1
      ]
    }
  ];

  getAvailableCommands = memoizeOne((task, selectedGroup) => {
    return this.commands.filter(c => {
      if (c.blockedGroup && c.blockedGroup.includes(selectedGroup))
        return false;

      return c.validators
        ? c.validators.every(v => v(task, selectedGroup))
        : true;
    });
  });

  renderIconCommands = ({ handleToggle }) => {
    return <IconCommands onClick={handleToggle} icon="baseline_more" />;
  };

  renderCommonMenu = ({ handleClose }) => {
    const availableCommands = this.getAvailableCommands(
      this.props.task,
      this.props.selectedGroup
    );

    return (
      <MenuWrapper>
        {availableCommands.map(c => {
          switch (c.id) {
            case "searchProduct": {
              return (
                <SearchProductCommand
                  id={this.props.task.id}
                  title={c.title}
                  task={this.props.task}
                  selectedGroup={this.props.selectedGroup}
                  onSelectedProducts={this.props.onSelectedProducts}
                  warehouseId={this.props.warehouseId}
                  onCloseMenu={handleClose}
                />
              );
            }
            case "takeProductWithoutBarCode": {
              return (
                <UpdateProductCommand
                  id={this.props.task.id}
                  title={c.title}
                  tasks={this.props.task.tasks}
                  selectedGroup={this.props.selectedGroup}
                  onSelectedProducts={this.props.onSelectedProducts}
                  onCloseMenu={handleClose}
                  openForm
                  needKey
                />
              );
            }
            case "searchProductByOrder": {
              return (
                <SearchProductByOrderCommand
                  title={c.title}
                  task={this.props.task}
                  selectedGroup={this.props.selectedGroup}
                  onSelectedProducts={this.props.onSelectedProducts}
                  onCloseMenu={handleClose}
                />
              );
            }
            default: {
              return (
                <MenuElement key={c.id} {...c.props}>
                  {c.title}
                </MenuElement>
              );
            }
          }
        })}
      </MenuWrapper>
    );
  };

  renderMenu = () => {
    const { task, selectedGroup } = this.props;

    const availableIdCommands = this.getAvailableCommands(task, selectedGroup);

    if (!availableIdCommands.length) {
      return <IconCommands disabled icon="baseline_more" />;
    }

    return (
      <CommonMenu renderMenu={this.renderCommonMenu}>
        {this.renderIconCommands}
      </CommonMenu>
    );
  };

  render() {
    return <ModalWrapper>{this.renderMenu()}</ModalWrapper>;
  }
}

export default ProductCommandMenu;
