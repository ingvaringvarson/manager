import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Accordion from "../../../designSystem/molecules/Accordion";
import Block from "../../../designSystem/organisms/Block";
import Heading from "../../../designSystem/molecules/Heading";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import ListItem from "../../common/list/ListItem";
import { mapFormFieldsToQuery } from "../../../fields/helpers";
import { ContainerContext } from "../../common/formControls";
import { groupFieldsForFilterByTaskType } from "../../../fields/formFields/tasks/fieldsForWarehousePage";
import memoizeOne from "memoize-one";
import { generateId } from "../../../utils";
import FieldForm from "../../common/form/FieldForm";

class GroupFilterByTaskType extends PureComponent {
  static propTypes = {
    tasks: PropTypes.array.isRequired,
    groupTaskType: PropTypes.object.isRequired,
    selectedGroup: PropTypes.string.isRequired,
    onChangeFilter: PropTypes.func.isRequired,
    selectedFilter: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired
      })
    ).isRequired
  };

  state = {
    filters: {},
    filteredTasks: [],
    prevTasks: []
  };

  static getDerivedStateFromProps(props, state) {
    if (state.prevTasks !== props.tasks) {
      return {
        prevTasks: props.tasks,
        filters: {},
        filteredTasks: props.tasks
      };
    }

    return null;
  }

  paramsTasksToAgentByOrderGroup = {
    keyByValue: "agent_code",
    keyByTitle: "agent_code",
    maskByTitle: value => `Клиент № ${value}`,
    paramsByItem: {
      keyByValue: "order_code",
      keyByTitle: "order_code",
      maskByTitle: value => `Заказ № ${value}`
    }
  };

  paramsTasksToAgentByPurchaseReturnGroup = {
    keyByValue: "agent_code",
    keyByTitle: "agent_code",
    maskByTitle: value => `Клиент № ${value}`,
    paramsByItem: {
      keyByValue: "purchase_return_code",
      keyByTitle: "purchase_return_code",
      maskByTitle: value => `Возврат № ${value}`
    }
  };

  paramsTasksToAgentByPurchaseOrderGroup = {
    keyByValue: "agent_code",
    keyByTitle: "agent_code",
    maskByTitle: value => `Поставщик № ${value}`,
    paramsByItem: {
      keyByValue: "purchase_order_code",
      keyByTitle: "purchase_order_code",
      maskByTitle: value => `Заказ № ${value}`
    }
  };

  paramsTasksToAgentByReturnGroup = {
    keyByValue: "agent_code",
    keyByTitle: "agent_code",
    maskByTitle: value => `Клиент № ${value}`,
    paramsByItem: {
      keyByValue: "return_code",
      keyByTitle: "return_code",
      maskByTitle: value => `Возврат № ${value}`
    }
  };

  paramsTasksToWarehouseGroup = {
    keyByValue: "warehouse_id",
    keyByTitle: "warehouse_name",
    paramsByItem: null
  };

  paramsTasksToWarehouseToGroup = {
    keyByValue: "warehouse_to_id",
    keyByTitle: "warehouse_to_name",
    paramsByItem: null
  };

  paramsTasksToCodeList = {
    keyByValue: "code",
    keyByTitle: "code",
    paramsByItem: null
  };

  mapTasksToCustomGroup = (tasks, params) => {
    const { keyByValue, keyByTitle, maskByTitle, paramsByItem } = params;

    return tasks.reduce(
      (result, task) => {
        if (
          !task.hasOwnProperty(keyByValue) ||
          result.itemIds.includes(task[keyByValue])
        ) {
          return result;
        }

        result.itemIds.push(task[keyByValue]);

        const group = {
          key: keyByValue,
          value: task[keyByValue],
          title: maskByTitle ? maskByTitle(task[keyByTitle]) : task[keyByTitle]
        };

        if (paramsByItem) {
          const { items } = tasks.reduce(
            (resultItems, taskByGroup) => {
              if (
                task[keyByValue] === taskByGroup[keyByValue] &&
                !resultItems.itemIds.includes(
                  taskByGroup[paramsByItem.keyByValue]
                )
              ) {
                resultItems.itemIds.push(taskByGroup[paramsByItem.keyByValue]);

                const item = {
                  key: paramsByItem.keyByValue,
                  value: taskByGroup[paramsByItem.keyByValue],
                  title: paramsByItem.maskByTitle
                    ? paramsByItem.maskByTitle(
                        taskByGroup[paramsByItem.keyByTitle]
                      )
                    : taskByGroup[paramsByItem.keyByTitle]
                };

                resultItems.items.push(item);
              }

              return resultItems;
            },
            { items: [], itemIds: [] }
          );

          group.items = items;
        }

        result.items.push(group);

        return result;
      },
      { items: [], itemIds: [] }
    );
  };

  getGroups() {
    switch (this.props.selectedGroup) {
      case "1_4":
      case "8_9":
        return this.mapTasksToCustomGroup(
          this.state.filteredTasks,
          this.paramsTasksToAgentByOrderGroup
        );
      case "2_3":
        return this.mapTasksToCustomGroup(
          this.state.filteredTasks,
          this.paramsTasksToWarehouseToGroup
        );
      case "16_17":
        return this.mapTasksToCustomGroup(
          this.state.filteredTasks,
          this.paramsTasksToAgentByPurchaseReturnGroup
        );
      case "3_5":
        return this.mapTasksToCustomGroup(
          this.state.filteredTasks,
          this.paramsTasksToWarehouseGroup
        );
      case "11":
        return this.mapTasksToCustomGroup(
          this.state.filteredTasks,
          this.paramsTasksToAgentByPurchaseOrderGroup
        );
      case "18":
        return this.mapTasksToCustomGroup(
          this.state.filteredTasks,
          this.paramsTasksToAgentByReturnGroup
        );
      case "7":
        return this.mapTasksToCustomGroup(
          this.state.filteredTasks,
          this.paramsTasksToCodeList
        );
      default:
        return { items: [], itemIds: [] };
    }
  }

  handleChangeFilter = params => {
    if (params.id && params.value) {
      this.props.onChangeFilter({ key: params.id, value: params.value });
    }
  };

  renderTasks() {
    const { selectedFilter } = this.props;
    const { items } = this.getGroups();
    const keyForm = this.generateKeyForm(
      this.props.tasks,
      this.props.selectedGroup
    );

    return items.map(group => {
      const active = selectedFilter.length
        ? selectedFilter.some(item => {
            return (
              (item.key === group.key && item.value === group.value) ||
              (group.items &&
                group.items.some(i =>
                  selectedFilter.some(
                    f => f.key === i.key && f.value === i.value
                  )
                ))
            );
          })
        : false;

      if (group.items) {
        return (
          <Accordion
            active={active}
            title={group.title}
            key={`${group.value}/${keyForm}`}
            id={group.key}
            value={group.value}
            onToggle={this.handleChangeFilter}
            marginBottom="10px"
          >
            {group.items.map(itemByGroup => {
              const active = selectedFilter.length
                ? selectedFilter.some(
                    item =>
                      item.key === itemByGroup.key &&
                      item.value === itemByGroup.value
                  )
                : false;
              return (
                <ListItem
                  active={active}
                  key={`${itemByGroup.value}/${keyForm}`}
                  id={itemByGroup.key}
                  value={itemByGroup.value}
                  onClick={this.handleChangeFilter}
                >
                  {itemByGroup.title}
                </ListItem>
              );
            })}
          </Accordion>
        );
      }

      return (
        <ListItem
          withBorder
          active={active}
          key={`${group.value}/${keyForm}`}
          id={group.key}
          value={group.value}
          onClick={this.handleChangeFilter}
        >
          {group.title}
        </ListItem>
      );
    });
  }

  handleSubmitForm = (values, fields) => {
    const filters = mapFormFieldsToQuery(fields, values);
    const filterKeys = Object.keys(filters);
    let filteredTasks = this.props.tasks;

    if (filterKeys.length) {
      filteredTasks = this.props.tasks.filter(task => {
        return filterKeys.every(key => task[key] === filters[key]);
      });
    }

    this.setState({ filters, filteredTasks });
  };

  generateKeyForm = memoizeOne(() => generateId());

  renderFilters() {
    const fields = groupFieldsForFilterByTaskType[this.props.selectedGroup];

    if (!fields) return null;

    const keyForm = this.generateKeyForm(
      this.props.tasks,
      this.props.selectedGroup
    );

    return (
      <ContainerContext
        key={keyForm}
        fields={fields}
        onSubmitForm={this.handleSubmitForm}
        autoSubmit
        submitValidation
      >
        {fields.map((filter, index) => (
          <FieldForm key={index} field={filter} index={index} />
        ))}
      </ContainerContext>
    );
  }

  render() {
    const { selectedGroup, groupTaskType, tasks } = this.props;

    const title =
      selectedGroup && groupTaskType && groupTaskType[selectedGroup]
        ? groupTaskType[selectedGroup].name
        : null;

    return (
      <Block>
        {title && <Heading title={title} />}
        <BlockContent>
          {tasks && tasks.length ? (
            <>
              {this.renderFilters()}
              {this.renderTasks()}
            </>
          ) : (
            "Нет данных"
          )}
        </BlockContent>
      </Block>
    );
  }
}

export default GroupFilterByTaskType;
