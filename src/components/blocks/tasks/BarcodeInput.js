import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Input from "../../../designSystem/atoms/Input";
import Icon from "../../../designSystem/atoms/Icon";

import { connect } from "react-redux";
import {
  fetchProductsInStock,
  productsInStockUnitsSelector,
  productsInStockLoadingSelector
} from "../../../ducks/productsInStock";

const StyledLabel = styled.p`
  font-size: 14px;
  color: #343434;
  margin: 10px 0;
`;
const InputWrapper = styled.div`
  position: relative;
`;
const StyledIcon = styled(Icon)`
  position: absolute;
  width: 24px;
  height: 24px;
  top: 50%;
  left: 10px;
  transform: translateY(-50%);
`;

const StyledInput = styled(Input)`
  padding-left: 40px;
`;

class BarcodeInput extends PureComponent {
  static propTypes = {
    field: PropTypes.shape({ id: PropTypes.string }),
    handleUnitChanged: PropTypes.func,
    productUnit: PropTypes.object
  };

  state = {
    barcodeValue: "",
    isLoaded: false,
    isLoading: false
  };

  static getDerivedStateFromProps(props, state) {
    if (props.isProductLoading !== state.isLoading) {
      if (!props.isProductLoading) {
        return {
          isLoaded: true,
          isLoading: props.isProductLoading
        };
      }
      return {
        isLoading: props.isProductLoading
      };
    }
    return null;
  }

  componentDidMount = () => {
    if (this.props.positions[0].product.type !== 1) {
      this.props.fetchProductsInStock(this.props.field.id, {
        warehouse_id: this.props.formValues.warehouse_id,
        state: [1]
      });
    }
  };

  componentDidUpdate = prevProps => {
    if (
      prevProps.productUnit !== this.props.productUnit &&
      this.props.handleUnitChanged
    ) {
      this.props.handleUnitChanged(this.props.productUnit, this.props.field);
    }
  };

  handleKeyPress = event => {
    const keyCode = event.keyCode;

    if (keyCode === 13) {
      event.preventDefault();

      this.props.fetchProductsInStock(this.props.field.id, {
        ean_13: [this.state.barcodeValue],
        warehouse_id: this.props.formValues.warehouse_id,
        state: [1]
      });
    }
  };

  handleBarcodeChange = event => {
    const { value } = event.target;

    this.setState({ barcodeValue: value, isLoaded: false });
  };

  render() {
    const { attrs } = this.props.field.formControlProps;
    return (
      <div>
        <StyledLabel>Отсканируйте штрих-код</StyledLabel>
        <InputWrapper>
          <StyledInput
            width="100%"
            {...attrs}
            placeholder="Отсканируйте штрих-код"
            name={this.props.field.id}
            onKeyDown={this.handleKeyPress}
            onChange={this.handleBarcodeChange}
            value={this.state.barcodeValue}
            disabled={this.props.positions[0].product.type !== 1}
          />
          <StyledIcon
            icon={
              this.props.productUnit && this.state.isLoaded ? "done" : "barcode"
            }
          />
        </InputWrapper>
        {this.state.isLoaded && !this.props.productUnit ? (
          this.props.positions[0].product.type === 1 ? (
            <StyledLabel>
              Товар с указанным штрих-кодом числится в не верном статусе или на
              другом складе. Выберите другую единицу товара в статусе "На
              складе"
            </StyledLabel>
          ) : (
            <StyledLabel>
              На складе не числится ни одной единицы товара в статусе "На
              складе"
            </StyledLabel>
          )
        ) : null}
      </div>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      productUnit: productsInStockUnitsSelector(state, { id: props.field.id }),
      isProductLoading: productsInStockLoadingSelector(state)
    };
  },
  {
    fetchProductsInStock
  }
)(BarcodeInput);
