import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import Pagination from "../Pagination";
import { Table } from "../../common/tableView";
import {
  TableContainer,
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore
} from "../../common/tableControls";
import { getUrl } from "../../../helpers/urls";

const propTypes = {
  requestParams: PropTypes.object.isRequired,
  keyRouter: PropTypes.string.isRequired,
  fields: PropTypes.arrayOf(PropTypes.object).isRequired,
  tasks: PropTypes.arrayOf(PropTypes.object).isRequired,
  enumerations: PropTypes.object,
  push: PropTypes.func.isRequired,
  pathname: PropTypes.string
};

const defaultProps = {
  maxPages: 99
};

class TasksTable extends PureComponent {
  handleMoveToTaskPage = entity => () => {
    if (entity) this.props.push(getUrl("task", { id: entity.id }));
  };

  rowBodyHandlers = {
    onClick: this.handleMoveToTaskPage
  };

  render() {
    const {
      tasks,
      enumerations,
      requestParams,
      pathname,
      push,
      maxPages,
      fields
    } = this.props;

    return (
      <>
        <TableContainer canBeSelected entities={tasks}>
          <Table
            hasFormControl
            entities={tasks}
            cells={fields}
            enumerations={enumerations}
            hasChildrenBefore
            renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
            renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
            renderRowFormControlChildrenBefore={
              renderRowFormControlChildrenBefore
            }
            rowBodyHandlers={this.rowBodyHandlers}
          />
        </TableContainer>
        <Pagination
          requestParams={requestParams}
          pathname={pathname}
          push={push}
          page={+requestParams.page}
          maxPages={maxPages}
        />
      </>
    );
  }
}

TasksTable.propTypes = propTypes;
TasksTable.defaultProps = defaultProps;

export default TasksTable;
