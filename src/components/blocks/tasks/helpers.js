import { generateId, getValueInDepth, objectToArray } from "../../../utils";

export function getParamsByUpdate(selectedGroup, tasks) {
  const params = { openForm: false, state: 2 };

  if (!Array.isArray(tasks)) return params;

  switch (true) {
    case selectedGroup === "8_9" &&
      tasks.every(t => t.unit && t.unit.state === 11): {
      params.state = 7;
      break;
    }
    case selectedGroup === "18" &&
      tasks.every(t => t.unit && t.unit.state === 7):
    case selectedGroup === "2_3" &&
      tasks.every(t => t.unit && t.unit.state === 3):
    case selectedGroup === "7" &&
      tasks.every(t => t.unit && t.unit.state !== 1): {
      params.openForm = true;
      break;
    }
    case selectedGroup === "11" && tasks.every(t => t.task.ean_13):
    case selectedGroup === "16_17" &&
      tasks.every(t => t.unit && t.unit.state === 13):
    case selectedGroup === "1_4" &&
      tasks.every(t => t.unit && t.unit.state === 2): {
      params.openForm = true;
      params.needKey = true;
      break;
    }
  }

  return params;
}

export function getTextParams(tasks, selectedGroup) {
  const params = {
    title: null,
    btnTitle: null
  };

  switch (selectedGroup) {
    case "1_4": {
      if (tasks.every(task => task.unit && task.unit.state === 2)) {
        params.title = "Подтвердить, что товар выдан";
        params.btnTitle = "Выдать товар";
        break;
      } else if (tasks.every(task => task.unit && task.unit.state === 1)) {
        params.title = "Подтвердить, что товар полностью готов к выдаче";
        params.btnTitle = "Готов к выдаче";
        break;
      }
      break;
    }
    case "8_9": {
      if (tasks.every(task => task.unit && task.unit.state === 5)) {
        params.title = "Подтвердить, что товар выдан";
        params.btnTitle = "Выдать товар";
        break;
      } else if (tasks.every(task => task.unit && task.unit.state === 1)) {
        params.title = "Подтвердить, что товар полностью готов к выдаче";
        params.btnTitle = "Готов к доставке";
        break;
      } else if (tasks.every(task => task.unit && task.unit.state === 11)) {
        params.title = "Подтвердить, что товар выдан курьеру";
        params.btnTitle = "Передать в доставку";
        break;
      }
      break;
    }
    case "2_3": {
      if (tasks.every(task => task.unit && task.unit.state === 1)) {
        params.title = "Подтвердить, что товар полностью готов к выдаче";
        params.btnTitle = "Готов к перемещению";
        break;
      } else if (tasks.every(task => task.unit && task.unit.state === 3)) {
        params.title = "Подтвердить, что товар выдан для перемещения";
        params.btnTitle = "Передать в перемещение";
        break;
      }
      break;
    }
    case "16_17": {
      if (tasks.every(task => task.unit && task.unit.state === 13)) {
        params.title = "Подтвердить, что товар выдан";
        params.btnTitle = "Выдать товар поставщику";
        break;
      } else if (tasks.every(task => task.unit && task.unit.state === 1)) {
        params.title = "Подтвердить, что товар полностью готов к выдаче";
        params.btnTitle = "Готов к выдаче поставщику";
        break;
      }
      break;
    }
    case "3_5": {
      if (tasks.every(task => task.unit && task.unit.state === 6)) {
        params.title = "Подтвердить, что товар принят";
        params.btnTitle = "Принять товар";
        break;
      } else if (tasks.every(task => task.unit && task.unit.state === 4)) {
        params.title = "Подтвердить, что товар выдан курьером";
        params.btnTitle = "Готов к приемке";
        break;
      }
      break;
    }
    case "18": {
      if (tasks.every(task => task.unit && task.unit.state === 7)) {
        params.title = "Подтвердить, что товар принят";
        params.btnTitle = "Принять товар";
        break;
      }
      break;
    }
    case "7": {
      if (tasks.every(task => task.unit && task.unit.state !== 1)) {
        params.title = "Подтвердить, что товар возвращен на склад";
        params.btnTitle = "Вернуть товар на склад";
        break;
      }
      break;
    }
    case "11": {
      if (tasks.every(task => task.task && task.task.ean_13)) {
        params.btnTitle = "Заказ готов к приемке";
        break;
      }
      break;
    }
    default:
      return params;
  }

  return params;
}

export function mapProductsToTasks(tasks, products) {
  const tasksWithProducts = tasks.map((task, index) => {
    let unit = null;

    if (task.product_unit_id) {
      products.some(item =>
        item.product_in_stock.some(productInStock =>
          productInStock.product_units.some(productUnit => {
            if (productUnit.id === task.product_unit_id) {
              return (unit = productUnit);
            }

            return false;
          })
        )
      );
    }

    return {
      task,
      unit,
      index: index + 1
    };
  });

  return {
    tasksWithProducts,
    key: generateId()
  };
}

const keysTasksToGroupCustom = [
  ["product", "article"],
  ["product", "brand"],
  "state",
  "type"
];

const keysTasksToGroupOrderCode = keysTasksToGroupCustom.concat("order_code");

const keysTasksToGroupProductName = keysTasksToGroupCustom.concat([
  ["product", "name"]
]);

const keysTasksToGroupPurchaseReturnCode = keysTasksToGroupCustom.concat(
  "purchase_return_code"
);

const keysTasksToGroupPurchaseOrderCode = keysTasksToGroupCustom.concat(
  "purchase_order_code"
);

export function mapTasksToProducts(selectedGroup, tasks) {
  switch (selectedGroup) {
    case "1_4":
    case "8_9":
    case "18":
      return mapTasksToCustomGroup(tasks, keysTasksToGroupOrderCode);
    case "2_3":
    case "3_5":
      return mapTasksToCustomGroup(tasks, keysTasksToGroupProductName);
    case "16_17":
      return mapTasksToCustomGroup(tasks, keysTasksToGroupPurchaseReturnCode);
    case "11":
      return mapTasksToCustomGroup(tasks, keysTasksToGroupPurchaseOrderCode);
    case "7":
      return mapTasksToCustomGroup(tasks, keysTasksToGroupCustom);
    default:
      return [];
  }
}

export function mapTasksToCustomGroup(tasks, keys) {
  const productGroup = tasks.reduce((products, task) => {
    const key = keys.reduce((keyString, keyPath) => {
      if (Array.isArray(keyPath)) {
        return `${keyString}/${getValueInDepth(task, keyPath)}`;
      }

      return `${keyString}/${task[keyPath]}`;
    }, "");

    if (products[key]) {
      products[key].tasks = products[key].tasks.concat(task);
    } else {
      products[key] = {
        ...task,
        tasks: [task]
      };
    }

    return products;
  }, {});

  return objectToArray(productGroup);
}
