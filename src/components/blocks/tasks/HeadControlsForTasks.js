import React, { PureComponent } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import Row from "../../../designSystem/templates/Row";
import Column from "../../../designSystem/templates/Column";
import IconButton from "../../../designSystem/molecules/IconButton";

import {
  fetchProductsInStock,
  productsInStockSelector
} from "../../../ducks/productsInStock";
import { getParamsByUpdate, mapProductsToTasks } from "./helpers";
import {
  entityIdByWarehousePageSelector,
  requestParamsByWarehousePageSelector,
  updateProductTasks
} from "../../../ducks/tasks";
import PrintingForms from "./PrintingForms";

const typeEntity = "warehouse";

class HeadControlsForTasks extends PureComponent {
  static propTypes = {
    tasks: PropTypes.array.isRequired,
    fetchProductsInStock: PropTypes.func.isRequired,
    selectedGroup: PropTypes.string,
    id: PropTypes.string.isRequired,
    idEntity: PropTypes.string.isRequired,
    requestParams: PropTypes.object.isRequired,
    updateProductTasks: PropTypes.func.isRequired,
    products: PropTypes.arrayOf(PropTypes.object),
    selectedProductIds: PropTypes.arrayOf(PropTypes.string)
  };

  componentDidMount() {
    const { hasNull, productUnitIds } = this.getProductUnitIds(
      this.props.tasks
    );

    if (!hasNull && productUnitIds.length) {
      this.props.fetchProductsInStock(this.props.id, {
        product_unit_id: productUnitIds
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.id !== prevProps.id ||
      this.props.tasks !== prevProps.tasks
    ) {
      const { hasNull, productUnitIds } = this.getProductUnitIds(
        this.props.tasks
      );

      if (!hasNull && productUnitIds.length) {
        this.props.fetchProductsInStock(this.props.id, {
          product_unit_id: productUnitIds
        });
      }
    }
  }

  getProductUnitIds = memoizeOne(tasks => {
    let hasNull = false;

    const productUnitIds = tasks.reduce((ids, task) => {
      if (task.product_unit_id) {
        return ids.concat(task.product_unit_id);
      }
      hasNull = true;
      return ids;
    }, []);

    return { hasNull, productUnitIds };
  });

  testProductUnitsState = (state, reverse) => {
    return this.props.productsInStock.length
      ? this.props.productsInStock.every(product => {
          return product.product_in_stock.every(productInStock => {
            return productInStock.product_units.every(unit =>
              reverse ? unit.state !== state : unit.state === state
            );
          });
        })
      : false;
  };

  testProductBarCode = reverse => {
    return this.props.tasks.length
      ? this.props.tasks.every(t => {
          return reverse ? !t.ean_13 : t.ean_13;
        })
      : false;
  };

  renderCustomButton = (
    text,
    disabled = false,
    iconName = "hand_with_good"
  ) => {
    return (
      <IconButton
        disabled={disabled}
        icon={iconName}
        secondary
        onClick={this.handleUpdateTasks}
      >
        {text}
      </IconButton>
    );
  };

  handleUpdateTasks = () => {
    const {
      selectedGroup,
      productsInStock,
      tasks,
      idEntity,
      requestParams
    } = this.props;
    const { tasksWithProducts } = mapProductsToTasks(tasks, productsInStock);
    const { openForm, needKey, ...updateParams } = getParamsByUpdate(
      selectedGroup,
      tasksWithProducts
    );

    this.props.updateProductTasks(tasks, updateParams, {
      notification: {
        id: this.props.idEntity
      },
      actionResult: {
        id: this.props.idEntity
      },
      fetchTasks: {
        idEntity,
        params: requestParams,
        typeEntity
      },
      openForm,
      needKey,
      selectedGroup
    });
  };

  renderUpdateButton() {
    const { selectedGroup } = this.props;

    switch (selectedGroup) {
      case "1_4": {
        if (this.testProductUnitsState(1)) {
          return this.renderCustomButton("Заказ готов\n к выдаче", false);
        } else if (this.testProductUnitsState(2)) {
          return this.renderCustomButton("Выдать товар");
        }

        return this.renderCustomButton("Заказ готов\n к выдаче", true);
      }
      case "8_9": {
        if (this.testProductUnitsState(1)) {
          return this.renderCustomButton("Заказ готов\n к доставке");
        } else if (this.testProductUnitsState(5)) {
          return this.renderCustomButton("Выдать товар");
        } else if (this.testProductUnitsState(11)) {
          return this.renderCustomButton("Передать\n в доставку");
        }

        return this.renderCustomButton("Заказ готов\n к доставке", true);
      }
      case "2_3": {
        if (this.testProductUnitsState(1)) {
          return this.renderCustomButton("Готов\n к перемещению");
        } else if (this.testProductUnitsState(3)) {
          return this.renderCustomButton("Передать\n в перемещение");
        }

        return this.renderCustomButton("Готов\n к перемещению", true);
      }
      case "16_17": {
        if (this.testProductUnitsState(1)) {
          return this.renderCustomButton("Заказ готов\n к выдаче");
        } else if (this.testProductUnitsState(13)) {
          return this.renderCustomButton("Выдать товар\n поставщику");
        }

        return this.renderCustomButton("Заказ готов\n к выдаче", true);
      }
      case "3_5": {
        if (this.testProductUnitsState(4)) {
          return this.renderCustomButton("Готов к приемке");
        } else if (this.testProductUnitsState(3)) {
          return this.renderCustomButton("Принять товар");
        }

        return this.renderCustomButton("Готов к приемке", true);
      }
      case "18": {
        if (this.testProductUnitsState(7)) {
          return this.renderCustomButton("Принять товар");
        }

        return this.renderCustomButton("Принять товар", true);
      }
      case "7": {
        if (this.testProductUnitsState(1, true)) {
          return this.renderCustomButton("Вернуть товар\n на склад");
        }

        return this.renderCustomButton("Вернуть товар\n на склад", true);
      }
      case "11": {
        if (this.testProductBarCode()) {
          return this.renderCustomButton("Заказ готов\n к приемке");
        }

        return this.renderCustomButton("Заказ готов\n к приемке", true);
      }
      default:
        return null;
    }
  }

  getTasksByPrint = memoizeOne((products, selectedProductIds) => {
    return Array.isArray(products) &&
      Array.isArray(selectedProductIds) &&
      products.length &&
      selectedProductIds.length
      ? products.reduce((ts, p) => {
          if (selectedProductIds.includes(p._id)) {
            return ts.concat(p.tasks.filter(t => t.state === 1));
          }

          return ts;
        }, [])
      : [];
  });

  renderPrintButton = (onClickCallback, formIsAvailable) => {
    return (
      <IconButton
        icon="printer"
        simple
        onClick={onClickCallback}
        disabled={!formIsAvailable}
      >
        Печать
        <br /> документов
      </IconButton>
    );
  };

  render() {
    const tasks = this.getTasksByPrint(
      this.props.products,
      this.props.selectedProductIds
    );

    return (
      <Row contentCenter justifyAround>
        <Column>{this.renderUpdateButton()}</Column>
        <Column>
          <PrintingForms tasks={tasks} renderButton={this.renderPrintButton} />
        </Column>
      </Row>
    );
  }
}

function mapStateToProps(state, props) {
  const idEntity = entityIdByWarehousePageSelector(state, props);
  const requestParams = requestParamsByWarehousePageSelector(state, props);

  return {
    requestParams,
    idEntity,
    productsInStock: productsInStockSelector(state, props.id)
  };
}

export default connect(
  mapStateToProps,
  { fetchProductsInStock, updateProductTasks }
)(HeadControlsForTasks);
