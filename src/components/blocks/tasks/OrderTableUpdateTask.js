import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import nanoId from "nanoid";

import { connect } from "react-redux";
import {
  fetchOrder,
  orderPositionByIdArray,
  orderInfoSelector
} from "../../../ducks/orders";
import { productsInStockUnitsSelector } from "../../../ducks/productsInStock";

import Table from "../../common/tableView/Table";
import { fields } from "../../../fields/formFields/tasks/updateTaskOrderPosititions";
import { reduceValuesToFormFields } from "../../../fields/helpers";

import BarcodeInput from "./BarcodeInput";

class OrderTabkeUpdateTask extends PureComponent {
  static propTypes = {
    entity: PropTypes.shape({
      id: PropTypes.string,
      order_position_id: PropTypes.string,
      order_id: PropTypes.string
    }),
    formValues: PropTypes.object,
    field: PropTypes.object,
    fetchEntity: PropTypes.func,
    orderData: PropTypes.object,
    handleUnitChanged: PropTypes.func
  };

  componentDidMount = () => {
    this.props.fetchOrder(this.props.entity.order_id, {
      id: this.props.entity.order_id
    });
  };

  renderCells = memoizeOne((values, enumerations, productUnit) => {
    return {
      cells: fields.reduce(
        reduceValuesToFormFields(
          {
            ...values,
            productUnit
          },
          enumerations
        ),
        []
      ),
      key: nanoId()
    };
  });

  render() {
    if (
      (!this.props.orderData && !this.props.positions) ||
      (Array.isArray(this.props.positions) && !this.props.positions.length)
    ) {
      return null;
    }

    const { cells, key } = this.renderCells(
      this.props.orderData,
      this.props.enumerations,
      this.props.productUnit
    );

    return (
      <>
        <BarcodeInput
          {...this.props}
          handleUnitChanged={this.props.handleUnitChanged}
        />
        <Table entities={this.props.positions} cells={cells} key={key} />
      </>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      orderData: orderInfoSelector(state, { id: props.entity.order_id }),
      positions: orderPositionByIdArray(state, {
        positionId: props.entity.order_position_id,
        id: props.entity.order_id
      }),
      productUnit: productsInStockUnitsSelector(state, { id: props.field.id })
    };
  },
  {
    fetchOrder
  }
)(OrderTabkeUpdateTask);
