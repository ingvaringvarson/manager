import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import IconField from "../../../designSystem/molecules/IconField";
import Input from "../../../designSystem/atoms/Input";
import styled from "styled-components";
import { palette } from "styled-tools";
import {
  fetchProductsInStock,
  productsInStockSelector
} from "../../../ducks/productsInStock";
import { addNotification } from "../../../ducks/logger";
import memoizeOne from "memoize-one";
import { getValueInDepth } from "../../../utils";

const StyledInput = styled(Input)`
  width: 100%;
  height: 42px;
  padding-left: 40px;
`;

const ErrorMsg = styled.span`
  position: absolute;
  top: calc(50% - 5px);
  right: 25px;
  color: ${palette("primary", 0)};
  font-size: 10px;
`;

class SearchBarCodeForm extends Component {
  static propTypes = {
    tasks: PropTypes.array.isRequired,
    id: PropTypes.string.isRequired,
    selectedGroup: PropTypes.string.isRequired,
    onSelectedProducts: PropTypes.func.isRequired,
    warehouseId: PropTypes.string.isRequired,
    selectedProductIds: PropTypes.array,
    addNotification: PropTypes.func.isRequired,
    products: PropTypes.array.isRequired
  };

  state = {
    barCode: "",
    prevId: null
  };

  static getDerivedStateFromProps(props, state) {
    if (state.prevId !== props.id) {
      return {
        barCode: "",
        prevId: props.id
      };
    }

    return null;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.productsInStock !== this.props.productsInStock) {
      const {
        selectedTaskIds,
        productsFound,
        selectedProductIds
      } = this.getPayloadByProductsFound(this.props.productsInStock);

      if (selectedTaskIds || productsFound) {
        this.props.onSelectedProducts(
          selectedTaskIds,
          productsFound,
          selectedProductIds
        );
        this.setState({ barCode: "" });
      }
    }
  }

  handleSubmitForm = event => {
    event.preventDefault();
    if (
      this.props.selectedGroup === "11" &&
      !this.props.selectedProductIds.length
    ) {
      this.props.addNotification({ message: "Выберите товар" });
    } else if (this.state.barCode) {
      this.props.fetchProductsInStock(this.props.id, {
        ean_13: [this.state.barCode.trim()]
      });
    }
  };

  handleChangeBarCode = event => {
    this.setState({ barCode: event.target.value });
  };

  getPayloadByProductsFound = memoizeOne(() => {
    const payload = {
      error: null,
      selectedTaskIds: null,
      productsFound: null,
      selectedProductIds: null
    };

    if (!this.state.barCode) return payload;

    const entity = getValueInDepth(this.props, ["productsInStock", 0]);
    const productInStock = getValueInDepth(entity, ["product_in_stock", 0]);

    switch (true) {
      case this.props.selectedGroup === "11": {
        if (this.props.productsInStock.length) {
          payload.error =
            "Товар с указанным штрих-кодом уже есть  в системе. Укажите другой штрих-код.";

          return payload;
        }
        break;
      }
      case this.props.selectedGroup === "7": {
        if (!this.props.productsInStock.length) {
          payload.error =
            "Товар с указанным штрих-кодом не найден. Укажите другую единицу товара.";

          return payload;
        }

        if (
          !productInStock.product_units[0] ||
          (productInStock.product_units[0].state !== 12 &&
            productInStock.product_units[0].state !== 7)
        ) {
          payload.error =
            "Указанный товар не в требуемом статусе. Укажите другую единицу товара.";

          return payload;
        }
        break;
      }
      case this.props.selectedGroup === "18": {
        if (!this.props.productsInStock.length) {
          payload.error =
            "Товар с указанным штрих-кодом не найден. Укажите другую единицу товара.";

          return payload;
        }

        if (
          !productInStock.product_units[0] ||
          productInStock.product_units[0].state !== 7
        ) {
          payload.error =
            "Указанный товар не в требуемом статусе. Укажите другую единицу товара в статусе 'Выдан клиенту'.";

          return payload;
        }
        break;
      }
      default: {
        if (!this.props.productsInStock.length) {
          payload.error =
            "Товар с указанным штрих-кодом не найден. Укажите другую единицу товара.";

          return payload;
        }

        if (
          !productInStock ||
          productInStock.warehouse_id !== this.props.warehouseId
        ) {
          payload.error =
            "Указанный товар числится на другом складе. Укажите другую единицу товара.";

          return payload;
        }

        if (
          !productInStock.product_units[0] ||
          productInStock.product_units[0].state !== 1
        ) {
          payload.error =
            "Указанный товар не в требуемом статусе. Укажите другую единицу товара в статусе 'на складе'.";

          return payload;
        }
      }
    }

    switch (this.props.selectedGroup) {
      case "16_17":
      case "18":
      case "1_4":
      case "8_9":
      case "2_3": {
        const task = this.props.tasks.find(
          t =>
            t.state === 1 &&
            !t.product_unit_id &&
            t.product.type === 1 &&
            t.product.article === entity.product.article &&
            t.product.brand === entity.product.brand
        );

        if (task) {
          payload.selectedTaskIds = [task.id];
          payload.productsFound = [[task.id, productInStock.product_units[0]]];
        } else {
          payload.error =
            "Указанного товара нет в этих заказах. Укажите другую единицу товара.";
        }

        break;
      }
      case "11": {
        let selectedProductIds = [];
        let selectedTask = null;
        let beforeSelected = true;

        this.props.products.forEach(p => {
          const tasksWithoutBarCode = p.tasks.filter(
            t =>
              t.state === 1 &&
              !t.product_unit_id &&
              !t.ean_13 &&
              t.product.type === 1
          );
          if (this.props.selectedProductIds.includes(p._id)) {
            beforeSelected = false;
            if (!selectedTask) {
              if (tasksWithoutBarCode.length) {
                selectedTask = tasksWithoutBarCode[0];

                if (tasksWithoutBarCode.length > 1) {
                  selectedProductIds.push(p._id);
                }
              }
            } else {
              if (tasksWithoutBarCode.length) {
                selectedProductIds.push(p._id);
              }
            }
          } else {
            if (
              !selectedProductIds.length &&
              tasksWithoutBarCode.length &&
              !beforeSelected
            ) {
              selectedProductIds.push(p._id);
            }
          }
        });

        if (selectedTask) {
          payload.selectedTaskIds = [selectedTask.id];
          payload.productsFound = [
            [selectedTask.id, { ean_13: this.state.barCode }]
          ];
          payload.selectedProductIds = selectedProductIds;
        } else {
          payload.error = "Не найден подходящий товар.";
        }

        break;
      }
      default: {
        payload.error =
          "Указанного товара нет в этих заказах. Укажите другую единицу товара.";

        return payload;
      }
    }

    return payload;
  });

  render() {
    const { error } = this.getPayloadByProductsFound(
      this.props.productsInStock
    );

    return (
      <form onSubmit={this.handleSubmitForm}>
        <IconField icon="barcode">
          <StyledInput
            onChange={this.handleChangeBarCode}
            invalid={error}
            placeholder="Отсканируйте штрих-код"
            type="text"
            value={this.state.barCode}
          />
          {!!error && <ErrorMsg>{error}</ErrorMsg>}
        </IconField>
      </form>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    productsInStock: productsInStockSelector(state, props.id)
  };
}

export default connect(
  mapStateToProps,
  { fetchProductsInStock, addNotification }
)(SearchBarCodeForm);
