import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled from "styled-components";
import {
  fetchProductsInStock,
  productsInStockSelector
} from "../../../ducks/productsInStock";
import memoizeOne from "memoize-one";
import { addNotification } from "../../../ducks/logger";

const Title = styled.p`
  cursor: pointer;
`;

class SearchProductCommand extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    task: PropTypes.object.isRequired,
    selectedGroup: PropTypes.string.isRequired,
    onSelectedProducts: PropTypes.func.isRequired,
    warehouseId: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    onCloseMenu: PropTypes.func
  };

  componentDidUpdate(prevProps) {
    if (prevProps.productsInStock !== this.props.productsInStock) {
      const {
        selectedTaskIds,
        productsFound,
        error
      } = this.getPayloadByProductsFound(this.props.productsInStock);

      if (selectedTaskIds || productsFound) {
        this.props.onSelectedProducts(selectedTaskIds, productsFound);
        if (this.props.onCloseMenu) this.props.onCloseMenu();
      } else if (error) {
        this.props.addNotification({ id: this.props.id, message: error });
        if (this.props.onCloseMenu) this.props.onCloseMenu();
      }
    }
  }

  handleSearchProduct = event => {
    event.preventDefault();

    if (!this.props.task.product_unit_id) {
      this.props.fetchProductsInStock(this.props.id, {
        state: [1],
        warehouse_id: this.props.warehouseId,
        product: [
          {
            article: this.props.task.product.article,
            brand: this.props.task.product.brand
          }
        ]
      });
    }
  };

  getPayloadByProductsFound = memoizeOne(() => {
    const payload = {
      error: null,
      selectedTaskIds: null,
      productsFound: null
    };

    if (!this.props.productsInStock.length) {
      payload.error =
        "Указанный товар не в требуемом статусе. Укажите другую единицу товара в статусе 'на складе'";

      return payload;
    }

    const entity = this.props.productsInStock[0];
    const productInStock = entity ? entity.product_in_stock[0] : null;
    const tasksWithoutProductUnit = this.props.task.tasks.filter(
      item => !item.product_unit_id
    );

    if (
      (this.props.selectedGroup === "7" && !productInStock.product_units[0]) ||
      (productInStock.product_units[0].state !== 12 &&
        productInStock.product_units[0].state !== 7)
    ) {
      payload.error =
        "Указанный товар не в требуемом статусе. Укажите другую единицу товара.";

      return payload;
    }

    if (
      !productInStock ||
      productInStock.total_count < tasksWithoutProductUnit.length
    ) {
      payload.error =
        "Указанный товар не в требуемом статусе. Укажите другую единицу товара в статусе 'на складе'";

      return payload;
    }

    payload.selectedTaskIds = [this.props.task.id];
    payload.productsFound = this.props.task.tasks
      .filter(item => !item.product_unit_id)
      .reduce((products, item, index) => {
        if (productInStock.product_units[index]) {
          products.push([item.id, productInStock.product_units[index]]);
        }

        return products;
      }, []);

    return payload;
  });

  render() {
    return <Title onClick={this.handleSearchProduct}>{this.props.title}</Title>;
  }
}

function mapStateToProps(state, props) {
  return {
    productsInStock: productsInStockSelector(state, props.id)
  };
}

export default connect(
  mapStateToProps,
  { fetchProductsInStock, addNotification }
)(SearchProductCommand);
