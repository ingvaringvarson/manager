import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled from "styled-components";
import { fetchOrder, orderInfoSelector } from "../../../ducks/orders";
import memoizeOne from "memoize-one";
import { addNotification } from "../../../ducks/logger";
import { getValueInDepth } from "../../../utils";

const Title = styled.p`
  cursor: pointer;
`;

class SearchProductByOrderCommand extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    task: PropTypes.object.isRequired,
    selectedGroup: PropTypes.string.isRequired,
    onSelectedProducts: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    onCloseMenu: PropTypes.func,
    orderInfo: PropTypes.object
  };

  componentDidUpdate(prevProps) {
    if (prevProps.orderInfo !== this.props.orderInfo) {
      const {
        selectedTaskIds,
        productsFound,
        error
      } = this.getPayloadByProductsFound(this.props.orderInfo);

      if (selectedTaskIds || productsFound) {
        this.props.onSelectedProducts(selectedTaskIds, productsFound);
        if (this.props.onCloseMenu) this.props.onCloseMenu();
      } else if (error) {
        this.props.addNotification({ id: this.props.id, message: error });
        if (this.props.onCloseMenu) this.props.onCloseMenu();
      }
    }
  }

  handleSearchProduct = event => {
    event.preventDefault();

    if (this.props.task.order_id) {
      this.props.fetchOrder(this.props.id, { id: this.props.task.order_id });
    }
  };

  getPayloadByProductsFound = memoizeOne(() => {
    const payload = {
      error: null,
      selectedTaskIds: null,
      productsFound: null
    };

    const order = getValueInDepth(this.props.orderInfo, ["order"]);

    const errorMsg =
      "Указанный товар не в требуемом статусе. Укажите другую единицу товара в статусе 'Выдан клиенту'";

    if (!order) {
      payload.error = errorMsg;

      return payload;
    }

    const position = order.positions.find(
      p => p.id === this.props.task.order_position_id
    );
    const tasksWithoutProductUnit = this.props.task.tasks.filter(
      item => !item.product_unit_id
    );

    if (!position || position.count < tasksWithoutProductUnit.length) {
      payload.error = errorMsg;

      return payload;
    }

    payload.selectedTaskIds = [this.props.task.id];
    payload.productsFound = tasksWithoutProductUnit.reduce(
      (products, item, index) => {
        if (position.product_units[index]) {
          products.push([item.id, position.product_units[index]]);
        }

        return products;
      },
      []
    );

    return payload;
  });

  render() {
    return <Title onClick={this.handleSearchProduct}>{this.props.title}</Title>;
  }
}

function mapStateToProps(state, props) {
  const id = getValueInDepth(props, ["task", "id"]);

  return {
    orderInfo: orderInfoSelector(state, { id }),
    id
  };
}

export default connect(
  mapStateToProps,
  { fetchOrder, addNotification }
)(SearchProductByOrderCommand);
