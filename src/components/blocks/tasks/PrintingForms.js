import React, { Component } from "react";
import PropTypes from "prop-types";
import { getValueInDepth } from "../../../utils";
import DocumentPrint from "../../blocks/DocumentPrint";
import IconButton from "../../../designSystem/molecules/IconButton";
import memoizeOne from "memoize-one";

class PrintingForms extends Component {
  static propTypes = {
    actionResult: PropTypes.object,
    tasks: PropTypes.arrayOf(PropTypes.object),
    renderButton: PropTypes.func
  };

  getOpenState = actionResult => {
    const openForm = getValueInDepth(actionResult, ["payload", "openForm"]);
    const hasErrors = getValueInDepth(actionResult, ["payload", "hasErrors"]);

    return !!(!hasErrors && openForm);
  };

  constructor(props) {
    super(props);

    const isOpen = props.actionResult
      ? this.getOpenState(props.actionResult)
      : false;

    this.state = {
      isOpen
    };
  }

  handleCloseModal = () => {
    this.setState({ isOpen: false });
  };

  handleOpenModal = () => {
    this.setState({ isOpen: true });
  };

  getPropsByDocumentPrint = memoizeOne(tasks => {
    const propsByDocumentPrint = {
      idEntity: null,
      availableIds: null,
      entityIds: null,
      formIsAvailable: false,
      objectTypeId: 13
    };

    let mainTask = null;

    if (this.props.actionResult) {
      const updatedTasks = getValueInDepth(this.props.actionResult, [
        "payload",
        "tasks"
      ]);

      if (!Array.isArray(updatedTasks) || !updatedTasks.length)
        return propsByDocumentPrint;

      mainTask = updatedTasks[0];

      propsByDocumentPrint.idEntity = mainTask.id;
      propsByDocumentPrint.entityIds = updatedTasks.map(t => t.id);

      switch (true) {
        case mainTask.type === 9 && mainTask.state === 7:
        case mainTask.type === 4: {
          propsByDocumentPrint.availableIds = [3, 4, 5, 12, 15, 19];
          propsByDocumentPrint.formIsAvailable = true;
          break;
        }
        case mainTask.type === 3 && mainTask.state === 3: {
          propsByDocumentPrint.availableIds = [8];
          propsByDocumentPrint.formIsAvailable = true;
          break;
        }
        case mainTask.type === 11 &&
          mainTask.state === 2 &&
          mainTask.reason === 1: {
          propsByDocumentPrint.availableIds = [6, 13];
          propsByDocumentPrint.formIsAvailable = true;
          break;
        }
      }
    } else if (Array.isArray(tasks) && tasks.length) {
      mainTask = tasks[0];

      propsByDocumentPrint.idEntity = tasks.id;
      propsByDocumentPrint.entityIds = tasks.map(t => t.id);

      switch (true) {
        case mainTask.type === 11: {
          propsByDocumentPrint.availableIds = [6, 13];
          propsByDocumentPrint.formIsAvailable = true;
          break;
        }
        case mainTask.type === 3 && mainTask.state === 3: {
          propsByDocumentPrint.availableIds = [8];
          propsByDocumentPrint.formIsAvailable = true;
          break;
        }
        case mainTask.type === 11 &&
          mainTask.state === 2 &&
          mainTask.reason === 1: {
          propsByDocumentPrint.availableIds = [6, 13];
          propsByDocumentPrint.formIsAvailable = true;
          break;
        }
      }
    }

    return propsByDocumentPrint;
  });

  render() {
    const {
      idEntity,
      availableIds,
      entityIds,
      formIsAvailable,
      objectTypeId
    } = this.getPropsByDocumentPrint(this.props.tasks);

    return (
      <>
        {this.state.isOpen && !!formIsAvailable && (
          <DocumentPrint
            isOpen={this.state.isOpen}
            title="Печатные формы"
            handleClose={this.handleCloseModal}
            objectTypeId={objectTypeId}
            idEntity={idEntity}
            availableIds={availableIds}
            entityIds={entityIds}
          />
        )}
        {!!this.props.renderButton &&
          this.props.renderButton(this.handleOpenModal, formIsAvailable)}
      </>
    );
  }
}

export default PrintingForms;
