import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Container from "../../../designSystem/templates/Container";
import Row from "../../../designSystem/templates/Row";
import Column from "../../../designSystem/templates/Column";
import { fieldsForTasksByProduct } from "../../../fields/formFields/tasks/fieldsForWarehousePage";
import {
  enumerationsSelector,
  fetchEnumerations
} from "../../../ducks/enumerations";
import Table from "../../common/tableView/Table";
import {
  fetchProductsInStock,
  productsInStockSelector
} from "../../../ducks/productsInStock";
import {
  entityIdByWarehousePageSelector,
  requestParamsByWarehousePageSelector,
  updateProductTasks
} from "../../../ducks/tasks";
import memoizeOne from "memoize-one";
import styled from "styled-components";
import { palette } from "styled-tools";
import Text from "../../../designSystem/atoms/Text";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import Icon from "../../../designSystem/atoms/Icon";
import Button from "../../../designSystem/atoms/Button";
import { scroller, Element } from "react-scroll";
import {
  getParamsByUpdate,
  getTextParams,
  mapProductsToTasks
} from "./helpers";

const subTableParams = {
  subTable: true,
  whiteBG: true,
  ml: "30px",
  mt: "8px",
  mb: "16px"
};
const enumerationNames = ["taskType", "taskState", "productUnitState"];

const StyledContainer = styled(Container)`
  border-radius: 5px;
  background-color: ${palette("grayscale", 2)};
`;

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 1px;
  left: -5px;
  width: 24px;
  height: 24px;
  transform: rotate(130deg);
`;

const OpenTrigger = styled(Text)`
  position: relative;
  padding: 5px 5px 5px 20px;
  cursor: pointer;
`;

const StyledBlock = styled(BlockContent)`
  border-radius: 5px;
  background: #fff;
  padding: 5px;
  margin-top: 8px;
`;

const typeEntity = "warehouse";

class TaskTableForProduct extends Component {
  static propTypes = {
    product: PropTypes.object.isRequired,
    fetchProductsInStock: PropTypes.func.isRequired,
    productsInStock: PropTypes.array.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    selectedGroup: PropTypes.string.isRequired,
    productsFound: PropTypes.array,
    updateProductTasks: PropTypes.func.isRequired,
    warehouseId: PropTypes.string.isRequired
  };

  state = {
    isOpen: true
  };

  componentDidMount() {
    const ids = this.getProductIds(this.props.product);

    if (ids.length) {
      this.props.fetchProductsInStock(this.props.product.id, {
        product_unit_id: ids
      });
      this.props.fetchEnumerations(enumerationNames);
    }

    scroller.scrollTo(`scroll_elem_${this.props.product.id}`, {
      smooth: true,
      offset: -150
    });
  }

  getTextParamsMemoize = memoizeOne(getTextParams);

  handleToggleOpen = () => {
    this.setState(prevProp => ({ isOpen: !prevProp.isOpen }));
  };

  handleUpdateTasks = () => {
    const {
      selectedGroup,
      productsInStock,
      product,
      idEntity,
      requestParams
    } = this.props;
    const { tasksWithProducts } = this.mapProductsToTasks(
      product.tasks,
      productsInStock
    );
    const { openForm, needKey, ...updateParams } = getParamsByUpdate(
      selectedGroup,
      tasksWithProducts
    );

    this.props.updateProductTasks(product.tasks, updateParams, {
      notification: {
        id: this.props.idEntity
      },
      actionResult: {
        id: this.props.idEntity
      },
      fetchTasks: {
        idEntity,
        params: requestParams,
        typeEntity
      },
      openForm,
      needKey,
      selectedGroup
    });
  };

  getProductIds = memoizeOne(product => {
    return product.tasks.reduce(
      (ids, task) =>
        task.product_unit_id ? ids.concat(task.product_unit_id) : ids,
      []
    );
  });

  mapProductsToTasks = memoizeOne(mapProductsToTasks);

  renderTable() {
    const { product, enumerations, productsInStock } = this.props;

    const { tasksWithProducts, key } = this.mapProductsToTasks(
      product.tasks,
      productsInStock
    );

    return (
      <Table
        key={key}
        tableProps={subTableParams}
        enumerations={enumerations}
        entities={tasksWithProducts}
        cells={fieldsForTasksByProduct}
      />
    );
  }

  renderCustomButtons(text, titleBtn) {
    return (
      <StyledBlock>
        {!!text && <Text p="10px">{text}</Text>}
        <Row p="0px 18px 5px" alignCenter>
          {!!titleBtn && (
            <Column>
              <Button defaultCase onClick={this.handleUpdateTasks}>
                {titleBtn}
              </Button>
            </Column>
          )}
          <Column>
            <OpenTrigger onClick={this.handleToggleOpen}>
              Скрыть уведомление
            </OpenTrigger>
          </Column>
        </Row>
      </StyledBlock>
    );
  }

  renderButtons() {
    const { selectedGroup, productsInStock, product } = this.props;
    const { tasksWithProducts } = this.mapProductsToTasks(
      product.tasks,
      productsInStock
    );
    const { title, btnTitle } = this.getTextParamsMemoize(
      tasksWithProducts,
      selectedGroup
    );

    if (title || btnTitle) {
      return this.renderCustomButtons(title, btnTitle);
    }

    return null;
  }

  renderControls() {
    if (!this.state.isOpen) {
      return (
        <StyledBlock>
          <OpenTrigger onClick={this.handleToggleOpen}>
            <StyledIcon icon="arrow_drop_up" />
            Развернуть
          </OpenTrigger>
        </StyledBlock>
      );
    }

    return this.renderButtons();
  }

  render() {
    return (
      <StyledContainer>
        <Element name={`scroll_elem_${this.props.product.id}`} />
        <Row>
          <Column basis="50%" pt="10px">
            {this.renderTable()}
          </Column>
          <Column pt="10px" ml="5px">
            {this.renderControls()}
          </Column>
        </Row>
      </StyledContainer>
    );
  }
}

function mapStateToProps(state, props) {
  const idEntity = entityIdByWarehousePageSelector(state, props);
  const requestParams = requestParamsByWarehousePageSelector(state, props);

  return {
    requestParams,
    idEntity,
    enumerations: enumerationsSelector(state, { enumerationNames }),
    productsInStock: productsInStockSelector(state, props.product.id)
  };
}

export default connect(
  mapStateToProps,
  { fetchProductsInStock, fetchEnumerations, updateProductTasks }
)(TaskTableForProduct);
