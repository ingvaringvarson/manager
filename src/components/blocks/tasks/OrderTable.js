import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import nanoId from "nanoid";
import { connect } from "react-redux";
import { orderSelectorByStoreIdAndOwnId } from "../../../ducks/orders";
import { ContainerContext } from "../../common/formControls/index";

import {
  fields as tableViewFields,
  formFields
} from "../../../fields/formFields/tasks/orderAddTable";
import RowRemoveableTable from "../../common/tables/RowRemoveableTable";
import { reduceValuesToFormFields } from "../../../fields/helpers";

const propTypes = {
  orderId: PropTypes.string.isRequired,
  field: PropTypes.object,
  entityInStore: PropTypes.object,
  handleChange: PropTypes.func
};

class OrderTable extends PureComponent {
  state = {
    removedIds: []
  };

  componentDidMount = () => {
    const { entityInStore, handleChange, orderId } = this.props;

    handleChange(entityInStore, orderId, null);
  };

  renderCells = memoizeOne((cells, enumerations, values, fieldsValues) => {
    return {
      cells: cells.reduce(
        reduceValuesToFormFields(
          {
            ...values,
            fields: fieldsValues
          },
          enumerations
        ),
        []
      ),
      key: nanoId()
    };
  });

  handleFormStateChanged = fieldsState => {
    const { orderId, handleChange, entityInStore } = this.props;
    const { values } = fieldsState;

    handleChange(entityInStore, orderId, values, this.state.removedIds);
  };

  handleRowRemove = (posId, oldRemovedIds, positions) => {
    this.setState(prevState => {
      return {
        removedIds: [...prevState.removedIds, posId]
      };
    });
  };

  render() {
    if (!this.props.entityInStore.positions) {
      return null;
    }

    const { cells, key } = this.renderCells(
      formFields,
      this.props.enumerations,
      null,
      this.props.entityInStore.positions
    );

    return (
      <ContainerContext
        key={key}
        fields={cells}
        handleStateChanged={this.handleFormStateChanged}
      >
        <RowRemoveableTable
          entities={this.props.entityInStore.positions}
          cells={tableViewFields}
          handleElementRemove={this.handleRowRemove}
        />
      </ContainerContext>
    );
  }
}

OrderTable.propTypes = propTypes;

export default connect((state, props) => {
  return {
    entityInStore: orderSelectorByStoreIdAndOwnId(state, {
      orderId: props.orderId,
      storeId: props.field.id
    })
  };
})(OrderTable);
