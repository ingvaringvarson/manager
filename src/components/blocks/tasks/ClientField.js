import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import styled from "styled-components";
import Icon from "../../../designSystem/atoms/Icon";

const IconWrapper = styled(Link)`
  color: #444;
`;

class ClientField extends PureComponent {
  static propTypes = {
    values: PropTypes.object,
    field: PropTypes.object,
    fields: PropTypes.array
  };

  render() {
    const clientId = this.props.values[this.props.field.id];
    if (!clientId) return null;

    return (
      <IconWrapper to={`/clients/${clientId}`}>
        <Icon icon="person" />
      </IconWrapper>
    );
  }
}

export default ClientField;
