import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { compose } from "recompose";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import {
  tasksForEntitySelector,
  fetchTasksForEntity
} from "../../../ducks/tasks";
import {
  keyRouterSelector,
  pathnameRouterSelector,
  entityIdForRouteSelector
} from "../../../redux/selectors/router";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../../fields/helpers";
import memoizeOne from "memoize-one";
import ContainerContext from "../../common/formControls/ContainerContext";
import { generateUrlWithQuery, generateId } from "../../../utils";
import Pagination from "../Pagination";
import { Modal as CreateTask } from "./commands/CreateTask";
import TaskMenu from "./TaskMenu";
import UpdateTask from "./UpdateTask";

import {
  enumerationsSelector,
  fetchEnumerations
} from "../../../ducks/enumerations";

import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";
import { Table, RowBody } from "../../common/tableView";

import {
  handleChangeTab,
  handleMoveToEntityPage,
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields
} from "../../../helpers/component/handlers";

import { queryKeysHoldByTab } from "../../../constants/props";

import { tasksTabs } from "../../../constants/tabs";
import ModalIconControls from "../../common/modalControls/ModalIconControls";

import { PointerBox } from "../../../components/common/styled/block";

const propTypes = {
  selectedTab: PropTypes.number.isRequired,
  idEntity: PropTypes.string.isRequired,
  typeEntity: PropTypes.string.isRequired,
  requestParams: PropTypes.object.isRequired,
  keyRouter: PropTypes.string.isRequired,
  fields: PropTypes.arrayOf(PropTypes.object).isRequired,
  tasks: PropTypes.arrayOf(PropTypes.object).isRequired,
  isCellsMapped: PropTypes.bool,
  enumerations: PropTypes.object,
  hasFormControl: PropTypes.bool,
  requestParamsSelector: PropTypes.func.isRequired,
  keyProps: PropTypes.string,
  hasExternalTable: PropTypes.bool,
  renderTableProps: PropTypes.func
};

const defaultProps = {
  isCellsMapped: false,
  maxPages: 99,
  hasFormControl: false,
  keyProps: generateId(),
  hasExternalTable: false
};

const customModalStyles = {
  content: {
    width: "630px"
  }
};

const enumerationNames = ["taskType", "taskState", "taskReason"];

class TasksTableForEntity extends PureComponent {
  constructor(props) {
    super(props);

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleChangeTab = handleChangeTab.bind(this);
    this.handleMoveToEntityPage = handleMoveToEntityPage.bind(this);
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
    this.rowBodyHandlers = {
      onClick: this.handleMoveToEntityPage
    };
  }
  componentDidMount() {
    this.props.fetchTasksForEntity(
      this.props.typeEntity,
      this.props.idEntity,
      this.props.requestParams
    );
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.props.fetchTasksForEntity(
        this.props.typeEntity,
        this.props.idEntity,
        this.props.requestParams
      );
    }
  }

  handleSubmit = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  renderCells = memoizeOne((fields, enumerations, values) => {
    return {
      cells: fields.reduce(reduceValuesToFormFields(values, enumerations), []),
      key: generateId()
    };
  });

  renderUpdateTaskForm = entity => (state, modalProps) => {
    return (
      <UpdateTask
        entity={entity}
        isOpen={state.isOpen}
        handleClose={modalProps.onClose}
      />
    );
  };

  renderRowBody = (entity, rowBodyProps) => (_, modalProps) => {
    return (
      <PointerBox onClick={modalProps.onOpen}>
        <RowBody
          {...rowBodyProps}
          entity={entity}
          key={entity.key || entity._id}
        />
      </PointerBox>
    );
  };

  renderTableBody = (entities, activeCells, enumerations, rowBodyProps) => {
    return entities.map(entity => {
      return (
        <ModalIconControls
          renderContent={this.renderUpdateTaskForm(entity)}
          modalStyles={customModalStyles}
        >
          {this.renderRowBody(entity, rowBodyProps)}
        </ModalIconControls>
      );
    });
  };

  renderAddButton = () => {
    // TODO:
    // Нужно дописать этот функционал,
    // где появится иконка, управляющая открытием и закрытием модалки
    return <CreateTask isOpen={false} />;
  };

  renderMoveButton = elementProps => {
    return <TaskMenu entity={elementProps.entity} />;
  };

  render() {
    const {
      keyProps,
      tasks,
      enumerations,
      requestParams,
      fields,
      pathname,
      push,
      maxPages,
      hasFormControl,
      hasExternalTable,
      renderTableProps,
      selectedTab
    } = this.props;

    let cells = this.props.fields,
      key = keyProps;

    if (!this.props.isCellsMapped) {
      const renderedObj = this.renderCells(fields, enumerations, requestParams);

      cells = renderedObj.cells;
      key = renderedObj.key;
    }

    return (
      <TabContainer
        selectedTabId={selectedTab}
        onChangeSelected={this.handleChangeTab}
      >
        <TabHeadContainer>
          {tasksTabs.map(item => {
            return (
              <TabHead id={item.id} key={item.id}>
                {item.name}
              </TabHead>
            );
          })}
        </TabHeadContainer>
        <TabBodyContainer>
          {tasksTabs.map(item => {
            return (
              <TabBody id={item.id} key={item.id}>
                <ContainerContext
                  key={key}
                  onSort={this.handleSort}
                  fields={cells}
                  onSubmitForm={this.handleSubmit}
                  autoSubmit
                >
                  {hasExternalTable ? (
                    renderTableProps &&
                    renderTableProps({ tasks, cells, enumerations })
                  ) : (
                    <Table
                      entities={tasks}
                      cells={cells}
                      enumerations={enumerations}
                      hasFormControl={hasFormControl}
                      hasChildrenAfter
                      hasExternalBody
                      renderRowHeadChildrenAfter={this.renderAddButton}
                      renderRowBodyChildrenAfter={this.renderMoveButton}
                      renderBody={this.renderTableBody}
                    />
                  )}
                </ContainerContext>
                <Pagination
                  requestParams={requestParams}
                  pathname={pathname}
                  push={push}
                  page={+requestParams.page}
                  maxPages={maxPages}
                />
              </TabBody>
            );
          })}
        </TabBodyContainer>
      </TabContainer>
    );
  }
}

TasksTableForEntity.propTypes = propTypes;
TasksTableForEntity.defaultProps = defaultProps;

function mapStateToProps(state, props) {
  const requestParams = props.requestParamsSelector(state, props);
  const queryKeyByTab = "tasks_state";
  const selectedTab = +(requestParams[queryKeyByTab]
    ? requestParams[queryKeyByTab]
    : 1);
  const idEntity = entityIdForRouteSelector(state, props);
  const tasksObject = tasksForEntitySelector(state, {
    idEntity,
    typeEntity: props.typeEntity
  });
  return {
    queryKeyByTab,
    selectedTab,
    queryKeysHold: queryKeysHoldByTab,
    tasks: tasksObject.entities,
    maxPages: tasksObject.maxPages,
    keyRouter: keyRouterSelector(state, props),
    pathname: pathnameRouterSelector(state),
    requestParams: requestParams,
    idEntity,
    enumerations: enumerationsSelector(state, { enumerationNames })
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    {
      fetchTasksForEntity,
      push,
      fetchEnumerations
    }
  )
)(TasksTableForEntity);
