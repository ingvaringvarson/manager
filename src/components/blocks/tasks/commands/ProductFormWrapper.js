import React, { PureComponent } from "react";
import memoizeOne from "memoize-one";
import { ContextForm } from "../../../common/formControls";
import Field from "../../../../designSystem/molecules/Field";

import OrderField from "./OrderField";
import ArticleField from "./ArticleField";
import ProductsTable from "./ProductsTable";
import { generateId } from "../../../../utils";
import {
  HeaderWithLine as Header,
  TextBetween
} from "../../../common/styled/text";
import { GridFieldsTemplate as GridTemplate } from "../../../common/styled/form";

class ProductFormWrapper extends PureComponent {
  static contextType = ContextForm;
  state = {
    products: {},
    articles: [generateId()],
    orders: [generateId()]
  };
  handleChangeProduct = value => {
    const { onChange } = this.context.typeProps.custom;
    const products = this.getProducts(this.state.products);

    const valuesEntries = Object.entries(value);

    const newValues = products.reduce((res, p) => {
      if (!valuesEntries.some(([key]) => key.includes(p._id))) {
        return res;
      }

      res[p._id] = {
        ...p,
        ...valuesEntries.reduce((valRes, [k, v]) => {
          if (k.includes(p._id)) {
            valRes[k.split("__")[0]] = v;
          }
          return valRes;
        }, {})
      };
      return res;
    }, {});

    onChange(newValues, "_product");
  };

  handleProductsChange = (id, products) => {
    this.setState(prevState => ({
      products: {
        ...prevState.products,
        [id]: products
      }
    }));
  };

  handleOrderSelectChange = () => {
    this.setState(prevState => ({
      orders: prevState.orders.concat(generateId())
    }));
  };

  handleArticleSelectChange = () => {
    this.setState(prevState => ({
      articles: prevState.orders.concat(generateId())
    }));
  };

  getProducts = memoizeOne(products => {
    return Object.values(products).reduce((res, products) => {
      products.forEach(p => res.push(p));
      return res;
    }, []);
  });

  render() {
    return (
      <>
        <Header>
          <TextBetween>Добавьте товары</TextBetween>
        </Header>
        <GridTemplate>
          <Field label="Заказ">
            <OrderField
              id={this.state.orders[this.state.orders.length - 1]}
              handleSelectChange={this.handleOrderSelectChange}
              handleProductsChange={this.handleProductsChange}
            />
          </Field>
          <Field label="Артикул">
            <ArticleField
              id={this.state.articles[this.state.articles.length - 1]}
              handleSelectChange={this.handleArticleSelectChange}
              handleProductsChange={this.handleProductsChange}
            />
          </Field>
        </GridTemplate>
        <ProductsTable
          products={this.getProducts(this.state.products)}
          onChange={this.handleChangeProduct}
        />
      </>
    );
  }
}

export default ProductFormWrapper;
