import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import {
  fieldsForProductTable,
  fieldsForProductForm
} from "../../../../fields/formFields/tasks/productFields";
import { reduceValuesToFormFields } from "../../../../fields/helpers";

import { ContainerContext } from "../../../common/formControls";
import Removeable from "../../../common/table/Removeable";
import { generateId } from "../../../../utils";

class ProductsTable extends PureComponent {
  static propTypes = {
    products: PropTypes.arrayOf(
      PropTypes.shape({
        brand: PropTypes.string,
        name: PropTypes.string,
        article: PropTypes.string,
        order: PropTypes.string,
        count: PropTypes.number,
        count_add: PropTypes.number
      })
    ).isRequired,
    onChange: PropTypes.func.isRequired
  };
  static getDerivedStateFromProps(props, state) {
    if (state.products !== props.products) {
      return {
        products: props.products,
        contextValues: props.products.reduce((r, p) => {
          r[`count__${p._id}`] = p.count;
          r[`count_add__${p._id}`] = p.count;
          return r;
        }, {})
      };
    }
  }

  state = {
    removedIds: [],
    contextValues: null,
    products: this.props.products
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state) {
      const value = Object.entries(this.state.contextValues).reduce(
        (res, [k, v]) => {
          if (this.state.removedIds.some(id => k.includes(id))) return res;

          res[k] = v;
          return res;
        },
        {}
      );

      this.props.onChange(value);
    }
  }

  getMappedFields = memoizeOne(products => {
    return {
      formFields: fieldsForProductForm.reduce(
        reduceValuesToFormFields({ products }),
        []
      ),
      talbeFields: fieldsForProductTable,
      key: generateId()
    };
  });

  handleValuesChange = contextValues => this.setState({ contextValues });
  handleRemovedChange = removedIds => this.setState({ removedIds });

  render() {
    const { formFields, talbeFields, key } = this.getMappedFields(
      this.props.products
    );

    return (
      <ContainerContext
        fields={formFields}
        onValuesChange={this.handleValuesChange}
        key={key}
      >
        <Removeable
          handleRemovedChange={this.handleRemovedChange}
          cells={talbeFields}
          entities={this.props.products}
          removedIds={this.state.removedIds}
        />
      </ContainerContext>
    );
  }
}

export default ProductsTable;
