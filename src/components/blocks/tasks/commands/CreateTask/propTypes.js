import PropTypes from "prop-types";

export const formPropTypes = {
  defaultValues: PropTypes.object,
  enumerations: PropTypes.object,
  fetchEnumerations: PropTypes.func.isRequired,
  successCallback: PropTypes.func,
  fetchEntities: PropTypes.func
};
export const formDefaultProps = {
  defaultValues: {
    type: [1],
    state: [1]
  }
};

export const modalPropTypes = {
  ...formPropTypes,
  isOpen: PropTypes.bool,
  handleClose: PropTypes.func
};
export const modalDefaultProps = {
  ...formDefaultProps,
  isOpen: true,
  title: "Создать задачу"
};
