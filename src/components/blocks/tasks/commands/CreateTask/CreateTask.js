import React, { PureComponent } from "react";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../../../ducks/enumerations";
import { createTask } from "../../../../../ducks/tasks";
import { fields as fieldsForForm } from "../../../../../fields/formFields/tasks/createTaskFields";
import { reduceValuesToFormFields } from "../../../../../fields/helpers";
import { formDefaultProps, formPropTypes } from "./propTypes";
import {
  ContainerContext,
  FormContext,
  RenderContext
} from "../../../../common/formControls";
import FieldForm from "../../../../common/form/FieldForm";
import { GridFieldsTemplate as GridTemplate } from "../../../../common/styled/form";
import RightStickedButton from "../../../../common/buttons/RightStickedButton";

const enumerationNames = ["taskType", "taskState"];
const gridBlockNames_1 = [
  "type",
  "date",
  "state",
  "executor_id",
  "warehouse_id",
  "warehouse_to_id"
];

class CreateTask extends PureComponent {
  static propTypes = formPropTypes;
  static defaultProps = formDefaultProps;

  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
  }

  getMappedFields = memoizeOne(enumerations => {
    const formFieldsMapped = fieldsForForm.reduce(
      reduceValuesToFormFields(this.props.defaultValues, enumerations),
      []
    );
    return {
      fieldsForForm: {
        all: formFieldsMapped,
        ...formFieldsMapped.reduce(
          (res, f) => {
            if (gridBlockNames_1.some(n => n === f.id)) {
              res.block_1.push(f);
            } else {
              res.block_2.push(f);
            }
            return res;
          },
          {
            block_1: [],
            block_2: []
          }
        )
      }
    };
  });

  successCallback() {
    this.props.fetchEntities();
    this.props.successCallback();
  }

  handleSubmitForm = values => {
    this.props.createTask(values, {
      successCallback: this.successCallback.bind(this)
    });
  };

  renderField(f, i) {
    return <FieldForm field={f} index={i} key={f.id} />;
  }

  render() {
    const { fieldsForForm } = this.getMappedFields(this.props.enumerations);
    return (
      <ContainerContext
        submitValidation
        fields={fieldsForForm.all}
        onSubmitForm={this.handleSubmitForm}
        onlyActiveFields
      >
        <FormContext>
          <GridTemplate>
            {fieldsForForm.block_1.map(this.renderField)}
          </GridTemplate>
          {fieldsForForm.block_2.map(this.renderField)}
        </FormContext>
        <RenderContext>
          {context => (
            <RightStickedButton disabled={!context.isFormValid}>
              Создать
            </RightStickedButton>
          )}
        </RenderContext>
      </ContainerContext>
    );
  }
}

export default connect(
  state => ({
    enumerations: enumerationsSelector(state, { enumerationNames })
  }),
  { fetchEnumerations, createTask }
)(CreateTask);
