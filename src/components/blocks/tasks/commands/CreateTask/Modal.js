import React from "react";
import { modalPropTypes, modalDefaultProps } from "./propTypes";

import CreateTask from "./CreateTask";
import CommonModal from "../../../../common/modals/CommonModal";

const contentStyles = {
  width: 800
};

const Modal = ({ isOpen, handleClose, title, ...props }) => (
  <CommonModal
    isOpen={isOpen}
    handleClose={handleClose}
    contentStyles={contentStyles}
    title={title}
  >
    <CreateTask {...props} successCallback={handleClose} />
  </CommonModal>
);

Modal.propTypes = modalPropTypes;
Modal.defaultProps = modalDefaultProps;

export default Modal;
