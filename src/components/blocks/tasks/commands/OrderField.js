import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";
import {
  fetchOrdersForEntity,
  ordersForEntitySelector
} from "../../../../ducks/orders";
import SearchRequestControl, {
  searchRequestPropsForOrders
} from "../../../common/form/SearchRequestControl";
import Select from "../../../../designSystem/molecules/Select";

const typeEntity = "product";

class OrderField extends PureComponent {
  static propTypes = {
    id: PropTypes.string.isRequired,
    handleProductsChange: PropTypes.func.isRequired,
    handleSelectChange: PropTypes.func.isRequired
  };

  componentDidUpdate = prevProps => {
    if (this.props.orders !== prevProps.orders && this.props.orders.length) {
      this.props.handleProductsChange(
        this.props.orders[0].order.id,
        this.getProducts(this.props.orders)
      );
      this.props.handleSelectChange();
    }
  };

  searchRequestSkeleton = searchRequestPropsForOrders();

  handleSelectChange = selectedOrder => {
    if (Array.isArray(selectedOrder) && selectedOrder.length > 0) {
      this.props.fetchOrdersForEntity(typeEntity, this.props.id, {
        id: selectedOrder[0]
      });
    }
  };

  getProducts = memoizeOne(orders =>
    orders.reduce((res, o) => {
      if (!Array.isArray(o.order.positions)) return res;

      o.order.positions.forEach(p =>
        res.push({
          ...p.product,
          count: p.count,
          count_add: p.count,
          order: o.order.code,
          order_position_id: p.id,
          _id: p._id
        })
      );
      return res;
    }, [])
  );

  render() {
    return (
      <SearchRequestControl {...this.searchRequestSkeleton}>
        {(
          onChangeSearchValue,
          onToggleFocusSearchControl,
          options,
          isLoading
        ) => (
          <Select
            placeholder="Введите номер заказа"
            searchMode="searchRequestControl"
            name={this.props.id}
            options={options}
            isLoading={isLoading}
            onChange={this.handleSelectChange}
            onChangeSearchValue={onChangeSearchValue}
            onToggleFocusSearchControl={onToggleFocusSearchControl}
          />
        )}
      </SearchRequestControl>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      orders: ordersForEntitySelector(state, {
        idEntity: props.id,
        typeEntity
      }).entities
    };
  },
  { fetchOrdersForEntity }
)(OrderField);
