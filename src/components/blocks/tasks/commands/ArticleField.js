import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import {
  fetchProductsInStock,
  productsInStockSelector
} from "../../../../ducks/productsInStock";
import SearchRequestControl, {
  searchRequestPropsForBrandVendorCode
} from "../../../common/form/SearchRequestControl";
import Select from "../../../../designSystem/molecules/Select";
import { ContextForm } from "../../../common/formControls";

class ArticleField extends PureComponent {
  static contextType = ContextForm;
  static propTypes = {
    id: PropTypes.string.isRequired,
    handleProductsChange: PropTypes.func.isRequired,
    handleSelectChange: PropTypes.func.isRequired
  };

  searchRequestSkeleton = searchRequestPropsForBrandVendorCode();

  componentDidUpdate = prevProps => {
    if (
      this.props.products !== prevProps.products &&
      this.props.products.length
    ) {
      const { article, brand } = this.props.products[0].product;
      this.props.handleProductsChange(
        `${article}/${brand}`,
        this.mapProducts(this.props.products)
      );
      this.props.handleSelectChange(this.props.id);
    }
  };

  handleSelectChange = (_1, _2, selectedObjectsArray) => {
    this.props.fetchProductsInStock(
      this.props.id,
      this.mapRequestValues(selectedObjectsArray)
    );
  };

  mapRequestValues = selectedObjectsArray => {
    return {
      product: Array.isArray(selectedObjectsArray)
        ? selectedObjectsArray.map(o => ({
            brand: o.brandKey,
            article: o.vendorCode
          }))
        : null,
      warehouse_id: this.context.values.warehouse_id,
      state: [1]
    };
  };

  mapAttrs = memoizeOne(values => {
    return {
      placeholder: "Введите номер заказа",
      disabled: Array.isArray(values.warehouse_id)
        ? values.warehouse_id.length === 0
        : values.warehouse_id === ""
    };
  });

  mapProducts = memoizeOne(products =>
    products.reduce((res, p) => {
      if (!Array.isArray(p.product_in_stock)) return res;

      p.product_in_stock.forEach(ps =>
        res.push({
          ...p.product,
          ...ps,
          count_add: ps.count,
          _id: p._id
        })
      );
      return res;
    }, [])
  );

  render() {
    return (
      <SearchRequestControl {...this.searchRequestSkeleton}>
        {(
          onChangeSearchValue,
          onToggleFocusSearchControl,
          options,
          isLoading
        ) => (
          <Select
            {...this.mapAttrs(this.context.values)}
            name={this.props.id}
            searchMode="searchRequestControl"
            options={options}
            isLoading={isLoading}
            onChange={this.handleSelectChange}
            onChangeSearchValue={onChangeSearchValue}
            onToggleFocusSearchControl={onToggleFocusSearchControl}
          />
        )}
      </SearchRequestControl>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      products: productsInStockSelector(state, props.id)
    };
  },
  { fetchProductsInStock }
)(ArticleField);
