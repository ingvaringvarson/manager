import React from "react";
import styled from "styled-components";
import { palette } from "styled-tools";

import Container from "../../../designSystem/templates/Container";
import Row from "../../../designSystem/templates/Row";
import HeaderLogo from "../../../designSystem/molecules/HeaderLogo";
import HeaderUser from "../HeaderUser";
import HeaderTypes from "../../../helpers/propTypes/components/HeaderTypes";

const StyledHeader = styled.header`
  position: fixed;
  z-index: 999;
  width: 100%;
  height: 64px;
  padding: 15px;
  border-bottom: 1px solid ${palette("grayscale", 1)};
  background-color: #ffffff;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.05);
`;

const Header = props => {
  return (
    <StyledHeader>
      <Container>
        <Row alignCenter justifyBetween>
          <HeaderLogo title="Приложение менеджера" />
          <HeaderUser onLogOut={props.onLogOut} name={props.name} />
        </Row>
      </Container>
    </StyledHeader>
  );
};

Header.propTypes = {
  ...HeaderTypes
};

export default Header;
