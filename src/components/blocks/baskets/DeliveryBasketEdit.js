import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import { reduceValuesToFormFields } from "../../../fields/helpers";
import { generateId } from "../../../utils";
import { fieldsForDeliveryBasket } from "../../../fields/formFields/orders/deliveryInfo";

import ContainerContext from "../../common/formControls";
import FieldForm from "../../common/form/FieldForm";

class DeliveryBasketEdit extends PureComponent {
  static propTypes = {
    basketInfo: PropTypes.object.isRequired,
    onSubmitForm: PropTypes.func.isRequired,
    enumerations: PropTypes.object.isRequired
  };

  mapDeliveryInfoToForm = memoizeOne((deliveryType, enumerations) => {
    const { basketInfo } = this.props;

    const deliveryInfo = {
      delivery_type: deliveryType,
      contract_id: basketInfo.deliveryContractId,
      date_plan: basketInfo.deliveryPeriod.date_plan,
      period: basketInfo.deliveryPeriod.period
    };

    switch (true) {
      case deliveryType === 1:
        deliveryInfo.warehouse_id = basketInfo.deliveryStoreId;
        break;
      case deliveryType === 2:
        deliveryInfo.address = basketInfo.deliveryAddress;
        break;
      case deliveryType === 3:
        deliveryInfo.location_cdek = basketInfo.deliveryAddress.location;
        deliveryInfo.codedc = basketInfo.deliveryAddress.delivery_point_id;
        break;
    }

    return {
      fields: fieldsForDeliveryBasket.reduce(
        reduceValuesToFormFields(deliveryInfo, enumerations),
        []
      ),
      key: generateId()
    };
  });

  render() {
    const deliveryFormInfo = this.mapDeliveryInfoToForm(
      this.props.basketInfo.deliveryType,
      this.props.enumerations
    );

    return (
      <ContainerContext
        key={deliveryFormInfo.key}
        fields={deliveryFormInfo.fields}
        onSubmitForm={this.props.onSubmitForm}
        autoSubmit
      >
        {deliveryFormInfo.fields.map((f, index) => {
          return (
            <FieldForm
              key={`${deliveryFormInfo.key}_${f.id || index}`}
              field={f}
            />
          );
        })}
      </ContainerContext>
    );
  }
}

export default DeliveryBasketEdit;
