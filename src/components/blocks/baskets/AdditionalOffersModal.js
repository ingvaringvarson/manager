import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import {
  brandLevelsSelector,
  fetchAdditionalOffersByBasketItem,
  requestIdForAdditionalOffersSelector,
  requestParamsForAdditionalOffersSelector,
  searchProductInfoSelector,
  moduleName as productsModuleName
} from "../../../ducks/products";
import {
  updateOfferItemInBasket,
  moveToProductsPage,
  moduleName as basketsModuleName
} from "../../../ducks/baskets";

import CommonModal from "../../common/modals/CommonModal";
import ProductCard from "../../blocks/products/ProductCard";
import PreLoader from "../../common/PreLoader";

import { RedButton } from "./styled";
import { getMainVendorCodeKey, getValueInDepth } from "../../../utils";
import { modulesIsLoadingSelector } from "../../../ducks/logger";

class AdditionalOffersModal extends PureComponent {
  static propTypes = {
    basket: PropTypes.object.isRequired,
    item: PropTypes.object.isRequired,
    fetchAdditionalOffersByBasketItem: PropTypes.func.isRequired,
    updateOfferItemInBasket: PropTypes.func.isRequired,
    moveToProductsPage: PropTypes.func.isRequired,
    requestParams: PropTypes.object.isRequired,
    productInfo: PropTypes.object,
    requestId: PropTypes.string.isRequired,
    brandLevels: PropTypes.object,
    isLoading: PropTypes.bool.isRequired
  };

  state = {
    isOpenModal: false
  };

  componentDidUpdate(prevProps, prevState) {
    const {
      fetchAdditionalOffersByBasketItem,
      requestId,
      requestParams,
      basket
    } = this.props;

    if (!prevState.isOpenModal && this.state.isOpenModal) {
      fetchAdditionalOffersByBasketItem(requestId, basket, requestParams);
    }
  }

  handleCloseModal = () => {
    this.setState({ isOpenModal: false });
  };

  handleOpenModal = () => {
    this.setState({ isOpenModal: true });
  };

  handleAddOfferToBasket = ({
    offerId,
    brandKey,
    vendorCodes,
    productKey,
    qty
  }) => {
    const { updateOfferItemInBasket, basket, item } = this.props;
    updateOfferItemInBasket(basket.managerBasketId, item.managerBasketItemId, {
      offerId,
      qty,
      productId: productKey
    });
  };

  modalContentStyles = {
    width: "1000px"
  };

  handleMoveToProductsPage = () => {
    const { moveToProductsPage, basket, item } = this.props;
    const brandKey = getValueInDepth(item, ["product", "brandKey"]);
    const vendorCode = getMainVendorCodeKey(
      getValueInDepth(item, ["product", "vendorCodes"])
    );
    moveToProductsPage(basket, brandKey, vendorCode);
  };

  render() {
    const { productInfo, brandLevels, isLoading } = this.props;
    return (
      <>
        <RedButton onClick={this.handleOpenModal}>
          Посмотреть другие
          <br /> предложения
        </RedButton>
        <CommonModal
          isOpen={this.state.isOpenModal}
          handleClose={this.handleCloseModal}
          title="Замена оффера"
          contentStyles={this.modalContentStyles}
        >
          {!!productInfo && (
            <>
              <ProductCard
                title="Актуальные предложения товара"
                product={productInfo}
                addOfferToBasket={this.handleAddOfferToBasket}
                brandLevels={brandLevels}
              />
              <RedButton onClick={this.handleMoveToProductsPage}>
                Посмотреть другие предложения
              </RedButton>
            </>
          )}
          {isLoading && <PreLoader viewType="content" />}
        </CommonModal>
      </>
    );
  }
}

const moduleNames = [basketsModuleName, productsModuleName];

function mapStateToProps(state, props) {
  const requestId = requestIdForAdditionalOffersSelector(state, props);

  return {
    requestId,
    requestParams: requestParamsForAdditionalOffersSelector(state, props),
    productInfo: searchProductInfoSelector(state, { id: requestId, ...props }),
    brandLevels: brandLevelsSelector(state, { id: requestId, ...props }),
    isLoading: modulesIsLoadingSelector(state, { moduleNames })
  };
}

export default connect(
  mapStateToProps,
  {
    fetchAdditionalOffersByBasketItem,
    updateOfferItemInBasket,
    moveToProductsPage
  }
)(AdditionalOffersModal);
