import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import {
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowHeadChildrenAfter,
  handleStopLifting
} from "../../common/tableControls";
import { fieldsForProducts } from "../../../fields/formFields/baskets/fields";

import { Table, RowBody } from "../../common/tableView";
import ItemRemoveControl from "./ItemRemoveControl";
import ItemNotificationContainer from "./notification/ItemNotificationContainer";

import TableCell from "../../../designSystem/molecules/TableCell";

class BasketItemsTable extends PureComponent {
  static propTypes = {
    basket: PropTypes.object.isRequired,
    items: PropTypes.array.isRequired
  };

  renderTableBody = (entities, activeCells, enumerations, rowBodyProps) => {
    return entities.map((entity, index) => {
      const rowBody = (
        <RowBody
          {...rowBodyProps}
          entity={entity}
          key={entity.key || entity._id || index}
        />
      );

      if (!entity.isActual) {
        return (
          <ItemNotificationContainer
            basket={this.props.basket}
            item={entity}
            key={entity.key || entity._id || index}
          >
            {rowBody}
          </ItemNotificationContainer>
        );
      }

      return rowBody;
    });
  };

  renderRowBodyChildrenAfter = ({ entity }) => {
    return (
      <TableCell control onClick={handleStopLifting}>
        <ItemRemoveControl
          basketId={entity.basketId}
          itemId={entity.managerBasketItemId}
        />
      </TableCell>
    );
  };

  render() {
    return (
      <Table
        cells={fieldsForProducts}
        entities={this.props.items}
        hasChildrenBefore
        hasChildrenAfter
        renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
        renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
        renderRowHeadChildrenAfter={renderRowHeadChildrenAfter}
        renderRowBodyChildrenAfter={this.renderRowBodyChildrenAfter}
        renderBody={this.renderTableBody}
        hasExternalBody
      />
    );
  }
}

export default BasketItemsTable;
