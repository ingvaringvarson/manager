import React, { memo } from "react";
import PropTypes from "prop-types";

import {
  digitsForNumbers,
  getDate,
  getPrepaidDescription
} from "../../../../utils";

import AdditionalOffersModal from "../AdditionalOffersModal";
import { Message, GeneralButton, RedButton } from "../styled";

const propTypes = {
  item: PropTypes.object.isRequired,
  basket: PropTypes.object.isRequired,
  onConfirmChanges: PropTypes.func.isRequired,
  onRemoveItem: PropTypes.func.isRequired
};

const NotificationMessage = memo(props => {
  const {
    item: {
      offerInfo: {
        offer,
        notification: { fields, confirm },
        hasAdditionalOffers
      }
    },
    item,
    onRemoveItem,
    onConfirmChanges,
    basket
  } = props;

  let qtyHasChanged = false;
  const defaultMessage = <p>Изменились условия продажи товара</p>;

  const messages = fields.length
    ? fields.map(f => {
        switch (f.name) {
          case "isPrepaid": {
            return (
              <p key={f.name}>
                Условие оплаты товара изменились с "
                {getPrepaidDescription(offer.isPrepaid)}" на "
                {getPrepaidDescription(f.newValue)}"
              </p>
            );
          }
          case "price": {
            return (
              <p key={f.name}>
                Цена на товар изменилась с {digitsForNumbers(offer.price)} руб.
                на {digitsForNumbers(f.newValue)} руб.
              </p>
            );
          }
          case "deliveryDate": {
            return (
              <p key={f.name}>
                Дата доставки изменилась с {getDate(offer.deliveryDate)} на{" "}
                {getDate(f.newValue)}{" "}
              </p>
            );
          }
          case "qty": {
            qtyHasChanged = true;
            return (
              <p key={f.name}>
                Наличие товара изменилось с {item.qty} на {f.newValue}
              </p>
            );
          }
          default: {
            return defaultMessage;
          }
        }
      })
    : defaultMessage;

  return (
    <>
      <Message>{messages}</Message>
      {(!confirm || qtyHasChanged) && hasAdditionalOffers && (
        <AdditionalOffersModal basket={basket} item={item} />
      )}
      <GeneralButton onClick={onRemoveItem}>
        Отказаться
        <br /> от товара
      </GeneralButton>
      {confirm && (
        <RedButton onClick={onConfirmChanges}>
          {qtyHasChanged ? (
            <>Оставить так </>
          ) : (
            <>
              Принять
              <br /> изменения
            </>
          )}
        </RedButton>
      )}
    </>
  );
});

NotificationMessage.propTypes = propTypes;

export default NotificationMessage;
