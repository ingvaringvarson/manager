import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import {
  removeItemsFromBasket,
  updateOfferItemInBasket
} from "../../../../ducks/baskets";

import { palette } from "styled-tools";
import styled from "styled-components";

import NotificationMessage from "./NotificationMessage";

export const Container = styled.div`
  position: relative;
`;

export const NotificationWrap = styled.div`
  display: flex;
  flex-flow: row wrap-reverse;
  justify-content: flex-end;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  padding: 2px 6px;
  background-color: rgba(0, 0, 0, 0.5);
`;

export const NotificationBlock = styled.div`
  display: flex;
  align-items: center;
  flex-basis: 50%;
  overflow-y: auto;
  border-radius: 5px;
  background-color: ${palette("white", 0)};
`;

class ItemNotificationContainer extends PureComponent {
  static propTypes = {
    item: PropTypes.object.isRequired,
    basket: PropTypes.object.isRequired,
    attachedAgentId: PropTypes.string,
    updateOfferItemInBasket: PropTypes.func.isRequired,
    removeItemsFromBasket: PropTypes.func.isRequired
  };

  mapItemToConfirmParams = () => {
    const {
      item: {
        qty: currentQty,
        offerInfo: {
          offer: { id: offerId },
          notification: { fields }
        },
        product: { id: productId }
      }
    } = this.props;
    const newQty = fields.find(f => f.name === "qty");
    const qty = newQty ? newQty.newValue : currentQty;

    return {
      productId,
      offerId,
      qty
    };
  };

  handleRemoveItem = () => {
    const {
      removeItemsFromBasket,
      basket: { managerBasketId },
      item: { managerBasketItemId }
    } = this.props;

    removeItemsFromBasket(managerBasketId, [managerBasketItemId]);
  };

  handleConfirmChanges = () => {
    const {
      updateOfferItemInBasket,
      basket: { managerBasketId },
      item: { managerBasketItemId }
    } = this.props;
    const params = this.mapItemToConfirmParams();

    updateOfferItemInBasket(managerBasketId, managerBasketItemId, params);
  };

  renderNotificationBlock() {
    const { item, basket } = this.props;

    return (
      <NotificationMessage
        basket={basket}
        item={item}
        onConfirmChanges={this.handleConfirmChanges}
        onRemoveItem={this.handleRemoveItem}
      />
    );
  }

  render() {
    return (
      <Container>
        {this.props.children}
        <NotificationWrap>
          <NotificationBlock>
            {this.renderNotificationBlock()}
          </NotificationBlock>
        </NotificationWrap>
      </Container>
    );
  }
}

export default connect(
  null,
  { removeItemsFromBasket, updateOfferItemInBasket }
)(ItemNotificationContainer);
