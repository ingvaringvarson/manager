import React, { memo } from "react";
import Select from "../../../designSystem/molecules/Select";
import Button from "../../../designSystem/atoms/Button";
import Input from "../../../designSystem/atoms/Input";

import { StyledText, Section, Grid } from "./styled";

//заглушка, в MAN пока нет реализации работы с авто
const options = [];
const handleChangeCar = () => {};

const CarBasketEdit = memo(() => {
  return (
    <Section>
      <StyledText mb="small" alter>
        Привязать к позиции автомбиль
      </StyledText>
      <Grid>
        <Input disabled placeholder="Введите VIN-номер" />
        <Select
          disabled
          name="car_brand"
          options={options}
          onChange={handleChangeCar}
          placeholder="Выберите марку"
        >
          <option value="">Lorem</option>
          <option value="">Lorem</option>
          <option value="">Lorem</option>
        </Select>
        <Select
          disabled
          name="car_model"
          options={options}
          onChange={handleChangeCar}
          placeholder="Выберите модель"
        >
          <option value="">Lorem</option>
          <option value="">Lorem</option>
          <option value="">Lorem</option>
        </Select>
        <Select
          disabled
          name="car_age"
          options={options}
          onChange={handleChangeCar}
          placeholder="Выберите поколение"
        >
          <option value="">Lorem</option>
          <option value="">Lorem</option>
          <option value="">Lorem</option>
        </Select>
        <Select
          disabled
          name="car_mod"
          options={options}
          onChange={handleChangeCar}
          placeholder="Выберите модификацию"
        />
        <Button white>Привязать</Button>
      </Grid>
    </Section>
  );
});

export default CarBasketEdit;
