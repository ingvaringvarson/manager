import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  fetchBaskets,
  basketListSelector,
  moduleName,
  createBasket,
  createdBasketIdSelector,
  basketLimitReachedSelector
} from "../../../ducks/baskets";
import { MenuItemBordered } from "../menu/MenuItem";
import BasketMenuItem from "./BasketMenuItem";
import { modulesIsLoadingSelector } from "../../../ducks/logger";
import PreLoader from "../../common/PreLoader";
import { pageFiltersSelector } from "../../../ducks/search";
import { keyRouterSelector } from "../../../redux/selectors/router";

const moduleNames = [moduleName];

class BasketMenu extends Component {
  static propTypes = {
    basketList: PropTypes.arrayOf(PropTypes.object).isRequired,
    fetchBaskets: PropTypes.func.isRequired,
    createBasket: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    createdBasketId: PropTypes.number,
    basketLimitReached: PropTypes.bool.isRequired,
    pageFilters: PropTypes.object.isRequired
  };

  state = {
    activeBasketId: null,
    prevCreatedBasketId: null
  };

  static getDerivedStateFromProps(props, state) {
    if (state.prevCreatedBasketId !== props.createdBasketId) {
      return {
        activeBasketId: props.createdBasketId,
        prevCreatedBasketId: props.createdBasketId
      };
    }

    return null;
  }

  componentDidMount() {
    this.props.fetchBaskets();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.keyRouter !== this.props.keyRouter) {
      this.setState({
        activeBasketId: null
      });
    }
  }

  handleToggleOpenBasket = id => {
    this.setState(prevState => ({
      activeBasketId: prevState.activeBasketId === id ? null : id
    }));
  };

  handleCreateBasket = () => {
    if (!this.props.basketLimitReached) this.props.createBasket();
  };

  render() {
    const {
      basketList,
      isLoading,
      basketLimitReached,
      pageFilters
    } = this.props;
    const { activeBasketId } = this.state;

    return (
      <>
        {basketList.map(basket => {
          const isAttached = !!basket.clientAgentId;
          const isActive = activeBasketId === basket.managerBasketId;
          const isFilterAttached =
            basket.managerBasketId === pageFilters.basketId;

          return (
            <BasketMenuItem
              key={basket.managerBasketId}
              isClientAttached={isAttached}
              isActive={isActive}
              isFilterAttached={isFilterAttached}
              onToggleOpenBasket={this.handleToggleOpenBasket}
              id={basket.managerBasketId}
              count={basket.itemsCount}
            />
          );
        })}
        <MenuItemBordered
          onClick={this.handleCreateBasket}
          disabled={basketLimitReached}
          icon="add_circle_outline"
        />
        {isLoading && <PreLoader viewType="content" />}
      </>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    keyRouter: keyRouterSelector(state, props),
    pageFilters: pageFiltersSelector(state, props),
    basketList: basketListSelector(state, props),
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    createdBasketId: createdBasketIdSelector(state, props),
    basketLimitReached: basketLimitReachedSelector(state, props)
  };
}

export default connect(
  mapStateToProps,
  { fetchBaskets, createBasket }
)(BasketMenu);
