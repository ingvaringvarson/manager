import React, { PureComponent } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { removeItemsFromBasket } from "../../../ducks/baskets";

import { StyledIcon } from "./styled";

class ItemRemoveControl extends PureComponent {
  static propTypes = {
    removeItemsFromBasket: PropTypes.func.isRequired,
    itemId: PropTypes.number.isRequired,
    basketId: PropTypes.number.isRequired
  };

  handleRemoveItem = () => {
    this.props.removeItemsFromBasket(this.props.basketId, [this.props.itemId]);
  };

  render() {
    return (
      <StyledIcon onClick={this.handleRemoveItem} icon="delete" width="24px" />
    );
  }
}

export default connect(
  null,
  { removeItemsFromBasket }
)(ItemRemoveControl);
