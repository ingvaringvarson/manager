import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";

import {
  fetchBasketInfo,
  basketInfoSelector,
  moduleName as basketsModuleName,
  removeBasket,
  updateBasketComment,
  updateBasketContract,
  updateBasketDelivery,
  createOrderBasedOnBasket,
  moveToProductsPage
} from "../../../ducks/baskets";
import {
  fetchEnumerations,
  enumerationsSelector,
  enumerationMapSelector,
  moduleName as enumerationsModuleName
} from "../../../ducks/enumerations";
import { contractsToOptionsByUserSelector } from "../../../ducks/user";
import { modulesIsLoadingSelector } from "../../../ducks/logger";
import { generateId, getDate, getValueInDepth } from "../../../utils";

import BlockContent from "../../../designSystem/organisms/BlockContent";

import PreLoader from "../../common/PreLoader";
import EditComment from "../../forms/EditComment";
import { TableContainer } from "../../common/tableControls";
import RenderContext from "../../common/tableControls/RenderContext";
import BasketSubmitButtons from "./BasketSubmitButtons";
import CarBasketEdit from "./CarBasketEdit";
import DeliveryBasketEdit from "./DeliveryBasketEdit";
import ContractBasketEdit from "./ContractBasketEdit";
import BasketItemsTable from "./BasketItemsTable";
import ClientBasketEdit from "./ClientBasketEdit";

import {
  Footer,
  StyledTitle,
  Order,
  FormSection,
  FormField,
  Header,
  Aside,
  StyledText,
  Section,
  StyledIcon,
  Row,
  Content,
  StyledBasket
} from "./styled";

const enumerationNames = ["city", "deliveryType", "deliveryTemplate"];

class Basket extends Component {
  static propTypes = {
    basketInfo: PropTypes.object,
    fetchBasketInfo: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    enumerations: PropTypes.shape().isRequired,
    cities: PropTypes.object.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    removeBasket: PropTypes.func.isRequired,
    contractsToOptions: PropTypes.array.isRequired,
    updateBasketComment: PropTypes.func.isRequired,
    updateBasketContract: PropTypes.func.isRequired,
    updateBasketDelivery: PropTypes.func.isRequired,
    createOrderBasedOnBasket: PropTypes.func.isRequired,
    moveToProductsPage: PropTypes.func.isRequired
  };

  componentDidMount() {
    if (this.props.id) {
      this.props.fetchEnumerations(enumerationNames);
      this.props.fetchBasketInfo(this.props.id);
    }
  }

  mapItems = memoizeOne(basketInfo => {
    const { items, initSelectedItemIds } = basketInfo.items.reduce(
      (p, i) => {
        p.items.push({
          ...i,
          basketId: basketInfo.managerBasketId
        });

        if (i.isActual) {
          p.initSelectedItemIds.push(i._id);
        }

        return p;
      },
      { items: [], initSelectedItemIds: [] }
    );

    return {
      items,
      initSelectedItemIds,
      key: generateId()
    };
  });

  getSelectedItemIds = selectedRowIds => {
    const { items } = this.mapItems(this.props.basketInfo);

    return selectedRowIds.reduce((arr, _id) => {
      const item = items.find(i => i._id === _id);

      if (item) {
        arr.push(item.managerBasketItemId);
      }

      return arr;
    }, []);
  };

  getTotalPayment = memoizeOne(items => {
    return items.reduce(
      (t, i) => {
        t.sum += i.amount;

        if (getValueInDepth(i, ["offerInfo", "offer", "isPrepaid"])) {
          t.prepaid += i.amount;
        }

        return t;
      },
      { sum: 0, prepaid: 0 }
    );
  });

  handleRemoveBasket = () => {
    this.props.removeBasket(this.props.id);
  };

  handleSubmitCommentForm = ({ comment }) => {
    const { id, updateBasketComment } = this.props;

    updateBasketComment(id, comment);
  };

  handleSubmitDeliveryForm = values => {
    const { id, updateBasketDelivery } = this.props;

    updateBasketDelivery(id, values);
  };

  handleSubmitContractForm = ({ contract_id }) => {
    const { id, updateBasketContract } = this.props;

    updateBasketContract(id, contract_id);
  };

  handleCreateOrder = (selected, type) => {
    const ids = this.getSelectedItemIds(selected);

    this.props.createOrderBasedOnBasket(this.props.id, ids, type);
  };

  handleMoveToProductsPage = () => {
    this.props.moveToProductsPage(this.props.basketInfo);
  };

  renderButtons = ({ selected }) => {
    return (
      <BasketSubmitButtons
        selected={selected}
        onClickButton={this.handleCreateOrder}
      />
    );
  };

  renderEmpty() {
    return (
      <StyledBasket>
        {this.props.isLoading && <PreLoader viewType="content" />}
        <Content>
          <Header between>
            <Row>
              <StyledTitle mr="middle">Корзина</StyledTitle>
            </Row>
          </Header>
        </Content>
        <Aside>
          <Header>
            <StyledTitle>Оформление заказа</StyledTitle>
          </Header>
        </Aside>
      </StyledBasket>
    );
  }

  render() {
    const {
      isLoading,
      cities,
      basketInfo,
      enumerations,
      contractsToOptions
    } = this.props;

    if (!basketInfo) return this.renderEmpty();

    const { key, items, initSelectedItemIds } = this.mapItems(basketInfo);
    const { sum, prepaid } = this.getTotalPayment(basketInfo.items);

    return (
      <StyledBasket>
        {isLoading && <PreLoader viewType="content" />}
        <TableContainer
          key={key}
          entities={items}
          selected={initSelectedItemIds}
          canBeSelected
        >
          <Content>
            <Header between>
              <Row>
                <StyledTitle mr="middle">
                  Корзина (ID: {basketInfo.managerBasketId})
                </StyledTitle>
                <ClientBasketEdit basketInfo={basketInfo} />
              </Row>
              <Row>
                {!!cities[basketInfo.cityId] && (
                  <StyledTitle mr="small">
                    {cities[basketInfo.cityId].name}
                  </StyledTitle>
                )}
                <StyledText mr="small" alter>
                  {getDate(basketInfo.createdDate)}
                </StyledText>
                <StyledIcon
                  onClick={this.handleRemoveBasket}
                  icon="delete"
                  width="24"
                />
              </Row>
            </Header>
            <BlockContent>
              <Section>
                <BasketItemsTable items={items} basket={basketInfo} />
                <Row>
                  <StyledIcon
                    icon="add"
                    mr="small"
                    width="24px"
                    onClick={this.handleMoveToProductsPage}
                  />
                  <StyledIcon icon="file_copy" width="24px" />
                </Row>
              </Section>
              <CarBasketEdit />
              <Section>
                <StyledText mb="small" alter>
                  Комментарий
                </StyledText>
                <EditComment
                  onSubmitForm={this.handleSubmitCommentForm}
                  comment={basketInfo.comment || ""}
                />
              </Section>
            </BlockContent>
          </Content>
          <Aside>
            <Header>
              <StyledTitle>Оформление заказа</StyledTitle>
            </Header>
            <BlockContent>
              <FormSection>
                <FormField>
                  <DeliveryBasketEdit
                    enumerations={enumerations}
                    onSubmitForm={this.handleSubmitDeliveryForm}
                    basketInfo={basketInfo}
                  />
                  <ContractBasketEdit
                    contractsToOptions={contractsToOptions}
                    onSubmitForm={this.handleSubmitContractForm}
                    basketInfo={basketInfo}
                  />
                </FormField>
              </FormSection>
              <FormSection>
                {!!prepaid && (
                  <Order>
                    <StyledTitle>Предоплата</StyledTitle>
                    <StyledTitle>{prepaid} руб.</StyledTitle>
                  </Order>
                )}
                {!!sum && (
                  <Order>
                    <StyledTitle>К оплате</StyledTitle>
                    <StyledTitle red>{sum} руб.</StyledTitle>
                  </Order>
                )}
              </FormSection>
              <Footer>
                <RenderContext key={key}>{this.renderButtons}</RenderContext>
              </Footer>
            </BlockContent>
          </Aside>
        </TableContainer>
      </StyledBasket>
    );
  }
}

const moduleNames = [basketsModuleName, enumerationsModuleName];

function mapStateToProps(state, props) {
  return {
    basketInfo: basketInfoSelector(state, props),
    cities: enumerationMapSelector(state, { name: "city" }),
    enumerations: enumerationsSelector(state, { enumerationNames }),
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    contractsToOptions: contractsToOptionsByUserSelector(state)
  };
}

export default connect(
  mapStateToProps,
  {
    fetchBasketInfo,
    fetchEnumerations,
    removeBasket,
    updateBasketComment,
    updateBasketContract,
    updateBasketDelivery,
    createOrderBasedOnBasket,
    moveToProductsPage
  }
)(Basket);
