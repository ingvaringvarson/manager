import { ifProp, palette } from "styled-tools";
import { space } from "styled-system";
import styled, { css } from "styled-components";
import { Triangle, TriangleRight } from "../../../designSystem/mixins/Triangle";
import Block from "../../../designSystem/organisms/Block";
import Icon from "../../../designSystem/atoms/Icon";
import Text from "../../../designSystem/atoms/Text";
import Title from "../../../designSystem/atoms/Title";

export const StyledBasket = styled(Block)`
  position: relative;
  display: grid;
  grid-template-columns: 1fr 350px;
  width: 1470px;

  ${Triangle};
  ${TriangleRight};
`;

export const StyledText = styled(Text)`
  a {
    color: ${palette("blue", 0)};
  }
`;

export const StyledTitle = styled(Title)`
  ${ifProp(
    "red",
    css`
      color: ${palette("primary", 0)};
    `
  )};
`;

export const StyledIcon = styled(Icon)`
  cursor: pointer;
`;

export const Row = styled.div`
  display: flex;
  align-items: center;
  ${space};
`;

export const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 110px;
  grid-column-gap: 10px;

  ${ifProp(
    "field",
    css`
      grid-template-columns: 1fr 24px;
      grid-column-gap: 5px;
    `
  )}

  ${ifProp(
    "delivery",
    css`
      grid-template-columns: 55% 45%;
      grid-column-gap: 5px;
    `
  )}
`;

export const Section = styled.div`
  :not(:last-of-type) {
    margin-bottom: 25px;
  }
`;

export const Header = styled.div`
  display: flex;
  align-items: center;
  min-height: 50px;
  padding: 6px 16px;
  border-bottom: 2px solid ${palette("grayscale", 1)};

  ${ifProp(
    "between",
    css`
      justify-content: space-between;
    `
  )};
`;

export const Content = styled(Block)`
  min-height: 150px;
`;

export const Aside = styled.div``;

export const FormSection = styled.div`
  margin-bottom: 15px;
  padding-bottom: 15px;
  border-bottom: 1px solid ${palette("grayscale", 1)};
`;

export const FormField = styled.div`
  :not(:last-of-type) {
    margin-bottom: 15px;
  }
`;

export const Order = styled.div`
  display: flex;
  justify-content: space-between;
  padding-right: 15px;

  :not(:last-of-type) {
    margin-bottom: 10px;
  }
`;

export const Footer = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const Message = styled.div`
  font-size: 12px;
  line-height: 14px;
  padding: 0 15px 0 5px;
`;

export const SimpleButton = styled.button`
  border: none;
  background-color: transparent;
  color: ${palette("general", 1)};
  text-transform: uppercase;
  cursor: pointer;
  ${space};
`;

export const RedButton = styled(SimpleButton)`
  color: ${palette("primary", 0)};
`;

export const GeneralButton = styled(SimpleButton)`
  color: ${palette("general", 1)};
`;
