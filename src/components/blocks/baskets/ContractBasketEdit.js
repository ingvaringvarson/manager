import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import { reduceValuesToFormFields } from "../../../fields/helpers";
import { generateId } from "../../../utils";
import { fieldsForContractBasket } from "../../../fields/formFields/orders/deliveryInfo";

import ContainerContext from "../../common/formControls";
import FieldForm from "../../common/form/FieldForm";

class ContractBasketEdit extends PureComponent {
  static propTypes = {
    basketInfo: PropTypes.object.isRequired,
    onSubmitForm: PropTypes.func.isRequired,
    contractsToOptions: PropTypes.array.isRequired
  };

  mapContractInfoToForm = memoizeOne((contract_id, contractsToOptions) => {
    const values = {
      contracts: contractsToOptions,
      contract_id
    };

    return {
      fields: fieldsForContractBasket.reduce(
        reduceValuesToFormFields(values),
        []
      ),
      key: generateId()
    };
  });

  render() {
    const contractFormInfo = this.mapContractInfoToForm(
      this.props.basketInfo.deliveryContractId,
      this.props.contractsToOptions
    );

    return (
      <ContainerContext
        key={contractFormInfo.key}
        fields={contractFormInfo.fields}
        onSubmitForm={this.handleSubmitContractForm}
        submitValidation
        autoSubmit
      >
        {contractFormInfo.fields.map((f, index) => {
          return (
            <FieldForm
              key={`${contractFormInfo.key}_${f.id || index}`}
              field={f}
            />
          );
        })}
      </ContainerContext>
    );
  }
}

export default ContractBasketEdit;
