import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";

import {
  openModal,
  closeModal,
  modalIsOpenSelector,
  payloadModalSelector
} from "../../../ducks/modals";
import {
  attachAgentToBasket,
  attachAgentToBasketWithCheckBasket,
  detachAgentFromBasket
} from "../../../ducks/baskets";
import { fetchClient, clientInfoSelector } from "../../../ducks/clients";

import { reduceValuesToFormFields } from "../../../fields/helpers";
import { generateId, getValueInDepth } from "../../../utils";
import { fieldForClientBasketForm } from "../../../fields/formFields/baskets/fields";
import { attachAgentToBasketWithCheckBasketModal } from "../../../constants/modalNames";

import Button from "../../../designSystem/atoms/Button";

import ContainerContext from "../../common/formControls";
import FieldForm from "../../common/form/FieldForm";
import CommonModal from "../../common/modals/CommonModal";
import Box from "../../../designSystem/organisms/Box";
import { StyledIcon } from "./styled";

class ClientBasketEdit extends PureComponent {
  static propTypes = {
    basketInfo: PropTypes.object.isRequired,
    clientInfo: PropTypes.object,
    fetchClient: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    attachAgentToBasket: PropTypes.func.isRequired,
    attachAgentToBasketWithCheckBasket: PropTypes.func.isRequired,
    detachAgentFromBasket: PropTypes.func.isRequired,
    modalIsOpen: PropTypes.bool.isRequired,
    payloadModal: PropTypes.object
  };

  componentDidMount() {
    if (this.props.basketInfo.clientAgentId && !this.props.clientInfo) {
      this.props.fetchClient(this.props.basketInfo.clientAgentId, {
        id: [this.props.basketInfo.clientAgentId]
      });
    }
  }

  componentWillUnmount() {
    if (this.props.modalIsOpen) {
      this.props.closeModal(attachAgentToBasketWithCheckBasketModal);
    }
  }

  mapClientInfoToForm = memoizeOne(client => {
    return {
      fields: fieldForClientBasketForm.reduce(
        reduceValuesToFormFields(client || {}),
        []
      ),
      key: generateId()
    };
  });

  handleAttachClient = event => {
    const agentId = getValueInDepth(this.props.payloadModal, ["id"]);
    this.props.attachAgentToBasket(
      this.props.basketInfo.managerBasketId,
      agentId,
      event.target.value === "copyBasket"
    );
    this.props.closeModal(attachAgentToBasketWithCheckBasketModal);
  };

  handleSubmitClientForm = ({ clientCode }) => {
    if (clientCode) {
      this.props.attachAgentToBasketWithCheckBasket(
        this.props.basketInfo.managerBasketId,
        clientCode
      );
    } else if (this.props.basketInfo.clientAgentId) {
      this.props.detachAgentFromBasket(this.props.basketInfo.managerBasketId);
    }
  };

  handleCloseModal = () => {
    this.props.closeModal(attachAgentToBasketWithCheckBasketModal);
  };

  handleMoveClientBasketToManagerBasket = () => {
    this.props.attachAgentToBasket(
      this.props.basketInfo.managerBasketId,
      this.props.basketInfo.clientAgentId,
      true
    );
  };

  handleMoveManagerBasketToClientBasket = () => {
    this.props.detachAgentFromBasket(this.props.basketInfo.managerBasketId, 1);
  };

  render() {
    const { modalIsOpen, clientInfo, basketInfo } = this.props;

    const clientFormInfo = this.mapClientInfoToForm(clientInfo);

    return (
      <>
        {modalIsOpen && (
          <CommonModal
            handleClose={this.handleCloseModal}
            title="Получить корзину клиента"
            isOpen={modalIsOpen}
          >
            <Box display="flex">
              <Button
                value="copyBasket"
                mr="small"
                secondary
                onClick={this.handleAttachClient}
              >
                Да
              </Button>
              <Button value="notCopyBasket" onClick={this.handleAttachClient}>
                Нет
              </Button>
            </Box>
          </CommonModal>
        )}
        <Box display="flex">
          <ContainerContext
            key={clientFormInfo.key}
            fields={clientFormInfo.fields}
            onSubmitForm={this.handleSubmitClientForm}
            submitValidation
            autoSubmit
          >
            {clientFormInfo.fields.map((f, index) => {
              return (
                <FieldForm
                  key={`${clientFormInfo.key}_${f.id || index}`}
                  field={f}
                />
              );
            })}
          </ContainerContext>
          {!!basketInfo.clientAgentId && (
            <>
              <StyledIcon
                onClick={this.handleMoveClientBasketToManagerBasket}
                width="32px"
                height="32px"
                mt="3px"
                ml="10px"
                icon="client_basket_to_manager_basket"
              />
              <StyledIcon
                onClick={this.handleMoveManagerBasketToClientBasket}
                width="32px"
                height="32px"
                mt="3px"
                ml="10px"
                icon="manager_basket_to_client_basket"
              />
            </>
          )}
        </Box>
      </>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    modalIsOpen: modalIsOpenSelector(state, {
      modalName: attachAgentToBasketWithCheckBasketModal
    }),
    clientInfo: clientInfoSelector(state, {
      id: props.basketInfo.clientAgentId
    }),
    payloadModal: payloadModalSelector(state, {
      modalName: attachAgentToBasketWithCheckBasketModal
    })
  };
}

export default connect(
  mapStateToProps,
  {
    openModal,
    closeModal,
    attachAgentToBasket,
    attachAgentToBasketWithCheckBasket,
    fetchClient,
    detachAgentFromBasket
  }
)(ClientBasketEdit);
