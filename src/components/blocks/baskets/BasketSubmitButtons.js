import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import Button from "../../../designSystem/atoms/Button";

class BasketSubmitButtons extends PureComponent {
  static propTypes = {
    selected: PropTypes.array.isRequired,
    onClickButton: PropTypes.func.isRequired
  };

  handleClick = event => {
    this.props.onClickButton(this.props.selected, +event.target.value);
  };

  getDisabledState = memoizeOne(selected => {
    return !selected.length;
  });

  render() {
    const disabled = this.getDisabledState(this.props.selected);
    return (
      <>
        <Button
          mr="small"
          secondary
          value="12"
          onClick={this.handleClick}
          disabled={disabled}
        >
          В обращение
        </Button>
        <Button value="0" onClick={this.handleClick} disabled={disabled}>
          В работу
        </Button>
      </>
    );
  }
}

export default BasketSubmitButtons;
