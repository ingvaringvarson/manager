import React, { PureComponent } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { updateQtyItemInBasket } from "../../../ducks/baskets";

import UpdateCountControl from "../../common/form/UpdateCountControl";
import { getValueInDepth } from "../../../utils";

class ItemQtyControl extends PureComponent {
  static propTypes = {
    updateQtyItemInBasket: PropTypes.func.isRequired,
    item: PropTypes.shape({
      managerBasketItemId: PropTypes.number,
      qty: PropTypes.number,
      offerInfo: PropTypes.shape({
        offer: PropTypes.shape({
          minOrder: PropTypes.number,
          qty: PropTypes.number
        })
      })
    }).isRequired,
    basketId: PropTypes.number.isRequired
  };

  handleChangeQty = qty => {
    this.props.updateQtyItemInBasket(
      this.props.basketId,
      this.props.item.managerBasketItemId,
      qty
    );
  };

  render() {
    const { item } = this.props;
    const minCount = getValueInDepth(item, ["offerInfo", "offer", "minOrder"]);
    const maxCount = getValueInDepth(item, ["offerInfo", "offer", "quantity"]);

    return (
      <UpdateCountControl
        key={item._id}
        item={item}
        onChangeCount={this.handleChangeQty}
        count={item.qty}
        minCount={minCount}
        maxCount={maxCount}
      />
    );
  }
}

export default connect(
  null,
  { updateQtyItemInBasket }
)(ItemQtyControl);
