import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { palette } from "styled-tools";
import styled from "styled-components";
import Icon from "../../../designSystem/atoms/Icon";
import MenuItem from "../menu/MenuItem";
import Basket from "./Basket";

const Container = styled.div`
  position: relative;
`;

const BasketContainer = styled.div`
  position: absolute;
  top: -12px;
  right: calc(100% + 20px);
  z-index: 5;
`;

const Overlay = styled.div`
  position: fixed;
  top: 64px;
  left: 64px;
  right: 64px;
  bottom: 0px;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 3;
`;

const NoticeIcon = styled(Icon)`
  position: absolute;
  top: 25px;
  left: 10px;
  z-index: 1;
  width: 28px;
  height: 28px;
  color: ${palette("primary", 0)};
`;

const Count = styled.div`
  position: absolute;
  top: 34px;
  left: 34px;
  z-index: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  flex-shrink: 0;
  width: 16px;
  height: 16px;
  font-size: 12px;
  line-height: normal;
  text-align: center;
  color: #ffffff;
  box-shadow: 0 0 0 2px #ffffff;
  border-radius: 50%;
  background: #000000;
  will-change: transform;
`;

class BasketMenuItem extends PureComponent {
  static propTypes = {
    onToggleOpenBasket: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    isActive: PropTypes.bool.isRequired,
    isClientAttached: PropTypes.bool,
    isFilterAttached: PropTypes.bool,
    count: PropTypes.number
  };

  handleToggleOpenBasket = () => {
    this.props.onToggleOpenBasket(this.props.id);
  };

  render() {
    const {
      count,
      isActive,
      isClientAttached,
      isFilterAttached,
      id
    } = this.props;
    return (
      <Container>
        <MenuItem
          active={isActive}
          attached={isFilterAttached}
          icon="shopping_cart"
          onClick={this.handleToggleOpenBasket}
        >
          {!!count && <Count>{count}</Count>}
          {!!isClientAttached && <NoticeIcon icon="person" />}
        </MenuItem>
        {isActive && (
          <>
            <Overlay onClick={this.handleToggleOpenBasket} />
            <BasketContainer>
              <Basket id={id} />
            </BasketContainer>
          </>
        )}
      </Container>
    );
  }
}

export default BasketMenuItem;
