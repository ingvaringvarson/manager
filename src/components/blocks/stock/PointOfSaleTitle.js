import React from "react";
import PropTypes from "prop-types";
import HeadingCard from "../../common/headlines/HeadingCard";

const PointOfSaleTitle = React.memo(({ employeeInfo }) => (
  <HeadingCard
    title="Торговая точка"
    name={employeeInfo && employeeInfo.warehouse_name}
  />
));

PointOfSaleTitle.propTypes = {
  employeeInfo: PropTypes.object.isRequired
};

export default PointOfSaleTitle;
