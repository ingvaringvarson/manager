import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import BlockContent from "../../../designSystem/organisms/BlockContent";
import Table from "../../common/tableView/Table";
import StockSubTable from "./StockSubTable";

import {
  TableContainer,
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore
} from "../../common/tableControls";

import { generateId } from "../../../utils";

export default class StockTable extends PureComponent {
  static propTypes = {
    productsInStock: PropTypes.array.isRequired,
    pagingSubEntity: PropTypes.object,
    handleSubPageOpen: PropTypes.func.isRequired,
    handleOpen: PropTypes.func.isRequired,
    fields: PropTypes.array.isRequired,
    enumerations: PropTypes.object.isRequired,
    footerElement: PropTypes.element.isRequired
  };

  getOpen = (productsInStock, pagingEntity) => {
    const result = {
      open: [],
      key: generateId()
    };

    if (!pagingEntity || !productsInStock.length) {
      return result;
    }

    const getWarehouseId = productInStock =>
      productInStock.product_in_stock && productInStock.product_in_stock.length
        ? productInStock.product_in_stock[0].warehouse_id
        : null;

    const openedProduct = productsInStock.find(
      p =>
        p._id === pagingEntity._id ||
        (p.product.article === pagingEntity.product.article &&
          p.product.brand === pagingEntity.product.brand) // &&
      // в 1C что-то сломано и склад приходит не всегда:(
      // getWarehouseId(p) === getWarehouseId(pagingEntity)
    );

    if (!openedProduct) {
      return result;
    }

    result.open.push(openedProduct._id);

    return result;
  };

  renderNestedTable = ({ entity }) => (
    <StockSubTable
      entity={entity}
      enumerations={this.props.enumerations}
      handleMoveToPage={this.props.handleSubPageOpen}
    />
  );

  render() {
    const {
      productsInStock,
      pagingSubEntity,
      footerElement,
      handleOpen
    } = this.props;

    const { open, key } = this.getOpen(productsInStock, pagingSubEntity);

    return (
      <BlockContent table>
        <TableContainer
          key={key}
          open={open}
          entities={productsInStock}
          handleOpen={handleOpen}
          hasNestedTable
        >
          <Table
            key={key}
            cells={this.props.fields}
            entities={productsInStock}
            enumerations={this.props.enumerations}
            hasChildrenBefore
            hasFormControl
            renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
            renderRowFormControlChildrenBefore={
              renderRowFormControlChildrenBefore
            }
            renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
            renderRowBodyAfter={this.renderNestedTable}
          />
        </TableContainer>
        {footerElement}
      </BlockContent>
    );
  }
}
