import React, { PureComponent } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import WideButton from "../../../designSystem/molecules/WideButton";
import Heading from "../../../designSystem/molecules/Heading";
import Column from "../../../designSystem/templates/Column";
import Block from "../../../designSystem/organisms/Block";
import Row from "../../../designSystem/templates/Row";

import ModalIconControls from "../../common/modalControls/ModalIconControls";

import PointOfSaleTitle from "../../blocks/stock/PointOfSaleTitle";

import PrintModalContent from "./PrintModalContent";

import { printBarcodes } from "../../../ducks/print";

const modalStyles = {
  content: {
    width: "454px"
  }
};

class HeaderStockPage extends PureComponent {
  static propTypes = {
    employeeInfo: PropTypes.object
  };

  handlePrintBarcode = closeModal => count => {
    this.props.printBarcodes(count);
    closeModal();
  };

  renderPrintBtn(_, modalProps) {
    return (
      <WideButton onClick={modalProps.onOpen}>Печать штрих-кодов</WideButton>
    );
  }

  renderPrintForm = (_0, modalProps, _1, iconProps) => {
    const onPrintBarcodes = this.handlePrintBarcode(modalProps.onClose);
    return (
      <Block>
        <Heading
          title="Печать штрих-кодов"
          icon="close"
          iconProps={iconProps}
        />
        <PrintModalContent onPrintBarcodes={onPrintBarcodes} />
      </Block>
    );
  };

  renderPrintContainer() {
    return (
      <ModalIconControls
        modalStyles={modalStyles}
        renderContent={this.renderPrintForm}
      >
        {this.renderPrintBtn}
      </ModalIconControls>
    );
  }

  render() {
    return (
      <Row>
        <Column basis="1200px" grow="2">
          <Row ml="0" mb="20px">
            <PointOfSaleTitle employeeInfo={this.props.employeeInfo} />
          </Row>
        </Column>
        <Column basis="350px" maxWidth="350px">
          <Row alignCenter justifyBetween mb="8px">
            {this.renderPrintContainer()}
          </Row>
        </Column>
      </Row>
    );
  }
}

export default connect(
  null,
  { printBarcodes }
)(HeaderStockPage);
