import React, { Component } from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import styled from "styled-components";
import { palette } from "styled-tools";

import Container from "../../../designSystem/templates/Container";
import Table from "../../common/tableView/Table";
import {
  TableContainer,
  ToggleOpenRow
} from "../../../components/common/tableControls";
import Pagination from "../../blocks/Pagination";
import EmptyResult from "../../common/EmptyResult";

import { pathnameRouterSelector } from "../../../redux/selectors/router";
import { requestParamsInStockSelector } from "../../../ducks/productsInStock";

import { fieldsForStockSubTable } from "../../../fields/formFields/stock";

import { subTableParams } from "../../../constants/defaultStyles/table";

import { generateId } from "../../../utils";

const StyledPagination = styled(Pagination)`
  background: ${palette("white")};
  padding: 10px;
`;

const StyledEmptyResult = styled(EmptyResult)`
  padding: 20px;
`;

class StockSubTable extends Component {
  static propTypes = {
    entity: PropTypes.object.isRequired,
    enumerations: PropTypes.object.isRequired,
    handleMoveToPage: PropTypes.func.isRequired,

    requestParams: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,

    push: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.handleMoveToPage = this.handleMoveToPage.bind(this);
  }

  handleMoveToPage = pageNumber => {
    return this.props.handleMoveToPage({
      pagingSubEntity: this.props.entity,
      pageProductUnit: pageNumber
    });
  };

  mapEnityToProductsUnits = memoizeOne(entity => {
    const singleStock = entity.product_in_stock[0] || {};
    return {
      productsUnits: singleStock.product_units || [],
      maxPages: singleStock.pages_num || 1,
      key: generateId()
    };
  });

  renderFooter = maxPages => () => {
    const { pathname, requestParams } = this.props;

    return maxPages > 1 ? (
      <StyledPagination
        pathname={pathname}
        moveToPage={this.handleMoveToPage}
        requestParams={requestParams}
        page={+requestParams.page_product_unit}
        maxPages={maxPages}
      />
    ) : null;
  };

  render() {
    const { entity, enumerations } = this.props;

    const { productsUnits, maxPages, key } = this.mapEnityToProductsUnits(
      entity
    );

    return (
      <ToggleOpenRow id={entity._id}>
        <Container>
          {productsUnits.length === 0 ? (
            <StyledEmptyResult />
          ) : (
            <TableContainer
              key={entity._id}
              entities={productsUnits}
              canBeSelected
            >
              <Table
                parentEntity={entity}
                key={key}
                tableProps={subTableParams}
                entities={productsUnits}
                cells={fieldsForStockSubTable}
                enumerations={enumerations}
                renderAfterContent={this.renderFooter(maxPages)}
              />
            </TableContainer>
          )}
        </Container>
      </ToggleOpenRow>
    );
  }
}

const mapStateToProps = (state, props) => ({
  requestParams: requestParamsInStockSelector(state, props),
  pathname: pathnameRouterSelector(state, props)
});

export default connect(
  mapStateToProps,
  {
    push
  }
)(StockSubTable);
