import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Block from "../../../designSystem/organisms/Block";
import Heading from "../../../designSystem/molecules/Heading";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import FieldForm from "../../common/form/FieldForm";

const StyledBlock = styled.div`
  width: 100%;
`;

const StockFilter = React.memo(({ fields }) => (
  <StyledBlock>
    <Block>
      <Heading title="Фильтрация" mb="10px" />
      <BlockContent>
        {fields.map((filter, index) => (
          <FieldForm field={filter} index={index} key={index} />
        ))}
      </BlockContent>
    </Block>
  </StyledBlock>
));

StockFilter.propTypes = {
  fields: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default StockFilter;
