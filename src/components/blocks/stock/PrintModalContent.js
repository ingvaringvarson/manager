import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import BlockContent from "../../../designSystem/organisms/BlockContent";
import Input from "../../../designSystem/atoms/Input";

import RightStickedButton from "../../common/buttons/RightStickedButton";

import { maskOnlyNumbers } from "../../../helpers/masks";

const PrintInputWrapper = styled.div`
  display: flex;
  align-items: center;
  input {
    width: 125px;
    margin-left: 20px;
  }
`;

export default class PrintModalContent extends Component {
  static propTypes = { onPrintBarcodes: PropTypes.func.isRequired };
  state = { inputValue: 1 };

  handlePrintBarcode = e => {
    this.props.onPrintBarcodes(this.state.inputValue);
    e.preventDefault();
  };

  handleInputChange = e => {
    const maskedValue = +maskOnlyNumbers(e.target.value);
    const value =
      maskedValue > 100
        ? 100
        : maskedValue < 1
        ? this.state.inputValue
        : maskedValue;

    this.setState({ inputValue: value });
  };

  render() {
    return (
      <BlockContent>
        <form onSubmit={this.handlePrintBarcode}>
          <PrintInputWrapper>
            <p>Количество штрих-кодов для печати</p>
            <Input
              name="count"
              value={this.state.inputValue}
              onChange={this.handleInputChange}
            />
          </PrintInputWrapper>
          <RightStickedButton>Напечатать</RightStickedButton>
        </form>
      </BlockContent>
    );
  }
}
