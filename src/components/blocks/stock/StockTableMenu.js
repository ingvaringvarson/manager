import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp } from "styled-tools";
import CommonMenu from "../../common/menus/CommonMenu";
import Icon from "../../../designSystem/atoms/Icon";
import memoizeOne from "memoize-one";
import Menu from "../../../designSystem/molecules/Menu";
//import SearchProduct from "./SearchProduct";

const ModalWrapper = styled.div`
  position: relative;
`;

const IconCommands = styled(Icon)`
  position: relative;
  cursor: pointer;
  ${ifProp(
    "disabled",
    css`
      opacity: 0.5;
      cursor: not-allowed;
    `
  )}
`;

const MenuElement = styled.p`
  cursor: pointer;
`;

const MenuWrapper = styled(Menu)`
  position: absolute;
  z-index: 3;
  left: -24px;
  top: 42px;
  width: 250px;
  right: 0;
`;

// ToDo: пока не используется, но, вероятно,
// нужно будет заменить на CommandsByStatusMenu
class StockTableMenu extends PureComponent {
  static propTypes = {
    task: PropTypes.object.isRequired,
    onSelectedProducts: PropTypes.func.isRequired,
    selectedGroup: PropTypes.string.isRequired,
    warehouseId: PropTypes.string.isRequired
  };

  commands = [
    {
      id: "searchProduct",
      title: "Поиск товара"
    }
  ];

  getAvailableIdCommands = memoizeOne(task => {
    const ids = [];

    if (task.tasks.find(item => !item.product_unit_id)) {
      ids.push("searchProduct");
    }

    return ids;
  });

  renderIconCommands = ({ handleToggle }) => {
    return <IconCommands onClick={handleToggle} icon="baseline_more" />;
  };

  renderCommonMenu = () => {
    const availableIdCommands = this.getAvailableIdCommands(this.props.task);

    return (
      <MenuWrapper>
        {this.commands.reduce((commands, command) => {
          if (!availableIdCommands.includes(command.id)) return commands;

          if (command.id === "searchProduct") {
            // commands.push(
            //   <SearchProduct
            //     key={command.id}
            //     id={this.props.task.id}
            //     title={command.title}
            //     task={this.props.task}
            //     selectedGroup={this.props.selectedGroup}
            //     onSelectedProducts={this.props.onSelectedProducts}
            //     warehouseId={this.props.warehouseId}
            //   />
            // );
          } else {
            commands.push(
              <MenuElement key={command.id} {...command.props}>
                {command.title}
              </MenuElement>
            );
          }

          return commands;
        }, [])}
      </MenuWrapper>
    );
  };

  renderMenu = () => {
    const { task } = this.props;

    const availableIdCommands = this.getAvailableIdCommands(task);

    if (!availableIdCommands.length) {
      return <IconCommands disabled icon="baseline_more" />;
    }

    return (
      <CommonMenu renderMenu={this.renderCommonMenu}>
        {this.renderIconCommands}
      </CommonMenu>
    );
  };

  render() {
    return <ModalWrapper>{this.renderMenu()}</ModalWrapper>;
  }
}

export default StockTableMenu;
