import React, { Component } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import {
  successResultsSelector,
  withdraw,
  deposit,
  printZreport,
  openShift,
  printXreport,
  reconciliationTotals,
  withdrawToCentralOffice,
  isDisabledSelector
} from "../../ducks/cash";
import nanoid from "nanoid";
import ReactModal from "react-modal";
import Block from "../../designSystem/organisms/Block";
import Heading from "../../designSystem/molecules/Heading";
import BlockContent from "../../designSystem/organisms/BlockContent";
import Button from "../../designSystem/atoms/Button";
import ContainerContext, { FieldContext } from "../common/formControls";
import {
  fieldsForDeposit,
  fieldsForWithdraw,
  fieldsForWithdrawToCentralOffice
} from "../../fields/formFields/cash/fieldsForCommands";
import BtnBlock from "../common/styled/BtnBlock";
import { customStyles } from "../../constants/defaultStyles/reactModalStyles";
import CommandsInList from "../common/menus/CommandsInList";

class CashCommands extends Component {
  static propTypes = {
    fetchEntities: PropTypes.func.isRequired,

    successResults: PropTypes.array.isRequired,
    withdraw: PropTypes.func.isRequired,
    deposit: PropTypes.func.isRequired,
    printZreport: PropTypes.func.isRequired,
    openShift: PropTypes.func.isRequired,
    printXreport: PropTypes.func.isRequired,
    reconciliationTotals: PropTypes.func.isRequired,
    isDisabled: PropTypes.bool.isRequired
  };

  getIconProps = memoizeOne(handleClose => ({
    onClick: handleClose,
    width: "24px"
  }));

  // Открыть смену
  handleOpenShift = () => {
    if (!this.props.isDisabled) this.props.openShift(nanoid());
  };

  // Напечатать X-отчёт
  handlePrintXreport = () => {
    if (!this.props.isDisabled) this.props.printXreport(nanoid());
  };

  // Сделать сверку итогов
  handleReconciliationTotals = () => {
    if (!this.props.isDisabled) this.props.reconciliationTotals(nanoid());
  };

  // Напечатать Z-отчёт
  handlePrintZreport = () => {
    if (!this.props.isDisabled) this.props.printZreport(nanoid());
  };

  // modal submit handlers

  handleDeposit = params => {
    const { commandId } = this.state;
    if (!this.props.isDisabled) {
      this.props.deposit(commandId, params, this.handleModalClose);
    }
  };

  handleWithdraw = params => {
    const { commandId } = this.state;
    if (!this.props.isDisabled) {
      this.props.withdraw(commandId, params, this.handleModalClose);
    }
  };

  modalCloseAndRefetch = () => {
    this.handleModalClose();
    this.props.fetchEntities();
  };

  handleWithdrawToCentralOffice = params => {
    const { commandId } = this.state;
    if (!this.props.isDisabled) {
      this.props.withdrawToCentralOffice(
        commandId,
        params,
        this.modalCloseAndRefetch
      );
    }
  };

  formProps = {
    deposit: {
      onSubmitForm: this.handleDeposit,
      btn: "Внести",
      fields: fieldsForDeposit
    },
    withdraw: {
      onSubmitForm: this.handleWithdraw,
      btn: "Изъять",
      fields: fieldsForWithdraw
    },
    withdrawToCentralOffice: {
      onSubmitForm: this.handleWithdrawToCentralOffice,
      btn: "Изъять",
      fields: fieldsForWithdrawToCentralOffice
    }
  };

  renderModal = modalType => (handleClose, command) => {
    const formProps = this.formProps[modalType];
    return (
      <ReactModal isOpen onRequestClose={handleClose} style={customStyles}>
        <Block>
          <Heading
            title={command.title}
            icon="close"
            iconProps={this.getIconProps(handleClose)}
          />
          <BlockContent>
            {!!formProps && (
              <ContainerContext
                fields={formProps.fields}
                onSubmitForm={formProps.onSubmitForm}
                submitValidation
              >
                {formProps.fields.map(field => (
                  <FieldContext key={field.id} name={field.id} />
                ))}
                <BtnBlock>
                  <Button type="submit">{formProps.btn}</Button>
                </BtnBlock>
              </ContainerContext>
            )}
          </BlockContent>
        </Block>
      </ReactModal>
    );
  };

  commands = [
    {
      id: "openShift",
      title: "Открыть смену",
      onClick: this.handleOpenShift
    },
    {
      id: "printXreport",
      title: "Напечатать X-отчёт",
      onClick: this.handlePrintXreport
    },
    {
      id: "reconciliationTotals",
      title: "Сделать сверку итогов",
      onClick: this.handleReconciliationTotals
    },
    {
      id: "printZreport",
      title: "Напечатать Z-отчёт",
      onClick: this.handlePrintZreport
    },
    {
      id: "deposit",
      title: "Внести деньги в кассу",
      renderModal: this.renderModal("deposit")
    },
    {
      id: "withdraw",
      title: "Изъять деньги из кассы",
      renderModal: this.renderModal("withdraw")
    },
    {
      id: "withdrawToCentralOffice",
      title: "Изъять деньги в центральную кассу",
      renderModal: this.renderModal("withdrawToCentralOffice")
    }
  ];

  render() {
    return (
      <CommandsInList
        buttonTitle="Операции с кассой"
        commands={this.commands}
      />
    );
  }
}

function mapStateToProps(state, props) {
  return {
    isDisabled: isDisabledSelector(state, props),
    successResults: successResultsSelector(state, props) || []
  };
}

export default connect(
  mapStateToProps,
  {
    withdraw,
    deposit,
    printZreport,
    openShift,
    printXreport,
    reconciliationTotals,
    withdrawToCentralOffice
  }
)(CashCommands);
