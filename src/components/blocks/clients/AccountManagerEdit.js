import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { putClient, normalizeAccountManagerEdit } from "../../../ducks/clients";
import memoizeOne from "memoize-one";

import { fields } from "../../../fields/formFields/clients/fieldsForAccountManagerEdit";
import { reduceValuesToFormFields } from "../../../fields/helpers";
import { generateId } from "../../../utils";

import ReactModal from "../../common/modals/CommonModal";
import FieldForm from "../../common/form/FieldForm";
import { ContainerContext, FormContext } from "../../common/formControls";
import RightStickedButton from "../../common/buttons/RightStickedButton";

class AccountManagerEdit extends PureComponent {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    enumerations: PropTypes.object,
    client: PropTypes.object,
    fetchEntities: PropTypes.func
  };

  state = {
    formValues: null
  };

  renderCells = memoizeOne((client, enumerations) => {
    return {
      formFields: fields.reduce(
        reduceValuesToFormFields(client, enumerations),
        []
      ),
      key: generateId()
    };
  });

  handleFormSubmit = values => {
    this.props.putClient(
      values,
      {
        client: this.props.client,
        fetchEntities: this.props.fetchEntities
      },
      normalizeAccountManagerEdit
    );
    this.props.handleClose();
  };

  handleValuesChange = formValues => this.setState({ formValues });

  render() {
    const { isOpen, handleClose, client, enumerations } = this.props;
    const { formFields, key } = this.renderCells(client, enumerations);

    return (
      <ReactModal
        ariaHideApp={false}
        isOpen={isOpen}
        title="Изменение"
        subtitle="Аккаунт-менеджера"
        handleClose={handleClose}
      >
        <ContainerContext
          key={key}
          fields={formFields}
          onSubmitForm={this.handleFormSubmit}
          onValuesChange={this.handleValuesChange}
          externalForm
        >
          <FormContext>
            {formFields.map((field, index) => (
              <FieldForm field={field} key={index} />
            ))}
            <RightStickedButton type="submit" disabled={!this.state.formValues}>
              Изменить
            </RightStickedButton>
          </FormContext>
        </ContainerContext>
      </ReactModal>
    );
  }
}

export default connect(
  null,
  { putClient }
)(AccountManagerEdit);
