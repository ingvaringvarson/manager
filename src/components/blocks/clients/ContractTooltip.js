import React, { memo } from "react";
import PropTypes from "prop-types";
import ReactTooltip from "react-tooltip";
import Icon from "../../../designSystem/atoms/Icon";
import { findEnumerationById } from "../../../utils";

const propTypes = {
  contract: PropTypes.object.isRequired,
  enumerations: PropTypes.object
};

const ContractTooltip = memo(props => {
  const cType = findEnumerationById(
    props.enumerations,
    "contractType",
    props.contract.type
  );
  const cState = findEnumerationById(
    props.enumerations,
    "contractState",
    props.contract.state
  );
  return (
    <>
      <Icon
        data-for={`contract-${props.contract.id}`}
        data-tip=""
        icon="info"
      />
      <ReactTooltip id={`contract-${props.contract.id}`}>
        {cType !== -1 && <p>Значение типа договора: {cType.name}</p>}
        {cState !== -1 && <p>Статус договора: {cState.name}</p>}
        {typeof props.contract.customer_name === "string" && (
          <p>Заказчик: {props.contract.customer_name}</p>
        )}
        {typeof props.contract.executor_name === "string" && (
          <p>Исполнитель: {props.contract.executor_name}</p>
        )}
      </ReactTooltip>
    </>
  );
});

ContractTooltip.propTypes = propTypes;

export default ContractTooltip;
