import React from "react";
import PropTypes from "prop-types";

import Icon from "../../../designSystem/atoms/Icon";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";
import ClientOrders from "./ClientOrders";
import ClientBills from "./ClientBills";
import ClientPayments from "./ClientPayments";
import ClientContracts from "./ClientContracts";
import ClientPurchaseOrders from "./ClientPurchaseOrders";
import ClientPurchaseReturns from "./ClientPurchaseReturns";
import ClientReturns from "./ClientReturns";
import Documents from "../Documents";
import ClientBalance from "./ClientBalance";
import ClientTasks from "./ClientTasks";

import documentObjectTypeId from "../../../constants/documentObjectTypeId";

const propTypes = {
  agentId: PropTypes.string.isRequired,
  selectedTabId: PropTypes.number.isRequired,
  onChangeTab: PropTypes.func.isRequired,
  isProviderMode: PropTypes.bool
};

const ClientTabs = React.memo(function ClientTabs(props) {
  const { isProviderMode } = props;
  return (
    <TabContainer
      selectedTabId={props.selectedTabId}
      onChangeSelected={props.onChangeTab}
    >
      <TabHeadContainer>
        <TabHead id={1}>
          {isProviderMode ? "Заказы поставщику" : "Заказы"}
        </TabHead>
        <TabHead id={2}>Задачи</TabHead>
        <TabHead id={3}>Баланс</TabHead>
        <TabHead id={4}>Обращения</TabHead>
        <TabHead id={5}>Счета</TabHead>
        <TabHead id={6}>Платежи</TabHead>
        <TabHead id={7}>Договор</TabHead>
        <TabHead id={8}>
          {isProviderMode ? "Возвраты поставщику" : "Возвраты"}
        </TabHead>
        {!!isProviderMode && (
          <TabHead id={9}>
            <span>Прайс-листы </span>
            <Icon width="18px" height="18px" icon="door_exit" />
          </TabHead>
        )}
        <TabHead id={10}>Документы</TabHead>
      </TabHeadContainer>
      <TabBodyContainer>
        <BlockContent table>
          <TabBody id={1}>
            {isProviderMode ? <ClientPurchaseOrders /> : <ClientOrders />}
          </TabBody>
          <TabBody id={2}>
            <ClientTasks />
          </TabBody>
          <TabBody id={3}>
            <ClientBalance />
          </TabBody>
          <TabBody id={4}>Обращения</TabBody>
          <TabBody id={5}>
            <ClientBills />
          </TabBody>
          <TabBody id={6}>
            <ClientPayments />
          </TabBody>
          <TabBody id={7}>
            <ClientContracts />
          </TabBody>
          <TabBody id={8}>
            {isProviderMode ? <ClientPurchaseReturns /> : <ClientReturns />}
          </TabBody>
          {!!isProviderMode && <TabBody id={9}>Прайс-листы content</TabBody>}
          <TabBody id={10}>
            <Documents
              idEntity={props.agentId}
              typeEntity={documentObjectTypeId.agents}
            />
          </TabBody>
        </BlockContent>
      </TabBodyContainer>
    </TabContainer>
  );
});

ClientTabs.propTypes = propTypes;

export default ClientTabs;
