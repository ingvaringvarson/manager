import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { push } from "connected-react-router";
import memoizeOne from "memoize-one";

import NestedTableForEntity from "../positions/NestedTableForEntity";
import OrderPositionsMenu from "../orders/OrderPositionsMenu";

import {
  ordersWithBillIdForEntitySelector,
  fetchOrdersForEntity,
  requestParamsByClientPageSelector,
  defaultValuesForFieldsSelector
} from "../../../ducks/orders";
import {
  entityIdForRouteSelector,
  keyRouterSelector,
  pathnameRouterSelector
} from "../../../redux/selectors/router";
import Pagination from "../Pagination";
import TableWithLinks from "../../common/table/TableWithLinks";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../ducks/enumerations";
import { ContainerContext } from "../../common/formControls";
import {
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore,
  renderRowFormControlChildrenAfter,
  TableContainer
} from "../../common/tableControls";
import {
  renderHeadOrderControl,
  renderOrderControl
} from "../../../helpers/component/byEntity/orders";
import {
  handleChangeTab,
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields
} from "../../../helpers/component/handlers";
import {
  fieldsForPositions,
  fieldsForTableByTab as fieldsForTable
} from "../../../fields/formFields/orders/ordersPage";

const enumerationNames = [
  "orderState",
  "orderPositionGoodState",
  "orderPositionServiceState",
  "deliveryType",
  "billType",
  "billState"
];

const typeEntityByRequest = "clients";

class ClientOrders extends Component {
  static propTypes = {
    requestParams: PropTypes.object.isRequired,
    orders: PropTypes.array.isRequired,
    enumerations: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,
    push: PropTypes.func.isRequired,
    maxPages: PropTypes.number.isRequired,
    fetchOrdersForEntity: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    keyRouter: PropTypes.string.isRequired,
    typeEntity: PropTypes.string.isRequired,
    fieldsForTable: PropTypes.array.isRequired,
    defaultValuesForFields: PropTypes.object.isRequired,
    idEntity: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleChangeTab = handleChangeTab.bind(this);
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
  }

  fetchEntities = () => {
    this.props.fetchOrdersForEntity(typeEntityByRequest, this.props.idEntity, {
      ...this.props.requestParams,
      isForRepeat: true
    });
  };

  componentDidMount() {
    this.fetchEntities();
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.fetchEntities();
    }
  }

  getRenderOrderControl = renderOrderControl({
    ...this.props,
    fetchEntities: this.fetchEntities
  });

  getRenderHeadOrderControl = () =>
    renderHeadOrderControl({
      ...this.props,
      fetchEntities: this.fetchEntities
    });

  renderPositionsMenu = order => ({ forList, positions }) => (
    <OrderPositionsMenu
      forList={forList}
      positions={positions}
      order={order}
      fetchEntities={this.fetchEntities}
    />
  );

  renderNestedTable = ({ entity, enumerations }) => {
    return (
      <NestedTableForEntity
        renderPositionsMenu={this.renderPositionsMenu(entity)}
        entity={entity}
        enumerations={enumerations}
        fields={fieldsForPositions}
        positions={entity.positions}
      />
    );
  };

  render() {
    const {
      requestParams,
      orders,
      enumerations,
      pathname,
      push,
      maxPages
    } = this.props;
    const { fieldsForTable, allFields, key } = this.mapValuesToFields(
      enumerations,
      orders
    );

    return (
      <>
        <ContainerContext
          key={key}
          onSort={this.handleTableSort}
          fields={allFields}
          onSubmitForm={this.handleSubmitForm}
          autoSubmit
          submitValidation
        >
          <TableContainer
            key={key}
            entities={orders}
            canBeSelected
            hasNestedTable
            stopLiftingByControls
          >
            <TableWithLinks
              cells={fieldsForTable}
              entities={orders}
              enumerations={enumerations}
              hasFormControl
              hasChildrenBefore
              hasChildrenAfter
              renderBeforeContent={this.renderTableHead}
              renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
              renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
              renderRowFormControlChildrenBefore={
                renderRowFormControlChildrenBefore
              }
              renderRowBodyChildrenAfter={this.getRenderOrderControl}
              renderRowHeadChildrenAfter={this.getRenderHeadOrderControl}
              renderRowFormControlChildrenAfter={
                renderRowFormControlChildrenAfter
              }
              renderRowBodyAfter={this.renderNestedTable}
              typeEntity="order"
            />
          </TableContainer>
        </ContainerContext>
        <Pagination
          requestParams={requestParams}
          pathname={pathname}
          push={push}
          page={+requestParams.page}
          maxPages={maxPages}
        />
      </>
    );
  }
}

function mapStateToProps(state, props) {
  const idEntity = entityIdForRouteSelector(state, props);
  const ordersForEntity = ordersWithBillIdForEntitySelector(state, {
    idEntity,
    typeEntity: typeEntityByRequest
  });
  const requestParams = requestParamsByClientPageSelector(state, props);

  return {
    idEntity,
    typeEntity: "orders",
    fieldsForTable,
    fieldsForPositions,
    requestParams,
    enumerations: enumerationsSelector(state, { enumerationNames }),
    orders: ordersForEntity.entities,
    maxPages: ordersForEntity.maxPages,
    keyRouter: keyRouterSelector(state, props),
    pathname: pathnameRouterSelector(state, props),
    defaultValuesForFields: defaultValuesForFieldsSelector(state, {
      ...props,
      pageMod: false
    })
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    {
      fetchOrdersForEntity,
      push,
      fetchEnumerations
    }
  )
)(ClientOrders);
