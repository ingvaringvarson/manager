import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import { push } from "connected-react-router";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { connect } from "react-redux";
import { requestParamsForEntitySelector } from "../../../ducks/clients";
import {
  fetchReturnsForEntity,
  returnsForEntitySelector
} from "../../../ducks/returns";
import { enumerationsSelector } from "../../../ducks/enumerations";
import {
  keyRouterSelector,
  pathnameRouterSelector,
  entityIdForRouteSelector
} from "../../../redux/selectors/router";
import {
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields
} from "../../../helpers/component/handlers";
import {
  fieldsForNestedTable,
  fieldsForTable
} from "../../../fields/formFields/clients/fieldsForClientReturns";

import { ContainerContext, FormContext } from "../../common/formControls";
import Pagination from "../Pagination";
import TableWithLinks from "../../common/table/TableWithLinks";
import {
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore,
  TableContainer,
  RenderContext
} from "../../common/tableControls";
import ReturnsNestedContentAfter from "../returns/ReturnsNestedContentAfter";

import ReturnPositionsMenu from "../returns/ReturnPositionsMenu";
import NestedTableForEntity from "../positions/NestedTableForEntity";

import ReturnsMenu from "../returns/ReturnsMenu";
import TableCell from "../../../designSystem/molecules/TableCell";

import { memoizeToArray } from "../../../helpers/cache";

const enumerationNames = [
  "agentType",
  "returnReason",
  "returnPurpose",
  "orderPositionGoodState",
  "returnState"
];

const typeEntity = "client";

class ClientReturns extends PureComponent {
  static propTypes = {
    clientId: PropTypes.string.isRequired,
    keyRouter: PropTypes.string,
    pathname: PropTypes.string,
    requestParams: PropTypes.object,
    enumerations: PropTypes.object,
    returns: PropTypes.array,
    fieldsForTable: PropTypes.array,
    maxPages: PropTypes.number,
    push: PropTypes.func.isRequired,
    fetchReturnsForEntity: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
  }

  componentDidMount = () => {
    this.fetchEntities();
  };

  componentDidUpdate = prevProps => {
    if (prevProps.keyRouter !== this.props.keyRouter) {
      this.fetchEntities();
    }
  };

  fetchEntities = () => {
    this.props.fetchReturnsForEntity(typeEntity, this.props.clientId, {
      ...this.props.requestParams,
      isForRepeat: true
    });
  };

  renderNestedTableAfterContent = entity => {
    return <ReturnsNestedContentAfter entity={entity} />;
  };

  renderPositionsMenu = returnEntity => ({ forList, positions }) => (
    <ReturnPositionsMenu
      forList={forList}
      positions={positions}
      returnEntity={returnEntity}
      fetchEntities={this.fetchEntities}
    />
  );

  renderNestedTable = ({ entity, enumerations }) => {
    if (!entity.return.positions) {
      return null;
    }

    return (
      <NestedTableForEntity
        renderPositionsMenu={this.renderPositionsMenu(entity)}
        enumerations={enumerations}
        fields={fieldsForNestedTable}
        positions={entity.return.positions}
        renderAfterContent={this.renderNestedTableAfterContent}
        entity={entity}
      />
    );
  };

  renderHeadControls = ({ selected }, { entities }) => {
    const returns = entities.filter(e => selected.includes(e._id));
    return (
      <TableCell control>
        <ReturnsMenu returns={returns} fetchEntities={this.fetchEntities} />
      </TableCell>
    );
  };

  renderRowHeadChildrenAfter = headProps => (
    <RenderContext {...headProps}>{this.renderHeadControls}</RenderContext>
  );

  renderRowBodyChildrenAfter = ({ entity }) => (
    <TableCell control>
      <ReturnsMenu
        returns={memoizeToArray(entity)}
        fetchEntities={this.fetchEntities}
      />
    </TableCell>
  );

  render() {
    const {
      enumerations,
      maxPages,
      requestParams,
      returns,
      pathname,
      push
    } = this.props;

    const { fieldsForTable, key } = this.mapValuesToFields(
      this.props.enumerations,
      this.props.returns
    );
    return (
      <>
        <ContainerContext
          key={key}
          fields={fieldsForTable}
          onSort={this.handleTableSort}
          onSubmitForm={this.handleSubmitForm}
          autoSubmit
        >
          <TableContainer
            key={key}
            entities={returns}
            canBeSelected
            hasNestedTable
          >
            <FormContext>
              <TableWithLinks
                cells={fieldsForTable}
                entities={returns}
                enumerations={enumerations}
                hasFormControl
                hasChildrenBefore
                hasChildrenAfter
                renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
                renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
                renderRowHeadChildrenAfter={this.renderRowHeadChildrenAfter}
                renderRowBodyChildrenAfter={this.renderRowBodyChildrenAfter}
                renderRowFormControlChildrenBefore={
                  renderRowFormControlChildrenBefore
                }
                renderRowBodyAfter={this.renderNestedTable}
                typeEntity="return"
              />
            </FormContext>
          </TableContainer>
        </ContainerContext>
        <Pagination
          requestParams={requestParams}
          pathname={pathname}
          push={push}
          page={+requestParams.page}
          maxPages={maxPages}
        />
      </>
    );
  }
}

function mapStateToProps(state, props) {
  const clientId = entityIdForRouteSelector(state, props);
  const requestParams = requestParamsForEntitySelector(state, props);
  return {
    clientId,
    requestParams,
    valuesForFields: requestParams,
    keyRouter: keyRouterSelector(state),
    pathname: pathnameRouterSelector(state),
    enumerations: enumerationsSelector(state, { enumerationNames }),
    returns: returnsForEntitySelector(state, { typeEntity, idEntity: clientId })
      .entities,
    maxPages: returnsForEntitySelector(state, {
      typeEntity,
      idEntity: clientId
    }).maxPages,
    fieldsForTable
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    { fetchReturnsForEntity, push }
  )
)(ClientReturns);
