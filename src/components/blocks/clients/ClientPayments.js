import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import {
  requestParamsForClientPageSelector,
  paymentsForEntitySelector,
  fetchPaymentsForEntity
} from "../../../ducks/payments";
import {
  keyRouterSelector,
  pathnameRouterSelector,
  entityIdForRouteSelector
} from "../../../redux/selectors/router";
import {
  fetchEnumerations,
  enumerationsSelector,
  enumerationListWithoutZeroIdSelector
} from "../../../ducks/enumerations";
import { push } from "connected-react-router";
import { ContainerContext } from "../../common/formControls";
import memoizeOne from "memoize-one";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";
import Pagination from "../Pagination";
import TableWithLinks from "../../common/table/TableWithLinks";
import { fieldsForTableByTypeTab } from "../../../fields/formFields/payments/paymentsPage";
import { defaultValuesForFieldsSelector } from "../../../helpers/selectors";
import { queryKeysHoldByTab } from "../../../constants/props";

import {
  handleChangeTab,
  handleMoveToEntityPage,
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields
} from "../../../helpers/component/handlers";

const enumerationNames = ["paymentState", "paymentMethod"];
const typeEntityByRequest = "clients";

class ClientPayments extends Component {
  static propTypes = {
    requestParams: PropTypes.object.isRequired,
    payments: PropTypes.array.isRequired,
    enumerations: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,
    push: PropTypes.func.isRequired,
    maxPages: PropTypes.number.isRequired,
    fetchPaymentsForEntity: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    keyRouter: PropTypes.string.isRequired,
    typeEntity: PropTypes.string.isRequired,
    fieldsForTable: PropTypes.array.isRequired,
    defaultValuesForFields: PropTypes.object.isRequired,
    idEntity: PropTypes.string.isRequired,
    queryKeysHold: PropTypes.array.isRequired,
    paymentMethods: PropTypes.array.isRequired,
    selectedTab: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleChangeTab = handleChangeTab.bind(this);
    this.handleMoveToEntityPage = handleMoveToEntityPage.bind(this);
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
    this.rowBodyHandlers = {
      onClick: this.handleMoveToEntityPage
    };
  }

  componentDidMount() {
    this.props.fetchPaymentsForEntity(
      typeEntityByRequest,
      this.props.idEntity,
      this.props.requestParams
    );
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.props.fetchPaymentsForEntity(
        typeEntityByRequest,
        this.props.idEntity,
        this.props.requestParams
      );
    }
  }

  render() {
    const {
      requestParams,
      payments,
      enumerations,
      pathname,
      push,
      maxPages,
      paymentMethods,
      selectedTab
    } = this.props;
    const { fieldsForTable, allFields, key } = this.mapValuesToFields(
      enumerations,
      payments
    );

    return (
      <TabContainer
        selectedTabId={selectedTab}
        onChangeSelected={this.handleChangeTab}
      >
        <TabHeadContainer>
          {paymentMethods.map(item => {
            return (
              <TabHead key={item.id} id={item.id}>
                {item.name}
              </TabHead>
            );
          })}
        </TabHeadContainer>
        <TabBodyContainer>
          {paymentMethods.map(item => {
            return (
              <TabBody key={item.id} id={item.id}>
                <ContainerContext
                  key={key}
                  onSort={this.handleTableSort}
                  fields={allFields}
                  onSubmitForm={this.handleSubmitForm}
                  autoSubmit
                  submitValidation
                >
                  <TableWithLinks
                    key={key}
                    cells={fieldsForTable}
                    entities={payments}
                    enumerations={enumerations}
                    hasFormControl
                    rowBodyHandlers={this.rowBodyHandlers}
                    typeEntity="payment"
                  />
                </ContainerContext>
                <Pagination
                  pathname={pathname}
                  push={push}
                  requestParams={requestParams}
                  page={+requestParams.page}
                  maxPages={maxPages}
                />
              </TabBody>
            );
          })}
        </TabBodyContainer>
      </TabContainer>
    );
  }
}

function mapStateToProps(state, props) {
  const idRoute = entityIdForRouteSelector(state, props);
  const requestParams = requestParamsForClientPageSelector(state, props);
  const queryKeyByTab = "payment_method";
  const selectedTab = +requestParams[queryKeyByTab];
  const idEntity = `${idRoute}_${selectedTab}`;
  const paymentsForEntity = paymentsForEntitySelector(state, {
    idEntity,
    typeEntity: typeEntityByRequest
  });

  return {
    requestParams,
    keyRouter: keyRouterSelector(state, props),
    pathname: pathnameRouterSelector(state, props),
    maxPages: paymentsForEntity.maxPages,
    enumerations: enumerationsSelector(state, { enumerationNames }),
    queryKeyByTab,
    typeEntity: "payments",
    selectedTab,
    payments: paymentsForEntity.entities,
    paymentMethods: enumerationListWithoutZeroIdSelector(state, {
      name: "paymentMethod"
    }),
    defaultValuesForFields: defaultValuesForFieldsSelector(state, props),
    idEntity,
    fieldsForTable: fieldsForTableByTypeTab[selectedTab],
    queryKeysHold: queryKeysHoldByTab
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    { fetchPaymentsForEntity, push, fetchEnumerations }
  )
)(ClientPayments);
