import React, { Component } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { push } from "connected-react-router";

import {
  fetchPurchaseOrdersForEntity,
  purchaseOrdersEntitySelector,
  requestParamsByClientPageSelector
} from "../../../ducks/purchaseOrders";
import {
  entityIdForRouteSelector,
  keyRouterSelector,
  pathnameRouterSelector
} from "../../../redux/selectors/router";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../ducks/enumerations";

import Pagination from "../Pagination";
import TableWithLinks from "../../common/table/TableWithLinks";
import TableCell from "../../../designSystem/molecules/TableCell";
import { ContainerContext, FormContext } from "../../common/formControls";
import {
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore,
  renderRowFormControlChildrenAfter,
  TableContainer,
  RenderContext,
  handleStopLifting
} from "../../common/tableControls";
import {
  getSelectedEntities,
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields
} from "../../../helpers/component/handlers";
import { fields } from "../../../fields/formFields/purchaseOrders/fieldsForClientEntity";

import NestedTableForEntity from "../positions/NestedTableForEntity";
import { purchaseOrdersFields as positionsFields } from "../../../fields/formFields/positions/fieldsForList";

import PurchaseOrderPositionsMenu from "../purchaseOrders/PurchaseOrderPositionsMenu";
import PurchaseOrderMenu from "../purchaseOrders/PurchaseOrderMenu";

const enumerationNames = [
  "orderState",
  "orderPositionGoodState",
  "orderSupplierState"
];
const typeEntity = "clients";

class ClientPurchaseOrders extends Component {
  static propTypes = {
    fieldsForTable: PropTypes.array,
    requestParams: PropTypes.object.isRequired,
    enumerations: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,
    maxPages: PropTypes.number.isRequired,
    keyRouter: PropTypes.string.isRequired,
    typeEntity: PropTypes.string.isRequired,
    idEntity: PropTypes.string.isRequired,
    purchaseOrders: PropTypes.array,
    push: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    fetchPurchaseOrdersForEntity: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
    this.getSelectedEntities = memoizeOne(getSelectedEntities);
  }

  getMemoizedElement = memoizeOne(el => [el]);

  fetchEntities = () => {
    this.props.fetchPurchaseOrdersForEntity(typeEntity, this.props.idEntity, {
      ...this.props.requestParams,
      isForRepeat: true
    });
  };

  componentDidMount() {
    this.fetchEntities();
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.fetchEntities();
    }
  }

  renderPositionsMenu = purchaseOrder => ({ forList, positions }) => (
    <PurchaseOrderPositionsMenu
      forList={forList}
      positions={positions}
      purchaseOrder={purchaseOrder}
      fetchEntities={this.fetchEntities}
    />
  );

  renderNestedTable = ({ entity, enumerations }) => {
    return (
      <NestedTableForEntity
        renderPositionsMenu={this.renderPositionsMenu(entity)}
        entity={entity}
        enumerations={enumerations}
        fields={positionsFields}
        positions={entity.order.positions}
      />
    );
  };

  renderHeadControls = ({ selected }, { entities }) => {
    const purchaseOrders = this.getSelectedEntities(entities, selected);
    return (
      <TableCell control>
        <PurchaseOrderMenu
          forList
          purchaseOrders={purchaseOrders}
          fetchEntities={this.fetchEntities}
        />
      </TableCell>
    );
  };

  renderRowHeadChildrenAfter = headProps => {
    return (
      <RenderContext {...headProps}>{this.renderHeadControls}</RenderContext>
    );
  };

  renderRowBodyChildrenAfter = ({ entity }) => {
    return (
      <TableCell control onClick={handleStopLifting}>
        <PurchaseOrderMenu
          purchaseOrders={this.getMemoizedElement(entity)}
          fetchEntities={this.fetchEntities}
        />
      </TableCell>
    );
  };

  render() {
    const {
      requestParams,
      purchaseOrders,
      enumerations,
      pathname,
      push,
      maxPages
    } = this.props;
    const { fieldsForTable, key } = this.mapValuesToFields(
      enumerations,
      purchaseOrders
    );
    return (
      <>
        <ContainerContext
          key={key}
          fields={fieldsForTable}
          autoSubmit
          externalForm
          submitValidation
          onSort={this.handleTableSort}
          onSubmitForm={this.handleSubmitForm}
        >
          <TableContainer
            entities={purchaseOrders}
            canBeSelected
            hasNestedTable
          >
            <FormContext>
              <TableWithLinks
                cells={fieldsForTable}
                entities={purchaseOrders}
                enumerations={enumerations}
                hasFormControl
                hasChildrenBefore
                hasChildrenAfter
                renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
                renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
                renderRowHeadChildrenAfter={this.renderRowHeadChildrenAfter}
                renderRowBodyChildrenAfter={this.renderRowBodyChildrenAfter}
                renderRowFormControlChildrenBefore={
                  renderRowFormControlChildrenBefore
                }
                renderRowFormControlChildrenAfter={
                  renderRowFormControlChildrenAfter
                }
                renderRowBodyAfter={this.renderNestedTable}
                typeEntity="purchaseOrder"
              />
            </FormContext>
          </TableContainer>
        </ContainerContext>
        <Pagination
          requestParams={requestParams}
          pathname={pathname}
          push={push}
          page={+requestParams.page}
          maxPages={maxPages}
        />
      </>
    );
  }
}

function mapStateToProps(state, props) {
  const idEntity = entityIdForRouteSelector(state, props);
  const purchaseOrders = purchaseOrdersEntitySelector(state, {
    idEntity,
    typeEntity
  });

  const requestParams = requestParamsByClientPageSelector(state, props);

  return {
    idEntity,
    typeEntity,
    fieldsForTable: fields,
    maxPages: purchaseOrders.maxPages,
    purchaseOrders: purchaseOrders.entities,
    enumerations: enumerationsSelector(state, { enumerationNames }),
    keyRouter: keyRouterSelector(state, props),
    pathname: pathnameRouterSelector(state, props),
    requestParams,
    valuesForFields: requestParams
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    {
      fetchPurchaseOrdersForEntity,
      push,
      fetchEnumerations
    }
  )
)(ClientPurchaseOrders);
