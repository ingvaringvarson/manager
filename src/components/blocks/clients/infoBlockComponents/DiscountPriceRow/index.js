import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  fetchDiscountGroups,
  fetchDiscountGroupsForAccount,
  discountGroupIdForAccountByListSelector,
  discountGroupListSelector,
  changeDiscountGroupsForAccount
} from "../../../../../ducks/discountGroup";
import styled from "styled-components";
import FormDiscountPrice from "./FormDiscountPrice";
import {
  ListDesc,
  ListRow,
  ListTitle
} from "../../../../../designSystem/organisms/List";
import { hasPermissionSelector } from "../../../../../ducks/user";
import { pm_agents_update_price_category } from "../../../../../constants/permissions";

const DiscountTag = styled.span`
  padding: 5px 8px;
  background: #c30028;
  border-radius: 5px;
  color: #fff;
  font-size: 12px;
  line-height: 20px;
`;

const Wrapper = styled.div`
  justify-content: space-between;
  display: flex;
`;

class DiscountPriceRow extends PureComponent {
  static propTypes = {
    enumerations: PropTypes.object,
    // entity - is client entity object
    entity: PropTypes.object,
    field: PropTypes.object,
    restHelperData: PropTypes.object,
    discountGroupsForAccount: PropTypes.object,
    fetchDiscountGroups: PropTypes.func.isRequired,
    fetchDiscountGroupsForAccount: PropTypes.func.isRequired,
    hasPermission: PropTypes.bool.isRequired
  };

  componentDidMount = () => {
    this.props.fetchDiscountGroupsForAccount(this.props.entity.id);
    this.props.fetchDiscountGroups();
  };

  render() {
    const { discountGroupsForAccount, field, hasPermission } = this.props;

    return (
      <ListRow>
        <ListTitle>{field.title}</ListTitle>
        <ListDesc>
          <Wrapper>
            {discountGroupsForAccount ? (
              <DiscountTag>{discountGroupsForAccount.name}</DiscountTag>
            ) : (
              <span>Не назначено</span>
            )}
            <FormDiscountPrice
              {...this.props}
              isDisabled={!hasPermission}
              handleFormSubmit={this.props.changeDiscountGroupsForAccount}
            />
          </Wrapper>
        </ListDesc>
      </ListRow>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      discountGroupsForAccount: discountGroupIdForAccountByListSelector(state, {
        idEntity: props.entity.id
      }),
      discountGroups: discountGroupListSelector(state),
      hasPermission: hasPermissionSelector(state, {
        name: pm_agents_update_price_category
      })
    };
  },
  {
    fetchDiscountGroups,
    fetchDiscountGroupsForAccount,
    changeDiscountGroupsForAccount
  }
)(DiscountPriceRow);
