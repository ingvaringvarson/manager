import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import ReactModal from "react-modal";
import memoizeOne from "memoize-one";
import styled from "styled-components";

import Block from "../../../../../designSystem/organisms/Block";
import BlockContent from "../../../../../designSystem/organisms/BlockContent";
import Heading from "../../../../../designSystem/molecules/Heading";
import Icon from "../../../../../designSystem/atoms/Icon";
import { discountPriceModal } from "../../../../../fields/formFields/clients/discountPriceForClient";
import { reduceValuesToFormFields } from "../../../../../fields/helpers";
import { ContainerContext, FormContext } from "../../../../common/formControls";
import FieldForm from "../../../../common/form/FieldForm";
import RightStickedButton from "../../../../common/buttons/RightStickedButton";
import { customStyles as defaultStyles } from "../../../../../constants/defaultStyles/reactModalStyles";
import { generateId } from "../../../../../utils";

const customStyles = {
  ...defaultStyles,
  content: {
    ...defaultStyles.content,
    width: "440px"
  }
};

const CursorIcon = styled(Icon)`
  cursor: pointer;
`;

class FormDiscountPrice extends PureComponent {
  static propTypes = {
    clientId: PropTypes.string,
    handleFormSubmit: PropTypes.func.isRequired,
    discountGroups: PropTypes.array,
    discountGroupsForAccount: PropTypes.object,
    entity: PropTypes.object,
    isDisabled: PropTypes.bool
  };

  static defaultProps = {
    isDisabled: false
  };

  state = {
    isOpen: false
  };

  handleModalToggle = () =>
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));

  handleModalClose = () => this.setState({ isOpen: false });

  handleFormSubmit = values => {
    if (this.props.isDisabled) return;

    this.props.handleFormSubmit(values, this.props.entity.id, {
      entity: this.props.entity,
      discountGroups: this.props.discountGroups,
      discountGroupsForAccount: this.props.discountGroupsForAccount
    });
    this.handleModalClose();
  };

  iconProps = {
    onClick: this.handleModalClose
  };

  renderModalData = memoizeOne(
    (
      modalData,
      enumerations,
      entity,
      discountGroupsForAccount,
      discountGroups
    ) => {
      const modalDataAfterMapping = modalData.renderView
        ? modalData.renderView({ modalData, enumerations, entity })
        : modalData;
      const fields = modalData.fields.reduce(
        reduceValuesToFormFields(
          {
            entity,
            discountGroups: discountGroups,
            discountGroupsForAccount: discountGroupsForAccount
          },
          enumerations
        ),
        []
      );

      return {
        key: generateId(),
        modalData: {
          ...modalDataAfterMapping,
          fields
        }
      };
    }
  );

  render() {
    const { key, modalData } = this.renderModalData(
      discountPriceModal,
      this.props.enumerations,
      this.props.entity,
      this.props.discountGroupsForAccount,
      this.props.discountGroups
    );

    return (
      <div>
        <CursorIcon icon="edit" onClick={this.handleModalToggle} />
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.isOpen}
          onRequestClose={this.handleModalClose}
          style={customStyles}
        >
          <Block>
            <Heading
              title={modalData.title}
              subtitle={modalData.subtitle}
              icon="close"
              iconProps={this.iconProps}
            />
            <BlockContent>
              <ContainerContext
                key={key}
                onSubmitForm={this.handleFormSubmit}
                fields={modalData.fields}
                externalForm
              >
                <FormContext>
                  {modalData.fields.map(field => (
                    <FieldForm key={field.id} name={field.id} field={field} />
                  ))}
                  <RightStickedButton
                    type="submit"
                    disabled={this.props.isDisabled}
                  >
                    Изменить
                  </RightStickedButton>
                </FormContext>
              </ContainerContext>
            </BlockContent>
          </Block>
        </ReactModal>
      </div>
    );
  }
}

export default FormDiscountPrice;
