import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import {
  fetchBillsForEntity,
  billsForEntitySelector,
  requestParamsByClientPageSelector
} from "../../../../ducks/bills";
import {
  clientIdSelector,
  clientInfoSelector
} from "../../../../ducks/clients";
import {
  keyRouterSelector,
  pathnameRouterSelector
} from "../../../../redux/selectors/router";
import Pagination from "../../Pagination";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../../ducks/enumerations";
import {
  fields,
  fieldsForPositions
} from "../../../../fields/formFields/clients/fieldsForClientBills";
import { push } from "connected-react-router";
import { generateUrlWithQuery } from "../../../../utils";
import { ContainerContext } from "../../../common/formControls";
import {
  TableContainer,
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore,
  ToggleOpenRow,
  handleStopLifting,
  renderRowFormControlChildrenAfter,
  RenderContext
} from "../../../common/tableControls";
import Container from "../../../../designSystem/templates/Container";
import Row from "../../../../designSystem/templates/Row";
import Column from "../../../../designSystem/templates/Column";
import Block from "../../../../designSystem/organisms/Block";
import Heading from "../../../../designSystem/molecules/Heading";
import BlockContent from "../../../../designSystem/organisms/BlockContent";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../../../fields/helpers";
import memoizeOne from "memoize-one";
import nanoid from "nanoid";
import PaymentsForBill from "../../payments/PaymentsForBill";
import TableWithLinks from "../../../common/table/TableWithLinks";
import Table from "../../../common/tableView/Table";
import TableCell from "../../../../designSystem/molecules/TableCell";
import BillMenu from "../../bills/BillMenu";
import CreateDummyBill from "../../bills/billCommands/CreateDummyBill";
import ModalIconControls from "../../../common/modalControls/ModalIconControls";
import renderIconBlock from "../../../common/modalControls/renderIconBlock";
import Box from "../../../../designSystem/organisms/Box";

const enumerationNames = [
  "billType",
  "billState",
  "productType",
  "contractType",
  "contractState"
];
const typeEntity = "clients";
const subTableParams = {
  subTable: true,
  ml: "80px",
  mt: "8px",
  mb: "16px"
};

class ClientBills extends Component {
  static propTypes = {
    enumerations: PropTypes.shape({
      billType: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired,
      billState: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired,
      productType: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired
    }).isRequired,
    bills: PropTypes.shape({
      entities: PropTypes.array.isRequired,
      maxPages: PropTypes.number.isRequired
    }).isRequired,
    requestParams: PropTypes.object.isRequired,
    fetchBillsForEntity: PropTypes.func.isRequired,
    keyRouter: PropTypes.string.isRequired,
    clientId: PropTypes.string.isRequired,
    client: PropTypes.object.isRequired
  };

  componentDidMount() {
    this.fetchEntities();
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.fetchEntities();
    }
  }

  fetchEntities = () => {
    this.props.fetchBillsForEntity(
      typeEntity,
      this.props.clientId,
      this.props.requestParams
    );
  };

  handleAddSuccessCallback = onClose => () => {
    this.fetchEntities();
    onClose();
  };

  renderCells = memoizeOne(enumerations => {
    const { requestParams } = this.props;

    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: nanoid()
    };
  });

  getSelectedEntities = memoizeOne((entities, selected) =>
    entities.filter(e => selected.some(s => s === e._id))
  );

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  renderNestedTable = ({ entity }) => {
    const { enumerations } = this.props;
    return (
      <ToggleOpenRow id={entity._id}>
        <Container>
          <Row>
            {entity.positions && !!entity.positions.length && (
              <Column basis="50%">
                <TableContainer
                  canBeSelected
                  key={entity.id}
                  entities={entity.positions}
                >
                  <Table
                    tableProps={subTableParams}
                    enumerations={enumerations}
                    entities={entity.positions}
                    cells={fieldsForPositions}
                    hasChildrenBefore
                    renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
                    renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
                  />
                </TableContainer>
              </Column>
            )}
            <Column basis="50%">
              <PaymentsForBill payments={entity.payments_inf} />
            </Column>
          </Row>
        </Container>
      </ToggleOpenRow>
    );
  };

  renderOptionControl = ({ entity }) => {
    return (
      <TableCell control onClick={handleStopLifting}>
        <BillMenu
          fetchEntities={this.fetchEntities}
          bills={[entity]}
          client={this.props.client}
          key={entity._id}
        />
      </TableCell>
    );
  };

  renderAddModal = (_1, modalProps, _2, iconProps) => {
    return (
      <Block>
        <Heading
          title="Создать фиктивный счет"
          icon="close"
          iconProps={iconProps}
        />
        <BlockContent>
          <CreateDummyBill
            client={this.props.client}
            successCallback={this.handleAddSuccessCallback(modalProps.onClose)}
          />
        </BlockContent>
      </Block>
    );
  };

  renderHeadControl = ({ selected }) => {
    const entities = this.getSelectedEntities(
      this.props.bills.entities,
      selected
    );

    return (
      <TableCell control>
        <Box display="flex" alignItems="center">
          <ModalIconControls renderContent={this.renderAddModal}>
            {renderIconBlock("add")}
          </ModalIconControls>
          <BillMenu
            fetchEntities={this.fetchEntities}
            bills={entities}
            client={this.props.client}
          />
        </Box>
      </TableCell>
    );
  };

  renderRowHeadChildrenAfter = () => (
    <RenderContext>{this.renderHeadControl}</RenderContext>
  );

  render() {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.bills
    );

    return (
      <>
        <ContainerContext
          key={key}
          onSort={this.handleSort}
          fields={cells}
          onSubmitForm={this.handleSubmitForm}
          autoSubmit
        >
          <TableContainer
            canBeSelected
            hasNestedTable
            entities={this.props.bills.entities}
          >
            <TableWithLinks
              key={key}
              cells={cells}
              entities={this.props.bills.entities}
              enumerations={this.props.enumerations}
              hasFormControl
              hasChildrenBefore
              hasChildrenAfter
              renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
              renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
              renderRowBodyChildrenAfter={this.renderOptionControl}
              renderRowHeadChildrenAfter={this.renderRowHeadChildrenAfter}
              renderRowFormControlChildrenBefore={
                renderRowFormControlChildrenBefore
              }
              renderRowBodyAfter={this.renderNestedTable}
              renderRowFormControlChildrenAfter={
                renderRowFormControlChildrenAfter
              }
              typeEntity="bill"
            />
          </TableContainer>
        </ContainerContext>
        <Pagination
          requestParams={this.props.requestParams}
          pathname={this.props.pathname}
          push={this.props.push}
          page={+this.props.requestParams.page}
          maxPages={this.props.bills.maxPages || 99}
        />
      </>
    );
  }
}

function mapStateToProps(state, props) {
  const clientId = clientIdSelector(state, props);
  return {
    enumerations: enumerationsSelector(state, { enumerationNames }),
    bills: billsForEntitySelector(state, {
      idEntity: clientId,
      typeEntity
    }),
    requestParams: requestParamsByClientPageSelector(state, props),
    keyRouter: keyRouterSelector(state, props),
    pathname: pathnameRouterSelector(state, props),
    clientId,
    client: clientInfoSelector(state, { id: clientId })
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    { fetchBillsForEntity, push, fetchEnumerations }
  )
)(ClientBills);
