import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import Icon from "../../../designSystem/atoms/Icon";
import AccountManagerEdit from "./AccountManagerEdit";

class HeadingCardModalWrapper extends PureComponent {
  static propTypes = {
    fetchEntities: PropTypes.func.isRequired,
    client: PropTypes.object.isRequired,
    enumerations: PropTypes.object.isRequired
  };

  state = {
    isModalOpen: false
  };

  handleModalOpen = () => this.setState({ isModalOpen: true });
  handleModalClose = () => this.setState({ isModalOpen: false });

  render() {
    return (
      <>
        <Icon icon="edit" onClick={this.handleModalOpen} />
        <AccountManagerEdit
          isOpen={this.state.isModalOpen}
          handleClose={this.handleModalClose}
          {...this.props}
        />
      </>
    );
  }
}

export default HeadingCardModalWrapper;
