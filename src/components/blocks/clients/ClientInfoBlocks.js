import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import InfoBlock from "../../common/info/InfoBlock";
import clientInfoTypes from "../../../helpers/propTypes/clientInfo";
import {
  personBlocks,
  companyBlocks
} from "../../../fields/formFields/clients/fieldsForInfoBlock";
import InfoModalControl from "../../common/info/InfoModalControl";

class ClientInfoBlocks extends PureComponent {
  static propTypes = {
    ...clientInfoTypes,
    onSubmitForm: PropTypes.func.isRequired
  };

  renderContent = info => (
    state = {},
    modalProps = null,
    blockProps = null,
    iconProps = null
  ) => (
    <InfoBlock
      iconIsShow={state.iconIsShow}
      info={info}
      blockProps={blockProps}
      iconProps={iconProps}
      entity={this.props.client}
      enumerations={this.props.enumerations}
    />
  );

  getBlock = (client, enumerations) => block => {
    let entityData = block.info.mapData
      ? block.info.mapData(client, block)
      : client[block.type];

    const info = block.info.mapValuesToFields(
      block.info,
      enumerations,
      entityData
    );
    return {
      ...block,
      info,
      renderContent: this.renderContent(info)
    };
  };

  getBlocks = memoizeOne((client, enumerations) => {
    switch (client.type) {
      case 1:
        return personBlocks.map(this.getBlock(client, enumerations));
      case 2:
        return companyBlocks.map(this.getBlock(client, enumerations));
      default:
        return null;
    }
  });

  getBlockProps = memoizeOne(onSubmitForm => ({
    onSubmitForm
  }));

  render() {
    const { client, enumerations, onSubmitForm } = this.props;

    if (!client) {
      return null;
    }

    const blocks = this.getBlocks(client, enumerations);

    if (!blocks) {
      return null;
    }

    const blockProps = this.getBlockProps(onSubmitForm);

    return blocks.map(block => {
      const { info } = block;

      if (!info.formMain) {
        return (
          <InfoBlock
            key={block.type}
            blockProps={blockProps}
            info={info}
            entity={client}
            enumerations={enumerations}
          />
        );
      }

      const fields =
        info.formMain.type === "edit" ? info.formFields : info.fieldsForAdd;

      return (
        <InfoModalControl
          key={block.type}
          formProps={info.formMain}
          onSubmitForm={onSubmitForm}
          fields={fields}
        >
          {block.renderContent}
        </InfoModalControl>
      );
    });
  }
}

export default ClientInfoBlocks;
