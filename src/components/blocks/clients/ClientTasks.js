import React from "react";
import TasksTalbeForEntity from "../tasks/TasksTableForEntity";
import { fields } from "../../../fields/formFields/tasks/agentTasksFields";
import { requestParamsByClientPageSelector } from "../../../ducks/tasks";

const typeEntity = "client";

const ClientTasks = () => {
  return (
    <TasksTalbeForEntity
      fields={fields}
      typeEntity={typeEntity}
      requestParamsSelector={requestParamsByClientPageSelector}
      hasFormControl
    />
  );
};

export default ClientTasks;
