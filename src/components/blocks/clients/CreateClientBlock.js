import React, { Fragment, PureComponent } from "react";
import PropTypes from "prop-types";
import ReactModal from "react-modal";
import Button from "../../../designSystem/atoms/Button";
import Heading from "../../../designSystem/molecules/Heading";
import Block from "../../../designSystem/organisms/Block";
import CreateClientForm from "../../forms/CreateClientForm";
import { customStyles as defaultStyles } from "../../../constants/defaultStyles/reactModalStyles";

const customStyles = {
  ...defaultStyles,
  content: {
    ...defaultStyles.content,
    width: "440px"
  }
};

class CreateClientBlock extends PureComponent {
  static propTypes = {
    onSubmitForm: PropTypes.func.isRequired
  };

  state = {
    modalIsOpen: false,
    type: 1
  };

  handleOpenModal = () => {
    this.setState({ modalIsOpen: true });
  };

  handleCloseModal = () => {
    this.setState({ modalIsOpen: false });
  };

  iconProps = {
    onClick: this.handleCloseModal,
    width: "24px",
    height: "24px"
  };

  render() {
    return (
      <Fragment>
        <Button onClick={this.handleOpenModal}>Создать клиента</Button>
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.handleCloseModal}
          style={customStyles}
        >
          <Block>
            <Heading
              title="Создать клиента"
              icon="close"
              iconProps={this.iconProps}
            />
            <CreateClientForm onSubmitForm={this.props.onSubmitForm} />
          </Block>
        </ReactModal>
      </Fragment>
    );
  }
}

export default CreateClientBlock;
