import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { push } from "connected-react-router";

import TableCell from "../../../designSystem/molecules/TableCell";
import TableRow from "../../../designSystem/molecules/TableRow";
import Title from "../../../designSystem/atoms/Title";

import { Table, RowBody } from "../../common/tableView";
import { handleStopLifting } from "../../common/tableControls";

import { pathnameRouterSelector } from "../../../redux/selectors/router";
import {
  updateClient,
  clientInfoSelector,
  clientChangedSelector
} from "../../../ducks/clients";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../ducks/enumerations";
import { fields } from "../../../fields/formFields/clients/fieldsForClientContracts";

import {
  WrappedWithModal as CreateUpdateForm,
  FormModal as CreateUpdateFormModal
} from "../contracts/CreateUpdateForm";
import ContractPrintDocumentsMenu from "./ContractPrintDocumentsMenu";

const enumerationNames = ["contractState", "contractType", "contractTerms"];

class ClientContracts extends Component {
  static propTypes = {
    enumerations: PropTypes.shape({
      contractState: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired,
      contractType: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired,
      contractTerms: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired
    }).isRequired,
    client: PropTypes.object.isRequired,
    updateClient: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    clientStateKey: PropTypes.string.isRequired
  };

  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
  }

  renderOptionControl = ({ entity }) => {
    return (
      <TableCell control onClick={handleStopLifting}>
        <ContractPrintDocumentsMenu contract={entity} />
      </TableCell>
    );
  };

  renderTableHead = () => {
    return (
      <TableRow>
        <TableCell flex="5 1 0">
          <Title>Условия договора</Title>
        </TableCell>
        <TableCell flex="4 1 0">
          <Title>Условия взаимодействия</Title>
        </TableCell>
        <TableCell flex="3 1 0">
          <Title>Баланс</Title>
        </TableCell>
        <TableCell flex="1 1 0">
          <CreateUpdateForm client={this.props.client} iconName="add" />
        </TableCell>
      </TableRow>
    );
  };

  renderTableBody = (entities, activeCells, enumerations, rowBodyProps) => {
    return entities.map((entity, index) => {
      return (
        <CreateUpdateFormModal client={this.props.client} contract={entity}>
          <RowBody
            {...rowBodyProps}
            entity={entity}
            key={entity.key || entity._id || index}
          />
        </CreateUpdateFormModal>
      );
    });
  };

  renderEmptyCell = () => <TableCell />;

  render() {
    return (
      <Table
        key={this.props.clientStateKey}
        cells={fields}
        entities={this.props.client.contracts || []}
        enumerations={this.props.enumerations}
        hasChildrenAfter
        renderRowHeadChildrenAfter={this.renderEmptyCell}
        renderRowBodyChildrenAfter={this.renderOptionControl}
        renderBeforeContent={this.renderTableHead}
        renderBody={this.renderTableBody}
        hasExternalBody
      />
    );
  }
}

function mapStateToProps(state, props) {
  return {
    enumerations: enumerationsSelector(state, { enumerationNames }),
    client: clientInfoSelector(state, props.match.params),
    pathname: pathnameRouterSelector(state, props),
    clientStateKey: clientChangedSelector(state, props.match.params)
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    {
      push,
      updateClient,
      fetchEnumerations
    }
  )
)(ClientContracts);
