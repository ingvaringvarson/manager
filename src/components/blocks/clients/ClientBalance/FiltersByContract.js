import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import { ContainerContext, FieldContext } from "../../../common/formControls";
import { fieldsForContractFilters } from "../../../../fields/formFields/clients/fieldsForClientBalances";
import { generateUrlWithQuery, generateId } from "../../../../utils";

import ContractPrintDocumentsMenu from "../ContractPrintDocumentsMenu";

import memoizeOne from "memoize-one";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../../../fields/helpers";

const HeaderWrapper = styled.div`
  display: flex;
  margin: 16px 0;
`;

const FieldWrapper = styled.div`
  display: inline-block;
  vertical-align: top;
  min-width: 300px;
  & > div {
    width: 200px;
    display: inline-block;
    vertical-align: middle;
  }
  & > label {
    vertical-align: middle;
    padding: 0 1rem;
  }
`;

class FiltersByContract extends PureComponent {
  static propTypes = {
    contracts: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        number: PropTypes.string.isRequired
      })
    ).isRequired,
    enumerations: PropTypes.shape({
      accountType: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired
    }).isRequired,
    requestParams: PropTypes.object,
    pathname: PropTypes.string.isRequired,
    push: PropTypes.func.isRequired
  };

  handleSubmitForm = (values, fields) => {
    const { pathname, push, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    push(generateUrlWithQuery({ pathname, newQuery }));
  };

  getContract = memoizeOne(contractId =>
    this.props.contracts.find(c => c.id === contractId)
  );

  renderPrintDocumentsMenu = () => {
    const {
      requestParams: { contract_id }
    } = this.props;
    return !!contract_id ? (
      <ContractPrintDocumentsMenu contract={this.getContract(contract_id)} />
    ) : null;
  };

  renderCell = memoizeOne((requestParams, contracts, enumerations, fields) => {
    return {
      fields: fields.reduce(
        reduceValuesToFormFields({ ...requestParams, contracts }, enumerations),
        []
      ),
      key: generateId()
    };
  });

  render() {
    const { fields, key } = this.renderCell(
      this.props.requestParams,
      this.props.contracts,
      this.props.enumerations,
      fieldsForContractFilters
    );

    return (
      <HeaderWrapper>
        <ContainerContext
          fields={fields}
          onSubmitForm={this.handleSubmitForm}
          key={key}
          autoSubmit
        >
          {fields.map(cell => {
            return (
              <FieldWrapper key={cell.id}>
                <FieldContext name={cell.id} />
              </FieldWrapper>
            );
          })}
        </ContainerContext>
        {this.renderPrintDocumentsMenu()}
      </HeaderWrapper>
    );
  }
}

export default FiltersByContract;
