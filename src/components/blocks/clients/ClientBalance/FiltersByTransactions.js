import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import memoizeOne from "memoize-one";

import { ContainerContext, FieldContext } from "../../../common/formControls";
import { fieldsForTransactionsFilters } from "../../../../fields/formFields/clients/fieldsForClientBalances";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../../../fields/helpers";
import { generateUrlWithQuery, generateId } from "../../../../utils";

import Button from "../../../../designSystem/atoms/Button";

const FilterBlockWrapper = styled.div`
  display: inline-block;
  width: 350px;
  margin-left: 30px;
  vertical-align: top;
`;

const DisplayFlexBlock = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const FilterBlockTime = styled(DisplayFlexBlock)`
  margin-top: 16px;
  & > div {
    display: inline-block;
    width: 150px;
  }
`;

const BalancesBlock = styled.div`
  background: #f1f1f1;
  padding: 12px;
  border-radius: 5px;
  margin-top: 15px;
  font-size: 14px;
  & > p {
    padding: 12px 0;
    & > span {
      color: #eb0028;
    }
  }
`;

class FiltersByTransactions extends PureComponent {
  static propTypes = {
    requestParams: PropTypes.object,
    enumerations: PropTypes.object,
    balance: PropTypes.object,
    pathname: PropTypes.string,
    push: PropTypes.func.isRequired
  };

  handleSubmit = (values, fields) => {
    const { requestParams, pathname, push } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    push(generateUrlWithQuery({ pathname, newQuery }));
  };

  renderCells = memoizeOne((requestParams, enumerations, fields) => {
    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: generateId()
    };
  });

  render() {
    const { requestParams, enumerations, balance } = this.props;
    const { key, cells } = this.renderCells(
      requestParams,
      enumerations,
      fieldsForTransactionsFilters
    );
    const beginingSum =
      requestParams.account_type === 1
        ? balance.account_in_begining_sum
        : balance.account_out_begining_sum;
    const closingSum =
      requestParams.account_type === 1
        ? this.props.balance.account_in_closing_sum
        : this.props.balance.account_out_closing_sum;

    return (
      <FilterBlockWrapper>
        <ContainerContext
          key={key}
          fields={cells}
          onSubmitForm={this.handleSubmit}
        >
          <DisplayFlexBlock>
            <div>Выбранный период</div>
            <Button type="submit">Сформировать</Button>
          </DisplayFlexBlock>
          <FilterBlockTime>
            {!!cells[0] && <FieldContext name={cells[0].id} />}-
            {!!cells[1] && <FieldContext name={cells[1].id} />}
          </FilterBlockTime>
          <BalancesBlock>
            <p>
              Сальдо на конец периода: <span>{beginingSum}</span> руб.
            </p>
            <p>
              Сальдо на начало периода: <span>{closingSum}</span> руб.
            </p>
          </BalancesBlock>
        </ContainerContext>
      </FilterBlockWrapper>
    );
  }
}

export default FiltersByTransactions;
