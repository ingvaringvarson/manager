import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";

import { push } from "connected-react-router";
import { connect } from "react-redux";
import {
  fetchEnumerations,
  enumerationsSelector,
  enumerationListSelector
} from "../../../../ducks/enumerations";
import { clientInfoSelector } from "../../../../ducks/clients";
import {
  requestParamsByClientPageSelector,
  fetchBalancesForEntity,
  balancesForEntitySelector,
  mappedBalanceTransactionsSelector
} from "../../../../ducks/balances";
import {
  entityIdForRouteSelector,
  keyRouterSelector,
  pathnameRouterSelector
} from "../../../../redux/selectors/router";

import FiltersByContract from "./FiltersByContract";
import BalanceScore from "./BalanceScore";
import Pagination from "../../Pagination";
import FiltersByTransactions from "./FiltersByTransactions";

import styled from "styled-components";
import { fieldsForTransactionsTable } from "../../../../fields/formFields/clients/fieldsForClientBalances";
import Table from "../../../common/table/Table";
import moment from "moment";

const enumerationNames = ["accountType", "transactionType"];
const typeEntity = "clients";

const TableWrapper = styled.div`
  display: inline-block;
  width: 900px;
  vertical-align: top;
`;

function decorateRequestParams(params, nextParams) {
  const initParams = {
    begining_date: "2010-01-01T00:00:00",
    closing_date: `${moment(Date.now()).format("YYYY-MM-DD")}T23:59:59`,
    ...params,
    ...nextParams
  };

  if (nextParams !== undefined) {
    if (!("begining_date" in nextParams)) delete initParams.begining_date;
    if (!("closing_date" in nextParams)) delete initParams.closing_date;
  }

  return initParams;
}

class ClientBalance extends Component {
  static getDerivedStateFromProps(props, state) {
    if (props.requestParams !== state.propsRequestParams) {
      return {
        requestParams: decorateRequestParams(
          state.requestParams,
          props.requestParams
        ),
        propsRequestParams: props.requestParams
      };
    }
    return null;
  }

  state = {
    requestParams: decorateRequestParams(this.props.requestParams),
    propsRequestParams: this.props.requestParams
  };

  componentDidMount() {
    const { requestParams } = this.state;
    const { fetchEnumerations, fetchBalancesForEntity } = this.props;

    fetchEnumerations(enumerationNames);
    if (requestParams.contract_id) {
      fetchBalancesForEntity(
        typeEntity,
        requestParams.contract_id,
        requestParams
      );
    }
  }

  componentDidUpdate(prevProps) {
    const { requestParams } = this.state;
    const { keyRouter, fetchBalancesForEntity } = this.props;
    if (prevProps.keyRouter !== keyRouter && requestParams.contract_id) {
      fetchBalancesForEntity(
        typeEntity,
        requestParams.contract_id,
        requestParams
      );
    }
  }

  render() {
    const { requestParams } = this.state;
    const {
      client,
      pathname,
      push,
      enumerations,
      balanceInfo,
      maxPages
    } = this.props;

    if (!client || !client.contracts) return null;

    return (
      <div>
        <FiltersByContract
          requestParams={requestParams}
          pathname={pathname}
          push={push}
          enumerations={enumerations}
          contracts={client.contracts}
        />
        {!!requestParams.contract_id && balanceInfo && (
          <>
            <BalanceScore
              total={balanceInfo.balance.account_out_total_balance}
              dept={balanceInfo.balance.dept}
            />

            <TableWrapper>
              {balanceInfo.balance.transactions && (
                <Table
                  cells={fieldsForTransactionsTable}
                  entities={this.props.transactions}
                  enumerations={enumerations}
                />
              )}
              <Pagination
                requestParams={requestParams}
                pathname={pathname}
                push={push}
                page={+requestParams.page}
                maxPages={maxPages || 99}
              />
            </TableWrapper>
            <FiltersByTransactions
              balance={balanceInfo}
              requestParams={this.state.requestParams}
              push={push}
              enumerations={enumerations}
              pathname={pathname}
            />
          </>
        )}
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  const clientId = entityIdForRouteSelector(state, props);
  const requestParams = requestParamsByClientPageSelector(state, {
    id: clientId
  });
  const idEntity = requestParams.contract_id;
  const { maxPages, entities } = balancesForEntitySelector(state, {
    ...props,
    typeEntity,
    idEntity
  });
  const balanceInfo = entities[0];

  return {
    maxPages,
    balanceInfo,
    requestParams,
    clientId,
    enumerations: enumerationsSelector(state, { enumerationNames }),
    accountTypes: enumerationListSelector(state, {
      name: "accountType"
    }),
    pathname: pathnameRouterSelector(state),
    keyRouter: keyRouterSelector(state),
    client: clientInfoSelector(state, { id: clientId }),
    transactions: mappedBalanceTransactionsSelector(state, {
      balance: balanceInfo
    })
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    {
      fetchEnumerations,
      push,
      fetchBalancesForEntity
    }
  )
)(ClientBalance);
