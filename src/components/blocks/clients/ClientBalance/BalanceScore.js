import React, { memo } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const propTypes = {
  total: PropTypes.number,
  dept: PropTypes.string
};

const ScoreWrapper = styled.div`
  padding: 24px 0;
  background: #f1f1f1;
  font-size: 18px;
  color: #383838;
  margin-bottom: 16px;
  p {
    display: inline-block;
    vertical-align: middle;
    padding-left: 20px;
  }
`;

const RedParagraph = styled.p`
  color: #ed0028;
`;

const RedSpan = styled.span`
  color: #ed0028;
`;

const BalanceScore = memo(props => {
  return (
    <ScoreWrapper>
      <p>
        Остаток на балансе: <RedSpan>{props.total}</RedSpan> руб.
      </p>
      {props.dept > 0 && (
        <RedParagraph>Просроченная задолжность: {props.dept} руб.</RedParagraph>
      )}
    </ScoreWrapper>
  );
});

BalanceScore.propTypes = propTypes;

export default BalanceScore;
