import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import styled from "styled-components";

import { IconControl } from "./../../../common/modalControls/renderIconBlock";
import CommandButton from "../../../common/buttons/CommandButton";
import Preloader from "../../../../designSystem/atoms/Spinner";
import CommonMenu from "../../../common/menus/CommonMenu";
import DocumentPrint from "../../DocumentPrint";

import {
  fetchFileslinkForObject,
  fileslinkForObjectSelector,
  moduleName as moduleNameFileslink
} from "../../../../ducks/fileslink";
import { fetchFile } from "../../../../ducks/files";
import { modulesIsLoadingSelector } from "../../../../ducks/logger";

import documentObjectTypeId from "../../../../constants/documentObjectTypeId";
import { memoizeToArray } from "../../../../helpers/cache";

const ModalWrapper = styled.div`
  position: relative;
`;

// ToDo: заменить на CommandsByStatusMenu
class ContractPrintDocumentsMenu extends PureComponent {
  static propTypes = {
    contract: PropTypes.object.isRequired,

    isLoading: PropTypes.bool.isRequired,

    fileslinks: PropTypes.arrayOf(PropTypes.object),
    fetchFile: PropTypes.func.isRequired
  };

  state = {
    nameModalShow: ""
  };

  fetchFileslinkForObject = () =>
    this.props.fetchFileslinkForObject(
      documentObjectTypeId.contracts,
      this.props.contract.id
    );

  componentDidMount = () => {
    this.fetchFileslinkForObject();
  };

  handleModalOpen = id => () => this.setState({ nameModalShow: id });
  handleModalClose = () => this.setState({ nameModalShow: "" });
  handleDownloadFile = () =>
    this.props.fileslinks.forEach(f => this.props.fetchFile(f.id));

  commandIds = {
    printDocuments: "printDocuments"
  };

  printDocumentsCommand = {
    id: this.commandIds.printDocuments,
    title: "Печать документов",
    onClick: this.handleModalOpen(this.commandIds.printDocuments)
  };

  renderToggleButton = ({ handleToggle }) => (
    <CommandButton onClick={handleToggle} />
  );

  renderMenu = () => {
    switch (true) {
      case this.props.isLoading:
        return <Preloader size="small" />;
      case !!this.props.fileslinks.length:
        return (
          <IconControl
            onClick={this.handleDownloadFile}
            icon={"cloud_download"}
          />
        );
    }

    return (
      <CommonMenu menuOptions={memoizeToArray(this.printDocumentsCommand)}>
        {this.renderToggleButton}
      </CommonMenu>
    );
  };

  renderModal = () => {
    switch (this.state.nameModalShow) {
      case this.commandIds.printDocuments:
        const { contract } = this.props;
        let entityFirmId;
        switch (contract.type) {
          case 1:
            entityFirmId = contract.executor_id;
            break;
          case 2:
            entityFirmId = contract.customer_id;
            break;
          default:
            throw new Error(
              `Для данного типа договора [${contract.type}] нельзя создать акт сверки`
            );
        }

        return (
          <DocumentPrint
            idEntity={contract.id}
            objectTypeId={documentObjectTypeId.contracts}
            entityFirmId={entityFirmId}
            handleClose={this.handleModalClose}
          />
        );
      default:
        return null;
    }
  };

  render() {
    return (
      <ModalWrapper>
        {this.renderMenu()}
        {this.renderModal()}
      </ModalWrapper>
    );
  }
}

const moduleNames = [moduleNameFileslink];

export default connect(
  (state, props) => ({
    fileslinks: fileslinkForObjectSelector(state, {
      idEntity: props.contract.id,
      typeEntity: documentObjectTypeId.contracts
    }).entities,
    isLoading: modulesIsLoadingSelector(state, { moduleNames })
  }),
  {
    fetchFile,
    fetchFileslinkForObject
  }
)(ContractPrintDocumentsMenu);
