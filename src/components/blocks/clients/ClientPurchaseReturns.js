import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { push } from "connected-react-router";

import Pagination from "../Pagination";
import TableWithLinks from "../../common/table/TableWithLinks";
import { ContainerContext, FormContext } from "../../common/formControls";
import {
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore,
  renderRowFormControlChildrenAfter,
  TableContainer,
  RenderContext
} from "../../common/tableControls";
import NestedTableForEntity from "../positions/NestedTableForEntity";
import NestedTableAfter from "../../common/tableView/NestedTableAfter";
import PurchaseReturnPositionsMenu from "../purchaseReturns/PurchaseReturnPositionsMenu";

import {
  entityIdForRouteSelector,
  keyRouterSelector,
  pathnameRouterSelector
} from "../../../redux/selectors/router";
import { requestParamsForEntitySelector } from "../../../ducks/clients";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../ducks/enumerations";
import {
  fetchPurchaseReturnsForEntity,
  purchaseReturnsForEntitySelector
} from "../../../ducks/purchaseReturns";
import {
  handleSubmitForm,
  handleTableSort,
  mapValuesToFields
} from "../../../helpers/component/handlers";
import { fields } from "../../../fields/formFields/purchaseReturns/fieldsForClientEntity";
import { purchaseReturnFields as positionsFields } from "../../../fields/formFields/positions/fieldsForList";
import PurchaseReturnMenu from "../purchaseReturns/PurchaseReturnMenu";
import TableCell from "../../../designSystem/molecules/TableCell";

import { memoizeToArray } from "../../../helpers/cache";

const enumerationNames = [
  "returnReason",
  "purchaseReturnState",
  "orderPositionGoodState"
];

const typeEntity = "clients";

class ClientPurchaseReturns extends PureComponent {
  static propTypes = {
    requestParams: PropTypes.object.isRequired,
    enumerations: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,
    push: PropTypes.func.isRequired,
    maxPages: PropTypes.number.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    keyRouter: PropTypes.string.isRequired,
    typeEntity: PropTypes.string.isRequired,
    idEntity: PropTypes.string.isRequired,
    purchaseReturns: PropTypes.array
  };

  constructor(props) {
    super(props);

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
    this.handleSubmitForm = handleSubmitForm.bind(this);
    this.handleTableSort = handleTableSort.bind(this);
  }

  fetchEntities = () => {
    this.props.fetchPurchaseReturnsForEntity(
      typeEntity,
      this.props.idEntity,
      this.props.requestParams
    );
  };

  componentDidMount() {
    this.fetchEntities();
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.fetchEntities();
    }
  }

  renderAfterNestedTable = purchaseReturn => {
    const { sum_payment, sum_total } = purchaseReturn.purchase_return;
    return <NestedTableAfter sum_payment={sum_payment} sum_total={sum_total} />;
  };

  renderPositionsMenu = purchaseReturn => ({ forList, positions }) => (
    <PurchaseReturnPositionsMenu
      forList={forList}
      positions={positions}
      purchaseReturn={purchaseReturn}
      fetchEntities={this.fetchEntities}
    />
  );

  renderNestedTable = ({ entity, enumerations }) => {
    return (
      <NestedTableForEntity
        renderPositionsMenu={this.renderPositionsMenu(entity)}
        entity={entity}
        enumerations={enumerations}
        fields={positionsFields}
        positions={entity.purchase_return.positions}
        renderAfterContent={this.renderAfterNestedTable}
      />
    );
  };

  renderHeadControls = ({ selected }, { entities }) => {
    const purchaseReturns = entities.filter(e => selected.includes(e._id));
    return (
      <TableCell control>
        <PurchaseReturnMenu
          purchaseReturns={purchaseReturns}
          fetchEntities={this.fetchEntities}
        />
      </TableCell>
    );
  };

  renderRowHeadChildrenAfter = headProps => (
    <RenderContext {...headProps}>{this.renderHeadControls}</RenderContext>
  );

  renderRowBodyChildrenAfter = ({ entity }) => (
    <TableCell control>
      <PurchaseReturnMenu
        purchaseReturns={memoizeToArray(entity)}
        fetchEntities={this.fetchEntities}
      />
    </TableCell>
  );

  render() {
    const {
      enumerations,
      requestParams,
      purchaseReturns,
      maxPages,
      pathname,
      push
    } = this.props;

    const { key, fieldsForTable } = this.mapValuesToFields(
      enumerations,
      purchaseReturns
    );

    return (
      <>
        <ContainerContext
          key={key}
          fields={fieldsForTable}
          autoSubmit
          externalForm
          submitValidation
          onSort={this.handleTableSort}
          onSubmitForm={this.handleSubmitForm}
        >
          <TableContainer
            entities={purchaseReturns}
            canBeSelected
            hasNestedTable
          >
            <FormContext>
              <TableWithLinks
                cells={fieldsForTable}
                entities={purchaseReturns}
                enumerations={enumerations}
                hasFormControl
                hasChildrenBefore
                hasChildrenAfter
                renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
                renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
                renderRowBodyChildrenAfter={this.renderRowBodyChildrenAfter}
                renderRowHeadChildrenAfter={this.renderRowHeadChildrenAfter}
                renderRowFormControlChildrenBefore={
                  renderRowFormControlChildrenBefore
                }
                renderRowFormControlChildrenAfter={
                  renderRowFormControlChildrenAfter
                }
                renderRowBodyAfter={this.renderNestedTable}
                typeEntity="purchaseReturn"
              />
            </FormContext>
          </TableContainer>
        </ContainerContext>
        <Pagination
          requestParams={requestParams}
          pathname={pathname}
          push={push}
          page={+requestParams.page}
          maxPages={maxPages}
        />
      </>
    );
  }
}

function mapStateToProps(state, props) {
  const idEntity = entityIdForRouteSelector(state, props);
  const purchaseReturns = purchaseReturnsForEntitySelector(state, {
    idEntity,
    typeEntity
  });

  const requestParams = requestParamsForEntitySelector(state, props);

  return {
    idEntity,
    typeEntity,
    fieldsForTable: fields,
    maxPages: +purchaseReturns.maxPages,
    purchaseReturns: purchaseReturns.entities,
    keyRouter: keyRouterSelector(state, props),
    pathname: pathnameRouterSelector(state, props),
    requestParams,
    valuesForFields: requestParams,
    enumerations: enumerationsSelector(state, { enumerationNames })
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    {
      push,
      fetchEnumerations,
      fetchPurchaseReturnsForEntity
    }
  )
)(ClientPurchaseReturns);
