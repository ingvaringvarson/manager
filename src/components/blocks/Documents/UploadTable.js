import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

import LoadButton from "./../../../designSystem/molecules/LoadButton";

import TableContainer from "./../../common/tableControls/TableContainer";

import { fields } from "../../../fields/formFields/documents/fieldsOfUploadFilesTable";
import Button from "../../../designSystem/atoms/Button";
import Table from "../../common/table/Table";
import Select from "../../../designSystem/molecules/Select";

const RightStickedButtonsWrapper = styled.div`
  padding-top: 16px;
  text-align: right;
`;

const StyledSmallLoadButton = styled(LoadButton)`
  margin-right: 10px;
`;

const isMainSelectOptions = [
  {
    id: "0",
    name: "нет"
  },
  {
    id: "1",
    name: "да"
  }
];

const renderIsMain = handleOnIsMainSelectChange => ({ entity }) => {
  const options = isMainSelectOptions;
  const handleOnChange = handleOnIsMainSelectChange(entity.id);
  const selectedOption = entity.isMain ? options[1].id : options[0].id;
  return (
    <Select
      name={"is-main-select"}
      onChange={handleOnChange}
      selected={selectedOption}
      options={options}
    />
  );
};

const UploadTable = ({ ...props }) => (
  <>
    <TableContainer>
      <Table
        cells={fields(renderIsMain(props.handleOnIsMainSelectChange))}
        entities={props.filesMeta}
      />
    </TableContainer>
    <RightStickedButtonsWrapper>
      <StyledSmallLoadButton onClick={props.handeLoadButtonClick}>
        Загрузить eще
      </StyledSmallLoadButton>
      <Button onClick={props.handleUpload}>Сохранить</Button>
    </RightStickedButtonsWrapper>
  </>
);

UploadTable.propTypes = {
  handleOnIsMainSelectChange: PropTypes.func.isRequired,
  handleUpload: PropTypes.func.isRequired,
  handeLoadButtonClick: PropTypes.func.isRequired,
  filesMeta: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default UploadTable;
