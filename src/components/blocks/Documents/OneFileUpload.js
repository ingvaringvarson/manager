import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import LoadButton from "../../../designSystem/molecules/LoadButton";

import RightStickedButton from "../../common/buttons/RightStickedButton";

const Wrapper = styled.div`
  margin: 10px;
`;

const StyledLoadButton = styled(LoadButton)`
  width: 100%;
  height: 100%;
  font-size: 17px;
`;

const OneFileUpload = ({ ...props }) => (
  <>
    <Wrapper>
      <StyledLoadButton
        onClick={props.handeLoadButtonClick}
        iconSize="28px"
        wrapperHeight="60px"
      >
        Загрузить файлы
      </StyledLoadButton>
    </Wrapper>
    <RightStickedButton onClick={props.handleUpload}>
      Сохранить
    </RightStickedButton>
  </>
);

OneFileUpload.propTypes = {
  handeLoadButtonClick: PropTypes.func.isRequired,
  handleUpload: PropTypes.func.isRequired
};

export default OneFileUpload;
