import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import ReactTooltip from "react-tooltip";

import BlockContent from "../../../designSystem/organisms/BlockContent";
import TableCell from "../../../designSystem/molecules/TableCell";
import Heading from "../../../designSystem/molecules/Heading";
import Block from "../../../designSystem/organisms/Block";

import ModalIconControls from "../../common/modalControls/ModalIconControls";
import { IconControl } from "./../../common/modalControls/renderIconBlock";
import renderIconBlock from "../../common/modalControls/renderIconBlock";
import PreLoader from "../../common/PreLoader";

import { Table } from "../../common/tableView";

import UploadModalContent from "./UploadModalContent";

import { fields } from "../../../fields/formFields/documents/fieldsOfDocumentsTabTable";

import { fetchFile } from "../../../ducks/files";
import {
  uploadAndLinkFiles,
  fetchFileslinkForObject,
  fileslinkForObjectSelector,
  moduleName as moduleNameFileslink
} from "../../../ducks/fileslink";
import { clientsForEntitySelector } from "./../../../ducks/clients";
import { modulesIsLoadingSelector } from "../../../ducks/logger";

import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../../ducks/enumerations";

import { reduceValuesToFormFields } from "../../../fields/helpers";

import { keyRouterSelector } from "../../../redux/selectors/router";

const enumerationNames = ["documentTemplate", "documentsType"];

class Documents extends PureComponent {
  static propTypes = {
    idEntity: PropTypes.string.isRequired,
    typeEntity: PropTypes.number.isRequired,

    fetchFileslinkForObject: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    uploadAndLinkFiles: PropTypes.func.isRequired,
    fetchFile: PropTypes.func.isRequired,
    clientsFromSearch: PropTypes.object.isRequired,

    isLoading: PropTypes.bool.isRequired,
    keyRouter: PropTypes.string.isRequired,

    fileslinks: PropTypes.arrayOf(PropTypes.object),
    requestParams: PropTypes.object,
    enumerations: PropTypes.object
  };

  fetchFileslinkForObject = () =>
    this.props.fetchFileslinkForObject(
      this.props.typeEntity,
      this.props.idEntity
    );

  componentDidMount = () => {
    this.props.fetchEnumerations(enumerationNames);
    this.fetchFileslinkForObject();
  };

  componentDidUpdate = prevProps => {
    if (prevProps.keyRouter !== this.props.keyRouter) {
      this.fetchFileslinkForObject();
    }
  };

  handleUpload = onClose => filesMeta => {
    this.props.uploadAndLinkFiles(
      filesMeta.map(meta => ({
        ...meta,
        docType: this.props.typeEntity,
        docId: this.props.idEntity
      }))
    );
    onClose();
  };

  handleDownloadFile = entity => () => this.props.fetchFile(entity.id);

  renderUploadForm = (modalState, modalProps, blockProps, iconProps) => {
    return (
      <Block>
        <Heading title="Загрузка файла" icon="close" iconProps={iconProps} />
        <UploadModalContent onUpload={this.handleUpload(modalProps.onClose)} />
      </Block>
    );
  };

  getAddIconProps = memoizeOne(iconId => ({
    "data-tip": "tooltip",
    "data-for": iconId,
    "data-effect": "solid"
  }));

  renderHeadOptionControl = () => {
    const iconId = `documents-${this.props.idEntity}`;
    const iconProps = this.getAddIconProps(iconId);

    return (
      <TableCell control>
        <ModalIconControls
          renderContent={this.renderUploadForm}
          iconProps={iconProps}
        >
          {renderIconBlock("add")}
        </ModalIconControls>
        <ReactTooltip id={iconId}>Загрузить файл</ReactTooltip>
      </TableCell>
    );
  };

  renderOptionControl = ({ entity }) => {
    const iconId = `documents-${entity.id}`;
    const iconProps = {
      "data-tip": "tooltip",
      "data-for": iconId,
      "data-effect": "solid"
    };

    return (
      <TableCell control onClick={this.handleDownloadFile(entity)}>
        <IconControl icon={"cloud_download"} {...iconProps} />
        <ReactTooltip id={iconId}>Скачать файл</ReactTooltip>
      </TableCell>
    );
  };

  renderCells = memoizeOne((enumerations, requestParams) => {
    const getClientsFromSearch = () => this.props.clientsFromSearch.entities;

    return {
      cells: fields.reduce(
        reduceValuesToFormFields(
          {
            ...requestParams,
            getClientsFromSearch
          },
          enumerations
        ),
        []
      )
    };
  });

  render() {
    const { cells } = this.renderCells(
      this.props.enumerations,
      this.props.requestParams
    );

    return (
      <BlockContent table>
        {this.props.isLoading ? (
          <PreLoader />
        ) : (
          <Table
            cells={cells}
            enumerations={this.props.enumerations}
            entities={this.props.fileslinks}
            hasChildrenAfter
            renderRowHeadChildrenAfter={this.renderHeadOptionControl}
            renderRowBodyChildrenAfter={this.renderOptionControl}
          />
        )}
      </BlockContent>
    );
  }
}

const moduleNames = [moduleNameEnumerations, moduleNameFileslink];

export default connect(
  (state, props) => ({
    fileslinks: fileslinkForObjectSelector(state, props).entities,
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    enumerations: enumerationsSelector(state, { enumerationNames }),
    keyRouter: keyRouterSelector(state),
    clientsFromSearch: clientsForEntitySelector(state, {
      idEntity: props.idEntity,
      typeEntity: "agents"
    })
  }),
  {
    fetchFileslinkForObject,
    fetchEnumerations,
    uploadAndLinkFiles,
    fetchFile
  }
)(Documents);
