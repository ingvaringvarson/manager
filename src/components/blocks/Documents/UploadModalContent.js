import React, { Component } from "react";
import PropTypes from "prop-types";

import BlockContent from "../../../designSystem/organisms/BlockContent";

import { generateId } from "./../../../utils";
import OneFileUpload from "./OneFileUpload";
import UploadTable from "./UploadTable";

export default class UploadModalContent extends Component {
  static propTypes = { onUpload: PropTypes.func };
  state = { files: [], filesMeta: [] };

  handleUpload = e => {
    this.props.onUpload(this.state.filesMeta);
    e.preventDefault();
  };

  getNewState = newFile => {
    const removeExt = s => s.replace(/\.[^/.]+$/, "");

    const newFileMeta = {
      id: generateId(),
      file: newFile,
      name: removeExt(newFile.name),
      isMain: false
    };

    return {
      files: [...this.state.files, newFile],
      filesMeta: [...this.state.filesMeta, newFileMeta]
    };
  };

  handeLoadButtonClick = e => {
    const newFile = e.target.files[0];

    if (!newFile) {
      return;
    }

    const alreadyUploaded = f => f.name === newFile.name;

    if (this.state.files.some(alreadyUploaded)) {
      return;
    }

    this.setState(this.getNewState(newFile));
  };

  handleOnIsMainSelectChange = id => e => {
    const patchById = f => (f.id === id ? { ...f, isMain: !!+e[0] } : f);

    const filesMeta = this.state.filesMeta.map(patchById);
    this.setState({ filesMeta });
  };

  render() {
    return (
      <BlockContent>
        {this.state.filesMeta.length ? (
          <UploadTable
            filesMeta={this.state.filesMeta}
            handleOnIsMainSelectChange={this.handleOnIsMainSelectChange}
            handeLoadButtonClick={this.handeLoadButtonClick}
            handleUpload={this.handleUpload}
          />
        ) : (
          <OneFileUpload
            handeLoadButtonClick={this.handeLoadButtonClick}
            handleUpload={this.handleUpload}
          />
        )}
      </BlockContent>
    );
  }
}
