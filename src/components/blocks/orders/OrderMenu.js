import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import CommandsByStatusMenu from "../../common/menus/CommandsByStatusMenu";
import PrepareToDelivery from "./OrderCommands/PrepareToDelivery";
import CreatePaymentForm from "../../forms/CreatePaymentForm";
import PositionsReject from "../positions/RejectModal";
import ClientChange from "./OrderCommands/ClientChange";
import UploadMaxoptra from "./OrderCommands/UploadMaxoptra";
import ExtendReserve from "./OrderCommands/ExtendReserve";
import OrderDiscount from "./OrderCommands/OrderDiscount";
import CreateReturn from "./OrderCommands/CreateReturn";
import CreateBillForPrepayment from "./OrderCommands/CreateBillForPrepayment";
import BillCreation from "../bills/forms/BillCreation";
import DocumentPrint from "../DocumentPrint";
import Modal from "../helperBlocks/Modal";

import orderCommands, {
  filterOrdersCommands,
  getMainCommandByOrder
} from "../../../commands/orders";

import documentObjectTypeId from "../../../constants/documentObjectTypeId";
import { typeMethods } from "../../../ducks/positions";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import prepareToMoveModel from "./OrderCommands/PrepareToDelivery/model";

class OrderMenu extends PureComponent {
  static propTypes = {
    orders: PropTypes.arrayOf(PropTypes.object).isRequired,
    forList: PropTypes.bool,
    useMainCommand: PropTypes.bool,
    fetchEntities: PropTypes.func.isRequired,

    onConfirmClick: PropTypes.func
  };

  static defaultProps = {
    forList: false,
    useMainCommand: false,
    onConfirmClick: () => null
  };

  modalIsNotImplementedFunc = (_, command) =>
    alert(`Функциональность ${command.title} пока не добавлена`);

  getPositisionsOfOrders = memoizeOne(orders =>
    orders.reduce((res, order) => {
      if (!Array.isArray(order.positions)) return res;

      order.positions.forEach(p => res.push(p));
      return res;
    }, [])
  );

  commands = [
    {
      ...orderCommands.inWorkCommand,
      onClick: this.props.onConfirmClick
    },
    {
      ...orderCommands.createPaymentCommand,
      renderModal: (handleClose, [order]) => (
        <Modal
          hasWrap={false}
          width="auto"
          title="Принять оплату"
          subtitle={`Заказ № ${order.code}`}
          isOpen
          handleClose={handleClose}
        >
          <CreatePaymentForm
            order={order}
            afterSubmitCallback={this.props.fetchEntities}
          />
        </Modal>
      )
    },
    {
      ...orderCommands.prepareToDeliveryCommand,
      renderModal: (handleClose, [order]) => (
        <PrepareToDelivery handleClose={handleClose} order={order} />
      )
    },
    {
      ...orderCommands.deliverGoodCommand,
      onClick: () =>
        this.props.push(prepareToMoveModel.generateGroup_1_4_Type())
    },
    {
      ...orderCommands.createReturnCommand,
      renderModal: (handleClose, orders) => (
        <CreateReturn
          isOpen
          orders={orders}
          fetchEntities={this.props.fetchEntities}
          handleClose={handleClose}
        />
      )
    },
    {
      ...orderCommands.printDocumentsCommand,
      renderModal: (handleClose, [order]) => (
        <DocumentPrint
          idEntity={order.id}
          objectTypeId={documentObjectTypeId.orders}
          entityFirmId={order.firm_id}
          handleClose={handleClose}
        />
      )
    },
    {
      ...orderCommands.returnPrepaymentCommand,
      renderModal: (handleClose, [order]) => (
        <CreateBillForPrepayment
          handleClose={handleClose}
          positions={orderCommands.returnPrepaymentCommand.positionsFilter(
            order.positions
          )}
          order={order}
        />
      )
    },
    {
      ...orderCommands.cancellationCommand,
      onClick: this.modalIsNotImplementedFunc
    },
    {
      ...orderCommands.createBillCommand,
      renderModal: (handleClose, orders) => (
        <BillCreation
          isOpen
          selectedAll
          orders={orders}
          handleClose={handleClose}
        />
      )
    },
    {
      ...orderCommands.discountCommand,
      renderModal: (handleClose, [order]) => (
        <OrderDiscount
          order={order}
          handleClose={handleClose}
          addOrderPositions={this.props.addOrderPositions}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...orderCommands.extendReserveCommand,
      renderModal: (handleClose, [order]) => (
        <ExtendReserve
          order={order}
          handleClose={handleClose}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...orderCommands.rejectCommand,
      renderModal: (handleClose, orders) => (
        <PositionsReject
          entities={orders}
          handleClose={handleClose}
          positions={this.getPositisionsOfOrders(orders)}
          typeMethod={typeMethods.orders}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...orderCommands.changeClientCommand,
      renderModal: (handleClose, [order]) => (
        <ClientChange
          order={order}
          handleClose={handleClose}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...orderCommands.uploadMaxoptraCommand,
      renderModal: (handleClose, orders) => (
        <UploadMaxoptra orders={orders} handleClose={handleClose} />
      )
    }
  ];

  getMainCommand = memoizeOne(([order]) => getMainCommandByOrder(order));

  render() {
    const { orders, forList, useMainCommand } = this.props;
    const mainCommandId = useMainCommand ? this.getMainCommand(orders) : null;
    return (
      <CommandsByStatusMenu
        forList={forList}
        entityOrEntities={orders}
        commands={this.commands}
        mainCommandId={mainCommandId}
        commandsToMenuOptionsFilter={filterOrdersCommands}
      />
    );
  }
}

export default connect(
  null,
  { push }
)(OrderMenu);
