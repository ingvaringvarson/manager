import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";

import NestedTableForEntity from "../positions/NestedTableForEntity";
import OrderPositionsMenu from "./OrderPositionsMenu";

import {
  orderInfoWithBillIdSelector,
  orderChangedSelector
} from "../../../ducks/orders";

import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../ducks/enumerations";

import { fields } from "../../../fields/formFields/orders/fieldsForOrderPositions";

const enumerationNames = [
  "orderPositionGoodState",
  "orderPositionServiceState"
];

class OrderPositions extends Component {
  static propTypes = {
    order: PropTypes.object.isRequired,
    orderStateKey: PropTypes.string.isRequired,
    fetchEntities: PropTypes.func,
    fetchEnumerations: PropTypes.func.isRequired,
    enumerations: PropTypes.shape({
      orderPositionGoodState: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired
    }).isRequired
  };

  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
  }

  renderPositionsMenu = ({ forList, positions }) => (
    <OrderPositionsMenu
      forList={forList}
      positions={positions}
      order={this.props.order}
      fetchEntities={this.props.fetchEntities}
    />
  );

  render() {
    if (!this.props.order) {
      return null;
    }

    return (
      <NestedTableForEntity
        renderPositionsMenu={this.renderPositionsMenu}
        entity={this.props.order}
        positions={this.props.order.positions}
        enumerations={this.props.enumerations}
        tableParams={null}
        key={this.props.orderStateKey}
        fields={fields}
        isNested={false}
      />
    );
  }
}

function mapStateToProps(state, props) {
  return {
    enumerations: enumerationsSelector(state, { enumerationNames }),
    order: orderInfoWithBillIdSelector(state, props.match.params),
    orderStateKey: orderChangedSelector(state, props.match.params)
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    { fetchEnumerations }
  )
)(OrderPositions);
