import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import Row from "../../../designSystem/templates/Row";
import Column from "../../../designSystem/templates/Column";
import HeadingCard from "../../common/headlines/HeadingCard";
import HeadingInfo from "../../common/headlines/HeadingInfo";
import OrderMenu from "../../blocks/orders/OrderMenu";
import { GreyText } from "../../common/styled/text";

import { digitsForNumbers } from "../../../utils";
import { renderOrderState } from "../../../fields/helpers";
import { memoizeToArray } from "../../../helpers/cache";

class HeaderOrderPage extends PureComponent {
  static propTypes = {
    order: PropTypes.object.isRequired,
    enumerations: PropTypes.object.isRequired,
    onConfirmClick: PropTypes.func.isRequired,
    fetchEntities: PropTypes.func.isRequired
  };

  renderHeaderInfo() {
    const { order } = this.props;
    return (
      <Row>
        <Column mr="30px">
          <HeadingInfo
            subTitle="Сумма всего"
            info={`${digitsForNumbers(order.sum_total)} руб.`}
          />
        </Column>
        <Column mr="30px">
          <HeadingInfo
            subTitle="Сумма предоплатных позиций"
            info={
              order.sum_prepaid === null
                ? "0 руб."
                : `${digitsForNumbers(order.sum_prepaid)} руб.`
            }
          />
        </Column>
        <Column mr="30px">
          <HeadingInfo
            subTitle="Сумма к оплате"
            info={
              <GreyText>{digitsForNumbers(order.sum_payment)} руб.</GreyText>
            }
          />
        </Column>
      </Row>
    );
  }

  render() {
    if (!this.props.order) {
      return null;
    }

    const { order, enumerations, onConfirmClick, fetchEntities } = this.props;

    const status = renderOrderState("orderState", "state")({
      entity: order,
      enumerations
    });

    return (
      <Row alignCenter justifyBetween mb="20px">
        <Column>
          <HeadingCard
            title="Карточка заказа"
            name={`id ${order.code}`}
            status={status}
          />
        </Column>
        <Column>{this.renderHeaderInfo()}</Column>
        <Column>
          <OrderMenu
            useMainCommand
            orders={memoizeToArray(order)}
            onConfirmClick={onConfirmClick}
            fetchEntities={fetchEntities}
          />
        </Column>
      </Row>
    );
  }
}

export default HeaderOrderPage;
