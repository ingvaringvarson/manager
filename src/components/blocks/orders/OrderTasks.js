import React from "react";
import { requestParamsByOrderPageSelector } from "../../../ducks/tasks";
import TasksTableForEntity from "../tasks/TasksTableForEntity";
import { fields } from "../../../fields/formFields/tasks/orderTasksFields";

const typeEntity = "orders";

const OrderTasks = () => {
  return (
    <TasksTableForEntity
      fields={fields}
      typeEntity={typeEntity}
      requestParamsSelector={requestParamsByOrderPageSelector}
    />
  );
};

export default OrderTasks;
