import { generateUrlWithQuery } from "../../../../../utils";
import { getUrl } from "../../../../../helpers/urls";
import { groupTaskType } from "../../../../../fields/formFields/tasks/fieldsForWarehousePage";

const typeToUrl = "warehouse";

const model = {
  group_1_4: [1, 4],
  group_8_9: [8, 9],
  group_2_3_5: [2, 3, 5],
  availablesStates: [1, 3, 6, 7],
  tasksStatesValidatorCreator(tasks = [], types = [], states = []) {
    if (tasks.length === 0) return false;

    return tasks.some(
      t =>
        types.some(type => type === t.type) &&
        states.some(state => state === t.state)
    );
  },
  isValid_1_4_group(tasks) {
    return this.tasksStatesValidatorCreator(
      tasks,
      this.group_1_4,
      this.availablesStates
    );
  },
  isValid_8_9_group(tasks) {
    return this.tasksStatesValidatorCreator(
      tasks,
      this.group_8_9,
      this.availablesStates
    );
  },
  isValid_2_3_5_group(tasks) {
    return this.tasksStatesValidatorCreator(
      tasks,
      this.group_2_3_5,
      this.availablesStates
    );
  },
  getTasksGroup_2_3_5_Type(tasks) {
    switch (true) {
      case tasks.every(t => t.state === 5) ||
        tasks.some(t => t.type === 3 && t.state === 3):
        return groupTaskType["3_5"].id;
      case tasks.every(t => t.state === 2):
      default:
        return groupTaskType["2_3"].id;
    }
  },
  generateGroup_1_4_Type() {
    return generateUrlWithQuery(
      {
        pathname: getUrl(typeToUrl),
        newQuery: { _type_group: groupTaskType["1_4"].id }
      },
      true
    );
  },
  generateGroup_8_9_Type() {
    return generateUrlWithQuery(
      {
        pathname: getUrl(typeToUrl),
        newQuery: { _type_group: groupTaskType["8_9"].id }
      },
      true
    );
  },
  generateGroup_2_3_5_Type(tasks) {
    return generateUrlWithQuery(
      {
        pathname: getUrl(typeToUrl),
        newQuery: {
          _type_group: this.getTasksGroup_2_3_5_Type(tasks)
        }
      },
      true
    );
  }
};

export default model;
