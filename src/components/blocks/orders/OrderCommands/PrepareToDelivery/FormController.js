import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { push } from "connected-react-router";
import styled from "styled-components";
import { connect } from "react-redux";
import {
  fetchTasksForEntity,
  tasksForEntitySelector
} from "../../../../../ducks/tasks";
import Button from "../../../../../designSystem/atoms/Button";
import ReactModal from "../../../../common/modals/CommonModal";
import model from "./model";

const FlexWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;
const typeEntity = "prepareToDeliveryOrder";

class FormController extends PureComponent {
  static propTypes = {
    order: PropTypes.object.isRequired,
    tasks: PropTypes.array.isRequired,
    handleClose: PropTypes.func.isRequired,
    fetchTasksForEntity: PropTypes.func.isRequired,
    isOpen: PropTypes.bool
  };
  static defaultProps = {
    isOpen: true
  };

  contentStyles = {
    width: 650
  };

  stateRouter = {
    selectedFilterGroup: [
      {
        key: "order_code",
        value: this.props.order.code
      }
    ]
  };

  componentDidMount() {
    this.props.fetchTasksForEntity(typeEntity, this.props.order.id, {
      order_id: this.props.order.id
    });
  }

  handleGroup_1_4_Click = () =>
    this.props.push(model.generateGroup_1_4_Type(), this.stateRouter);
  handleGroup_8_9_Click = () =>
    this.props.push(model.generateGroup_8_9_Type(), this.stateRouter);
  handleGroup_2_3_5_Click = () =>
    this.props.push(
      model.generateGroup_2_3_5_Type(this.props.tasks),
      this.stateRouter
    );

  render() {
    const { tasks } = this.props;
    const isAvaiableTasks =
      tasks.length > 0 &&
      !(
        model.isValid_1_4_group(tasks) ||
        model.isValid_8_9_group(tasks) ||
        model.isValid_2_3_5_group(tasks)
      );

    return (
      <ReactModal
        handleClose={this.props.handleClose}
        contentStyles={this.contentStyles}
        isOpen={this.props.isOpen}
        title="Выдать заказ"
      >
        {isAvaiableTasks && "Задач для выполнения нет"}
        <FlexWrapper>
          {model.isValid_1_4_group(tasks) && (
            <Button simple onClick={this.handleGroup_1_4_Click}>
              Выдать готовый товар
            </Button>
          )}
          {model.isValid_8_9_group(tasks) && (
            <Button onClick={this.handleGroup_8_9_Click}>
              Подготовить товар к доставке
            </Button>
          )}
          {model.isValid_2_3_5_group(tasks) && (
            <Button onClick={this.handleGroup_2_3_5_Click}>
              Подготовить перемещение
            </Button>
          )}
        </FlexWrapper>
      </ReactModal>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      tasks: tasksForEntitySelector(state, {
        idEntity: props.order.id,
        typeEntity
      }).entities
    };
  },
  { fetchTasksForEntity, push }
)(FormController);
