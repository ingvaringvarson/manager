import styled from "styled-components";

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  height: 36px;
  margin-top: 12px;
`;

export default ButtonWrapper;
