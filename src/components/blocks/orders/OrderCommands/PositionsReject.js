import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";

import Modal from "../../helperBlocks/Modal";
import MediatorFormAndTable from "../../../common/forms/MediatorFormAndTable";

import {
  addOrderPositions,
  mapOrdersPositions
} from "../../../../ducks/orders";

import { reduceValuesToFormFields } from "../../../../fields/helpers";

import {
  fields,
  formFields
} from "../../../../fields/formFields/orders/fieldsForPositionsUpdate";
import { generateId } from "../../../../utils";

// ToDo: для возвратов используется неверно.
// Видимо, сделать аналогично DividingModal.
class PositionsReject extends PureComponent {
  static propTypes = {
    handleClose: PropTypes.func,
    order: PropTypes.oneOfType([
      PropTypes.shape({
        type: PropTypes.number
      }),
      PropTypes.arrayOf(
        PropTypes.shape({
          type: PropTypes.number
        })
      )
    ]),
    addOrderPositions: PropTypes.func.isRequired,
    removedIds: PropTypes.array
  };

  static defaultProps = {
    removedIds: []
  };

  handleFormSubmit = (values, _, removedIds) => {
    this.props.addOrderPositions(
      this.filterValues(values, removedIds),
      {
        orders: Array.isArray(this.props.order)
          ? this.props.order
          : [this.props.order]
      },
      mapOrdersPositions
    );
  };

  filterValues(values, removedIds) {
    return Object.keys(values).reduce((vls, key) => {
      const isRemove = removedIds.some(id => key.includes(id));

      if (!isRemove) {
        vls[key] = values[key];
      }

      return vls;
    }, {});
  }

  renderCells = memoizeOne((values, enumerations, dataFields) => {
    return {
      cells: dataFields.reduce(
        reduceValuesToFormFields(values, enumerations),
        []
      ),
      key: generateId()
    };
  });

  mapPositions = memoizeOne(order => {
    let positions = [];
    if (Array.isArray(order)) {
      positions = order.reduce((result, currentOrder) => {
        if (!currentOrder.positions) {
          return result;
        }

        return result.concat(currentOrder.positions);
      }, []);
    } else if (order.positions) {
      positions = order.positions;
    }

    return {
      fields: positions.map(pos => ({ ...pos, reject: pos.count }))
    };
  });

  render() {
    const { order, enumerations, handleClose, removedIds } = this.props;

    if (!order) {
      return null;
    }

    const positions = this.mapPositions(order);

    const { cells } = this.renderCells(positions, enumerations, formFields);

    return (
      <Modal
        title="Отказаться от позиции"
        width="800px"
        handleClose={handleClose}
        isOpen
      >
        <MediatorFormAndTable
          entities={positions.fields}
          enumerations={enumerations}
          buttonTitle="Отказаться"
          fieldsForContainer={cells}
          fieldsForTable={fields}
          handleFormSubmit={this.handleFormSubmit}
          isRenderDefault={false}
          removedIds={removedIds}
        />
      </Modal>
    );
  }
}

export default connect(
  null,
  { addOrderPositions }
)(PositionsReject);
