import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";

import { ContainerContext } from "../../../common/formControls";
import ButtonPrimary from "../../../../designSystem/atoms/Button";
import ButtonWrapper from "./ButtonWrapper";
import Modal from "../../helperBlocks/Modal";

import {
  updateOrderPositions,
  normalizeOrdersPositionsForExtendReserve
} from "../../../../ducks/orders";

import { reduceValuesToFormFields } from "../../../../fields/helpers";

import { fields } from "../../../../fields/formFields/orders/fieldsForOrdersExtendReserve";
import FieldForm from "../../../common/form/FieldForm";
import { generateId } from "./../../../../utils";
import { pm_orders_update_reserve_time } from "../../../../constants/permissions";
import { hasPermissionSelector } from "../../../../ducks/user";

class ExtendReserve extends PureComponent {
  static propTypes = {
    order: PropTypes.object.isRequired,
    handleClose: PropTypes.func.isRequired,
    fetchEntities: PropTypes.func.isRequired,

    updateOrderPositions: PropTypes.func.isRequired,
    updateReserveTimeAvailable: PropTypes.bool.isRequired
  };

  handleFormSubmit = values => {
    const {
      updateOrderPositions,
      order,
      fetchEntities,
      handleClose
    } = this.props;

    updateOrderPositions(
      values,
      {
        order,
        fetchEntities
      },
      normalizeOrdersPositionsForExtendReserve
    );

    handleClose();
  };

  getCurrentMaxReserveDate(positions) {
    return positions.reduce((maxDate, p) => {
      const date = new Date(p.reserve_time);
      return date > maxDate ? date : maxDate;
    }, new Date("0001-01-01T00:00:00"));
  }

  getWithAddedDays(currentMaxReserveDate, addingDays) {
    const maxReserveDate = new Date(+currentMaxReserveDate);
    maxReserveDate.setDate(maxReserveDate.getDate() + addingDays);
    return maxReserveDate;
  }

  getFormCells = memoizeOne(positions => {
    const { updateReserveTimeAvailable } = this.props;
    const currentMaxReserveDate = this.getCurrentMaxReserveDate(positions);
    const maxReserveDate = updateReserveTimeAvailable
      ? new Date("9999-12-31T00:00:00")
      : this.getWithAddedDays(currentMaxReserveDate, 7);

    return {
      minReserveDate: currentMaxReserveDate,
      maxReserveDate,
      current_reserve_time: currentMaxReserveDate,
      reserve_time__: currentMaxReserveDate
    };
  });

  renderCells = memoizeOne(formCellsValues => ({
    cells: fields.reduce(reduceValuesToFormFields(formCellsValues), []),
    key: generateId()
  }));

  renderField = (field, index) => (
    <FieldForm key={field.id} field={field} index={index} />
  );

  renderForm = formState => {
    const isFormValid = !Object.keys(formState.invalidFields).length;
    return (
      <>
        {fields.map(this.renderField)}
        <ButtonWrapper>
          <ButtonPrimary disabled={!isFormValid} type="submit">
            Изменить
          </ButtonPrimary>
        </ButtonWrapper>
      </>
    );
  };

  render() {
    const { positions } = this.props.order;
    const formCellsValues = this.getFormCells(positions);
    const { key, cells } = this.renderCells(formCellsValues);

    return (
      <Modal
        isOpen
        title="Редактирование"
        subtitle="Дата резервирования"
        handleClose={this.props.handleClose}
        width="460px"
      >
        <ContainerContext
          key={key}
          fields={cells}
          onSubmitForm={this.handleFormSubmit}
          submitValidation
          renderForm={this.renderForm}
        />
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    updateReserveTimeAvailable: hasPermissionSelector(state, {
      name: pm_orders_update_reserve_time
    })
  };
}

export default connect(
  mapStateToProps,
  { updateOrderPositions }
)(ExtendReserve);
