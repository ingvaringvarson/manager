import { connect } from "react-redux";
import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import ContainerContext from "../../../common/formControls";
import Button from "../../../../designSystem/atoms/Button";
import FieldForm from "../../../common/form/FieldForm";
import BtnBlock from "../../../common/styled/BtnBlock";
import ListItem from "../../../common/list/ListItem";
import Modal from "../../helperBlocks/Modal";

import { fetchMaxoptraFile } from "./../../../../ducks/maxoptra/actionCreators";

import { renderFormFields } from "./../../../../fields/helpers/mapMethods";

const cells = renderFormFields([
  {
    id: "note",
    formControlProps: {
      render: () => (
        <ListItem textMode active mb="20px" mt="10px">
          Выгрузить выбранные заказы?
        </ListItem>
      )
    }
  }
]);

class UploadMaxoptra extends PureComponent {
  static propTypes = {
    handleClose: PropTypes.func,
    orders: PropTypes.arrayOf(PropTypes.object),

    fetchMaxoptraFile: PropTypes.func.isRequired
  };

  handleFormSubmit = () => {
    const { orders, handleClose, fetchMaxoptraFile } = this.props;
    fetchMaxoptraFile(orders);
    handleClose();
  };

  render() {
    const { handleClose } = this.props;

    return (
      <Modal
        title="Выгрузка заказов на доставку"
        handleClose={handleClose}
        width={"400px"}
        isOpen
      >
        <ContainerContext
          fields={cells}
          onSubmitForm={this.handleFormSubmit}
          submitValidation
        >
          <FieldForm field={cells[0]} />
          <BtnBlock>
            <Button type="submit">Выгрузить</Button>
          </BtnBlock>
        </ContainerContext>
      </Modal>
    );
  }
}

export default connect(
  null,
  { fetchMaxoptraFile }
)(UploadMaxoptra);
