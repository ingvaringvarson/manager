import React, { PureComponent } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import styled from "styled-components";
import memoizeOne from "memoize-one";
import nanoid from "nanoid";

import ButtonPrimary from "../../../../designSystem/atoms/Button";
import ButtonWrapper from "./ButtonWrapper";
import Modal from "../../helperBlocks/Modal";

import {
  ContainerContext,
  FieldContext,
  RenderContext
} from "../../../common/formControls";

import { fields } from "../../../../fields/formFields/orders/fieldsForOrdersDiscount";

import { reduceValuesToFormFields } from "../../../../fields/helpers";

import {
  addOrderPositions,
  mapOrdersPositions
} from "./../../../../ducks/orders";

const OrderWrapper = styled.div`
  & > p {
    display: inline-block;
    width: 200px;
    vertical-align: top;
  }
  & > div {
    display: inline-block;
    width: 220px;
    vertical-align: top;
    & > div {
      display: inline-block;
      width: 100px;
      vertical-align: top;
      &:not(:first-child) {
        margin-left: 10px;
      }
    }
  }
`;

class OrderDiscount extends PureComponent {
  static propTypes = {
    handleClose: PropTypes.func,
    order: PropTypes.oneOfType([
      PropTypes.shape({
        type: PropTypes.number
      }),
      PropTypes.arrayOf(
        PropTypes.shape({
          type: PropTypes.number
        })
      )
    ]),

    addOrderPositions: PropTypes.func.isRequired,
    fetchEntities: PropTypes.func.isRequired
  };

  handleFormSubmit = values => {
    this.props.addOrderPositions(
      values,
      {
        orders: Array.isArray(this.props.order)
          ? this.props.order
          : [this.props.order],
        fetchEntities: this.props.fetchEntities
      },
      mapOrdersPositions
    );
    //this.props.handleClose();
  };

  renderCells = memoizeOne((values, enumerations, dataFields) => {
    return {
      cells: dataFields.reduce(
        reduceValuesToFormFields(values, enumerations),
        []
      ),
      key: nanoid()
    };
  });

  renderOrders = () => {
    const { order } = this.props;
    return Array.isArray(order) ? (
      order.map((ord, index) => {
        if (index === order.length - 1) {
          return (
            <Link to={`/orders/${ord.id}`}>
              {ord.code} (полная сумма: {ord.sum_total}).
            </Link>
          );
        }
        return (
          <Link to={`/orders/${ord.id}`}>
            {ord.code} (полная сумма: {ord.sum_total}),{" "}
          </Link>
        );
      })
    ) : (
      <Link to={`/orders/${order.id}`}>
        {order.code} (полная сумма: {order.sum_total}).
      </Link>
    );
  };

  render() {
    const { cells, key } = this.renderCells(
      this.props.order,
      this.props.enumerations,
      fields
    );

    return (
      <Modal
        title="Добавить скидку"
        handleClose={this.props.handleClose}
        isOpen
        width="460px"
      >
        <ContainerContext
          key={key}
          fields={cells}
          submitValidation
          onSubmitForm={this.handleFormSubmit}
        >
          <OrderWrapper>
            <p>Вы хотите добавить скидку на заказы {this.renderOrders()}</p>
            <div>
              {fields.map(field => (
                <FieldContext name={field.id} key={field.id} />
              ))}
            </div>
          </OrderWrapper>
          <RenderContext>
            {({ isFormValid }) => (
              <ButtonWrapper>
                <ButtonPrimary type="submit" disabled={!isFormValid}>
                  Добавить
                </ButtonPrimary>
              </ButtonWrapper>
            )}
          </RenderContext>
        </ContainerContext>
      </Modal>
    );
  }
}

export default connect(
  null,
  { addOrderPositions }
)(OrderDiscount);
