import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import { fetchClient, clientInfoSelector } from "../../../../ducks/clients";
import {
  fetchDiscountGroups,
  fetchDiscountGroupsForAccount,
  discountGroupListSelector,
  groupsForAccountSelector
} from "../../../../ducks/discountGroup";
import { changeClient } from "../../../../ducks/orders";

import ButtonPrimary from "../../../../designSystem/atoms/Button";
import Box from "../../../../designSystem/organisms/Box";
import Text from "../../../../designSystem/atoms/Text";
import RightStickedButton from "../../../common/buttons/RightStickedButton";
import {
  ContainerContext,
  FieldContext,
  RenderContext
} from "../../../common/formControls";
import Modal from "../../../common/modals/CommonModal";

import {
  fields as fieldForForm,
  discountGroupsValidator,
  newAgentContractsValidator
} from "../../../../fields/formFields/orders/clientChange";
import { reduceValuesToFormFields } from "../../../../fields/helpers";
import { generateId } from "../../../../utils";

class ClientChange extends PureComponent {
  static propTypes = {
    handleClose: PropTypes.func.isRequired,
    order: PropTypes.object.isRequired,
    fetchEntities: PropTypes.func.isRequired,
    enumerations: PropTypes.object,

    client: PropTypes.object,
    groupsForAccountSelector: PropTypes.object,
    discountGroups: PropTypes.array,
    changeClient: PropTypes.func,
    fetchDiscountGroups: PropTypes.func,
    fetchDiscountGroupsForAccount: PropTypes.func,
    fetchClient: PropTypes.func
  };

  state = {
    selectedClient: null,
    selectedClientOptions: []
  };

  componentDidMount = () => {
    const id = this.props.order.agent_id;
    this.props.fetchClient(id, { id });
    this.props.fetchDiscountGroups();
    this.props.fetchDiscountGroupsForAccount(id);
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (
      prevState.selectedClient !== this.state.selectedClient &&
      this.state.selectedClient
    ) {
      this.props.fetchDiscountGroupsForAccount(this.state.selectedClient.id);
    }
  };

  successCallback = () => {
    this.props.handleClose();
    this.props.fetchEntities();
  };

  renderCells = memoizeOne((prevClient, selectedClient) => {
    return {
      cells: fieldForForm.reduce(
        reduceValuesToFormFields({
          order: this.props.order,
          selectedClient,
          prevClient,
          selectedClientOptions: this.state.selectedClientOptions
        }),
        []
      ),
      key: generateId()
    };
  });

  handleValueChange = (formValues, fields) => {
    const { options } = fields["new_agent_id"].formControlProps;
    const selectedClient = this.getSelectedClient(
      options,
      formValues.new_agent_id
    );

    this.setState({ selectedClient, selectedClientOptions: options });
  };

  handleFormSubmit = values =>
    this.props.changeClient(values, {
      selectedClient: this.state.selectedClient,
      order: this.props.order,
      successCallback: this.successCallback
    });

  getSelectedClient(clients, selectedClientIds) {
    return Array.isArray(selectedClientIds) && selectedClientIds.length > 0
      ? (() => {
          const foundClient = clients.find(c => c.id === selectedClientIds[0]);
          return foundClient ? foundClient.entity : null;
        })()
      : null;
  }

  getClientDiscountGroup(client, discountGroups, groupsForAccountSelector) {
    return client && groupsForAccountSelector[client.id]
      ? discountGroups.find(
          g =>
            g.discountGroupId ===
            groupsForAccountSelector[client.id].discountGroupId
        )
      : null;
  }

  validateDiscountGroups = () => {
    const { client, discountGroups, groupsForAccountSelector } = this.props;

    return discountGroupsValidator(
      this.getClientDiscountGroup(
        client,
        discountGroups,
        groupsForAccountSelector
      ),
      this.getClientDiscountGroup(
        this.state.selectedClient,
        discountGroups,
        groupsForAccountSelector
      )
    );
  };

  render() {
    const {
      client,
      discountGroups,
      groupsForAccountSelector,
      order
    } = this.props;
    const { selectedClient } = this.state;

    if (!client) return null;

    const { cells, key } = this.renderCells(client, selectedClient);

    const selectedClientGroup = this.getClientDiscountGroup(
      selectedClient,
      discountGroups,
      groupsForAccountSelector
    );

    const validatedDiscountGroup = this.validateDiscountGroups();
    const selectedClientValidated = selectedClient
      ? newAgentContractsValidator(selectedClient, order)
      : null;

    return (
      <Modal
        title="Изменить клиента"
        handleClose={this.props.handleClose}
        isOpen
        width="460px"
      >
        <ContainerContext
          fields={cells}
          key={key}
          onValuesChange={this.handleValueChange}
          onSubmitForm={this.handleFormSubmit}
          submitValidation
        >
          <Box mb="16px">
            <Text>
              Текущий клиент:
              {client.code} |
              {client.person
                ? `${client.person.person_name} ${client.person.person_surname}`
                : !!client.company && client.company.company_name_short}
            </Text>
          </Box>
          {cells.map(f => (
            <Box mb="10px">
              <FieldContext name={f.id} key={f.id} />
            </Box>
          ))}
          {selectedClient && selectedClientGroup && validatedDiscountGroup ? (
            <Text color="#EB0028">{validatedDiscountGroup}</Text>
          ) : null}
          {selectedClientValidated ? (
            <Text color="#EB0028">{selectedClientValidated}</Text>
          ) : null}
          <RenderContext>
            {({ isFormValid }) => (
              <RightStickedButton>
                <ButtonPrimary
                  type="submit"
                  disabled={
                    !isFormValid ||
                    !!validatedDiscountGroup ||
                    !!selectedClientValidated
                  }
                >
                  Добавить
                </ButtonPrimary>
              </RightStickedButton>
            )}
          </RenderContext>
        </ContainerContext>
      </Modal>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      client: clientInfoSelector(state, { id: props.order.agent_id }),
      discountGroups: discountGroupListSelector(state),
      groupsForAccountSelector: groupsForAccountSelector(state)
    };
  },
  {
    fetchClient,
    fetchDiscountGroups,
    fetchDiscountGroupsForAccount,
    changeClient
  }
)(ClientChange);
