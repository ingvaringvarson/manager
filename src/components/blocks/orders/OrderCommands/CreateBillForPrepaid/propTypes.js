import PropTypes from "prop-types";

export const formPropTypes = {
  agent: PropTypes.shape({
    contracts: PropTypes.array.isRequired
  }),
  enumerations: PropTypes.object,
  bill: PropTypes.object
};

export const modalPropTypes = {
  ...formPropTypes,
  handleClose: PropTypes.func.isRequired
};
