import React, { PureComponent } from "react";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../../../ducks/enumerations";
import {
  createBillByForm,
  mapCreationForReturnPrepaidValues
} from "../../../../../ducks/bills";
import { fieldsForForm } from "../../../../../fields/formFields/bills/createBillForReturnPrepaid";
import {
  ContainerContext,
  FormContext,
  FieldContext,
  RenderContext
} from "../../../../common/formControls";
import RightStickedButton from "../../../../common/buttons/RightStickedButton";
import { reduceValuesToFormFields } from "../../../../../fields/helpers";
import { generateId } from "../../../../../utils";

import { formPropTypes } from "./propTypes";

class PrepaidExpenseContentForm extends PureComponent {
  static propTypes = formPropTypes;
  static enumerationNames = ["contractType", "contractState"];

  componentDidMount() {
    this.props.fetchEnumerations(PrepaidExpenseContentForm.enumerationNames);
  }

  handleFormSubmit = formValues => {
    this.props.createBillByForm(
      formValues,
      {
        agent: this.props.agent,
        shouldMoveToEntityPage: true,
        bill: this.props.bill
      },
      mapCreationForReturnPrepaidValues
    );
  };

  renderFormFields = memoizeOne((agent, enumerations) => {
    return {
      fieldsForForm: fieldsForForm.reduce(
        reduceValuesToFormFields({ agent }, enumerations),
        []
      ),
      key: generateId()
    };
  });

  renderButton = ({ isFormValid }) => {
    return (
      <Button type="submit" disabled={!isFormValid}>
        Создать счет
      </Button>
    );
  };

  render() {
    const { fieldsForForm, key } = this.renderFormFields(
      this.props.agent,
      this.props.enumerations
    );
    return (
      <ContainerContext
        fields={fieldsForForm}
        onSubmitForm={this.handleFormSubmit}
        key={key}
      >
        <FormContext>
          {fieldsForForm.map(f => (
            <FieldContext name={f.id} key={f.id} />
          ))}
        </FormContext>
        <RightStickedButton hasOwnButton>
          <RenderContext>{this.renderButton}</RenderContext>
        </RightStickedButton>
      </ContainerContext>
    );
  }
}

export default connect(
  state => {
    return {
      enumerations: enumerationsSelector(state, {
        enumerationNames: PrepaidExpenseContentForm.enumerationNames
      })
    };
  },
  {
    fetchEnumerations,
    createBillByForm
  }
)(PrepaidExpenseContentForm);
