import React, { memo } from "react";
import CommonModal from "../../../../common/modals/CommonModal";
import PrepaidExpenseContentForm from "./PrepaidExpenseContentForm";
import { modalPropTypes } from "./propTypes";

const contentStyles = {
  content: {
    width: "480px"
  }
};

const PrepaidModal = memo(props => {
  return (
    <CommonModal
      isOpen
      handleClose={props.handleClose}
      title="Создать счет для возврата аванса"
      contentStyles={contentStyles}
    >
      <PrepaidExpenseContentForm {...props} />
    </CommonModal>
  );
});

PrepaidModal.propTypes = modalPropTypes;

export default PrepaidModal;
