import React, { memo } from "react";
import PropTypes from "prop-types";
import CommonModal from "../../../../common/modals/CommonModal";
import PrepaymentContentForm from "./PrepaymentContentForm";

const contentStyles = {
  width: "980px"
};

const PrepaymentModal = memo(props => {
  return (
    <CommonModal
      isOpen
      handleClose={props.handleClose}
      title='Создать счет для возврата предоплаты"'
      contentStyles={contentStyles}
    >
      <PrepaymentContentForm {...props} />
    </CommonModal>
  );
});

PrepaymentModal.propTypes = {
  handleClose: PropTypes.func.isRequired
};

export default PrepaymentModal;
