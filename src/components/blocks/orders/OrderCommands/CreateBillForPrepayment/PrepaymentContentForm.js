import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  createBillByForm,
  mapCreationForReturnPrepaymentValues
} from "../../../../../ducks/bills";
import MediatorFormAndTable from "../../../../common/forms/MediatorFormAndTable";
import {
  fieldsForForm,
  fieldsForTable
} from "../../../../../fields/formFields/orders/fieldsForCreateBillForPrepayment";
import { reduceValuesToFormFields } from "../../../../../fields/helpers";
import memoizeOne from "memoize-one";

class PrepaymentContentForm extends PureComponent {
  static propTypes = {
    positions: PropTypes.array.isRequired,
    order: PropTypes.object.isRequired
  };

  handleFormSubmit = (params, entities) => {
    this.props.createBillByForm(
      params,
      {
        order: this.props.order,
        positions: entities,
        shouldMoveToEntityPage: true
      },
      mapCreationForReturnPrepaymentValues
    );
  };

  renderFormFields = memoizeOne(enumerations => {
    return {
      fieldsForForm,
      fieldsForTable: fieldsForTable.reduce(
        reduceValuesToFormFields(null, enumerations),
        []
      )
    };
  });

  render() {
    const { fieldsForForm, fieldsForTable } = this.renderFormFields(
      this.props.enumerations,
      this.props.order
    );

    return (
      <MediatorFormAndTable
        buttonTitle="Создать счет"
        fieldsForTable={fieldsForTable}
        fieldsForForm={fieldsForForm}
        fieldsForContainer={fieldsForForm}
        handleFormSubmit={this.handleFormSubmit}
        entities={this.props.positions}
      />
    );
  }
}

export default connect(
  null,
  {
    createBillByForm
  }
)(PrepaymentContentForm);
