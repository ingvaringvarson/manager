import memoizeOne from "memoize-one";
import {
  fieldsForTable,
  fieldsForForm
} from "../../../../../fields/formFields/orders/fieldsForReturnCreate";
import { generateId } from "../../../../../utils";
import { reduceValuesToFormFields } from "../../../../../fields/helpers";

export const OrderModel = {
  renderError: memoizeOne(orders => {
    switch (true) {
      case orders.some(order => order.state !== 7):
        return {
          message:
            "Выбранные вами заказы не выданы клиенту. Создать возврат нельзя"
        };
      case orders.some(order => order.firm_id !== orders[0].firm_id):
        return {
          message:
            "Выбранные вами заказы оформлены на разные организации. Создать единый возврат нельзя"
        };
      case orders.some(order => order.agent_id !== orders[0].agent_id):
        return {
          message:
            "Выбранные вами заказы оформлены на разных клиентов. Создать единый возврат нельзя"
        };
      case orders.some(order => order.contract_id !== orders[0].contract_id):
        return {
          message:
            "Выбранные вами заказы оформлены на разные договоры. Создать единый возврат нельзя"
        };
      default:
        return null;
    }
  }),
  renderCells: memoizeOne((order, enumerations) => {
    const positions = Array.isArray(order)
      ? order.reduce((r, o) => r.concat(o.positions), [])
      : order.positions;

    return {
      cells: fieldsForTable.reduce(
        reduceValuesToFormFields({ fields: positions }, enumerations),
        []
      ),
      fields: fieldsForForm.reduce(
        reduceValuesToFormFields({ fields: positions }, enumerations),
        []
      ),
      key: generateId(),
      positions
    };
  }),
  checkIfValid(formState, removedIds = []) {
    if (!removedIds.length) return !Object.keys(formState.invalidFields).length;

    return !Object.keys(formState.invalidFields).reduce((res, curKey) => {
      if (removedIds.some(id => curKey.includes(id))) return res;
      return ++res;
    }, 0);
  }
};
