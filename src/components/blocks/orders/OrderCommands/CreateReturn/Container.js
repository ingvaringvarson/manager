import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { enumerationsSelector } from "../../../../../ducks/enumerations";
import {
  createReturn,
  normalizeReturnCreate
} from "../../../../../ducks/returns";
import { fetchClient, clientInfoSelector } from "../../../../../ducks/clients";
import Modal from "../../../helperBlocks/Modal";
import RemoveableTable from "../../../../common/table/Removeable";
import { TableContainer } from "../../../../common/tableControls";
import { ContainerContext, FormContext } from "../../../../common/formControls";

import ButtonPrimary from "../../../../../designSystem/atoms/Button";
import ButtonWrapper from "../ButtonWrapper";
import { OrderModel } from "./model";

const enumerationNames = ["returnReason", "returnPurpose"];

class CreateReturn extends PureComponent {
  static propTypes = {
    orders: PropTypes.arrayOf(PropTypes.object).isRequired,
    handleClose: PropTypes.func.isRequired,

    enumerations: PropTypes.object.isRequired,
    firstOrderId: PropTypes.string.isRequired,
    firstAgentId: PropTypes.string,
    client: PropTypes.object.isRequired,
    createReturn: PropTypes.func.isRequired,
    fetchEntities: PropTypes.func.isRequired
  };

  state = {
    removedIds: [],
    isFormValid: true
  };

  componentDidMount = () => {
    if (this.props.firstAgentId) {
      this.props.fetchClient(this.props.firstAgentId, {
        id: this.props.firstAgentId
      });
    }
  };

  handleFormValuesChanged = (values, fields, formState) => {
    return this.setState({
      isFormValid: OrderModel.checkIfValid(formState, this.state.removedIds)
    });
  };

  handleRemovedChange = removedIds => this.setState({ removedIds });

  handleSuccessCallback() {
    this.props.fetchEntities();
    this.props.handleClose();
  }

  handleFormSubmit = (values, fields, formState) => {
    const isFormValid = OrderModel.checkIfValid(
      formState,
      this.state.removedIds
    );
    if (!isFormValid) {
      this.setState({ isFormValid });
    } else {
      const {
        orders,
        enumerations,
        client,
        firstOrderId,
        createReturn
      } = this.props;
      const { positions } = OrderModel.renderCells(orders, enumerations);

      createReturn(
        values,
        {
          removedIds: this.state.removedIds,
          orders,
          enumerations,
          positions,
          client,
          orderId: firstOrderId,
          successCallback: this.handleSuccessCallback.bind(this)
        },
        normalizeReturnCreate
      );

      this.setState({ isFormValid });
    }
  };

  render() {
    const { orders, enumerations } = this.props;
    const { cells, key, fields, positions } = OrderModel.renderCells(
      orders,
      enumerations
    );
    const err = OrderModel.renderError(orders);

    return (
      <Modal
        isOpen
        title="Создать возврат"
        handleClose={this.props.handleClose}
      >
        {err ? (
          err
        ) : (
          <FormContext>
            <ContainerContext
              fields={fields}
              key={key}
              onSubmitForm={this.handleFormSubmit}
              onValuesChange={this.handleFormValuesChanged}
            >
              <TableContainer>
                <RemoveableTable
                  entities={positions}
                  cells={cells}
                  removedIds={this.state.removedIds}
                  enumerations={enumerations}
                  handleRemovedChange={this.handleRemovedChange}
                />
              </TableContainer>
              <ButtonWrapper>
                <ButtonPrimary type="submit" disabled={!this.state.isFormValid}>
                  СОЗДАТЬ ВОЗВРАТ
                </ButtonPrimary>
              </ButtonWrapper>
            </ContainerContext>
          </FormContext>
        )}
      </Modal>
    );
  }
}

export default connect(
  (state, props) => {
    const firstAgentId = props.orders[0].agent_id;
    const firstOrderId = props.orders[0].id;
    return {
      enumerations: enumerationsSelector(state, { enumerationNames }),
      firstAgentId,
      firstOrderId,
      client: clientInfoSelector(state, { id: firstAgentId })
    };
  },
  { createReturn, fetchClient }
)(CreateReturn);
