import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";
import nanoid from "nanoid";

import ButtonPrimary from "../../../../designSystem/atoms/Button";
import { TableContainer } from "../../../common/tableControls";
import { ContainerContext, RenderContext } from "../../../common/formControls";
import { Table } from "../../../common/tableView";
import ButtonWrapper from "./ButtonWrapper";
import Modal from "../../helperBlocks/Modal";

import {
  addOrderPositions,
  mapOrdersPositions
} from "../../../../ducks/orders";
import { reduceValuesToFormFields } from "../../../../fields/helpers";
import {
  fields,
  formFields
} from "../../../../fields/formFields/orders/fieldsForPositionsDiscount";

class PositionsDiscount extends PureComponent {
  static propTypes = {
    handleClose: PropTypes.func,
    order: PropTypes.oneOfType([
      PropTypes.shape({
        type: PropTypes.number
      }),
      PropTypes.arrayOf(
        PropTypes.shape({
          type: PropTypes.number
        })
      )
    ]),
    addOrderPositions: PropTypes.func.isRequired,
    selectedIds: PropTypes.arrayOf(PropTypes.string)
  };

  handleFormSubmit = values => {
    this.props.addOrderPositions(
      values,
      {
        orders: Array.isArray(this.props.order)
          ? this.props.order
          : [this.props.order]
      },
      mapOrdersPositions
    );
  };

  renderCells = memoizeOne((values, dataFields) => {
    return {
      cells: dataFields.reduce(reduceValuesToFormFields(values), []),
      key: nanoid()
    };
  });

  renderPositions = memoizeOne((order, selectedIds) => {
    let positions;
    if (Array.isArray(order)) {
      positions = order.reduce((result, currentOrder) => {
        if (!currentOrder.positions) {
          return result;
        }

        result.push(
          currentOrder.positions.filter(pos => {
            return selectedIds.some(id => pos.id === id);
          })
        );

        return result;
      }, []);
    } else {
      positions = order.positions.filter(pos => {
        return selectedIds.some(id => pos.id === id);
      });
    }

    return {
      fields: positions.map(pos => ({ ...pos, reject: pos.count }))
    };
  });

  render() {
    const { order, selectedIds } = this.props;

    const positions = this.renderPositions(order, selectedIds);

    const { cells, key } = this.renderCells(positions, formFields);

    return (
      <Modal
        title="Добавить скидку"
        handleClose={this.props.handleClose}
        isOpen
      >
        <ContainerContext
          fields={cells}
          key={key}
          submitValidation
          onSubmitForm={this.handleFormSubmit}
        >
          <TableContainer>
            <Table cells={fields} entities={positions.fields} />
          </TableContainer>
          <RenderContext>
            {({ isFormValid }) => (
              <ButtonWrapper>
                <ButtonPrimary type="submit" disabled={!isFormValid}>
                  Добавить
                </ButtonPrimary>
              </ButtonWrapper>
            )}
          </RenderContext>
        </ContainerContext>
      </Modal>
    );
  }
}

export default connect(
  null,
  { addOrderPositions }
)(PositionsDiscount);
