import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import InfoBlock from "../../common/info/InfoBlock";
import clientInfoTypes from "../../../helpers/propTypes/clientInfo";
import { fieldGroups } from "../../../fields/formFields/orders/fieldsForInfoBlock";
import memoizeOne from "memoize-one";
import InfoModalControl from "../../common/info/InfoModalControl";
import { routesToMove } from "../../../helpers/routesGenerate";

class OrderInfoBlocks extends PureComponent {
  static propTypes = {
    ...clientInfoTypes,
    order: PropTypes.object.isRequired,
    onSubmitClientForm: PropTypes.func.isRequired,
    onSubmitOrderForm: PropTypes.func.isRequired
  };

  renderContent = info => (
    state = {},
    modalProps = null,
    blockProps = null,
    iconProps = null
  ) => (
    <InfoBlock
      iconIsShow={state.iconIsShow}
      info={info}
      blockProps={blockProps}
      iconProps={iconProps}
    />
  );

  getBlock = (entity, enumerations, block) => {
    const info = block.mapValuesToFields(block, enumerations, entity);
    return {
      ...info,
      renderContent: this.renderContent(info)
    };
  };

  statesByActiveForm = [1, 2, 3, 4, 5, 6, 12];

  getOrderBlock = memoizeOne((client, order, enumerations) => {
    const { deliverylaf24, deliveryDC, pickup, delivery_type, state } = order;
    let block = {};
    const disabled = !this.statesByActiveForm.includes(state);

    switch (order.delivery_type) {
      case 1:
        block = this.getBlock(
          { ...pickup, delivery_type, client, disabled },
          enumerations,
          fieldGroups["delivery_1"]
        );
        break;
      case 2:
        block = this.getBlock(
          { ...deliverylaf24, delivery_type, client, disabled },
          enumerations,
          fieldGroups["delivery_2"]
        );
        break;
      case 3:
        block = this.getBlock(
          { ...deliveryDC, delivery_type, client, disabled },
          enumerations,
          fieldGroups["delivery_3"]
        );
        break;
      default:
        return null;
    }

    return {
      ...block,
      btnProps: {
        disabled
      }
    };
  });

  getClientBlocks = memoizeOne((client, order, enumerations) => {
    const blocks = [];

    if (client.type === 1) {
      blocks.push(
        this.getBlock({ client, order }, enumerations, fieldGroups["client"])
      );
    } else if (client.type === 2) {
      blocks.push(
        this.getBlock({ client, order }, enumerations, fieldGroups["company"])
      );
    }

    return blocks.concat(
      this.getBlock(
        Array.isArray(client["contacts"])
          ? client["contacts"].filter(c => c.is_active)
          : [],
        enumerations,
        fieldGroups["contacts"]
      ),
      this.getBlock(
        Array.isArray(client["contact_persons"])
          ? client["contact_persons"].map(cp => ({
              ...cp,
              contacts: Array.isArray(cp.contacts)
                ? cp.contacts.filter(c => c.is_active)
                : []
            }))
          : [],
        enumerations,
        fieldGroups["contact_persons"]
      )
    );
  });

  getClientBlockProps = memoizeOne(onSubmitForm => ({
    onSubmitForm
  }));

  handleMoveToClientPage = () => {
    const { client } = this.props;
    if (client) {
      window.open(routesToMove.client(client), "_blank");
    }
  };

  iconProps = {
    onClick: this.handleMoveToClientPage
  };

  render() {
    const {
      order,
      client,
      enumerations,
      onSubmitClientForm,
      onSubmitOrderForm
    } = this.props;

    if (!order) {
      return null;
    }

    const orderBlock = order
      ? this.getOrderBlock(client, order, enumerations)
      : null;
    const clientBlocks = client
      ? this.getClientBlocks(client, order, enumerations)
      : null;

    if (!orderBlock && !clientBlocks) {
      return null;
    }

    const clientBlockProps = this.getClientBlockProps(onSubmitClientForm);

    return (
      <>
        {!!orderBlock && (
          <InfoModalControl
            formProps={orderBlock.formMain}
            onSubmitForm={onSubmitOrderForm}
            fields={orderBlock.formFields}
            btnProps={orderBlock.btnProps}
          >
            {orderBlock.renderContent}
          </InfoModalControl>
        )}
        {!!clientBlocks &&
          clientBlocks.map(info => {
            if (!info.formMain || (info.formMain && !info.formMain.type)) {
              return (
                <InfoBlock
                  iconProps={this.iconProps}
                  iconIsShow
                  blockProps={clientBlockProps}
                  info={info}
                  key={info.title}
                />
              );
            }

            const fields =
              info.formMain.type === "edit"
                ? info.formFields
                : info.fieldsForAdd;

            return (
              <InfoModalControl
                formProps={info.formMain}
                onSubmitForm={onSubmitClientForm}
                fields={fields}
                key={info.title}
              >
                {info.renderContent}
              </InfoModalControl>
            );
          })}
      </>
    );
  }
}

export default OrderInfoBlocks;
