import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import DividingModal from "../positions/DividingModal";
import PositionsDiscount from "./OrderCommands/PositionsDiscount";
import CreateReturn from "./OrderCommands/CreateReturn";
import BillCreation from "../bills/forms/BillCreation";
import PositionsReject from "../positions/RejectModal";
import CreateBillForPrepayment from "./OrderCommands/CreateBillForPrepayment";
import CommandsByStatusMenu from "../../common/menus/CommandsByStatusMenu";

import orderPositionsCommands, {
  filterOrderPositionsCommands
} from "../../../commands/orderPositions";

import { memoizeToArray } from "../../../helpers/cache";
import { typeMethods } from "../../../ducks/positions";

export default class OrderPositionsMenu extends PureComponent {
  static propTypes = {
    forList: PropTypes.bool,
    positions: PropTypes.array.isRequired,
    order: PropTypes.object.isRequired,
    fetchEntities: PropTypes.func
  };

  static defaultProps = {
    forList: false
  };

  getPositionsIds = memoizeOne(positions => positions.map(p => p.id));

  getOrderWithPositions = memoizeOne((order, positions) => ({
    ...order,
    positions
  }));

  getRemovedPositionsIds = memoizeOne((order, positions) => {
    if (!order.positions) {
      return null;
    }

    return order.positions.reduce((res, orderPos) => {
      if (!positions.some(pos => orderPos._id === pos._id)) {
        res.push(orderPos.id); // здесь уже именно orderPos.id
      }
      return res;
    }, []);
  });

  commands = [
    {
      ...orderPositionsCommands.divideCommand,
      renderModal: (handleClose, positions, order) => (
        <DividingModal
          typeMethod={typeMethods.orders}
          positions={positions}
          handleClose={handleClose}
          entity={order}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...orderPositionsCommands.discountCommand,
      renderModal: (handleClose, positions, order) => (
        <PositionsDiscount
          handleClose={handleClose}
          order={order}
          selectedIds={this.getPositionsIds(positions)}
        />
      )
    },
    {
      ...orderPositionsCommands.createBillCommand,
      renderModal: (handleClose, positions, order) => (
        <BillCreation
          isOpen
          handleClose={handleClose}
          orders={memoizeToArray(order)}
          positionsIds={this.getPositionsIds(positions)}
        />
      )
    },
    {
      ...orderPositionsCommands.rejectByProductCommand,
      renderModal: (handleClose, positions, order) => (
        <PositionsReject
          handleClose={handleClose}
          entities={memoizeToArray(order)}
          positions={positions}
          typeMethod={typeMethods.orders}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...orderPositionsCommands.createReturnCommand,
      renderModal: (handleClose, positions, order) => (
        <CreateReturn
          order={this.getOrderWithPositions(order, positions)}
          handleClose={handleClose}
        />
      )
    },
    {
      ...orderPositionsCommands.returnPrepaymentCommand,
      customFilter: () =>
        this.props.order.sum_payment < this.props.order.sum_total,
      renderModal: (handleClose, positions, order) => (
        <CreateBillForPrepayment
          order={order}
          positions={positions}
          handleClose={handleClose}
        />
      )
    }
  ];

  render() {
    const { positions, order, forList } = this.props;
    return (
      <CommandsByStatusMenu
        forList={forList}
        entityOrEntities={positions}
        parentEntity={order}
        commands={this.commands}
        commandsToMenuOptionsFilter={filterOrderPositionsCommands}
      />
    );
  }
}
