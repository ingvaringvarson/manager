import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { orderIdForRouteSelector } from "../../../ducks/orders";
import {
  requestParamsForOrderPageSelector,
  billsForEntitySelector,
  fetchBillsForEntity
} from "../../../ducks/bills";
import {
  keyRouterSelector,
  pathnameRouterSelector,
  queryRouterSelector
} from "../../../redux/selectors/router";
import Table from "../../common/tableView/Table";
import TableWithLinks from "../../common/table/TableWithLinks";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../ducks/enumerations";
import { fields } from "../../../fields/formFields/orders/fieldsForOrderBills";
import { push } from "connected-react-router";
import { generateUrlWithQuery } from "../../../utils";
import { ContainerContext } from "../../common/formControls";
import {
  handleStopLifting,
  renderRowFormControlChildrenAfter,
  renderRowHeadChildrenAfter,
  TableContainer,
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  ToggleOpenRow
} from "../../common/tableControls";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../../fields/helpers";
import memoizeOne from "memoize-one";
import nanoid from "nanoid";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";
import TableCell from "../../../designSystem/molecules/TableCell";
import BillMenu from "../bills/BillMenu";
import { fieldsForPositions } from "../../../fields/formFields/clients/fieldsForClientBills";
import Container from "../../../designSystem/templates/Container";
import Row from "../../../designSystem/templates/Row";
import Column from "../../../designSystem/templates/Column";
import PaymentsForBill from "../payments/PaymentsForBill";

const subTableParams = {
  subTable: true,
  ml: "80px",
  mt: "8px",
  mb: "16px"
};

const enumerationNames = ["billState", "billType", "productType"];
const typeEntity = "orders";

class OrderBills extends Component {
  static propTypes = {
    enumerations: PropTypes.shape({
      billState: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired,
      billType: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired,
      productType: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired
    }).isRequired,
    bills: PropTypes.shape({
      entities: PropTypes.array.isRequired
    }).isRequired,
    idEntity: PropTypes.string.isRequired,
    pathname: PropTypes.string.isRequired,
    requestParams: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    fetchBillsForEntity: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    keyRouter: PropTypes.number.isRequired
  };

  componentDidMount() {
    this.props.fetchBillsForEntity(
      typeEntity,
      this.props.idEntity,
      this.props.requestParams
    );
    this.props.fetchEnumerations(enumerationNames);
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.props.fetchBillsForEntity(
        typeEntity,
        this.props.idEntity,
        this.props.requestParams
      );
    }
  }

  renderCells = memoizeOne(enumerations => {
    const { requestParams } = this.props;

    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: nanoid()
    };
  });

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleChangeTab = id => {
    const { pathname, query } = this.props;
    const queryForRootTab = `?_tab=${query._tab || 2}`;
    const queryForTab = id === 1 ? "" : `&_tab_state_bill=${id}`;
    this.props.push(`${pathname}${queryForRootTab}${queryForTab}`);
  };

  renderOptionControl = ({ entity }) => {
    return (
      <TableCell control onClick={handleStopLifting}>
        <BillMenu bills={[entity]} client={this.props.client} key={entity.id} />
      </TableCell>
    );
  };

  renderNestedTable = ({ entity }) => {
    const { enumerations } = this.props;
    return (
      <ToggleOpenRow id={entity._id}>
        <Container>
          <Row>
            {entity.positions && !!entity.positions.length && (
              <Column basis="50%">
                <TableContainer
                  canBeSelected
                  key={entity.id}
                  entities={entity.positions}
                >
                  <Table
                    tableProps={subTableParams}
                    enumerations={enumerations}
                    entities={entity.positions}
                    cells={fieldsForPositions}
                    hasChildrenBefore
                    renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
                    renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
                  />
                </TableContainer>
              </Column>
            )}
            <Column basis="50%">
              <PaymentsForBill payments={entity.payments_inf} />
            </Column>
          </Row>
        </Container>
      </ToggleOpenRow>
    );
  };

  renderTable = renderCallBacks => {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.bills
    );

    return (
      <TableWithLinks
        key={key}
        cells={cells}
        entities={this.props.bills.entities}
        enumerations={this.props.enumerations}
        hasFormControl
        hasChildrenBefore
        hasChildrenAfter
        renderRowHeadChildrenBefore={
          renderCallBacks.renderRowHeadChildrenBefore
        }
        renderRowBodyChildrenBefore={
          renderCallBacks.renderRowBodyChildrenBefore
        }
        renderRowFormControlChildrenBefore={
          renderCallBacks.renderRowFormControlChildrenBefore
        }
        renderRowBodyChildrenAfter={this.renderOptionControl}
        renderRowHeadChildrenAfter={renderRowHeadChildrenAfter}
        renderRowFormControlChildrenAfter={renderRowFormControlChildrenAfter}
        renderRowBodyAfter={this.renderNestedTable}
        typeEntity="bill"
      />
    );
  };

  render() {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.bills
    );

    return (
      <TabContainer
        selectedTabId={this.props.selectedTab}
        onChangeSelected={this.handleChangeTab}
      >
        <TabHeadContainer>
          <TabHead id={1}>Активные</TabHead>
          <TabHead id={2}>Аннулированные</TabHead>
        </TabHeadContainer>
        <TabBodyContainer>
          <TabBody id={1}>
            <ContainerContext
              key={key}
              onSort={this.handleSort}
              fields={cells}
              onSubmitForm={this.handleSubmitForm}
              autoSubmit
            >
              <TableContainer
                canBeSelected
                renderTable={this.renderTable}
                entities={this.props.bills.entities}
                hasNestedTable
              />
            </ContainerContext>
          </TabBody>
          <TabBody id={2}>
            <ContainerContext
              key={key}
              onSort={this.handleSort}
              fields={cells}
              onSubmitForm={this.handleSubmitForm}
              autoSubmit
            >
              <TableContainer
                canBeSelected
                renderTable={this.renderTable}
                entities={this.props.bills.entities}
                hasNestedTable
              />
            </ContainerContext>
          </TabBody>
        </TabBodyContainer>
      </TabContainer>
    );
  }
}

function mapStateToProps(state, props) {
  const query = queryRouterSelector(state, props);
  const selectedTab = query.hasOwnProperty("_tab_state_bill")
    ? +query._tab_state_bill
    : 1;
  const orderId = orderIdForRouteSelector(state, props);
  const requestParams = requestParamsForOrderPageSelector(state, props);
  const idEntity = `${orderId}_${selectedTab}`;

  return {
    selectedTab,
    query: queryRouterSelector(state, props),
    enumerations: enumerationsSelector(state, { enumerationNames }),
    bills: billsForEntitySelector(state, {
      idEntity,
      typeEntity
    }),
    requestParams,
    idEntity,
    keyRouter: keyRouterSelector(state, props),
    pathname: pathnameRouterSelector(state, props)
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    { fetchBillsForEntity, push, fetchEnumerations }
  )
)(OrderBills);
