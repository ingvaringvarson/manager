import React from "react";
import PropTypes from "prop-types";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";
import OrderPositions from "./OrderPositions";
import OrderPayments from "./OrderPayments";
import OrderBills from "./OrderBills";
import OrderTasks from "./OrderTasks";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import Documents from "../Documents";
import documentObjectTypeId from "../../../constants/documentObjectTypeId";

const propTypes = {
  orderId: PropTypes.string.isRequired,
  selectedTabId: PropTypes.number.isRequired,
  onChangeTab: PropTypes.func.isRequired,
  fetchEntities: PropTypes.func.isRequired
};

const OrderTabs = React.memo(function OrderTabs(props) {
  return (
    <TabContainer
      selectedTabId={props.selectedTabId}
      onChangeSelected={props.onChangeTab}
    >
      <TabHeadContainer>
        <TabHead id={1}>Позиции заказа</TabHead>
        <TabHead id={2}>Счета</TabHead>
        <TabHead id={3}>Платежи</TabHead>
        <TabHead id={4}>Задачи</TabHead>
        <TabHead id={5}>Документы</TabHead>
      </TabHeadContainer>
      <TabBodyContainer>
        <BlockContent table>
          <TabBody id={1}>
            <OrderPositions fetchEntities={props.fetchEntities} />
          </TabBody>
          <TabBody id={2}>
            <OrderBills />
          </TabBody>
          <TabBody id={3}>
            <OrderPayments />
          </TabBody>
          <TabBody id={4}>
            <OrderTasks />
          </TabBody>
          <TabBody id={5}>
            <Documents
              idEntity={props.orderId}
              typeEntity={documentObjectTypeId.orders}
            />
          </TabBody>
        </BlockContent>
      </TabBodyContainer>
    </TabContainer>
  );
});

OrderTabs.propTypes = propTypes;

export default OrderTabs;
