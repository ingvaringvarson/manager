import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";
import { positionsDeviding } from "../../../ducks/positions";
import {
  getFieldsForFormType,
  getFieldsForDividingType
} from "../../../fields/formFields/positions/fieldsForDividing";
import { mapValuesToFields } from "../../../helpers/component/handlers";

import MediatorFormAndTable from "../../common/forms/MediatorFormAndTable";

import ReactModal from "react-modal";
import Heading from "../../../designSystem/molecules/Heading";
import Block from "../../../designSystem/organisms/Block";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import { customStyles as modalStyles } from "../../../constants/defaultStyles/reactModalStyles";

class DividingModal extends PureComponent {
  static propTypes = {
    typeMethod: PropTypes.string.isRequired,
    positions: PropTypes.array,
    entity: PropTypes.object.isRequired,
    handleClose: PropTypes.func.isRequired,
    fetchEntities: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      valuesForFields: { fields: this.props.positions }
    };

    this.mapValuesToFields = memoizeOne(mapValuesToFields.bind(this));
  }

  customStyles = {
    ...modalStyles,
    content: {
      ...modalStyles.content,
      width: 800,
      maxHeight: "86vh"
    }
  };

  iconProps = {
    height: "24px",
    width: "24px",
    onClick: this.props.handleClose
  };

  successCallback = () => {
    this.props.fetchEntities();
    this.props.handleClose();
  };

  handleFormSubmit = (values, positions) => {
    this.props.positionsDeviding(
      positions,
      this.props.typeMethod,
      this.props.entity,
      values,
      this.successCallback
    );
  };

  render() {
    const { positions, handleClose } = this.props;

    const { fieldsForTable, fieldsForFilters, key } = this.mapValuesToFields();

    return (
      <ReactModal
        ariaHideApp={false}
        isOpen={true}
        style={this.customStyles}
        onRequestClose={handleClose}
      >
        <Block>
          <Heading
            title="Разделение позиций"
            icon="close"
            iconProps={this.iconProps}
          />
          <BlockContent>
            <MediatorFormAndTable
              key={key}
              buttonTitle="Разделить"
              entities={positions}
              fieldsForContainer={fieldsForFilters}
              isRenderDefault={false}
              fieldsForTable={fieldsForTable}
              handleFormSubmit={this.handleFormSubmit}
            />
          </BlockContent>
        </Block>
      </ReactModal>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      fieldsForTable: getFieldsForDividingType(props.typeMethod),
      fieldsForFilters: getFieldsForFormType(props.typeMethod)
    };
  },
  { positionsDeviding }
)(DividingModal);
