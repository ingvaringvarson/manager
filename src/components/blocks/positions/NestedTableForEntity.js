import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import {
  TableContainer,
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  ToggleOpenRow,
  RenderContext
} from "../../common/tableControls";
import { Table } from "../../common/tableView";
import { fields as defaultFields } from "../../../fields/formFields/positions/fieldsForList";

const subTableParams = {
  subTable: true,
  mt: "8px",
  mb: "16px"
};

class NestedTableForEntity extends PureComponent {
  static propTypes = {
    positions: PropTypes.array,
    fields: PropTypes.array,
    enumerations: PropTypes.object,
    entity: PropTypes.shape({
      _id: PropTypes.string
    }),
    isNested: PropTypes.bool,
    renderPositionsMenu: PropTypes.func.isRequired
  };

  static defaultProps = {
    fields: defaultFields,
    isNested: true
  };

  renderRowHeadAfter = headProps => {
    return (
      <RenderContext {...headProps}>{this.renderHeadControls}</RenderContext>
    );
  };

  renderHeadControls = ({ selected }, { entities }) => {
    const positions = entities.filter(e => selected.includes(e._id));
    return this.props.renderPositionsMenu({ forList: true, positions });
  };

  renderRowBodyAfter = ({ entity }) =>
    this.props.renderPositionsMenu({ forList: false, positions: [entity] });

  renderTableContainer = () => {
    const {
      entity,
      positions,
      fields,
      enumerations,
      tableParams,
      ...restProps
    } = this.props;

    return (
      <TableContainer entities={positions} canBeSelected>
        <Table
          tableProps={
            tableParams || tableParams === null ? tableParams : subTableParams
          }
          cells={fields}
          enumerations={enumerations}
          entities={positions}
          hasChildrenBefore
          hasChildrenAfter
          renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
          renderRowHeadChildrenAfter={this.renderRowHeadAfter}
          renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
          renderRowBodyChildrenAfter={this.renderRowBodyAfter}
          helperData={entity}
          hasHeadFixed={false}
          {...restProps}
        />
      </TableContainer>
    );
  };

  render() {
    const { entity, isNested } = this.props;

    if (isNested) {
      return (
        <ToggleOpenRow id={entity._id}>
          {this.renderTableContainer()}
        </ToggleOpenRow>
      );
    }
    return this.renderTableContainer();
  }
}

export default NestedTableForEntity;
