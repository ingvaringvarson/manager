import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";
import { typeMethods, positionsReject } from "../../../ducks/positions";

import Modal from "../helperBlocks/Modal";
import MediatorFormAndTable from "../../common/forms/MediatorFormAndTable";
import { reduceValuesToFormFields } from "../../../fields/helpers";
import { generateId } from "../../../utils";
import Button from "../../../designSystem/atoms/Button";
import {
  fieldsForFormContainer,
  fieldsForTableContainer
} from "../../../fields/formFields/positions/fieldsForReject";
import { TransparentButton } from "../../common/styled/form";

class RejectModal extends PureComponent {
  static propTypes = {
    typeMethod: PropTypes.string.isRequired,
    positions: PropTypes.array,
    handleClose: PropTypes.func,
    entities: PropTypes.object.isRequired
  };

  getMappedFields = memoizeOne((values, fields) => {
    return {
      formFields: fields.reduce(reduceValuesToFormFields(values), []),
      key: generateId()
    };
  });

  mapPositions = memoizeOne(positions => ({
    positions
  }));

  successCallback = () => {
    this.props.fetchEntities();
    this.props.handleClose();
  };

  handleFormSubmit = formValues => {
    const { positions } = this.props;
    this.props.positionsReject(
      positions,
      this.props.typeMethod,
      this.props.entities,
      formValues,
      this.successCallback
    );
  };

  handleRejectByClient = formValues => () =>
    this.handleFormSubmit({ ...formValues, good_state: 10 });
  handleRejectByProvider = formValues => () =>
    this.handleFormSubmit({ ...formValues, good_state: 11 });

  renderButton = () => {
    if (this.props.typeMethod !== typeMethods.purchaseOrders) {
      return null;
    }

    return ({ isFormValid, values }) => (
      <>
        <TransparentButton
          mr="5px"
          type="button"
          disabled={!isFormValid}
          onClick={this.handleRejectByClient(values)}
        >
          ОТКАЗ КЛИЕНТА
        </TransparentButton>
        <Button
          type="button"
          disabled={!isFormValid}
          onClick={this.handleRejectByProvider(values)}
        >
          ОТКАЗ ПОСТАВЩИКА
        </Button>
      </>
    );
  };

  render() {
    const {
      positions,
      handleClose,
      fieldsForForm,
      fieldsForTable
    } = this.props;

    const mappedPositions = this.mapPositions(positions);
    const { formFields, key } = this.getMappedFields(
      mappedPositions,
      fieldsForForm
    );

    return (
      <Modal
        title="Отказаться от позиции"
        width="800px"
        handleClose={handleClose}
        isOpen
      >
        <MediatorFormAndTable
          key={key}
          entities={mappedPositions.positions}
          fieldsForTable={fieldsForTable}
          fieldsForContainer={formFields}
          handleFormSubmit={this.handleFormSubmit}
          isRenderDefault={false}
          renderButtonContext={this.renderButton()}
        />
      </Modal>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      fieldsForTable: fieldsForTableContainer[props.typeMethod],
      fieldsForForm: fieldsForFormContainer[props.typeMethod]
    };
  },
  { positionsReject }
)(RejectModal);
