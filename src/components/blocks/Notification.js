import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  errorListSelector,
  clearErrorLog,
  notificationListSelector,
  clearNotificationLog
} from "../../ducks/logger";
import styled from "styled-components";
import { codes } from "../../constants/errorsCode";
import Block from "../../designSystem/organisms/Block";
import BlockContent from "../../designSystem/organisms/BlockContent";
import Heading from "../../designSystem/molecules/Heading";
import Spinner from "../../designSystem/atoms/Spinner";

const FixedBlock = styled.div`
  position: fixed;
  padding: 20px;
  max-width: 350px;
  background-color: transparent;
  right: 50px;
  bottom: 50px;
  z-index: 9999;
  box-shadow: 0px rgba(0, 0, 0, 0.05);
`;

const ErrorMessage = styled.div`
  margin-bottom: 10px;
  font-size: 14px;
`;

const NotificationMessage = styled.div`
  padding: 16px 10px;
  font-size: 14px;
  color: #787878;
`;

const FlexBlockContent = styled(BlockContent)`
  display: flex;
  align-items: center;
  padding: 10px;
`;

class Notification extends Component {
  static propTypes = {
    errorList: PropTypes.array.isRequired,
    notificationList: PropTypes.array.isRequired,
    clearNotificationLog: PropTypes.func.isRequired,
    clearErrorLog: PropTypes.func.isRequired
  };

  counter = null;

  componentDidUpdate(prevProps) {
    if (
      (prevProps.errorList !== this.props.errorList &&
        this.props.errorList.length) ||
      (prevProps.notificationList !== this.props.notificationList &&
        this.props.notificationList.length)
    ) {
      if (this.counter) clearTimeout(this.counter);

      this.counter = setTimeout(() => {
        this.props.clearErrorLog();
        this.props.clearNotificationLog();
      }, 10000);
    }
  }

  handleCloseMessage = () => {
    if (this.counter) clearTimeout(this.counter);

    this.props.clearErrorLog();
    this.props.clearNotificationLog();
  };

  renderError = error => {
    const errorTitle = codes[error.code];

    let content = (
      <ErrorMessage>
        {error.message ? error.message : "Ошибка без описания"}
      </ErrorMessage>
    );

    if (error.details) {
      if (error.details.validationErrors) {
        content = error.details.validationErrors.map((entity, entityKey) => {
          return (
            <ErrorMessage key={entityKey}>
              <b>
                [{entity.field}: {entity.value}]
              </b>{" "}
              - {entity.message}
            </ErrorMessage>
          );
        });
      } else if (error.details.errors) {
        content = error.details.errors.map((entity, entityKey) => {
          return <ErrorMessage key={entityKey}>{entity.message}</ErrorMessage>;
        });
      }
    }
    return (
      <Block mb="middle" key={error.id}>
        <Heading
          title={error.code + ""}
          subtitle={errorTitle ? errorTitle : "Неизвестный код ошибки"}
        />
        <BlockContent>{content}</BlockContent>
      </Block>
    );
  };

  renderNotification = notification => {
    const content = (
      <NotificationMessage>
        {notification.message ? notification.message : "Сообщение без описания"}
      </NotificationMessage>
    );

    return (
      <Block mb="middle" key={notification.key}>
        <FlexBlockContent>
          {content}
          {notification.loading && <Spinner ml="16px" size="medium" />}
        </FlexBlockContent>
      </Block>
    );
  };

  render() {
    const { errorList, notificationList } = this.props;

    if (!errorList.length && !notificationList.length) return null;

    return (
      <FixedBlock onClick={this.handleCloseMessage}>
        {notificationList.map(this.renderNotification)}
        {errorList.map(this.renderError)}
      </FixedBlock>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorList: errorListSelector(state),
    notificationList: notificationListSelector(state)
  };
}

export default connect(
  mapStateToProps,
  { clearNotificationLog, clearErrorLog }
)(Notification);
