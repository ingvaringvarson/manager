import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import PaginationWrap from "../../designSystem/organisms/Pagination";
import { maskOnlyNumbers } from "../../helpers/masks";
import styled from "styled-components";
import { generateUrlWithQuery } from "../../utils";

const StyledPaginationWrap = styled(PaginationWrap)`
  margin-top: 16px;
`;

class Pagination extends PureComponent {
  static propTypes = {
    page: PropTypes.number.isRequired,
    countItemsPerPage: PropTypes.number,
    maxCountItems: PropTypes.number,
    delay: PropTypes.number,
    moveToPage: PropTypes.func,
    push: PropTypes.func,
    maxPages: PropTypes.number,
    pathname: PropTypes.string.isRequired,
    requestParams: PropTypes.object.isRequired
  };

  static defaultProps = {
    page: 1,
    countItemsPerPage: 20,
    maxCountItems: 0,
    delay: 1000,
    maxPages: null
  };

  state = {
    page: 1,
    currentPage: 1,
    lastPage: 1
  };

  static getDerivedStateFromProps(props, state) {
    const newState = {};

    if (props.page !== state.page) {
      newState.page = props.page;

      if (props.page !== state.currentPage) {
        newState.currentPage = props.page;
      }
    }

    const lastPage =
      props.maxPages !== null
        ? props.maxPages
        : Math.floor(props.maxCountItems / props.countItemsPerPage);

    if (lastPage !== state.lastPage) {
      newState.lastPage = lastPage;
    }

    if (Object.keys(newState).length) return newState;

    return null;
  }

  giveCorrectPageNumber = number => {
    switch (true) {
      case !number:
        return 1;
      case number > this.state.lastPage:
        return this.state.lastPage;
      default:
        return number;
    }
  };

  handleMoveToPrevPage = event => {
    event.preventDefault();

    if (this.state.currentPage <= 1) return false;

    this.handleMoveToNewPage(this.state.currentPage - 1);
  };

  handleMoveToNextPage = event => {
    event.preventDefault();

    const nextPage = this.state.currentPage + 1;

    if (nextPage > this.state.lastPage) return false;

    this.handleMoveToNewPage(nextPage);
  };

  handleChangeCurrentPage = event => {
    event.preventDefault();

    const valueNumber = +maskOnlyNumbers(event.target.value);
    const newPageNumber = this.giveCorrectPageNumber(valueNumber);

    if (this.state.currentPage === newPageNumber) return false;

    const { delay } = this.props;
    const { handleMoveToNewPage } = this;

    if (this.counterDelay) clearTimeout(this.counterDelay);

    this.setState({ currentPage: newPageNumber });
    this.counterDelay = setTimeout(
      () => handleMoveToNewPage(newPageNumber),
      delay
    );
  };

  handleMoveToNewPage = pageNumber => {
    const { pathname, requestParams, moveToPage } = this.props;

    if (moveToPage) return moveToPage(pageNumber);

    this.props.push(
      generateUrlWithQuery({
        pathname,
        oldQuery: requestParams,
        newQuery: { page: pageNumber }
      })
    );
  };

  render() {
    return (
      <StyledPaginationWrap
        count={this.state.lastPage}
        inputProps={{
          onChange: this.handleChangeCurrentPage,
          value: this.state.currentPage
        }}
        prevButtonProps={{
          disabled: this.state.currentPage <= 1,
          onClick: this.handleMoveToPrevPage
        }}
        nextButtonProps={{
          disabled: this.state.currentPage >= this.state.lastPage,
          onClick: this.handleMoveToNextPage
        }}
        {...this.props}
      />
    );
  }
}

export default Pagination;
