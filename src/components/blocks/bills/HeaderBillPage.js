import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import Row from "../../../designSystem/templates/Row";
import Column from "../../../designSystem/templates/Column";
import HeadingCard from "../../common/headlines/HeadingCard";
import HeadingInfo from "../../common/headlines/HeadingInfo";
import { renderEnumerationValue } from "../../../fields/helpers";
import { digitsForNumbers } from "../../../utils";
import { RedText } from "../../common/styled/text";

import BillMenu from "./BillMenu";
import { memoizeToArray } from "../../../helpers/cache";

class HeaderBillPage extends PureComponent {
  static propTypes = {
    bill: PropTypes.object,
    client: PropTypes.object,
    enumerations: PropTypes.object
  };

  renderHeaderInfo() {
    const { bill, enumerations } = this.props;

    const billTypeName = renderEnumerationValue("billType", "type")({
      enumerations,
      entity: bill
    });

    return (
      <Row>
        {billTypeName && (
          <Column mr="30px">
            <HeadingInfo subTitle="Тип счёта" info={billTypeName} />
          </Column>
        )}
        <Column mr="30px">
          <HeadingInfo
            subTitle="Сумма"
            info={<RedText>{digitsForNumbers(bill.bill_sum)} руб.</RedText>}
          />
        </Column>
        <Column mr="30px">
          <HeadingInfo
            subTitle="Оплачено"
            info={
              <RedText>
                {digitsForNumbers(bill.bill_sum - bill.receipt_sum)} руб.
              </RedText>
            }
          />
        </Column>
        <Column mr="30px">
          <HeadingInfo
            subTitle="Сумма к оплате"
            info={<RedText>{digitsForNumbers(bill.receipt_sum)} руб.</RedText>}
          />
        </Column>
      </Row>
    );
  }

  render() {
    if (!this.props.bill) return null;

    const { bill, enumerations } = this.props;

    const billStateName = renderEnumerationValue("billState", "state")({
      enumerations,
      entity: bill
    });

    return (
      <Row alignCenter justifyBetween mb="20px">
        <Column>
          <HeadingCard
            title="Карточка счёта"
            name={`id ${bill.code}`}
            status={billStateName ? <RedText>{billStateName}</RedText> : null}
          />
        </Column>
        <Column>{this.renderHeaderInfo()}</Column>
        <Column>
          <BillMenu useMainCommand bills={memoizeToArray(bill)} />
        </Column>
      </Row>
    );
  }
}

export default HeaderBillPage;
