import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import Table from "../../common/table/Table";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../ducks/enumerations";
import { fields } from "../../../fields/formFields/bills/fieldsForBillPositions";
import { push } from "connected-react-router";
import { TableContainer } from "../../common/tableControls";
import { billInfoSelector } from "../../../ducks/bills";

const enumerationNames = ["productType"];

class BillPositions extends Component {
  static propTypes = {
    enumerations: PropTypes.shape({
      productType: PropTypes.shape({
        list: PropTypes.array,
        map: PropTypes.object
      }).isRequired
    }).isRequired,
    bill: PropTypes.object.isRequired,
    fetchEnumerations: PropTypes.func.isRequired
  };

  render() {
    if (!this.props.bill) return null;

    return (
      <>
        <TableContainer
          canBeSelected
          entities={this.props.bill.positions || []}
        >
          <Table
            cells={fields}
            entities={this.props.bill.positions || []}
            enumerations={this.props.enumerations}
          />
        </TableContainer>
      </>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    enumerations: enumerationsSelector(state, { enumerationNames }),
    bill: billInfoSelector(state, props.match.params)
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    { push, fetchEnumerations }
  )
)(BillPositions);
