import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import InfoBlock from "../../common/info/InfoBlock";
import { fieldGroups } from "../../../fields/formFields/bills/fieldsForInfoBlocks";
import memoizeOne from "memoize-one";
import InfoModalControl from "../../common/info/InfoModalControl";
import { getUrl } from "../../../helpers/urls";

class BillInfoBlocks extends PureComponent {
  static propTypes = {
    bill: PropTypes.object.isRequired,
    client: PropTypes.object.isRequired,
    enumerations: PropTypes.object.isRequired,
    onSubmitForm: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired
  };

  renderContent = info => (
    state = {},
    modalProps = null,
    blockProps = null,
    iconProps = null
  ) => (
    <InfoBlock
      iconIsShow={state.iconIsShow}
      info={info}
      blockProps={blockProps}
      iconProps={iconProps}
    />
  );

  getBlock = (entity, enumerations, block) => {
    const info = block.mapValuesToFields(block, enumerations, entity);
    return {
      ...info,
      renderContent: this.renderContent(info)
    };
  };

  getBlocks = memoizeOne((bill, client, enumerations) => {
    const blocks = [];

    if (client.type === 1) {
      blocks.push(this.getBlock(client, enumerations, fieldGroups["client"]));
    } else if (client.type === 2) {
      blocks.push(this.getBlock(client, enumerations, fieldGroups["company"]));
    }

    blocks.push(
      this.getBlock(client["contacts"], enumerations, fieldGroups["contacts"])
    );

    blocks.push(
      this.getBlock(
        client["contact_persons"],
        enumerations,
        fieldGroups["contact_persons"]
      )
    );

    const orders =
      bill.order_id && bill.order_id.length
        ? [
            bill.order_id.map((order, index) => ({
              id: order,
              code: bill.order_code ? bill.order_code[index] : null
            }))
          ]
        : [];

    return blocks.concat(
      this.getBlock(orders, enumerations, fieldGroups["orders"]),
      this.getBlock(
        bill.payments_inf || [],
        enumerations,
        fieldGroups["payments"]
      ),
      this.getBlock(bill, enumerations, fieldGroups["firm"])
    );
  });

  getBlockProps = memoizeOne(onSubmitForm => ({
    onSubmitForm
  }));

  handleMoveToClientPage = () => {
    if (this.props.client) {
      window.open(
        `//${window.location.host}${getUrl("clients", {
          id: this.props.client.id
        })}`
      );
    }
  };

  handleMoveToFirmPage = () => {
    if (this.props.bill && this.props.bill.firm_id) {
      window.open(
        `//${window.location.host}${getUrl("clients", {
          id: this.props.bill.firm_id
        })}`
      );
    }
  };

  iconPropsForClient = {
    onClick: this.handleMoveToClientPage
  };

  iconPropsForFirm = {
    onClick: this.handleMoveToFirmPage
  };

  render() {
    const { bill, client, enumerations, onSubmitForm } = this.props;

    if (!bill || !client) return null;

    const blocks = this.getBlocks(bill, client, enumerations);
    const blockProps = this.getBlockProps(onSubmitForm);

    return blocks.map(info => {
      if (!info.formMain) {
        return <InfoBlock blockProps={blockProps} info={info} />;
      } else if (info.formMain && !info.formMain.type) {
        return (
          <InfoBlock
            iconProps={
              info.formMain.type === "info"
                ? this.iconPropsForFirm
                : this.iconPropsForClient
            }
            iconIsShow
            blockProps={blockProps}
            info={info}
          />
        );
      }

      const fields =
        info.formMain.type === "edit" ? info.formFields : info.fieldsForAdd;

      return (
        <InfoModalControl
          formProps={info.formMain}
          onSubmitForm={onSubmitForm}
          fields={fields}
        >
          {info.renderContent}
        </InfoModalControl>
      );
    });
  }
}

export default BillInfoBlocks;
