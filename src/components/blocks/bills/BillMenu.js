import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import CommandsByStatusMenu from "../../common/menus/CommandsByStatusMenu";
import DocumentPrint from "../DocumentPrint";
import BillCancellation from "./billCommands/BillCancellation";
import ReturnBillPayment from "./billCommands/ReturnBillPayment";

import billCommands, { filterBillsCommands } from "../../../commands/bills";

import documentObjectTypeId from "../../../constants/documentObjectTypeId";

class BillMenu extends PureComponent {
  static propTypes = {
    bills: PropTypes.arrayOf(PropTypes.object).isRequired,
    client: PropTypes.object,
    forList: PropTypes.bool,
    useMainCommand: PropTypes.bool,
    fetchEntities: PropTypes.func
  };

  static defaultProps = {
    forList: false,
    useMainCommand: false,
    fetchEntities: () => null
  };

  modalIsNotImplementedFunc = (_, command) =>
    alert(`Функциональность ${command.title} пока не добавлена`);

  commands = [
    {
      ...billCommands.cancellationCommand,
      renderModal: (handleClose, bills) => (
        <BillCancellation
          fetchEntities={this.props.fetchEntities}
          handleClose={handleClose}
          bill={bills[0]}
        />
      )
    },
    {
      ...billCommands.printDocumentsCommand,
      renderModal: (handleClose, bills) => (
        <DocumentPrint
          isOpen
          idEntity={bills[0].id}
          objectTypeId={documentObjectTypeId.bills}
          entityFirmId={bills[0].firm_id}
          handleClose={handleClose}
        />
      )
    },
    {
      ...billCommands.returnPaymentCommand,
      renderModal: (handleClose, bills) => (
        <ReturnBillPayment
          isOpen
          handleClose={handleClose}
          title={billCommands.returnPaymentCommand.title}
          bill={bills[0]}
          client={this.props.client}
        />
      )
    },
    {
      ...billCommands.acceptPaymentCommand,
      onClick: this.modalIsNotImplementedFunc
    }
  ];

  getMainCommand = memoizeOne(([bill]) => {
    switch (true) {
      case !this.props.useMainCommand:
        return null;
      case bill.receipt_sum > 0 && bill.type === 5:
        return billCommands.returnPaymentCommand.id;
      default:
        return billCommands.acceptPaymentCommand.id;
    }
  });

  render() {
    const { bills, forList } = this.props;
    return (
      <CommandsByStatusMenu
        forList={forList}
        entityOrEntities={bills}
        commands={this.commands}
        mainCommandId={this.getMainCommand(bills)}
        commandsToMenuOptionsFilter={filterBillsCommands}
      />
    );
  }
}

export default BillMenu;
