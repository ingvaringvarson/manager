import React from "react";
import PropTypes from "prop-types";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";
import BillPositions from "./BillPositions";
import BillPayments from "./BillPayments";
import Documents from "../Documents";
import documentObjectTypeId from "../../../constants/documentObjectTypeId";

const propTypes = {
  billId: PropTypes.string.isRequired,
  selectedTabId: PropTypes.number.isRequired,
  onChangeTab: PropTypes.func.isRequired
};

const BillTabs = React.memo(function OrderTabs(props) {
  return (
    <TabContainer
      selectedTabId={props.selectedTabId}
      onChangeSelected={props.onChangeTab}
    >
      <TabHeadContainer>
        <TabHead id={1}>Позиции счёта</TabHead>
        <TabHead id={2}>Платежи</TabHead>
        <TabHead id={3}>Документы</TabHead>
      </TabHeadContainer>
      <TabBodyContainer p="16px" pt="0">
        <TabBody id={1}>
          <BillPositions />
        </TabBody>
        <TabBody id={2}>
          <BillPayments />{" "}
        </TabBody>
        <TabBody id={3}>
          <Documents
            idEntity={props.billId}
            typeEntity={documentObjectTypeId.bills}
          />
        </TabBody>
      </TabBodyContainer>
    </TabContainer>
  );
});

BillTabs.propTypes = propTypes;

export default BillTabs;
