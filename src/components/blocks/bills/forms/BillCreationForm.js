import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Checkbox from "../../../../designSystem/molecules/Checkbox";
import Input from "../../../../designSystem/atoms/Input";
import Button from "../../../../designSystem/atoms/Button";

const ButtonWrapper = styled.div`
  padding: 16px 0;
  text-align: right;
`;

const InputsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  label {
    font-size: 14px;
    line-height: 16px;
  }
  input {
    width: 100%;
    margin-top: 10px;
  }

  & > div:last-child {
    padding-left: 15px;
  }
  & > div:first-child {
    padding-right: 15px;
  }
`;

const CheckboxWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const CheckboxesWrapper = styled.div`
  margin-bottom: 10px;
  label {
    cursor: pointer;
  }
`;

class BillCreationForm extends PureComponent {
  static propTypes = {
    handleFormSubmit: PropTypes.func.isRequired,
    initialValues: PropTypes.shape({
      billType: PropTypes.string,
      prepayed: PropTypes.string,
      notPrepayed: PropTypes.string
    }).isRequired
  };

  state = {
    radioButtons: [
      {
        label: "Предоплатный",
        name: "prepayed",
        checked: false,
        inputNames: ["prepayed"]
      },
      {
        label: "Не Предоплатный",
        name: "notPrepayed",
        checked: false,
        inputNames: ["notPrepayed"]
      },
      {
        label: "Два Счета",
        name: "twoBills",
        checked: true,
        inputNames: ["prepayed", "notPrepayed"]
      }
    ],
    sumInputs: [
      {
        placeholder: "Введите сумму предоплатного счета",
        label: "Сумма предоплатного счета",
        name: "prepayed",
        id: "prepayed"
      },
      {
        placeholder: "Введите сумму не предоплатного счета",
        label: "Сумма не предоплатного счета",
        name: "notPrepayed",
        id: "notPrepayed"
      }
    ],
    formValues: this.props.initialValues || {
      billType: "twoBills",
      prepayed: "",
      notPrepayed: ""
    }
  };

  handleRadioChange = event => {
    const { name } = event.target;

    this.setState(prevState => {
      return prevState.radioButtons.reduce(
        (result, currentButton) => {
          if (name === currentButton.name) {
            result.formValues.billType = currentButton.name;
            currentButton.inputNames.forEach(
              name => (result.formValues[name] = "")
            );
          }

          return {
            ...result,
            radioButtons: result.radioButtons.concat({
              ...currentButton,
              checked: currentButton.name === name
            })
          };
        },
        {
          radioButtons: [],
          formValues: {}
        }
      );
    });
  };

  handleInputChange = event => {
    const { name, value } = event.target;

    this.setState(prevState => {
      return {
        formValues: {
          ...prevState.formValues,
          [name]: value
        },
        sumInputs: prevState.sumInputs.map(input => {
          if (input.id !== name) {
            return input;
          }
          return {
            ...input,
            value
          };
        })
      };
    });
  };

  handleFormSubmit = () => {
    this.props.handleFormSubmit(this.state.formValues);
  };

  renderInputs = (billTypeRadio, sumInputs, formValues) => {
    const checkedRadio = billTypeRadio.find(button => {
      return button.checked;
    });
    if (!checkedRadio) return null;

    return sumInputs.map(input => {
      return (
        <div key={input.id}>
          <label>{input.label}</label>
          <Input
            {...input}
            value={formValues[input.id] || ""}
            onChange={this.handleInputChange}
            disabled={!checkedRadio.inputNames.some(name => input.id === name)}
          />
        </div>
      );
    });
  };

  render() {
    const { sumInputs, radioButtons, formValues } = this.state;
    const inputs = this.renderInputs(radioButtons, sumInputs, formValues);
    return (
      <div>
        <CheckboxesWrapper>
          {this.state.radioButtons.map(button => {
            return (
              <CheckboxWrapper key={button.name}>
                <Checkbox
                  type="radio"
                  id={button.name}
                  name={button.name}
                  checked={button.checked}
                  onChange={this.handleRadioChange}
                />
                <label htmlFor={button.name}>{button.label}</label>
              </CheckboxWrapper>
            );
          })}
        </CheckboxesWrapper>
        <InputsWrapper>{inputs}</InputsWrapper>
        <ButtonWrapper>
          <Button type="button" onClick={this.handleFormSubmit}>
            СОЗДАТЬ СЧЕТ
          </Button>
        </ButtonWrapper>
      </div>
    );
  }
}

export default BillCreationForm;
