import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createBill } from "../../../../ducks/bills";

import Table from "../../../common/table/Table";
import TableContainer from "../../../common/tableControls/TableContainer";
import CommonModal from "../../../common/modals/CommonModal";
import { tableFields } from "../../../../fields/formFields/bills/fieldsForBillCreation";

import BillCreationForm from "./BillCreationForm";

class BillCreation extends PureComponent {
  static propTypes = {
    isOpen: PropTypes.bool,
    handleClose: PropTypes.func.isRequired,
    orders: PropTypes.arrayOf(PropTypes.object).isRequired,
    positionsIds: PropTypes.arrayOf(PropTypes.string),
    selectedAll: PropTypes.bool,
    createBill: PropTypes.func.isRequired,
    handleBillCreate: PropTypes.func
  };

  static defaultProps = {
    isOpen: false,
    selectedAll: false
  };

  contentStyles = {
    width: 800
  };

  renderPositions = () => {
    let positions = [];
    let isContractIdIsSame = false;
    let isFirmIdIsSame = false;

    if (this.props.selectedAll) {
      this.props.orders.map(order => {
        if (order.positions) {
          positions = positions.concat(order.positions);
        }
      });
    } else {
      this.props.orders.map(order => {
        if (order.positions) {
          positions = positions.concat(
            order.positions.filter(pos =>
              this.props.positionsIds.some(id => pos.id === id)
            )
          );
        }
      });
    }

    if (this.props.orders.length) {
      isContractIdIsSame = this.props.orders.every(
        order => order.contract_id === this.props.orders[0].contract_id
      );
      isFirmIdIsSame = this.props.orders.every(
        order => order.firm_id === this.props.orders[0].firm_id
      );
    }

    return {
      positions,
      shouldShow: !!this.props.orders,
      isContractIdIsSame,
      isFirmIdIsSame,
      shouldShowContent: isFirmIdIsSame && isContractIdIsSame
    };
  };

  renderBillPositions = positions => {
    let posIds = [];

    const bills = positions.reduce((currPosition, result) => {
      if (
        currPosition.order_position_id &&
        !posIds.some(id => id === currPosition.order_position_id)
      ) {
        posIds.push(currPosition.order_position_id);
        result.push(
          <div key={currPosition.order_position_id}>
            Указанная вами позиция заказа уже есть в счете №
            {currPosition.order_position_id}. Аннулируйте его, если это
            необходимо
          </div>
        );
      }
      return result;
    }, []);

    return bills.length ? bills : null;
  };

  handleFormSubmit = formValues => {
    /*
      export const createBill = (
        typeEntity = "",
        formData = {},
        values = [],
        normalizeCallback
      )
    */

    /*
      props:

      isOpen: PropTypes.bool,
      handleClose: PropTypes.func.isRequired,
      orders: PropTypes.arrayOf(PropTypes.object).isRequired,
      positionsIds: PropTypes.arrayOf(PropTypes.string),
      selectedAll: PropTypes.bool,
      createBill: PropTypes.func.isRequired
    */

    //ToDo: переписать метод создания заказа, вынести условия мапинга в отдельную функцию
    this.props.createBill("orders", formValues, { ...this.props });
    this.props.handleClose();
  };

  render() {
    const {
      positions,
      shouldShow,
      isContractIdIsSame,
      isFirmIdIsSame,
      shouldShowContent
    } = this.renderPositions();

    if (!shouldShow) {
      return null;
    }

    return (
      <CommonModal
        isOpen={this.props.isOpen}
        handleClose={this.props.handleClose}
        title="Создать счет"
        contentStyles={this.contentStyles}
      >
        {this.renderBillPositions(positions)}
        {shouldShowContent ? (
          <>
            <TableContainer>
              <Table entities={positions} cells={tableFields} />
            </TableContainer>
            <BillCreationForm
              handleFormSubmit={this.handleFormSubmit}
              initialValues={{
                billType: "twoBills",
                prepayed: "",
                notPrepayed: ""
              }}
            />
          </>
        ) : (
          <>
            {isContractIdIsSame && (
              <div>
                Выбранные вами заказы оформлены на разные договоры. Создать
                единый счет нельзя.
              </div>
            )}
            {isFirmIdIsSame && (
              <div>
                Выбранные вами заказы оформлены на разные организации. Создать
                единый счет нельзя.
              </div>
            )}
          </>
        )}
      </CommonModal>
    );
  }
}

export default connect(
  null,
  { createBill }
)(BillCreation);
