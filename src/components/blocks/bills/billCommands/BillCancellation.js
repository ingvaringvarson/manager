import React, { PureComponent } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled from "styled-components";

import CommonModal from "../../../common/modals/CommonModal";
import Table from "../../../common/table/Table";
import TableContainer from "../../../common/tableControls/TableContainer";

import { fields } from "../../../../fields/formFields/bills/fieldsBillCancellationTable";

import Input from "../../../../designSystem/atoms/Input";
import Button from "../../../../designSystem/atoms/Button";

import { putBillPrepared } from "../../../../ducks/bills";

const InfoBlockWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 16px;
  input {
    width: 150px;
    margin-left: 10px;
    &:disabled {
      color: #3b3b3b;
      opacity: 1;
    }
  }
`;

const NoteBlock = styled.p`
  padding-left: 10px;
  border-left: 2px solid #eb0028;
  margin-bottom: 16px;
  span {
    color: #eb0028;
  }
`;

const CancelPaymentBlock = styled.div`
  margin: 16px 7px;
  color: #336ea8;
  text-align: end;
  cursor: pointer;
`;

const StickToRightWrapper = styled.div`
  text-align: end;
`;

class BillCancellation extends PureComponent {
  static propTypes = {
    bill: PropTypes.object.isRequired,
    handleClose: PropTypes.func,
    putBillPrepared: PropTypes.func.isRequired,
    isOpen: PropTypes.bool,
    fetchEntities: PropTypes.func.isRequired
  };

  static defaultProps = {
    isOpen: true
  };

  successCallback = () => {
    this.props.handleClose();
    this.props.fetchEntities();
  };

  handleBillPaymentsCancel = () => {
    this.props.putBillPrepared(
      "bills",
      this.props.bill.id,
      {
        ...this.props.bill,
        payments_inf: null
      },
      this.props.fetchEntities
    );
  };

  handleBillCancel = () => {
    this.props.putBillPrepared(
      "bills",
      this.props.bill.id,
      {
        ...this.props.bill,
        state: 4
      },
      this.successCallback
    );
  };

  renderContent = () => {
    const { bill } = this.props;
    let infoBlock = (
      <InfoBlockWrapper>
        <p>Вы хотите аннулировать счет №</p>
        <Input disabled value={bill.code} />
      </InfoBlockWrapper>
    );

    const cancelButton = (
      <StickToRightWrapper>
        <Button onClick={this.handleBillCancel} disabled={!!bill.payments_inf}>
          Аннулировать
        </Button>
      </StickToRightWrapper>
    );

    if (!bill.payments_inf) {
      return (
        <div>
          {infoBlock}
          {cancelButton}
        </div>
      );
    }
    return (
      <div>
        {infoBlock}
        <NoteBlock>
          Этот счет <span>оплачен</span>. Отмените оплату этого счета перед
          аннулированием!
        </NoteBlock>
        <TableContainer>
          <Table cells={fields} entities={bill.payments_inf} />
        </TableContainer>
        <CancelPaymentBlock onClick={this.handleBillPaymentsCancel}>
          Отменить оплату
        </CancelPaymentBlock>
        {cancelButton}
      </div>
    );
  };

  render() {
    return (
      <CommonModal
        title="Аннулировать счет"
        handleClose={this.props.handleClose}
        isOpen={this.props.isOpen}
      >
        {this.renderContent()}
      </CommonModal>
    );
  }
}

export default connect(
  null,
  { putBillPrepared }
)(BillCancellation);
