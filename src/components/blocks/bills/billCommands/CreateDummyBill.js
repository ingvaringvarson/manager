import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createDummyBill } from "../../../../ducks/bills";
import { enumerationsSelector } from "../../../../ducks/enumerations";
import memoizeOne from "memoize-one";
import { fields } from "../../../../fields/formFields/bills/createDummyBillFields";
import { reduceValuesToFormFields } from "../../../../fields/helpers";
import {
  ContainerContext,
  FieldContext,
  RenderContext
} from "../../../common/formControls";
import Button from "../../../common/buttons/RightStickedButton";
import { generateId } from "../../../../utils";

const enumerationNames = ["contractType", "contractState"];

class CreateDummyBill extends PureComponent {
  static propTypes = {
    client: PropTypes.object.isRequired,
    enumerations: PropTypes.object.isRequired,
    successCallback: PropTypes.func.isRequired
  };

  renderCells = memoizeOne((cells, enumerations, client) => {
    return {
      cells: cells.reduce(
        reduceValuesToFormFields({ client }, enumerations),
        []
      ),
      key: generateId()
    };
  });

  handleFormSubmit = values => {
    this.props.createDummyBill(values, {
      client: this.props.client,
      successCallback: this.props.successCallback
    });
  };

  render() {
    const { cells, key } = this.renderCells(
      fields,
      this.props.enumerations,
      this.props.client
    );

    return (
      <ContainerContext
        key={key}
        fields={cells}
        onSubmitForm={this.handleFormSubmit}
        enumerations={this.props.enumerations}
        helperFieldsData={this.props.client}
      >
        <RenderContext>
          {({ isFormValid }) => (
            <>
              {cells.map(cell => {
                return (
                  <FieldContext
                    helperFieldData={this.props.client}
                    name={cell.id}
                    key={cell.id}
                  />
                );
              })}
              <Button type="submit" disabled={!isFormValid}>
                Создать счет
              </Button>
            </>
          )}
        </RenderContext>
      </ContainerContext>
    );
  }
}

export default connect(
  state => ({
    enumerations: enumerationsSelector(state, { enumerationNames })
  }),
  { createDummyBill }
)(CreateDummyBill);
