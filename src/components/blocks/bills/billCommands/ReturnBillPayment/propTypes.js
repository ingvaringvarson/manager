import PropTypes from "prop-types";

export const formPropTypes = {
  returnEntity: PropTypes.object,
  bill: PropTypes.object,
  client: PropTypes.object.isRequired,
  bills: PropTypes.shape({
    entities: PropTypes.array.isRequired
  }),
  fetchBillsForEntity: PropTypes.func.isRequired,
  returnPaymentCommand: PropTypes.func.isRequired,
  internetAcquiringIsAvailable: PropTypes.bool.isRequired,
  withoutCheckIsAvailable: PropTypes.bool.isRequired,
  enumerations: PropTypes.object,
  fetchEnumerations: PropTypes.func.isRequired
};

export const ModalPropTypes = {
  ...formPropTypes,
  handleClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool,
  title: PropTypes.string
};
