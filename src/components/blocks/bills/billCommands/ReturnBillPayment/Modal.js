import React from "react";
import Form from "./Form";
import CommonModal from "../../../../common/modals/CommonModal";
import { contentStyles } from "./styles";
import { ModalPropTypes } from "./propTypes";

const Modal = ({ handleClose, isOpen, title, ...props }) => (
  <CommonModal
    handleClose={handleClose}
    isOpen={isOpen}
    title={title}
    contentStyles={contentStyles}
  >
    <Form {...props} />
  </CommonModal>
);

Modal.propTypes = ModalPropTypes;

export default Modal;
