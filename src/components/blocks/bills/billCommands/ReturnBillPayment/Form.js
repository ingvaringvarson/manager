import React, { Component } from "react";
import { connect } from "react-redux";
import styled from "styled-components";

import ReactTooltip from "react-tooltip";
import Title from "../../../../../designSystem/atoms/Title";
import Text from "../../../../../designSystem/atoms/Text";
import Icon from "../../../../../designSystem/atoms/Icon";
import Box from "../../../../../designSystem/organisms/Box";
import BlockContent from "../../../../../designSystem/organisms/BlockContent";
import FieldForm from "../../../../common/form/FieldForm";
import {
  fieldsForReturnPaymentForm,
  balanceField,
  billIdField,
  internetAcquiringField,
  paymentMethodField_1,
  paymentMethodField_2,
  paymentTypeField_1,
  paymentTypeField_2,
  paymentTypeField_3,
  paymentTypeField_4,
  receiptField,
  sumField,
  withoutCheckField,
  receiptAddressField
} from "../../../../../fields/formFields/payments/returnPayment";
import { ContainerContext } from "../../../../common/formControls";
import memoizeOne from "memoize-one";
import {
  fetchBillsForEntity,
  billsForEntitySelector,
  moduleName as billsModuleName
} from "../../../../../ducks/bills";
import { modulesIsLoadingSelector } from "../../../../../ducks/logger";
import { reduceValuesToFormFields } from "../../../../../fields/helpers";
import { generateId, getValueInDepth } from "../../../../../utils";
import {
  returnPaymentCommand,
  commandsLoaderName
} from "../../../../../ducks/payments";
import Button from "../../../../../designSystem/atoms/Button";
import BtnBlock from "../../../../common/styled/BtnBlock";
import PreLoader from "../../../../common/PreLoader";
import { hasPermissionSelector } from "../../../../../ducks/user";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../../../ducks/enumerations";
import {
  pm_payments_create_return_internet_acquiring,
  pm_payments_create_without_check
} from "../../../../../constants/permissions";
import { Link } from "react-router-dom";
import { getUrl } from "../../../../../helpers/urls";
import { formPropTypes } from "./propTypes";

const StyledIcon = styled(Icon)`
  margin-top: 8px;
  justify-self: center;
  width: 24px;
  color: #000;
  cursor: pointer;
`;

const BlockContentForm = styled(BlockContent)`
  position: static;
  max-height: 650px;
  width: 460px;
  overflow-y: auto;
`;

const PreloaderContainer = styled.div`
  position: relative;
`;
const ToolTipContainer = styled.div`
  width: 300px;
  padding: 5px 0;
`;

const typeEntity = "returns";

const enumerationNames = ["billType", "billState"];

class ReturnPaymentForm extends Component {
  static propTypes = formPropTypes;

  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
    if (this.props.returnEntity) {
      const { id } = this.props.returnEntity;
      this.props.fetchBillsForEntity(typeEntity, id, {
        return_id: id,
        state: [1, 2]
      });
    }
  }

  getBillInfo = memoizeOne((bill, id, bills) => {
    if (bill && bill.id === id) return bill;

    return bills.entities.find(e => e.id === id);
  });

  renderBillInfo = values => {
    const { bill, bills, enumerations } = this.props;
    const billId = getValueInDepth(values, ["bill_id", 0]);
    const info = this.getBillInfo(bill, billId, bills);

    if (!info) return "Нет информации по счёту.";

    const billType = getValueInDepth(enumerations, [
      "billType",
      "map",
      info.type,
      "name"
    ]);
    const billState = getValueInDepth(enumerations, [
      "billState",
      "map",
      info.state,
      "name"
    ]);

    return (
      <ToolTipContainer>
        {!!billType && (
          <Text mb="10px" fontSize="12px">
            Тип счета: {billType}
          </Text>
        )}
        {!!billState && (
          <Text mb="10px" fontSize="12px">
            Статус счета: {billState}
          </Text>
        )}
        {!!(info.positions && info.positions.length) && (
          <>
            <Text mb="5px" fontSize="12px">
              Позиции в счете:
            </Text>
            {info.positions.map(p => {
              let name = "";

              if (p.product) {
                if (p.product.article)
                  name = name
                    ? `${name} ${p.product.article}`
                    : `${p.product.article}`;
                if (p.product.brand)
                  name = name
                    ? `${name} ${p.product.brand}`
                    : `${p.product.brand}`;
                if (p.product.name)
                  name = name
                    ? `${name} ${p.product.name}`
                    : `${p.product.name}`;
              }

              return name ? (
                <Text mb="5px" pl="5px" fontSize="10px">
                  {" "}
                  - {name}
                </Text>
              ) : (
                name
              );
            })}
          </>
        )}
      </ToolTipContainer>
    );
  };

  renderForm = ({ values, fields, invalidFields }) => {
    const {
      internetAcquiringIsAvailable,
      withoutCheckIsAvailable
    } = this.props;
    return (
      <>
        <Box
          display="grid"
          gridTemplateColumns="1fr 24px 24px"
          gridColumnGap="8px"
          pb="8px"
          borderBottom="1px solid"
          borderColor="grayscale.1"
        >
          <FieldForm field={fields[billIdField.id]} />
          <StyledIcon
            icon="error_outline"
            data-tip="tooltip"
            data-for={`bill_info_${values.bill_id}`}
            data-place="right"
            data-effect="solid"
          />
          <Link to={getUrl("bills", { id: values.bill_id })}>
            <StyledIcon icon="edit" />
          </Link>
        </Box>
        <ReactTooltip id={`bill_info_${values.bill_id}`}>
          {this.renderBillInfo(values)}
        </ReactTooltip>
        <Box
          display="grid"
          gridRowGap="8px"
          py={16}
          borderBottom="1px solid"
          borderColor="grayscale.1"
        >
          <Box display="grid" gridTemplateColumns="1fr 1fr" alignItems="center">
            <FieldForm field={fields[paymentTypeField_1.id]} />
            <FieldForm field={fields[paymentTypeField_2.id]} />
          </Box>
          <Box display="grid" gridTemplateColumns="1fr 1fr" alignItems="center">
            <FieldForm field={fields[paymentTypeField_3.id]} />
            <FieldForm field={fields[paymentTypeField_4.id]} />
          </Box>
          {!!invalidFields.payment_type && (
            <Text fontSize="10px" color="#eb0028">
              Не выбран тип платежа
            </Text>
          )}
          <Box display="grid" gridTemplateColumns="35% 65%" alignItems="center">
            <Text>Отправить электронный чек на</Text>
            <Box
              display="grid"
              gridTemplateColumns="1fr 24px"
              alignItems="center"
              gridColumnGap="8px"
            >
              <FieldForm field={fields[receiptAddressField.id]} />
            </Box>
          </Box>
        </Box>
        <Box
          py="16px"
          borderBottom="1px solid"
          borderColor="grayscale.1"
          textAlign="center"
        >
          <Title>
            К оплате ИТОГО{" "}
            <Box display="inline-block" color="primary.0">
              {values.sum}
            </Box>{" "}
            руб.
          </Title>
        </Box>
        <Box display="grid" gridRowGap="10px" pt={16} pb={0}>
          <Box display="grid" gridTemplateColumns="35% 65%" alignItems="center">
            <Text>Сумма оплаты</Text>
            <FieldForm field={fields[sumField.id]} />
          </Box>
          <Box display="grid" gridTemplateColumns="35% 65%" alignItems="center">
            <Text>Сдача с</Text>
            <Box
              display="grid"
              gridTemplateColumns="1fr 1fr"
              gridColumnGap="8px"
            >
              <FieldForm field={fields[receiptField.id]} />
              <FieldForm field={fields[balanceField.id]} />
            </Box>
          </Box>
        </Box>
        <Box
          display="grid"
          gridTemplateColumns="1fr 1fr"
          gridColumnGap="16px"
          py={16}
          mb="16px"
        >
          <FieldForm field={fields[paymentMethodField_1.id]} />
          <FieldForm field={fields[paymentMethodField_2.id]} />
          {!!invalidFields.payment_method && (
            <Text fontSize="10px" color="#eb0028" mt="15px">
              Не выбран способ оплаты
            </Text>
          )}
        </Box>
        {internetAcquiringIsAvailable && values.payment_method === "2" && (
          <FieldForm field={fields[internetAcquiringField.id]} />
        )}
        {withoutCheckIsAvailable && (
          <FieldForm field={fields[withoutCheckField.id]} />
        )}
        <BtnBlock>
          <Button type="submit">Создать возврат</Button>
        </BtnBlock>
      </>
    );
  };

  mapValuesToFields = memoizeOne((bill, returnEntity, client, bills) => {
    const enumerations = {};
    const values = {
      bill,
      returnEntity,
      client,
      bill_id: "",
      sum: 0,
      receipt: 0,
      balance: 0,
      payment_state: 2,
      payment_place: 2
    };

    if (returnEntity) {
      enumerations.bills = {
        list: bills.entities.map(entity => ({
          id: entity.id,
          name: entity.code,
          entity
        }))
      };
      const selectedBill = bills.entities[0];
      values.agent_id = returnEntity.agent_id;
      values.order_id = returnEntity.id;
      values.contract_id = returnEntity.contract_id;

      if (selectedBill) {
        values.bill_id = selectedBill.id;
        values.sum = selectedBill.bill_sum;
        values.receipt = selectedBill.bill_sum;
        values.firm_id = selectedBill.firm_id;
        values.firm_name = selectedBill.firm_name;
      }
    } else if (bill) {
      enumerations.bills = {
        list: [
          {
            id: bill.id,
            name: bill.code,
            entity: bill
          }
        ]
      };
      values.agent_id = bill.agent_id;
      values.order_id = getValueInDepth(bill, ["return_id", 0]);
      values.contract_id = bill.contract_id;
      values.bill_id = bill.id;
      values.sum = bill.bill_sum;
      values.receipt = bill.bill_sum;
      values.firm_id = bill.firm_id;
      values.firm_name = bill.firm_name;
    }

    return {
      fields: fieldsForReturnPaymentForm.reduce(
        reduceValuesToFormFields(values, enumerations),
        []
      ),
      key: generateId()
    };
  });

  handleSubmitForm = values => {
    this.props.returnPaymentCommand(values, this.props.client);
  };

  render() {
    const { bill, returnEntity, client, bills, isLoading } = this.props;
    const { fields, key } = this.mapValuesToFields(
      bill,
      returnEntity,
      client,
      bills
    );
    return (
      <PreloaderContainer>
        <BlockContentForm>
          <ContainerContext
            key={key}
            fields={fields}
            renderForm={this.renderForm}
            onSubmitForm={this.handleSubmitForm}
            submitValidation
          />
          {isLoading && <PreLoader viewType="content" />}
        </BlockContentForm>
      </PreloaderContainer>
    );
  }
}

const moduleNames = [billsModuleName, commandsLoaderName];

function mapStateToProps(state, props) {
  return {
    internetAcquiringIsAvailable: hasPermissionSelector(state, {
      name: pm_payments_create_return_internet_acquiring
    }),
    withoutCheckIsAvailable: hasPermissionSelector(state, {
      name: pm_payments_create_without_check
    }),
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    bills: billsForEntitySelector(state, {
      idEntity: props.returnEntity ? props.returnEntity.id : null,
      typeEntity
    }),
    enumerations: enumerationsSelector(state, { enumerationNames })
  };
}

export default connect(
  mapStateToProps,
  { fetchBillsForEntity, returnPaymentCommand, fetchEnumerations }
)(ReturnPaymentForm);
