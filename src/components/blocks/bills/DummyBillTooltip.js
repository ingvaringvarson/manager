import React, { memo } from "react";
import PropTypes from "prop-types";
import ReactTooltip from "react-tooltip";
import Icon from "../../../designSystem/atoms/Icon";

const propTypes = {
  enumerations: PropTypes.object.isRequired,
  client: PropTypes.object.isRequired,
  contract: PropTypes.object
};

const DummyBillTooltip = memo(props => {
  const {
    enumerations,
    contract: { type, state, customer_name, executor_name }
  } = props;

  const contrType = enumerations.contractType.list.find(
    conType => conType.id === type
  );
  const contrState = enumerations.contractState.list.find(
    conState => conState.id === state
  );

  return (
    <div>
      <Icon data-tip="React-tooltip" icon="info" />
      <ReactTooltip place="right" type="info" effect="float">
        {!!contrType && <p>Тип договора: {contrType.name}</p>}
        {!!contrState && <p>Статус договора: {contrState.name}</p>}
        {!!customer_name && <p>Заказчик: {customer_name}</p>}
        {!!executor_name && <p>Исполнитель: {executor_name}</p>}
      </ReactTooltip>
    </div>
  );
});

DummyBillTooltip.propTypes = propTypes;

export default DummyBillTooltip;
