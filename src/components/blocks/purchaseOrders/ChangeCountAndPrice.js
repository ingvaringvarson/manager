import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import {
  postPurchaseOrderPositions,
  normalizePositionChangePriceAndCount
} from "../../../ducks/purchaseOrders";

import { reduceValuesToFormFields } from "../../../fields/helpers";
import { generateId } from "../../../utils";
import {
  tableFields,
  formFields
} from "../../../fields/formFields/purchaseOrders/fieldsForChangePositionsCountAndPrice";

import { ContainerContext, RenderContext } from "../../common/formControls";
import RightStickedButton from "../../common/buttons/RightStickedButton";
import ReactModal from "../../common/modals/CommonModal";
import RemoveableTable from "../../common/table/Removeable";

class ChangeCountAndPrice extends PureComponent {
  static propTypes = {
    positions: PropTypes.array.isRequired,
    handleClose: PropTypes.func.isRequired,
    purchaseOrder: PropTypes.object.isRequired,
    fetchEntities: PropTypes.func,

    postPurchaseOrderPositions: PropTypes.func.isRequired
  };

  static defaultProps = {
    fetchEntities: () => null
  };

  state = {
    removedIds: []
  };

  modalStyles = {
    width: 800
  };

  mapValuesToFields = memoizeOne(positions => {
    return {
      tableFields: tableFields,
      formFields: formFields.reduce(
        reduceValuesToFormFields({ fields: positions }),
        []
      ),
      key: generateId()
    };
  });

  handleFormSubmit = values => {
    this.props.postPurchaseOrderPositions(
      values,
      {
        positions: this.props.positions,
        removedIds: this.state.removedIds,
        purchaseOrder: this.props.purchaseOrder,
        fetchEntities: this.props.fetchEntities,
        successCallback: this.props.handleClose
      },
      normalizePositionChangePriceAndCount
    );
  };

  handleRemovedChange = removedIds => this.setState({ removedIds: removedIds });

  render() {
    const { positions, handleClose } = this.props;

    const { tableFields, formFields, key } = this.mapValuesToFields(positions);

    return (
      <ReactModal
        ariaHideApp={false}
        isOpen
        title="Изменить цену и количество"
        handleClose={handleClose}
        contentStyles={this.modalStyles}
      >
        <ContainerContext
          key={key}
          fields={formFields}
          onSubmitForm={this.handleFormSubmit}
        >
          <RemoveableTable
            cells={tableFields}
            entities={positions}
            removedIds={this.state.removedIds}
            handleRemovedChange={this.handleRemovedChange}
          />
          <RenderContext>
            {({ isFormValid }) => (
              <RightStickedButton
                type="submit"
                disabled={
                  this.state.removedIds.length === positions.length ||
                  !isFormValid
                }
              >
                СОХРАНИТЬ ИЗМЕНЕНИЯ
              </RightStickedButton>
            )}
          </RenderContext>
        </ContainerContext>
      </ReactModal>
    );
  }
}

export default connect(
  null,
  { postPurchaseOrderPositions }
)(ChangeCountAndPrice);
