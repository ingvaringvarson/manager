import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  changePurchaseOrderPositionsComingDate,
  normalizeChangeComingDate
} from "../../../ducks/purchaseOrders";
import memoizeOne from "memoize-one";

import { reduceValuesToFormFields } from "../../../fields/helpers";
import { generateId } from "../../../utils";
import {
  tableFields,
  formFields
} from "../../../fields/formFields/purchaseOrders/fieldsForCommandChangeDate";

import { ContainerContext } from "../../common/formControls";
import RightStickedButton from "../../common/buttons/RightStickedButton";
import ReactModal from "../../common/modals/CommonModal";
import RemoveableTable from "../../common/table/Removeable";

const propTypes = {
  positions: PropTypes.array.isRequired,
  handleClose: PropTypes.func.isRequired,
  purchaseOrder: PropTypes.object.isRequired,
  fetchEntities: PropTypes.func,

  changePurchaseOrderPositionsComingDate: PropTypes.func.isRequired
};

class ChangeComingDate extends PureComponent {
  state = {
    removedIds: []
  };

  modalStyles = {
    width: 800
  };

  mapValuesToFields = memoizeOne(positions => {
    return {
      tableFields: tableFields,
      formFields: formFields.reduce(
        reduceValuesToFormFields({ fields: positions }),
        []
      ),
      key: generateId()
    };
  });

  handleFormSubmit = values => {
    this.props.changePurchaseOrderPositionsComingDate(
      values,
      {
        positions: this.props.positions,
        removedIds: this.state.removedIds,
        purchaseOrder: this.props.purchaseOrder,
        fetchEntities: this.props.fetchEntities,
        successCallback: this.props.handleClose.bind(this)
      },
      normalizeChangeComingDate
    );
  };

  handleRemovedChange = removedIds => this.setState({ removedIds: removedIds });

  render() {
    const { positions, handleClose } = this.props;

    const { tableFields, formFields, key } = this.mapValuesToFields(
      positions,
      this.state.removedIds
    );

    return (
      <ReactModal
        isOpen
        title="Изменить дату прихода позиции"
        handleClose={handleClose}
        contentStyles={this.modalStyles}
      >
        <ContainerContext
          key={key}
          fields={formFields}
          onSubmitForm={this.handleFormSubmit}
        >
          <RemoveableTable
            cells={tableFields}
            entities={positions}
            removedIds={this.state.removedIds}
            handleRemovedChange={this.handleRemovedChange}
          />
          <RightStickedButton
            type="submit"
            disabled={this.state.removedIds.length === positions.length}
          >
            СОХРАНИТЬ ИЗМЕНЕНИЯ
          </RightStickedButton>
        </ContainerContext>
      </ReactModal>
    );
  }
}

ChangeComingDate.propTypes = propTypes;

export default connect(
  null,
  { changePurchaseOrderPositionsComingDate }
)(ChangeComingDate);
