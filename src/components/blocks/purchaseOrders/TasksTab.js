import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import {
  fetchTasksForEntity,
  tasksForEntitySelector,
  requestParamsForTasksEntitySelector
} from "../../../ducks/tasks";
import { keyRouterSelector } from "../../../redux/selectors/router";
import { TableContainer } from "../../common/tableControls";
import { ContainerContext } from "../../common/formControls";
import Pagination from "../Pagination";
import Table from "../../common/table/Table";
import { generateUrlWithQuery, generateId } from "../../../utils";
import { reduceValuesToFormFields } from "../../../fields/helpers";
import { fields } from "../../../fields/formFields/purchaseOrders/fieldsOfTasksTable";

const typeEntity = "purchaseOrder";

class TasksTab extends PureComponent {
  static propTypes = {
    keyRouter: PropTypes.string.isRequired,
    purchaseOrder: PropTypes.object,
    requestParams: PropTypes.object,
    enumerations: PropTypes.object,
    pathname: PropTypes.string,
    purchaseOrderId: PropTypes.string,
    tasks: PropTypes.array
  };

  componentDidMount = () => {
    this.props.fetchTasksForEntity(
      typeEntity,
      this.props.purchaseOrderId,
      this.props.requestParams
    );
  };

  componentDidUpdate = prevProps => {
    if (prevProps.keyRouter !== this.props.keyRouter) {
      this.props.fetchTasksForEntity(
        typeEntity,
        this.props.purchaseOrderId,
        this.props.requestParams
      );
    }
  };

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  renderCells = memoizeOne((tasks, enumerations, requestParams) => {
    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: generateId()
    };
  });

  render() {
    const { cells, key } = this.renderCells(
      this.props.tasks,
      this.props.enumerations,
      this.props.requestParams
    );

    return (
      <>
        <ContainerContext fields={cells} onSort={this.handleSort} key={key}>
          <TableContainer canBeSelected entities={this.props.tasks}>
            <Table
              cells={cells}
              enumerations={this.props.enumerations}
              entities={this.props.tasks}
            />
          </TableContainer>
        </ContainerContext>
        <Pagination
          requestParams={this.props.requestParams}
          pathname={this.props.pathname}
          push={this.props.push}
          page={+this.props.requestParams.page}
          maxPages={this.props.maxPages || 99}
        />
      </>
    );
  }
}

export default connect(
  (state, props) => {
    const purchaseOrderId =
      props.purchaseOrder &&
      props.purchaseOrder.order &&
      props.purchaseOrder.order.id;

    const tasksForEntity = tasksForEntitySelector(state, {
      idEntity: purchaseOrderId,
      typeEntity
    });

    return {
      tasks: tasksForEntity.entities,
      maxPages: tasksForEntity.maxPages,
      purchaseOrderId,
      keyRouter: keyRouterSelector(state),
      requestParams: requestParamsForTasksEntitySelector(state, {
        typeEntity: "purchase_order_id",
        idEntity: props.purchaseOrder.order.id
      })
    };
  },
  { push, fetchTasksForEntity }
)(TasksTab);
