import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";

import FieldForm from "../../common/form/FieldForm";
import RightStickedButton from "../../common/buttons/RightStickedButton";
import ReactModal from "../../common/modals/CommonModal";
import { ContainerContext, FormContext } from "../../common/formControls";

import { fields } from "../../../fields/formFields/purchaseOrders/fieldsForChangeDeliveryType";

import { changePODeliveryType } from "../../../ducks/purchaseOrders";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../ducks/enumerations";

import { reduceValuesToFormFields } from "../../../fields/helpers";
import { generateId } from "../../../utils";

const enumerationNames = ["pricelistDeliveryType"];

class ChangeDeliveryType extends PureComponent {
  static propTypes = {
    purchaseOrder: PropTypes.object.isRequired,
    handleClose: PropTypes.func.isRequired,
    fetchEntities: PropTypes.func.isRequired,

    enumerations: PropTypes.object.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    changePODeliveryType: PropTypes.func.isRequired
  };

  modalStyles = {
    width: 400
  };

  getMaxPositionsEstimatedDate = memoizeOne(purchaseOrder => {
    return purchaseOrder.order.positions.reduce((result, pos) => {
      if (result < pos.estimated_time) return pos.estimated_time;
      return result;
    }, purchaseOrder.order.positions[0].estimated_time);
  });

  successCallback = () => {
    this.props.fetchEntities();
    this.props.handleClose();
  };

  handleFormSubmit = values => {
    const { purchaseOrder } = this.props;

    const maxEstimatedTime = this.getMaxPositionsEstimatedDate(
      this.props.purchaseOrder
    );

    this.props.changePODeliveryType(values, {
      purchaseOrder,
      maxEstimatedTime,
      successCallback: this.successCallback
    });
  };

  renderCells = memoizeOne((values, enumerations) => {
    return {
      formFields: fields.reduce(
        reduceValuesToFormFields(values, enumerations),
        []
      ),
      key: generateId()
    };
  });

  render() {
    const { handleClose, enumerations, purchaseOrder } = this.props;
    const { formFields, key } = this.renderCells(purchaseOrder, enumerations);

    return (
      <ReactModal
        isOpen
        title="Редактировать"
        subtitle="Способ получения"
        handleClose={handleClose}
        contentStyles={this.modalStyles}
      >
        <ContainerContext
          key={key}
          fields={formFields}
          onSubmitForm={this.handleFormSubmit}
          externalForm
        >
          <FormContext>
            {formFields.map((field, index) => (
              <FieldForm field={field} key={index} index={index} />
            ))}
            <RightStickedButton type="submit">ИЗМЕНИТЬ</RightStickedButton>
          </FormContext>
        </ContainerContext>
      </ReactModal>
    );
  }
}

export default connect(
  state => {
    return {
      enumerations: enumerationsSelector(state, { enumerationNames })
    };
  },
  { fetchEnumerations, changePODeliveryType }
)(ChangeDeliveryType);
