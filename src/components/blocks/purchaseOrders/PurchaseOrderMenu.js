import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import CommandsByStatusMenu from "../../common/menus/CommandsByStatusMenu";
import CommandOrderedFromProvider from "./CommandOrderedFromProvider";
import CreatePurchaseReturn from "../purchaseReturns/CreatePurchaseReturn";
import ChangeDeliveryType from "./ChangeDeliveryType";
import CommandReject from "../positions/RejectModal";
import ChangeCountAndPrice from "./ChangeCountAndPrice";
import DocumentPrint from "../DocumentPrint/DocumentPrint";

import purchaseOrderCommands, {
  filterPurchaseOrdersCommands,
  getMainCommandByPurchaseOrder,
  filters
} from "../../../commands/purchaseOrders";

import documentObjectTypeId from "../../../constants/documentObjectTypeId";
import { typeMethods } from "../../../ducks/positions";

export default class PurchaseOrderMenu extends PureComponent {
  static propTypes = {
    purchaseOrders: PropTypes.arrayOf(PropTypes.object).isRequired,
    forList: PropTypes.bool,
    useMainCommand: PropTypes.bool,

    fetchEntities: PropTypes.func
  };

  static defaultProps = {
    forList: false,
    useMainCommand: false,
    fetchEntities: () => null
  };

  modalIsNotImplementedFunc = (_, command) =>
    alert(`Функциональность ${command.title} пока не добавлена`);

  getBasePositionsIds = memoizeOne(purchaseOrder =>
    purchaseOrder.order.positions.map(p => p._id)
  );

  // ToDo: сделать общую функцию selectMany через getValueInDepth
  getAllPurchaseOrdersPositions = memoizeOne(purchaseOrders => {
    return purchaseOrders.reduce((result, purchaseOrder) => {
      const { positions } = purchaseOrder.order;
      positions.forEach(pos => result.push(pos));
      return result;
    }, []);
  });

  commands = [
    {
      ...purchaseOrderCommands.orderedFromSupplierCommand,
      renderModal: (handleClose, [purchaseOrder]) => (
        <CommandOrderedFromProvider
          purchaseOrder={purchaseOrder}
          fetchEntities={this.props.fetchEntities}
          handleClose={handleClose}
        />
      )
    },
    {
      ...purchaseOrderCommands.takeProductCommand,
      onClick: this.modalIsNotImplementedFunc
    },
    {
      ...purchaseOrderCommands.rejectCommand,
      renderModal: (handleClose, purchaseOrders) => (
        <CommandReject
          positions={this.getAllPurchaseOrdersPositions(purchaseOrders)}
          handleClose={handleClose}
          entities={purchaseOrders}
          typeMethod={typeMethods.purchaseOrders}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...purchaseOrderCommands.changeDeliveryTypeCommand,
      renderModal: (handleClose, [purchaseOrder]) => (
        <ChangeDeliveryType
          purchaseOrder={purchaseOrder}
          handleClose={handleClose}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...purchaseOrderCommands.changePriceAndCountCommand,
      renderModal: (handleClose, [purchaseOrder]) => (
        <ChangeCountAndPrice
          positions={filters.changePriceAndCountPositionsFilter(
            purchaseOrder.order.positions
          )}
          handleClose={handleClose}
          purchaseOrder={purchaseOrder}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...purchaseOrderCommands.printDocumentsCommand,
      renderModal: (handleClose, [purchaseOrder]) => (
        <DocumentPrint
          idEntity={purchaseOrder.order.id}
          objectTypeId={documentObjectTypeId.purchase_orders}
          entityFirmId={purchaseOrder.order.firm_id}
          handleClose={handleClose}
        />
      )
    },
    {
      ...purchaseOrderCommands.createPurchaseReturnCommand,
      renderModal: (handleClose, purchaseOrders) => (
        <CreatePurchaseReturn
          forMulti={this.props.forList}
          selectedIds={this.getBasePositionsIds(purchaseOrders[0])}
          entities={purchaseOrders}
          fetchEntities={this.props.fetchEntities}
          handleClose={handleClose}
          positions={this.getAllPurchaseOrdersPositions(purchaseOrders)}
        />
      )
    }
  ];

  getMainCommand = memoizeOne(([purchaseOrder]) =>
    getMainCommandByPurchaseOrder(purchaseOrder.order)
  );

  render() {
    const { purchaseOrders, forList, useMainCommand } = this.props;
    const mainCommandId = useMainCommand
      ? this.getMainCommand(purchaseOrders)
      : null;
    return (
      <CommandsByStatusMenu
        forList={forList}
        entityOrEntities={purchaseOrders}
        commands={this.commands}
        mainCommandId={mainCommandId}
        commandsToMenuOptionsFilter={filterPurchaseOrdersCommands}
      />
    );
  }
}
