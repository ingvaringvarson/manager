import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import DividingModal from "../positions/DividingModal";
import ChangeCountAndPrice from "../purchaseOrders/ChangeCountAndPrice";
import CommandReject from "../positions/RejectModal";
import ChangeComingDate from "../purchaseOrders/ChangeComingDate";
import CreatePurchaseReturn from "../purchaseReturns/CreatePurchaseReturn";
import CommandsByStatusMenu from "../../common/menus/CommandsByStatusMenu";

import purchaseOrderPositionsCommands, {
  filterPurchaseOrderPositionsCommands
} from "../../../commands/purchaseOrderPositions";

import { memoizeToArray } from "../../../helpers/cache";
import { typeMethods } from "../../../ducks/positions";

export default class PurchaseOrderPositionsMenu extends PureComponent {
  static propTypes = {
    forList: PropTypes.bool,
    positions: PropTypes.array.isRequired,
    purchaseOrder: PropTypes.object.isRequired,
    fetchEntities: PropTypes.func.isRequired
  };

  static defaultProps = {
    forList: false
  };

  getBasePositionsIds = memoizeOne(positions => positions.map(p => p._id));

  commands = [
    {
      ...purchaseOrderPositionsCommands.changeComingDateCommand,
      renderModal: (handleClose, positions, purchaseOrder) => (
        <ChangeComingDate
          positions={positions}
          handleClose={handleClose}
          purchaseOrder={purchaseOrder}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...purchaseOrderPositionsCommands.rejectByProductCommand,
      renderModal: (handleClose, positions, purchaseOrder) => (
        <CommandReject
          positions={positions}
          handleClose={handleClose}
          typeMethod={typeMethods.purchaseOrders}
          entities={memoizeToArray(purchaseOrder)}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...purchaseOrderPositionsCommands.divideCommand,
      renderModal: (handleClose, positions, purchaseOrder) => (
        <DividingModal
          typeMethod={typeMethods.purchaseOrders}
          positions={positions}
          handleClose={handleClose}
          entity={purchaseOrder}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...purchaseOrderPositionsCommands.changePriceAndCountCommand,
      renderModal: (handleClose, positions, purchaseOrder) => (
        <ChangeCountAndPrice
          positions={positions}
          handleClose={handleClose}
          purchaseOrder={purchaseOrder}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...purchaseOrderPositionsCommands.createPurchaseReturnCommand,
      renderModal: (handleClose, positions, purchaseOrder) => (
        <CreatePurchaseReturn
          selectedIds={this.getBasePositionsIds(positions)}
          entities={memoizeToArray(purchaseOrder)}
          fetchEntities={this.props.fetchEntities}
          handleClose={handleClose}
          positions={positions}
        />
      )
    }
  ];

  render() {
    const { positions, purchaseOrder, forList } = this.props;
    return (
      <CommandsByStatusMenu
        forList={forList}
        entityOrEntities={positions}
        parentEntity={purchaseOrder}
        commands={this.commands}
        commandsToMenuOptionsFilter={filterPurchaseOrderPositionsCommands}
      />
    );
  }
}
