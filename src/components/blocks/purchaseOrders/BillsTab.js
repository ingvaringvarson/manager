import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import styled from "styled-components";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import {
  billsForEntityTabParamsSelector,
  billsForEntitySelector,
  fetchBillsForEntity,
  requestParamsForBillEntitySelector
} from "../../../ducks/bills";
import { queryRouterSelector } from "../../../redux/selectors/router";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";
import { generateUrlWithQuery } from "../../../utils";
import Table from "../../common/tableView/Table";
import TableWithLinks from "../../common/table/TableWithLinks";
import { fields } from "../../../fields/formFields/orders/fieldsForOrderBills";
import { ContainerContext } from "../../common/formControls";
import {
  TableContainer,
  ToggleOpenRow,
  handleStopLifting
} from "../../common/tableControls";
import Container from "../../../designSystem/templates/Container";
import Row from "../../../designSystem/templates/Row";
import Column from "../../../designSystem/templates/Column";
import Pagination from "../Pagination";
import TableCell from "../../../designSystem/molecules/TableCell";

import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../../fields/helpers";
import { generateId } from "../../../utils";
import { billPositionFields } from "../../../fields/formFields/returns/fieldsForBillsInner";
import BillMenu from "../bills/BillMenu";
import PaymentsForBill from "../payments/PaymentsForBill";

const Wrapper = styled.div`
  padding: 10px;
`;
const subTableParams = {
  subTable: true,
  ml: "80px",
  mt: "8px",
  mb: "16px"
};

const typeEntity = "purchaseOrders";

const propTypes = {
  selectedTabState: PropTypes.number,
  purchaseOrder: PropTypes.object,
  enumerations: PropTypes.object,
  pathname: PropTypes.string,
  keyRouter: PropTypes.string,
  requestParams: PropTypes.object,
  client: PropTypes.object
};

class BillsTab extends PureComponent {
  componentDidMount() {
    this.props.fetchBillsForEntity(
      typeEntity,
      this.props.idEntity,
      this.props.requestParams
    );
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.props.fetchBillsForEntity(
        typeEntity,
        this.props.idEntity,
        this.props.requestParams
      );
    }
  }

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleChangeTab = id => {
    const { pathname, query } = this.props;
    const queryForRootTab = `?_tab=${query._tab || 1}`;
    const queryForTab = id === 1 ? "" : `&_tab_state_bill=${id}`;
    this.props.push(`${pathname}${queryForRootTab}${queryForTab}`);
  };

  renderCells = memoizeOne(enumerations => {
    const { requestParams } = this.props;

    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: generateId()
    };
  });

  renderOptionControl = ({ entity }) => {
    return (
      <TableCell control onClick={handleStopLifting}>
        <BillMenu bills={[entity]} client={this.props.client} key={entity.id} />
      </TableCell>
    );
  };

  renderNestedTable = ({ entity, ...restData }) => {
    const { enumerations } = this.props;
    const { positions } = entity;

    return (
      <ToggleOpenRow id={entity._id}>
        <Container>
          <Row>
            {positions && Array.isArray(positions) && positions.length && (
              <Column basis="50%">
                <TableContainer>
                  <Table
                    tableProps={subTableParams}
                    entities={positions}
                    cells={billPositionFields}
                    enumerations={enumerations}
                  />
                </TableContainer>
              </Column>
            )}
            <Column basis="50%">
              <PaymentsForBill payments={entity.payments_inf} />
            </Column>
          </Row>
        </Container>
      </ToggleOpenRow>
    );
  };

  renderTable = renderCallBacks => {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.bills
    );

    return (
      <TableWithLinks
        key={key}
        cells={cells}
        entities={this.props.bills.entities}
        enumerations={this.props.enumerations}
        hasFormControl
        hasChildrenBefore
        hasChildrenAfter
        renderRowHeadChildrenBefore={
          renderCallBacks.renderRowHeadChildrenBefore
        }
        renderRowBodyChildrenBefore={
          renderCallBacks.renderRowBodyChildrenBefore
        }
        renderRowFormControlChildrenBefore={
          renderCallBacks.renderRowFormControlChildrenBefore
        }
        renderRowBodyAfter={this.renderNestedTable}
        renderRowBodyChildrenAfter={this.renderOptionControl}
        typeEntity="bill"
      />
    );
  };

  render() {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.bills
    );

    return (
      <Wrapper>
        <TabContainer
          selectedTabId={this.props.selectedTabState}
          onChangeSelected={this.handleChangeTab}
        >
          <TabHeadContainer>
            <TabHead id={1}>Активные</TabHead>
            <TabHead id={2}>Аннулированные</TabHead>
          </TabHeadContainer>
          <TabBodyContainer>
            <TabBody id={1}>
              <ContainerContext
                key={key}
                onSort={this.handleSort}
                fields={cells}
                onSubmitForm={this.handleSubmitForm}
                autoSubmit
              >
                <TableContainer
                  canBeSelected
                  hasNestedTable
                  entities={this.props.bills.entities}
                  renderTable={this.renderTable}
                />
              </ContainerContext>
            </TabBody>
            <TabBody id={2}>
              <ContainerContext
                key={key}
                onSort={this.handleSort}
                fields={cells}
                onSubmitForm={this.handleSubmitForm}
                autoSubmit
              >
                <TableContainer
                  entities={this.props.bills.entities}
                  canBeSelected
                  hasNestedTable
                  renderTable={this.renderTable}
                />
              </ContainerContext>
            </TabBody>
            <Pagination
              requestParams={this.props.requestParams}
              pathname={this.props.pathname}
              push={this.props.push}
              page={+this.props.requestParams.page}
              maxPages={this.props.bills.maxPages || 99}
            />
          </TabBodyContainer>
        </TabContainer>
      </Wrapper>
    );
  }
}

BillsTab.propTypes = propTypes;

export default connect(
  (state, props) => {
    const { purchaseOrder } = props;
    const selectedTabState = billsForEntityTabParamsSelector(state)
      ._tab_state_bill;
    const idEntity = `${purchaseOrder.order.id}_${selectedTabState}`;
    return {
      requestParams: requestParamsForBillEntitySelector(state, {
        typeEntity: "purchase_order_id",
        idEntity: props.purchaseOrder.order.id
      }),
      selectedTabState,
      idEntity,
      bills: billsForEntitySelector(state, { typeEntity, idEntity }),
      query: queryRouterSelector(state)
    };
  },
  { push, fetchBillsForEntity }
)(BillsTab);
