import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import InfoBlock from "../../common/info/InfoBlock";
import InfoModalControl from "../../common/info/InfoModalControl";

import { fieldGroups } from "../../../fields/formFields/purchaseOrders/fieldsForInfoBlocks";
import { routesToMove } from "../../../helpers/routesGenerate";

class PurchaseOrderInfoBlocks extends PureComponent {
  static propTypes = {
    purchaseOrder: PropTypes.object,
    client: PropTypes.object,
    enumerations: PropTypes.object,
    onSubmitClientForm: PropTypes.func.isRequired
  };

  handleMoveToClientPage = () => {
    const { client } = this.props;
    if (client) {
      window.open(routesToMove.client(client), "_blank");
    }
  };

  iconProps = {
    onClick: this.handleMoveToClientPage
  };

  renderHelperValues = memoizeOne((client, purchaseOrder, enumerations) => {
    return {
      client,
      purchaseOrder,
      enumerations
    };
  });

  renderContent = info => (
    state = {},
    modalProps = null,
    blockProps = null,
    iconProps = null
  ) => {
    const { client, purchaseOrder, enumerations } = this.props;
    const restHelperValues = this.renderHelperValues(
      client,
      purchaseOrder,
      enumerations
    );

    return (
      <InfoBlock
        iconIsShow={state.iconIsShow}
        info={info}
        blockProps={blockProps}
        iconProps={iconProps}
        restHelperValues={restHelperValues}
      />
    );
  };

  getBlock = (entity, enumerations, block) => {
    const info = block.mapValuesToFields(block, enumerations, entity);
    return {
      ...info,
      renderContent: this.renderContent(info)
    };
  };

  getPurchaseOrderBlock = memoizeOne((client, purchaseOrder, enumerations) => {
    const { delivery_type } = purchaseOrder.order;
    let block = {};

    switch (delivery_type) {
      case 1:
        block = this.getBlock(
          { ...purchaseOrder.order, client },
          enumerations,
          fieldGroups["delivery_1"]
        );
        break;
      case 2:
        block = this.getBlock(
          { ...purchaseOrder.order, client },
          enumerations,
          fieldGroups["delivery_2"]
        );
        break;
      default:
        return null;
    }

    return block;
  });

  getClientBlocks = memoizeOne((client, purchaseOrder, enumerations) => {
    const blocks = [];

    if (client.type === 1) {
      blocks.push(
        this.getBlock(
          { client, purchaseOrder },
          enumerations,
          fieldGroups["client"]
        )
      );
    } else if (client.type === 2) {
      blocks.push(
        this.getBlock(
          { client, purchaseOrder },
          enumerations,
          fieldGroups["company"]
        )
      );
    }

    return blocks.concat(
      this.getBlock(client["contacts"], enumerations, fieldGroups["contacts"]),
      this.getBlock(
        client["contact_persons"],
        enumerations,
        fieldGroups["contact_persons"]
      )
    );
  });

  getBlockProps = memoizeOne(onSubmitForm => ({
    onSubmitForm
  }));

  render() {
    const {
      purchaseOrder,
      enumerations,
      client,
      onSubmitClientForm
    } = this.props;

    if (!purchaseOrder) {
      return null;
    }

    const purchaseOrderBlock = purchaseOrder
      ? this.getPurchaseOrderBlock(client, purchaseOrder, enumerations)
      : null;

    const clientBlocks = client
      ? this.getClientBlocks(client, purchaseOrder, enumerations)
      : null;

    const blockProps = this.getBlockProps(onSubmitClientForm);

    return (
      <>
        {!!purchaseOrderBlock && (
          <InfoModalControl
            formProps={purchaseOrderBlock.formMain}
            fields={purchaseOrderBlock.formFields}
            // ToDo: здесь скорее всего ошибка. Починить!
            onSubmitForm={onSubmitClientForm}
          >
            {purchaseOrderBlock.renderContent}
          </InfoModalControl>
        )}
        {!!clientBlocks &&
          clientBlocks.map(info => {
            if (!info.formMain || (info.formMain && !info.formMain.type)) {
              return (
                <InfoBlock
                  key={info.title}
                  iconProps={this.iconProps}
                  iconIsShow
                  blockProps={blockProps}
                  info={info}
                />
              );
            }
            const fields =
              info.formMain.type === "edit"
                ? info.formFields
                : info.fieldsForAdd;

            return (
              <InfoModalControl
                key={info.title}
                formProps={info.formMain}
                onSubmitForm={onSubmitClientForm}
                fields={fields}
              >
                {info.renderContent}
              </InfoModalControl>
            );
          })}
      </>
    );
  }
}

export default PurchaseOrderInfoBlocks;
