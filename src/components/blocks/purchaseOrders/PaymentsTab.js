import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import { connect } from "react-redux";
import { push } from "connected-react-router";
import {
  paymentsForEntitySelector,
  fetchPaymentsForEntity,
  requestParamsForPaymentsTabPageSelector
} from "../../../ducks/payments";
import { enumerationListWithoutZeroIdSelector } from "../../../ducks/enumerations";
import { queryRouterSelector } from "../../../redux/selectors/router";

import { fieldsForPaymentMethods } from "../../../fields/formFields/orders/fieldsForOrderPayments";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../../fields/helpers";

import { generateUrlWithQuery, generateId } from "../../../utils";
import { ContainerContext } from "../../common/formControls";
import { TableContainer } from "../../common/tableControls";
import TableWithLinks from "../../common/table/TableWithLinks";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";

import Block from "../../../designSystem/organisms/Block";
import BlockContent from "../../../designSystem/organisms/BlockContent";

import Pagination from "../Pagination";
import { getUrl } from "../../../helpers/urls";

const typeEntity = "purchaseOrders";

const propTypes = {
  purchaseOrder: PropTypes.object,
  enumerations: PropTypes.object,
  pathname: PropTypes.string,
  keyRouter: PropTypes.string,
  requestParams: PropTypes.object,
  paymentMethods: PropTypes.string,
  fields: PropTypes.array,
  payments: PropTypes.array
};

class PaymentsTab extends PureComponent {
  componentDidMount = () => {
    this.props.fetchPaymentsForEntity(
      typeEntity,
      this.props.idEntity,
      this.props.requestParams
    );
  };

  componentDidUpdate = prevProps => {
    if (prevProps.keyRouter !== this.props.keyRouter) {
      this.props.fetchPaymentsForEntity(
        typeEntity,
        this.props.idEntity,
        this.props.requestParams
      );
    }
  };

  renderCells = memoizeOne(enumerations => {
    const { fields, requestParams } = this.props;

    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: generateId()
    };
  });

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleChangeTab = id => {
    const { pathname, query } = this.props;
    const queryForRootTab = `?_tab=${query._tab || 1}`;
    const queryForTab = id === 1 ? "" : `&payment_method=${id}`;
    this.props.push(`${pathname}${queryForRootTab}${queryForTab}`);
  };

  handleMoveToPaymentPage = ({ entity }) => () => {
    if (entity) this.props.push(getUrl("payments", { id: entity.id }));
  };

  rowBodyHandlers = {
    onClick: this.handleMoveToPaymentPage
  };

  renderTable = renderCallBacks => {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.payments
    );

    return (
      <TableWithLinks
        key={key}
        cells={cells}
        entities={this.props.payments.entities}
        enumerations={this.props.enumerations}
        hasFormControl
        hasChildrenBefore
        renderRowHeadChildrenBefore={
          renderCallBacks.renderRowHeadChildrenBefore
        }
        renderRowBodyChildrenBefore={
          renderCallBacks.renderRowBodyChildrenBefore
        }
        renderRowFormControlChildrenBefore={
          renderCallBacks.renderRowFormControlChildrenBefore
        }
        rowBodyHandlers={this.rowBodyHandlers}
        typeEntity="payment"
      />
    );
  };

  render() {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.payments
    );

    return (
      <>
        <Block mb="middle">
          <BlockContent>
            <TabContainer
              selectedTabId={this.props.requestParams.payment_method}
              onChangeSelected={this.handleChangeTab}
            >
              <TabHeadContainer>
                {this.props.paymentMethods.map(item => {
                  return <TabHead id={item.id}>{item.name}</TabHead>;
                })}
              </TabHeadContainer>
              <TabBodyContainer>
                {this.props.paymentMethods.map(item => {
                  return (
                    <TabBody id={item.id}>
                      <ContainerContext
                        key={key}
                        onSort={this.handleSort}
                        fields={cells}
                        onSubmitForm={this.handleSubmitForm}
                        autoSubmit
                      >
                        <TableContainer
                          entities={this.props.payments.entities}
                          canBeSelected
                          renderTable={this.renderTable}
                        />
                      </ContainerContext>
                      <Pagination
                        requestParams={this.props.requestParams}
                        pathname={this.props.pathname}
                        push={this.props.push}
                        page={+this.props.requestParams.page}
                        maxPages={this.props.payments.maxPages || 99}
                      />
                    </TabBody>
                  );
                })}
              </TabBodyContainer>
            </TabContainer>
          </BlockContent>
        </Block>
      </>
    );
  }
}

PaymentsTab.propTypes = propTypes;

export default connect(
  (state, props) => {
    const requestParams = requestParamsForPaymentsTabPageSelector(state, {
      typeEntity: "purchase_order_id",
      idEntity: props.purchaseOrder.order.id
    });
    const idEntity = `${props.purchaseOrder.order.id}_${requestParams.payment_method}`;
    return {
      idEntity,
      requestParams,
      payments: paymentsForEntitySelector(state, { idEntity, typeEntity }),
      paymentMethods: enumerationListWithoutZeroIdSelector(state, {
        name: "paymentMethod"
      }),
      fields: fieldsForPaymentMethods[requestParams.payment_method],
      query: queryRouterSelector(state)
    };
  },
  { push, fetchPaymentsForEntity }
)(PaymentsTab);
