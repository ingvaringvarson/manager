import React, { memo } from "react";
import PropTypes from "prop-types";

import Row from "../../../designSystem/templates/Row";
import Column from "../../../designSystem/templates/Column";
import HeadingCard from "../../common/headlines/HeadingCard";
import HeadingInfo from "../../common/headlines/HeadingInfo";
import PurchaseOrderMenu from "./PurchaseOrderMenu";

import { memoizeToArray } from "../../../helpers/cache";

const propTypes = {
  purchaseOrder: PropTypes.object,
  enumerations: PropTypes.object
};

const renderPurchaseOrderStatus = (purchaseOrder, enumerations) => {
  const state = purchaseOrder.order && purchaseOrder.order.state_supplier_order;
  const stateEnumMap =
    enumerations &&
    enumerations.orderSupplierState &&
    enumerations.orderSupplierState.map;
  if ((!state && state !== 0) || !stateEnumMap || !stateEnumMap[state])
    return null;

  let color = null;
  /*switch (stateEnumMap[state].id) {
    case 1:
      color = '#ccc500';
      break;
    case 1:
      color = '#ccc500';
      break;
    case 1:
      color = '#ccc500';
      break;
    case 1:
      color = '#ccc500';
      break;
    case 1:
      color = '#ccc500';
      break;
    default:
      break;
  }*/
  return {
    ...stateEnumMap[state],
    color
  };
};

const HeaderPurchaseOrder = memo(props => {
  const { purchaseOrder, enumerations, fetchEntities } = props;
  const state = renderPurchaseOrderStatus(purchaseOrder, enumerations);
  const sum_payment =
    purchaseOrder.order && purchaseOrder.order.sum_payment
      ? `${purchaseOrder.order.sum_payment} руб.`
      : "";
  const sum_total =
    purchaseOrder.order && purchaseOrder.order.sum_total
      ? `${purchaseOrder.order.sum_total} руб.`
      : "";

  return (
    <Row alignCenter justifyBetween mb="20px">
      <Column>
        <HeadingCard
          title="Карточка заказа поставщика"
          name={`id ${purchaseOrder.order.code}`}
          status={state && state.name}
          statusColor={state && state.color}
        />
      </Column>
      <Column>
        <Row>
          <Column mr="30px">
            <HeadingInfo subTitle="Сумма всего" info={sum_total} />
          </Column>
          <Column mr="30px">
            <HeadingInfo subTitle="Сумма к оплате" info={sum_payment} />
          </Column>
        </Row>
      </Column>
      <Column>
        <PurchaseOrderMenu
          purchaseOrders={memoizeToArray(purchaseOrder)}
          useMainCommand
          fetchEntities={fetchEntities}
        />
      </Column>
    </Row>
  );
});

HeaderPurchaseOrder.propTypes = propTypes;

export default HeaderPurchaseOrder;
