import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchPurchaseOrder } from "../../../ducks/purchaseOrders";
import { fields } from "../../../fields/formFields/purchaseOrders/fieldsForPositions";

import PurchaseOrderPositionsMenu from "./PurchaseOrderPositionsMenu";
import NestedTableForEntity from "../positions/NestedTableForEntity";

const propTypes = {
  purchaseOrder: PropTypes.object,
  enumerations: PropTypes.object
};

class PositionsTable extends PureComponent {
  fetchEntities = () => {
    const { id } = this.props.purchaseOrder.order;
    this.props.fetchPurchaseOrder(id, { id, isForRepeat: true });
  };

  renderPositionsMenu = ({ forList, positions }) => (
    <PurchaseOrderPositionsMenu
      forList={forList}
      positions={positions}
      purchaseOrder={this.props.purchaseOrder}
      fetchEntities={this.fetchEntities}
    />
  );

  render() {
    return (
      <NestedTableForEntity
        key={this.props.purchaseOrder._id}
        renderPositionsMenu={this.renderPositionsMenu}
        entity={this.props.purchaseOrder}
        positions={this.props.purchaseOrder.order.positions}
        enumerations={this.props.enumerations}
        tableParams={null}
        fields={fields}
        isNested={false}
      />
    );
  }
}

PositionsTable.propTypes = propTypes;

export default connect(
  null,
  { fetchPurchaseOrder }
)(PositionsTable);
