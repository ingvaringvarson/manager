import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { changeStateToOrderedFromProvider } from "../../../ducks/purchaseOrders";
import memoizeOne from "memoize-one";

import { reduceValuesToFormFields } from "../../../fields/helpers";
import { generateId } from "../../../utils";
import {
  tableFields,
  formFields
} from "../../../fields/formFields/purchaseOrders/fieldsForCommandChangeOrdered";

import { ContainerContext } from "../../common/formControls";
import RightStickedButton from "../../common/buttons/RightStickedButton";
import ReactModal from "../../common/modals/CommonModal";
import { Table } from "../../common/tableView";

class CommandOrderedFromProvider extends PureComponent {
  static propTypes = {
    purchaseOrder: PropTypes.object.isRequired,
    fetchEntities: PropTypes.func.isRequired,
    handleClose: PropTypes.func.isRequired,

    changeStateToOrderedFromProvider: PropTypes.func.isRequired,
    hasPermission: PropTypes.bool.isRequired
  };

  state = {
    removedIds: []
  };

  modalStyles = {
    width: 800
  };

  mapValuesToFields = memoizeOne(positions => {
    return {
      tableFields: tableFields,
      formFields: formFields.reduce(
        reduceValuesToFormFields({
          fields: positions.map(position => ({
            ...position,
            hasPermission: this.props.hasPermission
          }))
        }),
        []
      ),
      key: generateId()
    };
  });

  successCallback = () => {
    this.props.fetchEntities();
    this.props.handleClose();
  };

  handleFormSubmit = values => {
    this.props.changeStateToOrderedFromProvider(values, {
      //никак не учитываются удалённые позиции далее:(
      //positions: this.props.positions,
      //removedIds: this.state.removedIds,
      purchaseOrder: this.props.purchaseOrder,
      fetchEntities: this.successCallback
    });
  };

  render() {
    const { purchaseOrder, handleClose } = this.props;
    const { positions } = purchaseOrder.order;

    const { tableFields, formFields, key } = this.mapValuesToFields(positions);

    return (
      <ReactModal
        isOpen
        title="Подтверждение заказа у поставщика"
        handleClose={handleClose}
        contentStyles={this.modalStyles}
      >
        <ContainerContext
          key={key}
          fields={formFields}
          onSubmitForm={this.handleFormSubmit}
        >
          <Table cells={tableFields} entities={positions} />
          <RightStickedButton
            type="submit"
            disabled={this.state.removedIds.length === positions.length}
          >
            Заказан у поставщика
          </RightStickedButton>
        </ContainerContext>
      </ReactModal>
    );
  }
}

export default connect(
  () => ({ hasPermission: false }),
  { changeStateToOrderedFromProvider }
)(CommandOrderedFromProvider);
