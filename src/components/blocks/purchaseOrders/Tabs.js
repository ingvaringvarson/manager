import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";

import PositionsTable from "./PositionsTable";
import BillsTab from "./BillsTab";
import PaymentsTab from "./PaymentsTab";
import TasksTab from "./TasksTab";
import Documents from "../Documents";
import documentObjectTypeId from "../../../constants/documentObjectTypeId";

class Tabs extends PureComponent {
  static propTypes = {
    purchaseOrder: PropTypes.object,
    enumerations: PropTypes.object,
    push: PropTypes.func,
    client: PropTypes.object,
    requestParams: PropTypes.object,
    pathname: PropTypes.string,
    keyRouter: PropTypes.string
  };

  handleTabChanged = id => {
    this.props.push(`${this.props.pathname}?_tab=${id}`);
  };

  render() {
    const { requestParams } = this.props;
    return (
      <TabContainer
        selectedTabId={requestParams._tab}
        onChangeSelected={this.handleTabChanged}
      >
        <TabHeadContainer>
          <TabHead id={1}>Позиции заказа</TabHead>
          <TabHead id={2}>Счета</TabHead>
          <TabHead id={3}>Платежи</TabHead>
          <TabHead id={4}>Задачи</TabHead>
          <TabHead id={5}>Документы</TabHead>
        </TabHeadContainer>
        <TabBodyContainer>
          <TabBody id={1}>
            <PositionsTable {...this.props} />
          </TabBody>
          <TabBody id={2}>
            <BillsTab {...this.props} />
          </TabBody>
          <TabBody id={3}>
            <PaymentsTab {...this.props} />
          </TabBody>
          <TabBody id={4}>
            <TasksTab {...this.props} />
          </TabBody>
          <TabBody id={5}>
            <Documents
              idEntity={this.props.purchaseOrder.order.id}
              typeEntity={documentObjectTypeId.purchase_orders}
            />
          </TabBody>
        </TabBodyContainer>
      </TabContainer>
    );
  }
}

export default Tabs;
