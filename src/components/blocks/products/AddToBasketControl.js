import React, { Component } from "react";
import PropTypes from "prop-types";
import { palette } from "styled-tools";
import styled from "styled-components";
import { display } from "styled-system";
import memoizeOne from "memoize-one";

import IconButton from "../../../designSystem/molecules/IconButton";
import Input from "../../../designSystem/atoms/Input";
import Box from "../../../designSystem/organisms/Box";

const InputOrder = styled(Input)`
  width: 50px;
`;

const ButtonOrder = styled(IconButton)`
  position: relative;
  padding: 0 10px;
  background: ${palette("red", 1)};
`;

const StyledForm = styled.form`
  border: none;
  ${display}
`;

class AddToBasketControl extends Component {
  static propTypes = {
    addOfferToBasket: PropTypes.func.isRequired,
    offerId: PropTypes.number.isRequired,
    goodIds: PropTypes.arrayOf(PropTypes.number).isRequired,
    brandKey: PropTypes.string.isRequired,
    vendorCode: PropTypes.string.isRequired,
    productName: PropTypes.string.isRequired,
    vin: PropTypes.string,
    carVariantId: PropTypes.number,
    minOrder: PropTypes.number.isRequired,
    basketAttached: PropTypes.object,
    availability: PropTypes.number.isRequired,
    productId: PropTypes.string.isRequired
  };

  static defaultProps = {
    vin: "",
    carModelVariantId: 0
  };

  constructor(props) {
    super(props);

    const { minOrder } = props;

    this.state = {
      qty: minOrder
    };
  }

  getQtyItemInBasket = memoizeOne((goodIds, basket) => {
    if (!basket) return 0;

    const item = basket.items.find(i =>
      i.offerInfo.offer.goodIds.some(id => goodIds.includes(id))
    );

    return item ? item.qty : 0;
  });

  handleChangeQty = event => {
    event.preventDefault();
    const { qty } = this.state;
    const numbers = event.target.value.match(/[0-9]/g);

    const value = numbers ? +numbers.join("") : "";

    if (qty === value) return false;

    this.setState({ qty: value });
  };

  handleSubmitForm = event => {
    event.preventDefault();
    const {
      addOfferToBasket,
      offerId,
      brandKey,
      vendorCode: vendorCodes,
      productName: name,
      vin,
      productId: productKey,
      carVariantId: carModelVariantId,
      minOrder,
      availability,
      basketAttached,
      goodIds
    } = this.props;
    const { qty } = this.state;
    const inBasket = this.getQtyItemInBasket(goodIds, basketAttached);

    if (qty === "") return false;

    let value = qty;

    if (qty < minOrder) {
      value = minOrder;
    } else {
      const rest = qty % minOrder;

      if (rest) {
        value = qty + minOrder;
      }
    }

    if (value + inBasket > availability) {
      const range = availability - inBasket;
      value = range - (range % minOrder);
    }

    if (value !== qty) {
      this.setState({ qty: value });
    }

    if (qty === "") return false;

    addOfferToBasket({
      offerId,
      brandKey,
      vendorCodes,
      name,
      vin,
      productKey,
      carModelVariantId,
      qty
    });
  };

  render() {
    const { goodIds, basketAttached, minOrder, availability } = this.props;
    const inBasket = this.getQtyItemInBasket(goodIds, basketAttached);
    const disabled = inBasket + minOrder > availability;

    return (
      <Box display="flex" ml="auto">
        <StyledForm display="flex" onSubmit={this.handleSubmitForm}>
          <InputOrder
            disabled={disabled}
            onChange={this.handleChangeQty}
            value={this.state.qty}
          />
          <ButtonOrder
            disabled={disabled}
            type="submit"
            icon="add_shopping_cart"
            ml="-10px"
          />
        </StyledForm>
      </Box>
    );
  }
}

export default AddToBasketControl;
