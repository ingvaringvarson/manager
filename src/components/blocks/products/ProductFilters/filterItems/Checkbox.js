import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import CustomCheckbox from "../../../../../designSystem/molecules/Checkbox";

class Checkbox extends PureComponent {
  static propTypes = {
    label: PropTypes.string,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string,
    mark: PropTypes.string,
    classNames: PropTypes.string,
    children: PropTypes.element
  };

  render() {
    const {
      value,
      checked,
      disabled,
      label = null,
      name,
      classNames,
      children = null
    } = this.props;

    return (
      <CustomCheckbox
        name={name}
        onChange={this.handleChange}
        type="checkbox"
        value={value}
        disabled={disabled}
        checked={checked}
        label={label}
        className={classNames ? classNames : "filled-in"}
        children={children}
      />
    );
  }

  handleChange = event => {
    event.preventDefault();

    this.props.onChange(this.props.value, this.props.mark);
  };
}

export default Checkbox;
