import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Input from "../../../../../designSystem/atoms/Input";

class TextInput extends PureComponent {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
    name: PropTypes.string,
    mark: PropTypes.string,
    classNames: PropTypes.string
  };

  render() {
    const { value, label, name } = this.props;

    return (
      <Input
        name={name}
        onChange={this.handleChange}
        value={`${value}`}
        label={label}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        type="text"
      />
    );
  }

  handleFocus = event => {
    event.preventDefault();

    this.props.onFocus(this.props.mark);
  };

  handleBlur = event => {
    event.preventDefault();

    this.props.onBlur(this.props.mark);
  };

  handleChange = event => {
    event.preventDefault();

    this.props.onChange(event.target.value, this.props.mark);
  };
}

export default TextInput;
