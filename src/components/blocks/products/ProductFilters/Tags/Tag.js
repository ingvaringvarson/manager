import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { customView } from "../../../../../helpers/productFilters";
import { digitsForNumbers } from "../../../../../utils";
import { filterPropTypes } from "../../../../../helpers/propTypes/filterTypes";

class Tag extends PureComponent {
  static propTypes = {
    filter: PropTypes.shape({
      ...filterPropTypes
    }).isRequired,
    selected: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.arrayOf(PropTypes.number)
    ]).isRequired,
    onResetFilter: PropTypes.func.isRequired
  };

  state = {
    isShow: true
  };

  render() {
    return this.state.isShow ? this._getTag() : null;
  }

  _getTag() {
    const {
      filter: { type, items, titleProperty, urlProperty },
      selected
    } = this.props;
    let tagName = "";

    if (type.view === "parameters") {
      const field = items.find(item => selected === item.valueUrl);

      if (!field) return null;

      tagName = field.name;
    } else if (type.view === "minmax") {
      if (
        customView[urlProperty] &&
        (type.ui === "select" || type.ui === "checkboxlist")
      ) {
        const customField = customView[urlProperty]().find(
          item => selected === item.id
        );

        if (!customField) return null;

        tagName = customField.name;
      } else if (Array.isArray(selected)) {
        tagName =
          urlProperty === "price"
            ? `${digitsForNumbers(selected[0])} - ${digitsForNumbers(
                selected[1]
              )}`
            : `${selected[0]} - ${selected[1]}`;
      }
    }

    return (
      <div className="chip chip--tag">
        <i className="chip__close" onClick={this.handleResetFilter} />
        <span className="t">{titleProperty}</span>
        <span>{tagName}</span>
      </div>
    );
  }

  handleResetFilter = event => {
    event.preventDefault();
    const {
      filter: { type, urlProperty },
      selected
    } = this.props;

    this.setState({ isShow: false });
    this.props.onResetFilter({ urlProperty, type, value: selected });
  };
}

export default Tag;
