import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { customView } from "../../../../../helpers/productFilters";
import Tag from "./Tag";

class Tags extends PureComponent {
  static propTypes = {
    filters: PropTypes.array.isRequired,
    onResetFilters: PropTypes.func.isRequired
  };

  state = {
    isShow: true
  };

  render() {
    if (!this.state.isShow) return null;

    return (
      <div className="filter-type filter-type--tags">
        <span>{this._getTags()}</span>
        <span
          onClick={this.handleResetAllFilter}
          className="filter-type__remove-btn"
        >
          убрать все фильтры
        </span>
      </div>
    );
  }

  _getTags() {
    const { filters } = this.props;

    if (!filters.length) return null;

    return filters.reduce((tags, filter) => {
      if (
        filter.type.view === "parameters" ||
        (filter.type.view === "minmax" &&
          customView[filter.urlProperty] &&
          (filter.type.ui === "select" || filter.type.ui === "checkboxlist"))
      ) {
        if (filter.selectedItems.length === 1) {
          tags.push(
            <Tag
              key={`${filter.urlProperty}_${filter.selectedItems[0]}`}
              filter={filter}
              selected={filter.selectedItems[0]}
              onResetFilter={this.handleResetFilter}
            />
          );
        } else {
          filter.selectedItems.forEach(selectedField => {
            tags.push(
              <Tag
                key={`${filter.urlProperty}_${selectedField}`}
                filter={filter}
                selected={selectedField}
                onResetFilter={this.handleResetFilter}
              />
            );
          });
        }
      } else if (filter.type.view === "minmax") {
        tags.push(
          <Tag
            key={filter.urlProperty}
            filter={filter}
            selected={filter.selectedItems}
            onResetFilter={this.handleResetFilter}
          />
        );
      }

      return tags;
    }, []);
  }

  handleResetFilter = params => {
    const { urlProperty, type, value } = params;

    if (
      type.view === "parameters" ||
      (type.view === "minmax" &&
        customView[urlProperty] &&
        (type.ui === "select" || type.ui === "checkboxlist"))
    ) {
      this.props.onResetFilters([
        { urlProperty, type, value: { [value]: null } }
      ]);
    } else if (type.view === "minmax") {
      this.props.onResetFilters([{ urlProperty, type, value: null }]);
    }
  };

  handleResetAllFilter = event => {
    event.preventDefault();
    const { filters } = this.props;

    const resetFilters = filters.map(filter => {
      const { type, urlProperty } = filter;

      return { urlProperty, type, value: null };
    });

    this.setState({ isShow: false });
    this.props.onResetFilters(resetFilters, true);
  };
}

export default Tags;
