import React, { Component } from "react";
import PropTypes from "prop-types";
import { generateUrlWithFilters } from "../../../../utils";
import FilterRow from "./FilterRow";
import Box from "../../../../designSystem/organisms/Box";
import Title from "../../../../designSystem/atoms/Title";

class ProductFilters extends Component {
  static propTypes = {
    filters: PropTypes.arrayOf(PropTypes.object).isRequired,
    selectedFilters: PropTypes.arrayOf(PropTypes.object).isRequired,
    isLoading: PropTypes.bool.isRequired,
    push: PropTypes.func.isRequired,
    pathname: PropTypes.string.isRequired,
    query: PropTypes.object.isRequired,
    resetPage: PropTypes.bool,
    filterKeyList: PropTypes.object.isRequired
  };

  render() {
    const { filters } = this.props;

    if (!filters.length) return null;

    return (
      <>
        <Box
          p="middle"
          onClick={this.handleClickFiltersBlock}
          borderBottom="1px solid"
          borderColor="grayscale.1"
        >
          <Box>
            <Title mr="small">Фильтрация</Title>
          </Box>
          {/*        {!!selectedFilters.length && (
          <Tags
            filters={selectedFilters}
            onResetFilters={this.handleChangeFilter}
          />
        )}
        */}
        </Box>
        {this._getFilters()}
      </>
    );
  }

  _getFilters = () => {
    const { filters, filterKeyList } = this.props;

    return filters.map(filter => {
      return (
        <FilterRow
          key={filter.urlProperty}
          filter={filter}
          storeList={filterKeyList.storelist}
          onChangeFilter={this.handleChangeFilter}
        />
      );
    });
  };

  handleChangeFilter = (newFiltersList, isResetAll = false) => {
    const { query, isLoading, pathname, resetPage, filterKeyList } = this.props;

    if (isLoading) return;

    if (
      filterKeyList.storelist &&
      newFiltersList.some(item => {
        return (
          item.urlProperty === "delivery" &&
          ((item.value && item.value[1] === null) || item.value === null)
        );
      })
    ) {
      const { urlProperty, type } = filterKeyList.storelist;

      newFiltersList.push({ urlProperty, type, value: null });
    }

    this.props.push(
      generateUrlWithFilters({
        query,
        pathname,
        filters: newFiltersList,
        resetPage
      })
    );
  };

  handleClickFiltersBlock = event => {
    if (!this.props.isLoading) return;

    event.preventDefault();
    event.stopPropagation();
  };
}

export default ProductFilters;
