import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { customView } from "../../../../../helpers/productFilters";
import {
  CheckboxCustomLayout,
  RangeInputsLayout,
  SelectLayout,
  SliderLayout,
  CheckboxListLayout,
  BrandLevelsLayout
} from "./layouts";
import { filterPropTypes } from "../../../../../helpers/propTypes/filterTypes";
import styled from "styled-components";
import Box from "../../../../../designSystem/organisms/Box";
import Icon from "../../../../../designSystem/atoms/Icon";

const OpenButton = styled(Icon)`
  width: 24px;
  height: 24px;
  font-size: 24px;
  cursor: pointer;
`;

const Title = styled.p`
  font-size: 16px;
  color: #336ea8;
  margin-left: 10px;
  cursor: pointer;
`;

const SelectedIcon = styled.i`
  flex-shrink: 0;
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: #ef5350;
  margin-left: 10px;
`;

class FilterRow extends PureComponent {
  static propTypes = {
    filter: PropTypes.shape({
      ...filterPropTypes
    }).isRequired,
    onChangeFilter: PropTypes.func.isRequired,
    storeList: PropTypes.shape({
      ...filterPropTypes
    })
  };

  state = {
    isOpen: false
  };

  render() {
    const {
      filter: { items, urlProperty, selectedItems, titleProperty }
    } = this.props;
    const { isOpen } = this.state;

    if (!items.length || urlProperty === "storelist") return null;

    return (
      <>
        <Box
          p="middle"
          borderBottom="1px solid"
          borderColor="grayscale.1"
          display="flex"
          alignItems="center"
          onClick={this.handleToggleOpen}
        >
          <OpenButton icon={isOpen ? "expand_more" : "chevron_right"} />
          <Title mr="small">{titleProperty}</Title>
          {!!selectedItems.length && <SelectedIcon />}
        </Box>

        {isOpen && (
          <Box p="middle" borderBottom="1px solid" borderColor="grayscale.1">
            {this._getFilterLayouts()}
          </Box>
        )}
      </>
    );
  }

  _getFilterLayouts() {
    const {
      filter: { type, urlProperty },
      filter,
      onChangeFilter,
      storeList,
      brandLevels
    } = this.props;

    switch (true) {
      case type.view === "parameters" && type.ui === "brand_level":
        return (
          <BrandLevelsLayout
            filter={filter}
            onChangeFilter={onChangeFilter}
            onToggleFilterBlock={this.handleToggleOpen}
            brandLevels={brandLevels}
          />
        );
      /*      case type.view === "minmax" && urlProperty === "delivery":
        return (
          <DeliveryDaysLayout
            storeList={storeList}
            filter={filter}
            onChangeFilter={onChangeFilter}
            onToggleFilterBlock={this.handleToggleOpen}
          />
        );*/
      case type.view === "minmax" &&
        type.ui === "checkboxlist" &&
        !!customView[urlProperty]:
        return (
          <CheckboxCustomLayout
            filter={filter}
            onChangeFilter={onChangeFilter}
            onToggleFilterBlock={this.handleToggleOpen}
          />
        );
      case type.view === "parameters" && type.ui === "checkboxlist":
        return (
          <CheckboxListLayout
            filter={filter}
            onChangeFilter={onChangeFilter}
            onToggleFilterBlock={this.handleToggleOpen}
          />
        );
      case type.view === "parameters" && type.ui === "select":
        return (
          <SelectLayout
            filter={filter}
            onChangeFilter={onChangeFilter}
            onToggleFilterBlock={this.handleToggleOpen}
          />
        );
      case type.view === "minmax" &&
        (type.ui === "range" || type.ui === "number"):
        return (
          <RangeInputsLayout
            filter={filter}
            onChangeFilter={onChangeFilter}
            onToggleFilterBlock={this.handleToggleOpen}
          />
        );
      case type.view === "minmax" && type.ui === "slider":
        return (
          <SliderLayout
            filter={filter}
            onChangeFilter={onChangeFilter}
            onToggleFilterBlock={this.handleToggleOpen}
          />
        );
      default:
        return null;
    }
  }

  handleToggleOpen = () => {
    this.setState(prevState => {
      return {
        isOpen: !prevState.isOpen
      };
    });
  };
}

export default FilterRow;
