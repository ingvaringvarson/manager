import React, { PureComponent } from "react";
import { Checkbox } from "../../filterItems";
import { filterLayoutPropTypes } from "../../../../../../helpers/propTypes/filterTypes";

class ClampUpWiperBladeLayout extends PureComponent {
  static propTypes = {
    ...filterLayoutPropTypes
  };

  constructor(props) {
    super(props);

    this.state = {
      selected: props.filter.selectedItems,
      filter: props.filter
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.filter !== state.filter) {
      return {
        filter: props.filter,
        selected: props.filter.selectedItems
      };
    }

    return null;
  }

  render() {
    return <div>{this._getList()}</div>;
  }

  _getList() {
    const {
      filter: { urlProperty, items, availableItems }
    } = this.props;
    const { selected } = this.state;

    const itemsList = items.map(item => {
      const isSelected = selected.includes(item.valueUrl);
      const isDisabled = !availableItems.includes(item.valueUrl);

      return (
        <li
          key={`${item.valueUrl}_${isSelected}_${isDisabled}`}
          className="item"
        >
          <div className={`item__wrap ${isSelected ? "is-checked" : ""}`}>
            <Checkbox
              checked={isSelected}
              disabled={isDisabled}
              value={item.valueUrl}
              onChange={this.handleChange}
              children={
                <img
                  title=""
                  alt={item.name}
                  src={`/content/img/filters/${item.valueUrl}.jpg`}
                />
              }
              name={`${urlProperty}_${item.valueUrl}`}
              classNames="filled-in filled-in--img"
            />
          </div>
        </li>
      );
    });

    const listClass = items.length >= 10 ? "filter-type__inner-ul-long" : "";

    return <ul className={listClass}>{itemsList}</ul>;
  }

  handleChange = (value, mark) => {
    const {
      filter: { urlProperty, type }
    } = this.props;
    const { selected } = this.state;
    const newValue = { [value]: null };
    let newSelected = selected.slice();

    if (!selected.includes(value)) {
      newSelected.push(value);
      newValue[value] = value;
    } else {
      newSelected = newSelected.filter(item => item !== value);
    }

    this.setState({ selected: newSelected });
    this.props.onChangeFilter([{ urlProperty, type, value: newValue }]);

    if (newValue[value]) this.props.onToggleFilterBlock();
  };
}

export default ClampUpWiperBladeLayout;
