import React, { PureComponent } from "react";
import { customView } from "../../../../../../helpers/productFilters";
import { filterLayoutPropTypes } from "../../../../../../helpers/propTypes/filterTypes";
import Field from "../../../../../../designSystem/molecules/Field";
import Checkbox from "../../../../../../designSystem/molecules/Checkbox";

class CheckboxCustomLayout extends PureComponent {
  static propTypes = {
    ...filterLayoutPropTypes
  };

  constructor(props) {
    super(props);

    this.state = {
      selected: props.filter.selectedItems,
      filter: props.filter,
      itemList: customView[props.filter.urlProperty](props.filter.items)
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.filter !== state.filter) {
      return {
        selected: props.filter.selectedItems,
        filter: props.filter,
        itemList: customView[props.filter.urlProperty](props.filter.items)
      };
    }

    return null;
  }

  render() {
    return this._getList();
  }

  _getList() {
    const {
      filter: { urlProperty, availableItems }
    } = this.props;
    const { selected, itemList } = this.state;

    const itemsList = itemList.map(item => {
      const isSelected = selected.includes(item.id);
      const isDisabled =
        item.value.length === 1
          ? !(
              availableItems[0] <= item.value[0] &&
              availableItems[1] >= item.value[0]
            )
          : !(
              availableItems[0] <= item.value[1] &&
              availableItems[1] >= item.value[0]
            );

      return (
        <Field>
          <Checkbox
            type="checkbox"
            text={item.name}
            id={`${urlProperty}_${item.id}`}
            label={item.name}
            checked={isSelected}
            disabled={isDisabled}
            value={item.id}
            onChange={this.handleChange}
            name={`${urlProperty}_${item.id}`}
          />
        </Field>
      );
    });

    return itemsList;
  }

  handleChange = (value, mark) => {
    const {
      filter: { urlProperty, type }
    } = this.props;
    const { selected } = this.state;
    const newValue = { [value]: null };
    let newSelected = selected.slice();

    if (!selected.includes(value)) {
      newSelected.push(value);
      newValue[value] = value;
    } else {
      newSelected = newSelected.filter(item => item !== value);
    }

    this.setState({ selected: newSelected });
    this.props.onChangeFilter([{ urlProperty, type, value: newValue }]);

    if (newValue[value]) this.props.onToggleFilterBlock();
  };
}

export default CheckboxCustomLayout;
