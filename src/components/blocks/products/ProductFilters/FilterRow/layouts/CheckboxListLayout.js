import React, { PureComponent } from "react";
import { filterLayoutPropTypes } from "../../../../../../helpers/propTypes/filterTypes";
import Field from "../../../../../../designSystem/molecules/Field";
import Checkbox from "../../../../../../designSystem/molecules/Checkbox";
import Box from "../../../../../../designSystem/organisms/Box";
import styled from "styled-components";

const Title = styled.p`
  display: block;
  font-size: 14px;
  color: #787878;
  padding-bottom: 15px;
`;

const TitleLink = styled.p`
  cursor: pointer;
  color: #336ea8;
  display: block;
  margin: 15px 0;
  font-size: 12px;
`;

const entitiesTitles = {
  br: "бренды"
};

class CheckboxListLayout extends PureComponent {
  static propTypes = {
    ...filterLayoutPropTypes
  };

  constructor(props) {
    super(props);

    this.state = {
      selected: props.filter.selectedItems,
      filter: props.filter,
      isOpen: !props.filter.type.top
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.filter !== state.filter) {
      return {
        filter: props.filter,
        selected: props.filter.selectedItems
      };
    }

    return null;
  }

  render() {
    return (
      <div>
        {this._getTopList()}
        {this._getList()}
      </div>
    );
  }

  _getTopList() {
    const {
      filter: { urlProperty, items, type, availableItems }
    } = this.props;
    const { selected } = this.state;

    if (!type.top) return null;

    const topItems = items.filter(item => item.isTop);
    const topItemsList = topItems.map(item => {
      const isSelected = selected.includes(item.valueUrl);
      const isDisabled = !availableItems.includes(item.valueUrl);

      return (
        <Field>
          <Checkbox
            type="checkbox"
            text={item.name}
            id={`${urlProperty}_${item.valueUrl}`}
            label={item.name}
            checked={isSelected}
            disabled={isDisabled}
            value={item.valueUrl}
            onChange={this.handleChange}
            name={`${urlProperty}_${item.valueUrl}`}
          />
        </Field>
      );
    });

    const entitiesTitle = entitiesTitles[urlProperty] || "значения";
    const toggleTitle = this.state.isOpen
      ? `Скрыть все ${entitiesTitle}`
      : `Показать все ${entitiesTitle}`;

    return (
      <>
        <Title>Популярные {entitiesTitle}</Title>
        <Box>{topItemsList}</Box>
        <TitleLink onClick={this.handleToggleOpen}>{toggleTitle}</TitleLink>
      </>
    );
  }

  _getList() {
    const {
      filter: { urlProperty, items, availableItems }
    } = this.props;
    const { selected, isOpen } = this.state;

    if (!isOpen) return null;

    const itemsList = items.map(item => {
      const isSelected = selected.includes(item.valueUrl);
      const isDisabled = !availableItems.includes(item.valueUrl);
      const id = `${urlProperty}_${item.valueUrl}`;

      return (
        <Field key={id}>
          <Checkbox
            type="checkbox"
            text={item.name}
            id={id}
            label={item.name}
            checked={isSelected}
            disabled={isDisabled}
            value={item.valueUrl}
            onChange={this.handleChange}
            name={id}
          />
        </Field>
      );
    });

    return <Box>{itemsList}</Box>;
  }

  handleToggleOpen = () => {
    this.setState(prevState => ({ isOpen: !prevState.isOpen }));
  };

  handleChange = event => {
    const { value } = event.target;

    const {
      filter: { urlProperty, type }
    } = this.props;
    const { selected } = this.state;
    const newValue = { [value]: null };
    let newSelected = selected.slice();

    if (!selected.includes(value)) {
      newSelected.push(value);
      newValue[value] = value;
    } else {
      newSelected = newSelected.filter(item => item !== value);
    }

    this.setState({ selected: newSelected });
    this.props.onChangeFilter([{ urlProperty, type, value: newValue }]);

    if (newValue[value]) this.props.onToggleFilterBlock();
  };
}

export default CheckboxListLayout;
