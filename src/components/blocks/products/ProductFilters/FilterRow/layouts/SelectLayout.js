import React, { PureComponent } from "react";
import { filterLayoutPropTypes } from "../../../../../../helpers/propTypes/filterTypes";

class SelectLayout extends PureComponent {
  static propTypes = {
    ...filterLayoutPropTypes
  };

  constructor(props) {
    super(props);

    this.state = {
      selected: props.filter.selectedItems,
      filter: props.filter
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.filter !== state.filter) {
      return {
        filter: props.filter,
        selected: props.filter.selectedItems
      };
    }

    return null;
  }

  render() {
    return <div>{this._getSelect()}</div>;
  }

  _getSelect() {
    const {
      filter: { urlProperty, items, titleProperty, availableItems }
    } = this.props;
    const { selected } = this.state;

    const itemList = items.map(item => {
      return (
        <option
          value={item.valueUrl}
          disabled={!availableItems.includes(item.valueUrl)}
          key={item.valueUrl}
        >
          {item.name}
        </option>
      );
    });

    return {
      /*      <CustomSelect
        key={`${urlProperty}_${selected.join("_")}_${availableItems.join("_")}`}
        placeholder={titleProperty}
        onChange={this.handleChange}
        value={selected[0]}
        disabled={!items.length}
      >
        {itemList}
      </CustomSelect>*/
    };
  }

  handleChange = (value, name) => {
    const {
      filter: { urlProperty, type, selectedItems }
    } = this.props;
    const dataValue = {};
    let newValue = null;
    const newSelected = [];

    const selectedOptionList = [value];

    selectedOptionList.forEach(option => {
      if (!selectedItems.find(entity => entity === option)) {
        dataValue[option] = option;
        newValue = option;
      }
      newSelected.push(option);
    });
    selectedItems.forEach(option => {
      if (!selectedOptionList.find(selectedOption => selectedOption === option))
        dataValue[option] = null;
    });

    this.setState({ selected: newSelected });
    this.props.onChangeFilter([{ urlProperty, type, value: dataValue }]);

    if (newValue) {
      this.props.onToggleFilterBlock();
    }
  };
}

export default SelectLayout;
