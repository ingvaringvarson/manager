import React, { PureComponent } from "react";
import Slider from "rc-slider";
import { filterLayoutPropTypes } from "../../../../../../helpers/propTypes/filterTypes";

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

class SliderLayout extends PureComponent {
  static propTypes = {
    ...filterLayoutPropTypes
  };

  constructor(props) {
    super(props);

    this.style = { backgroundColor: "#336ea8" };

    const { selectedItems, items } = props.filter;
    const selected = selectedItems.length
      ? [Math.floor(selectedItems[0]), Math.floor(selectedItems[1])]
      : [Math.floor(items[0]), Math.floor(items[1])];

    this.state = {
      selected,
      marks: {
        [selected[0]]: <span>{selected[0]}</span>,
        [selected[1]]: <span>{selected[1]}</span>
      },
      filter: props.filter
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.filter !== state.filter) {
      const { selectedItems, items } = props.filter;
      const selected = selectedItems.length
        ? [Math.floor(selectedItems[0]), Math.floor(selectedItems[1])]
        : [Math.floor(items[0]), Math.floor(items[1])];

      return {
        selected,
        marks: {
          [selected[0]]: <span>{selected[0]}</span>,
          [selected[1]]: <span>{selected[1]}</span>
        },
        filter: props.filter
      };
    }

    return null;
  }

  render() {
    const {
      filter: { items }
    } = this.props;
    const { selected, marks } = this.state;

    return (
      <Range
        className="rc-slider-filter"
        min={Math.floor(items[0])}
        max={Math.floor(items[1])}
        marks={marks}
        tipFormatter={this.handleTipFormatter}
        onAfterChange={this.handleChange}
        defaultValue={selected}
        maximumTrackStyle={this.style}
        handleStyle={this.style}
      />
    );
  }

  handleTipFormatter = value => Math.floor(value);

  handleChange = value => {
    const {
      filter: { items, type, urlProperty },
      onChangeFilter,
      onToggleFilterBlock
    } = this.props;
    const resetSelected =
      value[0] &&
      Math.floor(value[0]) === Math.floor(items[0]) &&
      (value[1] && Math.floor(value[1]) === Math.floor(items[1]));

    onToggleFilterBlock();
    return onChangeFilter([
      {
        urlProperty,
        type,
        value: resetSelected
          ? null
          : [Math.floor(value[0]), Math.floor(value[1])]
      }
    ]);
  };
}

export default SliderLayout;
