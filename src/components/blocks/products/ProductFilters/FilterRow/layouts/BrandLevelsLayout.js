import React, { PureComponent } from "react";
import PropTypes from "prop-types";
/*import { Checkbox } from "../../filterItems";
import { yaCounter } from "../../../../../helpers";
import { filterLayoutPropTypes } from "../../../../../helpers/propTypes/filterTypes";
import BrandInfo from "../../../../common/BrandInfo";*/

class BrandLevelsLayout extends PureComponent {
  static propTypes = {
    /*  ...filterLayoutPropTypes,*/
    brandLevels: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      selected: props.filter.selectedItems,
      filter: props.filter,
      isOpen: !props.filter.type.top
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.filter !== state.filter) {
      return {
        filter: props.filter,
        selected: props.filter.selectedItems
      };
    }

    return null;
  }

  render() {
    return <div>{this._getList()}</div>;
  }

  _getList() {
    const {
      filter: { urlProperty, items, availableItems },
      brandLevels
    } = this.props;
    const { selected } = this.state;

    const itemsList = items.map(item => {
      const isSelected = selected.includes(item.valueUrl);
      const isDisabled = !availableItems.includes(item.valueUrl);

      return (
        <li
          key={`${item.valueUrl}_${isSelected}_${isDisabled}`}
          className="item item-flex"
        >
          {/*          <Checkbox
            label={item.name}
            checked={isSelected}
            disabled={isDisabled}
            value={item.valueUrl}
            onChange={this.handleChange}
            name={`${urlProperty}_${item.valueUrl}`}
          />
          <BrandInfo
            id={`${item.valueUrl}_${isSelected}_${isDisabled}`}
            brandLevels={brandLevels}
            brandLevel={item.valueUrl}
            hasTitle={false}
            classNames="filter-type__marker"
          />*/}
        </li>
      );
    });

    const listClass = items.length >= 10 ? "filter-type__inner-ul-long" : "";

    return <ul className={listClass}>{itemsList}</ul>;
  }

  handleChange = (value, mark) => {
    const {
      filter: { urlProperty, type, nameProperty },
      catalogKey
    } = this.props;
    const { selected } = this.state;
    const newValue = { [value]: null };
    let newSelected = selected.slice();

    /*    if (!selected.includes(value)) {
      newSelected.push(value);
      newValue[value] = value;
      yaCounter({
        type: "filter",
        params: { value, name: nameProperty, catalog: catalogKey, mark }
      });
    } else {
      newSelected = newSelected.filter(item => item !== value);
    }*/

    this.setState({ selected: newSelected });
    this.props.onChangeFilter([{ urlProperty, type, value: newValue }]);

    if (newValue[value]) this.props.onToggleFilterBlock();
  };
}

export default BrandLevelsLayout;
