import React, { PureComponent } from "react";
import { TextInput } from "../../filterItems";
import { filterLayoutPropTypes } from "../../../../../../helpers/propTypes/filterTypes";

class RangeInputsLayout extends PureComponent {
  static propTypes = {
    ...filterLayoutPropTypes
  };

  constructor(props) {
    super(props);

    this.counter = null;

    const { selectedItems, items } = props.filter;

    this.state = {
      selected: selectedItems.length
        ? [Math.floor(selectedItems[0]), Math.floor(selectedItems[1])]
        : [Math.floor(items[0]), Math.floor(items[1])],
      filter: props.filter
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.filter !== state.filter) {
      const { selectedItems, items } = props.filter;

      return {
        filter: props.filter,
        selected: selectedItems.length
          ? [Math.floor(selectedItems[0]), Math.floor(selectedItems[1])]
          : [Math.floor(items[0]), Math.floor(items[1])]
      };
    }

    return null;
  }

  render() {
    return this.props.filter.type.ui === "number"
      ? this._getMin()
      : this._getMinMax();
  }

  _getMin() {
    const {
      filter: { urlProperty }
    } = this.props;
    const { selected } = this.state;

    return (
      <TextInput
        value={selected[0]}
        onChange={this.handleChange}
        onBlur={this.handleBlur}
        onFocus={this.handleFocus}
        name={`${urlProperty}_min`}
        label="Количество более"
        classNames="single_input"
        mark="min"
      />
    );
  }

  _getMinMax() {
    const {
      filter: { urlProperty }
    } = this.props;
    const { selected } = this.state;

    return (
      <div className="input-range flex-row">
        <TextInput
          value={selected[0]}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          onFocus={this.handleFocus}
          name={`${urlProperty}_min`}
          label="От"
          classNames="input-bordered input-bordered--blue"
          mark="min"
        />
        <TextInput
          value={selected[1]}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          onFocus={this.handleFocus}
          name={`${urlProperty}_max`}
          label="До"
          classNames="input-bordered input-bordered--blue"
          mark="max"
        />
      </div>
    );
  }

  handleChange = (value, mark) => {
    const {
      filter: { items, type, urlProperty },
      onChangeFilter,
      onToggleFilterBlock
    } = this.props;
    const { selected } = this.state;
    const newSelected = [Math.floor(items[0]), Math.floor(items[1])];
    let resetSelected = true;

    if (!(!isNaN(parseFloat(value)) && isFinite(value))) return;

    const newVal = Math.floor(value < items[1] ? value : items[1]);

    if (
      (mark === "min" && selected[0] === newVal) ||
      (mark === "max" && selected[1] === newVal)
    )
      return;

    if (this.counter) clearTimeout(this.counter);

    if (mark === "min") {
      resetSelected =
        newVal === newSelected[0] && selected[1] === newSelected[1];
      newSelected[0] = newVal;
      newSelected[1] = selected[1];
    } else if (mark === "max") {
      resetSelected =
        newVal === newSelected[1] && selected[0] === newSelected[0];
      newSelected[0] = selected[0];
      newSelected[1] = newVal;
    }

    this.counter = setTimeout(() => {
      onChangeFilter([
        { urlProperty, type, value: resetSelected ? null : newSelected }
      ]);
      onToggleFilterBlock();
    }, 1000);

    return this.setState({ selected: newSelected });
  };

  handleFocus = type => {
    this.setState(prevState => {
      return {
        selected: [
          type === "min" ? "" : prevState.selected[0],
          type === "max" ? "" : prevState.selected[1]
        ]
      };
    });
  };

  handleBlur = type => {
    const {
      filter: { items, selectedItems }
    } = this.props;

    if (this.counter) return;

    this.setState(prevState => {
      return {
        selected: [
          type === "min" ? selectedItems[0] || items[0] : prevState.selected[0],
          type === "max" ? selectedItems[1] || items[1] : prevState.selected[1]
        ]
      };
    });
  };
}

export default RangeInputsLayout;
