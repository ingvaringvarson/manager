import React, { PureComponent } from "react";
import { Checkbox } from "../../filterItems";
import PropTypes from "prop-types";
import { customView } from "../../../../../../helpers/productFilters";
import {
  filterLayoutPropTypes,
  filterPropTypes
} from "../../../../../../helpers/propTypes/filterTypes";

class DeliveryDaysLayout extends PureComponent {
  static propTypes = {
    ...filterLayoutPropTypes,
    storeList: PropTypes.shape({
      ...filterPropTypes
    })
  };

  constructor(props) {
    super(props);

    this.style = {
      overlay: {
        backgroundColor: "rgba(0,0,0, .3)",
        zIndex: "1000"
      },
      content: {
        maxWidth: "1200px",
        margin: "auto",
        marginTop: "15px"
      }
    };

    this.state = {
      selectedDays: props.filter.selectedItems,
      filterDays: props.filter,
      itemList: customView[props.filter.urlProperty](props.filter.items),
      selectedStores: props.storeList && props.storeList.selectedItems,
      filterStores: props.storeList,
      isOpen: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    const newState = {};

    if (props.filter !== state.filterDays) {
      newState.filterDays = props.filter;
      newState.itemList = customView[props.filter.urlProperty](
        props.filter.items
      );
      newState.selectedDays = props.filter.selectedItems;
    }

    if (props.storeList !== state.filterStores) {
      newState.filterStores = props.storeList;
      newState.selectedStores =
        props.storeList && props.storeList.selectedItems;
    }

    if (Object.keys(newState).length) return newState;

    return null;
  }

  render() {
    return (
      <div>
        {this._getList()}
        {this._getTriggerMap()}
        {this._getModal()}
      </div>
    );
  }

  _getModal() {
    return {
      /*      <Modal
        isOpen={this.state.isOpen}
        contentLabel="Карта магазинов"
        className="map-modal"
        style={this.style}
        onRequestClose={this.handleCloseModal}
      >
        <div className="map-wrapper">
          <i className="close-ico" onClick={this.handleCloseModal} />
          <StoresOnMain />
        </div>
      </Modal>*/
    };
  }

  _getTriggerMap() {
    const { storeList } = this.props;
    const { selectedDays } = this.state;

    return selectedDays.includes("1") &&
      storeList &&
      storeList.items.length > 1 ? (
      <div className="filter__map" onClick={this.handleOpenModal}>
        Карта
      </div>
    ) : null;
  }

  _getList() {
    const {
      filter: { urlProperty, availableItems }
    } = this.props;
    const { selectedDays, itemList } = this.state;

    const itemsList = itemList.map(item => {
      const isSelected = selectedDays.includes(item.id);
      const isDisabled =
        item.value.length === 1
          ? !(
              availableItems[0] <= item.value[0] &&
              availableItems[1] >= item.value[0]
            )
          : !(
              availableItems[0] <= item.value[1] &&
              availableItems[1] >= item.value[0]
            );

      return (
        <li key={`${item.id}_${isSelected}_${isDisabled}`} className="item">
          <Checkbox
            label={item.name}
            checked={isSelected}
            disabled={isDisabled}
            value={item.id}
            onChange={this.handleChange}
            name={`${urlProperty}_${item.id}`}
          />
          {this._getStoreList(item.id)}
        </li>
      );
    });

    return <ul>{itemsList}</ul>;
  }

  _getStoreList(id) {
    const { storeList } = this.props;
    const { selectedDays } = this.state;

    if (
      !(
        id === "1" &&
        selectedDays.includes("1") &&
        storeList &&
        storeList.items.length > 1
      )
    )
      return null;

    const itemList = storeList.items.map(item => {
      return (
        <option
          disabled={!storeList.availableItems.includes(item.valueUrl)}
          value={item.valueUrl}
          key={item.valueUrl}
        >
          {item.name}
        </option>
      );
    });

    return (
      <div className="filter--mapped">
        {/*        <CustomSelect
          placeholder="Выберите магазин"
          onChange={this.handleChangeStore}
          value={selectedStores[0]}
        >
          {itemList}
        </CustomSelect>*/}
      </div>
    );
  }

  handleOpenModal = () => {
    this.setState({ isOpen: true });
  };

  handleChange = value => {
    const {
      filter: { urlProperty, type },
      storeList
    } = this.props;
    const { selectedDays } = this.state;
    const newValue = { [value]: null };
    let newSelected = selectedDays.slice();

    if (!selectedDays.includes(value)) {
      newSelected.push(value);
      newValue[value] = value;
    } else {
      newSelected = newSelected.filter(item => item !== value);
    }

    this.setState({ selectedDays: newSelected });
    this.props.onChangeFilter([{ urlProperty, type, value: newValue }]);

    if (
      !(
        urlProperty === "delivery" &&
        newValue[1] === "1" &&
        storeList &&
        storeList.items.length > 1
      ) &&
      newValue[value]
    ) {
      this.props.onToggleFilterBlock();
    }
  };
}

export default DeliveryDaysLayout;
