export function getColorForRejectStatsInversion(rejectStats) {
  switch (true) {
    case rejectStats > 90:
      return "green.1";
    case rejectStats > 50:
      return "yellow.2";
    default:
      return "red.1";
  }
}

export function getValueForDeliveryDays(deliveryDays) {
  switch (deliveryDays) {
    case 0:
      return "на складе";
    case 1:
      return "сегодня";
    case 2:
      return "завтра";
    case 3:
      return "послезавтра";
    default:
      return `> ${deliveryDays} дней`;
  }
}

export function getValueForQty(qty) {
  if (qty > 10) return "> 10 шт.";

  return `${qty} шт.`;
}

export function getValueForPrepaid(isPrepaid) {
  return isPrepaid ? "Требуется" : "Не требуется";
}
