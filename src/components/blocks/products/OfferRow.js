import React from "react";
import PropTypes from "prop-types";
import Box from "../../../designSystem/organisms/Box";
import { ifProp } from "styled-tools";
import styled, { css } from "styled-components";
import Text from "../../../designSystem/atoms/Text";
import Icon from "../../../designSystem/atoms/Icon";
import { color } from "styled-system";
import Title from "../../../designSystem/atoms/Title";
import {
  getColorForRejectStatsInversion,
  getValueForDeliveryDays,
  getValueForQty,
  getValueForPrepaid
} from "./helpers";
import { digitsForNumbers } from "../../../utils";
import AddToBasketControl from "./AddToBasketControl";

const StyledText = styled(Text)`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  ${ifProp(
    "strike",
    css`
      text-decoration: line-through;
    `
  )}
`;

const InfoIcon = styled(Icon)`
  width: 14px;
  ${color};
`;

const propTypes = {
  offer: PropTypes.shape({
    rejectStatsInversion: PropTypes.number,
    deliveryDays: PropTypes.number,
    id: PropTypes.number,
    key: PropTypes.string,
    goods: PropTypes.arrayOf(PropTypes.number),
    price: PropTypes.number,
    priceList: PropTypes.number,
    qty: PropTypes.number,
    minOrder: PropTypes.number,
    isPrepaid: PropTypes.bool,
    isInStock: PropTypes.bool,
    sale: PropTypes.object,
    isBackOrder: PropTypes.bool,
    saleRank: PropTypes.number,
    deliveryDate: PropTypes.string,
    storeList: PropTypes.arrayOf(PropTypes.object)
  }).isRequired,
  basketAttached: PropTypes.object,
  addOfferToBasket: PropTypes.func.isRequired,
  brandKey: PropTypes.string.isRequired,
  productName: PropTypes.string.isRequired,
  vendorCode: PropTypes.string.isRequired,
  productId: PropTypes.string.isRequired
};

function OfferRow(props) {
  const {
    offer: {
      rejectStatsInversion,
      deliveryDays,
      id,
      goods,
      price,
      qty,
      minOrder,
      isPrepaid,
      sale
    },
    vendorCode,
    productName,
    brandKey,
    addOfferToBasket,
    basketAttached,
    productId
  } = props;
  return (
    <Box
      display="grid"
      gridTemplateColumns="20% 20% 20% 20% 1fr"
      alignItems="start"
    >
      <Box display="flex" alignItems="start">
        <InfoIcon
          icon="thumb_up"
          ml="-24px"
          color={getColorForRejectStatsInversion(rejectStatsInversion)}
        />
        <Box display="flex" ml="10px" flexDirection="column">
          <StyledText mb="10px">{getValueForQty(qty)}</StyledText>
          {minOrder > 1 && (
            <StyledText color="secondary.0">
              Мин.заказ: {minOrder} шт.
            </StyledText>
          )}
        </Box>
      </Box>
      <StyledText>{getValueForDeliveryDays(deliveryDays)}</StyledText>
      <StyledText>{getValueForPrepaid(isPrepaid)}</StyledText>
      <Box display="flex" flexDirection="column">
        <Box display="flex" alignItems="baseline" mb="5px">
          <Title mr="5px" fontWeight="500">
            {digitsForNumbers(price)}{" "}
          </Title>
          <StyledText>руб.</StyledText>
        </Box>
        {sale && (
          <StyledText color="secondary.0" strike>
            {digitsForNumbers(sale.oldPrice)} руб.
          </StyledText>
        )}
      </Box>
      <AddToBasketControl
        offerId={id}
        minOrder={minOrder}
        goodIds={goods}
        availability={qty}
        vendorCode={vendorCode}
        productName={productName}
        brandKey={brandKey}
        addOfferToBasket={addOfferToBasket}
        basketAttached={basketAttached}
        productId={productId}
      />
    </Box>
  );
}

OfferRow.propTypes = propTypes;

export default OfferRow;
