import React, { Fragment } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import ToolTip from "../../common/ToolTip";
import { switchProp } from "styled-tools";

const propTypes = {
  id: PropTypes.string.isRequired,
  brandLevels: PropTypes.object.isRequired,
  brandLevel: PropTypes.string.isRequired
};

const Icon = styled.i`
  position: relative;
  display: inline-flex;
  flex-shrink: 0;
  width: 8px;
  height: 8px;
  font-size: 16px;
  border-radius: 50%;
  cursor: pointer;
  background-color: ${switchProp(
    "brandId",
    {
      original: "#336ea8",
      premium: "#00cf34",
      standart: "#f0a01e",
      ekonom: "#ef5350"
    },
    "#c4c4c4;"
  )};
`;

const Content = styled.div`
  max-width: 260px;
  padding: 8px 0;
`;

const Title = styled.p`
  margin-bottom: 5px;
  font-size: 14px;
  font-weight: 500;
`;

const Desc = styled.p`
  font-size: 12px;
`;

function BrandInfo(props) {
  const brandInfo = props.brandLevels && props.brandLevels[props.brandLevel];

  if (!brandInfo) return null;

  return (
    <Fragment>
      <Icon
        data-tip
        data-for={`brandLevel_${props.id}`}
        brandId={brandInfo.id}
      />
      <ToolTip
        id={`brandLevel_${props.id}`}
        place="bottom"
        type="dark"
        effect="solid"
        aria-haspopup="true"
      >
        <Content>
          <Title>{brandInfo.title}</Title>
          <Desc>{brandInfo.description}</Desc>
        </Content>
      </ToolTip>
    </Fragment>
  );
}

BrandInfo.propTypes = propTypes;

export default BrandInfo;
