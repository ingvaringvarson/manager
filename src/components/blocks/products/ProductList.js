import React, { Component } from "react";
import PropTypes from "prop-types";
import Box from "../../../designSystem/organisms/Box";
import ProductCard from "./ProductCard";

class ProductList extends Component {
  static propTypes = {
    title: PropTypes.string,
    showSubTitle: PropTypes.bool,
    isOriginal: PropTypes.bool,
    products: PropTypes.arrayOf(PropTypes.object).isRequired,
    addOfferToBasket: PropTypes.func.isRequired,
    basketAttached: PropTypes.object,
    brandLevels: PropTypes.object
  };

  state = { isShowAllProduct: false };

  handleShowAllProducts = () => {
    if (!this.state.isShowAllProduct) this.setState({ isShowAllProduct: true });
  };

  renderOriginalView() {
    const {
      basketAttached,
      title,
      products,
      showSubTitle,
      addOfferToBasket,
      brandLevels
    } = this.props;

    return products.slice(0, 5).map((product, index) => {
      return (
        <ProductCard
          key={product.id}
          title={!index ? title : null}
          showSubTitle={showSubTitle}
          product={product}
          basketAttached={basketAttached}
          addOfferToBasket={addOfferToBasket}
          showAllProducts={index === 4 ? this.handleShowAllProducts : null}
          brandLevels={brandLevels}
        />
      );
    });
  }

  renderDefaultView() {
    const {
      basketAttached,
      title,
      products,
      showSubTitle,
      addOfferToBasket,
      brandLevels
    } = this.props;

    return products.map((product, index) => {
      return (
        <ProductCard
          key={product.id}
          title={!index ? title : null}
          showSubTitle={showSubTitle}
          product={product}
          basketAttached={basketAttached}
          addOfferToBasket={addOfferToBasket}
          brandLevels={brandLevels}
        />
      );
    });
  }

  render() {
    const { isOriginal, products } = this.props;
    const { isShowAllProduct } = this.state;

    return (
      <Box pb="20px" borderBottom="1px solid" borderColor="grayscale.1">
        {isOriginal && !isShowAllProduct && products.length > 5
          ? this.renderOriginalView()
          : this.renderDefaultView()}
      </Box>
    );
  }
}

export default ProductList;
