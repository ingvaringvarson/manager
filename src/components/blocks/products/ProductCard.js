import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp, palette } from "styled-tools";
import memoizeOne from "memoize-one";

import OfferRow from "./OfferRow";
import BrandLevel from "./BrandLevel";
import SpecificationInfo from "./SpecificationInfo";
import Box from "../../../designSystem/organisms/Box";
import Title from "../../../designSystem/atoms/Title";
import Text from "../../../designSystem/atoms/Text";
import Icon from "../../../designSystem/atoms/Icon";

const StyledText = styled(Text)`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  ${ifProp(
    "strike",
    css`
      text-decoration: line-through;
    `
  )}
`;

const Status = styled.div`
  padding: 5px;
  border: 1px solid;
  border-radius: 3px;

  :not(:first-of-type) {
    margin-left: 10px;
  }

  ${ifProp(
    "blue",
    css`
      color: ${palette("blue", 0)};
      border-color: ${palette("blue", 0)};
    `
  )}

  ${ifProp(
    "green",
    css`
      color: ${palette("green", 0)};
      border-color: ${palette("green", 0)};
    `
  )}
`;

const Shop = styled.div`
  display: flex;

  :not(:first-of-type) {
    margin-left: 10px;
  }
`;

const LinkOpen = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`;

class ProductCard extends Component {
  static propTypes = {
    product: PropTypes.shape({
      id: PropTypes.string.isRequired,
      brandKey: PropTypes.string.isRequired,
      brandName: PropTypes.string.isRequired,
      productName: PropTypes.string.isRequired,
      categoryId: PropTypes.number.isRequired,
      categoryUrl: PropTypes.string.isRequired,
      brandLevel: PropTypes.string.isRequired,
      vendorCodes: PropTypes.arrayOf(PropTypes.object).isRequired,
      specification: PropTypes.arrayOf(PropTypes.object).isRequired,
      photos: PropTypes.arrayOf(PropTypes.object).isRequired,
      images: PropTypes.arrayOf(PropTypes.string),
      images3d: PropTypes.arrayOf(PropTypes.object),
      offers: PropTypes.arrayOf(PropTypes.object).isRequired,
      mainVendorCode: PropTypes.object.isRequired,
      mainOffers: PropTypes.arrayOf(PropTypes.object).isRequired,
      additionalOffers: PropTypes.arrayOf(PropTypes.object).isRequired
    }).isRequired,
    basketAttached: PropTypes.object,
    addOfferToBasket: PropTypes.func.isRequired,
    title: PropTypes.string,
    showSubTitle: PropTypes.bool,
    showAllProducts: PropTypes.func,
    brandLevels: PropTypes.object
  };

  state = {
    isVisibleAdditionalOffers: false
  };

  handleToggleShowAdditionalOffers = () => {
    this.setState(prevState => ({
      isVisibleAdditionalOffers: !prevState.isVisibleAdditionalOffers
    }));
  };

  getNotActualVendorCodes = memoizeOne(vendorCodes =>
    vendorCodes.filter(vendorCode => !vendorCode.isActual)
  );

  render() {
    const {
      title,
      showAllProducts,
      showSubTitle,
      basketAttached,
      addOfferToBasket,
      product: {
        productName,
        brandName,
        mainVendorCode,
        mainOffers,
        additionalOffers,
        brandKey,
        vendorCodes,
        brandLevel,
        id,
        specification
      },
      brandLevels
    } = this.props;
    const mainOffer = mainOffers[0];
    const notActualVendorCodes = showSubTitle
      ? this.getNotActualVendorCodes(vendorCodes)
      : [];

    return (
      <>
        {!!title && (
          <Box
            display="grid"
            gridTemplateColumns="55% 45%"
            alignItems="center"
            p="middle"
            borderBottom="1px solid"
            borderColor="grayscale.1"
          >
            <Box display="flex" alignItems="baseline">
              <Title mr="small">{title}</Title>
              {showSubTitle && (
                <Box display="flex">
                  {!!notActualVendorCodes.length && (
                    <>
                      <StyledText>Артикул имеет замены: </StyledText>
                      {notActualVendorCodes.map((item, index) => {
                        return (
                          <Fragment key={index}>
                            {!!index && ", "}
                            <StyledText ml="5px" fontWeight="500">
                              {item.vendorCode}
                            </StyledText>
                          </Fragment>
                        );
                      })}
                    </>
                  )}
                </Box>
              )}
            </Box>
            <Box
              display="grid"
              gridTemplateColumns="20% 20% 20% 40%"
              alignItems="center"
            >
              <StyledText>Кол-во</StyledText>
              <StyledText>Срок</StyledText>
              <StyledText>Предоплата</StyledText>
              <StyledText>Цена за ед.</StyledText>
            </Box>
          </Box>
        )}
        <Box
          display="grid"
          gridRowGap="10px"
          px="middle"
          py="small"
          borderBottom="1px solid"
          borderColor="grayscale.1"
        >
          <Box display="grid" gridTemplateColumns="55% 45%" alignItems="start">
            <Box
              display="grid"
              gridTemplateColumns="10% 25% 40% 35%"
              gridColumnGap="20px"
              alignItems="center"
            >
              <Box display="flex" alignItems="center">
                <StyledText mr="6px" fontWeight="500">
                  {brandName}
                </StyledText>
                <BrandLevel
                  brandLevels={brandLevels}
                  brandLevel={brandLevel}
                  id={`product_card_${id}`}
                />
              </Box>
              {mainVendorCode && (
                <StyledText>{mainVendorCode.vendorCode}</StyledText>
              )}
              <StyledText>{productName}</StyledText>
              <Box
                display="grid"
                gridTemplateColumns="repeat(2, 14px)"
                gridColumnGap="8px"
                alignItems="center"
                pr="25px"
              >
                <SpecificationInfo
                  specification={specification}
                  id={`product_card_${id}`}
                />
                <Icon icon="camera" />
              </Box>
            </Box>
            <Box display="grid" gridRowGap="20px">
              {mainOffers.length ? (
                mainOffers.map(offer => {
                  return (
                    <OfferRow
                      key={offer.id}
                      offer={offer}
                      basketAttached={basketAttached}
                      addOfferToBasket={addOfferToBasket}
                      brandKey={brandKey}
                      productName={productName}
                      vendorCode={mainVendorCode.vendorCode}
                      productId={id}
                    />
                  );
                })
              ) : (
                <Text>
                  К сожалению, предложений для данного товара сейчас нет, но мы
                  можем предложить вам аналоги.
                </Text>
              )}
            </Box>
          </Box>
          <Box
            display="flex"
            alignItems="flex-end"
            justifyContent="space-between"
          >
            {mainOffer && (
              <Box display="grid" gridRowGap="5px">
                <Box display="flex">
                  {!!mainOffer.sale && <Status blue>Распродажа</Status>}
                </Box>
                {!!mainOffer.storeList.length && (
                  <Box display="flex">
                    {mainOffer.storeList.map(store => {
                      return (
                        <Shop key={store.storeId}>
                          <StyledText mr="5px">
                            {!!store.name && store.name.slice(0, 4)}
                          </StyledText>
                          <StyledText alter>{store.qty}</StyledText>
                        </Shop>
                      );
                    })}
                  </Box>
                )}
              </Box>
            )}
            {showAllProducts && (
              <LinkOpen onClick={showAllProducts}>
                <StyledText mr="6px" alter>
                  Посмотреть все товары
                </StyledText>
                <Icon icon="arrow_drop_down" width="12px" />
              </LinkOpen>
            )}
            {!!additionalOffers.length && (
              <LinkOpen onClick={this.handleToggleShowAdditionalOffers}>
                <StyledText mr="6px" alter>
                  Ещё предложения
                </StyledText>
                <Icon icon="arrow_drop_down" width="12px" />
              </LinkOpen>
            )}
          </Box>
          {this.state.isVisibleAdditionalOffers && !!additionalOffers.length && (
            <Box
              display="grid"
              gridTemplateColumns="55% 45%"
              alignItems="start"
            >
              <Box />
              <Box display="grid" gridRowGap="20px">
                {additionalOffers.map(offer => {
                  return (
                    <OfferRow
                      key={offer.id}
                      offer={offer}
                      basketAttached={basketAttached}
                      addOfferToBasket={addOfferToBasket}
                      brandKey={brandKey}
                      productName={productName}
                      vendorCode={mainVendorCode.vendorCode}
                      productId={id}
                    />
                  );
                })}
              </Box>
            </Box>
          )}
        </Box>
      </>
    );
  }
}

export default ProductCard;
