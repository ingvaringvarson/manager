import React, { Fragment } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import ToolTip from "../../common/ToolTip";

import Icon from "../../../designSystem/atoms/Icon";

const StyledIcon = styled(Icon)`
  font-size: 14px;
  cursor: default;
`;

const ToolTipContent = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 10px;
  max-width: 260px;
  padding: 8px 10px;
`;

const Row = styled.div`
  position: relative;
  display: flex;
  align-items: baseline;
  justify-content: space-between;
  font-size: 14px;

  ::after {
    content: "";
    position: absolute;
    z-index: 1;
    top: 15px;
    width: 100%;
    border-bottom: 2px dotted #cbcbcb;
  }
`;

const Title = styled.p`
  position: relative;
  z-index: 2;
  padding-right: 2px;
  background: white;
`;

const Desc = styled.p`
  position: relative;
  z-index: 2;
  padding-left: 2px;
  background: white;
`;

const propTypes = {
  id: PropTypes.string.isRequired,
  specification: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      value: PropTypes.string
    })
  )
};

function SpecificationInfo(props) {
  if (!props.specification.length) return null;

  return (
    <Fragment>
      <StyledIcon
        icon="error"
        data-tip
        data-for={`specificationInfo_${props.id}`}
      />
      <ToolTip
        id={`specificationInfo_${props.id}`}
        place="bottom"
        type="light"
        effect="solid"
        aria-haspopup="true"
      >
        <ToolTipContent>
          {props.specification.map(s => {
            return (
              <Row key={s.name}>
                <Title>{s.name}</Title>
                <Desc>{s.value}</Desc>
              </Row>
            );
          })}
        </ToolTipContent>
      </ToolTip>
    </Fragment>
  );
}

SpecificationInfo.propTypes = propTypes;

export default SpecificationInfo;
