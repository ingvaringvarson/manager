import React from "react";
import MenuSidebar from "./MenuSidebar";
import MenuItem from "./MenuItem";
import { getUrl } from "../../../helpers/urls";

const LeftMenu = props => {
  const { pathname, ...rest } = props;
  return (
    <MenuSidebar left {...rest}>
      <MenuItem
        id="search"
        active={pathname === getUrl("search")}
        url={getUrl("search")}
        title="Подбор товара"
        icon="card_giftcard"
      />
      <MenuItem
        id="clients"
        active={pathname === "/"}
        url="/"
        title="Клиенты"
        icon="people"
      />
      <MenuItem
        id="orders"
        active={pathname === getUrl("orders")}
        url={getUrl("orders")}
        title="Заказы"
        icon="description"
      />
      <MenuItem
        id="payments"
        active={pathname === getUrl("payments")}
        url={getUrl("payments")}
        title="Платежи"
        icon="account_balance_wallet"
      />
      <MenuItem
        id="tasks"
        active={pathname === getUrl("tasks")}
        url={getUrl("tasks")}
        title="Задачи"
        icon="assignment"
      />
      <MenuItem
        id="warehouse"
        active={pathname === getUrl("warehouse")}
        url={getUrl("warehouse")}
        title="Склад"
        icon="store_mall_directory"
      />
      <MenuItem
        id="purchaseorders"
        active={pathname === getUrl("purchaseorders")}
        url={getUrl("purchaseorders")}
        title="Заказы поставщику"
        icon="mdi-worker"
      />
    </MenuSidebar>
  );
};

export default LeftMenu;
