import React from "react";
import { Link } from "react-router-dom";
import styled, { css } from "styled-components";
import { ifProp, palette } from "styled-tools";
import Icon from "../../../designSystem/atoms/Icon";
import ReactTooltip from "react-tooltip";
import PropTypes from "prop-types";

const StyledItem = styled.span`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 64px;
  height: 64px;
  margin: 0;
  padding: 0;
  color: ${ifProp("disabled", "#f1f1f1", "#000000")};
  border: 0 none;
  outline: none;
  background: transparent;
  cursor: pointer;
  transition: background 0.3s;

  :before {
    content: "";
    position: absolute;
    top: 50%;
    left: 0;
    width: 2px;
    height: 50px;
    background: transparent;
    transform: translateY(-50%);
    transition: background 0.3s;
  }

  ${ifProp(
    "active",
    css`
      background: rgba(238, 247, 255, 0.6);

      :before {
        background: ${palette("primary", 0)};
      }
    `
  )};

  ${ifProp(
    "attached",
    css`
      color: ${palette("primary", 0)};
    `
  )};
`;

const UserIcon = styled(Icon)`
  width: 36px;
  height: 36px;
`;

const propTypes = {
  active: PropTypes.bool,
  url: PropTypes.string,
  title: PropTypes.string,
  icon: PropTypes.string.isRequired,
  id: PropTypes.string,
  children: PropTypes.any,
  disabled: PropTypes.bool,
  attached: PropTypes.bool
};

const MenuItem = props => {
  const { url, title, icon, id, active, children, ...rest } = props;

  const iconBlock = id ? (
    <>
      <UserIcon
        data-tip="tooltip"
        data-for={`left-menu-${id}`}
        data-place="right"
        data-effect="solid"
        icon={icon}
      />
      <ReactTooltip id={`left-menu-${id}`}>{title}</ReactTooltip>
    </>
  ) : (
    <UserIcon icon={icon} />
  );
  return url && !props.disabled ? (
    <Link to={url}>
      <StyledItem {...rest} active={active}>
        {iconBlock}
        {children}
      </StyledItem>
    </Link>
  ) : (
    <StyledItem {...rest} active={active}>
      {iconBlock}
      {children}
    </StyledItem>
  );
};

MenuItem.propTypes = propTypes;

const MenuItemBordered = styled(MenuItem)`
  border-bottom: 2px solid ${palette("grayscale", 1)};
`;

export { MenuItemBordered };
export default MenuItem;
