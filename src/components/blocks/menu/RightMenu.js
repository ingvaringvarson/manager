import React from "react";
import BasketMenu from "../baskets/BasketMenu";

import MenuSidebar from "./MenuSidebar";
import { MenuItemBordered } from "./MenuItem";

const RightMenu = ({ count, ...props }) => {
  return (
    <MenuSidebar right {...props}>
      <MenuItemBordered icon="notifications" />
      <BasketMenu />
    </MenuSidebar>
  );
};

export default RightMenu;
