import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp } from "styled-tools";

import Aside from "../../../designSystem/molecules/Aside";

const StyledMenu = styled(Aside)`
  position: fixed;
  top: 64px;
  height: calc(100% - 64px);
  z-index: 10;

  ${ifProp(
    "left",
    css`
      left: 0;
    `
  )}

  ${ifProp(
    "right",
    css`
      right: 0;
    `
  )}
`;

const MenuSidebar = ({ children, ...props }) => {
  return <StyledMenu {...props}>{children}</StyledMenu>;
};

MenuSidebar.propTypes = {
  children: PropTypes.any
};

export default MenuSidebar;
