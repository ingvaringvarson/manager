import React, { PureComponent } from "react";

import ModalIconControls from "../../../common/modalControls/ModalIconControls";
import renderIconBlock from "../../../common/modalControls/renderIconBlock";
import FormController from "./FormController";

import model from "./model";

class WrappedWithModal extends PureComponent {
  static propTypes = model.propTypes;
  static defaultProps = model.defaultProps;

  modalStyles = {
    content: {
      width: "400px"
    }
  };

  renderForm = (state, modalProps, blockProps, iconProps) => {
    return (
      <FormController
        {...this.props}
        iconProps={iconProps}
        contract={this.props.contract}
        afterSubmit={modalProps.onClose}
      />
    );
  };

  render() {
    return (
      <ModalIconControls
        modalStyles={this.modalStyles}
        renderContent={this.renderForm}
        iconProps={this.props.iconProps}
      >
        {renderIconBlock(this.props.iconName)}
      </ModalIconControls>
    );
  }
}

export default WrappedWithModal;
