import FormController from "./FormController";

import WrappedWithModal from "./WrappedWithModal";
import FormModal from "./FormModal";

export default FormController;

export { WrappedWithModal, FormModal };
