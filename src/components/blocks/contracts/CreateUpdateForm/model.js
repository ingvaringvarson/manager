import PropTypes from "prop-types";
import { reduceValuesToFormFields } from "../../../../fields/helpers";
import { generateId } from "../../../../utils";

const formModel = {
  mapFieldsForForm(entity, enumerations, fields) {
    return fields.reduce(
      reduceValuesToFormFields(
        entity || {},
        enumerations,
        entity ? null : generateId()
      ),
      []
    );
  },
  propTypes: {
    contract: PropTypes.object,
    client: PropTypes.object.isRequired,
    iconName: PropTypes.string,
    iconProps: PropTypes.object
  },
  defaultProps: {
    iconName: "add",
    contract: null
  },
  enumerationNames: ["contractState", "contractType", "contractTerms"]
};

export default formModel;
