import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../../ducks/enumerations";
import { updateClient } from "../../../../ducks/clients";

import FieldForm from "../../../common/form/FieldForm";
import BtnBlock from "../../../common/styled/BtnBlock";
import { ContainerContext } from "../../../common/formControls";

import BlockContent from "../../../../designSystem/organisms/BlockContent";
import Button from "../../../../designSystem/atoms/Button";
import Heading from "../../../../designSystem/molecules/Heading";
import Block from "../../../../designSystem/organisms/Block";

import { fieldsForForm } from "../../../../fields/formFields/clients/fieldsForClientContracts";

import model from "./model";

class FormController extends PureComponent {
  static propTypes = {
    ...model.propTypes,
    iconProps: PropTypes.object.isRequired,
    afterSubmit: PropTypes.func
  };
  static defaultProps = model.defaultProps;

  componentDidMount() {
    this.props.fetchEnumerations(model.enumerationNames);
  }

  handleSubmitForm = params => {
    this.props.updateClient(this.props.client.id, params, this.props.client);
    if (this.props.afterSubmit) this.props.afterSubmit();
  };

  render() {
    const copyFields = model.mapFieldsForForm(
      { ...this.props.contract, client: this.props.client },
      this.props.enumerations,
      fieldsForForm
    );

    return (
      <Block>
        <Heading
          subtitle={
            this.props.contract
              ? `Договор №${this.props.contract.number}`
              : null
          }
          title={this.props.contract ? "Редактирование" : "Создание договора"}
          icon="close"
          iconProps={this.props.iconProps}
        />
        <BlockContent>
          <ContainerContext
            fields={copyFields}
            onSubmitForm={this.handleSubmitForm}
            submitValidation
          >
            {copyFields.map((field, index) => {
              return <FieldForm field={field} index={index} />;
            })}
            <BtnBlock>
              <Button type="submit">
                {this.props.contract ? "Изменить" : "Создать"}
              </Button>
            </BtnBlock>
          </ContainerContext>
        </BlockContent>
      </Block>
    );
  }
}

export default connect(
  state => {
    return {
      enumerations: enumerationsSelector(state, {
        enumerationNames: model.enumerationNames
      })
    };
  },
  {
    fetchEnumerations,
    updateClient
  }
)(FormController);
