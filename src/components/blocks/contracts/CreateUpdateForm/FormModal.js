import React, { PureComponent } from "react";
import ReactModal from "react-modal";
import FormController from "./FormController";
import { customStyles } from "../../../../constants/defaultStyles/reactModalStyles";

class FormModal extends PureComponent {
  state = {
    isOpen: false
  };

  customStyles = {
    ...customStyles,
    content: {
      ...customStyles.content,
      width: "400px",
      overflow: "visible"
    }
  };

  handleModalClose = () => this.setState({ isOpen: false });
  handleModalOpen = () => this.setState({ isOpen: true });

  iconProps = {
    onClick: this.handleModalClose
  };

  render() {
    return (
      <>
        <div onClick={this.handleModalOpen}>{this.props.children}</div>
        <ReactModal
          onRequestClose={this.handleModalClose}
          isOpen={this.state.isOpen}
          style={this.customStyles}
        >
          <FormController {...this.props} iconProps={this.iconProps} />
        </ReactModal>
      </>
    );
  }
}

export default FormModal;
