import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import Title from "../../../designSystem/atoms/Title";
import Box from "../../../designSystem/organisms/Box";
import Menu from "../../../designSystem/molecules/Menu";
import { Overlay } from "../../common/styled/modal";
import MenuItem from "../../common/menus/MenuItem";
import { ifProp } from "styled-tools";
import {
  enumerationStateSelector,
  fetchEnumerations
} from "../../../ducks/enumerations";
import { queryRouterSelector } from "../../../redux/selectors/router";
import { ContextSearch } from "../../routes/search/context";

const enumerationNames = ["city"];

const LinkButton = styled.button`
  background: transparent;
  border: none;
  cursor: pointer;
  color: #336ea8;
`;

const CityList = styled(Menu)`
  display: none;
  position: absolute;
  text-align: left;
  top: 50px;
  ${ifProp(
    "isShow",
    css`
      display: block;
    `
  )}
`;

class ChangeCityBlock extends Component {
  static propTypes = {
    cities: PropTypes.shape({
      list: PropTypes.array.isRequired,
      map: PropTypes.object.isRequired
    }),
    onChangeCity: PropTypes.func.isRequired,
    selectedCityId: PropTypes.number.isRequired
  };

  static contextType = ContextSearch;

  static getDerivedStateFromProps(props, state) {
    if (state.isShowMenu && state.prevSelectedCityId !== props.selectedCityId) {
      return {
        prevSelectedCityId: props.selectedCityId,
        isShowMenu: false
      };
    }

    return null;
  }

  componentDidMount() {
    this.props.fetchEnumerations(enumerationNames);
  }

  state = {
    isShowMenu: false,
    prevSelectedCityId: this.props.selectedCityId
  };

  handleToggleShowMenu = () => {
    this.setState(prevState => ({ isShowMenu: !prevState.isShowMenu }));
  };

  render() {
    const { cities, selectedCityId, onChangeCity } = this.props;
    const title = cities.map.hasOwnProperty(selectedCityId)
      ? cities.map[selectedCityId].name
      : "Город неопределён";
    return (
      <Box textAlign="right" position="relative">
        <Title>{title}</Title>
        <LinkButton onClick={this.handleToggleShowMenu}>Изменить</LinkButton>
        <CityList isShow={this.state.isShowMenu} right>
          {cities.list.map(city => {
            return (
              <MenuItem id={city.id} onClick={onChangeCity} key={city.id}>
                {city.name}
              </MenuItem>
            );
          })}
        </CityList>
        {this.state.isShowMenu && (
          <Overlay onClick={this.handleToggleShowMenu} />
        )}
      </Box>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    cities: enumerationStateSelector(state, { name: "city" }),
    query: queryRouterSelector(state, props)
  };
}

export default connect(
  mapStateToProps,
  { fetchEnumerations }
)(ChangeCityBlock);
