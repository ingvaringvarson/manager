import React from "react";
import styled from "styled-components";

import Icon from "../../../designSystem/atoms/Icon/index";
import Text from "../../../designSystem/atoms/Text/index";
import Menu from "../../../designSystem/molecules/Menu/index";
import HeaderTypes from "../../../helpers/propTypes/components/HeaderTypes";

const UserMenu = styled(Menu)`
  display: none;
  position: absolute;
  width: 200px;
  right: 0;
  bottom: 0;
  padding-top: 20px;
  transform: translateY(100%);
`;

const StyledUser = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  cursor: pointer;
  :hover {
    ${UserMenu} {
      display: block;
    }
  }
`;

const UserWrap = styled.div`
  width: 36px;
  height: 36px;
  margin-right: 10px;
`;

const StyledIcon = styled(Icon)`
  width: 100%;
`;

const StyledText = styled(Text)`
  width: 115px;
  padding-right: 30px;
  overflow: hidden;
  text-overflow: ellipsis;
  ::after {
    content: "";
    position: absolute;
    top: 50%;
    right: 5px;
    display: inline-block;
    padding: 5px;
    border: solid black;
    border-width: 0 1px 1px 0;
    transform: translateY(-50%) rotate(45deg);
  }
`;

const HeaderUser = ({ name, onLogOut }) => {
  return (
    <StyledUser>
      <UserWrap>
        <StyledIcon icon="user" />
      </UserWrap>
      <StyledText>{name}</StyledText>
      <UserMenu right user>
        <Text onClick={onLogOut}>Выйти</Text>
      </UserMenu>
    </StyledUser>
  );
};

HeaderUser.propTypes = { ...HeaderTypes };

export default HeaderUser;
