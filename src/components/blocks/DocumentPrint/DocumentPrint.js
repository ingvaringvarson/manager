import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";
import styled from "styled-components";

import ButtonPrimary from "../../../designSystem/atoms/Button";
import CommonModal from "../../common/modals/CommonModal";
import { Table } from "../../common/tableView";
import PreLoader from "../../common/PreLoader";

import { ContainerContext } from "../../common/formControls";
import { TableContainer } from "../../common/tableControls";

import {
  templatesForEntitySelector,
  fetchTemplates,
  createTemplates,
  moduleName as moduleNameTemplates,
  availableTemplatesForEntitySelector
} from "../../../ducks/templates";

import {
  fetchEnumerations,
  enumerationsSelector,
  moduleName as moduleNameEnumerations
} from "../../../ducks/enumerations";

import { modulesIsLoadingSelector } from "../../../ducks/logger";

import {
  fields,
  formFields
} from "../../../fields/formFields/documentPrint/fieldsForDocumentPrintForm";

import { keyRouterSelector } from "../../../redux/selectors/router";
import { reduceValuesToFormFields } from "../../../fields/helpers";
import { generateId } from "../../../utils";
import documentObjectTypeId from "../../../constants/documentObjectTypeId";

const enumerationNames = ["documentTemplate", "documentsType"];

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  margin-right: 10px;
`;

class DocumentPrint extends PureComponent {
  static propTypes = {
    handleClose: PropTypes.func.isRequired,
    title: PropTypes.string,
    getNormalizeCallback: PropTypes.func,

    idEntity: PropTypes.string.isRequired,
    objectTypeId: PropTypes.number.isRequired,
    entityFirmId: PropTypes.string,
    entityIds: PropTypes.arrayOf(PropTypes.string),
    availableIds: PropTypes.arrayOf(PropTypes.number.isRequired),

    templates: PropTypes.arrayOf(PropTypes.object),
    fetchTemplates: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired,
    createTemplates: PropTypes.func.isRequired
  };

  static defaultProps = {
    title: "Печать документа"
  };

  constructor(props) {
    super(props);

    if (props.getNormalizeCallback) {
      this.getNormalizeCallback = props.getNormalizeCallback;
    }
  }

  state = {
    selected: []
  };

  fetchTemplates = () =>
    this.props.fetchTemplates(this.props.objectTypeId, this.props.idEntity);

  componentDidMount = () => {
    this.props.fetchEnumerations(enumerationNames);
    this.fetchTemplates();
  };

  componentDidUpdate = prevProps => {
    if (prevProps.keyRouter !== this.props.keyRouter) {
      this.fetchTemplates();
    }
  };

  getExtInfo = () => {
    const { idEntity, entityIds } = this.props;
    switch (this.props.objectTypeId) {
      case documentObjectTypeId.bills:
        return { bills_id: idEntity };
      case documentObjectTypeId.orders:
        return { orders_id: entityIds ? entityIds : [idEntity] };
      case documentObjectTypeId.purchase_orders:
        return { purchaseorders_id: entityIds ? entityIds : [idEntity] };
      case documentObjectTypeId.returns:
        return { returns_id: entityIds ? entityIds : [idEntity] };
      case documentObjectTypeId.purchase_returns:
        return { purchasereturns_id: entityIds ? entityIds : [idEntity] };
      case documentObjectTypeId.contracts:
        return { contract_id: idEntity };
      case documentObjectTypeId.tasks:
        return { tasks_id: entityIds ? entityIds : [idEntity] };
    }

    return {};
  };

  getNormalizeCallback = selected => values => {
    const valuesMap = {};

    for (const prop in values) {
      const [propName, mapId] = prop.split("__");
      if (valuesMap[mapId] === undefined) {
        valuesMap[mapId] = {};
      }
      valuesMap[mapId][propName] = values[prop];
    }

    const templates = [];

    for (const mapProp in valuesMap) {
      if (selected.includes(mapProp)) {
        const { id, doc_types, with_stamp } = valuesMap[mapProp];
        templates.push({
          count: 1,
          template: {
            firm_id: this.props.entityFirmId || null,
            id: +id,
            doc_type: +doc_types,
            ext_info: this.getExtInfo(),
            with_stamp: !!with_stamp
          }
        });
      }
    }

    return { templates };
  };

  handleFormSubmit = formValues => {
    const { idEntity, objectTypeId } = this.props;
    this.props.createTemplates(
      formValues,
      {
        typeEntity: objectTypeId,
        idEntity
      },
      this.getNormalizeCallback(this.state.selected)
    );
    this.props.handleClose();
  };

  handleChangeSelected = selected => this.setState({ selected });

  contentStyles = {
    width: 600
  };

  renderCells = memoizeOne((values, enumerations) => {
    values = { fields: values };
    return {
      cells: formFields.reduce(
        reduceValuesToFormFields(values, enumerations),
        []
      ),
      key: generateId()
    };
  });

  getTableData = memoizeOne((templates, enumerations) => ({
    templates,
    enumerations,
    key: generateId()
  }));

  renderTable = (renderCallBacks, { selected }) => {
    const { key, templates, enumerations } = this.getTableData(
      this.props.templates,
      this.props.enumerations
    );

    return (
      <>
        <Table
          key={key}
          cells={fields}
          entities={templates}
          enumerations={enumerations}
          hasChildrenBefore
          renderRowHeadChildrenBefore={
            renderCallBacks.renderRowHeadChildrenBefore
          }
          renderRowBodyChildrenBefore={
            renderCallBacks.renderRowBodyChildrenBefore
          }
          hasHeadFixed={false}
        />
        <ButtonWrapper>
          <ButtonPrimary disabled={!selected.length}>Печать</ButtonPrimary>
        </ButtonWrapper>
      </>
    );
  };

  renderForm = () => {
    const { templates } = this.getTableData(
      this.props.templates,
      this.props.enumerations
    );

    return (
      <TableContainer
        canBeSelected
        entities={templates}
        renderTable={this.renderTable}
        handleChangeSelected={this.handleChangeSelected}
      />
    );
  };

  render() {
    const { cells, key } = this.renderCells(
      this.props.templates,
      this.props.enumerations
    );

    return (
      <CommonModal
        isOpen
        handleClose={this.props.handleClose}
        title="Печать документа"
        contentStyles={this.contentStyles}
      >
        {this.props.isLoading ? (
          <PreLoader />
        ) : (
          <ContainerContext
            key={key}
            fields={cells}
            onSubmitForm={this.handleFormSubmit}
            renderForm={this.renderForm}
          />
        )}
      </CommonModal>
    );
  }
}

const moduleNames = [moduleNameEnumerations, moduleNameTemplates];

export default connect(
  (state, props) => ({
    templates: availableTemplatesForEntitySelector(state, {
      ...props,
      typeEntity: props.objectTypeId
    }),
    isLoading: modulesIsLoadingSelector(state, { moduleNames }),
    enumerations: enumerationsSelector(state, { enumerationNames }),
    keyRouter: keyRouterSelector(state)
  }),
  {
    fetchTemplates,
    fetchEnumerations,
    createTemplates
  }
)(DocumentPrint);
