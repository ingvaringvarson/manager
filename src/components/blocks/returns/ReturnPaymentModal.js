import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import ReturnPaymentForm from "../../forms/ReturnPaymentForm";
import PreLoader from "../../common/PreLoader";
import Modal from "../helperBlocks/Modal";

import {
  fetchClient,
  clientInfoSelector,
  moduleName as clientsModuleName
} from "../../../ducks/clients";
import { modulesIsLoadingSelector } from "../../../ducks/logger";

const moduleNames = [clientsModuleName];

class ReturnPaymentModal extends PureComponent {
  static propTypes = {
    returnEntity: PropTypes.object,
    handleClose: PropTypes.func.isRequired,

    // ToDo: перенести запрос клиента непосредственно в ReturnPaymentForm
    client: PropTypes.object.isRequired,
    fetchClient: PropTypes.func.isRequired
  };

  fetchClient = () => {
    const { fetchClient, returnEntity, client, isLoading } = this.props;
    const clientId = returnEntity.agent_id;

    if (isLoading || (client && client.id === clientId)) {
      return;
    }

    fetchClient(clientId, { id: clientId });
  };

  componentDidMount = () => this.fetchClient();

  componentDidUpdate = () => this.fetchClient();

  render() {
    const { returnEntity, client, handleClose } = this.props;

    return (
      <Modal
        title="Вернуть оплату"
        subtitle={`Возврат № ${returnEntity.code}`}
        isOpen
        handleClose={handleClose}
      >
        {this.props.isLoading ? (
          <PreLoader />
        ) : (
          <ReturnPaymentForm returnEntity={returnEntity} client={client} />
        )}
      </Modal>
    );
  }
}

export default connect(
  (state, props) => {
    const { returnEntity } = props;
    return {
      isLoading: modulesIsLoadingSelector(state, { moduleNames }),
      client: clientInfoSelector(state, {
        idEntity: returnEntity.id,
        id: returnEntity.agent_id
      })
    };
  },
  { fetchClient }
)(ReturnPaymentModal);
