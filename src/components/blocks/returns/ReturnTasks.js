import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import {
  fetchTasksForEntity,
  tasksForEntitySelector,
  requestParamsForEntitySelector,
  maxPagesForEntitySelector
} from "../../../ducks/tasks";
import { keyRouterSelector } from "../../../redux/selectors/router";
import { TableContainer } from "../../common/tableControls";
import { ContainerContext } from "../../common/formControls";
import Pagination from "../Pagination";
import Table from "../../common/table/Table";
import { generateUrlWithQuery, generateId } from "../../../utils";
import { reduceValuesToFormFields } from "../../../fields/helpers";
import { fields } from "../../../fields/formFields/returns/fieldsOfTasksTable";

const typeEntity = "returns";

class ReturnTasks extends PureComponent {
  static propTypes = {
    keyRouter: PropTypes.string.isRequired,
    returnEntity: PropTypes.object,
    requestParams: PropTypes.object,
    enumerations: PropTypes.object,
    pathname: PropTypes.string,
    returnId: PropTypes.string,
    tasks: PropTypes.array
  };

  fetchEntities = () =>
    this.props.fetchTasksForEntity(
      typeEntity,
      this.props.returnId,
      this.props.requestParams
    );

  componentDidMount = () => this.fetchEntities();

  componentDidUpdate = prevProps => {
    if (prevProps.keyRouter !== this.props.keyRouter) {
      this.fetchEntities();
    }
  };

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  renderCells = memoizeOne((tasks, enumerations, requestParams) => {
    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: generateId()
    };
  });

  render() {
    const { cells, key } = this.renderCells(
      this.props.tasks,
      this.props.enumerations,
      this.props.requestParams
    );

    return (
      <>
        <ContainerContext fields={cells} onSort={this.handleSort} key={key}>
          <TableContainer canBeSelected entities={this.props.tasks}>
            <Table
              cells={cells}
              enumerations={this.props.enumerations}
              entities={this.props.tasks}
            />
          </TableContainer>
        </ContainerContext>
        <Pagination
          requestParams={this.props.requestParams}
          pathname={this.props.pathname}
          push={this.props.push}
          page={+this.props.requestParams.page}
          maxPages={this.props.maxPages}
        />
      </>
    );
  }
}

export default connect(
  (state, props) => {
    const returnId =
      props.returnEntity &&
      props.returnEntity.return &&
      props.returnEntity.return.id;

    return {
      tasks: tasksForEntitySelector(state, {
        idEntity: returnId,
        typeEntity
      }).entities,
      returnId,
      keyRouter: keyRouterSelector(state),
      requestParams: requestParamsForEntitySelector(state, {
        idEntity: returnId,
        typeEntity: "return_id"
      }),
      maxPages: maxPagesForEntitySelector(state, {
        idEntity: returnId,
        typeEntity
      })
    };
  },
  {
    push,
    fetchTasksForEntity
  }
)(ReturnTasks);
