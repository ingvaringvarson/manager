import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchReturn } from "../../../ducks/returns";
import { fields } from "../../../fields/formFields/returns/fieldsForReturnPositions";

import NestedTableForEntity from "../positions/NestedTableForEntity";
import ReturnPositionsMenu from "./ReturnPositionsMenu";

class ReturnPositionsList extends PureComponent {
  static propTypes = {
    enumerations: PropTypes.object,
    returnEntity: PropTypes.object
  };

  componentDidMount() {
    this.fetchEntities();
  }

  fetchEntities = () => {
    const { id } = this.props.returnEntity.return;
    this.props.fetchReturn(id, { id, isForRepeat: true });
  };

  renderPositionsMenu = ({ forList, positions }) => (
    <ReturnPositionsMenu
      forList={forList}
      positions={positions}
      returnEntity={this.props.returnEntity}
      fetchEntities={this.fetchEntities}
    />
  );

  render() {
    return (
      <NestedTableForEntity
        renderPositionsMenu={this.renderPositionsMenu}
        entity={this.props.returnEntity}
        positions={this.props.returnEntity.return.positions || []}
        enumerations={this.props.enumerations}
        tableParams={null}
        fields={fields}
        isNested={false}
      />
    );
  }
}

export default connect(
  null,
  { fetchReturn }
)(ReturnPositionsList);
