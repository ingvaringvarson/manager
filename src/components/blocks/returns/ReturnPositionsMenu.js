import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import DividingModal from "../positions/DividingModal";
import PositionsReject from "../positions/RejectModal";
import CommandsByStatusMenu from "../../common/menus/CommandsByStatusMenu";

import { memoizeToArray } from "../../../helpers/cache";

import returnPositionsCommands, {
  filterReturnPositionsCommands
} from "../../../commands/returnPositions";

import { typeMethods } from "../../../ducks/positions";

export default class ReturnPositionsMenu extends PureComponent {
  static propTypes = {
    forList: PropTypes.bool,
    positions: PropTypes.array.isRequired,
    // ToDo: возможно, здесь должен находится returnEntity.return
    returnEntity: PropTypes.object.isRequired
  };

  static defaultProps = {
    forList: false
  };

  modalIsNotImplementedFunc = (_, command) =>
    alert(`Функциональность ${command.title} пока не добавлена`);

  commands = [
    {
      ...returnPositionsCommands.divideCommand,
      renderModal: (handleClose, positions, returnEntity) => (
        <DividingModal
          typeMethod={typeMethods.returns}
          positions={positions}
          handleClose={handleClose}
          entity={returnEntity}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...returnPositionsCommands.rejectInReturnCommand,
      renderModal: (handleClose, positions, returnEntity) => (
        <PositionsReject
          handleClose={handleClose}
          entities={memoizeToArray(returnEntity)}
          positions={positions}
          typeMethod={typeMethods.returns}
          fetchEntities={this.props.fetchEntities}
        />
      )
    }
  ];

  render() {
    const { positions, returnEntity, forList } = this.props;
    return (
      <CommandsByStatusMenu
        forList={forList}
        entityOrEntities={positions}
        parentEntity={returnEntity}
        commands={this.commands}
        commandsToMenuOptionsFilter={filterReturnPositionsCommands}
      />
    );
  }
}
