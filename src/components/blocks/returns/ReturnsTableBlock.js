import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import BlockContent from "../../../designSystem/organisms/BlockContent";
import TableCell from "../../../designSystem/molecules/TableCell";
import TableRow from "../../../designSystem/molecules/TableRow";
import Block from "../../../designSystem/organisms/Block";
import Box from "../../../designSystem/organisms/Box";
import Title from "../../../designSystem/atoms/Title";

import ClearButtonHeading from "../../common/styled/ClearButtonHeading";
import NestedTableForEntity from "../positions/NestedTableForEntity";
import ReturnsNestedContentAfter from "./ReturnsNestedContentAfter";
import TableWithLinks from "../../common/table/TableWithLinks";
import OrdersTabLinks from "../helperBlocks/OrdersTabLinks";
import ReturnPositionsMenu from "./ReturnPositionsMenu";
import ReturnsMenu from "./ReturnsMenu";
import Pagination from "../Pagination";

import {
  TableContainer,
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenBefore
} from "../../common/tableControls";
import { FormContext } from "../../common/formControls";

import { fetchReturns, returnListSelector } from "../../../ducks/returns";

import { returnFields } from "../../../fields/formFields/positions/fieldsForList";
import { memoizeToArray } from "./../../../helpers/cache";
import { generateId } from "../../../utils";

class ReturnsTableBlock extends PureComponent {
  static propTypes = {
    requestParams: PropTypes.object,
    enumerations: PropTypes.object,
    keyRouter: PropTypes.string,
    pathname: PropTypes.string,
    maxPages: PropTypes.number,

    fetchReturns: PropTypes.func.isRequired,
    returns: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired
  };

  fetchEntities = () => {
    this.props.fetchReturns({
      ...this.props.requestParams,
      isForRepeat: true
    });
  };

  componentDidMount = () => {
    this.fetchEntities();
  };

  componentDidUpdate = prevProps => {
    if (prevProps.keyRouter !== this.props.keyRouter) {
      this.fetchEntities();
    }
  };

  renderTableHead = () => {
    return (
      <TableRow>
        <TableCell flex="0 1 80px" />
        <TableCell flex="3 1 0">
          <Title>Клиент</Title>
        </TableCell>
        <TableCell flex="7 1 0">
          <Title>Возврат</Title>
        </TableCell>
      </TableRow>
    );
  };

  renderNestedTableAfterContent = entity => {
    return <ReturnsNestedContentAfter entity={entity} />;
  };

  renderPositionsMenu = returnEntity => ({ forList, positions }) => (
    <ReturnPositionsMenu
      forList={forList}
      positions={positions}
      returnEntity={returnEntity}
      fetchEntities={this.fetchEntities}
    />
  );

  renderNestedTable = ({ cells, entity, entities, enumerations }) => {
    const { positions } = entity.return;
    if (!(positions && Array.isArray(positions) && positions.length)) {
      return null;
    }

    return (
      <NestedTableForEntity
        renderPositionsMenu={this.renderPositionsMenu(entity)}
        entity={entity}
        enumerations={enumerations}
        fields={returnFields}
        positions={positions}
        renderAfterContent={this.renderNestedTableAfterContent}
      />
    );
  };

  renderOptionControl = ({ entity }) => {
    return (
      <ReturnsMenu
        returns={memoizeToArray(entity)}
        fetchEntities={this.fetchEntities}
      />
    );
  };

  renderSelectedOptionsControl = ({ entities }) => (
    <ReturnsMenu
      returns={entities}
      forList
      fetchEntities={this.fetchEntities}
    />
  );

  renderTable = () => {
    const { fields, returns, enumerations } = this.props;
    return (
      <TableContainer entities={returns} canBeSelected hasNestedTable>
        <TableWithLinks
          cells={fields}
          entities={returns}
          enumerations={enumerations}
          hasFormControl
          hasChildrenBefore
          hasChildrenAfter
          renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
          renderRowHeadChildrenAfter={this.renderSelectedOptionsControl}
          renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
          renderRowBodyAfter={this.renderNestedTable}
          renderRowFormControlChildrenBefore={
            renderRowFormControlChildrenBefore
          }
          renderRowBodyChildrenAfter={this.renderOptionControl}
          renderBeforeContent={this.renderTableHead}
          typeEntity="return"
        />
      </TableContainer>
    );
  };

  renderKey = memoizeOne(() => {
    return {
      key: generateId()
    };
  });

  render() {
    const {
      fields,
      returns,
      enumerations,
      requestParams,
      pathname,
      push,
      maxPages
    } = this.props;
    const { key } = this.renderKey(returns);

    return (
      <Block>
        <Box position="relative">
          <OrdersTabLinks />
          <ClearButtonHeading title="Сбросить все" fields={fields} />
        </Box>
        <BlockContent>
          <FormContext>
            <TableContainer
              entities={returns}
              key={key}
              canBeSelected
              hasNestedTable
            >
              <TableWithLinks
                cells={fields}
                entities={returns}
                enumerations={enumerations}
                hasFormControl
                hasChildrenBefore
                hasChildrenAfter
                renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
                renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
                renderRowBodyAfter={this.renderNestedTable}
                renderRowFormControlChildrenBefore={
                  renderRowFormControlChildrenBefore
                }
                renderRowBodyChildrenAfter={this.renderOptionControl}
                renderBeforeContent={this.renderTableHead}
                typeEntity="return"
              />
            </TableContainer>
          </FormContext>
          <Pagination
            requestParams={requestParams}
            pathname={pathname}
            push={push}
            page={+requestParams.page}
            maxPages={maxPages || 99}
          />
        </BlockContent>
      </Block>
    );
  }
}

export default connect(
  state => {
    return {
      returns: returnListSelector(state)
    };
  },
  {
    fetchReturns,
    push
  }
)(ReturnsTableBlock);
