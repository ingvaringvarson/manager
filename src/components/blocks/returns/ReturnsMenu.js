import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";

import CommandsByStatusMenu from "../../common/menus/CommandsByStatusMenu";
import ReturnPaymentModal from "./ReturnPaymentModal";
import DocumentPrint from "../DocumentPrint";
import PositionsReject from "../positions/RejectModal";

import { updateReturn } from "./../../../ducks/returns";

import returnsCommands, {
  filterReturnsCommands,
  getMainCommandByReturn
} from "../../../commands/returns";

import documentObjectTypeId from "../../../constants/documentObjectTypeId";
import { typeMethods } from "../../../ducks/positions";

class ReturnsMenu extends PureComponent {
  static propTypes = {
    returns: PropTypes.arrayOf(PropTypes.object).isRequired,
    forList: PropTypes.bool,
    useMainCommand: PropTypes.bool,
    fetchEntities: PropTypes.func.isRequired,

    updateReturn: PropTypes.func.isRequired
  };

  static defaultProps = {
    forList: false,
    useMainCommand: false
  };

  modalIsNotImplementedFunc = (_, command) =>
    alert(`Функциональность ${command.title} пока не добавлена`);

  handleConfirmClick = () => {
    const { returns, updateReturn } = this.props;
    const returnEntity = returns[0].return;
    updateReturn(returnEntity.id, returnEntity, {
      state_return: 2
    });
  };

  getPositionsOfReturns = memoizeOne(returnEntities => {
    return returnEntities.reduce((res, returnEntity) => {
      if (!Array.isArray(returnEntity.return.positions)) return res;

      returnEntity.return.positions.forEach(p => res.push(p));
      return res;
    }, []);
  });

  commands = [
    {
      ...returnsCommands.acceptReturnCommand,
      onClick: this.handleConfirmClick
    },
    {
      ...returnsCommands.returnPaymentCommand,
      renderModal: (handleClose, [returnEntity]) => (
        <ReturnPaymentModal
          returnEntity={returnEntity.return}
          handleClose={handleClose}
        />
      )
    },
    {
      ...returnsCommands.takeProductCommand,
      onClick: this.modalIsNotImplementedFunc
    },
    {
      ...returnsCommands.printDocumentsCommand,
      renderModal: (handleClose, [returnEntity]) => (
        <DocumentPrint
          idEntity={returnEntity.return.id}
          objectTypeId={documentObjectTypeId.returns}
          entityFirmId={returnEntity.return.firm_id}
          handleClose={handleClose}
        />
      )
    },
    {
      ...returnsCommands.createReturnCommand,
      renderModal: (handleClose, returnEntities) => (
        <PositionsReject
          typeMethod={typeMethods.returns}
          fetchEntities={this.props.fetchEntities}
          positions={this.getPositionsOfReturns(returnEntities)}
          entities={returnEntities}
          handleClose={handleClose}
        />
      )
    }
  ];

  getMainCommand = memoizeOne(([returnEntity]) =>
    getMainCommandByReturn(returnEntity.return)
  );

  render() {
    const { returns, forList, useMainCommand } = this.props;
    const mainCommandId = useMainCommand ? this.getMainCommand(returns) : null;
    return (
      <CommandsByStatusMenu
        forList={forList}
        entityOrEntities={returns}
        commands={this.commands}
        mainCommandId={mainCommandId}
        commandsToMenuOptionsFilter={filterReturnsCommands}
      />
    );
  }
}

export default connect(
  null,
  { updateReturn }
)(ReturnsMenu);
