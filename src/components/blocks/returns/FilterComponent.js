import React, { memo } from "react";
import PropTypes from "prop-types";

import Block from "../../../designSystem/organisms/Block";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import ClearButtonHeading from "../../common/styled/ClearButtonHeading";
import Heading from "../../../designSystem/molecules/Heading";
import { FormContext } from "../../common/formControls";
import FieldForm from "../../common/form/FieldForm";

const propTypes = {
  fields: PropTypes.array
};

const FilterComponent = memo(props => {
  const { fields } = props;
  return (
    <Block>
      <Heading title="Фильтрация">
        <ClearButtonHeading title="Сбросить все" fields={fields} />
      </Heading>
      <BlockContent>
        <FormContext>
          {fields.map((field, index) => {
            return <FieldForm field={field} index={index} />;
          })}
        </FormContext>
      </BlockContent>
    </Block>
  );
});

FilterComponent.propTypes = propTypes;

export default FilterComponent;
