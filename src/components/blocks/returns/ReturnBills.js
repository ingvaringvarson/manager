import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { push } from "connected-react-router";
import {
  keyRouterSelector,
  queryRouterSelector
} from "../../../redux/selectors/router";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";
import {
  billsForEntitySelector,
  fetchBillsForEntity
} from "../../../ducks/bills";
import { requestParamsBillsForReturnPageSelector } from "../../../ducks/returns";
import { generateUrlWithQuery } from "../../../utils";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../../fields/helpers";
import Table from "../../common/tableView/Table";
import TableWithLinks from "../../common/table/TableWithLinks";
import { fields } from "../../../fields/formFields/orders/fieldsForOrderBills";
import { ContainerContext } from "../../common/formControls";
import {
  TableContainer,
  ToggleOpenRow,
  renderRowFormControlChildrenAfter,
  renderRowHeadChildrenAfter
} from "../../common/tableControls";
import Container from "../../../designSystem/templates/Container";
import Row from "../../../designSystem/templates/Row";
import Column from "../../../designSystem/templates/Column";
import TableCell from "../../../designSystem/molecules/TableCell";

import Pagination from "../Pagination";
import BillMenu from "../bills/BillMenu";

import { generateId } from "../../../utils";
import { billPositionFields } from "../../../fields/formFields/returns/fieldsForBillsInner";
import PaymentsForBill from "../payments/PaymentsForBill";
import { memoizeToArray } from "../../../helpers/cache";

const subTableParams = {
  subTable: true,
  ml: "80px",
  mt: "8px",
  mb: "16px"
};

const typeEntity = "returns";

class ReturnBills extends PureComponent {
  static propTypes = {
    // comes from parent Component
    returnEntity: PropTypes.object,
    requestParams: PropTypes.object,
    enumerations: PropTypes.object,
    pathname: PropTypes.string,
    // comes from connect
    selectedTab: PropTypes.number,
    query: PropTypes.object,
    idEntity: PropTypes.string,
    bills: PropTypes.shape({
      entities: PropTypes.array.isRequired
    }),
    keyRouter: PropTypes.string.isRequired,
    push: PropTypes.func.isRequired,
    fetchBillsForEntity: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchBillsForEntity(
      typeEntity,
      this.props.idEntity,
      this.props.requestParams
    );
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.props.fetchBillsForEntity(
        typeEntity,
        this.props.idEntity,
        this.props.requestParams
      );
    }
  }

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleChangeTab = id => {
    const { pathname, query } = this.props;
    const queryForRootTab = `?_tab=${query._tab || 1}`;
    const queryForTab = id === 1 ? "" : `&_tab_state_bill=${id}`;
    this.props.push(`${pathname}${queryForRootTab}${queryForTab}`);
  };

  renderCells = memoizeOne(enumerations => {
    const { requestParams } = this.props;

    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: generateId()
    };
  });

  renderRowBodyChildrenAfter = ({ entity }) => (
    <TableCell control>
      <BillMenu
        bills={memoizeToArray(entity)}
        client={this.props.client}
        key={entity.id}
      />
    </TableCell>
  );

  renderNestedTable = ({ entity }) => {
    const { enumerations } = this.props;
    const { positions } = entity;

    return (
      <ToggleOpenRow id={entity._id}>
        <Container>
          <Row>
            {positions && Array.isArray(positions) && positions.length && (
              <Column basis="50%">
                <TableContainer>
                  <Table
                    tableProps={subTableParams}
                    entities={positions}
                    cells={billPositionFields}
                    enumerations={enumerations}
                  />
                </TableContainer>
              </Column>
            )}
            <Column basis="50%">
              <PaymentsForBill payments={entity.payments_inf} />
            </Column>
          </Row>
        </Container>
      </ToggleOpenRow>
    );
  };

  renderTable = renderCallBacks => {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.bills
    );

    return (
      <TableWithLinks
        key={key}
        cells={cells}
        entities={this.props.bills.entities}
        enumerations={this.props.enumerations}
        hasFormControl
        hasChildrenBefore
        hasChildrenAfter
        renderRowHeadChildrenBefore={
          renderCallBacks.renderRowHeadChildrenBefore
        }
        renderRowBodyChildrenBefore={
          renderCallBacks.renderRowBodyChildrenBefore
        }
        renderRowBodyChildrenAfter={this.renderRowBodyChildrenAfter}
        renderRowHeadChildrenAfter={renderRowHeadChildrenAfter}
        renderRowFormControlChildrenAfter={renderRowFormControlChildrenAfter}
        renderRowFormControlChildrenBefore={
          renderCallBacks.renderRowFormControlChildrenBefore
        }
        renderRowBodyAfter={this.renderNestedTable}
        typeEntity="bill"
      />
    );
  };

  render() {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.bills
    );

    return (
      <TabContainer
        selectedTabId={this.props.selectedTab}
        onChangeSelected={this.handleChangeTab}
      >
        <TabHeadContainer>
          <TabHead id={1}>Активные</TabHead>
          <TabHead id={2}>Аннулированные</TabHead>
        </TabHeadContainer>
        <TabBodyContainer>
          <TabBody id={1}>
            <ContainerContext
              key={key}
              onSort={this.handleSort}
              fields={cells}
              onSubmitForm={this.handleSubmitForm}
              autoSubmit
            >
              <TableContainer
                entities={this.props.bills.entities}
                canBeSelected
                hasNestedTable
                renderTable={this.renderTable}
              />
            </ContainerContext>
          </TabBody>
          <TabBody id={2}>
            <ContainerContext
              key={key}
              onSort={this.handleSort}
              fields={cells}
              onSubmitForm={this.handleSubmitForm}
              autoSubmit
            >
              <TableContainer
                entities={this.props.bills.entities}
                canBeSelected
                hasNestedTable
                renderTable={this.renderTable}
              />
            </ContainerContext>
          </TabBody>
          <Pagination
            requestParams={this.props.requestParams}
            pathname={this.props.pathname}
            push={this.props.push}
            page={+this.props.requestParams.page}
            maxPages={this.props.bills.maxPages || 99}
          />
        </TabBodyContainer>
      </TabContainer>
    );
  }
}

export default compose(
  withRouter,
  connect(
    (state, props) => {
      const { returnEntity } = props;
      const query = queryRouterSelector(state);
      const selectedTab = query.hasOwnProperty("_tab_state_bill")
        ? +query._tab_state_bill
        : 1;
      const idEntity = `${returnEntity.id}_${selectedTab}`;

      const requestParams = requestParamsBillsForReturnPageSelector(
        state,
        props
      );

      return {
        selectedTab,
        query,
        idEntity,
        bills: billsForEntitySelector(state, { typeEntity, idEntity }),
        keyRouter: keyRouterSelector(state),
        requestParams
      };
    },
    { push, fetchBillsForEntity }
  )
)(ReturnBills);
