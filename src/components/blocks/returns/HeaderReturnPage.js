import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import Column from "../../../designSystem/templates/Column";
import Row from "../../../designSystem/templates/Row";

import HeadingCard from "../../common/headlines/HeadingCard";
import HeadingInfo from "../../common/headlines/HeadingInfo";

import ReturnsMenu from "./ReturnsMenu";

import { memoizeToArray } from "./../../../helpers/cache";

export default class HeaderReturnPage extends PureComponent {
  static propTypes = {
    returnEntity: PropTypes.object,
    enumerations: PropTypes.object,
    fetchEntities: PropTypes.func.isRequired
  };

  renderReturnStatus = (returnEntity, enumerations) => {
    const state = returnEntity.return && returnEntity.return.state_return;
    const stateEnumMap =
      enumerations && enumerations.returnState && enumerations.returnState.map;
    if ((!state && state !== 0) || !stateEnumMap) return null;

    return stateEnumMap[state] && stateEnumMap[state].name;
  };

  render() {
    if (!this.props.returnEntity.return) return null;

    const { returnEntity, enumerations, fetchEntities } = this.props;

    const returnStatus = this.renderReturnStatus(returnEntity, enumerations);

    const returnSum = `${returnEntity.return.sum_payment} руб.`;

    return (
      <Row alignCenter justifyBetween mb="20px">
        <Column>
          <HeadingCard
            title="Карточка возврата"
            name={`id ${returnEntity.return.code}`}
            status={returnStatus}
          />
        </Column>
        <Column>
          <HeadingInfo subTitle="Сумма к возврату" info={returnSum} />
        </Column>
        <Column>
          <ReturnsMenu
            returns={memoizeToArray(returnEntity)}
            useMainCommand
            fetchEntities={fetchEntities}
          />
        </Column>
      </Row>
    );
  }
}
