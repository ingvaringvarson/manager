import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { pathnameRouterSelector } from "../../../redux/selectors/router";
import { requestParamsReturnPageSelector } from "../../../ducks/returns";

import ReturnPositionsList from "./ReturnPositionsList";
import ReturnBills from "./ReturnBills";
import ReturnTasks from "./ReturnTasks";

import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";

import BlockContent from "../../../designSystem/organisms/BlockContent";
import Documents from "../Documents";
import documentObjectTypeId from "../../../constants/documentObjectTypeId";

class ReturnTabs extends PureComponent {
  static propTypes = {
    requestParams: PropTypes.object,
    enumerations: PropTypes.object,
    returnEntity: PropTypes.object,
    client: PropTypes.object
  };

  handleTabChanged = id => {
    this.props.push(`${this.props.pathname}?_tab=${id}`);
  };

  render() {
    const {
      returnEntity,
      requestParams,
      enumerations,
      pathname,
      client
    } = this.props;

    return (
      <TabContainer
        selectedTabId={requestParams._tab}
        onChangeSelected={this.handleTabChanged}
      >
        <TabHeadContainer>
          <TabHead id={1}>Позиции возврата</TabHead>
          <TabHead id={2}>Счета</TabHead>
          {/*<TabHead id={3}>Платежи</TabHead>*/}
          <TabHead id={4}>Задачи</TabHead>
          <TabHead id={5}>Файлы</TabHead>
          <TabHead id={6}>Документы</TabHead>
        </TabHeadContainer>
        <TabBodyContainer>
          <BlockContent table>
            <TabBody id={1}>
              <ReturnPositionsList
                returnEntity={returnEntity}
                requestParams={requestParams}
                enumerations={enumerations}
              />
            </TabBody>
            <TabBody id={2}>
              <ReturnBills
                returnEntity={returnEntity}
                requestParams={requestParams}
                enumerations={enumerations}
                pathname={pathname}
                client={client}
              />
            </TabBody>
            {/*<TabBody id={3}>
              <ReturnPayments
                returnEntity={returnEntity}
                enumerations={enumerations}
              />
            </TabBody>*/}
            <TabBody id={4}>
              <ReturnTasks
                returnEntity={returnEntity}
                requestParams={requestParams}
                enumerations={enumerations}
                pathname={pathname}
              />
            </TabBody>
            <TabBody id={5}>Tab 5 COMPONENT BODY</TabBody>
            <TabBody id={6}>
              <Documents
                idEntity={returnEntity.return.id}
                typeEntity={documentObjectTypeId.returns}
              />
            </TabBody>
          </BlockContent>
        </TabBodyContainer>
      </TabContainer>
    );
  }
}

export default connect(
  state => {
    return {
      pathname: pathnameRouterSelector(state),
      requestParams: requestParamsReturnPageSelector(state)
    };
  },
  { push }
)(ReturnTabs);
