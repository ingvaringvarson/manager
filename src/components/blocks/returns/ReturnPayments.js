import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import {
  paymentsForEntitySelector,
  fetchPaymentsForEntity
} from "../../../ducks/payments";
import {
  keyRouterSelector,
  pathnameRouterSelector,
  queryRouterSelector
} from "../../../redux/selectors/router";
import { requestParamsForPaymentsTabPageSelector } from "../../../ducks/returns";
import { enumerationListWithoutZeroIdSelector } from "../../../ducks/enumerations";
import { fieldsForPaymentMethods } from "../../../fields/formFields/orders/fieldsForOrderPayments";
import { push } from "connected-react-router";
import { generateUrlWithQuery } from "../../../utils";
import { ContainerContext } from "../../common/formControls";
import { TableContainer } from "../../common/tableControls";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../../fields/helpers";
import memoizeOne from "memoize-one";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";
import Pagination from "../Pagination";
import Table from "../../common/tableView/Table";
import { getUrl } from "../../../helpers/urls";
import { generateId } from "../../../utils";

const typeEntity = "return";

class ReturnPayments extends PureComponent {
  static propTypes = {
    // comes from parent Component
    returnEntity: PropTypes.object,
    requestParams: PropTypes.object,
    enumerations: PropTypes.object,
    pathname: PropTypes.string,
    // comes from connect
    selectedTab: PropTypes.number,
    query: PropTypes.object,
    idEntity: PropTypes.string,
    payments: PropTypes.array,
    keyRouter: PropTypes.object,
    push: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchPaymentsForEntity(
      typeEntity,
      this.props.idEntity,
      this.props.requestParams
    );
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.props.fetchPaymentsForEntity(
        typeEntity,
        this.props.idEntity,
        this.props.requestParams
      );
    }
  }

  renderCells = memoizeOne(enumerations => {
    const { fields, requestParams } = this.props;

    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: generateId()
    };
  });

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleChangeTab = id => {
    const { pathname, query } = this.props;
    const queryForRootTab = `?_tab=${query._tab || 1}`;
    const queryForTab = id === 1 ? "" : `&payment_method=${id}`;
    this.props.push(`${pathname}${queryForRootTab}${queryForTab}`);
  };

  handleMoveToPaymentPage = ({ entity }) => () => {
    if (entity) this.props.push(getUrl("payments", { id: entity.id }));
  };

  rowBodyHandlers = {
    onClick: this.handleMoveToPaymentPage
  };

  renderTable = renderCallBacks => {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.payments
    );

    return (
      <Table
        key={key}
        cells={cells}
        entities={this.props.payments.entities}
        enumerations={this.props.enumerations}
        hasFormControl
        hasChildrenBefore
        renderRowHeadChildrenBefore={
          renderCallBacks.renderRowHeadChildrenBefore
        }
        renderRowBodyChildrenBefore={
          renderCallBacks.renderRowBodyChildrenBefore
        }
        renderRowFormControlChildrenBefore={
          renderCallBacks.renderRowFormControlChildrenBefore
        }
        rowBodyHandlers={this.rowBodyHandlers}
      />
    );
  };

  render() {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.payments
    );

    return (
      <>
        <TabContainer
          selectedTabId={this.props.requestParams.payment_method}
          onChangeSelected={this.handleChangeTab}
        >
          <TabHeadContainer>
            {this.props.paymentMethods.map(item => {
              return <TabHead id={item.id}>{item.name}</TabHead>;
            })}
          </TabHeadContainer>
          <TabBodyContainer>
            {this.props.paymentMethods.map(item => {
              return (
                <TabBody id={item.id}>
                  <ContainerContext
                    key={key}
                    onSort={this.handleSort}
                    fields={cells}
                    onSubmitForm={this.handleSubmitForm}
                    autoSubmit
                  >
                    <TableContainer
                      entities={this.props.payments.entities}
                      canBeSelected
                      renderTable={this.renderTable}
                    />
                  </ContainerContext>
                  <Pagination
                    requestParams={this.props.requestParams}
                    pathname={this.props.pathname}
                    push={this.props.push}
                    page={+this.props.requestParams.page}
                    maxPages={this.props.payments.maxPages || 99}
                  />
                </TabBody>
              );
            })}
          </TabBodyContainer>
        </TabContainer>
      </>
    );
  }
}

export default compose(
  withRouter,
  connect(
    (state, props) => {
      const requestParams = requestParamsForPaymentsTabPageSelector(
        state,
        props
      );
      const idEntity = `${props.returnEntity.return.id}_${requestParams.payment_method}`;

      return {
        query: queryRouterSelector(state, props),
        paymentMethods: enumerationListWithoutZeroIdSelector(state, {
          name: "paymentMethod"
        }),
        payments: paymentsForEntitySelector(state, {
          idEntity,
          typeEntity
        }),
        idEntity,
        keyRouter: keyRouterSelector(state, props),
        pathname: pathnameRouterSelector(state, props),
        fields: fieldsForPaymentMethods[requestParams.payment_method],
        requestParams
      };
    },
    { fetchPaymentsForEntity, push }
  )
)(ReturnPayments);
