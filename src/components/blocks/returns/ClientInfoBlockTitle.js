import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import Heading from "../../../designSystem/molecules/Heading";
import { routesToMove } from "../../../helpers/routesGenerate";

// ToDo: подумать на счёт того, чтобы это удалить
// возможно использовать стандартный InfoBlock > Heading
class ClientInfoBlockTitle extends PureComponent {
  static propTypes = {
    values: PropTypes.shape({
      client: PropTypes.object,
      returnEntity: PropTypes.object
    }).isRequired,
    enumerations: PropTypes.object.isRequired,
    field: PropTypes.object.isRequired
  };

  handleGoToClient = () => {
    const { client } = this.props.values;

    if (client) {
      window.open(routesToMove.client(client), "_blank");
    }
  };

  iconProps = {
    onClick: this.handleGoToClient
  };

  render() {
    const { client } = this.props.values;
    const subtitle = client ? `id${client.code}` : "";
    return (
      <Heading
        title="Клиент"
        subtitle={subtitle}
        iconProps={this.iconProps}
        icon={client && client.id ? "account_box" : null}
      />
    );
  }
}

export default ClientInfoBlockTitle;
