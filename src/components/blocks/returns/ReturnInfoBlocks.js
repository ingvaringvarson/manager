import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import InfoBlock from "../../common/info/InfoBlock";
import InfoModalControl from "../../common/info/InfoModalControl";

import { infoFields } from "../../../fields/formFields/returns/fieldsForInfoPage";

import { generateId } from "../../../utils";

class ReturnInfoBlocks extends PureComponent {
  static propTypes = {
    returnEntity: PropTypes.object,
    enumerations: PropTypes.object,
    client: PropTypes.object
  };

  renderContent = info => (
    state = {},
    modalProps = null,
    blockProps = null,
    iconProps = null
  ) => (
    <InfoBlock
      iconIsShow={state.iconIsShow}
      info={info}
      blockProps={blockProps}
      iconProps={iconProps}
    />
  );

  getBlock = (returnEntity, enumerations, client) => block => {
    const info = block.info.mapValuesToFields(block.info, enumerations, {
      ...returnEntity,
      client
    });
    return {
      ...block,
      info: {
        ...info,
        helpersData: {
          ...info.helpersData,
          values: {
            returnEntity,
            client
          },
          enumerations,
          field: block.info
        }
      },
      renderContent: block.renderContent
        ? block.renderContent
        : this.renderContent(info),
      key: generateId()
    };
  };

  getBlocks = memoizeOne((returnEntity, enumerations, client) => {
    return infoFields.map(this.getBlock(returnEntity, enumerations, client));
  });

  getBlockProps = memoizeOne(onSubmitForm => ({
    onSubmitForm
  }));

  render() {
    const { returnEntity, enumerations, client, onSubmitForm } = this.props;

    if (!returnEntity) {
      return null;
    }

    const blockProps = this.getBlockProps(onSubmitForm);

    return this.getBlocks(returnEntity, enumerations, client).map(block => {
      const { info, key } = block;
      if (!info.formMain) {
        return <InfoBlock key={key} blockProps={blockProps} info={info} />;
      }

      const fields =
        info.formMain.type === "edit" ? info.formFields : info.fieldsForAdd;

      return (
        <InfoModalControl
          key={key}
          formProps={info.formMain}
          onSubmitForm={onSubmitForm}
          fields={fields}
        >
          {block.renderContent}
        </InfoModalControl>
      );
    });
  }
}

export default ReturnInfoBlocks;
