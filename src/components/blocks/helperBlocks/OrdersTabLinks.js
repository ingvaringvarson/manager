import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { push } from "connected-react-router";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withRouter } from "react-router-dom";

import Tabs from "../../../designSystem/molecules/Tabs";
import TabLinkComponent from "./TabLinkComponent";

class OrdersTabLinks extends PureComponent {
  static propTypes = {
    push: PropTypes.func.isRequired
  };

  links = [
    {
      id: "orders",
      url: "/orders",
      title: "Список заказов"
    },
    {
      id: "returns",
      url: "/returns",
      title: "Список возвратов"
    }
  ];

  handleTabClicked = url => this.props.push(url);

  render() {
    return (
      <Tabs>
        {this.links.map(link => {
          return (
            <TabLinkComponent
              key={link.id}
              {...link}
              handleSelect={this.handleTabClicked}
              active={this.props.match.path.includes(link.url)}
            />
          );
        })}
        {this.props.children}
      </Tabs>
    );
  }
}

export default compose(
  withRouter,
  connect(
    null,
    { push }
  )
)(OrdersTabLinks);
