import React, { memo } from "react";
import PropTypes from "prop-types";

import Tab from "../../../designSystem/atoms/Tab";

const propTypes = {
  url: PropTypes.string,
  title: PropTypes.string
};

const TabLinkComponent = memo(props => {
  return <Tab {...props} onClick={() => props.handleSelect(props.url)} />;
});

TabLinkComponent.propTypes = propTypes;

export default TabLinkComponent;
