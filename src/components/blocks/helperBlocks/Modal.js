import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import ReactModal from "react-modal";

import Heading from "../../../designSystem/molecules/Heading";
import Block from "../../../designSystem/organisms/Block";
import BlockContent from "../../../designSystem/organisms/BlockContent";

import { customStyles as modalStyles } from "../../../constants/defaultStyles/reactModalStyles";

class Modal extends PureComponent {
  static propTypes = {
    handleClose: PropTypes.func.isRequired,
    isOpen: PropTypes.bool,
    title: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    subtitle: PropTypes.string,
    hasWrap: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.array])
      .isRequired
  };

  static defaultProps = {
    isOpen: false,
    subtitle: "",
    hasWrap: true,
    width: "auto",
    height: "auto"
  };

  customStyles = {
    ...modalStyles,
    content: {
      ...modalStyles.content,
      width: this.props.width,
      height: this.props.height,
      overflow: "visible",
      maxHeight: "86vh"
    }
  };

  iconProps = {
    height: "24px",
    width: "24px",
    onClick: this.props.handleClose
  };

  render() {
    return (
      <ReactModal
        ariaHideApp={false}
        style={this.customStyles}
        onRequestClose={this.props.handleClose}
        isOpen={this.props.isOpen}
      >
        {this.props.hasWrap ? (
          <Block>
            <Heading
              title={this.props.title}
              subtitle={this.props.subtitle}
              icon="close"
              iconProps={this.iconProps}
            />
            <BlockContent>{this.props.children}</BlockContent>
          </Block>
        ) : (
          <>
            <Heading
              title={this.props.title}
              subtitle={this.props.subtitle}
              icon="close"
              iconProps={this.iconProps}
            />
            {this.props.children}
          </>
        )}
      </ReactModal>
    );
  }
}

export default Modal;
