import React, { Component } from "react";
import PropTypes from "prop-types";
import { fieldsForPayments } from "../../../fields/formFields/clients/fieldsForClientBills";
import TableWithLinks from "../../common/table/TableWithLinks";

const subTableParams = {
  subTable: true,
  ml: "80px",
  mt: "8px",
  mb: "16px"
};

class PaymentsForBill extends Component {
  static propTypes = {
    payments: PropTypes.array
  };

  render() {
    return (
      <TableWithLinks
        cells={fieldsForPayments}
        entities={this.props.payments || []}
        typeEntity="billPayment"
        tableProps={subTableParams}
        hasChildrenBefore
      />
    );
  }
}
export default PaymentsForBill;
