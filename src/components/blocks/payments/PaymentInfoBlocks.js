import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import InfoBlock from "../../common/info/InfoBlock";
import { fieldGroups } from "../../../fields/formFields/payments/fieldsForInfoBlock";
import memoizeOne from "memoize-one";
import InfoModalControl from "../../common/info/InfoModalControl";
import { getUrl } from "../../../helpers/urls";

class PaymentInfoBlocks extends PureComponent {
  static propTypes = {
    payment: PropTypes.object.isRequired,
    client: PropTypes.object.isRequired,
    enumerations: PropTypes.object.isRequired,
    bills: PropTypes.array.isRequired,
    onSubmitForm: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired
  };

  renderContent = info => (
    state = {},
    modalProps = null,
    blockProps = null,
    iconProps = null
  ) => (
    <InfoBlock
      iconIsShow={state.iconIsShow}
      info={info}
      blockProps={blockProps}
      iconProps={iconProps}
    />
  );

  getBlock = (entity, enumerations, block) => {
    const info = block.mapValuesToFields(block, enumerations, entity);
    return {
      ...info,
      renderContent: this.renderContent(info)
    };
  };

  getBlocks = memoizeOne((payment, client, bills, enumerations) => {
    const blocks = [];

    if (client.type === 1) {
      blocks.push(this.getBlock(client, enumerations, fieldGroups["client"]));
    } else if (client.type === 2) {
      blocks.push(this.getBlock(client, enumerations, fieldGroups["company"]));
    }

    if (payment.payment_state === 3) return blocks;

    blocks.push(
      this.getBlock(client["contacts"], enumerations, fieldGroups["contacts"])
    );

    blocks.push(
      this.getBlock(
        client["contact_persons"],
        enumerations,
        fieldGroups["contact_persons"]
      )
    );

    blocks.push(
      this.getBlock(
        payment.payments_inf || [],
        enumerations,
        fieldGroups["bills"]
      )
    );

    const orders = bills ? bills.filter(bill => bill.order_id !== null) : [];
    blocks.push(this.getBlock(orders, enumerations, fieldGroups["orders"]));

    if (payment.payment_method === 3) {
      blocks.push(
        this.getBlock(
          payment.agent_bank_details,
          enumerations,
          fieldGroups["agent_bank_details"]
        )
      );
      blocks.push(
        this.getBlock(
          payment.firm_bank_details,
          enumerations,
          fieldGroups["firm_bank_details"]
        )
      );
    }

    return blocks;
  });

  getBlockProps = memoizeOne(onSubmitForm => ({
    onSubmitForm
  }));

  handleMoveToClientPage = () => {
    if (this.props.client) {
      this.props.push(getUrl("clients", { id: this.props.client.id }));
    }
  };

  iconProps = {
    onClick: this.handleMoveToClientPage
  };

  render() {
    const { payment, client, bills, enumerations, onSubmitForm } = this.props;

    if (!payment || !client) {
      return null;
    }

    const blocks = this.getBlocks(payment, client, bills, enumerations);
    const blockProps = this.getBlockProps(onSubmitForm);

    return blocks.map(info => {
      if (!info.formMain) {
        return <InfoBlock blockProps={blockProps} info={info} />;
      } else if (info.formMain && !info.formMain.type) {
        return (
          <InfoBlock
            iconProps={this.iconProps}
            iconIsShow
            blockProps={blockProps}
            info={info}
          />
        );
      }

      const fields =
        info.formMain.type === "edit" ? info.formFields : info.fieldsForAdd;

      return (
        <InfoModalControl
          formProps={info.formMain}
          onSubmitForm={this.props.onSubmitForm}
          fields={fields}
        >
          {info.renderContent}
        </InfoModalControl>
      );
    });
  }
}

export default PaymentInfoBlocks;
