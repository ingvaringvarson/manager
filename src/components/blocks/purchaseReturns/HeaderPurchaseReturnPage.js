import React, { memo } from "react";
import PropTypes from "prop-types";

import Row from "../../../designSystem/templates/Row";
import Column from "../../../designSystem/templates/Column";
import HeadingCard from "../../common/headlines/HeadingCard";
import HeadingInfo from "../../common/headlines/HeadingInfo";
import PurchaseReturnMenu from "./PurchaseReturnMenu";

import { memoizeToArray } from "../../../helpers/cache";

const propTypes = {
  purchaseReturn: PropTypes.object.isRequired,
  enumerations: PropTypes.object.isRequired,
  fetchPurchaseReturn: PropTypes.func.isRequired
};

const renderReturnStatus = (purchaseReturn, enumerations) => {
  const state =
    purchaseReturn.purchase_return &&
    purchaseReturn.purchase_return.state_return;
  const stateEnumMap =
    enumerations &&
    enumerations.purchaseReturnState &&
    enumerations.purchaseReturnState.map;
  if ((!state && state !== 0) || !stateEnumMap) return null;

  return stateEnumMap[state] && stateEnumMap[state].name;
};

const HeaderPurchaseReturnPage = memo(props => {
  const { purchaseReturn, enumerations, fetchPurchaseReturn } = props;
  const returnCode =
    purchaseReturn.purchase_return && purchaseReturn.purchase_return.code;

  const returnStatus = renderReturnStatus(purchaseReturn, enumerations);

  const returnSum = `${purchaseReturn.purchase_return.sum_total} руб.`;
  return (
    <Row alignCenter justifyBetween mb="20px">
      <Column>
        <HeadingCard
          title="Карточка возврата поставщику"
          name={`id ${returnCode}`}
          status={returnStatus}
        />
      </Column>
      <Column>
        <HeadingInfo subTitle="Сумма к возврату" info={returnSum} />
      </Column>
      <Column>
        <PurchaseReturnMenu
          useMainCommand
          purchaseReturns={memoizeToArray(purchaseReturn)}
          fetchEntities={fetchPurchaseReturn}
        />
      </Column>
    </Row>
  );
});

HeaderPurchaseReturnPage.propTypes = propTypes;

export default HeaderPurchaseReturnPage;
