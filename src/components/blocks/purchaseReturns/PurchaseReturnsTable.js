import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import {
  TableContainer,
  renderRowBodyChildrenBefore,
  renderRowHeadChildrenBefore,
  renderRowFormControlChildrenAfter,
  renderRowFormControlChildrenBefore,
  RenderContext
} from "../../common/tableControls";
import PurchaseTabs from "../helperBlocks/PurchaseTabs";
import ClearButtonHeading from "../../common/styled/ClearButtonHeading";
import { FormContext } from "../../common/formControls";
import TableWithLinks from "../../common/table/TableWithLinks";
import Pagination from "../Pagination";
import NestedTableForEntity from "../positions/NestedTableForEntity";

import Block from "../../../designSystem/organisms/Block";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import TableRow from "../../../designSystem/molecules/TableRow";
import TableCell from "../../../designSystem/molecules/TableCell";
import Title from "../../../designSystem/atoms/Title";
import Box from "../../../designSystem/organisms/Box";

import PurchaseReturnMenu from "./PurchaseReturnMenu";
import PurchaseReturnPositionsMenu from "./PurchaseReturnPositionsMenu";

import { getSelectedEntities } from "../../../helpers/component/handlers";

import { returnFields } from "../../../fields/formFields/positions/fieldsForList";
import { memoizeToArray } from "../../../helpers/cache";

class PurchaseReturnsTable extends PureComponent {
  static propTypes = {
    requestParams: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,
    keyRouter: PropTypes.string.isRequired,
    enumerations: PropTypes.object.isRequired,
    purchaseReturns: PropTypes.array.isRequired,
    push: PropTypes.func.isRequired,
    maxPages: PropTypes.number.isRequired,
    fields: PropTypes.array.isRequired,
    fetchEntities: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.getSelectedEntities = memoizeOne(getSelectedEntities);
  }

  renderTableHead = () => {
    return (
      <TableRow>
        <TableCell flex="0 1 80px" />
        <TableCell flex="2 1 0">
          <Title>Поставщик</Title>
        </TableCell>
        <TableCell flex="1 1 0">
          <Title>Заказчик</Title>
        </TableCell>
        <TableCell flex="5 1 0">
          <Title>Возврат</Title>
        </TableCell>
      </TableRow>
    );
  };

  renderMainTableHeadAfter = headProps => {
    return (
      <RenderContext {...headProps}>
        {this.renderMainTableHeadAfterContext}
      </RenderContext>
    );
  };

  renderMainTableHeadAfterContext = ({ selected }, { entities }) => (
    <PurchaseReturnMenu
      forList
      purchaseReturns={this.getSelectedEntities(entities, selected)}
      fetchEntities={this.fetchEntities}
    />
  );

  renderMainTableRowAfter = ({ entity }) => (
    <PurchaseReturnMenu
      purchaseReturns={memoizeToArray(entity)}
      fetchEntities={this.fetchEntities}
    />
  );

  renderPositionsMenu = purchaseReturn => ({ forList, positions }) => (
    <PurchaseReturnPositionsMenu
      forList={forList}
      positions={positions}
      purchaseReturn={purchaseReturn}
      fetchEntities={this.props.fetchEntities}
    />
  );

  renderNestedTable = ({ cells, entity, entities, enumerations }) => {
    const { positions } = entity.purchase_return;
    if (!(positions && positions.length)) {
      return "У возврата нет позиций";
    }

    return (
      <NestedTableForEntity
        renderPositionsMenu={this.renderPositionsMenu(entity)}
        entity={entity}
        enumerations={enumerations}
        fields={returnFields}
        positions={positions}
      />
    );
  };

  renderTable = () => {
    const { fields, purchaseReturns, enumerations } = this.props;
    return (
      <TableWithLinks
        cells={fields}
        entities={purchaseReturns}
        enumerations={enumerations}
        hasFormControl
        hasChildrenBefore
        hasChildrenAfter
        renderBeforeContent={this.renderTableHead}
        rowBodyHandlers={this.rowBodyHandlers}
        renderRowHeadChildrenBefore={renderRowHeadChildrenBefore}
        renderRowBodyChildrenBefore={renderRowBodyChildrenBefore}
        renderRowFormControlChildrenBefore={renderRowFormControlChildrenBefore}
        renderRowFormControlChildrenAfter={renderRowFormControlChildrenAfter}
        renderRowBodyAfter={this.renderNestedTable}
        renderRowHeadChildrenAfter={this.renderMainTableHeadAfter}
        renderRowBodyChildrenAfter={this.renderMainTableRowAfter}
        typeEntity="purchaseReturn"
      />
    );
  };

  render() {
    const {
      requestParams,
      push,
      maxPages,
      pathname,
      fields,
      purchaseReturns
    } = this.props;

    return (
      <Block>
        <Box position="relative">
          <PurchaseTabs />
          <ClearButtonHeading title="Сбросить все" fields={fields} />
        </Box>
        <BlockContent table>
          <FormContext>
            <TableContainer
              entities={purchaseReturns}
              canBeSelected
              hasNestedTable
              renderTable={this.renderTable}
            />
          </FormContext>
          <Pagination
            requestParams={requestParams}
            pathname={pathname}
            push={push}
            page={+requestParams.page}
            maxPages={maxPages || 99}
          />
        </BlockContent>
      </Block>
    );
  }
}

export default PurchaseReturnsTable;
