import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";

import CommandsByStatusMenu from "../../common/menus/CommandsByStatusMenu";

import DocumentPrint from "../DocumentPrint";
import RejectPositions from "../positions/RejectModal";

import { putPurchaseReturn } from "../../../ducks/purchaseReturns";
import { typeMethods } from "../../../ducks/positions";

import purchaseReturns, {
  filterPurchaseReturnsCommands,
  getMainCommandByPurchaseReturn
} from "../../../commands/purchaseReturns";

import documentObjectTypeId from "../../../constants/documentObjectTypeId";

class PurchaseReturnMenu extends PureComponent {
  static propTypes = {
    purchaseReturns: PropTypes.arrayOf(PropTypes.object).isRequired,
    forList: PropTypes.bool,
    useMainCommand: PropTypes.bool,
    fetchEntities: PropTypes.func,

    putPurchaseReturn: PropTypes.func.isRequired
  };

  static defaultProps = {
    forList: false,
    useMainCommand: false,
    fetchEntities: () => null
  };

  modalIsNotImplementedFunc = (_, command) =>
    alert(`Функциональность ${command.title} пока не добавлена`);

  handleChangeState = state_return => ([purchaseReturn]) => {
    const { fetchEntities } = this.props;

    // предполагается, что операция здесь только для одной сущности
    this.props.putPurchaseReturn(
      {
        ...purchaseReturn.purchase_return,
        state_return
      },
      { fetchEntities }
    );
  };

  // переводим по статусам: 1 -> 2 -> 3
  handleAgreedBySupplier = ([purchaseReturn]) => {
    const { fetchEntities } = this.props;

    if (purchaseReturn.purchase_return.state_return === 2) {
      this.props.putPurchaseReturn(
        {
          ...purchaseReturn.purchase_return,
          state_return: 3
        },
        { fetchEntities }
      );
      return;
    }

    this.props.putPurchaseReturn(
      {
        ...purchaseReturn.purchase_return,
        state_return: 2
      },
      {
        successCallback: purchaseReturn =>
          this.props.putPurchaseReturn(
            {
              ...purchaseReturn.purchase_return,
              state_return: 3
            },
            { fetchEntities }
          )
      }
    );
  };

  // здесь всегда будут позиции, так как иначе команда не должна
  // быть показана, фильтр в commands\purchaseReturns.js
  getPositionsReturnIsNotAgreed = memoizeOne(purchaseReturns => {
    const posStates = [21, 25];

    return purchaseReturns.reduce((res, pr) => {
      if (!Array.isArray(pr.purchase_return.positions)) return res;

      pr.purchase_return.positions.forEach(p => {
        if (!posStates.includes(p.good_state)) {
        }

        res.push(p);
      });

      return res;
    }, []);
    /*purchaseReturn.purchase_return.positions.filter(({ position }) =>
      [21, 25].includes(position.good_state)
    )*/
  });

  commands = [
    {
      ...purchaseReturns.waitingForApprovalCommand,
      onClick: this.handleChangeState(2)
    },
    {
      ...purchaseReturns.agreedBySupplierCommand,
      onClick: this.handleAgreedBySupplier
    },
    {
      ...purchaseReturns.deliverGoodCommand,
      onClick: this.modalIsNotImplementedFunc
    },
    {
      ...purchaseReturns.returnIsNotAgreedCommand,
      renderModal: (handleClose, purchaseReturns) => (
        <RejectPositions
          entities={purchaseReturns}
          typeMethod={typeMethods.purchaseReturns}
          positions={this.getPositionsReturnIsNotAgreed(purchaseReturns)}
          handleClose={handleClose}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...purchaseReturns.printDocumentsCommand,
      renderModal: (handleClose, [purchaseReturn]) => (
        <DocumentPrint
          idEntity={purchaseReturn.purchase_return.id}
          objectTypeId={documentObjectTypeId.purchase_returns}
          entityFirmId={purchaseReturn.purchase_return.firm_id}
          handleClose={handleClose}
        />
      )
    }
  ];

  getMainCommand = memoizeOne(([purchaseReturn]) =>
    getMainCommandByPurchaseReturn(purchaseReturn.purchase_return)
  );

  render() {
    const { purchaseReturns, forList, useMainCommand } = this.props;
    const mainCommandId = useMainCommand
      ? this.getMainCommand(purchaseReturns)
      : null;
    return (
      <CommandsByStatusMenu
        forList={forList}
        entityOrEntities={purchaseReturns}
        commands={this.commands}
        mainCommandId={mainCommandId}
        commandsToMenuOptionsFilter={filterPurchaseReturnsCommands}
      />
    );
  }
}

export default connect(
  null,
  { putPurchaseReturn }
)(PurchaseReturnMenu);
