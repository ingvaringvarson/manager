import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import {
  fetchTasksForEntity,
  tasksForEntitySelector
} from "../../../ducks/tasks";
import { keyRouterSelector } from "../../../redux/selectors/router";
import { TableContainer } from "../../common/tableControls";
import { ContainerContext } from "../../common/formControls";
import Pagination from "../Pagination";
import Table from "../../common/table/Table";
import { generateUrlWithQuery, generateId } from "../../../utils";
import { reduceValuesToFormFields } from "../../../fields/helpers";
import { fields } from "../../../fields/formFields/returns/fieldsOfTasksTable";

import BlockContent from "../../../designSystem/organisms/BlockContent";

const typeEntity = "purchaseReturn";

class Tasks extends PureComponent {
  static propTypes = {
    keyRouter: PropTypes.string.isRequired,
    entity: PropTypes.object,
    requestParams: PropTypes.object,
    enumerations: PropTypes.object,
    pathname: PropTypes.string,
    idEntity: PropTypes.string,
    tasks: PropTypes.array
  };

  componentDidMount = () => {
    this.props.fetchTasksForEntity(typeEntity, this.props.idEntity, {
      ...this.props.requestParams,
      purchase_return_id: this.props.idEntity
    });
  };

  componentDidUpdate = prevProps => {
    if (prevProps.keyRouter !== this.props.keyRouter) {
      this.props.fetchTasksForEntity(typeEntity, this.props.idEntity, {
        ...this.props.requestParams,
        purchase_return_id: this.props.idEntity
      });
    }
  };

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  renderCells = memoizeOne((tasks, enumerations, requestParams) => {
    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: generateId()
    };
  });

  renderOptionControl = () => {
    return <div>-></div>;
  };

  renderHeadOptionControl = () => {
    return <div>+</div>;
  };

  render() {
    const { cells, key } = this.renderCells(
      this.props.tasks,
      this.props.enumerations,
      this.props.requestParams
    );

    return (
      <BlockContent table>
        <ContainerContext fields={cells} onSort={this.handleSort} key={key}>
          <TableContainer canBeSelected entities={this.props.tasks}>
            <Table
              cells={cells}
              enumerations={this.props.enumerations}
              entities={this.props.tasks}
              hasControls
              renderOptionControl={this.renderOptionControl}
              renderHeadOptionControl={this.renderHeadOptionControl}
            />
          </TableContainer>
        </ContainerContext>
        <Pagination
          requestParams={this.props.requestParams}
          pathname={this.props.pathname}
          push={this.props.push}
          page={+this.props.requestParams.page}
          maxPages={this.props.maxPages || 99}
        />
      </BlockContent>
    );
  }
}

export default connect(
  (state, props) => {
    const idEntity =
      props.entity &&
      props.entity.purchase_return &&
      props.entity.purchase_return.id;

    return {
      tasks: tasksForEntitySelector(state, {
        idEntity: idEntity,
        typeEntity
      }).entities,
      idEntity,
      keyRouter: keyRouterSelector(state)
    };
  },
  {
    push,
    fetchTasksForEntity
  }
)(Tasks);
