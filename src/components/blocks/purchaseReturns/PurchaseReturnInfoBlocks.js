import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";

import InfoBlock from "../../common/info/InfoBlock";
import InfoModalControl from "../../common/info/InfoModalControl";

import { routesToMove } from "./../../../helpers/routesGenerate";
import { fieldGroups } from "../../../fields/formFields/purchaseReturns/filedsForInfoPage";

class PurchaseReturnInfoBlocks extends PureComponent {
  static propTypes = {
    purchaseReturn: PropTypes.object,
    enumerations: PropTypes.object,
    client: PropTypes.object,
    onSubmitClientForm: PropTypes.func.isRequired
  };

  handleMoveToClientPage = () => {
    const { client } = this.props;
    if (client) {
      window.open(routesToMove.client(client), "_blank");
    }
  };

  iconProps = {
    onClick: this.handleMoveToClientPage
  };

  renderContent = info => (
    state = {},
    modalProps = null,
    blockProps = null,
    iconProps = null
  ) => (
    <InfoBlock
      iconIsShow={state.iconIsShow}
      info={info}
      blockProps={blockProps}
      iconProps={iconProps}
    />
  );

  getBlock = (entity, enumerations, block) => {
    const info = block.mapValuesToFields(block, enumerations, entity);
    return {
      ...info,
      renderContent: this.renderContent(info)
    };
  };

  getPurchaseReturnBlock = memoizeOne(
    (client, purchaseReturn, enumerations) => {
      const { delivery_type } = purchaseReturn.purchase_return;
      let block = {};

      switch (delivery_type) {
        case 1:
          block = this.getBlock(
            { ...purchaseReturn.purchase_return, client },
            enumerations,
            fieldGroups["delivery_1"]
          );
          break;
        case 2:
          block = this.getBlock(
            { ...purchaseReturn.purchase_return, client },
            enumerations,
            fieldGroups["delivery_2"]
          );
          break;
        default:
          return null;
      }

      return block;
    }
  );

  getClientBlocks = memoizeOne((client, purchaseReturn, enumerations) => {
    const blocks = [];

    if (client.type === 1) {
      blocks.push(
        this.getBlock(
          { client, purchaseReturn },
          enumerations,
          fieldGroups["client"]
        )
      );
    } else if (client.type === 2) {
      blocks.push(
        this.getBlock(
          { client, purchaseReturn },
          enumerations,
          fieldGroups["company"]
        )
      );
    }

    return blocks.concat(
      this.getBlock(client["contacts"], enumerations, fieldGroups["contacts"]),
      this.getBlock(
        client["contact_persons"],
        enumerations,
        fieldGroups["contact_persons"]
      )
    );
  });

  getBlockProps = memoizeOne(onSubmitForm => ({
    onSubmitForm
  }));

  render() {
    const {
      purchaseReturn,
      enumerations,
      client,
      onSubmitClientForm
    } = this.props;

    if (!purchaseReturn) {
      return null;
    }

    const purchaseReturnBlock = purchaseReturn
      ? this.getPurchaseReturnBlock(client, purchaseReturn, enumerations)
      : null;

    const clientBlocks = client
      ? this.getClientBlocks(client, purchaseReturn, enumerations)
      : null;

    const blockProps = this.getBlockProps(onSubmitClientForm);

    return (
      <>
        {!!purchaseReturnBlock && (
          <InfoModalControl
            formProps={purchaseReturnBlock.formMain}
            // ToDo: здесь скорее всего ошибка. Починить!
            onSubmitForm={onSubmitClientForm}
            fields={purchaseReturnBlock.formFields}
          >
            {purchaseReturnBlock.renderContent}
          </InfoModalControl>
        )}
        {!!clientBlocks &&
          clientBlocks.map(info => {
            if (!info.formMain || (info.formMain && !info.formMain.type)) {
              return (
                <InfoBlock
                  key={info.title}
                  iconProps={this.iconProps}
                  iconIsShow
                  blockProps={blockProps}
                  info={info}
                />
              );
            }
            const fields =
              info.formMain.type === "edit"
                ? info.formFields
                : info.fieldsForAdd;

            return (
              <InfoModalControl
                key={info.title}
                formProps={info.formMain}
                onSubmitForm={onSubmitClientForm}
                fields={fields}
              >
                {info.renderContent}
              </InfoModalControl>
            );
          })}
      </>
    );
  }
}

export default PurchaseReturnInfoBlocks;
