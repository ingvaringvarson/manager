import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import CommandsByStatusMenu from "../../common/menus/CommandsByStatusMenu";
import DividingModal from "../positions/DividingModal";
import RejectPositions from "../positions/RejectModal";

import purchaseReturnPositionsCommands, {
  filterPurchaseReturnPositionsCommands
} from "../../../commands/purchaseReturnPositions";

import { typeMethods } from "../../../ducks/positions";
import { memoizeToArray } from "../../../helpers/cache";

export default class PurchaseReturnPositionsMenu extends PureComponent {
  static propTypes = {
    forList: PropTypes.bool,
    positions: PropTypes.array.isRequired,
    purchaseReturn: PropTypes.object.isRequired,
    fetchEntities: PropTypes.func.isRequired
  };

  static defaultProps = {
    forList: false
  };

  modalIsNotImplementedFunc = (_, command) =>
    alert(`Функциональность ${command.title} пока не добавлена`);

  commands = [
    {
      ...purchaseReturnPositionsCommands.returnIsNotAgreedCommand,
      renderModal: (handleClose, positions, purchaseReturn) => (
        <RejectPositions
          entities={memoizeToArray(purchaseReturn)}
          typeMethod={typeMethods.purchaseReturns}
          positions={positions}
          handleClose={handleClose}
          fetchEntities={this.props.fetchEntities}
        />
      )
    },
    {
      ...purchaseReturnPositionsCommands.changePriceAndCountCommand,
      onClick: this.modalIsNotImplementedFunc
    },
    {
      ...purchaseReturnPositionsCommands.divideCommand,
      renderModal: (handleClose, positions, purchaseReturn) => (
        <DividingModal
          typeMethod={typeMethods.purchaseReturns}
          positions={positions}
          handleClose={handleClose}
          entity={purchaseReturn}
          fetchEntities={this.props.fetchEntities}
        />
      )
    }
  ];

  render() {
    const { positions, purchaseReturn, forList } = this.props;
    return (
      <CommandsByStatusMenu
        forList={forList}
        entityOrEntities={positions}
        parentEntity={purchaseReturn}
        commands={this.commands}
        commandsToMenuOptionsFilter={filterPurchaseReturnPositionsCommands}
      />
    );
  }
}
