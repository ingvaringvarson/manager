import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import memoizeOne from "memoize-one";
import ReactModal from "../../common/modals/CommonModal";
import {
  fetchEnumerations,
  enumerationsSelector
} from "../../../ducks/enumerations";
import {
  normalizePurchaseReturnCreate,
  createPurchaseReturn
} from "../../../ducks/purchaseReturns";
import {
  fieldsForForm,
  fieldsFormForTable,
  fieldsViewForTable
} from "../../../fields/formFields/purchaseReturns/fieldsForCreation";
import { reduceValuesToFormFields } from "../../../fields/helpers";
import { generateId } from "../../../utils";
import { FormContext, FieldContext } from "../../common/formControls";
import MediatorFormAndTable from "../../common/forms/MediatorFormAndTable";

const complimentaryStyles = {
  width: "800px"
};

const enumerationNames = ["returnReason"];

class CreatePurchaseReturn extends PureComponent {
  static propTypes = {
    forMulti: PropTypes.bool,
    selectedIds: PropTypes.arrayOf(PropTypes.string),
    entities: PropTypes.arrayOf(PropTypes.object),
    fetchEntities: PropTypes.func,
    handleClose: PropTypes.func.isRequired,
    positions: PropTypes.arrayOf(PropTypes.object).isRequired,

    enumerations: PropTypes.object.isRequired,
    createPurchaseReturn: PropTypes.func.isRequired,
    fetchEnumerations: PropTypes.func.isRequired
  };

  static defaultProps = {
    forMulti: false,
    fetchEntities: () => null
  };

  componentDidMount = () => {
    this.props.fetchEnumerations(enumerationNames);
  };

  handleFormSubmit = (formValues, entities) => {
    this.props.createPurchaseReturn(
      formValues,
      {
        entities: this.props.entities,
        positions: entities,
        forMulti: this.props.forMulti,
        fetchEntities: this.props.fetchEntities
      },
      normalizePurchaseReturnCreate
    );
  };

  renderError = () => {
    if (
      this.props.forMulti &&
      Array.isArray(this.props.selectedIds) &&
      Array.isArray(this.props.entities)
    ) {
      switch (true) {
        case !this.props.selectedIds.length:
          return "Заказы поставщика не были выбраны";
        case this.props.entities.some(
          entity =>
            entity.order.contract_id !==
            this.props.entities[0].order.contract_id
        ):
          return "Выбранные вами заказы оформлены на разные договоры. Создать единый возврат нельзя";
        case this.props.entities.some(
          entity =>
            entity.order.firm_id !== this.props.entities[0].order.firm_id
        ):
          return "Выбранные вами заказы оформлены на разные организации. Создать единый возврат нельзя";
        case this.props.entities.some(
          entity =>
            entity.order.agent_id !== this.props.entities[0].order.agent_id
        ):
          return "Выбранные вами заказы оформлены на разных поставщиков. Создать единый возврат нельзя";
        case this.props.entities.some(
          entity =>
            !(
              entity.order.state_supplier_order === 3 ||
              entity.order.state_supplier_order === 4
            )
        ):
          return "Выбранные вами заказы не приняты. Создать возврат нельзя";
        default:
          return null;
      }
    }

    return null;
  };

  renderCells = memoizeOne((positions, enumerations) => {
    const formFields = fieldsForForm.reduce(
      reduceValuesToFormFields({ positions }, enumerations),
      []
    );
    const formForTableFields = fieldsFormForTable.reduce(
      reduceValuesToFormFields(
        {
          fields: positions
        },
        enumerations
      ),
      []
    );
    return {
      formFields,
      formForTableFields,
      allFormFields: formFields.concat(formForTableFields),
      key: generateId()
    };
  });

  renderFormBeforeTable = () => {
    const { positions, enumerations } = this.props;
    const { formFields } = this.renderCells(positions, enumerations);

    return (
      <FormContext>
        {formFields.map((field, index) => (
          <FieldContext index={index} name={field.id} key={field.id} />
        ))}
      </FormContext>
    );
  };

  renderContent = () => {
    const { positions, enumerations } = this.props;
    const { formFields, allFormFields, key } = this.renderCells(
      positions,
      enumerations
    );

    return (
      <MediatorFormAndTable
        key={key}
        entities={positions}
        enumerations={enumerations}
        buttonTitle="Создать возврат"
        fieldsForContainer={allFormFields}
        fieldsForTable={fieldsViewForTable}
        fieldsForForm={formFields}
        handleFormSubmit={this.handleFormSubmit}
        isRenderDefault={false}
        hasFormBeforeTable
        renderFormBeforeTable={this.renderFormBeforeTable}
      />
    );
  };

  render() {
    const error = this.renderError();

    return (
      <ReactModal
        ariaHideApp={false}
        isOpen
        handleClose={this.props.handleClose}
        title="Создать возврат поставщику"
        contentStyles={complimentaryStyles}
      >
        {error || this.renderContent()}
      </ReactModal>
    );
  }
}

export default connect(
  state => {
    return {
      enumerations: enumerationsSelector(state, { enumerationNames })
    };
  },
  { createPurchaseReturn, fetchEnumerations }
)(CreatePurchaseReturn);
