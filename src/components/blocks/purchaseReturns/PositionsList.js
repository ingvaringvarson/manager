import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import NestedTableForEntity from "../positions/NestedTableForEntity";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import PurchaseReturnPositionsMenu from "./PurchaseReturnPositionsMenu";

import { fetchPurchaseReturn } from "../../../ducks/purchaseReturns";
import { fields } from "../../../fields/formFields/purchaseReturns/fieldsForReturnPositions";

const propTypes = {
  enumerations: PropTypes.object,
  purchaseReturn: PropTypes.object
};

class PositionsList extends PureComponent {
  fetchEntities = () => {
    const { id } = this.props.purchaseReturn.purchase_return;
    this.props.fetchPurchaseReturn(id, { id });
  };

  renderPositionsMenu = ({ forList, positions }) => (
    <PurchaseReturnPositionsMenu
      forList={forList}
      positions={positions}
      purchaseReturn={this.props.purchaseReturn}
      fetchEntities={this.fetchEntities}
    />
  );

  render() {
    return (
      <BlockContent table>
        <NestedTableForEntity
          renderPositionsMenu={this.renderPositionsMenu}
          entity={this.props.purchaseReturn}
          positions={this.props.purchaseReturn.purchase_return.positions || []}
          enumerations={this.props.enumerations}
          tableParams={null}
          fields={fields}
          isNested={false}
        />
      </BlockContent>
    );
  }
}

PositionsList.propTypes = propTypes;

export default connect(
  null,
  { fetchPurchaseReturn }
)(PositionsList);
