import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";

import RightStickedButton from "../../common/buttons/RightStickedButton";
import ReactModal from "../../common/modals/CommonModal";
import RemoveableTable from "../../common/table/Removeable";

import { ContainerContext } from "../../common/formControls";

import { postPurchaseReturnPositions } from "../../../ducks/purchaseReturns";

import {
  tableFields,
  formFields
} from "../../../fields/formFields/purchaseReturns/fieldsForReturnIsNotAgreed";

import { generateId } from "../../../utils";
import { reduceValuesToFormFields } from "../../../fields/helpers";
import { normalizeReturnCount } from "./../../../ducks/purchaseReturns/normalize";

class ReturnIsNotAgreed extends PureComponent {
  static propTypes = {
    purchaseReturn: PropTypes.object.isRequired,
    positions: PropTypes.array.isRequired,
    handleClose: PropTypes.func.isRequired,

    fetchEntities: PropTypes.func.isRequired,

    postPurchaseReturnPositions: PropTypes.func.isRequired
  };

  state = {
    removedIds: []
  };

  modalStyles = {
    width: 800
  };

  mapValuesToFields = memoizeOne(positions => {
    const realPositions = positions.map(p => ({
      ...p.position,
      _id: p._id
    }));
    return {
      tableFields: tableFields,
      formFields: formFields.reduce(
        reduceValuesToFormFields({ fields: realPositions }),
        []
      ),
      realPositions,
      key: generateId()
    };
  });

  handleFormSubmit = values => {
    this.props.postPurchaseReturnPositions(
      values,
      {
        positions: this.props.positions,
        removedIds: this.state.removedIds,
        fetchEntities: this.props.fetchEntities,
        purchaseReturn: this.props.purchaseReturn,
        successCallback: this.props.handleClose.bind(this)
      },
      normalizeReturnCount
    );
  };

  handleRemovedChange = removedIds => this.setState({ removedIds });

  renderForm = formState => {
    const { positions } = this.props;
    const { removedIds } = this.state;
    const { tableFields, realPositions } = this.mapValuesToFields(
      positions,
      removedIds
    );
    const isFormValid = !Object.keys(formState.invalidFields).length;
    const isSubmitButtonDisabled =
      !isFormValid || this.state.removedIds.length === positions.length;

    return (
      <>
        <RemoveableTable
          cells={tableFields}
          entities={realPositions}
          removedIds={this.state.removedIds}
          handleRemovedChange={this.handleRemovedChange}
        />
        <RightStickedButton type="submit" disabled={isSubmitButtonDisabled}>
          Отказ на возврат
        </RightStickedButton>
      </>
    );
  };

  render() {
    const { positions, handleClose } = this.props;
    const { removedIds } = this.state;
    const { formFields, key } = this.mapValuesToFields(positions, removedIds);

    return (
      <ReactModal
        ariaHideApp={false}
        isOpen
        title="Отказ в согласовании возврата"
        handleClose={handleClose}
        contentStyles={this.modalStyles}
      >
        <ContainerContext
          key={key}
          fields={formFields}
          onSubmitForm={this.handleFormSubmit}
          renderForm={this.renderForm}
        ></ContainerContext>
      </ReactModal>
    );
  }
}

export default connect(
  null,
  { postPurchaseReturnPositions }
)(ReturnIsNotAgreed);
