import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  paymentsForEntitySelector,
  fetchPaymentsForEntity,
  requestParamsPaymentsForEntitySelector
} from "../../../ducks/payments";
import {
  keyRouterSelector,
  pathnameRouterSelector
} from "../../../redux/selectors/router";
import { enumerationListWithoutZeroIdSelector } from "../../../ducks/enumerations";
import { fieldsForPaymentMethods } from "../../../fields/formFields/orders/fieldsForOrderPayments";
import { push } from "connected-react-router";
import { generateUrlWithQuery } from "../../../utils";
import { ContainerContext } from "../../common/formControls";
import { TableContainer } from "../../common/tableControls";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../../fields/helpers";
import memoizeOne from "memoize-one";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";
import BlockContent from "../../../designSystem/organisms/BlockContent";
import Pagination from "../Pagination";
import TableWithLinks from "../../common/table/TableWithLinks";
import { getUrl } from "../../../helpers/urls";
import { generateId } from "../../../utils";

const typeEntity = "purchaseReturns";

class Payments extends PureComponent {
  static propTypes = {
    // comes from parent Component
    purchaseReturn: PropTypes.object,
    enumerations: PropTypes.object,
    // comes from connect
    requestParams: PropTypes.object,
    selectedTab: PropTypes.number,
    pathname: PropTypes.string,
    idEntity: PropTypes.string,
    payments: PropTypes.array,
    keyRouter: PropTypes.object,
    push: PropTypes.func.isRequired,
    fetchPaymentsForEntity: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchPaymentsForEntity(
      typeEntity,
      this.props.idEntity,
      this.props.requestParams
    );
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.props.fetchPaymentsForEntity(
        typeEntity,
        this.props.idEntity,
        this.props.requestParams
      );
    }
  }

  renderCells = memoizeOne(enumerations => {
    const { fields, requestParams } = this.props;

    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: generateId()
    };
  });

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleChangeTab = id => {
    const { pathname, requestParams } = this.props;
    const queryForRootTab = `?_tab=${requestParams._tab || 1}`;
    const queryForTab = id === 1 ? "" : `&payment_method=${id}`;
    this.props.push(`${pathname}${queryForRootTab}${queryForTab}`);
  };

  handleMoveToPaymentPage = ({ entity }) => () => {
    if (entity) this.props.push(getUrl("payments", { id: entity.id }));
  };

  rowBodyHandlers = {
    onClick: this.handleMoveToPaymentPage
  };

  renderTable = renderCallBacks => {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.payments
    );

    return (
      <TableWithLinks
        key={key}
        cells={cells}
        entities={this.props.payments.entities}
        enumerations={this.props.enumerations}
        hasFormControl
        hasChildrenBefore
        renderRowHeadChildrenBefore={
          renderCallBacks.renderRowHeadChildrenBefore
        }
        renderRowBodyChildrenBefore={
          renderCallBacks.renderRowBodyChildrenBefore
        }
        renderRowFormControlChildrenBefore={
          renderCallBacks.renderRowFormControlChildrenBefore
        }
        rowBodyHandlers={this.rowBodyHandlers}
        typeEntity="payment"
      />
    );
  };

  render() {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.payments
    );

    return (
      <>
        <BlockContent table>
          <TabContainer
            selectedTabId={this.props.requestParams.payment_method}
            onChangeSelected={this.handleChangeTab}
          >
            <TabHeadContainer>
              {this.props.paymentMethods.map(item => {
                return <TabHead id={item.id}>{item.name}</TabHead>;
              })}
            </TabHeadContainer>
            <TabBodyContainer>
              {this.props.paymentMethods.map(item => {
                return (
                  <TabBody id={item.id}>
                    <ContainerContext
                      key={key}
                      onSort={this.handleSort}
                      fields={cells}
                      onSubmitForm={this.handleSubmitForm}
                      autoSubmit
                    >
                      <TableContainer
                        entities={this.props.payments.entities}
                        canBeSelected
                        renderTable={this.renderTable}
                      />
                    </ContainerContext>
                    <Pagination
                      requestParams={this.props.requestParams}
                      pathname={this.props.pathname}
                      push={this.props.push}
                      page={+this.props.requestParams.page}
                      maxPages={this.props.payments.maxPages || 99}
                    />
                  </TabBody>
                );
              })}
            </TabBodyContainer>
          </TabContainer>
        </BlockContent>
      </>
    );
  }
}

export default connect(
  (state, props) => {
    const requestParams = requestParamsPaymentsForEntitySelector(state, {
      typeEntity: "purchase_return_id",
      idEntity: [props.purchaseReturn.purchase_return.id]
    });
    const idEntity = `${props.purchaseReturn.purchase_return.id}_${requestParams.payment_method}`;

    return {
      idEntity,
      requestParams,
      keyRouter: keyRouterSelector(state),
      paymentMethods: enumerationListWithoutZeroIdSelector(state, {
        name: "paymentMethod"
      }),
      payments: paymentsForEntitySelector(state, {
        idEntity,
        typeEntity
      }),
      pathname: pathnameRouterSelector(state),
      fields: fieldsForPaymentMethods[requestParams.payment_method]
    };
  },
  {
    fetchPaymentsForEntity,
    push
  }
)(Payments);
