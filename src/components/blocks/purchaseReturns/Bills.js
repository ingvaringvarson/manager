import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import memoizeOne from "memoize-one";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import {
  keyRouterSelector,
  queryRouterSelector
} from "../../../redux/selectors/router";
import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";
import {
  billsForEntitySelector,
  fetchBillsForEntity,
  requestParamsForBillEntitySelector
} from "../../../ducks/bills";
import { generateUrlWithQuery } from "../../../utils";
import {
  reduceValuesToFormFields,
  mapFormFieldsToQuery
} from "../../../fields/helpers";
import Table from "../../common/tableView/Table";
import TableWithLinks from "../../common/table/TableWithLinks";
import { fields } from "../../../fields/formFields/orders/fieldsForOrderBills";
import { ContainerContext } from "../../common/formControls";
import { TableContainer, ToggleOpenRow } from "../../common/tableControls";
import Container from "../../../designSystem/templates/Container";
import Row from "../../../designSystem/templates/Row";
import Column from "../../../designSystem/templates/Column";
import BlockContent from "../../../designSystem/organisms/BlockContent";

import { generateId } from "../../../utils";
import { billPositionFields } from "../../../fields/formFields/purchaseReturns/fieldsForBillsInner";
import Pagination from "../Pagination";
import PaymentsForBill from "../payments/PaymentsForBill";

const subTableParams = {
  subTable: true,
  ml: "80px",
  mt: "8px",
  mb: "16px"
};

const typeEntity = "purchaseReturns";

class Bills extends PureComponent {
  static propTypes = {
    // comes from parent Component
    purchaseReturn: PropTypes.object,
    requestParams: PropTypes.object,
    enumerations: PropTypes.object,
    pathname: PropTypes.string,
    // comes from connect
    selectedTab: PropTypes.number,
    query: PropTypes.object,
    idEntity: PropTypes.string,
    bills: PropTypes.array,
    keyRouter: PropTypes.object,
    push: PropTypes.func.isRequired,
    fetchBillsForEntity: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchBillsForEntity(
      typeEntity,
      this.props.idEntity,
      this.props.requestParams
    );
  }

  componentDidUpdate(prevProps) {
    if (this.props.keyRouter !== prevProps.keyRouter) {
      this.props.fetchBillsForEntity(
        typeEntity,
        this.props.idEntity,
        this.props.requestParams
      );
    }
  }

  handleSort = query => {
    const { pathname, requestParams } = this.props;
    const newQuery = { ...requestParams, ...query };

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleSubmitForm = (values, fields) => {
    const { pathname, requestParams } = this.props;
    const newQuery = mapFormFieldsToQuery(fields, values, requestParams);

    this.props.push(generateUrlWithQuery({ pathname, newQuery }));
  };

  handleChangeTab = id => {
    const { pathname, requestParams } = this.props;
    const queryForRootTab = `?_tab=${requestParams._tab || 1}`;
    const queryForTab = id === 1 ? "" : `&_tab_state_bill=${id}`;
    this.props.push(`${pathname}${queryForRootTab}${queryForTab}`);
  };

  renderCells = memoizeOne(enumerations => {
    const { requestParams } = this.props;

    return {
      cells: fields.reduce(
        reduceValuesToFormFields(requestParams, enumerations),
        []
      ),
      key: generateId()
    };
  });

  renderNestedTable = ({ entity }) => {
    const { enumerations } = this.props;
    const { positions, payments_inf } = entity;

    return (
      <ToggleOpenRow id={entity._id}>
        <Container>
          <Row>
            <Column basis="50%">
              {positions && Array.isArray(positions) && positions.length && (
                <TableContainer>
                  <Table
                    tableProps={subTableParams}
                    entities={positions}
                    cells={billPositionFields}
                    enumerations={enumerations}
                  />
                </TableContainer>
              )}
            </Column>
            <Column basis="50%">
              {!!(Array.isArray(payments_inf) && payments_inf.length) ? (
                <PaymentsForBill payments={payments_inf} />
              ) : (
                "У счета отсутсвуют сквитованные счета"
              )}
            </Column>
          </Row>
        </Container>
      </ToggleOpenRow>
    );
  };

  renderTable = renderCallBacks => {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.bills
    );

    return (
      <TableWithLinks
        key={key}
        cells={cells}
        entities={this.props.bills.entities}
        enumerations={this.props.enumerations}
        hasFormControl
        hasChildrenBefore
        hasChildrenAfter
        renderRowHeadChildrenBefore={
          renderCallBacks.renderRowHeadChildrenBefore
        }
        renderRowBodyChildrenBefore={
          renderCallBacks.renderRowBodyChildrenBefore
        }
        renderRowFormControlChildrenBefore={
          renderCallBacks.renderRowFormControlChildrenBefore
        }
        renderRowBodyAfter={this.renderNestedTable}
        typeEntity="bill"
      />
    );
  };

  render() {
    const { cells, key } = this.renderCells(
      this.props.enumerations,
      this.props.bills
    );

    return (
      <BlockContent table>
        <TabContainer
          selectedTabId={this.props.selectedTab}
          onChangeSelected={this.handleChangeTab}
        >
          <TabHeadContainer>
            <TabHead id={1}>Активные</TabHead>
            <TabHead id={2}>Аннулированные</TabHead>
          </TabHeadContainer>
          <TabBodyContainer>
            <TabBody id={1}>
              <ContainerContext
                key={key}
                onSort={this.handleSort}
                fields={cells}
                onSubmitForm={this.handleSubmitForm}
                autoSubmit
              >
                <TableContainer
                  entities={this.props.bills.entities}
                  canBeSelected
                  hasNestedTable
                  renderTable={this.renderTable}
                />
              </ContainerContext>
            </TabBody>
            <TabBody id={2}>
              <ContainerContext
                key={key}
                onSort={this.handleSort}
                fields={cells}
                onSubmitForm={this.handleSubmitForm}
                autoSubmit
              >
                <TableContainer
                  entities={this.props.bills.entities}
                  canBeSelected
                  hasNestedTable
                  renderTable={this.renderTable}
                />
              </ContainerContext>
            </TabBody>
          </TabBodyContainer>
        </TabContainer>
        <Pagination
          requestParams={this.props.requestParams}
          pathname={this.props.pathname}
          push={this.props.push}
          page={+this.props.requestParams.page}
          maxPages={this.props.bills.maxPages || 99}
        />
      </BlockContent>
    );
  }
}

export default connect(
  (state, props) => {
    const { purchaseReturn } = props;
    const query = queryRouterSelector(state);
    const idEntity = purchaseReturn.purchase_return.id;
    const requestParams = requestParamsForBillEntitySelector(state, {
      typeEntity,
      idEntity
    });
    const selectedTab = requestParams._tab_state_bill;
    return {
      requestParams,
      selectedTab,
      query,
      idEntity,
      bills: billsForEntitySelector(state, { typeEntity, idEntity }),
      keyRouter: keyRouterSelector(state)
    };
  },
  { push, fetchBillsForEntity }
)(Bills);
