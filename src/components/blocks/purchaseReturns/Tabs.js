import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { pathnameRouterSelector } from "../../../redux/selectors/router";
import { requestParamsForListSelector } from "../../../ducks/purchaseReturns";

import {
  TabBody,
  TabBodyContainer,
  TabContainer,
  TabHead,
  TabHeadContainer
} from "../../common/tabControls";

import PositionsList from "./PositionsList";
import Bills from "./Bills";
import Payments from "./Payments";
import Tasks from "./Tasks";
import Documents from "../Documents";
import documentObjectTypeId from "../../../constants/documentObjectTypeId";

class Tabs extends PureComponent {
  static propTypes = {
    requestParams: PropTypes.object,
    enumerations: PropTypes.object,
    purchaseReturn: PropTypes.object
  };

  handleTabChanged = id => {
    this.props.push(`${this.props.pathname}?_tab=${id}`);
  };

  render() {
    const { requestParams } = this.props;
    return (
      <TabContainer
        selectedTabId={requestParams._tab}
        onChangeSelected={this.handleTabChanged}
      >
        <TabHeadContainer>
          <TabHead id={1}>Позиции возврата</TabHead>
          <TabHead id={2}>Счета</TabHead>
          <TabHead id={3}>Платежи</TabHead>
          <TabHead id={4}>Задачи</TabHead>
          <TabHead id={5}>Документы</TabHead>
        </TabHeadContainer>
        <TabBodyContainer>
          <TabBody id={1}>
            <PositionsList {...this.props} />
          </TabBody>
          <TabBody id={2}>
            <Bills {...this.props} />
          </TabBody>
          <TabBody id={3}>
            <Payments {...this.props} />
          </TabBody>
          <TabBody id={4}>
            <Tasks {...this.props} entity={this.props.purchaseReturn} />
          </TabBody>
          <TabBody id={5}>
            <Documents
              idEntity={this.props.purchaseReturn.purchase_return.id}
              typeEntity={documentObjectTypeId.purchase_returns}
            />
          </TabBody>
        </TabBodyContainer>
      </TabContainer>
    );
  }
}

export default connect(
  state => {
    return {
      pathname: pathnameRouterSelector(state),
      requestParams: requestParamsForListSelector(state)
    };
  },
  { push }
)(Tabs);
