import { apiUri } from "../config";
import { serializationParamsToString } from "../utils";

export function getEntities(typeEntities, params = {}) {
  return {
    action: `${apiUri}${typeEntities}${serializationParamsToString(params)}`,
    options: {
      method: "GET"
    }
  };
}

export function getEntitiesCommon(entitiesUrl = "") {
  return {
    action: `${apiUri}${entitiesUrl}`,
    options: {
      method: "GET"
    }
  };
}

export function getEntitiesCommonById(entitiesUrl = "", id = "") {
  return {
    action: `${apiUri}${entitiesUrl}/${encodeURIComponent(id)}`,
    options: {
      method: "GET"
    }
  };
}

export function postEntities(typeEntities = "", body = {}) {
  return {
    action: `${apiUri}${typeEntities}`,
    options: {
      method: "POST",
      body: JSON.stringify(body)
    }
  };
}

export function postCommon(url = "", body = {}) {
  return {
    action: `${apiUri}${url}`,
    options: {
      method: "POST",
      body: JSON.stringify(body)
    }
  };
}

export function postUploadCommon(url = "", body = {}) {
  return {
    action: `${apiUri}${url}`,
    options: {
      method: "POST",
      body: body
    },
    type: "multipart/form-data"
  };
}

export function putEntities(typeEntities = "", body = {}) {
  return {
    action: `${apiUri}${typeEntities}`,
    options: {
      method: "PUT",
      body: JSON.stringify(body)
    }
  };
}

export function deleteCommon(url = "") {
  return {
    action: `${apiUri}${url}`,
    options: {
      method: "DELETE"
    }
  };
}
