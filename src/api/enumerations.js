import { apiUri } from "../config";

export function getEnumeration(name) {
  return {
    action: `${apiUri}lookups?name=${encodeURIComponent(name)}`,
    options: {
      method: "GET"
    }
  };
}
