import { apiUri } from "../config";

export function getBasketInfo(params) {
  const { basketId } = params;

  return {
    action: `${apiUri}basket/${encodeURIComponent(basketId)}`,
    options: {
      method: "GET"
    }
  };
}

export function getBaskets() {
  return {
    action: `${apiUri}basket/summary`,
    options: {
      method: "GET"
    }
  };
}

export function checkAgentBasket(params) {
  const { agentId } = params;

  return {
    action: `${apiUri}basket/checkExisting?agentId=${agentId}`,
    options: {
      method: "GET"
    }
  };
}

export function addBasket(basket) {
  return {
    action: `${apiUri}basket`,
    options: {
      method: "POST",
      body: JSON.stringify(basket)
    }
  };
}

export function addItemsToBasket(params) {
  const { basketId, items } = params;

  return {
    action: `${apiUri}basket/${encodeURIComponent(basketId)}/items`,
    options: {
      method: "POST",
      body: JSON.stringify({ items })
    }
  };
}

export function deleteBasket(params) {
  const { basketId } = params;
  return {
    action: `${apiUri}basket/${encodeURIComponent(basketId)}`,
    options: {
      method: "DELETE"
    }
  };
}

export function detachAgentFromBasket(params) {
  const {
    basketId,
    query: { mode, cleanup }
  } = params;
  return {
    action: `${apiUri}basket/${encodeURIComponent(
      basketId
    )}/client?mode=${mode}&cleanup=${cleanup}`,
    options: {
      method: "DELETE"
    }
  };
}

export function removeItemsFromBasket(params) {
  const { basketId, itemIds: itemsIds } = params;

  return {
    action: `${apiUri}basket/${encodeURIComponent(basketId)}/deleteitems`,
    options: {
      method: "POST",
      body: JSON.stringify({ itemsIds })
    }
  };
}

export function updateBasketFabric(params) {
  const { basketId, actionApiName, body } = params;
  return {
    action: `${apiUri}basket/${encodeURIComponent(basketId)}/${actionApiName}`,
    options: {
      method: "PUT",
      body: JSON.stringify(body)
    }
  };
}

export function updateItemInBasketFabric(params) {
  const { basketId, itemId, actionApiName, body } = params;
  return {
    action: `${apiUri}basket/${encodeURIComponent(
      basketId
    )}/items/${encodeURIComponent(itemId)}/${actionApiName}`,
    options: {
      method: "PUT",
      body: JSON.stringify(body)
    }
  };
}

export function createOrder(params) {
  const { basketId, body } = params;
  return {
    action: `${apiUri}basket/${encodeURIComponent(basketId)}/order`,
    options: {
      method: "POST",
      body: JSON.stringify(body)
    }
  };
}
