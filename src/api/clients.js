import { apiUri } from "../config";

export function updateClient(id, params = {}) {
  return {
    action: `${apiUri}agents`,
    options: {
      method: "PUT",
      body: JSON.stringify(params)
    }
  };
}

export function createClient(params = {}) {
  return {
    action: `${apiUri}agents`,
    options: {
      method: "POST",
      body: JSON.stringify(params)
    }
  };
}
