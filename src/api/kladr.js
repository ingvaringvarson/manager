import { apiUri } from "../config";

export function getDaDataSuggest(
  params = {
    query: "",
    count: 10,
    locations: [],
    locations_boost: [],
    from_bound: { value: "" },
    to_bound: { value: "" }
  }
) {
  return {
    action: `${apiUri}dadata/suggest`,
    options: {
      method: "POST",
      body: JSON.stringify(params)
    }
  };
}

export function getDaDataInfo(params = { query: "" }) {
  return {
    action: `${apiUri}dadata/findById`,
    options: {
      method: "POST",
      body: JSON.stringify(params)
    }
  };
}
