import { apiUri } from "../config";

export function getInfoByUser() {
  return {
    action: `${apiUri}services/user/info?_=${Date.now()}`,
    options: {
      method: "GET"
    }
  };
}

export function logIn(login = "", password = "") {
  return {
    action: `${apiUri}services/user/login`,
    options: {
      method: "POST",
      body: JSON.stringify({ login, password })
    }
  };
}

export function logOut() {
  return {
    action: `${apiUri}services/user/logout`,
    options: {
      method: "POST"
    }
  };
}

export function getPermissions() {
  return {
    action: `${apiUri}services/user/permissions`,
    options: {
      method: "GET"
    }
  };
}
