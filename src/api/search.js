import { apiUri } from "../config";

export function getBrandsForVendorCode(vendorCode) {
  if (+vendorCode !== 0 && !vendorCode) {
    return null;
  }

  const vendorCodeMatch = vendorCode.toString().match(/([a-zA-Z0-9]+)$/);

  if (!vendorCodeMatch) {
    return null;
  }

  return {
    action: `${apiUri}search/catalogs/${encodeURIComponent(
      vendorCodeMatch[0]
    )}`,
    options: {
      method: "GET"
    }
  };
}

export function getProducts(params = {}) {
  const { brandKey, vendorCode, ...body } = params;

  return {
    action: `${apiUri}search/${encodeURIComponent(
      brandKey
    )}/${encodeURIComponent(vendorCode)}`,
    options: {
      method: "POST",
      body: JSON.stringify(body)
    }
  };
}
