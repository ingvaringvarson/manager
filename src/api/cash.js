import { apiUri } from "../config";

export function openShift() {
  return {
    action: `${apiUri}cash/shift/open`,
    options: {
      method: "POST"
    }
  };
}

export function closeShift() {
  return {
    action: `${apiUri}cash/shift/close`,
    options: {
      method: "POST"
    }
  };
}

export function printXreport() {
  return {
    action: `${apiUri}cash/print/xreport`,
    options: {
      method: "POST"
    }
  };
}

export function printZreport() {
  return {
    action: `${apiUri}cash/print/zreport`,
    options: {
      method: "POST"
    }
  };
}

export function deposit(params = {}) {
  return {
    action: `${apiUri}cash/deposit`,
    options: {
      method: "POST",
      body: JSON.stringify({ amount: +params.amount })
    }
  };
}

export function withdraw(params = {}) {
  return {
    action: `${apiUri}cash/withdrawal`,
    options: {
      method: "POST",
      body: JSON.stringify({ amount: +params.amount })
    }
  };
}

export function reconciliationTotals(params = {}) {
  return {
    action: `${apiUri}cash/terminal/reconciliation`,
    options: {
      method: "POST",
      body: JSON.stringify(params)
    }
  };
}

export function getScenarioInfo(id) {
  return {
    action: `${apiUri}cash/scenario/${id}`,
    options: {
      method: "GET"
    }
  };
}

export function createPaymentScenario(params = {}) {
  return {
    action: `${apiUri}cash/payment`,
    options: {
      method: "POST",
      body: JSON.stringify(params)
    }
  };
}

export function createReturnPaymentScenario(params = {}) {
  return {
    action: `${apiUri}cash/return-payment`,
    options: {
      method: "POST",
      body: JSON.stringify(params)
    }
  };
}

export function createReturnPaymentWithCheck(params = {}) {
  return {
    action: `${apiUri}cash/return-payment/with-check`,
    options: {
      method: "POST",
      body: JSON.stringify(params)
    }
  };
}

export function createReturnPaymentWithoutCheck(params = {}) {
  return {
    action: `${apiUri}cash/return-payment/without-check`,
    options: {
      method: "POST",
      body: JSON.stringify(params)
    }
  };
}
