import { apiUri } from "../config";
import { serializationParamsToString } from "../utils";

export function getCdekInfo(params = {}) {
  return {
    action: `${apiUri}cdek${serializationParamsToString(params)}`,
    options: {
      method: "GET"
    }
  };
}
