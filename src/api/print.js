import { apiUri } from "../config";
import { serializationParamsToString } from "../utils";

export function printBarcodes(params) {
  return {
    action: `${apiUri}print/barcodes${serializationParamsToString(params)}`,
    options: { method: "POST" }
  };
}
