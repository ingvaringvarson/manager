import React from "react";
import moment from "moment";
import nanoid from "nanoid";
import { objectToArray } from "../../utils";
import Text from "../../designSystem/atoms/Text";
import FieldContext from "../../components/common/formControls/FieldContext";
import SortButtonContext from "../../components/common/formControls/SortButtonContext";
import {
  maxNotRequiredValue,
  minNotRequiredValue
} from "../../helpers/validators";

export function renderFormFields(fields, forFilters = false) {
  const defaultRestProps = {
    id: "",
    title: "",
    enumerationKey: null,
    queryKeys: [],
    isFilter: forFilters,
    isActive: true,
    afterMap: null,
    shouldNotBeInValue: false
  };

  const defaultFormControlCellProps = {
    cellProps: null,
    render: ({ field }) => {
      return <FieldContext name={field.id} />;
    },
    handlers: null
  };

  const defaultHeadCellProps = {
    sortBy: false,
    sortName: null,
    render: ({ field }) => {
      return field.headCellProps.sortBy ? (
        <SortButtonContext
          title={field.title}
          sortName={field.headCellProps.sortName}
          name={field.id}
        />
      ) : (
        <Text>{field.title}</Text>
      );
    },
    handlers: null
  };

  const defaultBodyCellProps = {
    cellProps: { breakContent: false },
    render: ({ entity, field, enumerations }) => {
      const { id, enumerationKey } = field;
      const value = entity[id];
      if (
        enumerations &&
        enumerationKey &&
        enumerations[enumerationKey] &&
        enumerations[enumerationKey].map &&
        enumerations[enumerationKey].map[value]
      ) {
        return enumerations[enumerationKey].map[value].name;
      }
      return entity.hasOwnProperty(id) && value !== null
        ? String(value)
        : value;
    },
    handlers: null
  };

  const defaultFormControlProps = {
    name: "",
    defaultValue: "",
    value: "",
    autoSubmitDrop: false,
    valuesByChecked: new Map([]),
    checkedByValue: new Map([]),
    type: "",
    notice: "",
    label: "",
    attrs: {
      fullWidth: true,
      autoComplite: "off",
      disabled: false,
      placeholder: ""
    },
    mask: null,
    revertMask: null,
    validators: [],
    options: [],
    afterChange: null,
    searchRequestProps: {
      actionInitDefaultValue: null,
      actionChangeSearch: null,
      selectorDefaultValue: null,
      selectorSearchResult: null,
      params: null,
      keyParam: null,
      getValueFromResult: null
    },
    datePickerProps: {
      dateFormat: "dd.MM.yyyy"
    },
    render: null,
    data: {},
    renderIconAfter: null
  };

  return fields.map(field => {
    if (Array.isArray(field)) {
      return [field[0], renderFormFields(field[1])];
    }

    const {
      bodyCellProps = {},
      headCellProps = {},
      formControlProps = {},
      headLineCellProps = {},
      id,
      ...restProps
    } = field;
    return {
      ...defaultRestProps,
      ...restProps,
      id,
      headCellProps: {
        ...defaultHeadCellProps,
        ...headCellProps
      },
      headLineCellProps: {
        ...defaultFormControlCellProps,
        ...headLineCellProps
      },
      bodyCellProps: {
        ...defaultBodyCellProps,
        ...bodyCellProps
      },
      formControlProps: {
        ...defaultFormControlProps,
        ...formControlProps,
        attrs: {
          ...defaultFormControlProps.attrs,
          ...formControlProps.attrs
        },
        searchRequestProps: {
          ...defaultFormControlProps.searchRequestProps,
          ...formControlProps.searchRequestProps
        },
        name: formControlProps.name ? formControlProps.name : id,
        validators:
          formControlProps.type === "date"
            ? (() => {
                const validators = [
                  maxNotRequiredValue("2999-12-31T00:00:00"),
                  minNotRequiredValue("1919-01-01T00:00:00")
                ];
                if (Array.isArray(formControlProps.validators)) {
                  formControlProps.validators.forEach(v => validators.push(v));
                }

                return validators;
              })()
            : []
      }
    };
  });
}

function getValue(id, field, values) {
  if (!values) return undefined;

  switch (true) {
    case values.hasOwnProperty(id): {
      return Array.isArray(field.formControlProps.defaultValue)
        ? values[id] === "" || values[id] === null
          ? []
          : [].concat(values[id])
        : values[id];
    }
    case !!field.queryKeys.length: {
      const key = field.queryKeys.find(queryKey =>
        values.hasOwnProperty(queryKey)
      );

      if (key) {
        return Array.isArray(field.formControlProps.defaultValue)
          ? values[key] === "" || values[id] === null
            ? []
            : [].concat(values[key])
          : values[key];
      }

      return undefined;
    }
    default:
      return undefined;
  }
}

export function reduceValuesToFormFields(
  values = {},
  enumerations = {},
  _id = null,
  mainValues = null
) {
  return function(fields, field) {
    if (Array.isArray(field)) {
      if (field[0].destructuring) {
        if (Array.isArray(values[field[0].id]) && values[field[0].id].length) {
          return values[field[0].id].reduce((copyFields, value) => {
            return field[1].reduce(
              reduceValuesToFormFields(value, enumerations, null, values),
              copyFields
            );
          }, fields);
        }
        return field[1].reduce(
          reduceValuesToFormFields(values, enumerations, null, values),
          fields
        );
      } else {
        return fields.concat([
          [
            field[0],
            field[1].reduce(
              reduceValuesToFormFields(values, enumerations, null, values),
              []
            )
          ]
        ]);
      }
    }

    const copyField = {
      ...field,
      formControlProps: { ...field.formControlProps }
    };
    let id = field.id;

    if (field.id.includes("__")) {
      const arrId = field.id.split("__");
      id = arrId[0];

      if (_id !== null) {
        copyField.id = `${id}__${_id}`;
      } else {
        if (
          (values.hasOwnProperty("_id") && values.hasOwnProperty(id)) ||
          field.shouldNotBeInValue
        ) {
          copyField.id = `${id}__${values["_id"]}`;
        } else {
          copyField.id = `${id}__`;
        }
      }
    }

    const value = getValue(id, field, values);

    if (
      field.enumerationKey &&
      enumerations.hasOwnProperty(field.enumerationKey)
    ) {
      copyField.formControlProps = {
        ...copyField.formControlProps,
        options: copyField.formControlProps.options.concat(
          enumerations[field.enumerationKey].list.slice()
        )
      };

      copyField.formControlProps = {
        ...copyField.formControlProps,
        value: value !== undefined ? value : field.formControlProps.defaultValue
      };
    } else if (value !== undefined) {
      copyField.formControlProps = {
        ...copyField.formControlProps,
        value: value
      };
    } else if (_id !== null && copyField.id === "_id") {
      copyField.formControlProps = {
        ...copyField.formControlProps,
        value: _id
      };
    }

    const filledField = {
      ...copyField,
      formControlProps: { ...copyField.formControlProps, name: copyField.id }
    };

    return fields.concat(
      filledField.afterMap
        ? filledField.afterMap({
            field: filledField,
            fields,
            values,
            enumerations,
            mainValues
          })
        : filledField
    );
  };
}

export function renderInfoField(field) {
  const defaultProps = {
    id: "",
    key: "",
    // title - is a label of row
    title: "",
    value: null,
    // enumerationKey - is a key of enumeration that will be applied to get value from enumeration
    enumerationKey: null,
    // mask - is a format that will be applied to value
    mask: null,
    mapValueToFields: null,
    view: "default",
    valuesForForm: null,
    // callback to render Title
    renderTitle: null,
    // callback to render Value
    renderValue: null
  };

  return {
    ...defaultProps,
    id: field.key,
    ...field
  };
}

export function renderInfoFields(fields) {
  return fields.map(field => {
    return {
      ...field,
      ...renderInfoField(field)
    };
  });
}

function mapValueToInfoField(values = {}, enumerations = {}) {
  return function(field) {
    const {
      enumerationKey,
      key,
      mask,
      mapValueToFields,
      mapFieldInMapping
    } = field;

    if (key === "_root") {
      return {
        ...field,
        value: values
      };
    }

    const value = mask ? mask(values[key]) : values[key];

    switch (true) {
      case !!mapFieldInMapping:
        return mapFieldInMapping({ field, values, enumerations });
      case !values.hasOwnProperty(key) || value === null:
        return field;
      case !!(
        enumerationKey &&
        enumerations[enumerationKey] &&
        !Array.isArray(value) &&
        enumerations[enumerationKey].map.hasOwnProperty(value)
      ):
        return {
          ...field,
          value: enumerations[enumerationKey].map[value].name
        };
      case !!(
        enumerationKey &&
        enumerations[enumerationKey] &&
        Array.isArray(value)
      ):
        return {
          ...field,
          value: value.reduce((arr, id) => {
            if (!enumerations[enumerationKey].map.hasOwnProperty(id)) {
              return arr;
            }

            return arr.concat(enumerations[enumerationKey].map[id].name);
          }, [])
        };
      case !!mapValueToFields:
        return {
          ...field,
          value: mapValueToFields(value, enumerations)
        };
      default:
        return {
          ...field,
          value
        };
    }
  };
}

export function mapValuesToFields(
  fieldProps = {},
  enumerations = {},
  values = {}
) {
  const valuesState = values ? values : {};
  return {
    ...fieldProps,
    infoFields: fieldProps.infoFields.map(
      mapValueToInfoField(valuesState, enumerations)
    ),
    formFields: fieldProps.formFields.reduce(
      reduceValuesToFormFields(valuesState, enumerations),
      []
    ),
    fieldsForAdd: fieldProps.formFields.reduce(
      reduceValuesToFormFields({}, enumerations),
      []
    )
  };
}

export function mapEntitiesToList(
  fieldProps = [],
  enumerations = {},
  entities = []
) {
  const entitiesState = entities ? entities : [];
  return {
    ...fieldProps,
    infoFields: entitiesState.map(entity => {
      return [
        entity._id,
        fieldProps.infoFields.map(mapValueToInfoField(entity, enumerations))
      ];
    }),
    formFields: entitiesState.map(entity => {
      return [
        entity._id,
        fieldProps.formFields.reduce(
          reduceValuesToFormFields(entity, enumerations),
          []
        )
      ];
    }),
    fieldsForAdd: fieldProps.formFields.reduce(
      reduceValuesToFormFields({}, enumerations, nanoid()),
      []
    )
  };
}

export function mapValuesToGroupedList(enumerationKey, key = "value") {
  return function(entities = [], enumerations = {}) {
    const entitiesState = entities ? entities : [];

    return entitiesState.reduce((groups, entity) => {
      const title =
        enumerations[enumerationKey] &&
        enumerations[enumerationKey].map.hasOwnProperty(entity.type)
          ? enumerations[enumerationKey].map[entity.type].name
          : "";

      const index = groups.findIndex(
        item => Array.isArray(item[0]) && item[0] === entity.type
      );
      const copyGroups = groups.slice();

      if (!~index) {
        return copyGroups.concat([
          [
            entity.type,
            [
              [
                entity._id,
                renderInfoField({
                  ...entity,
                  value: entity[key],
                  title,
                  id: entity._id
                })
              ]
            ]
          ]
        ]);
      }

      copyGroups[index] = [
        entity.type,
        copyGroups[index][1].concat([
          [
            entity._id,
            renderInfoField({
              ...entity,
              value: entity[key],
              title,
              id: entity._id
            })
          ]
        ])
      ];
      return copyGroups;
    }, []);
  };
}

export function mapEntitiesToGroupedList(enumerationKey, key = "value") {
  return function(fieldProps = [], enumerations = {}, entities = []) {
    const entitiesState = entities ? entities : [];
    return {
      ...fieldProps,
      infoFields: mapValuesToGroupedList(enumerationKey, key)(
        entities,
        enumerations
      ),
      formFields: entitiesState.map(entity => {
        return [
          entity._id,
          fieldProps.formFields.reduce(
            reduceValuesToFormFields(entity, enumerations),
            []
          )
        ];
      }),
      fieldsForAdd: fieldProps.formFields.reduce(
        reduceValuesToFormFields({}, enumerations, nanoid()),
        []
      )
    };
  };
}

export function mapClientCodeHeading(fieldProps, enumerations, values) {
  const {
    client: { code }
  } = values;
  const defaultFieldProps = mapValuesToFields(fieldProps, enumerations, values);

  return {
    ...defaultFieldProps,
    subtitle: `id${code}`
  };
}

export function mapFormFieldsToQuery(fields, values, query = {}) {
  return objectToArray(fields).reduce(
    (newQuery, field) => {
      switch (true) {
        case !values.hasOwnProperty(field.id) ||
          values[field.id] === null ||
          (Array.isArray(values[field.id]) && !values[field.id].length): {
          if (newQuery.hasOwnProperty(field.id)) delete newQuery[field.id];
          if (field.queryKeys.length) {
            field.queryKeys.forEach(key => {
              if (newQuery.hasOwnProperty(key)) delete newQuery[key];
            });
          }
          return newQuery;
        }
        case !!field.queryKeys.length: {
          return field.queryKeys.reduce((newQueryWithQueryKeys, key, index) => {
            let value = values[field.id];
            if (field.formControlProps.type === "date") {
              if (!index) {
                value = `${moment(value).format("YYYY-MM-DDT")}00:00:00`;
              }
              if (index) {
                value = `${moment(value).format("YYYY-MM-DDT")}23:59:59`;
              }
            }
            return {
              ...newQueryWithQueryKeys,
              [key]: value
            };
          }, newQuery);
        }
        default:
          return {
            ...newQuery,
            [field.id]: values[field.id]
          };
      }
    },
    { ...query }
  );
}

export function mapValuesToGroupedListByContacts(
  entities = [],
  enumerations = {}
) {
  const activeEntities = entities
    ? entities.filter(entity => entity.is_active)
    : [];

  return mapValuesToGroupedList("contactType")(activeEntities, enumerations);
}

export function mapEntitiesToGroupedListByContacts(
  fieldProps = {},
  enumerations = {},
  entities = []
) {
  const activeEntities = entities
    ? entities.filter(entity => entity.is_active)
    : [];

  return mapEntitiesToGroupedList("contactType")(
    fieldProps,
    enumerations,
    activeEntities
  );
}
