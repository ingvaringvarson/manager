import React from "react";
import { getDate, getTime, getValueInDepth } from "../../utils";
import { colors } from "../../constants/orderStateColors";
import { prop } from "styled-tools";
import styled from "styled-components";

const OrderStateText = styled.span`
  color: ${prop("color", "inherit")};
`;

export function renderDeliveryType({ entity }) {
  switch (entity.delivery_type) {
    case 1: {
      return `Самовывоз\n${
        entity.pickup && entity.pickup.warehouse_name
          ? entity.pickup.warehouse_name
          : ""
      }`;
    }
    case 2: {
      let storeName = "";
      let datePlan = "";
      let dateFact = "";

      if (entity.deliverylaf24) {
        storeName = entity.deliverylaf24.warehouse_name || "";
        if (entity.deliverylaf24.date_plan) {
          const date = new Date(entity.deliverylaf24.date_plan);

          datePlan = `желаемое время доставки ${getTime(date)} ${getDate(
            date
          )}`;
        }
        if (entity.deliverylaf24.date_fact) {
          const date = new Date(entity.deliverylaf24.date_fact);

          dateFact = `ожидаемое время доставки ${getTime(date)} ${getDate(
            date
          )}`;
        }
      }

      return `Доставка LAF24\n${storeName}\n${datePlan}\n${dateFact}`;
    }
    case 3: {
      let storeName = "";
      let dateFact = "";

      if (entity.deliveryDC) {
        storeName = entity.deliveryDC.warehouse_name || "";
        if (entity.deliveryDC.date_fact) {
          const date = new Date(entity.deliveryDC.date_fact);

          dateFact = `ожидаемое время доставки ${getTime(date)} ${getDate(
            date
          )}`;
        }
      }

      return `Доставка СДЕК\n${storeName}\n${dateFact}`;
    }
    default:
      return null;
  }
}

export function maxDateAmongPositions(key) {
  return function({ entity }) {
    if (!entity || !entity.positions || !key) return null;

    const maxDate = entity.positions.reduce((currentMaxDate, position) => {
      if (!position[key]) return currentMaxDate;

      const currentDate = new Date(position[key]);

      if (!currentMaxDate || +currentMaxDate < +currentDate) {
        return currentDate;
      }

      return currentMaxDate;
    }, "");

    return maxDate ? `${getDate(maxDate)} ${getTime(maxDate)}` : "";
  };
}

export function renderDateTime(propKey, withTime = true) {
  return function({ entity }) {
    const value = Array.isArray(propKey)
      ? getValueInDepth(entity, propKey)
      : entity[propKey];

    if (!propKey || !value) return null;

    const currentDate = new Date(value);

    return withTime
      ? `${getDate(currentDate)} ${getTime(currentDate)}`
      : getDate(currentDate);
  };
}

export function renderSum(propKey) {
  return function({ entity }) {
    if (!propKey || !entity) return null;
    const value = Array.isArray(propKey)
      ? getValueInDepth(entity, propKey)
      : entity[propKey];

    if (!value && value !== 0) return null;

    return `${value} руб.`;
  };
}

export function renderEnumerationValue(enumerationKey, propKey) {
  return function({ entity, enumerations }) {
    if (!enumerationKey || !propKey || !enumerations) return null;

    const value = Array.isArray(propKey)
      ? getValueInDepth(entity, propKey)
      : entity[propKey];

    if (value === null || value === undefined) return null;

    return enumerations[enumerationKey] &&
      enumerations[enumerationKey].map[value]
      ? enumerations[enumerationKey].map[value].name
      : null;
  };
}

export function renderOrderState(enumerationKey, propKey) {
  return function({ entity, enumerations }) {
    if (!enumerationKey || !propKey || !enumerations) return null;

    const colorEntity = colors.find(color => color.id === entity[propKey]);

    return enumerations["orderState"] &&
      enumerations["orderState"].map[entity[propKey]] ? (
      <OrderStateText color={colorEntity ? colorEntity.color : null}>
        {enumerations["orderState"].map[entity[propKey]].name}
      </OrderStateText>
    ) : null;
  };
}

export function renderBooleanValue(
  propKey = null,
  trueAnswer = "",
  falseAnswer = "",
  emptyAnswer = ""
) {
  return function({ entity }) {
    if (!propKey || !entity || entity[propKey] === null) return emptyAnswer;

    return entity[propKey] ? trueAnswer : falseAnswer;
  };
}
