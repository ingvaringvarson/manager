export function disableFieldHandler(field) {
  return {
    ...field,
    formControlProps: {
      ...field.formControlProps,
      attrs: {
        ...field.formControlProps.attrs,
        disabled: true
      }
    }
  };
}

export function forUserAgentCodeHandler({ field, values }) {
  if (
    (values.hasOwnProperty("_for_user") && values._for_user !== "") ||
    (values.hasOwnProperty("_agent_code") && values._for_user !== "")
  ) {
    return disableFieldHandler(field);
  }

  return field;
}

export function objectIdCreateForUserHandler({ field, values }) {
  if (
    (values.hasOwnProperty("object_id_create") &&
      values.object_id_create.length) ||
    (values.hasOwnProperty("_for_user") && values._for_user !== "")
  ) {
    return disableFieldHandler(field);
  }

  return field;
}
