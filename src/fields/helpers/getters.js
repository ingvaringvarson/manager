import { getValueInDepth } from "../../utils";

//ToDo: http://jira.laf24.ru/browse/MAN-1744

export const getPersonFullName = entity => {
  const surname = getValueInDepth(entity, ["person", "person_surname"]);
  const name = getValueInDepth(entity, ["person", "person_name"]);

  if (!surname && !name) return null;

  return `${surname ? `${surname} ` : ""}${name || ""}`;
};
export const getNameForEntity = entity => {
  return getValueInDepth(entity, ["name"]);
};
export const getNameCompanyShort = entity => {
  return getValueInDepth(entity, ["company", "company_name_short"]);
};
export const getOrderCode = entity =>
  getValueInDepth(entity, ["order", "code"]);
export const getFirstLevelOption = option => entity =>
  getValueInDepth(entity, [option]);

export const getIdForEntity = entity => getValueInDepth(entity, ["id"]);
export const getOrderId = entity => getValueInDepth(entity, ["order", "id"]);
export const getIdByEmployee = entity =>
  getValueInDepth(entity, ["employees", 0, "id"]);
export const getAgentName = entity => {
  return getPersonFullName(entity) || getNameCompanyShort(entity);
};
