import React from "react";

import {
  renderFormFields,
  renderDateTime,
  renderEnumerationValue
} from "../../helpers";
import { sortByField } from "../../templates/serviceTemplates";
import { maskOnlyText } from "../../../helpers/masks";
import { searchRequestPropsForWarehouses } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

import SortButtonContext from "../../../components/common/formControls/SortButtonContext";

export const fields = renderFormFields([
  {
    id: "code",
    title: "Номер",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.code || "";
      }
    },
    formControlProps: {
      mask: maskOnlyText,
      type: "text",
      attrs: {
        placeholder: "Введите номер"
      }
    },
    headCellProps: {
      sortBy: true,
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    }
  },
  {
    id: "type",
    title: "Тип",
    bodyCellProps: { render: renderEnumerationValue("taskType", "type") },
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Введите тип"
      }
    },
    headCellProps: {
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    }
  },
  {
    id: "state",
    title: "Статус",
    bodyCellProps: { render: renderEnumerationValue("taskState", "state") },
    formControlProps: {
      type: "text",
      attrs: { disabled: true }
    },
    headCellProps: {
      sortBy: true,
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    }
  },
  {
    id: "executor_name",
    headCellProps: { sortBy: true },
    title: "Исполнитель",
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Введите исполнителя"
      }
    }
  },
  {
    id: "director_name",
    title: "Постановщик",
    headCellProps: { sortBy: true },
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Введите поставщика"
      }
    }
  },
  {
    id: "warehouse_id",
    title: "Склад приемки",
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Введите склад отгрузки" },
      searchRequestProps: searchRequestPropsForWarehouses()
    },
    bodyCellProps: {
      render: ({ entity }) => entity.warehouse_name || ""
    }
  },
  {
    id: "warehouse_to_id",
    title: "Склад отгрузки",
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Введите склад приёмки" },
      searchRequestProps: searchRequestPropsForWarehouses()
    },
    bodyCellProps: {
      render: ({ entity }) => entity.warehouse_to_name || ""
    }
  },
  {
    id: "object_date_create",
    title: "Дата создания",
    queryKeys: ["datefrom_created", "dateto_created"],
    bodyCellProps: {
      render: renderDateTime("object_date_create")
    },
    formControlProps: {
      type: "date",
      attrs: {
        placeholder: "Введите дату создания"
      }
    },
    headCellProps: {
      sortBy: true,
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    }
  },
  {
    id: "date",
    title: "Дата реализации",
    queryKeys: ["date_from", "date_to"],
    bodyCellProps: {
      render: renderDateTime("date")
    },
    formControlProps: {
      type: "date",
      attrs: {
        placeholder: "Введите дату реализации"
      }
    },
    headCellProps: {
      sortBy: true,
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    }
  },
  {
    id: "reason",
    title: "Причина",
    headCellProps: { sortBy: true },
    bodyCellProps: { render: renderEnumerationValue("taskReason", "reason") },
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Выберите причину"
      }
    }
  },
  sortByField
]);
