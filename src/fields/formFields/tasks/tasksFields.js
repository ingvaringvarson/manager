import React from "react";
import {
  renderFormFields,
  renderDateTime,
  renderEnumerationValue
} from "../../helpers/index";
import { searchRequestPropsForAgents } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

import SortButtonContext from "../../../components/common/formControls/SortButtonContext";

export const fields = renderFormFields([
  {
    id: "code",
    title: "Номер",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.code || "";
      }
    },
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Введите номер"
      }
    },
    headCellProps: {
      sortBy: true,
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    }
  },
  {
    id: "type_v2",
    title: "Тип",
    queryKeys: ["type"],
    enumerationKey: "taskType",
    bodyCellProps: { render: renderEnumerationValue("taskType", "type") },
    formControlProps: {
      type: "select",
      attrs: {
        placeholder: "Выберите тип"
      }
    },
    headCellProps: {
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    }
  },
  {
    id: "state_v2",
    title: "Статус",
    queryKeys: ["state"],
    enumerationKey: "taskState",
    bodyCellProps: { render: renderEnumerationValue("taskState", "state") },
    formControlProps: {
      type: "select",
      attrs: {
        placeholder: "Выберите статус"
      }
    },
    headCellProps: {
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    }
  },
  {
    id: "executor_name",
    title: "Исполнитель",
    queryKeys: ["executor_id"],
    formControlProps: {
      attrs: {
        placeholder: "Введите исполнителя"
      },
      value: [],
      defaultValue: [],
      type: "select_search_request",
      searchRequestProps: searchRequestPropsForAgents({
        params: {
          roles: [5]
        }
      })
    }
  },
  {
    id: "director_name",
    title: "Постановщик",
    queryKeys: ["director_id"],
    formControlProps: {
      attrs: {
        placeholder: "Введите поставщика"
      },
      value: [],
      defaultValue: [],
      type: "select_search_request",
      searchRequestProps: searchRequestPropsForAgents({
        params: {
          roles: [5]
        }
      })
    }
  },
  {
    id: "object_date_create",
    title: "Дата создания",
    queryKeys: ["datefrom_created", "dateto_created"],
    bodyCellProps: {
      render: renderDateTime("object_date_create")
    },
    formControlProps: {
      type: "date",
      attrs: {
        placeholder: "Выберите дату создания"
      }
    },
    headCellProps: {
      sortBy: true,
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    }
  },
  {
    id: "date",
    title: "Дата реализации",
    queryKeys: ["date_from", "date_to"],
    bodyCellProps: {
      render: renderDateTime("date")
    },
    formControlProps: {
      type: "date",
      attrs: {
        placeholder: "Выберите дату реализации"
      }
    },
    headCellProps: {
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    }
  },
  {
    id: "reason",
    title: "Причина",
    enumerationKey: "taskReason",
    bodyCellProps: { render: renderEnumerationValue("taskReason", "reason") },
    formControlProps: {
      type: "select",
      value: [],
      defaultValue: [],
      attrs: {
        placeholder: "Выберите причину"
      }
    }
  }
]);
