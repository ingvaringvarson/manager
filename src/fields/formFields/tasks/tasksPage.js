import {
  renderFormFields,
  renderDateTime,
  renderEnumerationValue,
  disableFieldHandler
} from "../../helpers";
import { maxDateRange, minDateRange } from "../../../helpers/validators";
import { maskOnlyNumbers, maskOnlyText } from "../../../helpers/masks";
import { sortByField } from "../../templates/serviceTemplates";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForWarehouses
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

const code = {
  id: "code",
  title: "Номер",
  headCellProps: { sortBy: true },
  formControlProps: {
    mask: maskOnlyText,
    type: "text",
    attrs: {
      placeholder: "Введите номер"
    }
  }
};

const typeV2 = {
  id: "type_v2",
  title: "Тип",
  queryKeys: ["type"],
  headCellProps: { sortBy: true, sortName: "type" },
  enumerationKey: "taskType",
  bodyCellProps: { render: renderEnumerationValue("taskType", "type") },
  formControlProps: {
    type: "select",
    attrs: {
      placeholder: "Введите тип"
    }
  }
};

const stateV2 = {
  id: "state_v2",
  title: "Статус",
  queryKeys: ["state"],
  headCellProps: { sortBy: true, sortName: "state" },
  enumerationKey: "taskState",
  bodyCellProps: { render: renderEnumerationValue("taskState", "state") },
  formControlProps: {
    type: "select",
    attrs: {
      placeholder: "Введите статус"
    }
  }
};

const executorName = {
  id: "executor_name",
  title: "Исполнитель",
  queryKeys: ["executor_id"],
  formControlProps: {
    attrs: {
      placeholder: "Введите исполнителя"
    },
    value: [],
    defaultValue: [],
    type: "select_search_request",
    searchRequestProps: searchRequestPropsForAgents({
      params: {
        roles: [5]
      }
    })
  }
};

const directorName = {
  id: "director_name",
  title: "Постановщик",
  queryKeys: ["director_id"],
  formControlProps: {
    attrs: {
      placeholder: "Введите поставщика"
    },
    value: [],
    defaultValue: [],
    type: "select_search_request",
    searchRequestProps: searchRequestPropsForAgents({
      params: {
        roles: [5]
      }
    })
  }
};

const objectDateCreate = {
  id: "object_date_create",
  title: "Дата создания",
  queryKeys: ["datefrom_created", "dateto_created"],
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: renderDateTime("object_date_create")
  },
  formControlProps: {
    type: "date",
    attrs: {
      placeholder: "Выберите дату создания"
    }
  }
};

const date = {
  id: "date",
  title: "Дата требуемой реализации",
  queryKeys: ["date_from", "date_to"],
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: renderDateTime("date")
  },
  formControlProps: {
    type: "date",
    attrs: {
      placeholder: "Выберите дату реализации"
    }
  }
};

const reason = {
  id: "reason",
  title: "Причина",
  enumerationKey: "taskReason",
  bodyCellProps: { render: renderEnumerationValue("taskReason", "reason") },
  formControlProps: {
    type: "select",
    value: [],
    defaultValue: [],
    attrs: {
      placeholder: "Выберите причину"
    }
  }
};

export const fieldsForTable = renderFormFields([
  code,
  typeV2,
  stateV2,
  executorName,
  directorName,
  objectDateCreate,
  date,
  reason,
  sortByField
]);

const datefromCreated = {
  id: "datefrom_created",
  formControlProps: {
    validators: [maxDateRange("dateto_created")],
    type: "date",
    attrs: { placeholder: "Выберите дату" }
  }
};

const datetoCreated = {
  id: "dateto_created",
  formControlProps: {
    validators: [minDateRange("datefrom_created")],
    type: "date",
    attrs: { placeholder: "Выберите дату" }
  }
};

const dateFrom = {
  id: "date_from",
  formControlProps: {
    validators: [maxDateRange("dateto_to")],
    type: "date",
    attrs: { placeholder: "Выберите дату" }
  }
};
const dateTo = {
  id: "date_to",
  formControlProps: {
    validators: [minDateRange("date_from")],
    type: "date",
    attrs: { placeholder: "Выберите дату" }
  }
};
const orderCode = {
  id: "_order_code",
  title: "Заказ",
  formControlProps: {
    label: "Заказ",
    mask: maskOnlyText,
    type: "text",
    attrs: { placeholder: "Введите код заказа" }
  }
};
const agentCode = {
  id: "_agent_code",
  title: "Клиент",
  formControlProps: {
    label: "Клиент",
    mask: maskOnlyNumbers,
    type: "text",
    attrs: { placeholder: "Введите код клиента" }
  }
};
const ean13 = {
  id: "ean_13",
  title: "Штрих-код",
  formControlProps: {
    label: "Штрих-код",
    mask: maskOnlyNumbers,
    type: "text",
    attrs: { placeholder: "Введите штрих-код товара" }
  }
};
const article = {
  id: "article",
  title: "Артикул",
  formControlProps: {
    label: "Артикул",
    mask: maskOnlyText,
    type: "text",
    attrs: { placeholder: "Введите артикул" }
  }
};
const warehouseId = {
  id: "warehouse_id",
  title: "Склад приемки",
  formControlProps: {
    label: "Склад приемки",
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Введите склад отгрузки" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};
const warehouseToId = {
  id: "warehouse_to_id",
  title: "Склад отгрузки",
  formControlProps: {
    label: "Склад отгрузки",
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Введите склад приёмки" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};
const description = {
  id: "description",
  title: "Описание",
  formControlProps: {
    label: "Описание",
    type: "text",
    name: "description",
    attrs: { placeholder: "Введите текст для поиска по описанию" }
  }
};
const type = {
  id: "type",
  title: "Выберите тип",
  enumerationKey: "taskType",
  formControlProps: {
    selectedAll: "Поиск по всем типам задач",
    isMultiple: true,
    value: [],
    defaultValue: [],
    type: "select",
    attrs: { placeholder: "Выберите тип" }
  }
};
const state = {
  id: "state",
  title: "Поиск всем статусам задач",
  enumerationKey: "taskState",
  formControlProps: {
    selectedAll: "Поиск по всем статусам задач",
    isMultiple: true,
    value: [],
    defaultValue: [],
    type: "select",
    attrs: { placeholder: "Выберите статус" }
  }
};
const forUser = {
  id: "_for_user",
  title: "Мной созданные задачи",
  formControlProps: {
    type: "checkbox",
    valuesByChecked: new Map([[false, ""], [true, "1"]]),
    checkedByValue: new Map([["", false], ["1", true]]),
    label: "Мной созданные задачи",
    afterChange: ({ field, values }) => {
      if (
        (values.hasOwnProperty("object_id_create") &&
          values.object_id_create.length) ||
        (values.hasOwnProperty("_agent_code") && values._created_by_user !== "")
      ) {
        return disableFieldHandler(field);
      }

      return field;
    }
  },
  afterMap: ({ field, values }) => {
    if (
      (values.hasOwnProperty("object_id_create") &&
        values.object_id_create.length) ||
      (values.hasOwnProperty("_agent_code") && values._created_by_user !== "")
    ) {
      return disableFieldHandler(field);
    }

    return field;
  }
};
const createdForUser = {
  id: "_created_for_user",
  title: "Задачи, созданные на меня",
  formControlProps: {
    type: "checkbox",
    valuesByChecked: new Map([[false, ""], [true, "1"]]),
    checkedByValue: new Map([["", false], ["1", true]]),
    label: "Задачи, созданные на меня",
    afterChange: ({ field, values }) => {
      if (
        (values.hasOwnProperty("executor_id") && values.executor_id.length) ||
        (values.hasOwnProperty("_agent_code") &&
          values._created_for_user !== "")
      ) {
        return disableFieldHandler(field);
      }

      return field;
    }
  },
  afterMap: ({ field, values }) => {
    if (
      (values.hasOwnProperty("executor_id") && values.executor_id.length) ||
      (values.hasOwnProperty("_agent_code") && values._created_for_user !== "")
    ) {
      return disableFieldHandler(field);
    }

    return field;
  }
};
const executedByUser = {
  id: "_executed_by_user",
  title: "Мной выполненные задачи",
  formControlProps: {
    type: "checkbox",
    valuesByChecked: new Map([[false, ""], [true, "1"]]),
    checkedByValue: new Map([["", false], ["1", true]]),
    label: "Мной выполненные задачи",
    afterChange: ({ field, values }) => {
      if (
        (values.hasOwnProperty("object_id_edit") &&
          values.object_id_edit.length) ||
        (values.hasOwnProperty("_agent_code") &&
          values._executed_by_user !== "")
      ) {
        return disableFieldHandler(field);
      }

      return field;
    }
  },
  afterMap: ({ field, values }) => {
    if (
      (values.hasOwnProperty("object_id_edit") &&
        values.object_id_edit.length) ||
      (values.hasOwnProperty("_agent_code") && values._executed_by_user !== "")
    ) {
      return disableFieldHandler(field);
    }

    return field;
  }
};
export const fieldsForFilters = renderFormFields([
  [
    {
      destructuring: false,
      isRange: false,
      id: null,
      name: "Поиск по дате создания"
    },
    [datefromCreated, datetoCreated]
  ],
  [
    {
      destructuring: false,
      isRange: false,
      id: null,
      name: "Поиск по дате реализации"
    },
    [dateFrom, dateTo]
  ],
  orderCode,
  agentCode,
  ean13,
  article,
  warehouseId,
  warehouseToId,
  description,
  type,
  state,
  forUser,
  createdForUser,
  executedByUser
]);
