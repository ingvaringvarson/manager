import {
  renderFormFields,
  renderDateTime,
  renderEnumerationValue
} from "../../helpers/index";
import { sortByField } from "../../templates/serviceTemplates";

export const fields = renderFormFields([
  {
    id: "code",
    title: "Номер",
    bodyCellProps: {
      render: ({ entity }) => entity.code || ""
    }
  },
  {
    id: "type",
    title: "Тип",
    bodyCellProps: { render: renderEnumerationValue("taskType", "type") }
  },
  {
    id: "state",
    title: "Статус",
    bodyCellProps: { render: renderEnumerationValue("taskState", "state") }
  },
  {
    id: "executor_name",
    title: "Исполнитель",
    bodyCellProps: {
      render: ({ entity }) => entity.executor_name || ""
    }
  },
  {
    id: "director_name",
    title: "Постановщик",
    bodyCellProps: {
      render: ({ entity }) => entity.director_name || ""
    }
  },
  {
    id: "warehouse_name",
    title: "Склад отгрузки",
    bodyCellProps: {
      render: ({ entity }) => entity.warehouse_name || ""
    }
  },
  {
    id: "warehouse_to_name",
    title: "Склад приемки",
    bodyCellProps: {
      render: ({ entity }) => entity.warehouse_to_name || ""
    }
  },
  {
    id: "object_date_create",
    title: "Дата создания",
    queryKeys: ["datefrom_created", "dateto_created"],
    bodyCellProps: {
      render: renderDateTime("object_date_create")
    }
  },
  {
    id: "date",
    title: "Дата реализации",
    queryKeys: ["date_from", "date_to"],
    bodyCellProps: {
      render: renderDateTime("date")
    }
  },
  {
    id: "reason",
    title: "Причина",
    bodyCellProps: { render: renderEnumerationValue("taskReason", "reason") }
  },
  sortByField
]);
