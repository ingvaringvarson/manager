import { renderFormFields } from "../../helpers";

export const fields = renderFormFields([
  {
    id: "brand",
    title: "Товар",
    headControlProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.brand : "";
      }
    }
  },
  {
    id: "article",
    title: "",
    headControlProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.article : "";
      }
    }
  },
  {
    id: "name",
    title: "",
    headControlProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.name : "";
      }
    }
  },
  {
    id: "barcode",
    title: "Штрих-код",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product && entity.product.type === 1
          ? ""
          : "Учет не по шк";
      }
    },
    afterMap: ({ fields, field, values }) => {
      const { productUnit } = values;
      if (productUnit) {
        return {
          ...field,
          bodyCellProps: {
            ...field.bodyCellProps,
            render: ({ entity }) => {
              return entity.product && entity.product.type === 1
                ? productUnit.ean_13
                : "Учет не по шк";
            }
          }
        };
      }
      return field;
    }
  }
]);
