import React from "react";

import {
  searchRequestPropsForAgents,
  searchRequestPropsForOrders,
  searchRequestPropsForWarehouses
} from "../../../../components/common/form/SearchRequestControl/searchRequestProps";
import { getCurrentDateInISO, getValueInDepth } from "../../../../utils";

import OrderField from "../../../../components/blocks/tasks/OrderField";
import ClientField from "../../../../components/blocks/tasks/ClientField";
import { getFirstLevelOption, getIdForEntity } from "../../../helpers";
import { required } from "../../../../helpers/validators";

//ToDo: проверить филды с типом select_search_request http://jira.laf24.ru/browse/MAN-1743

export const typeField = {
  id: "type",
  enumerationKey: "taskType",
  formControlProps: {
    type: "select",
    label: "Тип задачи",
    attrs: { placeholder: "Выберите тип задачи" },
    validators: [required]
  },
  afterMap({ field, enumerations }) {
    const typeEnums = getValueInDepth(enumerations, [
      field.enumerationKey,
      "list"
    ]);
    return typeEnums === null
      ? field
      : {
          ...field,
          formControlProps: {
            ...field.formControlProps,
            options: typeEnums.filter(o => o.id !== 0)
          }
        };
  }
};

export const updateTypeField = {
  id: "type",
  enumerationKey: "taskType",
  formControlProps: {
    type: "select",
    label: "Тип задачи",
    attrs: { placeholder: "Выберите тип задачи" }
  }
};

export const dateField = {
  id: "date",
  formControlProps: {
    type: "date",
    label: "Дата требуемой реализации",
    attrs: { placeholder: "Выберите дату" },
    value: getCurrentDateInISO(),
    validators: [required]
  }
};

export const stateField = {
  id: "state",
  enumerationKey: "taskState",
  formControlProps: {
    type: "select",
    value: [1],
    defaultValue: [1],
    attrs: { disabled: true, placeholder: "Не выполнено" },
    label: "Статус задачи	"
  }
};

export const updateStateField = {
  id: "state",
  enumerationKey: "taskState",
  formControlProps: {
    type: "select",
    defaultValue: [1],
    label: "Статус задачи",
    attrs: { placeholder: "Выберите статус задачи" }
  }
};

export const executorField = {
  id: "executor_id",
  formControlProps: {
    type: "select_search_request",
    label: "Исполнитель",
    attrs: { placeholder: "Введите фамилию исполнителя" },
    value: [],
    defaultValue: [],
    searchRequestProps: searchRequestPropsForAgents({
      params: {
        roles: [5],
        types: [1]
      }
    })
  }
};

export const warehouseField = {
  id: "warehouse_id",
  formControlProps: {
    type: "select_search_request",
    label: "Склад отгрузки",
    attrs: { placeholder: "Выберите склад" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};

export const warehouseIdAccept = {
  id: "warehouse_to_id",
  formControlProps: {
    type: "select_search_request",
    label: "Склад приемки",
    attrs: { placeholder: "Выберите склад" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};

export const warehouseToField = {
  id: "warehouse_to_id",
  enumerationKey: "",
  formControlProps: {
    type: "select_search_request",
    name: "warehouse_to_id",
    label: "Склад приемки",
    attrs: { placeholder: "Введите название склада" },
    value: [],
    defaultValue: [],
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};

export const nameField = {
  id: "name",
  formControlProps: {
    type: "text",
    label: "Заголовок",
    attrs: { placeholder: "Введите название задачи" }
  }
};

export const descriptionField = {
  id: "description",
  formControlProps: {
    type: "textarea",
    label: "Описание",
    attrs: { placeholder: "Введите описание задачи" }
  }
};

export const agentField = {
  id: "agent_id",
  formControlProps: {
    type: "select_search_request",
    label: "Клиент",
    attrs: { placeholder: "Введите код клиента" },
    renderAfterField: ({ field, fields, values, ...restProps }) => {
      return (
        <ClientField
          {...restProps}
          field={field}
          fields={fields}
          values={values}
        />
      );
    },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      key: "code",
      getName: getFirstLevelOption("code"),
      getId: getIdForEntity
    })
  }
};

export const orderField = {
  id: "order_id",
  formControlProps: {
    type: "select_search_request",
    label: "Заказ",
    attrs: { placeholder: "Введите код заказа" },
    renderAfterField: ({ field, fields, values, ...restProps }) => {
      return (
        <OrderField
          {...restProps}
          field={field}
          fields={fields}
          values={values}
        />
      );
    },
    searchRequestProps: searchRequestPropsForOrders()
  }
};

export const reasonField = {
  id: "reason",
  enumerationKey: "taskReason",
  formControlProps: {
    type: "select",
    label: "Причина",
    attrs: { placeholder: "Введите причину статуса" },
    value: [],
    defaultValue: []
  }
};

export const deliveryAddress = {
  id: "location",
  formControlProps: {
    type: "text",
    label: "Адрес доставки",
    attrs: { placeholder: "Введите адрес доставки" }
  }
};

export const productWithHasOwnRepresentationField = {
  id: "product",
  hasOwnRepresentation: true,
  afterMap: ({ field, fields, values, mainValues }) => {
    return {
      ...field,
      helperData: {
        field,
        fields,
        values,
        mainValues
      },
      isActive: values.type === 1
    };
  }
};

export const articleField = {
  id: "article",
  enumerationKey: "",
  formControlProps: {
    type: "select_search_request",
    name: "article",
    label: "Артикул",
    attrs: { placeholder: "Введите артикул" },
    value: [],
    defaultValue: []
  }
};
