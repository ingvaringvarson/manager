export function fieldValuesTypeCheck(
  fieldSkeleton = null,
  typesArray = [],
  shouldInclude = false,
  callbackToReturnType = null
) {
  return {
    ...fieldSkeleton,
    formControlProps: {
      ...fieldSkeleton.formControlProps,
      afterChange: ({ field, values }) => {
        const type = Array.isArray(values.type)
          ? values.type.length && +values.type[0]
          : +values.type;

        const isActive = shouldInclude
          ? typesArray.some(curType => curType === type)
          : !typesArray.some(curType => curType === type);

        return {
          ...field,
          isActive
        };
      }
    },
    afterMap: ({ field, fields, values, mainValues, ...restProps }) => {
      const type = callbackToReturnType
        ? callbackToReturnType({
            field,
            fields,
            values,
            mainValues,
            ...restProps
          })
        : (Array.isArray(values.type) &&
            values.type.length &&
            +values.type[0]) ||
          +values.type;

      const isActive = shouldInclude
        ? typesArray.some(curType => curType === type)
        : !typesArray.some(curType => curType === type);

      return {
        ...field,
        isActive
      };
    }
  };
}

export function fieldValuesChecking(
  fieldSkeleton = null,
  rules = [
    {
      values: [],
      shouldInclude: false,
      callbackToReturnValue: null,
      byCallback: false,
      valueName: "type"
    }
  ]
) {
  return {
    ...fieldSkeleton,
    afterMap: ({ field, values, fields, mainValues }) => {
      const isActive = rules.every(rule => {
        const value = rule.byCallback
          ? rule.callbackToReturnValue({ field, values, mainValues })
          : values[rule.valueName];

        return rule.shouldInclude
          ? rule.values.some(curVal => curVal === value)
          : !rule.values.some(curVal => curVal === value);
      });

      return {
        ...field,
        isActive
      };
    }
  };
}
