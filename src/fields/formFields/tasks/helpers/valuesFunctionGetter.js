export function getTypeOfMainValues({ field, mainValues }) {
  const typeValue = mainValues.type;
  const type = Array.isArray(typeValue) && typeValue.length && +typeValue[0];

  return type;
}

export function getTaskStateOfValues({ values }) {
  return (
    (values.taskState &&
      Array.isArray(values.taskState) &&
      values.taskState.length &&
      +values.taskState[0]) ||
    +values.taskState
  );
}

export function getStateOfValues({ values }) {
  return (
    (values.state &&
      Array.isArray(values.state) &&
      values.state.length &&
      +values.state[0]) ||
    +values.state
  );
}
