import { renderFormFields, disableFieldHandler } from "../../helpers";
import { maxDateRange, minDateRange } from "../../../helpers/validators";
import { maskOnlyNumbers, maskOnlyText } from "../../../helpers/masks";
import { searchRequestPropsForWarehouses } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

export const fields = renderFormFields([
  [
    {
      destructuring: false,
      isRange: false,
      id: null,
      name: "Поиск по дате создания"
    },
    [
      {
        id: "datefrom_created",
        formControlProps: {
          validators: [maxDateRange("dateto_created")],
          type: "date",
          attrs: { placeholder: "Выберите дату" }
        }
      },
      {
        id: "dateto_created",
        formControlProps: {
          validators: [minDateRange("datefrom_created")],
          type: "date",
          attrs: { placeholder: "Выберите дату" }
        }
      }
    ]
  ],
  [
    {
      destructuring: false,
      isRange: false,
      id: null,
      name: "Поиск по дате реализации"
    },
    [
      {
        id: "date_from",
        formControlProps: {
          validators: [maxDateRange("dateto_to")],
          type: "date",
          attrs: { placeholder: "Выберите дату" }
        }
      },
      {
        id: "dateto_to",
        formControlProps: {
          validators: [minDateRange("date_from")],
          type: "date",
          attrs: { placeholder: "Выберите дату" }
        }
      }
    ]
  ],
  {
    id: "_order_code",
    title: "Заказ",
    formControlProps: {
      mask: maskOnlyNumbers,
      type: "text",
      attrs: { placeholder: "Введите номер заказа" }
    }
  },
  {
    id: "_agent_code",
    title: "Клиент",
    formControlProps: {
      mask: maskOnlyNumbers,
      type: "text",
      attrs: { placeholder: "Введите номер клиента" }
    }
  },
  {
    id: "ean_13",
    title: "Штрих-код",
    formControlProps: {
      mask: maskOnlyNumbers,
      type: "text",
      attrs: { placeholder: "Введите штрих-код товара" }
    }
  },
  {
    id: "article",
    title: "Артикул",
    formControlProps: {
      mask: maskOnlyText,
      type: "text",
      attrs: {
        placeholder: "Введите артикул",
        iconClearProps: { iconFix: true }
      }
    }
  },
  {
    id: "warehouse_id",
    title: "Склад приемки",
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Введите склад отгрузки" },
      searchRequestProps: searchRequestPropsForWarehouses()
    }
  },
  {
    id: "warehouse_to_id",
    title: "Склад отгрузки",
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Введите склад приёмки" },
      searchRequestProps: searchRequestPropsForWarehouses()
    }
  },
  {
    id: "description",
    title: "Описание",
    formControlProps: {
      type: "text",
      name: "description",
      attrs: { placeholder: "Введите текст для поиска по описанию" }
    }
  },
  {
    id: "type",
    title: "Выберите тип",
    enumerationKey: "taskType",
    formControlProps: {
      selectedAll: "Поиск по всем типам задач",
      isMultiple: true,
      value: [],
      defaultValue: [],
      type: "select",
      attrs: { placeholder: "Выберите тип" }
    }
  },
  {
    id: "state",
    title: "Поиск всем статусам задач",
    enumerationKey: "taskState",
    formControlProps: {
      selectedAll: "Поиск по всем статусам задач",
      isMultiple: true,
      value: [],
      defaultValue: [],
      type: "select",
      attrs: { placeholder: "Выберите статус" }
    }
  },
  {
    id: "_for_user",
    title: "Мной созданные задачи",
    formControlProps: {
      type: "checkbox",
      valuesByChecked: new Map([[false, ""], [true, "1"]]),
      checkedByValue: new Map([["", false], ["1", true]]),
      label: "Мной созданные задачи",
      afterChange: ({ field, values }) => {
        if (
          (values.hasOwnProperty("object_id_create") &&
            values.object_id_create.length) ||
          (values.hasOwnProperty("_agent_code") &&
            values._created_by_user !== "")
        ) {
          return disableFieldHandler(field);
        }

        return field;
      }
    },
    afterMap: ({ field, values }) => {
      if (
        (values.hasOwnProperty("object_id_create") &&
          values.object_id_create.length) ||
        (values.hasOwnProperty("_agent_code") && values._created_by_user !== "")
      ) {
        return disableFieldHandler(field);
      }

      return field;
    }
  },
  {
    id: "_created_for_user",
    title: "Задачи, созданные на меня",
    formControlProps: {
      type: "checkbox",
      valuesByChecked: new Map([[false, ""], [true, "1"]]),
      checkedByValue: new Map([["", false], ["1", true]]),
      label: "Задачи, созданные на меня",
      afterChange: ({ field, values }) => {
        if (
          (values.hasOwnProperty("executor_id") && values.executor_id.length) ||
          (values.hasOwnProperty("_agent_code") &&
            values._created_for_user !== "")
        ) {
          return disableFieldHandler(field);
        }

        return field;
      }
    },
    afterMap: ({ field, values }) => {
      if (
        (values.hasOwnProperty("executor_id") && values.executor_id.length) ||
        (values.hasOwnProperty("_agent_code") &&
          values._created_for_user !== "")
      ) {
        return disableFieldHandler(field);
      }

      return field;
    }
  },
  {
    id: "_executed_by_user",
    title: "Мной выполненные задачи",
    formControlProps: {
      type: "checkbox",
      valuesByChecked: new Map([[false, ""], [true, "1"]]),
      checkedByValue: new Map([["", false], ["1", true]]),
      label: "Мной выполненные задачи",
      afterChange: ({ field, values }) => {
        if (
          (values.hasOwnProperty("object_id_edit") &&
            values.object_id_edit.length) ||
          (values.hasOwnProperty("_agent_code") &&
            values._executed_by_user !== "")
        ) {
          return disableFieldHandler(field);
        }

        return field;
      }
    },
    afterMap: ({ field, values }) => {
      if (
        (values.hasOwnProperty("object_id_edit") &&
          values.object_id_edit.length) ||
        (values.hasOwnProperty("_agent_code") &&
          values._executed_by_user !== "")
      ) {
        return disableFieldHandler(field);
      }

      return field;
    }
  }
]);
