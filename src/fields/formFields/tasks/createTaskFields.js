import { renderFormFields } from "../../helpers";
import ProductFormWrapper from "../../../components/blocks/tasks/commands/ProductFormWrapper";
import {
  typeField,
  dateField,
  stateField,
  executorField,
  warehouseField,
  warehouseIdAccept,
  nameField,
  descriptionField
} from "./helpers/fieldSkeletons";
import { fieldValuesTypeCheck } from "./helpers/isActiveCheckFunctions";

const productField = {
  id: "_product",
  formControlProps: {
    type: "custom",
    Component: ProductFormWrapper
  }
};

export const fields = renderFormFields([
  typeField,
  dateField,
  stateField,
  executorField,
  fieldValuesTypeCheck(warehouseField, [1, 2, 8], true),
  fieldValuesTypeCheck(warehouseIdAccept, [2], true),
  fieldValuesTypeCheck(productField, [2], true),
  nameField,
  descriptionField
]);
