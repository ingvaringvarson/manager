import React from "react";
import { renderFormFields } from "../../helpers";
import { maskOnlyNumbers } from "../../../helpers/masks";
import { FieldContext } from "../../../components/common/formControls";

export const fieldsForProductTable = renderFormFields([
  {
    id: "brand",
    title: "Товар"
  },
  {
    id: "article",
    title: ""
  },
  {
    id: "name",
    title: ""
  },
  {
    id: "order",
    title: "заказ"
  },
  {
    id: "count",
    title: "Количество",
    bodyCellProps: {
      render: ({ entity, field }) => (
        <FieldContext name={`${field.id}__${entity._id}`} />
      )
    }
  },
  {
    id: "count_add",
    title: "Добавить",
    bodyCellProps: {
      render: ({ entity, field }) => (
        <FieldContext name={`${field.id}__${entity._id}`} />
      )
    }
  }
]);

export const fieldsForProductForm = renderFormFields([
  [
    { destructuring: true, id: "products", name: null },
    [
      {
        id: "count__",
        formControlProps: {
          type: "text",
          attrs: { disabled: true }
        }
      },
      {
        id: "count_add__",
        formControlProps: {
          type: "text",
          mask: maskOnlyNumbers
        }
      }
    ]
  ]
]);
