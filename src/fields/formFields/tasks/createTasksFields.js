import React from "react";

import {
  getFirstLevelOption,
  getIdForEntity,
  renderFormFields
} from "../../helpers";
import { getCurrentDateInISO } from "../../../utils";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForOrders
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

import OrderField from "../../../components/blocks/tasks/OrderField";
import ClientField from "../../../components/blocks/tasks/ClientField";

//ToDo: проверить филды с типом select_search_request http://jira.laf24.ru/browse/MAN-1743
export const prepareForIssueFields = renderFormFields([
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "type",
        enumerationKey: "taskType",
        formControlProps: {
          type: "select",
          name: "type",
          label: "Тип задачи",
          attrs: { placeholder: "Выберите тип задачи" },
          value: [],
          defaultValue: [6]
        }
      },
      {
        id: "date",
        formControlProps: {
          type: "date",
          label: "Дата требуемой реализации",
          name: "date",
          attrs: { placeholder: "Выберите дату" },
          value: new Date()
        }
      }
    ]
  ],
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "state",
        formControlProps: {
          type: "text",
          value: "Не выполнено",
          label: "Статус задачи",
          name: "state",
          attrs: { placeholder: "Выберите статус задачи" }
        }
      },
      {
        id: "executor_id",
        formControlProps: {
          type: "select_search_request",
          label: "Исполнитель",
          name: "executor_id",
          attrs: { placeholder: "Введите фамилию исполнителя" },
          value: [],
          defaultValue: [],
          searchRequestProps: searchRequestPropsForAgents({
            params: {
              roles: [5]
            }
          })
        }
      }
    ]
  ],
  {
    id: "warehouse_id_in",
    formControlProps: {
      type: "select_search_request",
      label: "Склад отгрузки",
      name: "warehouse_id_in",
      attrs: { placeholder: "Введите название склада" },
      value: [],
      defaultValue: [],
      searchRequestProps: searchRequestPropsForAgents({
        type: "warehouse",
        params: {
          roles: [5]
        }
      })
    }
  },
  {
    id: "warehouse_id",
    formControlProps: {
      type: "select_search_request",
      label: "Исполнитель",
      name: "warehouse_id",
      attrs: { placeholder: "Введите фамилию исполнителя" },
      value: [],
      defaultValue: [],
      searchRequestProps: searchRequestPropsForAgents({
        type: "warehouse",
        params: {
          roles: [5]
        }
      })
    }
  },
  [
    { destructuring: true, id: "orders", name: null },
    [
      {
        id: "order_code__",
        shouldNotBeInValue: true,
        formControlProps: {
          type: "select_search_request",
          name: "order_code",
          label: "Заказ",
          attrs: { placeholder: "Введите код заказа" },
          value: [],
          defaultValue: []
        },
        afterMap: ({ field }) => {
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              searchRequestProps: searchRequestPropsForOrders(field.id)
            }
          };
        }
      }
    ]
  ],
  {
    id: "name",
    formControlProps: {
      type: "text",
      name: "name",
      label: "Заголовок",
      attrs: { placeholder: "Введите название задачи" }
    }
  },
  {
    id: "description",
    formControlProps: {
      type: "textarea",
      name: "description",
      label: "Описание",
      attrs: { placeholder: "Введите описание задачи" }
    }
  }
]);

export const prepareForMovementFields = renderFormFields([
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "type",
        enumerationKey: "taskType",
        formControlProps: {
          type: "select",
          name: "type",
          label: "Тип задачи",
          attrs: { placeholder: "Выберите тип задачи" },
          value: [],
          defaultValue: [6]
        }
      },
      {
        id: "date",
        formControlProps: {
          type: "date",
          label: "Дата требуемой реализации",
          name: "date",
          attrs: { placeholder: "Выберите дату" },
          value: new Date()
        }
      }
    ]
  ],
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "state",
        formControlProps: {
          type: "text",
          value: "Не выполнено",
          label: "Статус задачи",
          name: "state",
          attrs: { placeholder: "Выберите статус задачи" }
        }
      },
      {
        id: "executor_id",
        formControlProps: {
          type: "select_search_request",
          label: "Исполнитель",
          name: "executor_id",
          attrs: { placeholder: "Введите фамилию исполнителя" },
          value: [],
          defaultValue: [],
          searchRequestProps: searchRequestPropsForAgents({
            params: {
              roles: [5],
              types: [1]
            }
          })
        }
      }
    ]
  ],
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "warehouse_id_in",
        formControlProps: {
          type: "select_search_request",
          label: "Склад отгрузки",
          name: "warehouse_id_in",
          attrs: { placeholder: "Введите название склада" },
          value: [],
          defaultValue: [],
          searchRequestProps: searchRequestPropsForAgents({
            type: "warehouse",
            params: {
              roles: [5]
            }
          })
        }
      },
      {
        id: "warehouse_id_out",
        formControlProps: {
          type: "select_search_request",
          label: "Склад приемки",
          name: "warehouse_id_out",
          attrs: { placeholder: "Введите название склада" },
          value: [],
          defaultValue: [],
          searchRequestProps: searchRequestPropsForAgents({
            type: "warehouse",
            params: {
              roles: [5]
            }
          })
        }
      }
    ]
  ],
  {
    id: "order_code",
    formControlProps: {
      type: "text",
      name: "order_code",
      label: "Заказ",
      attrs: { placeholder: "Введите код заказа" },
      renderAfterField: ({ field, fields, values, ...restProps }) => {
        return (
          <OrderField
            {...restProps}
            field={field}
            fields={fields}
            values={values}
          />
        );
      }
    }
  },
  {
    id: "vendor_code",
    formControlProps: {
      type: "text",
      name: "vendor_code",
      label: "Артикул",
      attrs: { placeholder: "Введите артикул" },
      renderAfterField: ({ field, fields, values, ...restProps }) => {
        return <div>adj</div>;
      }
    }
  },
  {
    id: "name",
    formControlProps: {
      type: "text",
      name: "name",
      label: "Заголовок",
      attrs: { placeholder: "Введите название задачи" }
    }
  },
  {
    id: "description",
    formControlProps: {
      type: "textarea",
      name: "description",
      label: "Описание",
      attrs: { placeholder: "Введите описание задачи" }
    }
  }
]);

export const callToClientFields = renderFormFields([
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "type",
        enumerationKey: "taskType",
        formControlProps: {
          type: "select",
          name: "type",
          label: "Тип задачи",
          attrs: { placeholder: "Выберите тип задачи" },
          value: [],
          defaultValue: [6]
        }
      },
      {
        id: "date",
        formControlProps: {
          type: "date",
          label: "Дата требуемой реализации",
          name: "date",
          attrs: { placeholder: "Выберите дату" },
          value: getCurrentDateInISO()
        }
      }
    ]
  ],
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "state",
        enumerationKey: "taskState",
        formControlProps: {
          type: "text",
          defaultValue: "Не выполнено",
          label: "Статус задачи",
          name: "state",
          attrs: { placeholder: "Выберите статус задачи", disabled: true }
        }
      },
      {
        id: "executor_id",
        formControlProps: {
          type: "select_search_request",
          label: "Исполнитель",
          name: "executor_id",
          attrs: { placeholder: "Введите фамилию исполнителя" },
          value: [],
          defaultValue: [],
          searchRequestProps: searchRequestPropsForAgents({
            params: {
              roles: [5],
              types: [1]
            }
          })
        }
      }
    ]
  ],
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "agent_id",
        formControlProps: {
          type: "select_search_request",
          name: "agent_id",
          label: "Клиент",
          attrs: { placeholder: "Введите код клиента" },
          renderAfterField: ({ field, fields, values, ...restProps }) => {
            return (
              <ClientField
                {...restProps}
                field={field}
                fields={fields}
                values={values}
              />
            );
          },
          searchRequestProps: searchRequestPropsForAgents({
            key: "code",
            getName: getFirstLevelOption("code"),
            getId: getIdForEntity
          })
        }
      },
      {
        id: "order_id",
        formControlProps: {
          type: "select_search_request",
          name: "order_id",
          label: "Заказ",
          attrs: { placeholder: "Введите код заказа" },
          renderAfterField: ({ field, fields, values, ...restProps }) => {
            return (
              <OrderField
                {...restProps}
                field={field}
                fields={fields}
                values={values}
              />
            );
          },
          searchRequestProps: searchRequestPropsForOrders()
        }
      }
    ]
  ],
  {
    id: "name",
    formControlProps: {
      type: "text",
      name: "name",
      label: "Заголовок",
      attrs: { placeholder: "Введите название задачи" }
    }
  },
  {
    id: "description",
    formControlProps: {
      type: "textarea",
      name: "description",
      label: "Описание",
      attrs: { placeholder: "Введите описание задачи" }
    }
  }
]);
