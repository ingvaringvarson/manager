import React from "react";

import ClientField from "../../../components/blocks/tasks/ClientField";

import {
  getFirstLevelOption,
  getIdForEntity,
  renderFormFields
} from "../../helpers";
import {
  nameField,
  descriptionField,
  dateField,
  executorField,
  warehouseField,
  orderField,
  reasonField,
  updateStateField,
  updateTypeField,
  deliveryAddress,
  productWithHasOwnRepresentationField
} from "./helpers/fieldSkeletons";
import {
  fieldValuesTypeCheck,
  fieldValuesChecking
} from "./helpers/isActiveCheckFunctions";
import {
  getStateOfValues,
  getTypeOfMainValues
} from "./helpers/valuesFunctionGetter";
import { searchRequestPropsForAgents } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

//ToDo: проверить филды с типом select_search_request http://jira.laf24.ru/browse/MAN-1743
export const formFields = renderFormFields([
  [
    { destructuring: false, separator: "", id: null },
    [updateTypeField, dateField]
  ],
  [
    { destructuring: false, separator: "", id: null },
    [
      fieldValuesTypeCheck(updateStateField, [5], false, getStateOfValues),
      fieldValuesTypeCheck(executorField, [5], false, getStateOfValues)
    ]
  ],
  [
    { destructuring: false, separator: "", id: null },
    [
      fieldValuesTypeCheck(updateStateField, [5], true, getStateOfValues),
      fieldValuesTypeCheck(reasonField, [5], true, getStateOfValues)
    ]
  ],
  [
    { destructuring: false, separator: "", id: null },
    [
      fieldValuesTypeCheck(executorField, [5], true, getStateOfValues),
      fieldValuesTypeCheck(orderField, [5], true, getStateOfValues)
    ]
  ],
  //fieldValuesTypeCheck(executorField, [5], true, getStateOfValues),
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "agent_id",
        formControlProps: {
          type: "select_search_request",
          label: "Клиент",
          attrs: { placeholder: "Введите код клиента" },
          renderAfterField: ({ field, fields, values, ...restProps }) => {
            return (
              <ClientField
                {...restProps}
                field={field}
                fields={fields}
                values={values}
              />
            );
          },
          searchRequestProps: searchRequestPropsForAgents({
            key: "code",
            keyById: "id",
            getName: getFirstLevelOption("code"),
            getId: getIdForEntity
          })
        },
        afterMap: ({ mainValues, fields, field, values }) => {
          const type =
            (values.type &&
              Array.isArray(values.type) &&
              values.type.length &&
              +values.type[0]) ||
            +values.type;

          const isActive = type !== 8;

          if (type === 1) {
            return {
              ...field,
              ...warehouseField,
              isActive
            };
          }

          return {
            ...field,
            isActive
          };
        }
      },
      fieldValuesTypeCheck(orderField, [8], true, getTypeOfMainValues)
    ]
  ],
  fieldValuesChecking(orderField, [
    {
      values: [5],
      byCallback: true,
      callbackToReturnValue: getStateOfValues,
      shouldInclude: false
    },
    {
      values: [8],
      shouldInclude: true,
      valueName: "type"
    }
  ]),
  fieldValuesTypeCheck(deliveryAddress, [8], true),
  productWithHasOwnRepresentationField,
  nameField,
  descriptionField
]);
