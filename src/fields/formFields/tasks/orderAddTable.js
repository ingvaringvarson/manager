import React from "react";
import { renderFormFields } from "../../helpers";
import FormField from "../../../components/common/formControls/FieldContext";
import {
  valueLessOrEqualThan,
  valueGreaterThan,
  required
} from "../../../helpers/validators";

export const fields = renderFormFields([
  {
    id: "brand",
    title: "Товар",
    headControlProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.brand : "";
      }
    }
  },
  {
    id: "article",
    title: "",
    headControlProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.article : "";
      }
    }
  },
  {
    id: "name",
    title: "",
    headControlProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.name : "";
      }
    }
  },
  {
    id: "count",
    title: "Количество",
    headControlProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return <FormField name={`count__${entity._id}`} />;
      }
    }
  },
  {
    id: "add",
    title: "Добавить",
    headControlProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return <FormField name={`add__${entity._id}`} />;
      }
    }
  }
]);

export const formFields = renderFormFields([
  [
    { destructuring: true, id: "fields", name: null },
    [
      {
        id: "count__",
        formControlProps: {
          type: "text",
          attrs: {
            placeholder: "Количество",
            disabled: true
          }
        }
      },
      {
        id: "add__",
        shouldNotBeInValue: true,
        formControlProps: { type: "text" },
        afterMap: ({ field, fields, values, mainValues }) => {
          const currentPos = mainValues.fields.find(
            val => val._id === field.id.split("__")[1]
          );
          const count = currentPos ? currentPos.count : null;
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              validators: [
                valueLessOrEqualThan(values.count),
                valueGreaterThan(0),
                required
              ],
              value: count
            }
          };
        }
      }
    ]
  ]
]);
