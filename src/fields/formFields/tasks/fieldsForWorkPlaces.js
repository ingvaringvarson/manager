import { renderFormFields } from "../../helpers";
import { required } from "../../../helpers/validators";
import { searchRequestPropsForWarehouses } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

export const fields = renderFormFields([
  {
    id: "phone_extra",
    formControlProps: {
      validators: [required],
      type: "text",
      name: "phone_extra",
      attrs: { placeholder: "Введите добавочный номер телефона" }
    }
  },
  {
    id: "warehouse_id",
    formControlProps: {
      type: "select_search_request",
      validators: [required],
      value: [],
      defaultValue: [],
      name: "warehouse_id",
      attrs: { placeholder: "Выберите магазин" },
      searchRequestProps: searchRequestPropsForWarehouses()
    }
  },
  {
    id: "_root",
    formControlProps: { value: "addWorkPlace" },
    isActive: false
  }
]);
