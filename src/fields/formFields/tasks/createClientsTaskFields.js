import React from "react";

import { renderFormFields } from "../../helpers";
import { searchRequestPropsForAgents } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

import ClientField from "../../../components/blocks/tasks/ClientField";
import OrderField from "../../../components/blocks/tasks/OrderField";

export const fields = renderFormFields([
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "type",
        enumerationKey: "taskType",
        formControlProps: {
          type: "select",
          label: "Тип задачи",
          attrs: { placeholder: "Выберите тип задачи" },
          value: [],
          defaultValue: [6]
        }
      },
      {
        id: "date",
        formControlProps: {
          type: "date",
          label: "Дата требуемой реализации",
          attrs: { placeholder: "Выберите дату" },
          value: new Date()
        }
      }
    ]
  ],
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "state",
        formControlProps: {
          type: "text",
          value: "Не выполнено",
          label: "Статус задачи",
          attrs: { placeholder: "Выберите статус задачи" }
        }
      },
      {
        id: "executor_id",
        formControlProps: {
          type: "select_search_request",
          label: "Исполнитель",
          attrs: { placeholder: "Введите фамилию исполнителя" },
          value: [],
          defaultValue: [],
          searchRequestProps: searchRequestPropsForAgents({
            params: {
              roles: [5]
            }
          })
        }
      }
    ]
  ],
  [
    { destructuring: false, separator: "", id: null },
    [
      {
        id: "agent_code",
        formControlProps: {
          type: "text",
          label: "Клиент",
          attrs: { placeholder: "Введите код клиента" },
          renderAfterField: ({ field, fields, values, ...restProps }) => {
            return (
              <ClientField
                {...restProps}
                field={field}
                fields={fields}
                values={values}
              />
            );
          }
        }
      },
      {
        id: "order_code",
        formControlProps: {
          type: "text",
          label: "Заказ",
          attrs: { placeholder: "Введите код заказа" },
          renderAfterField: ({ field, fields, values, ...restProps }) => {
            return (
              <OrderField
                {...restProps}
                field={field}
                fields={fields}
                values={values}
              />
            );
          }
        }
      }
    ]
  ],
  {
    id: "name",
    formControlProps: {
      type: "text",
      label: "Заголовок",
      attrs: { placeholder: "Введите название задачи" }
    }
  },
  {
    id: "description",
    formControlProps: {
      type: "textarea",
      label: "Описание",
      attrs: { placeholder: "Введите описание задачи" }
    }
  }
]);
