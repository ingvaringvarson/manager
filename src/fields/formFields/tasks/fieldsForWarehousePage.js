import React from "react";

import { maxDateRange, minDateRange } from "../../../helpers/validators";
import {
  maskDateBeginningDay,
  maskDateEndDay,
  maskOnlyNumbers,
  maskOnlyText
} from "../../../helpers/masks";
import { renderEnumerationValue, renderFormFields } from "../../helpers";
import { getValueInDepth } from "../../../utils";
import { PreSpaceText } from "../../../components/common/styled/text";
import { searchRequestPropsForWarehouses } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

export const groupTaskType = {
  "1_4": {
    name: "Выдать клиенту",
    id: "1_4"
  },
  "8_9": {
    name: "Для доставки клиенту",
    id: "8_9"
  },
  "2_3": {
    name: "Для перемещения",
    id: "2_3"
  },
  "16_17": {
    name: "Возврат поставщику",
    id: "16_17"
  },
  "3_5": {
    name: "Принять перемещение",
    id: "3_5"
  },
  "11": {
    name: "Принять от поставщика",
    id: "11"
  },
  "18": {
    name: "Принять возврат от клиента",
    id: "18"
  },
  "7": {
    name: "Отнести на склад",
    id: "7"
  }
};

export const taskTypesByFirstGroup = ["1_4", "8_9", "2_3", "16_17"];

export const taskTypesBySecondGroup = ["3_5", "11", "18", "7"];

const mainFields = [
  [
    { id: null, name: "Поиск по дате выполнения", destructuring: false },
    [
      {
        id: "date_from",
        formControlProps: {
          validators: [maxDateRange("date_to")],
          mask: maskDateBeginningDay,
          type: "date",
          attrs: { placeholder: "Выберите дату" }
        }
      },
      {
        id: "date_to",
        formControlProps: {
          validators: [minDateRange("date_from")],
          mask: maskDateEndDay,
          type: "date",
          attrs: { placeholder: "Выберите дату" }
        }
      }
    ]
  ],
  {
    id: "director_id",
    formControlProps: {
      type: "select",
      attrs: { placeholder: "Выберите поставщика", disabled: true },
      label: "Поставщик"
    }
  },
  {
    id: "warehouse_id",
    title: "Склад отгрузки",
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Выберите склад отгрузки" },
      searchRequestProps: searchRequestPropsForWarehouses(),
      label: "Склад отгрузки"
    }
  },
  {
    id: "warehouse_to_id",
    title: "Склад приемки",
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Выберите склад приемки" },
      searchRequestProps: searchRequestPropsForWarehouses(),
      label: "Склад приемки"
    }
  },
  {
    id: "article",
    title: "Артикул",
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Введите артикул",
        iconClearProps: { iconFix: true }
      },
      label: "Артикул"
    }
  },
  {
    id: "code",
    title: "Введите номер задачи",
    mask: maskOnlyText,
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Введите номер задачи",
        iconClearProps: { iconFix: true }
      },
      label: "Номер задачи"
    }
  },
  {
    id: "_completed_tasks",
    title: "Отображать выполненные задачи",
    formControlProps: {
      type: "checkbox",
      valuesByChecked: new Map([[false, ""], [true, "1"]]),
      checkedByValue: new Map([["", false], ["1", true]]),
      label: "Отображать выполненные задачи"
    }
  },
  {
    id: "_canceled_tasks",
    title: "Отображать отмененные задачи",
    formControlProps: {
      type: "checkbox",
      valuesByChecked: new Map([[false, ""], [true, "1"]]),
      checkedByValue: new Map([["", false], ["1", true]]),
      label: "Отображать отмененные задачи"
    }
  }
];

export const groupFieldsForFilter = {
  "1": renderFormFields(
    [
      {
        id: "_type_group",
        title: "Типы задач",
        formControlProps: {
          type: "select",
          attrs: {
            placeholder: "Типы задач",
            iconClearProps: {
              hide: true
            }
          },
          options: taskTypesByFirstGroup.map(id => groupTaskType[id])
        }
      },
      ...mainFields
    ],
    true
  ),
  "2": renderFormFields(
    [
      {
        id: "_type_group",
        title: "Типы задач",
        formControlProps: {
          type: "select",
          attrs: {
            placeholder: "Типы задач"
          },
          options: taskTypesBySecondGroup.map(id => groupTaskType[id])
        }
      },
      ...mainFields
    ],
    true
  )
};

const fieldsForAgentAndOrder = [
  [
    { id: null, name: "", destructuring: false, separator: "" },
    [
      {
        id: "agent_code",
        title: "Поиск по клиенту",
        mask: maskOnlyNumbers,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "ID клиента" },
          label: "Поиск по клиенту"
        }
      },
      {
        id: "order_code",
        title: "Поиск по заказу",
        mask: maskOnlyText,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "ID заказа" },
          label: "Поиск по заказу"
        }
      }
    ]
  ]
];

const fieldsForWarehouseTO = [
  {
    id: "warehouse_to_id",
    title: "Поиск склада для перемещения",
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Введите название склада" },
      searchRequestProps: searchRequestPropsForWarehouses(),
      label: "Поиск склада перемещения"
    }
  }
];

const fieldsForWarehouse = [
  {
    id: "warehouse_id",
    title: "Поиск склада отгрузки",
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Введите название склада" },
      searchRequestProps: searchRequestPropsForWarehouses(),
      label: "Поиск склада отгрузки"
    }
  }
];

const fieldsForDirectorAndPurchaseOrder = [
  [
    { id: null, name: "", destructuring: false, separator: "" },
    [
      {
        id: "director_id",
        formControlProps: {
          type: "select",
          attrs: { placeholder: "Выберите поставщика", disabled: true },
          label: "Поиск поставщика"
        }
      },
      {
        id: "purchase_order_code",
        title: "Поиск по возврату",
        mask: maskOnlyNumbers,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "ID заказа поставщика" },
          label: "Поиск по возврату"
        }
      }
    ]
  ]
];

const fieldsForDirectorAndPurchaseReturnOrder = [
  [
    { id: null, name: "", destructuring: false, separator: "" },
    [
      {
        id: "director_id",
        formControlProps: {
          type: "select",
          attrs: { placeholder: "Выберите поставщика", disabled: true },
          label: "Поиск поставщика"
        }
      },
      {
        id: "purchase_return_code",
        title: "Поиск по возврату",
        mask: maskOnlyNumbers,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "ID возврата" },
          label: "Поиск по возврату"
        }
      }
    ]
  ]
];

const fieldsForAgentAndReturn = [
  [
    { id: null, name: "", destructuring: false, separator: "" },
    [
      {
        id: "agent_code",
        title: "Поиск по клиенту",
        mask: maskOnlyNumbers,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "ID клиента" },
          label: "Поиск по клиенту"
        }
      },
      {
        id: "return_code",
        title: "Поиск по возврату",
        mask: maskOnlyNumbers,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "ID возврата" },
          label: "Поиск по возврату"
        }
      }
    ]
  ]
];

const fieldsForArticle = [
  {
    id: "article",
    title: "Поиск товара",
    mask: maskOnlyNumbers,
    formControlProps: {
      type: "text",
      attrs: { placeholder: "Введите артикул" },
      label: "Поиск товара"
    }
  }
];

export const groupFieldsForFilterByTaskType = {
  "1_4": renderFormFields(fieldsForAgentAndOrder, true),
  "8_9": renderFormFields(fieldsForAgentAndOrder, true),
  "2_3": renderFormFields(fieldsForWarehouseTO, true),
  "16_17": renderFormFields(fieldsForDirectorAndPurchaseReturnOrder, true),
  "3_5": renderFormFields(fieldsForWarehouse, true),
  "11": renderFormFields(fieldsForDirectorAndPurchaseOrder, true),
  "18": renderFormFields(fieldsForAgentAndReturn, true),
  "7": renderFormFields(fieldsForArticle, true)
};

const brandField = {
  id: "brand",
  title: "Товар",
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.product ? entity.product.brand : null;
    }
  }
};

const articleField = {
  id: "article",
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.product ? entity.product.article : null;
    }
  }
};

const nameField = {
  id: "name",
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.product ? entity.product.name : null;
    }
  }
};

const countField = {
  id: "count",
  title: "Кол-во",
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.tasks ? entity.tasks.length : null;
    }
  }
};

const orderCodeField = {
  id: "order_code",
  title: "Заказ",
  bodyCellProps: {
    render: ({ entity }) => {
      const codes = entity.tasks
        ? entity.tasks
            .reduce((arr, t) => {
              if (!arr.includes(t.order_code)) {
                arr.push(t.order_code);
              }

              return arr;
            }, [])
            .join(`\n`)
        : null;

      return codes && <PreSpaceText>{codes}</PreSpaceText>;
    }
  }
};

const purchaseOrderCodeField = {
  id: "purchase_order_code",
  title: "Заказ"
};

const purchaseReturnCodeField = {
  id: "purchase_return_code",
  title: "Возврат"
};

const stateField = {
  id: "state",
  title: "Статус позиции",
  bodyCellProps: {
    render: renderEnumerationValue("taskState", "state")
  },
  enumerationKey: "taskState"
};

const firstFieldsForProduct = [brandField, articleField, nameField, countField];

export const fieldsForProduct = renderFormFields([
  ...firstFieldsForProduct,
  orderCodeField,
  stateField
]);

export const fieldsForProductWithPurchaseOrder = renderFormFields([
  ...firstFieldsForProduct,
  purchaseOrderCodeField,
  stateField
]);

export const fieldsForProductWithPurchaseReturn = renderFormFields([
  ...firstFieldsForProduct,
  purchaseReturnCodeField,
  stateField
]);

export const getFieldsForProduct = selectedGroup => {
  switch (selectedGroup) {
    case "11":
      return fieldsForProductWithPurchaseOrder;
    case "16_17":
      return fieldsForProductWithPurchaseReturn;
    default:
      return fieldsForProduct;
  }
};

export const fieldsForTasksByProduct = renderFormFields([
  {
    id: "index",
    title: "№",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.index ? entity.index : null;
      }
    }
  },
  {
    id: "state",
    title: "Статус товара",
    bodyCellProps: {
      render: renderEnumerationValue("productUnitState", ["unit", "state"])
    },
    enumerationKey: "productUnitState"
  },
  {
    id: "ean_13",
    title: "Штрих-код",
    bodyCellProps: {
      render: ({ entity }) => {
        if (entity.task.product && entity.task.product.type !== 1) {
          return "Учет не по шк";
        }

        const unitBarCode = getValueInDepth(entity, ["unit", "ean_13"]);
        const taskBarCode = getValueInDepth(entity, ["task", "ean_13"]);

        return taskBarCode || unitBarCode;
      }
    }
  }
]);
