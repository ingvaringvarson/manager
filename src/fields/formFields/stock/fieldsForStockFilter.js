import { maskOnlyNumbers } from "../../../helpers/masks";
import {
  renderFormFields,
  renderEnumerationValue,
  getNameCompanyShort,
  getIdForEntity
} from "../../helpers";
import { moduleName } from "./../../../ducks/pricelists";
import {
  searchRequestPropsForWarehouses,
  searchRequestPropsForPriceList,
  searchRequestPropsForBrandVendorCode,
  searchRequestPropsForAgents
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

export default renderFormFields(
  [
    {
      id: "firm_id",
      title: "Организация",
      bodyCellProps: {
        render: ({ entity }) =>
          entity && entity.firm_name ? entity.firm_name : ""
      },
      formControlProps: {
        value: [],
        defaultValue: [],
        type: "select_search_request",
        attrs: { placeholder: "Введите название организации" },
        searchRequestProps: searchRequestPropsForAgents({
          keyById: "id",
          getName: getNameCompanyShort,
          getId: getIdForEntity,
          params: {
            roles: [2]
          }
        }),
        label: "Организация"
      }
    },
    {
      id: "filter_warehouse",
      queryKeys: ["warehouse_id"],
      title: "Склад",
      formControlProps: {
        value: [],
        defaultValue: [],
        type: "select_search_request",
        attrs: { placeholder: "Выберите склад" },
        searchRequestProps: searchRequestPropsForWarehouses(),
        label: "Склад"
      }
    },
    {
      id: "pricelist_id_code",
      queryKeys: ["pricelist_id"],
      title: "Прайс-лист",
      formControlProps: {
        value: [],
        defaultValue: [],
        type: "select_search_request",
        attrs: { placeholder: "Введите код прайс-листа" },
        searchRequestProps: searchRequestPropsForPriceList({
          type: moduleName
        }),
        label: "Прайс-лист"
      }
    },
    {
      id: "ean_13",
      queryKeys: ["ean_13"],
      title: "Штрих-код",
      formControlProps: {
        mask: maskOnlyNumbers,
        type: "text",
        attrs: {
          placeholder: "Введите штрих-код",
          iconClearProps: { iconFix: true }
        },
        label: "Штрих-код"
      }
    },
    {
      id: "article",
      title: "Артикул",
      queryKeys: ["product"],
      formControlProps: {
        value: [],
        defaultValue: [],
        type: "select_search_request",
        attrs: { placeholder: "Введите артикул" },
        searchRequestProps: searchRequestPropsForBrandVendorCode(),
        label: "Артикул"
      }
    },
    {
      id: "state",
      title: "Выберите статус товара",
      enumerationKey: "productUnitState",
      formControlProps: {
        selectedAll: "Поиск по всем статусам товара",
        isMultiple: true,
        value: [],
        defaultValue: [],
        type: "select",
        attrs: { placeholder: "Выберите статус товара" }
      },
      bodyCellProps: {
        render: renderEnumerationValue("paymentState", "payment_state")
      }
    }
  ],
  /* forFilters */ true
);
