import React from "react";

import SortButtonContext from "../../../components/common/formControls/SortButtonContext";

import { renderFormFields, renderEnumerationValue } from "../../helpers";
import {
  searchRequestPropsForWarehouses,
  searchRequestPropsForBrandVendorCode
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";
import { getValueInDepth } from "../../../utils";
import { sortByField } from "../../templates/serviceTemplates";

const getSafeProductFieldFunc = field => ({ entity }) =>
  getValueInDepth(entity, ["product", field]);

const getSafeFirstProductInStockFieldFunc = field => ({ entity }) =>
  entity.product_in_stock && entity.product_in_stock[0]
    ? entity.product_in_stock[0][field]
    : null;

const disabledTextProps = {
  formControlProps: {
    type: "text",
    attrs: {
      disabled: true
    }
  }
};

export default renderFormFields([
  {
    id: "brand",
    title: "Бренд",
    bodyCellProps: { render: getSafeProductFieldFunc("brand") },
    ...disabledTextProps
  },
  {
    id: "article_id",
    title: "Артикул",
    queryKeys: ["product"],
    bodyCellProps: { render: getSafeProductFieldFunc("article") },
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Поиск..." },
      searchRequestProps: searchRequestPropsForBrandVendorCode()
    }
  },
  {
    id: "name",
    title: "Наименование",
    bodyCellProps: { render: getSafeProductFieldFunc("name") },
    ...disabledTextProps
  },
  {
    id: "type",
    title: "Тип товара",
    headCellProps: {
      sortBy: true,
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    },
    bodyCellProps: {
      render: renderEnumerationValue("productType", ["product", "type"])
    },
    ...disabledTextProps
  },
  {
    id: "warehouse_id",
    queryKeys: ["warehouse_id"],
    title: "Склад",
    headCellProps: {
      sortBy: true,
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    },
    bodyCellProps: {
      render: getSafeFirstProductInStockFieldFunc("name")
    },
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Поиск..." },
      searchRequestProps: searchRequestPropsForWarehouses()
    }
  },
  {
    id: "total_count",
    title: "Количество итого",
    bodyCellProps: {
      render: getSafeFirstProductInStockFieldFunc("total_count")
    },
    ...disabledTextProps
  },
  {
    id: "count",
    title: "Доступно к заказу",
    bodyCellProps: {
      render: getSafeFirstProductInStockFieldFunc("count")
    },
    ...disabledTextProps
  },
  sortByField
]);
