import { renderFormFields } from "../../helpers";

export default renderFormFields([
  {
    id: "ean_13",
    title: "Штрих-код"
  },
  {
    id: "state",
    title: "Статус",
    enumerationKey: "productUnitState"
  },
  {
    id: "income_price",
    title: "Цена приемки"
  },
  {
    id: "GTD",
    title: "ГТД"
  },
  {
    id: "Country",
    title: "Страна"
  },
  {
    id: "id",
    title: "Идентификатор"
  }
]);
