import fieldsForStockTable from "./fieldsForStockTable";
import fieldsForStockSubTable from "./fieldsForStockSubTable";
import fieldsForStockFilter from "./fieldsForStockFilter";

export { fieldsForStockTable, fieldsForStockSubTable, fieldsForStockFilter };
