import { renderFormFields } from "../../helpers/index";
import {
  brandField,
  articleField,
  nameField,
  typeField,
  sumBillPositionField
} from "../../templates/tableCellTemplates/positionFields";

export const billPositionFields = renderFormFields([
  brandField,
  articleField,
  nameField,
  typeField,
  sumBillPositionField
]);
