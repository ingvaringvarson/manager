import {
  renderFormFields,
  renderEnumerationValue,
  renderDateTime
} from "../../helpers";
import {
  agentCodeField,
  codeField,
  contactsValueField,
  objectIdCreateField,
  objectDateCreate
} from "../../templates/tableCellTemplates/tableCellSkeletons";
import { getValueInDepth } from "../../../utils";
import { searchRequestPropsForWarehouses } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

const clientFullNameField = {
  id: "clientFullName",
  title: "ФИО",
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.agent_tech
        ? entity.agent_tech.person_name || entity.agent_tech.company_name_full
        : "";
    }
  },
  formControlProps: {
    type: "text",
    attrs: { placeholder: "Введите ФИО" }
  }
};

const contactsField = {
  ...contactsValueField,
  id: "contacts",
  queryKeys: ["_contacts_7"],
  bodyCellProps: {
    render: ({ entity }) => {
      if (
        !(
          entity.agent_tech &&
          entity.agent_tech.contacts &&
          Array.isArray(entity.agent_tech.contacts) &&
          entity.agent_tech.contacts.length
        )
      ) {
        return "";
      }

      const contact = entity.agent_tech.contacts.find(cont => {
        return +cont.type === 7 && cont.is_active;
      });

      return contact ? contact.value : "";
    }
  }
};

const stateField = {
  id: "state",
  title: "Статус возврата",
  enumerationKey: "returnState",
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select",
    attrs: { placeholder: "Поиск по статусу" }
  },
  bodyCellProps: {
    render: renderEnumerationValue("returnState", ["return", "state_return"])
  }
};

const warehouseNameField = {
  id: "warehouse_names",
  queryKeys: ["warehouse_id"],
  title: "Склад приемки",
  bodyCellProps: {
    render: ({ entity }) => {
      switch (true) {
        case !!entity.return.pickup:
          return entity.return.pickup.warehouse_name;
        case !!entity.return.deliverylaf24:
          return entity.return.deliverylaf24.warehouse_name;
        case !!entity.return.deliveryDC:
          return entity.return.deliveryDC.warehouse_name;
        default:
          return null;
      }
    }
  },
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Поиск по складу" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};

export const fields = renderFormFields([
  {
    ...agentCodeField,
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.agent_tech ? entity.agent_tech.code : "";
      }
    }
  },
  clientFullNameField,
  contactsField,
  {
    ...codeField,
    bodyCellProps: {
      render: ({ entity, field }) => {
        return entity.return ? entity.return[field.id] : "";
      }
    }
  },
  stateField,
  warehouseNameField,
  {
    ...objectDateCreate,
    bodyCellProps: {
      render: renderDateTime(["return", "object_date_create"])
    }
  },
  {
    ...objectIdCreateField,
    bodyCellProps: {
      render: ({ entity }) =>
        getValueInDepth(entity, ["return", "object_name_create"])
    }
  }
]);
