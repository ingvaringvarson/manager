import { renderFormFields } from "../../helpers/index";
import {
  brandField,
  articleField,
  nameField,
  statusField,
  priceField,
  countField,
  sumField,
  purposeField,
  reasonField
} from "../../templates/tableCellTemplates/positionFields";
import { getValueInDepth } from "../../../utils";

export const fields = renderFormFields([
  {
    ...brandField,
    bodyCellProps: {
      render: ({ entity }) =>
        getValueInDepth(entity, ["position", "product", "brand"])
    }
  },
  {
    ...articleField,
    bodyCellProps: {
      render: ({ entity }) =>
        getValueInDepth(entity, ["position", "product", "article"])
    }
  },
  {
    ...nameField,
    bodyCellProps: {
      render: ({ entity }) =>
        getValueInDepth(entity, ["position", "product", "name"])
    }
  },
  statusField,
  priceField,
  countField,
  sumField,
  purposeField,
  reasonField
]);
