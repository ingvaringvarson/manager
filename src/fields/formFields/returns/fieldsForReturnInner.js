import {
  renderFormFields,
  renderEnumerationValue,
  renderSum
} from "../../helpers";
import {
  statusField,
  priceField,
  countField,
  sumField,
  purposeField,
  reasonField
} from "../../templates/tableCellTemplates/positionFields";
import { getValueInDepth } from "../../../utils";

const brandField = {
  id: "brand",
  title: "Товар",
  bodyCellProps: {
    render: ({ entity }) =>
      getValueInDepth(entity, ["position", "product", "brand"])
  }
};

const articleField = {
  id: "article",
  bodyCellProps: {
    render: ({ entity }) =>
      getValueInDepth(entity, ["position", "product", "article"])
  }
};

const nameField = {
  id: "name",
  bodyCellProps: {
    render: ({ entity }) =>
      getValueInDepth(entity, ["position", "product", "name"])
  }
};

export const fields = renderFormFields([
  brandField,
  articleField,
  nameField,
  {
    ...statusField,
    bodyCellProps: {
      render: renderEnumerationValue("orderPositionGoodState", [
        "position",
        "good_state"
      ])
    }
  },
  countField,
  {
    ...priceField,
    bodyCellProps: {
      render: renderSum(["position", "price"])
    }
  },
  {
    ...sumField,
    bodyCellProps: {
      render: renderSum(["position", "sum"])
    }
  },
  {
    ...reasonField,
    bodyCellProps: {
      render: renderEnumerationValue("returnReason", "reason")
    }
  },
  {
    ...purposeField,
    bodyCellProps: {
      render: renderEnumerationValue("returnPurpose", "purpose")
    }
  }
]);
