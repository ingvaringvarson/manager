import { maxDateRange, minDateRange } from "../../../helpers/validators";
import { maskOnlyText } from "../../../helpers/masks";
import {
  getIdForEntity,
  getNameCompanyShort,
  renderFormFields
} from "../../helpers";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForWarehouses
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

const dateCreateField = [
  { id: null, name: "Поиск по дате создания", destructuring: false },
  [
    {
      id: "datefrom_created",
      formControlProps: {
        validators: [maxDateRange("dateto_created")],
        type: "date",
        attrs: { placeholder: "Выберите дату" }
      }
    },
    {
      id: "dateto_created",
      formControlProps: {
        validators: [minDateRange("datefrom_created")],
        type: "date",
        attrs: { placeholder: "Выберите дату" }
      }
    }
  ]
];

const firmIdField = {
  id: "firm_id",
  formControlProps: {
    label: "Организация",
    value: [],
    defaultValue: [],
    type: "select_search_request",
    name: "firm_id",
    attrs: { placeholder: "Введите название организации" },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      getName: getNameCompanyShort,
      getId: getIdForEntity,
      params: {
        roles: [2]
      }
    })
  }
};

const warehouseIdField = {
  id: "warehouse_id",
  formControlProps: {
    label: "Склад приемки",
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Выберите склад" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};

const articleField = {
  id: "article",
  formControlProps: {
    mask: maskOnlyText,
    type: "text",
    label: "Введите артикул",
    name: "article",
    attrs: { placeholder: "Введите артикул" }
  }
};

const returnClientCodeField = {
  id: "_agent_code",
  formControlProps: {
    type: "text",
    label: "Создатель возврата",
    attrs: { placeholder: "Введите код создателя возврата" }
  }
};

const forUserField = {
  id: "_for_user",
  title: "Мной созданные возвраты",
  formControlProps: {
    type: "checkbox",
    valuesByChecked: new Map([[false, ""], [true, "1"]]),
    checkedByValue: new Map([["", false], ["1", true]]),
    label: "Мной созданные возвраты"
  }
};

const stateField = {
  id: "state_list",
  title: "Статус возврата",
  queryKeys: ["state"],
  enumerationKey: "returnState",
  formControlProps: {
    type: "select",
    label: "Статус возврата",
    attrs: { placeholder: "Выберите статусы возрата" },
    isMultiple: true,
    value: [],
    defaultValue: []
  }
};

const goodStateField = {
  id: "good_state",
  title: "Статус позиции",
  enumerationKey: "orderPositionGoodState",
  formControlProps: {
    type: "select",
    label: "Статус позиции",
    attrs: { placeholder: "Выберите статусы позиции" },
    isMultiple: true,
    value: [],
    defaultValue: []
  }
};

const reasonField = {
  id: "reason",
  title: "Причина возврата",
  enumerationKey: "returnReason",
  formControlProps: {
    type: "select",
    label: "Причина возврата",
    attrs: { placeholder: "Выберите причины возвратов" },
    isMultiple: true,
    value: [],
    defaultValue: []
  }
};

const purposeField = {
  id: "purpose",
  title: "Назначение возврата",
  enumerationKey: "returnPurpose",
  formControlProps: {
    type: "select",
    label: "Назначение возврата",
    attrs: { placeholder: "Выберите назначения возвратов" },
    isMultiple: true,
    value: [],
    defaultValue: []
  }
};

export const fields = renderFormFields([
  dateCreateField,
  firmIdField,
  warehouseIdField,
  articleField,
  returnClientCodeField,
  forUserField,
  stateField,
  goodStateField,
  reasonField,
  purposeField
]);
