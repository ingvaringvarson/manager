import React from "react";
import { renderFormFields, renderDateTime } from "../../helpers/index";
import SortButtonContext from "../../../components/common/formControls/SortButtonContext";

export const fields = renderFormFields([
  {
    id: "code",
    title: "Номер",
    headCellProps: {
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    }
  },
  {
    id: "type",
    enumerationKey: "taskType",
    title: "Тип"
  },
  {
    id: "state",
    enumerationKey: "taskState",
    title: "Статус"
  },
  {
    id: "executor_name",
    title: "Исполнитель"
  },
  {
    id: "director_name",
    title: "Постановщик"
  },
  {
    id: "warehouse_name",
    title: "Склад отгрузки"
  },
  {
    id: "warehouse_to_name",
    title: "Склад приемки"
  },
  {
    id: "object_date_create",
    title: "Дата создания",
    headCellProps: {
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    },
    bodyCellProps: {
      render: renderDateTime("object_date_create")
    }
  },
  {
    id: "date",
    title: "Дата требуемой реализации",
    headCellProps: {
      render: ({ field }) => {
        return <SortButtonContext name={field.id} title={field.title} />;
      }
    },
    bodyCellProps: {
      render: renderDateTime("date")
    }
  },
  {
    id: "reason",
    title: "Причина",
    enumerationKey: "taskReason"
  }
]);
