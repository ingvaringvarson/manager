import React from "react";
import { renderInfoFields, mapValuesToFields } from "../../helpers";
import ClientInfoBlockTitle from "../../../components/blocks/returns/ClientInfoBlockTitle";
import { contacts, contact_persons } from "../../templates/infoBlocks/contacts";

const EntityRecord = {
  title: null,
  subtitle: "",
  formMain: null,
  formField: null,
  view: "default",
  infoFields: [],
  formFields: [],
  fieldsForAdd: [],
  mapValuesToFields,
  devidingFields: []
};

export const fieldGroups = {
  receivingMethod: {
    ...EntityRecord,
    title: "Способ получения",
    subtitle: "",
    infoFields: renderInfoFields([
      {
        key: "pickup__warehouse_name",
        id: "pickup__warehouse_name",
        validDeliveryTypes: [1],
        title: "Торговая точка",
        mapFieldInMapping: ({ field, values }) => {
          if (!(values.return && values.return.delivery_type)) return field;

          const { delivery_type } = values.return;
          const isValid = field.validDeliveryTypes.some(
            type => type === delivery_type
          );
          const warehouse_name =
            values.return && values.return.pickup
              ? values.return.pickup.warehouse_name
              : null;
          const value = isValid && warehouse_name ? warehouse_name : null;

          return {
            ...field,
            value
          };
        }
      },
      {
        key: "delivery__warehouse_name",
        id: "delivery__warehouse_name",
        validDeliveryTypes: [2, 3],
        title: "Склад сборки",
        mapFieldInMapping: ({ field, values }) => {
          if (!(values.return && values.return.delivery_type)) return field;

          const { delivery_type } = values.return;
          const isValid = field.validDeliveryTypes.some(
            type => type === delivery_type
          );
          let warehouse_name = null;
          if (delivery_type === 2) {
            warehouse_name =
              values.return && values.return.deliverylaf24
                ? values.return.deliverylaf24.warehouse_name
                : null;
          }
          if (delivery_type === 3) {
            warehouse_name =
              values.return && values.return.deliveryDC
                ? values.return.deliveryDC.warehouse_name
                : null;
          }
          const value = isValid && warehouse_name ? warehouse_name : null;

          return {
            ...field,
            value
          };
        }
      },
      {
        key: "location",
        id: "location",
        title: "Адрес доставки заказа",
        validDeliveryTypes: [2, 3],
        mapFieldInMapping: ({ field, values }) => {
          if (!(values.return && values.return.delivery_type)) return field;

          const { delivery_type } = values.return;
          const isValid = field.validDeliveryTypes.some(
            type => type === delivery_type
          );

          let location = null;
          if (delivery_type === 2) {
            location =
              values.return && values.return.deliverylaf24
                ? values.return.deliverylaf24.location
                : null;
          }
          if (delivery_type === 3) {
            location =
              values.return && values.return.deliveryDC
                ? values.return.deliveryDC.location
                : null;
          }

          const value = isValid && location ? location : null;

          return {
            ...field,
            value
          };
        }
      },
      {
        key: "delivery_company_name",
        id: "delivery_company_name",
        title: "Транспортная компания",
        validDeliveryTypes: [3],
        mapFieldInMapping: ({ field, values }) => {
          if (!(values.return && values.return.delivery_type)) return field;

          const { delivery_type } = values.return;
          const isValid = field.validDeliveryTypes.some(
            type => type === delivery_type
          );

          const delivery_company_name =
            values.return && values.return.deliveryDC
              ? values.return.deliveryDC.delivery_company_name
              : null;

          const value =
            isValid && delivery_company_name ? delivery_company_name : null;

          return {
            ...field,
            value
          };
        }
      }
    ])
  },
  client: {
    ...EntityRecord,
    title: "Клиент",
    renderTitle: ({ field, values, enumerations }) => {
      return (
        <ClientInfoBlockTitle
          field={field}
          values={values}
          enumerations={enumerations}
        />
      );
    },
    infoFields: renderInfoFields([
      {
        id: "type",
        key: "type",
        title: "Тип клиента",
        mapFieldInMapping: ({ field, values }) => {
          const { client } = values;
          if (client && client.type === 1) {
            return {
              ...field,
              value: "Физическое лицо"
            };
          }
          return field;
        }
      },
      {
        id: "person_surname",
        key: "person_surname",
        title: "Фамилия",
        mapFieldInMapping: ({ field, values }) => {
          if (values.client && values.client.type === 1) {
            return {
              ...field,
              value: values.client.person
                ? values.client.person.person_surname
                : ""
            };
          }
          return field;
        }
      },
      {
        id: "person_name",
        key: "person_name",
        title: "Имя",
        mapFieldInMapping: ({ field, values }) => {
          if (values.client && values.client.type === 1) {
            return {
              ...field,
              value: values.client.person
                ? values.client.person.person_name
                : ""
            };
          }
          return field;
        }
      },
      {
        id: "person_second_name",
        key: "person_second_name",
        title: "Отчество",
        mapFieldInMapping: ({ field, values }) => {
          if (values.client && values.client.type === 1) {
            return {
              ...field,
              value: values.client.person
                ? values.client.person.person_second_name
                : ""
            };
          }
          return field;
        }
      },
      {
        id: "company_name_full",
        key: "company_name_full",
        title: "Полное наименование	",
        mapFieldInMapping: ({ values, field }) => {
          if (values.client && values.client.type === 2) {
            return {
              ...field,
              value: values.client.company
                ? values.client.company.company_name_full
                : ""
            };
          }
          return field;
        }
      },
      {
        id: "company_name_short",
        key: "company_name_short",
        title: "Сокращенное наименование",
        mapFieldInMapping: ({ values, field }) => {
          if (values.client && values.client.type === 2) {
            return {
              ...field,
              value: values.client.company
                ? values.client.company.company_name_short
                : ""
            };
          }
          return field;
        }
      },
      {
        id: "tags",
        key: "tags",
        title: "Теги",
        enumerationKey: "agentTag",
        mapFieldInMapping: ({ field, values, enumerations }) => {
          const enumMap =
            enumerations[field.enumerationKey] &&
            enumerations[field.enumerationKey].map
              ? enumerations[field.enumerationKey].map
              : null;

          return {
            ...field,
            value:
              values.client &&
              values.client.tags &&
              enumMap &&
              Array.isArray(values.client.tags)
                ? values.client.tags.map(tag => {
                    return enumMap[tag] && enumMap[tag].name;
                  })
                : null
          };
        }
      },
      {
        id: "segments",
        key: "segments",
        title: "Сегменты",
        enumerationKey: "agentSegment",
        mapFieldInMapping: ({ field, values, enumerations }) => {
          const enumMap =
            enumerations[field.enumerationKey] &&
            enumerations[field.enumerationKey].map
              ? enumerations[field.enumerationKey].map
              : null;

          return {
            ...field,
            value:
              values.client &&
              values.client.segments &&
              enumMap &&
              Array.isArray(values.client.segments)
                ? values.client.segments.map(tag => {
                    return enumMap[tag] && enumMap[tag].name;
                  })
                : null
          };
        }
      }
    ])
  },
  contacts,
  contact_persons
};

export const infoFields = [
  {
    info: fieldGroups["receivingMethod"],
    renderContent: null
  },
  {
    info: fieldGroups["client"],
    renderContent: null
  },
  {
    info: fieldGroups["contacts"],
    renderContent: null
  },
  {
    info: fieldGroups["contact_persons"],
    renderContent: null
  }
];
