import React from "react";
import { maskOnlyNumbers } from "../../../helpers/masks";
import { getValueInDepth } from "../../../utils";
import Text from "../../../designSystem/atoms/Text/index";
import { renderFormFields } from "../../helpers/index";
import { sortByField } from "../../templates/serviceTemplates";

const codeField = {
  id: "code",
  title: "ID клиента",
  headCellProps: { sortBy: true },
  formControlProps: {
    mask: maskOnlyNumbers,
    type: "text",
    attrs: {
      placeholder: "Поиск по ID клиента"
    }
  }
};

const fullNameField = {
  id: "full_name",
  title: "ФИО",
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      if (!entity.person) return null;

      const personSurname = entity.person.person_surname
        ? `${entity.person.person_surname} `
        : "";
      const personName = entity.person.person_name
        ? `${entity.person.person_name} `
        : "";
      const personSecondName = entity.person.person_second_name
        ? entity.person.person_second_name
        : "";

      return `${personSurname}${personName}${personSecondName}`;
    }
  },
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "Поиск по ФИО"
    }
  }
};

const fullNameField_v2 = {
  id: "full_name",
  title: "Наименование",
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["company", "company_name_full"]);
    }
  },
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "Поиск по наименованию"
    }
  }
};

const phoneField = {
  id: "phone",
  title: "Телефон",
  queryKeys: ["_contacts_7"],
  bodyCellProps: {
    render: ({ entity }) => {
      if (!entity.contacts || !entity.contacts.length) return null;

      return entity.contacts.reduce((str, contact) => {
        if (contact.type === 1 || contact.type === 7) {
          if (str) {
            return `${str},\n${contact.value}`;
          } else {
            return `${contact.value}`;
          }
        }

        return str;
      }, "");
    }
  },
  formControlProps: {
    mask: maskOnlyNumbers,
    type: "text",
    attrs: {
      placeholder: "Поиск по телефону"
    }
  }
};

const emailField = {
  id: "email",
  title: "Email",
  queryKeys: ["_contacts_2"],
  bodyCellProps: {
    render: ({ entity }) => {
      if (!entity.contacts || !entity.contacts.length) return null;

      return entity.contacts.reduce((str, contact) => {
        if (contact.type === 2) {
          if (str) {
            return `${str},\n${contact.value}`;
          } else {
            return `${contact.value}`;
          }
        }

        return str;
      }, "");
    }
  },
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "Поиск по email"
    }
  }
};

const historyIdField = {
  id: "history_id",
  title: "Старый ID",
  formControlProps: {
    masks: maskOnlyNumbers,
    type: "text",
    attrs: {
      placeholder: "Поиск по ID старой системы"
    }
  }
};

const innField = {
  id: "inn",
  title: "ИНН",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["company_requisites", "company_inn"]);
    }
  },
  formControlProps: {
    mask: maskOnlyNumbers,
    type: "text",
    attrs: {
      placeholder: "Поиск по ИНН"
    }
  }
};

const contactPersonField = {
  id: "contact_persons",
  title: "Контактное лицо",
  bodyCellProps: {
    render: ({ entity }) => {
      if (!entity.contact_persons || !entity.contact_persons.length)
        return null;

      return entity.contact_persons.map((contactPerson, index) => {
        const mainContact =
          contactPerson.contacts && contactPerson.contacts.length
            ? contactPerson.contacts.find(
                contact => contact.is_main && contact.is_active
              )
            : null;
        const position = contactPerson.position ? (
          <b>{contactPerson.position} </b>
        ) : null;
        const name = contactPerson.name ? <>{contactPerson.name} </> : null;

        return (
          <Text key={index} mt={index ? "10" : "0"}>
            {position}
            {name}
            {mainContact ? mainContact.value : null}
          </Text>
        );
      });
    }
  },
  formControlProps: {
    type: "text",
    attrs: {
      disabled: true
    }
  }
};

const managerIdField = {
  id: "manager_id",
  title: "С кем работает",
  bodyCellProps: {
    render: ({ entity }) => {
      if (!entity.code && !entity.manager_name) return null;
      const managerCode = entity.code ? `${entity.code} ` : "";
      const managerName = entity.manager_name ? entity.manager_name : "";

      return `${managerCode} ${managerName}`;
    }
  },
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "Поиск по ID менеджера"
    }
  }
};

export const fieldsForTableByType = {
  1: renderFormFields([
    sortByField,
    codeField,
    fullNameField,
    phoneField,
    emailField,
    historyIdField
  ]),
  2: renderFormFields([
    sortByField,
    codeField,
    fullNameField_v2,
    phoneField,
    emailField,
    innField,
    contactPersonField,
    managerIdField,
    historyIdField
  ])
};

const contactTypeField = {
  id: "contactType",
  queryKeys: ["_contactType"],
  title: "Поиск по контактам",
  enumerationKey: "contactType",
  formControlProps: {
    autoSubmitDrop: true,
    type: "select",
    attrs: {
      placeholder: "Выберите тип контакта"
    }
  }
};

const contactValueField = {
  id: "contactValue",
  afterMap: ({ field, fields, values }) => {
    const currentTypeByKey = field.queryKeys[0]
      ? field.queryKeys[0].split("_")[2]
      : null;
    const contactTypeValue = Array.isArray(values["_contactType"])
      ? values["_contactType"][0]
      : values["_contactType"];

    if (contactTypeValue && contactTypeValue !== currentTypeByKey) {
      return {
        ...field,
        queryKeys: [`_contacts_${contactTypeValue}`],
        formControlProps: {
          ...field.formControlProps,
          value: values.hasOwnProperty(`_contacts_${contactTypeValue}`)
            ? values[`_contacts_${contactTypeValue}`]
            : "",
          attrs: {
            ...field.formControlProps.attrs,
            disabled: false
          }
        }
      };
    }

    return field;
  },
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "Укажите контакт",
      disabled: true
    },
    afterChange: ({ field, fields, values }) => {
      const currentTypeByKey = field.queryKeys[0]
        ? field.queryKeys[0].split("_")[2]
        : null;
      const contactTypeValue = Array.isArray(values["contactType"])
        ? values["contactType"][0]
        : values["contactType"];

      if (contactTypeValue && contactTypeValue !== currentTypeByKey) {
        return {
          ...field,
          queryKeys: [`_contacts_${contactTypeValue}`],
          formControlProps: {
            ...field.formControlProps,
            value: "",
            attrs: {
              ...field.formControlProps.attrs,
              disabled: false
            }
          }
        };
      }

      return field;
    }
  }
};

const correspondentAccountField = {
  id: "correspondent_account",
  title: "Поиск по банковским реквизитам",
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "Введите корреспондентский счет"
    }
  }
};

const settlementAccountField = {
  id: "settlement_account",
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "Введите расчетный счет"
    }
  }
};

export const fieldsForFiltersByType = {
  1: renderFormFields([contactTypeField, contactValueField]),
  2: renderFormFields([
    contactTypeField,
    contactValueField,
    correspondentAccountField,
    settlementAccountField
  ])
};
