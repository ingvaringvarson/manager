import { renderFormFields, renderEnumerationValue } from "../../helpers";
import { getDate, getTime, getValueInDepth } from "../../../utils";
import { maskOnlyText } from "../../../helpers/masks";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForWarehouses
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

// TABLE FIELDS --- START
const codeField = {
  id: "code",
  title: "Номер",
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.return.code;
    }
  },
  formControlProps: {
    mask: maskOnlyText,
    type: "text",
    attrs: {
      placeholder: "Введите номер"
    }
  }
};

const stateField = {
  id: "state",
  title: "Статус",
  enumerationKey: "returnState",
  bodyCellProps: {
    render: renderEnumerationValue("returnState", ["return", "state_return"])
  },
  formControlProps: {
    type: "select",
    attrs: { placeholder: "Поиск по статусу" }
  }
};

const warehouseField = {
  id: "warehouse_id",
  title: "Склад приемки",
  headCellProps: {
    sortBy: true,
    sortName: "warehouse_id"
  },
  bodyCellProps: {
    render: ({ entity }) => {
      if (entity.return.pickup) {
        return entity.return.pickup.warehouse_name;
      } else if (entity.return.deliverylaf24) {
        return entity.return.deliverylaf24.warehouse_name;
      } else if (entity.return.deliveryDC) {
        return entity.return.deliveryDC.warehouse_name;
      } else {
        return null;
      }
    }
  },
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Поиск по складу приемки" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};

const sumTotalField = {
  id: "sum_total",
  title: "Сумма",
  bodyCellProps: {
    render: ({ entity }) => {
      const sum = entity.return.sum_total;
      return sum || sum === 0 ? `${sum} руб.` : "";
    }
  },
  formControlProps: {
    type: "text",
    // не реализовано на стороне 1С
    attrs: { disabled: true }
  }
};

const sumPaymentField = {
  id: "sum_payment",
  title: "Сумма к возврату",
  bodyCellProps: {
    render: ({ entity }) => {
      const sum = entity.return.sum_payment;
      return sum || sum === 0 ? `${sum} руб.` : "";
    }
  },
  formControlProps: {
    type: "text",
    // не реализовано на стороне 1С
    attrs: { disabled: true }
  }
};

const dataField = {
  id: "date",
  title: "Дата создания",
  queryKeys: ["datefrom_created", "dateto_created"],
  headCellProps: { sortBy: true, sortName: "object_date_create" },
  bodyCellProps: {
    render: ({ entity }) => {
      const date = new Date(entity.return.object_date_create);
      return `${getDate(date)} ${getTime(date)}`;
    }
  },
  formControlProps: {
    type: "date",
    attrs: { placeholder: "Поиск по дате создания" }
  }
};

const authorField = {
  id: "object_name_create",
  title: "Автор",
  queryKeys: ["object_id_create"],
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.return.object_name_create;
    }
  },
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Поиск по автору" },
    searchRequestProps: searchRequestPropsForAgents({
      params: {
        roles: [5]
      }
    })
  }
};
// TABLE FIELD --- END

const brandField = {
  id: "brand",
  title: "Товар",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["position", "product", "brand"]);
    }
  }
};

const articleField = {
  id: "article",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["position", "product", "article"]);
    }
  }
};

const nameField = {
  id: "name",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["position", "product", "name"]);
    }
  }
};

const goodStateField = {
  id: "good_state",
  title: "Статус",
  enumerationKey: "orderPositionGoodState",
  bodyCellProps: {
    render: renderEnumerationValue("orderPositionGoodState", [
      "position",
      "good_state"
    ])
  }
};

const countField = {
  id: "count",
  title: "Количество",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["position", "count"]);
    }
  }
};

const priceField = {
  id: "price",
  title: "Цена",
  bodyCellProps: {
    render: ({ entity }) => {
      const price = entity.position && entity.position.price;
      return price || price === 0 ? `${price} руб.` : "";
    }
  }
};

const sumField = {
  id: "sum",
  title: "Сумма",
  bodyCellProps: {
    render: ({ entity }) => {
      const sum = entity.position && entity.position.sum;
      return sum || sum === 0 ? `${sum} руб.` : "";
    }
  }
};

const reasonField = {
  id: "reason",
  title: "Причина возврата",
  enumerationKey: "returnReason",
  bodyCellProps: {
    render: renderEnumerationValue("returnReason", "reason")
  }
};

const purposeField = {
  id: "purpose",
  title: "Назначение возврата",
  enumerationKey: "returnPurpose",
  bodyCellProps: {
    render: renderEnumerationValue("returnPurpose", "purpose")
  }
};

export const fieldsForTable = renderFormFields([
  codeField,
  stateField,
  warehouseField,
  sumTotalField,
  sumPaymentField,
  dataField,
  authorField
]);

export const fieldsForNestedTable = renderFormFields([
  brandField,
  articleField,
  nameField,
  goodStateField,
  countField,
  priceField,
  sumField,
  reasonField,
  purposeField
]);
