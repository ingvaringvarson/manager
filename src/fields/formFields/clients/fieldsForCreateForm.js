import {
  required,
  maxLength,
  minLength,
  requiredOneOf
} from "../../../helpers/validators";
import {
  maskPhoneNumberWithCode,
  maskOnlyNumbers
} from "../../../helpers/masks";
import { renderFormFields } from "../../helpers";

const max255 = maxLength(255);
const max1000 = maxLength(1000);
const min10 = minLength(10);
const max12 = maxLength(12);
const requiredForPhone = requiredOneOf(["contacts_email"]);
const requiredForEmail = requiredOneOf(["contacts_phone"]);

export const fields = {
  1: renderFormFields(
    [
      {
        id: "person_name",
        formControlProps: {
          validators: [required, max255],
          notice: "* Обязательно к заполнению",
          type: "text",
          attrs: {
            placeholder: "Введите имя"
          }
        }
      },
      {
        id: "person_surname",
        formControlProps: {
          validators: [max255],
          type: "text",
          attrs: {
            placeholder: "Введите фамилию"
          }
        }
      },
      {
        id: "person_second_name",
        formControlProps: {
          validators: [max255],
          type: "text",
          attrs: {
            placeholder: "Введите отчество"
          }
        }
      },
      {
        id: "contacts_phone",
        formControlProps: {
          validators: [required, max255],
          mask: maskPhoneNumberWithCode,
          revertMask: maskOnlyNumbers,
          notice: "* Обязательно к заполнению",
          type: "text",
          attrs: {
            placeholder: "Введите телефон в формате +Х ХХХХХХХХХХ"
          }
        }
      },
      {
        id: "type",
        formControlProps: { value: 1 },
        isActive: false
      },
      {
        id: "roles",
        formControlProps: { value: [4] },
        isActive: false
      }
    ],
    true
  ),
  2: renderFormFields(
    [
      {
        id: "company_name_full",
        formControlProps: {
          validators: [required, max1000],
          notice: "* Обязательно к заполнению",
          type: "text",
          attrs: {
            placeholder: "Введите полное наименование компании"
          }
        }
      },
      {
        id: "company_inn",
        formControlProps: {
          validators: [required, min10, max12],
          notice: "* Обязательно к заполнению",
          type: "text",
          attrs: {
            placeholder: "Введите инн"
          }
        }
      },
      {
        id: "contacts_phone",
        formControlProps: {
          validators: [requiredForPhone, max255],
          mask: maskPhoneNumberWithCode,
          revertMask: maskOnlyNumbers,
          notice: "* Обязательно к заполнению",
          type: "text",
          attrs: {
            placeholder: "Введите телефон в формате +Х ХХХХХХХХХХ"
          }
        }
      },
      {
        id: "contacts_email",
        formControlProps: {
          validators: [requiredForEmail, max255],
          notice: "* Обязательно к заполнению",
          type: "text",
          attrs: {
            placeholder: "Введите email"
          }
        }
      },
      {
        id: "create_supplier",
        isActive: false,
        formControlProps: {
          type: "checkbox",
          name: "create_supplier",
          label: "Создать поставщика",
          value: false,
          valuesByChecked: new Map([[false, false], [true, true]]),
          checkedByValue: new Map([[false, false], [true, true]])
        },
        afterMap: ({ field, values }) => {
          const createSupplierAvailable = !!values;
          return createSupplierAvailable ? { ...field, isActive: true } : field;
        }
      },
      {
        id: "type",
        formControlProps: { value: 2 },
        isActive: false
      },
      {
        id: "roles",
        formControlProps: { value: [4] },
        isActive: false
      }
    ],
    true
  )
};
