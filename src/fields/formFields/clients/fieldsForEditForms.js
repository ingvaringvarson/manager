import {
  required,
  maxLength,
  minLength,
  notLaterCurrentDate,
  requiredKladrId
} from "../../../helpers/validators";
import {
  maskOnlyNumbers,
  masksByContacts,
  revertMasksByContacts,
  maskCustom,
  masksByNumberOnPersonRequisites,
  revertMasksByNumberOnPersonRequisites
} from "../../../helpers/masks";
import { renderFormFields } from "../../helpers/index";

const max255 = maxLength(255);
const max1000 = maxLength(1000);
const min10 = minLength(10);
const max12 = maxLength(12);
const min13 = minLength(13);
const max13 = maxLength(13);
const min9 = minLength(9);
const max9 = maxLength(9);
const min8 = minLength(8);
const max10 = maxLength(10);
const min20 = minLength(20);
const max20 = maxLength(20);

const afterMapForPersonRequisites = ({ field, values }) => {
  const selectedType = values.type;

  if (+selectedType === 1 && !field.isActive) {
    return {
      ...field,
      formControlProps: { ...field.formControlProps, value: "" },
      isActive: true
    };
  } else if (+selectedType !== 1 && field.isActive) {
    return {
      ...field,
      formControlProps: { ...field.formControlProps, value: "" },
      isActive: false
    };
  }

  return field;
};

const afterMapForContactValueByGroup = ({ field, values }) => {
  const _id = field.id.split("__")[1];

  const mask = masksByContacts(`type__${_id}`);
  return {
    ...field,
    formControlProps: {
      ...field.formControlProps,
      value: mask(field.formControlProps.value, values),
      mask,
      revertMask: revertMasksByContacts(`type__${_id}`)
    }
  };
};

const afterMapForContactValue = ({ field, values }) => {
  return {
    ...field,
    formControlProps: {
      ...field.formControlProps,
      value: field.formControlProps.mask(field.formControlProps.value, values)
    }
  };
};

export const fields = {
  person: renderFormFields([
    {
      id: "person_name",
      formControlProps: {
        validators: [required, max255],
        notice: "* Обязательно к заполнению",
        type: "text",
        attrs: { placeholder: "Введите имя" }
      }
    },
    {
      id: "person_surname",
      formControlProps: {
        validators: [max255],
        type: "text",
        attrs: { placeholder: "Введите фамилию" }
      }
    },
    {
      id: "person_second_name",
      formControlProps: {
        validators: [max255],
        type: "text",
        fullWidth: true,
        attrs: { placeholder: "Введите отчество" }
      }
    },
    {
      id: "person_born_date",
      formControlProps: {
        label: "Дата рождения",
        fullWidth: true,
        readonly: true,
        validators: [notLaterCurrentDate],
        type: "date"
      }
    },
    {
      id: "person_born_place",
      formControlProps: {
        validators: [max255],
        type: "text",
        attrs: { placeholder: "Введите место рождения" }
      }
    },
    {
      id: "person_gender",
      enumerationKey: "personGender",
      formControlProps: {
        type: "select",
        defaultValue: 0
      }
    },
    {
      id: "tags",
      enumerationKey: "agentTag",
      formControlProps: {
        type: "select",
        attrs: { placeholder: "Выберите теги" },
        isMultiple: true
      }
    },
    {
      id: "segments",
      enumerationKey: "agentSegment",
      formControlProps: {
        type: "select",
        attrs: { placeholder: "Выберите сегменты" },
        isMultiple: true
      }
    },
    {
      id: "_root",
      formControlProps: { value: "person" },
      isActive: false
    }
  ]),
  company: renderFormFields([
    {
      id: "company_name_full",
      formControlProps: {
        validators: [required, max1000],
        notice: "* Обязательно к заполнению",
        type: "text",
        attrs: { placeholder: "Введите полное наименование компании" }
      }
    },
    {
      id: "company_name_short",
      formControlProps: {
        validators: [max1000],
        type: "text",
        attrs: { placeholder: "Введите сокращенное наименование компании" }
      }
    },
    {
      id: "company_type",
      enumerationKey: "companyType",
      formControlProps: {
        type: "select",
        defaultValue: 0,
        attrs: { placeholder: "Выберите тип компании" }
      }
    },
    {
      id: "company_sno",
      enumerationKey: "companySno",
      formControlProps: {
        type: "select",
        defaultValue: 0,
        attrs: { placeholder: "Выберите тип системы налогообложения" }
      }
    },
    {
      id: "tags",
      enumerationKey: "agentTag",
      formControlProps: {
        type: "select",
        attrs: { placeholder: "Выберите теги" },
        isMultiple: true
      }
    },
    {
      id: "segments",
      enumerationKey: "agentSegment",
      formControlProps: {
        type: "select",
        attrs: { placeholder: "Выберите сегменты" },
        isMultiple: true
      }
    },
    {
      id: "_root",
      formControlProps: { value: "company" },
      isActive: false
    }
  ]),
  company_requisites: renderFormFields([
    {
      id: "company_inn",
      formControlProps: {
        mask: maskOnlyNumbers,
        validators: [required, max12, min10],
        notice: "* Обязательно к заполнению",
        type: "text",
        attrs: { placeholder: "Введите ИНН" }
      }
    },
    {
      id: "company_ogrn",
      formControlProps: {
        mask: maskOnlyNumbers,
        validators: [max13, min13],
        type: "text",
        attrs: { placeholder: "Введите ОГРН" }
      }
    },
    {
      id: "company_kpp",
      formControlProps: {
        mask: maskOnlyNumbers,
        validators: [min9, max9],
        type: "text",
        attrs: { placeholder: "Введите КПП" }
      }
    },
    {
      id: "company_okpo",
      formControlProps: {
        mask: maskOnlyNumbers,
        validators: [min8, max10],
        type: "text",
        attrs: { placeholder: "Введите ОКПО" }
      }
    },
    {
      id: "_root",
      formControlProps: { value: "company_requisites" },
      isActive: false
    }
  ]),
  contacts: renderFormFields([
    {
      id: "type",
      enumerationKey: "contactType",
      formControlProps: {
        validators: [required],
        notice: "* Обязательно к заполнению",
        type: "select",
        defaultValue: 0,
        attrs: { placeholder: "Выберите тип" }
      }
    },
    {
      id: "value",
      afterMap: afterMapForContactValue,
      formControlProps: {
        mask: masksByContacts(),
        revertMask: revertMasksByContacts(),
        notice: "* Обязательно к заполнению",
        validators: [required, max255],
        type: "text",
        attrs: { placeholder: "Введите контакт" }
      }
    },
    {
      id: "is_main",
      formControlProps: {
        value: false,
        valuesByChecked: new Map([[false, false], [true, true]]),
        checkedByValue: new Map([[false, false], [true, true]]),
        type: "checkbox",
        label: "Основной контакт",
        name: "is_main",
        attrs: { placeholder: "Основной контакт" }
      }
    },
    {
      id: "is_active",
      formControlProps: {
        value: false,
        valuesByChecked: new Map([[false, false], [true, true]]),
        checkedByValue: new Map([[false, false], [true, true]]),
        label: "Активный контакт",
        type: "checkbox",
        name: "is_active",
        attrs: { placeholder: "Активный контакт" }
      }
    },
    {
      id: "_root",
      formControlProps: { value: "contacts" },
      isActive: false
    },
    {
      id: "_id",
      formControlProps: { value: 0 },
      isActive: false
    }
  ]),
  contact_persons: renderFormFields([
    {
      id: "name",
      formControlProps: {
        notice: "* Обязательно к заполнению",
        validators: [required, max255],
        type: "text",
        attrs: { placeholder: "Введите Фамилию, Имя, Отчество" }
      }
    },
    {
      id: "gender",
      enumerationKey: "personGender",
      formControlProps: {
        type: "select",
        defaultValue: 0,
        attrs: { placeholder: "Выберите пол" }
      }
    },
    {
      id: "position",
      formControlProps: {
        validators: [max255],
        type: "text",
        attrs: { placeholder: "Введите наименование должности" }
      }
    },
    [
      { destructuring: true, id: "contacts", name: null },
      [
        {
          id: "type__",
          enumerationKey: "contactType",
          formControlProps: {
            notice: "* Обязательно к заполнению",
            type: "select",
            defaultValue: 0,
            attrs: { placeholder: "Выберите тип" }
          }
        },
        {
          id: "value__",
          afterMap: afterMapForContactValueByGroup,
          formControlProps: {
            mask: masksByContacts(),
            revertMask: revertMasksByContacts(),
            notice: "* Обязательно к заполнению",
            validators: [required, max255],
            type: "text",
            attrs: { placeholder: "Введите контакт" }
          }
        },
        {
          id: "is_main__",
          formControlProps: {
            type: "checkbox",
            value: false,
            valuesByChecked: new Map([[false, false], [true, true]]),
            checkedByValue: new Map([[false, false], [true, true]]),
            label: "Основной контакт",
            attrs: { placeholder: "Основной контакт" }
          }
        },
        {
          id: "is_active__",
          formControlProps: {
            type: "checkbox",
            value: false,
            valuesByChecked: new Map([[false, false], [true, true]]),
            checkedByValue: new Map([[false, false], [true, true]]),
            label: "Активный контакт",
            attrs: { placeholder: "Активный контакт" }
          }
        }
      ]
    ],
    {
      id: "id",
      isActive: false
    },
    {
      id: "_root",
      formControlProps: { value: "contact_persons" },
      isActive: false
    },
    {
      id: "_id",
      formControlProps: { value: 0 },
      isActive: false
    }
  ]),
  bank_details: renderFormFields([
    {
      id: "bank_name",
      formControlProps: {
        notice: "* Обязательно к заполнению",
        validators: [required, max1000],
        type: "text",
        attrs: { placeholder: "Введите Наименование банка" }
      }
    },
    {
      id: "correspondent_account",
      formControlProps: {
        notice: "* Обязательно к заполнению",
        validators: [required, max20, min20],
        type: "text",
        mask: maskOnlyNumbers,
        attrs: { placeholder: "Введите Корреспондентский счет" }
      }
    },
    {
      id: "settlement_account",
      formControlProps: {
        notice: "* Обязательно к заполнению",
        validators: [required, max20, min20],
        type: "text",
        mask: maskOnlyNumbers,
        attrs: { placeholder: "Введите Расчетный счет" }
      }
    },
    {
      id: "bik",
      formControlProps: {
        notice: "* Обязательно к заполнению",
        validators: [required, max9, min9],
        type: "text",
        mask: maskOnlyNumbers,
        attrs: { placeholder: "Введите БИК" }
      }
    },
    {
      id: "id",
      isActive: false,
      formControlProps: {
        type: "text"
      }
    },
    {
      id: "_root",
      formControlProps: { value: "bank_details" },
      isActive: false
    },
    {
      id: "_id",
      formControlProps: { value: 0 },
      isActive: false
    }
  ]),
  person_requisites: renderFormFields([
    {
      id: "type",
      enumerationKey: "documentType",
      formControlProps: {
        type: "select",
        defaultValue: 0,
        attrs: { placeholder: "Выберите тип документа" }
      }
    },
    {
      id: "number",
      formControlProps: {
        notice: "* Обязательно к заполнению",
        validators: [required, max255],
        mask: masksByNumberOnPersonRequisites("type"),
        revertMask: revertMasksByNumberOnPersonRequisites("typr"),
        type: "text",
        attrs: { placeholder: "Введите номер" }
      }
    },
    {
      id: "series",
      formControlProps: {
        validators: [max255],
        mask: maskOnlyNumbers,
        type: "text",
        attrs: { placeholder: "Введите серию" },
        afterChange: afterMapForPersonRequisites
      },
      afterMap: afterMapForPersonRequisites
    },
    {
      id: "issued",
      validators: [max255],
      type: "text",
      formControlProps: {
        type: "text",
        name: "issued",
        attrs: { placeholder: "Кем выдан" },
        afterChange: afterMapForPersonRequisites
      },
      afterMap: afterMapForPersonRequisites
    },
    {
      id: "division_code",
      formControlProps: {
        validators: [max255],
        mask: maskCustom,
        type: "text",
        attrs: { placeholder: "Введите код подразделения выдачи" },
        afterChange: afterMapForPersonRequisites
      },
      afterMap: afterMapForPersonRequisites
    },
    {
      id: "date_of_issue",
      formControlProps: {
        type: "date",
        name: "date_of_issue",
        attrs: { placeholder: "Выберите дату выдачи" },
        afterChange: afterMapForPersonRequisites
      },
      afterMap: afterMapForPersonRequisites
    },
    {
      id: "_root",
      formControlProps: { value: "person_requisites" },
      isActive: false
    },
    {
      id: "_id",
      formControlProps: { value: 0 },
      isActive: false
    }
  ]),
  addresses: renderFormFields([
    {
      id: "location",
      title: "Редактируемый адрес",
      isActive: false,
      formControlProps: {
        type: "text",
        label: "Редактируемый адрес:",
        attrs: {
          readOnly: true,
          iconClearProps: {
            hide: true
          }
        },
        afterChange: ({ field, values }) => {
          if (
            values.hasOwnProperty("kladr") &&
            values.hasOwnProperty("location") &&
            (values.kladr === "0" && !field.isActive && !!values.location)
          ) {
            return {
              ...field,
              isActive: true
            };
          }

          return field;
        }
      },
      afterMap: ({ field, values }) => {
        if (
          values.hasOwnProperty("kladr") &&
          values.hasOwnProperty("location") &&
          (!values.kladr || (values.kladr === "0" && !!values.location))
        ) {
          return {
            ...field,
            isActive: true
          };
        }

        return field;
      }
    },
    {
      id: "type",
      enumerationKey: "addressType",
      formControlProps: {
        type: "select",
        defaultValue: 0,
        validators: [required],
        attrs: { placeholder: "Выберите тип" },
        afterChange: ({ field, values }) => {
          if (
            values.hasOwnProperty("kladr") &&
            values.hasOwnProperty("location") &&
            (values.kladr === "0" && !field.isActive && !!values.location)
          ) {
            return {
              ...field,
              formControlProps: {
                ...field.formControlProps,
                label: "Выберите значения из списка адресов:"
              }
            };
          }

          return field;
        }
      },
      afterMap: ({ field, values }) => {
        if (
          values.hasOwnProperty("kladr") &&
          values.hasOwnProperty("location") &&
          (!values.kladr || (values.kladr === "0" && !!values.location))
        ) {
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              label: "Выберите значения из списка адресов:"
            }
          };
        }

        return field;
      }
    },
    {
      id: "kladr",
      formControlProps: {
        validators: [requiredKladrId],
        type: "kladr"
      }
    },
    {
      id: "_root",
      formControlProps: { value: "addresses" },
      isActive: false
    },
    {
      id: "_id",
      formControlProps: { value: 0 },
      isActive: false
    }
  ])
};
