import React from "react";
import { getUrl } from "../../../helpers/urls";
import { Link } from "react-router-dom";
import { handleStopLifting } from "../../../components/common/tableControls";

import {
  renderDateTime,
  renderFormFields,
  renderEnumerationValue
} from "../../helpers/index";

const renderBillCode = ({ entity }) => {
  const billsTypes = [1, 6, 7, 10, 11, 12, 16];
  const paymentsTypes = [2, 5, 9, 13];
  const billsAndPaymentsTypes = [3, 19];
  const ordersTypes = [4];
  const returnsTypes = [8];
  const purchaseOrdersTypes = [14];
  const purchaseReturnsTypes = [15];
  const nullBillCodeTypes = [17];
  const nullExternalBalance = [18];

  const isEntityType = type => entity.type === type;

  switch (true) {
    case billsTypes.some(isEntityType) && !!entity.bill_id:
      return (
        !!entity.bill_code && (
          <Link
            to={getUrl("bills", { id: entity.bill_id })}
            onClick={handleStopLifting}
          >
            {entity.bill_code}
          </Link>
        )
      );
    case paymentsTypes.some(isEntityType) && !!entity.payment_id:
      return (
        !!entity.payment_code && (
          <Link
            to={getUrl("payments", { id: entity.payment_id })}
            onClick={handleStopLifting}
          >
            {entity.payment_code}
          </Link>
        )
      );
    case billsAndPaymentsTypes.some(isEntityType) &&
      !!entity.bill_id &&
      !!entity.payment_id:
      return (
        !!entity.bill_code &&
        !!entity.payment_code && (
          <div>
            <p>
              <Link
                to={getUrl("bills", { id: entity.bill_id })}
                onClick={handleStopLifting}
              >
                {entity.bill_code}
              </Link>
            </p>
            <p>
              <Link
                to={getUrl("payments", { id: entity.payment_id })}
                onClick={handleStopLifting}
              >
                {entity.payment_code}
              </Link>
            </p>
          </div>
        )
      );
    case ordersTypes.some(isEntityType) && !!entity.order_code:
      return (
        !!entity.order_id && (
          <Link
            to={getUrl("orders", { id: entity.order_id })}
            onClick={handleStopLifting}
          >
            {entity.order_code}
          </Link>
        )
      );
    case returnsTypes.some(isEntityType) && !!entity.order_code:
      return (
        !!entity.order_id && (
          <Link
            to={getUrl("orders", { id: entity.order_id })}
            onClick={handleStopLifting}
          >
            {entity.order_code}
          </Link>
        )
      );
    case purchaseOrdersTypes.some(isEntityType) && !!entity.order_code:
      return (
        !!entity.order_id && (
          <Link
            to={getUrl("orders", { id: entity.order_id })}
            onClick={handleStopLifting}
          >
            {entity.order_code}
          </Link>
        )
      );
    case purchaseReturnsTypes.some(isEntityType) && !!entity.order_code:
      return (
        !!entity.order_id && (
          <Link
            to={getUrl("orders", { id: entity.order_id })}
            onClick={handleStopLifting}
          >
            {entity.order_code}
          </Link>
        )
      );
    case nullBillCodeTypes.some(isEntityType) && !!entity.bill_code:
      return entity.bill_code;
    case nullExternalBalance.some(isEntityType):
      return (
        entity.order_code || entity.bill_code || entity.payment_code || null
      );
    default:
      return "";
  }
};

export const fieldsForContractFilters = renderFormFields([
  {
    id: "contract_id",
    title: "Договор",
    enumerationKey: "contract_id",
    formControlProps: {
      type: "select",
      attrs: {
        placeholder: "Выберите договор"
      }
    },
    afterMap: ({ field, values }) => {
      if (!values.contracts) return field;
      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          options: values.contracts.map(v => ({ id: v.id, name: v.number }))
        }
      };
    }
  },
  {
    id: "account_type",
    type: "select",
    title: "Тип баланса",
    enumerationKey: "accountType",
    formControlProps: {
      type: "select",
      attrs: {
        placeholder: "Выберите тип баланса"
      }
    }
  }
]);

export const fieldsForTransactionsTable = renderFormFields([
  {
    id: "object_date_create",
    title: "Дата",
    bodyCellProps: {
      render: renderDateTime("object_date_create")
    }
  },
  {
    id: "type",
    title: "Тип операции",
    bodyCellProps: {
      render: renderEnumerationValue("transactionType", "type")
    }
  },
  {
    id: "bill_code",
    title: "Номер документа",
    bodyCellProps: {
      render: renderBillCode
    }
  },
  {
    id: "account_in_value",
    title: "Приход",
    bodyCellProps: {
      render: ({ entity }) => {
        let value = "";

        if (entity.type === 1 && entity.account_in_value > 0) {
          value = entity.account_in_value;
        } else if (entity.type === 2 && entity.account_out_value > 0) {
          value = entity.account_out_value;
        }

        return value;
      }
    }
  },
  {
    id: "account_out_value",
    title: "Расход",
    bodyCellProps: {
      render: ({ entity }) => {
        let value = "";

        if (entity.type === 1 && entity.account_in_value < 0) {
          value = entity.account_in_value;
        } else if (entity.type === 2 && entity.account_out_value < 0) {
          value = entity.account_out_value;
        }

        return value;
      }
    }
  }
]);

export const fieldsForTransactionsFilters = renderFormFields([
  {
    id: "begining_date",
    formControlProps: {
      attrs: { placeholder: "Выберите начало диапазона" },
      type: "date"
    }
  },
  {
    id: "closing_date",
    formControlProps: {
      attrs: { placeholder: "Выберите окончание диапазона" },
      type: "date"
    }
  }
]);
