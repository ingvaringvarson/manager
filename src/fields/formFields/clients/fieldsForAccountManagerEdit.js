import { renderFormFields } from "../../helpers";
import { searchRequestPropsForAgents } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

export const fields = renderFormFields([
  {
    id: "manager_id",
    formControlProps: {
      type: "select_search_request",
      label: "Аккаунт-менеджер",
      attrs: { placeholder: "Введите ФИО менеджера" },
      searchRequestProps: searchRequestPropsForAgents({
        params: {
          roles: [5]
        }
      })
    },
    afterMap: ({ values, field }) => {
      const value = values.manager_id ? [values.manager_id] : [];
      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          value,
          defaultValue: []
        }
      };
    }
  }
]);
