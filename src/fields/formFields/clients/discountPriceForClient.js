import { renderFormFields } from "../../helpers/index";

export const discountPriceModal = {
  title: "Редактирование",
  subtitle: "Физическое лицо",
  fields: renderFormFields([
    {
      id: "discountGroup",
      enumerationKey: "discountGroup",
      formControlProps: {
        type: "select",
        title: "Ценовая колонка",
        attrs: { placeholder: "Выберите ценовую колонку" }
      },
      afterMap: ({ field, values }) => {
        const { discountGroups, discountGroupsForAccount } = values;
        const valueToField =
          discountGroupsForAccount && discountGroupsForAccount.discountGroupId
            ? [+discountGroupsForAccount.discountGroupId]
            : null;
        const options = discountGroups.map(discountGroup => ({
          ...discountGroup,
          id: discountGroup.discountGroupId
        }));
        return {
          ...field,
          formControlProps: {
            ...field.formControlProps,
            value: valueToField,
            defaultValue: valueToField,
            options
          }
        };
      }
    }
  ]),
  renderView: ({ entity, enumerations, modalData }) => {
    const { type } = entity;
    const { agentType } = enumerations;
    if (
      !(
        agentType &&
        agentType.map &&
        agentType.map[type] &&
        agentType.map[type].name
      )
    )
      return modalData;

    return {
      ...modalData,
      subtitle: agentType.map[type].name
    };
  }
};
