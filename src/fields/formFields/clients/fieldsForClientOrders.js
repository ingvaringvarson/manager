import { renderFormFields } from "../../helpers/index";
import { getDate, getTime } from "../../../utils";
import { renderDeliveryType } from "../../helpers";

export const fields = renderFormFields(
  [
    {
      id: "code",
      title: "Номер",
      headCellProps: { sortBy: true },
      formControlProps: {
        type: "text",
        attrs: {
          placeholder: "Введите номер"
        }
      }
    },
    {
      id: "state",
      title: "Статус",
      bodyCellProps: {
        render: ({ entity, enumerations }) => {
          return enumerations["orderState"] &&
            enumerations["orderState"].map[entity.state]
            ? enumerations["orderState"].map[entity.state].name
            : null;
        }
      },
      enumerationKey: "orderState",
      formControlProps: {
        type: "select",
        attrs: {
          placeholder: "Введите статус"
        }
      }
    },
    {
      id: "estimated_time",
      title: "Ожидаемая дата поступления",
      queryKeys: ["estimated_time_start", "estimated_time_end"],
      headCellProps: { sortBy: true },
      bodyCellProps: {
        render: ({ entity }) => {
          if (!entity.positions) return null;

          const maxDate = entity.positions.reduce(
            (currentMaxDate, position) => {
              const currentDate = new Date(position.estimated_time);

              if (!currentMaxDate || +currentMaxDate < +currentDate) {
                return currentDate;
              }

              return currentMaxDate;
            },
            ""
          );

          return `${getDate(maxDate)} ${getTime(maxDate)}`;
        }
      },
      formControlProps: {
        type: "date",
        attrs: {
          placeholder: "Введите способ получения"
        }
      }
    },
    {
      id: "warehouse_id",
      title: "Способ получения",
      headCellProps: { sortBy: true },
      bodyCellProps: {
        render: renderDeliveryType
      },
      formControlProps: {
        type: "text",
        name: "warehouse_id",
        attrs: {
          placeholder: "Введите способ получения"
        }
      }
    },
    {
      id: "sum_total",
      title: "Сумма заказа",
      formControlProps: {
        attrs: { disabled: true },
        type: "text"
      }
    },
    {
      id: "not_ready_0",
      title: "Сумма предоплаты",
      formControlProps: {
        attrs: { disabled: true },
        type: "text"
      }
    },
    {
      id: "not_ready_1",
      title: "Сумма к оплате",
      formControlProps: {
        attrs: { disabled: true },
        type: "text"
      }
    },
    {
      id: "reserve_time",
      title: "Время резерва",
      queryKeys: ["reserve_time_start", "reserve_time_end"],
      headCellProps: { sortBy: true },
      bodyCellProps: {
        render: ({ entity }) => {
          if (!entity.positions) return null;

          const maxDate = entity.positions.reduce(
            (currentMaxDate, position) => {
              const currentDate = new Date(position.reserve_time);

              if (!currentMaxDate || +currentMaxDate < +currentDate) {
                return currentDate;
              }

              return currentMaxDate;
            },
            ""
          );

          return `${getDate(maxDate)} ${getTime(maxDate)}`;
        }
      },
      formControlProps: {
        type: "date",
        attrs: {
          placeholder: "Введите время резерва"
        }
      }
    },
    {
      id: "object_date_create",
      title: "Дата создания",
      queryKeys: ["datefrom_created", "dateto_created"],
      headCellProps: { sortBy: true },
      bodyCellProps: {
        render: ({ entity }) => {
          if (!entity.object_date_create) return null;

          const currentDate = new Date(entity.object_date_create);

          return `${getDate(currentDate)} ${getTime(currentDate)}`;
        }
      },
      formControlProps: {
        type: "date",
        attrs: {
          placeholder: "Введите дату создания"
        }
      }
    },
    {
      id: "object_id_create",
      title: "Автор",
      formControlProps: {
        type: "text",
        attrs: {
          disabled: true,
          placeholder: "Введите автора"
        }
      }
    },
    {
      id: "firm_id",
      title: "Фирма",
      formControlProps: {
        type: "select",
        attrs: {
          disabled: true,
          placeholder: "Введите фирму"
        }
      }
    },
    {
      id: "sort_by",
      isActive: false
    }
  ],
  true
);
