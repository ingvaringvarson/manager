import React from "react";

import {
  renderFormFields,
  renderEnumerationValue,
  renderBooleanValue,
  getNameCompanyShort,
  getIdForEntity
} from "../../helpers";
import { getDate, getValueInDepth, generateId } from "../../../utils";
import Title from "../../../designSystem/atoms/Title";
import {
  required,
  maxDateRange,
  minDateRange
} from "../../../helpers/validators";
import { searchRequestPropsForAgents } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

export const headlines = [
  {
    id: generateId(),
    flex: "5 1 0",
    render: () => <Title>Условия договора</Title>
  },
  {
    id: generateId(),
    flex: "4 1 0",
    render: () => <Title>Условия взаимодействия</Title>
  },
  {
    id: generateId(),
    flex: "3 1 0",
    render: () => <Title>Баланс</Title>
  }
];

export const fields = renderFormFields([
  {
    id: "number",
    title: "Номер"
  },
  {
    id: "type",
    title: "Тип",
    bodyCellProps: { render: renderEnumerationValue("contractType", "type") },
    enumerationKey: "contractType"
  },
  {
    id: "date_range",
    title: "Срок действия",
    bodyCellProps: {
      render: ({ entity }) => {
        if (!entity.date_start && !entity.date_end) return null;

        const dateStart = entity.date_start
          ? new Date(entity.date_start)
          : null;
        const dateEnd = entity.date_end ? new Date(entity.date_end) : null;

        return `${dateStart ? getDate(dateStart) : ""} - ${getDate(dateEnd)}`;
      }
    }
  },
  {
    id: "state",
    title: "Статус",
    bodyCellProps: { render: renderEnumerationValue("contractState", "state") },
    enumerationKey: "contractState"
  },
  {
    id: "executor_name",
    title: "Исполнитель"
  },
  {
    id: "credit_limit",
    title: "Кредитный лимит"
  },
  {
    id: "deferred_payment",
    title: "Дни отсрочки"
  },
  {
    id: "auto_delivery",
    title: "Автоматически в работу",
    bodyCellProps: { render: renderBooleanValue("auto_delivery", "Да", "Нет") }
  },
  {
    id: "terms",
    title: "Отгрузка без платежа",
    bodyCellProps: { render: renderEnumerationValue("contractTerms", "terms") },
    enumerationKey: "contractTerms"
  },
  {
    id: "account_out",
    title: "Внешний"
  },
  {
    id: "account_in",
    title: "Внутренний"
  },
  {
    id: "description",
    title: "Комментарий"
  }
]);

export const fieldsForForm = renderFormFields([
  // в values.client должна лежать информация об агенте, кому принадлежит договор или для кого догвор создаётся
  {
    id: "number",
    title: "Номер договора",
    formControlProps: {
      type: "text",
      label: "Номер договора",
      notice: "* Обязательно к заполнению",
      validators: [required],
      attrs: { placeholder: "Введите номер договора" }
    }
  },
  {
    id: "type",
    title: "Тип",
    enumerationKey: "contractType",
    formControlProps: {
      type: "select",
      label: "Тип договора",
      notice: "* Обязательно к заполнению",
      validators: [required],
      attrs: { placeholder: "Выберите тип" },
      defaultValue: ["1"],
      value: ["1"]
    },
    afterMap: ({ values, field }) => {
      if (
        values.client &&
        Array.isArray(values.client.roles) &&
        values.client.roles.includes(1)
      ) {
        return field;
      }
      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          options: field.formControlProps.options.filter(opt => opt.id !== 2)
        }
      };
    }
  },
  [
    { destructuring: false, name: "Срок действия", id: null },
    [
      {
        id: "date_start",
        formControlProps: {
          type: "date",
          notice: "* Обязательно к заполнению",
          attrs: { placeholder: "Выберите дату" },
          validators: [required, maxDateRange("date_end")]
        }
      },
      {
        id: "date_end",
        formControlProps: {
          type: "date",
          attrs: { placeholder: "Выберите дату" },
          validators: [minDateRange("date_start")]
        }
      }
    ]
  ],
  {
    id: "state",
    title: "Статус",
    enumerationKey: "contractState",
    formControlProps: {
      type: "select",
      notice: "* Обязательно к заполнению",
      validators: [required],
      label: "Статус договора",
      attrs: { placeholder: "Выберите статус" }
    }
  },
  {
    id: "executor_id",
    /*
     **  По требованиям MAN-1395 этот филд должен появлятся при условиях:
     **  Если выбранный тип договора не "Закупка"
     **  id "Закупки" === 2
     */
    title: "Исполнитель",
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      notice: "* Обязательно к заполнению",
      validators: [required],
      attrs: { placeholder: "Введите исполнителя" },
      label: "Организация - Исполнитель",
      searchRequestProps: searchRequestPropsForAgents({
        keyById: "id",
        getName: getNameCompanyShort,
        getId: getIdForEntity,
        params: {
          roles: [2]
        }
      }),
      afterChange: ({ values, prevValues, field }) => {
        if (values.type !== prevValues.type) {
          if (!(Array.isArray(values.type) && !values.type.includes("2"))) {
            return {
              ...field,
              isActive: false,
              formControlProps: {
                ...field.formControlProps,
                value: [values._agent_id]
              },
              values: {
                ...values,
                [field.id]: [values._agent_id]
              }
            };
          } else {
            return {
              ...field,
              isActive: true,
              formControlProps: {
                ...field.formControlProps,
                value: []
              },
              values: {
                ...values,
                [field.id]: []
              }
            };
          }
        }
        return field;
      }
    },
    afterMap: ({ values, field }) => {
      if (values.hasOwnProperty("type") && values.type === 2) {
        const agentId = getValueInDepth(values, ["client", "id"]);

        return {
          ...field,
          isActive: false,
          formControlProps: {
            ...field.formControlProps,
            value: [agentId]
          }
        };
      }

      return field;
    }
  },
  {
    id: "customer_id",
    /*
     **  По требованиям MAN-1395 этот филд должен появлятся при условиях:
     **  Если выбранный тип договора не "Поставка"
     **  id "Поставки" === 1
     */
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      notice: "* Обязательно к заполнению",
      validators: [required],
      attrs: { placeholder: "Введите покупателя" },
      label: "Организация - Покупатель",
      searchRequestProps: searchRequestPropsForAgents({
        keyById: "id",
        getName: getNameCompanyShort,
        getId: getIdForEntity,
        params: {
          roles: [2]
        }
      }),
      afterChange: ({ values, prevValues, field }) => {
        if (values.type !== prevValues.type) {
          if (!(Array.isArray(values.type) && !values.type.includes("1"))) {
            return {
              ...field,
              isActive: false,
              formControlProps: {
                ...field.formControlProps,
                value: [values._agent_id]
              },
              values: {
                ...values,
                [field.id]: [values._agent_id]
              }
            };
          } else {
            return {
              ...field,
              isActive: true,
              formControlProps: {
                ...field.formControlProps,
                value: []
              },
              values: {
                ...values,
                [field.id]: []
              }
            };
          }
        }
        return field;
      }
    },
    afterMap: ({ values, field }) => {
      if (
        !values.hasOwnProperty("type") ||
        (values.hasOwnProperty("type") && values.type === 1)
      ) {
        const agentId = getValueInDepth(values, ["client", "id"]);

        return {
          ...field,
          isActive: false,
          formControlProps: {
            ...field.formControlProps,
            value: [agentId]
          }
        };
      }

      return field;
    }
  },
  {
    id: "credit_limit",
    title: "Кредитный лимит",
    formControlProps: {
      type: "text",
      label: "Кредитный лимит",
      attrs: { placeholder: "Введите кредитный лимит" }
    }
  },
  {
    id: "deferred_payment",
    title: "Дни отсрочки",
    formControlProps: {
      type: "text",
      label: "Количество дней отсрочки",
      attrs: { placeholder: "Введите дни отсрочки" }
    }
  },
  {
    id: "terms",
    title: "Выдать без платежа",
    formControlProps: {
      type: "checkbox",
      valuesByChecked: new Map([[false, false], [true, true]]),
      checkedByValue: new Map([[false, false], [true, true]]),
      label: "Выдать без платежа"
    },
    afterMap: ({ values, field }) => ({
      ...field,
      formControlProps: {
        ...field.formControlProps,
        value: values[field.id] === 2
      }
    })
  },
  {
    id: "auto_delivery",
    title: "Автоматически брать в работу без предоплаты",
    formControlProps: {
      type: "checkbox",
      valuesByChecked: new Map([[false, false], [true, true]]),
      checkedByValue: new Map([[false, false], [true, true]]),
      label: "Автоматически брать в работу без предоплаты"
    }
  },
  {
    id: "description",
    title: "Описание",
    formControlProps: {
      type: "textarea",
      label: "Описание",
      attrs: { placeholder: "Введите значение" }
    }
  },
  {
    id: "id",
    isActive: false
  },
  {
    id: "object_id_edit",
    isActive: false
  },
  {
    id: "_root",
    formControlProps: { value: "contracts" },
    isActive: false
  },
  {
    id: "_id",
    isActive: false
  },
  {
    id: "_agent_id",
    isActive: false,
    afterMap: ({ values, field }) => {
      // agent_id будет являться значением либо для customer_id или либо для executor_id, смотрим тикет MAN-1395
      const agentId = getValueInDepth(values, ["client", "id"]);

      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          value: agentId
        }
      };
    }
  }
]);
