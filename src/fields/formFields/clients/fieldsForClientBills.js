import { renderEnumerationValue, renderFormFields } from "../../helpers";

export const fields = renderFormFields(
  [
    {
      id: "code",
      title: "Номер",
      headCellProps: { sortBy: true },
      formControlProps: {
        type: "text",
        name: "code",
        attrs: {
          placeholder: "Введите номер"
        }
      }
    },
    {
      id: "state",
      title: "Статус",
      bodyCellProps: {
        render: renderEnumerationValue("billState", "state")
      },
      enumerationKey: "billState",
      formControlProps: {
        type: "select",
        attrs: {
          placeholder: "Введите статус"
        }
      }
    },
    {
      id: "type",
      title: "Тип счета",
      bodyCellProps: {
        render: renderEnumerationValue("billType", "type")
      },
      enumerationKey: "billType",
      formControlProps: {
        type: "select",
        attrs: {
          placeholder: "Введите тип счета"
        }
      }
    },
    {
      id: "order_id",
      title: "Номер заказа",
      formControlProps: {
        attrs: { disabled: true },
        type: "text"
      }
    },
    {
      id: "bill_sum",
      title: "Сумма",
      formControlProps: {
        attrs: { disabled: true },
        type: "text"
      }
    },
    {
      id: "bill_sum_receipt_sum",
      title: "Оплачено",
      bodyCellProps: {
        render: ({ entity }) => {
          return entity.bill_sum - entity.receipt_sum;
        }
      },
      formControlProps: {
        attrs: { disabled: true },
        type: "text"
      }
    },
    {
      id: "receipt_sum",
      title: "Сумма к оплате",
      headCellProps: { sortBy: true },
      formControlProps: {
        attrs: { disabled: true },
        type: "text"
      }
    },
    {
      id: "comment",
      title: "Комментарий",
      formControlProps: {
        attrs: { disabled: true },
        type: "text"
      }
    }
  ],
  true
);

export const fieldsForPositions = renderFormFields(
  [
    {
      id: "product",
      title: "Товар",
      bodyCellProps: {
        render: ({ entity }) => {
          if (!entity.product) return null;

          return `${entity.product.article} ${entity.product.brand} ${entity.product.name}`;
        }
      }
    },
    {
      id: "type",
      title: "Тип",
      bodyCellProps: {
        render: ({ entity, enumerations }) => {
          return entity.product
            ? renderEnumerationValue("productType", "type")({
                entity: entity.product,
                enumerations
              })
            : "";
        }
      }
    },
    {
      id: "sum",
      title: "Сумма"
    }
  ],
  true
);

export const fieldsForPayments = renderFormFields(
  [
    {
      id: "payment_code",
      title: "Платеж"
    },
    {
      id: "sum",
      title: "Сумма"
    }
  ],
  true
);
