import { getDate } from "../../../utils";
import {
  mapEntitiesToGroupedList,
  renderInfoFields,
  mapValuesToFields,
  mapEntitiesToList,
  mapValuesToGroupedList
} from "../../helpers/index";
import { fields } from "./fieldsForEditForms";

import {
  discountPriceViewField,
  mailingPriceViewField,
  clientTagsField,
  clientSegmentsField
} from "../../templates/infoBlockFieldTemplates/clients";

const EntityRecord = {
  // title: infoBlock - title
  title: null,
  // formMain: form description for heading icon and form
  formMain: null,
  // formField: form description for body icon and form
  formField: null,
  view: "default",
  infoFields: [],
  // formFieldTemplates: fields
  formFields: [],
  fieldsForAdd: [],
  mapValuesToFields
};

export const fieldGroups = {
  person: {
    ...EntityRecord,
    title: "Физическое лицо",
    formMain: {
      subtitle: "Физическое лицо",
      title: "Редактирование",
      icon: "edit",
      type: "edit"
    },
    infoFields: renderInfoFields([
      {
        key: "person_surname",
        title: "Фамилия"
      },
      {
        key: "person_name",
        title: "Имя"
      },
      {
        key: "person_second_name",
        title: "Отчество"
      },
      {
        key: "person_born_date",
        title: "Дата рождения",
        mask: getDate
      },
      {
        key: "person_born_place",
        title: "Место рождения"
      },
      {
        key: "person_gender",
        title: "Пол",
        enumerationKey: "personGender"
      },
      clientTagsField,
      clientSegmentsField,
      discountPriceViewField,
      mailingPriceViewField
    ]),
    formFields: fields["person"]
  },
  company: {
    ...EntityRecord,
    title: "Юридическое лицо",
    mapData: (entity, block) => ({ ...entity, ...entity[block.type] }),
    formMain: {
      subtitle: "Юридическое лицо",
      title: "Редактирование",
      icon: "edit",
      type: "edit"
    },
    infoFields: renderInfoFields([
      {
        key: "company_name_full",
        title: "Полное наименование"
      },
      {
        key: "company_name_short",
        title: "Сокращенное наименование"
      },
      {
        key: "company_type",
        title: "Тип компании",
        enumerationKey: "companyType"
      },
      {
        key: "company_sno",
        title: "СНО",
        enumerationKey: "companySno"
      },
      clientTagsField,
      clientSegmentsField,
      discountPriceViewField,
      mailingPriceViewField
    ]),
    formFields: fields["company"]
  },
  contacts: {
    ...EntityRecord,
    title: "Контакты",
    formMain: {
      subtitle: "Контакты",
      title: "Создание контакта",
      type: "add",
      icon: "add"
    },
    formField: {
      subtitle: "Контакт",
      title: "Редактирование",
      type: "edit"
    },
    view: "groupedList",
    mapValuesToFields: mapEntitiesToGroupedList("contactType"),
    formFields: fields["contacts"]
  },
  addresses: {
    ...EntityRecord,
    title: "Адреса",
    formMain: {
      subtitle: "Адреса",
      title: "Создание",
      type: "add",
      icon: "add"
    },
    formField: {
      subtitle: "Адрес",
      title: "Редактирование",
      type: "edit"
    },
    view: "groupedList",
    mapValuesToFields: mapEntitiesToGroupedList("addressType", "location"),
    formFields: fields["addresses"]
  },
  contact_persons: {
    ...EntityRecord,
    title: "Контактные лица",
    formMain: {
      subtitle: "Контактные лица",
      title: "Создание",
      type: "add",
      icon: "add"
    },
    formField: {
      subtitle: "Контактные лица",
      title: "Редактирование",
      type: "edit"
    },
    view: "list",
    infoFields: renderInfoFields([
      {
        key: "name",
        title: "Имя"
      },
      {
        key: "gender",
        title: "Пол",
        enumerationKey: "personGender"
      },
      {
        key: "position",
        title: "Должность"
      },
      {
        key: "contacts",
        mapValueToFields: mapValuesToGroupedList("contactType"),
        view: "groupedList"
      }
    ]),
    mapValuesToFields: mapEntitiesToList,
    formFields: fields["contact_persons"]
  },
  bank_details: {
    ...EntityRecord,
    title: "Банковские реквизиты",
    formMain: {
      subtitle: "Банковские реквизиты",
      title: "Создание",
      type: "add",
      icon: "add"
    },
    formField: {
      subtitle: "Банковские реквизиты",
      title: "Редактирование",
      type: "edit"
    },
    view: "list",
    infoFields: renderInfoFields([
      {
        key: "bank_name",
        title: "Наименование банка"
      },
      {
        key: "settlement_account",
        title: "Расчетный счет"
      },
      {
        key: "correspondent_account",
        title: "Корреспондентский счет"
      },
      {
        key: "bik",
        title: "БИК"
      }
    ]),
    mapValuesToFields: mapEntitiesToList,
    formFields: fields["bank_details"]
  },
  person_requisites: {
    ...EntityRecord,
    title: "Персональные документы",
    formMain: {
      subtitle: "Персональные документы",
      title: "Создание",
      type: "add",
      icon: "add"
    },
    formField: {
      subtitle: "Персональные документы",
      title: "Редактирование",
      type: "edit"
    },
    view: "list",
    infoFields: renderInfoFields([
      {
        key: "type",
        title: "Тип",
        enumerationKey: "documentType"
      },
      {
        key: "number",
        title: "Номер"
      },
      {
        key: "series",
        title: "Серия"
      },
      {
        key: "issued",
        title: "Кем выдан"
      },
      {
        key: "division_code",
        title: "Код подразделения выдачи"
      },
      {
        key: "date_of_issue",
        title: "Дата выдачи"
      }
    ]),
    mapValuesToFields: mapEntitiesToList,
    formFields: fields["person_requisites"]
  },
  company_requisites: {
    ...EntityRecord,
    title: "Реквизиты компании",
    formMain: {
      subtitle: "Реквизиты компании",
      title: "Редактирование",
      type: "edit",
      icon: "edit"
    },
    infoFields: renderInfoFields([
      {
        key: "company_inn",
        title: "ИНН"
      },
      {
        key: "company_ogrn",
        title: "ОГРН"
      },
      {
        key: "company_kpp",
        title: "КПП"
      },
      {
        key: "company_okpo",
        title: "ОКПО"
      }
    ]),
    formFields: fields["company_requisites"]
  }
};

export const personBlocks = [
  {
    type: "person",
    info: fieldGroups["person"],
    renderContent: null
  },
  {
    type: "contacts",
    info: fieldGroups["contacts"],
    renderContent: null
  },
  {
    type: "addresses",
    info: fieldGroups["addresses"],
    renderContent: null
  },
  {
    type: "person_requisites",
    info: fieldGroups["person_requisites"],
    renderContent: null
  },
  {
    type: "bank_details",
    info: fieldGroups["bank_details"],
    renderContent: null
  }
];

export const companyBlocks = [
  {
    type: "company",
    info: fieldGroups["company"],
    renderContent: null,
    mapValuesToFields: null
  },
  {
    type: "contacts",
    info: fieldGroups["contacts"],
    renderContent: null,
    mapValuesToFields: null
  },
  {
    type: "contact_persons",
    info: fieldGroups["contact_persons"],
    renderContent: null,
    mapValuesToFields: null
  },
  {
    type: "addresses",
    info: fieldGroups["addresses"],
    renderContent: null,
    mapValuesToFields: null
  },
  {
    type: "company_requisites",
    info: fieldGroups["company_requisites"],
    renderContent: null,
    mapValuesToFields: null
  },
  {
    type: "bank_details",
    info: fieldGroups["bank_details"],
    renderContent: null,
    mapValuesToFields: null
  }
];
