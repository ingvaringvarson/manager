import { renderFormFields } from "../../helpers/index";
import { maskOnlyNumbers } from "../../../helpers/masks";
import { valueGreaterOrEqualThan, required } from "../../../helpers/validators";

const validators = [valueGreaterOrEqualThan(1), required];

export const fieldsForWithdraw = renderFormFields([
  {
    id: "amount",
    formControlProps: {
      validators,
      mask: maskOnlyNumbers,
      type: "text",
      attrs: {
        placeholder: "Сумма изъятия"
      }
    }
  }
]);

export const fieldsForDeposit = renderFormFields([
  {
    id: "amount",
    formControlProps: {
      validators,
      mask: maskOnlyNumbers,
      type: "text",
      attrs: {
        placeholder: "Сумма внесения"
      }
    }
  }
]);

export const fieldsForWithdrawToCentralOffice = renderFormFields([
  {
    id: "amount",
    formControlProps: {
      validators,
      mask: maskOnlyNumbers,
      type: "text",
      attrs: {
        placeholder: "Сумма изъятия"
      }
    }
  }
]);
