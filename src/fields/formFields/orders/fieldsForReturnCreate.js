import React from "react";
import { renderFormFields } from "../../helpers/index";
import {
  brandField,
  nameField,
  articleField
} from "../../templates/tableCellTemplates/positionFields";
import {
  required,
  valueGreaterThan,
  valueLessOrEqualThan,
  valueGreaterOrEqualThan
} from "../../../helpers/validators";
import FieldContext from "../../../components/common/formControls/FieldContext";

const bodyCellProps = {
  render: ({ entity, field }) => {
    const name = `${field.id}__${entity._id}`;
    return <FieldContext name={name} />;
  }
};

const countTableCell = {
  id: "count",
  title: "Кол-во",
  bodyCellProps
};

const countFormField = {
  id: "count__",
  shouldNotBeInValue: true,
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "Количество",
      disabled: true
    },
    validators: [valueGreaterThan(0), required]
  }
};

const returnTableCell = {
  id: "return",
  title: "Вернуть",
  bodyCellProps
};

const returnFormField = {
  id: "return__",
  shouldNotBeInValue: true,
  formControlProps: {
    type: "text",
    attrs: { placeholder: "Вернуть" }
  },
  afterMap: ({ field, fields, values }) => {
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        value: values.count,
        validators: [
          valueGreaterOrEqualThan(1),
          valueLessOrEqualThan(values.count),
          required
        ]
      }
    };
  }
};

const reasonTableCell = {
  id: "reason",
  title: "Причина возврата",
  bodyCellProps
};

const reasonFormField = {
  id: "reason__",
  shouldNotBeInValue: true,
  enumerationKey: "returnReason",
  formControlProps: {
    type: "select",
    attrs: { placeholder: "Выберите причину" },
    validators: [required],
    defaultValue: [1],
    value: [1]
  }
};

const purposeTableCell = {
  id: "purpose",
  title: "Назначение возврата",
  bodyCellProps
};

const purposeFormField = {
  id: "purpose__",
  enumerationKey: "returnPurpose",
  shouldNotBeInValue: true,
  formControlProps: {
    type: "select",
    attrs: { placeholder: "Выберите назначение возврата" },
    validators: [required],
    defaultValue: [1],
    value: [1]
  }
};

export const fieldsForTable = renderFormFields([
  brandField,
  articleField,
  nameField,
  countTableCell,
  returnTableCell,
  reasonTableCell,
  purposeTableCell
]);

export const fieldsForForm = renderFormFields([
  [
    { destructuring: true, id: "fields", name: null },
    [countFormField, returnFormField, reasonFormField, purposeFormField]
  ]
]);
