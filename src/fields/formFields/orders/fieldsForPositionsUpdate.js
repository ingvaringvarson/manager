import React from "react";
import { renderFormFields } from "../../helpers/index";
import FormField from "../../../components/common/formControls/FieldContext";

import {
  valueGreaterOrEqualThan,
  valueLessOrEqualThan,
  required
} from "../../../helpers/validators";

export const fields = renderFormFields([
  {
    id: "brand",
    title: "Товар",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.brand : "";
      }
    }
  },
  {
    id: "article",
    title: "",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.article : "";
      }
    }
  },
  {
    id: "name",
    title: "",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.name : "";
      }
    }
  },
  {
    id: "count",
    title: "Количество",
    bodyCellProps: {
      render: ({ entity }) => {
        return <FormField name={`count__${entity._id}`} />;
      }
    }
  },
  {
    id: "reject",
    title: "Отказаться от",
    bodyCellProps: {
      render: ({ entity }) => {
        return <FormField name={`reject__${entity._id}`} />;
      }
    }
  }
]);

export const formFields = renderFormFields([
  [
    { destructuring: true, id: "fields", name: null },
    [
      {
        id: "count__",
        formControlProps: {
          type: "text",
          attrs: {
            placeholder: "Количество",
            disabled: true
          }
        }
      },
      {
        id: "reject__",
        formControlProps: {
          type: "text",
          attrs: {
            placeholder: "Введите количество для отказа"
          }
        },
        afterMap: ({ field, fields, values }) => {
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              validators: [
                valueGreaterOrEqualThan(1),
                valueLessOrEqualThan(values.count),
                required
              ]
            }
          };
        }
      }
    ]
  ]
]);
