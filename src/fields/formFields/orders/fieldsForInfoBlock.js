import { mapClientCodeHeading } from "./../../helpers/mapMethods";
import { getValueInDepth } from "../../../utils";
import {
  mapEntitiesToGroupedList,
  renderInfoFields,
  mapValuesToFields,
  mapEntitiesToList,
  mapValuesToGroupedList
} from "../../helpers";
import { maskDateTime, maskDate } from "../../../helpers/masks";
import { fields } from "../clients/fieldsForEditForms";
import { fieldsForEditForm } from "./deliveryInfo";

const EntityRecord = {
  title: null,
  subtitle: "",
  formMain: null,
  formField: null,
  view: "default",
  infoFields: [],
  formFields: [],
  fieldsForAdd: [],
  mapValuesToFields
};

export const fieldGroups = {
  delivery_1: {
    ...EntityRecord,
    title: "Способ получения",
    subtitle: "Самовывоз",
    formMain: {
      subtitle: "Способ получения",
      title: "Редактирование",
      type: "edit",
      icon: "edit"
    },
    infoFields: renderInfoFields([
      {
        key: "warehouse_name",
        title: "Торговая точка"
      },
      {
        key: "min_date_delivery",
        title: "Ожид. дата получения",
        mask: maskDateTime
      }
    ]),
    formFields: fieldsForEditForm
  },
  delivery_2: {
    ...EntityRecord,
    title: "Способ получения",
    subtitle: "Доставка  Laf24",
    formMain: {
      subtitle: "Способ получения",
      title: "Редактирование",
      type: "edit",
      icon: "edit"
    },
    infoFields: renderInfoFields([
      {
        key: "warehouse_name",
        title: "Торговая точка"
      },
      {
        key: "min_date_delivery",
        title: "Ожид. дата получения",
        mask: maskDateTime
      },
      {
        key: "address",
        id: "location",
        title: "Адрес доставки заказа",
        mask: value => getValueInDepth(value, ["location"])
      },
      {
        key: "date_fact",
        title: "Дата фактической доставки",
        mask: maskDateTime
      },
      {
        key: "date_plan",
        title: "Планируемая дата доставки",
        mask: maskDate
      },
      {
        key: "period",
        title: "Период времени доставки",
        enumerationKey: "deliveryTemplate"
      },
      {
        key: "driver_name",
        title: "Водитель"
      }
    ]),
    formFields: fieldsForEditForm
  },
  delivery_3: {
    ...EntityRecord,
    title: "Способ получения",
    subtitle: "Доставка ТК",
    formMain: {
      subtitle: "Способ получения",
      title: "Редактирование",
      type: "edit",
      icon: "edit"
    },
    infoFields: renderInfoFields([
      {
        key: "warehouse_name",
        title: "Склад сборки"
      },
      {
        key: "min_date_delivery",
        title: "Ожид. дата получения",
        mask: maskDateTime
      },
      {
        key: "address",
        title: "Адрес доставки заказа",
        mask: value => getValueInDepth(value, ["location"])
      },
      {
        key: "date_fact",
        title: "Дата фактической доставки",
        mask: maskDateTime
      },
      {
        key: "date_plan",
        title: "Планируемая дата доставки",
        mask: maskDate
      },
      {
        key: "period",
        title: "Период времени доставки",
        enumerationKey: "deliveryTemplate"
      },
      {
        key: "driver_name",
        title: "Водитель"
      },
      {
        key: "delivery_company_name",
        title: "Транспортная компания"
      }
    ]),
    formFields: fieldsForEditForm
  },
  client: {
    ...EntityRecord,
    title: "Клиент",
    formMain: {
      icon: "account_box"
    },
    infoFields: renderInfoFields([
      {
        key: "client",
        id: "person_surname",
        title: "Фамилия",
        mask: value => getValueInDepth(value, ["person", "person_surname"])
      },
      {
        key: "client",
        id: "person_name",
        title: "Имя",
        mask: value => getValueInDepth(value, ["person", "person_name"])
      },
      {
        key: "client",
        id: "person_second_name",
        title: "Отчество",
        mask: value => getValueInDepth(value, ["person", "person_second_name"])
      },
      {
        key: "client",
        id: "tags",
        title: "Теги",
        enumerationKey: "agentTag",
        mask: value => getValueInDepth(value, ["tags"])
      },
      {
        key: "client",
        id: "segments",
        title: "Сегменты",
        enumerationKey: "agentSegment",
        mask: value => getValueInDepth(value, ["segments"])
      },
      {
        key: "order",
        title: "Аккаунт-менеджер",
        mask: value => getValueInDepth(value, ["agent_tech", "manager_name"])
      }
    ]),
    mapValuesToFields: mapClientCodeHeading
  },
  company: {
    ...EntityRecord,
    title: "Клиент",
    formMain: {
      icon: "account_box"
    },
    infoFields: renderInfoFields([
      {
        key: "client",
        id: "company_name_full",
        title: "Полное наименование",
        mask: value => getValueInDepth(value, ["company", "company_name_full"])
      },
      {
        key: "client",
        id: "company_name_short",
        title: "Сокращенное наименование",
        mask: value => getValueInDepth(value, ["company", "company_name_short"])
      },
      {
        key: "client",
        id: "tags",
        title: "Теги",
        enumerationKey: "agentTag",
        mask: value => getValueInDepth(value, ["tags"])
      },
      {
        key: "client",
        id: "segments",
        title: "Сегменты",
        enumerationKey: "agentSegment",
        mask: value => getValueInDepth(value, ["segments"])
      },
      {
        key: "order",
        title: "Аккаунт-менеджер",
        mask: value => getValueInDepth(value, ["agent_tech", "manager_name"])
      }
    ]),
    mapValuesToFields: mapClientCodeHeading
  },
  contacts: {
    ...EntityRecord,
    title: "Контакты",
    formMain: {
      subtitle: "Контакты",
      title: "Создание контакта",
      type: "add",
      icon: "add"
    },
    formField: {
      subtitle: "Контакт",
      title: "Редактирование",
      type: "edit"
    },
    view: "groupedList",
    mapValuesToFields: mapEntitiesToGroupedList("contactType"),
    formFields: fields["contacts"]
  },
  contact_persons: {
    ...EntityRecord,
    title: "Контактные лица",
    formField: {
      subtitle: "Контактные лица",
      title: "Редактирование",
      type: "edit"
    },
    view: "list",
    infoFields: renderInfoFields([
      {
        key: "name",
        title: "Имя"
      },
      {
        key: "gender",
        title: "Пол",
        enumerationKey: "personGender"
      },
      {
        key: "position",
        title: "Должность"
      },
      {
        key: "contacts",
        mapValueToFields: mapValuesToGroupedList("contactType"),
        view: "groupedList"
      }
    ]),
    mapValuesToFields: mapEntitiesToList,
    formFields: fields["contact_persons"]
  }
};
