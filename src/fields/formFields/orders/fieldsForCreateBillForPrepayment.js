import { renderFormFields } from "../../helpers";
import { valueGreaterThan, required } from "../../../helpers/validators";
import { maskOnlyNumbers } from "../../../helpers/masks";
import { getValueInDepth, getNumberWrappedWithRub } from "../../../utils";

export const fieldsForTable = renderFormFields([
  {
    id: "brand",
    title: "Товар",
    bodyCellProps: {
      render: ({ entity }) => getValueInDepth(entity, ["product", "brand"])
    }
  },
  {
    id: "article",
    bodyCellProps: {
      render: ({ entity }) => getValueInDepth(entity, ["product", "article"])
    }
  },
  {
    id: "name",
    bodyCellProps: {
      render: ({ entity }) => getValueInDepth(entity, ["product", "name"])
    }
  },
  {
    id: "price",
    title: "Цена",
    bodyCellProps: {
      render: ({ entity }) => getNumberWrappedWithRub(entity, ["price"])
    }
  },
  {
    id: "count",
    title: "Количество"
  },
  {
    id: "sum",
    title: "Сумма",
    bodyCellProps: {
      render: ({ entity }) => getNumberWrappedWithRub(entity, ["sum"])
    }
  }
]);

export const fieldsForForm = renderFormFields([
  {
    id: "fact_sum",
    formControlProps: {
      label: "Сумма счета",
      type: "text",
      mask: maskOnlyNumbers,
      attrs: { placeholder: "Введите сумму счета" },
      validators: [valueGreaterThan(0), required]
    }
  }
]);
