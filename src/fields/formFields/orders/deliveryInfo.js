import React from "react";
import { renderFormFields } from "../../helpers";
import { generateId, getValueInDepth } from "../../../utils";
import {
  requiredNotEarlierCurrentDate,
  required,
  requiredKladrId
} from "../../../helpers/validators";
import ListItem from "../../../components/common/list/ListItem";
import { RedText } from "../../../components/common/styled/text";
import FieldContext from "../../../components/common/formControls/FieldContext";
import { fieldStyle } from "../../../components/common/styled/form";
import deliveryPrice, {
  totalPriceByFreeDelivery
} from "../../../constants/deliveryPrice";
import { maskDateEndDay } from "../../../helpers/masks";
import Separator from "../../../designSystem/atoms/Separator";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForWarehouses
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

const afterChangeByDeliveryType = types => ({ field, values }) => {
  if (types.some(t => isDeliveryType(values, t))) {
    return {
      ...field,
      isActive: true
    };
  }

  return {
    ...field,
    isActive: false
  };
};

const afterMapByStateOrder = ({ field, values }) => {
  if (values.disabled) {
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        attrs: {
          ...field.formControlProps.attrs,
          iconClearProps: {
            hide: true
          },
          disabled: true
        }
      }
    };
  }

  return field;
};

const delivery_type = {
  id: "delivery_type",
  title: "Способ получения",
  enumerationKey: "deliveryType",
  formControlProps: {
    validators: [required],
    value: [],
    defaultValue: [],
    type: "select",
    label: "Выберите способ получения",
    attrs: {
      iconClearProps: {
        hide: true
      },
      placeholder: "Выберите способ получения"
    }
  },
  afterMap: afterMapByStateOrder
};

const isDeliveryType = (values, type) => {
  if (!values) return null;
  const value = getValueInDepth(values, ["delivery_type"]);

  return (
    (Array.isArray(value) && value.includes(String(type))) ||
    String(value) === String(type)
  );
};

const afterChangeByWarehouseId = ({ field, values }) => {
  if (isDeliveryType(values, 1)) {
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        label: "Выберите магазин",
        attrs: { placeholder: "Поиск по наименованию магазина" }
      }
    };
  }

  return {
    ...field,
    formControlProps: {
      ...field.formControlProps,
      label: "Выберите склад сборки",
      attrs: { placeholder: "Поиск по наименованию склада" }
    }
  };
};

const warehouse_id = {
  id: "warehouse_id",
  title: "Выберите магазин",
  formControlProps: {
    validators: [required],
    type: "select_search_request",
    value: [],
    defaultValue: [],
    label: "Выберите магазин",
    attrs: { placeholder: "Поиск по наименованию магазина" },
    searchRequestProps: searchRequestPropsForWarehouses({
      params: { type: [1, 3, 5] }
    }),
    afterChange: afterChangeByWarehouseId
  },
  afterMap: props => {
    const field = afterChangeByWarehouseId(props);

    return afterMapByStateOrder({ ...props, field });
  }
};

const warehouseIdByDeliveryBasket = {
  ...warehouse_id,
  afterMap: afterChangeByDeliveryType([1]),
  formControlProps: {
    ...warehouse_id.formControlProps,
    afterChange: afterChangeByDeliveryType([1])
  }
};

const min_date_delivery = {
  id: "min_date_delivery",
  title: "Выберите дату для изменения планируемой даты готовности",
  formControlProps: {
    type: "date",
    validators: [requiredNotEarlierCurrentDate],
    label: "Выберите дату для изменения планируемой даты готовности",
    attrs: {
      dateFormat: "dd.MM.yyyy HH:mm",
      placeholder: "Выберите дату и время"
    },
    datePickerProps: {
      dateFormat: "dd.MM.yyyy HH:mm",
      showTimeSelect: true,
      timeFormat: "HH:mm",
      timeIntervals: 15,
      timeCaption: "time"
    }
  },
  afterMap: afterMapByStateOrder
};

const note = {
  id: "note",
  formControlProps: {
    render: () => {
      return (
        <ListItem textMode active mb="20px" mt="10px">
          При изменении способа получения срок готовности товара может{" "}
          <RedText>увеличиться</RedText>. В заказе сохранятся данные о времени
          готовности <RedText>без учета</RedText> изменения способа получения.
        </ListItem>
      );
    }
  }
};

const kladr = {
  id: "kladr",
  formControlProps: {
    validators: [requiredKladrId],
    type: "kladr",
    label: "Укажите адрес, на который нужно оформить доставку",
    afterChange: props => {
      const { values, prevValues } = props;
      const copyField = afterChangeByDeliveryType([2])(props);
      const kladrValue = getValueInDepth(values, ["kladr"]);
      const addressValue = getValueInDepth(values, ["address_list"]);
      const prevAddressValue = getValueInDepth(prevValues, ["address_list"]);
      const selectedAddress =
        Array.isArray(addressValue) && addressValue[0] ? addressValue[0] : null;
      const prevSelectedAddress =
        Array.isArray(prevAddressValue) && prevAddressValue[0]
          ? prevAddressValue[0]
          : null;

      if (
        selectedAddress !== prevSelectedAddress &&
        selectedAddress !== kladrValue
      ) {
        return {
          ...copyField,
          formControlProps: {
            ...copyField.formControlProps,
            attrs: { ...copyField.formControlProps.attrs, key: generateId() },
            value: selectedAddress
          },
          values: { ...values, [copyField.id]: selectedAddress }
        };
      }

      return copyField;
    }
  },
  afterMap: ({ field, values, ...restProps }) => {
    const copyField = afterChangeByDeliveryType([2])({
      field,
      values,
      ...restProps
    });
    const kladrId = getValueInDepth(values, ["address", "kladr"]);

    if (kladrId) {
      return {
        ...copyField,
        formControlProps: {
          ...copyField.formControlProps,
          value: kladrId
        }
      };
    }

    return afterMapByStateOrder({ ...restProps, values, field: copyField });
  }
};

const afterChangeByLocation = ({ field, values }, location) => {
  if (isDeliveryType(values, 2)) {
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        value: location
      },
      isActive: true
    };
  }

  return {
    ...field,
    isActive: false
  };
};

const location = {
  id: "location",
  title: "Адрес доставки",
  isActive: false,
  formControlProps: {
    label: "Адрес доставки",
    attrs: {
      readOnly: true,
      iconClearProps: {
        hide: true
      }
    },
    afterChange: ({ field, values }) => {
      const location = getValueInDepth(values, ["location"]);
      const kladr = getValueInDepth(values, ["kladr"]);

      return afterChangeByLocation({ field, values }, location, kladr);
    }
  },
  afterMap: ({ field, values, ...props }) => {
    const location = getValueInDepth(values, ["address", "location"]);
    const kladr = getValueInDepth(values, ["address", "kladr"]);

    return afterMapByStateOrder({
      ...props,
      values,
      field: afterChangeByLocation({ field, values }, location, kladr)
    });
  }
};

const addressTypeByKladr = [5, 6];

const address_list = {
  id: "address_list",
  title: "Выберите адрес доставки клиента",
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select",
    label: "Выберите адрес доставки клиента",
    attrs: {
      iconClearProps: {
        hide: true
      },
      placeholder: "Выберите адрес доставки клиента"
    },
    afterChange: ({ field, values }) => {
      const copyField = afterChangeByDeliveryType([2])({ field, values });

      return {
        ...copyField,
        isActive:
          copyField.isActive && !!copyField.formControlProps.options.length
      };
    },
    render: ({ field }) => {
      return (
        <>
          <Separator text="или" mb="10px" />
          <FieldContext
            fieldStyle={fieldStyle}
            name={field.formControlProps.name}
          />
        </>
      );
    }
  },
  afterMap: ({ field, values, ...props }) => {
    const copyField = afterChangeByDeliveryType([2])({ field, values });
    const addressList = getValueInDepth(values, ["client", "addresses"]);
    const addressListByKladr = addressList
      ? addressList.filter(
          i => i.kladr && i.kladr !== "0" && addressTypeByKladr.includes(i.type)
        )
      : [];
    const options = addressListByKladr.map(i => ({
      id: i.kladr,
      name: i.location
    }));

    const newField = {
      ...copyField,
      formControlProps: {
        ...copyField.formControlProps,
        options
      },
      isActive: copyField.isActive && !!options.length
    };

    return afterMapByStateOrder({ ...props, values, field: newField });
  }
};

const date_plan = {
  id: "date_plan",
  formControlProps: {
    type: "date",
    mask: maskDateEndDay,
    validators: [requiredNotEarlierCurrentDate],
    attrs: {
      placeholder: "Выберите дату"
    },
    afterChange: afterChangeByDeliveryType([2, 3])
  },
  afterMap: props => {
    const field = afterChangeByDeliveryType([2, 3])(props);

    return afterMapByStateOrder({ ...props, field });
  }
};

const period = {
  id: "period",
  enumerationKey: "deliveryTemplate",
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select",
    attrs: {
      iconClearProps: {
        hide: true
      },
      placeholder: "Выберите период доставки"
    },
    afterChange: afterChangeByDeliveryType([2, 3])
  },
  afterMap: props => {
    const field = afterChangeByDeliveryType([2, 3])(props);

    return afterMapByStateOrder({ ...props, field });
  }
};

const date_fact = {
  id: "date_fact",
  title: "Выберите фактическую дату доставки",
  formControlProps: {
    type: "date",
    validators: [requiredNotEarlierCurrentDate],
    label: "Выберите фактическую дату доставки",
    attrs: {
      dateFormat: "dd.MM.yyyy HH:mm",
      placeholder: "Выберите дату и время"
    },
    datePickerProps: {
      dateFormat: "dd.MM.yyyy HH:mm",
      showTimeSelect: true,
      timeFormat: "HH:mm",
      timeIntervals: 15,
      timeCaption: "time"
    },
    afterChange: afterChangeByDeliveryType([2, 3])
  },
  afterMap: props => {
    const field = afterChangeByDeliveryType([2, 3])(props);

    return afterMapByStateOrder({ ...props, field });
  }
};

const driver_id = {
  id: "driver_id",
  title: "Выберите водителя",
  formControlProps: {
    label: "Выберите водителя",
    type: "select_search_request",
    value: [],
    defaultValue: [],
    attrs: { placeholder: "Поиск по имени водителя" },
    searchRequestProps: searchRequestPropsForAgents({
      params: {
        roles: [5]
      }
    }),
    afterChange: afterChangeByDeliveryType([2, 3])
  },
  afterMap: props => {
    const copyField = afterChangeByDeliveryType([2, 3])(props);
    const id = getValueInDepth(props.values, ["client", "employees", 0, "id"]);

    const newField = {
      ...copyField,
      formControlProps: {
        ...copyField.formControlProps,
        value:
          id && !props.values.hasOwnProperty(copyField.id)
            ? [id]
            : copyField.formControlProps.value
      }
    };

    return afterMapByStateOrder({ ...props, field: newField });
  }
};

const cdek = {
  id: "codedc",
  formControlProps: {
    validators: [required],
    type: "cdek",
    afterChange: afterChangeByDeliveryType([3])
  },
  afterMap: props => {
    const field = afterChangeByDeliveryType([3])(props);

    return afterMapByStateOrder({ ...props, field });
  }
};

const location_cdek = {
  id: "location_cdek",
  isActive: false
};

const delivery_price = {
  id: "delivery_price",
  formControlProps: {
    type: "select",
    validators: [required],
    afterChange: afterChangeByDeliveryType([2])
  },
  afterMap: props => {
    const copyField = afterChangeByDeliveryType([2])(props);
    const agent_type = getValueInDepth(props.values, ["order", "agent_type"]);
    const positions = getValueInDepth(props.values, ["order", "positions"]);
    const deliveryPosition = positions && positions.find(p => p.type === 5);
    let value = [deliveryPrice.info.delivery_min_3000.id];

    if (
      isDeliveryType(props.value, [2, 3]) &&
      deliveryPosition &&
      deliveryPosition.product
    ) {
      value = [deliveryPosition.product.article];
    } else if (agent_type === 2) {
      value = [deliveryPrice.info.delivery_b2b.id];
    } else {
      const totalPrice = positions
        ? positions.reduce((total, p) => total + p.sum, 0)
        : 0;
      if (totalPrice > totalPriceByFreeDelivery) {
        value = [deliveryPrice.info.delivery_max_3000.id];
      }
    }

    const newField = {
      ...copyField,
      formControlProps: {
        ...copyField.formControlProps,
        value,
        options: deliveryPrice.list
      }
    };

    return afterMapByStateOrder({ ...props, field: newField });
  }
};

const delivery_company = {
  id: "delivery_company",
  isActive: false
};

const delivery_company_name = {
  id: "delivery_company_name",
  isActive: false
};

const contract_id = {
  id: "contract_id",
  type: "select",
  title: "Договор",
  enumerationKey: "contracts",
  formControlProps: {
    type: "select",
    attrs: { placeholder: "Выберите договор" },
    afterChange: props => {
      const copyField = afterChangeByDeliveryType([1])(props);
      const sizeOptions = getValueInDepth(copyField, [
        "formControlProps",
        "options",
        "length"
      ]);
      return {
        ...copyField,
        isActive: !!sizeOptions
      };
    }
  },
  afterMap: props => {
    const copyField = afterChangeByDeliveryType([1])(props);
    const sizeOptions = getValueInDepth(props, [
      "values",
      "contracts",
      "length"
    ]);

    return {
      ...copyField,
      isActive: !!sizeOptions
    };
  }
};

export const fieldsForEditForm = renderFormFields([
  delivery_type,
  location,
  cdek,
  location_cdek,
  kladr,
  address_list,
  [
    {
      id: null,
      name: "Выберите дату и период доставки",
      destructuring: false,
      separator: " "
    },
    [date_plan, period]
  ],
  delivery_price,
  date_fact,
  driver_id,
  warehouse_id,
  note,
  min_date_delivery,
  delivery_company,
  delivery_company_name
]);

export const fieldsForDeliveryBasket = renderFormFields([
  delivery_type,
  location,
  cdek,
  location_cdek,
  kladr,
  address_list,
  [
    {
      id: null,
      name: "Выберите дату и период доставки",
      destructuring: false,
      separator: " "
    },
    [date_plan, period]
  ],
  warehouseIdByDeliveryBasket
]);

export const fieldsForContractBasket = renderFormFields([contract_id]);
