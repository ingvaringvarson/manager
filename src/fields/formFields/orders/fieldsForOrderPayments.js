import { renderDateTime, renderFormFields } from "../../helpers/index";

export const fields = {
  code: {
    id: "code",
    title: "Номер",
    headCellProps: { sortBy: true },
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Поиск по номеру"
      }
    }
  },
  payment_state: {
    id: "payment_state",
    title: "Тип транзакции",
    enumerationKey: "paymentState",
    formControlProps: {
      type: "select",
      attrs: {
        placeholder: "Тип транзакции"
      }
    }
  },
  store_id: {
    id: "store_id",
    title: "Торговая точка",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.store_name;
      }
    },
    formControlProps: {
      type: "text",
      attrs: {
        disabled: true
      }
    }
  },
  sum: {
    id: "sum",
    title: "Сумма",
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Поиск по сумме"
      }
    }
  },
  object_date_create: {
    id: "object_date_create",
    title: "Дата создания",
    bodyCellProps: {
      render: renderDateTime("object_date_create")
    },
    formControlProps: {
      type: "date",
      attrs: {
        placeholder: "Поиск по дате создания"
      }
    }
  },
  receiptAmount: {
    id: "receiptAmount",
    title: "Свитованные счета",
    formControlProps: {
      type: "text",
      attrs: {
        disabled: true
      }
    }
  },
  receiptSum: {
    id: "receiptSum",
    title: "Сумма квитования",
    formControlProps: {
      type: "text",
      attrs: {
        disabled: true
      }
    }
  },
  firm_id: {
    id: "firm_id",
    title: "Организация",
    formControlProps: {
      type: "text",
      attrs: {
        disabled: true
      }
    }
  },
  receipt_date: {
    id: "receipt_date",
    title: "Дата поступления",
    bodyCellProps: {
      render: renderDateTime("receipt_date", false)
    },
    formControlProps: {
      type: "date",
      attrs: {
        placeholder: "Поиск по дате поступления"
      }
    }
  },
  purpose: {
    id: "purpose",
    title: "Назначение",
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Поиск по назначению"
      }
    }
  },
  comment: {
    id: "comment",
    title: "Комментарий",
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Поиск по комментарию"
      }
    }
  }
};

export const fieldsForPaymentMethods = {
  1: renderFormFields([
    fields.code,
    fields.payment_state,
    fields.store_id,
    fields.sum,
    fields.object_date_create,
    fields.receiptAmount,
    fields.receiptSum
  ]),
  2: renderFormFields([
    fields.code,
    fields.payment_state,
    fields.store_id,
    fields.sum,
    fields.object_date_create,
    fields.receiptAmount,
    fields.receiptSum
  ]),
  3: renderFormFields([
    fields.code,
    fields.payment_state,
    fields.firm_id,
    fields.receipt_date,
    fields.sum,
    fields.purpose,
    fields.object_date_create,
    fields.receiptAmount,
    fields.comment
  ])
};
