import React from "react";
import { Link } from "react-router-dom";
import { getAgentName, getIdForEntity, renderFormFields } from "../../helpers";
import { required } from "../../../helpers/validators";
import { generateId } from "../../../utils";
import { getUrl } from "../../../helpers/urls";
import { searchRequestPropsForAgents } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

export function discountGroupsValidator(c1, c2) {
  if (!c1 || !c2) return "Не определена ценовая группа клиента";

  return c1.discountGroupId === c2.discountGroupId
    ? null
    : `У клиента в заказе и выбранного клиента разные группы цен. Выберите клиента с группой "${c1.name}"`;
}

const renderContractError = client => {
  const clientUrl = `${getUrl("clients", { id: client.id })}?_tab=5`;
  return (
    <>
      У клиента <Link to={clientUrl}>№{client.code} </Link>
      нет договора с фирмой, на которую оформлен заказ. Создайте договор или
      выберите другого клиента
    </>
  );
};

const contractValidator = (contract, order) =>
  contract.state === 2 &&
  contract.type === 1 &&
  contract.executor_id === order.firm_id;

export const newAgentContractsValidator = (selectedClient, order) => {
  if (!selectedClient) return "Значения не были получены";

  return Array.isArray(selectedClient.contracts)
    ? selectedClient.contracts.some(c => contractValidator(c, order))
      ? null
      : renderContractError(selectedClient)
    : renderContractError(selectedClient);
};

export const fields = renderFormFields([
  {
    id: "new_agent_id",
    formControlProps: {
      type: "select_search_request",
      label: "Новый клиент",
      validators: [required],
      attrs: { placeholder: "Введите код клиента" },
      searchRequestProps: searchRequestPropsForAgents({
        keyById: "id",
        key: "code",
        getName: getAgentName,
        getId: getIdForEntity
      })
    },
    afterMap: ({ field, values }) => {
      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          validators: [required],
          options: values.selectedClientOptions,
          value: values.selectedClient && [values.selectedClient.id]
        }
      };
    }
  },
  {
    id: "contract_id",
    formControlProps: {
      type: "select",
      label: "Договор",
      validators: [required],
      options: [],
      attrs: { placeholder: "Выберите договор" }
    },
    afterMap: ({ field, values }) => {
      const options =
        values.selectedClient && Array.isArray(values.selectedClient.contracts)
          ? values.selectedClient.contracts
              .filter(c => contractValidator(c, values.order))
              .map(c => ({
                id: c.id,
                name: c.number
              }))
          : [];

      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          options
        }
      };
    }
  }
]);
