import { renderFormFields } from "../../helpers";

import {
  required,
  valueGreaterThan,
  maxNumericDiscountValue
} from "../../../helpers/validators";

export const fields = renderFormFields([
  {
    id: "discount",
    formControlProps: {
      type: "text",
      attrs: {
        placeholder: "Скидка"
      }
    },
    afterMap: ({ field, fields, values }) => {
      const maxSum = values.sum_total || 0;
      const currentDiscount = values.positions
        ? values.positions.reduce((result, pos) => {
            const curSum = +pos.discount || 0;
            return result + curSum;
          }, 0)
        : 0;
      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          value: currentDiscount || null,
          validators: [
            required,
            valueGreaterThan(0),
            maxNumericDiscountValue(maxSum, 100, "discountValue")
          ]
        }
      };
    }
  },
  {
    id: "discountValue",
    formControlProps: {
      type: "select",
      options: [
        {
          id: "rub",
          name: "руб."
        },
        {
          id: "percent",
          name: "%"
        }
      ],
      value: "rub",
      attrs: {
        placeholder: ""
      }
    }
  }
]);
