import {
  isRequiredDateValue,
  minDateRange,
  maxDateRange
} from "./../../../helpers/validators";
import { renderFormFields } from "../../helpers";

export const fields = renderFormFields([
  {
    id: "minReserveDate",
    title: "",
    isActive: false
  },
  {
    id: "maxReserveDate",
    title: "",
    isActive: false
  },
  {
    id: "current_reserve_time",
    title: "",
    formControlProps: {
      type: "date",
      label: "Текущая дата резерва",
      attrs: {
        dateFormat: "dd.MM.yyyy",
        disabled: true,
        iconClearProps: {
          hide: true
        }
      }
    }
  },
  {
    id: "reserve_time__",
    title: "",
    formControlProps: {
      type: "date",
      validators: [
        isRequiredDateValue,
        minDateRange("minReserveDate"),
        maxDateRange("maxReserveDate")
      ],
      label: "Новая дата резерва",
      attrs: {
        dateFormat: "dd.MM.yyyy HH:mm:ss",
        placeholder: "Выберите новую дату резерва",
        iconClearProps: {
          hide: true
        }
      }
    },
    afterMap: ({ field, fields, values }) => {
      const currentReserveTime = new Date(values.current_reserve_time);
      const openToDate = currentReserveTime.getTime()
        ? currentReserveTime
        : null;
      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          datePickerProps: {
            dateFormat: "dd.MM.yyyy",
            openToDate
          }
        }
      };
    }
  }
]);
