import { renderFormFields, renderSum } from "../../helpers/index";

export const fields = renderFormFields([
  {
    id: "code",
    title: "Номер",
    formControlProps: {
      type: "text",
      attrs: { placeholder: "Поиск по номеру" }
    }
  },
  {
    id: "state",
    title: "Статус",
    enumerationKey: "billState",
    formControlProps: {
      type: "select",
      attrs: {
        placeholder: "Поиск по статусу",
        disabled: true,
        iconClearProps: {
          hide: true
        }
      }
    }
  },
  {
    id: "type",
    title: "Тип счета",
    enumerationKey: "billType",
    formControlProps: {
      type: "select",
      attrs: { placeholder: "Поиск по типу счета" }
    }
  },
  {
    id: "bill_sum",
    title: "Сумма счета",
    formControlProps: {
      type: "text",
      attrs: { placeholder: "Поиск по сумме счета", disabled: true }
    },
    bodyCellProps: {
      render: renderSum("bill_sum")
    }
  },
  {
    id: "paid",
    title: "Оплачено",
    bodyCellProps: {
      render: ({ entity }) => {
        const value = entity.bill_sum - entity.receipt_sum;
        return value || value === 0 ? `${value} руб.` : "";
      }
    },
    formControlProps: {
      type: "text",
      attrs: { placeholder: "Поиск по оплате", disabled: true }
    }
  },
  {
    id: "receipt_sum",
    title: "Сумма к оплате",
    bodyCellProps: {
      render: ({ entity, field }) => {
        return entity[field.id] ? `${entity[field.id]} руб.` : "";
      }
    },
    formControlProps: {
      type: "text",
      attrs: { placeholder: "Поиск по сумме оплаты", disabled: true }
    }
  },
  {
    id: "comment",
    title: "Комментарий",
    formControlProps: {
      type: "text",
      attrs: { placeholder: "Поиск по комментарию", disabled: true }
    }
  }
]);
