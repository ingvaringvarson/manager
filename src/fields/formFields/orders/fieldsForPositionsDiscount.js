import React from "react";
import { renderFormFields } from "../../helpers/index";
import FormField from "../../../components/common/formControls/FieldContext";

import {
  required,
  valueGreaterThan,
  maxNumericDiscountValue
} from "../../../helpers/validators";

export const fields = renderFormFields([
  {
    id: "brand",
    title: "Товар",
    headCellProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.brand : "";
      }
    }
  },
  {
    id: "article",
    title: "Артикул",
    headCellProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.article : "";
      }
    }
  },
  {
    id: "name",
    title: "Имя товара",
    headCellProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.name : "";
      }
    }
  },
  {
    id: "price",
    title: "Цена",
    headCellProps: { sortBy: true }
  },
  {
    id: "discount",
    title: "Значение скидки",
    headCellProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return <FormField name={`discount__${entity._id}`} />;
      }
    }
  },
  {
    id: "discountValue",
    title: "Тип скидки",
    headCellProps: { sortBy: true },
    bodyCellProps: {
      render: ({ entity }) => {
        return <FormField name={`discountValue__${entity._id}`} />;
      }
    }
  }
]);

export const formFields = renderFormFields([
  [
    { destructuring: true, id: "fields", name: null },
    [
      {
        id: "discount__",
        formControlProps: {
          type: "text",
          attrs: {
            placeholder: "Скидка"
          }
        },
        afterMap: ({ field, fields, values }) => {
          const maxValue = values.price;
          const [_, idEntity] = field.id.split("__");
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              value: values.discount,
              validators: [
                required,
                valueGreaterThan(0),
                maxNumericDiscountValue(
                  maxValue,
                  100,
                  `discountValue__${idEntity}`
                )
              ]
            }
          };
        }
      },
      {
        id: "discountValue__",
        shouldNotBeInValue: true,
        formControlProps: {
          type: "select",
          options: [
            {
              id: "rub",
              name: "руб."
            },
            {
              id: "percent",
              name: "%"
            }
          ],
          attrs: {
            placeholder: ""
          },
          value: "rub"
        }
      }
    ]
  ]
]);
