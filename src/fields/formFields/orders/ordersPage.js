import React from "react";

import {
  renderFormFields,
  renderOrderState,
  renderDeliveryType,
  maxDateAmongPositions,
  renderDateTime,
  disableFieldHandler,
  forUserAgentCodeHandler,
  objectIdCreateForUserHandler,
  getNameCompanyShort,
  getIdForEntity
} from "../../helpers";
import {
  maskDateBeginningDay,
  maskDateEndDay,
  maskOnlyNumbers,
  maskOnlyText
} from "../../../helpers/masks";
import { maxDateRange, minDateRange } from "../../../helpers/validators";
import { sortByField } from "../../templates/serviceTemplates";
import { generateId, getValueInDepth } from "../../../utils";
import { renderOrderPositionState } from "../../templates/tableCellTemplates/positionFields";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForWarehouses
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

import Title from "../../../designSystem/atoms/Title";

export const headlines = [
  {
    id: generateId(),
    flex: "1 1 80px",
    render: () => ""
  },
  {
    id: generateId(),
    flex: "3 1 0",
    render: () => <Title>Клиент</Title>
  },
  {
    id: generateId(),
    flex: "7 1 0",
    render: () => <Title>Заказ</Title>
  },
  {
    id: generateId(),
    flex: "1 1 30px",
    render: () => ""
  }
];

const agentCodeField = {
  id: "agent_code",
  title: "ID клиента",
  headCellProps: {
    sortBy: true,
    sortName: "orders_with_bills.agent_tech.code"
  },
  formControlProps: {
    mask: maskOnlyNumbers,
    type: "text",
    attrs: { placeholder: "Введите ID клиента" }
  },
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["agent_tech", "code"]);
    }
  }
};
const fullNameField = {
  id: "full_name",
  title: "Наименование",
  formControlProps: {
    type: "text",
    attrs: { placeholder: "Введите наименование" }
  },
  bodyCellProps: {
    render: ({ entity }) => {
      const companyNameFull = getValueInDepth(entity, [
        "agent_tech",
        "company_name_full"
      ]);
      const personName = getValueInDepth(entity, ["agent_tech", "person_name"]);

      return `${companyNameFull ? companyNameFull : ""}\n${
        personName ? personName : ""
      }`;
    }
  }
};
const phoneField = {
  id: "phone",
  title: "Телефон",
  queryKeys: ["_contacts_1", "_contacts_7"],
  bodyCellProps: {
    render: ({ entity }) => {
      if (
        !entity.agent_tech ||
        !entity.agent_tech.contacts ||
        !entity.agent_tech.contacts.length
      )
        return null;

      return entity.agent_tech.contacts.reduce((str, contact) => {
        if (contact.type === 1 || contact.type === 7) {
          if (str) {
            return `${str},\n${contact.value}`;
          } else {
            return `${contact.value}`;
          }
        }

        return str;
      }, "");
    }
  },
  formControlProps: {
    mask: maskOnlyNumbers,
    type: "text",
    attrs: {
      placeholder: "Поиск по телефону"
    }
  }
};
const codeField = {
  id: "code",
  title: "Номер",
  headCellProps: {
    sortBy: true
  },
  formControlProps: {
    mask: maskOnlyText,
    type: "text",
    attrs: { placeholder: "Введите номер" }
  }
};
const stateField = {
  id: "state",
  title: "Статус",
  bodyCellProps: {
    render: renderOrderState("orderState", "state")
  },
  enumerationKey: "orderState",
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select",
    attrs: { placeholder: "Выберите статус" }
  }
};
const deliveryTypeField = {
  id: "delivery_type",
  title: "Способ получения",
  enumerationKey: "deliveryType",
  bodyCellProps: {
    render: renderDeliveryType
  },
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select",
    attrs: { placeholder: "Выберите способ получения" }
  }
};
const estimatedTime = {
  id: "estimated_time",
  title: "Ожидаемая дата готовности",
  queryKeys: ["estimated_time_start", "estimated_time_end"],
  bodyCellProps: {
    render: maxDateAmongPositions("estimated_time")
  },
  headCellProps: {
    sortBy: true,
    sortName: "orders_with_bills.order.positions.estimated_time"
  },
  formControlProps: {
    type: "date",
    attrs: { placeholder: "Введите дату готовности" }
  }
};
const reserveTime = {
  id: "reserve_time",
  title: "Время резерва",
  queryKeys: ["reserve_time_start", "reserve_time_end"],
  bodyCellProps: {
    render: maxDateAmongPositions("reserve_time")
  },
  headCellProps: {
    sortBy: true,
    sortName: "orders_with_bills.order.positions.reserve_time"
  },
  formControlProps: {
    type: "date",
    attrs: { placeholder: "Введите время резерва" }
  }
};
const objectDateCreate = {
  id: "object_date_create",
  title: "Дата создания",
  queryKeys: ["datefrom_created", "dateto_created"],
  bodyCellProps: {
    render: renderDateTime("object_date_create")
  },
  headCellProps: {
    sortBy: true
  },
  formControlProps: {
    type: "date",
    attrs: { placeholder: "Введите дату создания" }
  }
};
const objectIdCreate = {
  id: "object_id_create",
  title: "Автор",
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.object_name_create || "";
    }
  },
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Введите наименование агента" },
    searchRequestProps: searchRequestPropsForAgents({
      params: {
        roles: [5]
      }
    }),
    afterChange: forUserAgentCodeHandler
  },
  afterMap: forUserAgentCodeHandler
};
const sumTotalTableField = {
  id: "sum_total",
  title: "Cумма",
  bodyCellProps: {
    render: ({ entity }) => entity.sum_total || 0
  },
  formControlProps: {
    attrs: { disabled: true }
  }
};
const commentField = {
  id: "comment",
  title: "Комментарий",
  bodyCellProps: {
    render: ({ entity }) => entity.comment || ""
  },
  formControlProps: {
    attrs: { disabled: true }
  }
};

export const fieldsForTable = renderFormFields(
  [
    sortByField,
    agentCodeField,
    fullNameField,
    phoneField,
    codeField,
    stateField,
    deliveryTypeField,
    estimatedTime,
    reserveTime,
    objectDateCreate,
    objectIdCreate,
    sumTotalTableField,
    commentField
  ],
  true
);

const productFieldPosition = {
  id: "product",
  title: "Товар",
  bodyCellProps: {
    render: ({ entity }) => {
      if (!entity.product) return null;

      return `${entity.product.article} ${entity.product.brand} ${entity.product.name}`;
    }
  }
};
const goodStateFieldPosition = {
  id: "good_state",
  title: "Статус",
  bodyCellProps: {
    render: renderOrderPositionState
  }
};
const countFieldPosition = {
  id: "count",
  title: "Количество"
};
const priceFieldPosition = {
  id: "price",
  title: "Цена"
};
const discountFieldPosition = {
  id: "discount",
  title: "Скидка"
};
const estimatedTimeFieldPosition = {
  id: "estimated_time",
  title: "Ожидаемая дата готовности",
  bodyCellProps: {
    render: renderDateTime("estimated_time")
  }
};
const supplierNameFieldPosition = {
  id: "supplier_name",
  title: "Поставщик"
};

export const fieldsForPositions = renderFormFields([
  productFieldPosition,
  goodStateFieldPosition,
  countFieldPosition,
  priceFieldPosition,
  discountFieldPosition,
  estimatedTimeFieldPosition,
  supplierNameFieldPosition
]);

const dateFromCreatedField = {
  id: "datefrom_created",
  formControlProps: {
    validators: [maxDateRange("dateto_created")],
    mask: maskDateBeginningDay,
    type: "date",
    attrs: { placeholder: "Выберите дату" }
  }
};
const dateToCreatedField = {
  id: "dateto_created",
  formControlProps: {
    validators: [minDateRange("datefrom_created")],
    mask: maskDateEndDay,
    type: "date",
    attrs: { placeholder: "Выберите дату" }
  }
};
const datePlanStartField = {
  id: "date_plan_start",
  formControlProps: {
    validators: [maxDateRange("date_plan_end")],
    mask: maskDateBeginningDay,
    type: "date",
    attrs: { placeholder: "Выберите дату" }
  }
};
const datePlanEndField = {
  id: "date_plan_end",
  formControlProps: {
    type: "date",
    mask: maskDateEndDay,
    validators: [minDateRange("date_plan_end")],
    attrs: { placeholder: "Выберите дату" }
  }
};
const firmIdField = {
  id: "firm_id",
  title: "Фирма",
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.firm_name || "";
    }
  },
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Введите название организации" },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      getName: getNameCompanyShort,
      getId: getIdForEntity,
      params: {
        roles: [2]
      }
    })
  }
};
const warehouseIdField = {
  id: "warehouse_id",
  title: "Выберите склад отгрузки",
  formControlProps: {
    label: "Склад отгрузки",
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Выберите склад отгрузки" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};
const articleField = {
  id: "article",
  title: "Введите артикул",
  formControlProps: {
    label: "Артикул",
    mask: maskOnlyText,
    type: "text",
    name: "article",
    attrs: { placeholder: "Введите артикул" }
  }
};
const _agentCodeField = {
  id: "_agent_code",
  title: "Введите код создателя заказа",
  mask: maskOnlyNumbers,
  formControlProps: {
    label: "Создатель заказа",
    type: "text",
    attrs: { placeholder: "Введите код создателя заказа" },
    afterChange: objectIdCreateForUserHandler
  },
  afterMap: objectIdCreateForUserHandler
};
const _forUserField = {
  id: "_for_user",
  title: "Мной созданные заказы",
  formControlProps: {
    type: "checkbox",
    valuesByChecked: new Map([[false, ""], [true, "1"]]),
    checkedByValue: new Map([["", false], ["1", true]]),
    label: "Мной созданные заказы",
    afterChange: ({ field, values }) => {
      if (
        (values.hasOwnProperty("object_id_create") &&
          values.object_id_create.length) ||
        (values.hasOwnProperty("_agent_code") && values._for_user !== "")
      ) {
        return disableFieldHandler(field);
      }

      return field;
    }
  },
  afterMap: ({ field, values }) => {
    if (
      (values.hasOwnProperty("object_id_create") &&
        values.object_id_create.length) ||
      (values.hasOwnProperty("_agent_code") && values._for_user !== "")
    ) {
      return disableFieldHandler(field);
    }

    return field;
  }
};
const stateListField = {
  id: "state_list",
  title: "Поиск по статусу заказа",
  queryKeys: ["state"],
  enumerationKey: "orderState",
  formControlProps: {
    label: "Статус заказа",
    isMultiple: true,
    value: [],
    defaultValue: [],
    type: "select",
    attrs: { placeholder: "Выберите статус заказа" }
  }
};
const goodStateListField = {
  id: "good_state_list",
  title: "Поиск по статусу позиции",
  queryKeys: ["good_state"],
  enumerationKey: "orderPositionGoodState",
  formControlProps: {
    label: "Статус позиции заказа",
    isMultiple: true,
    value: [],
    defaultValue: [],
    type: "select",
    attrs: { placeholder: "Выберите статус позиции" }
  },
  afterMap: ({ values, field, enumerations }) => {
    const enums = [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      16,
      17,
      18,
      29
    ];
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        options: enumerations[field.enumerationKey].list.filter(f =>
          enums.includes(f.id)
        )
      }
    };
  }
};

export const fieldsForFilters = renderFormFields(
  [
    [
      { id: null, name: "Поиск по дате создания", destructuring: false },
      [dateFromCreatedField, dateToCreatedField]
    ],
    [
      { id: null, name: "Поиск по дате доставки", destructuring: false },
      [datePlanStartField, datePlanEndField]
    ],
    {
      ...firmIdField,
      formControlProps: {
        label: "Организация",
        ...firmIdField.formControlProps
      }
    },
    warehouseIdField,
    articleField,
    _agentCodeField,
    _forUserField,
    stateListField,
    goodStateListField
  ],
  true
);

const sumTotalField = {
  id: "sum_total",
  title: "Сумма заказа",
  formControlProps: {
    attrs: { disabled: true },
    type: "text"
  }
};

const sumPrePaidField = {
  id: "sum_pre_paid",
  title: "Сумма предоплаты",
  formControlProps: {
    attrs: { disabled: true },
    type: "text"
  }
};

const sumPaidField = {
  id: "sum_paid",
  title: "Сумма к оплате",
  formControlProps: {
    attrs: { disabled: true },
    type: "text"
  }
};

export const fieldsForTableByTab = renderFormFields(
  [
    codeField,
    stateField,
    estimatedTime,
    deliveryTypeField,
    sumTotalField,
    sumPrePaidField,
    sumPaidField,
    reserveTime,
    objectDateCreate,
    objectIdCreate,
    firmIdField,
    sortByField
  ],
  true
);
