import { renderFormFields, renderDateTime } from "../../helpers";
import { getValueInDepth } from "../../../utils";
import { renderOrderPositionState } from "../../templates/tableCellTemplates/positionFields";

export const fields = renderFormFields([
  {
    id: "brand",
    title: "Товар",
    bodyCellProps: {
      render: ({ entity }) => {
        return getValueInDepth(entity, ["product", "brand"]);
      }
    }
  },
  {
    id: "article",
    title: "",
    bodyCellProps: {
      render: ({ entity }) => {
        return getValueInDepth(entity, ["product", "article"]);
      }
    }
  },
  {
    id: "name",
    title: "",
    bodyCellProps: {
      render: ({ entity }) => {
        return getValueInDepth(entity, ["product", "name"]);
      }
    }
  },
  {
    id: "good_state",
    title: "Статус",
    bodyCellProps: {
      render: renderOrderPositionState
    }
  },
  {
    id: "price",
    title: "Цена"
  },
  {
    id: "discount_percent",
    title: "Скидка %"
  },
  {
    id: "discount",
    title: "Скидка руб."
  },
  {
    id: "count",
    title: "Кол-во"
  },
  {
    id: "sum",
    title: "Сумма"
  },
  {
    id: "prepaid_sum",
    title: "Предоплата"
  },
  {
    id: "estimated_time",
    title: "Ожид. срок доставки",
    bodyCellProps: {
      render: renderDateTime("estimated_time")
    }
  },
  {
    id: "warehouse_name",
    title: "Склад"
  },
  {
    id: "supplier_name",
    title: "Поставщик"
  },
  {
    id: "pricelist_code",
    title: "Прайс-лист"
  }
]);
