import {
  renderFormFields,
  renderEnumerationValue,
  renderSum,
  renderDateTime
} from "../../helpers/index";
import {
  brandField,
  articleField,
  nameField,
  statusField,
  priceField,
  countField,
  sumField,
  purposeField,
  reasonField
} from "../../templates/tableCellTemplates/positionFields";
import { getValueInDepth } from "../../../utils";

const returnBrandField = {
  id: "brand",
  title: "Товар",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["position", "product", "brand"]);
    }
  }
};

const returnArticleField = {
  id: "article",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["position", "product", "article"]);
    }
  }
};

const returnNameField = {
  id: "name",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["position", "product", "name"]);
    }
  }
};

const returnGoodStateField = {
  id: "good_state",
  title: "Статус",
  enumerationKey: "orderPositionGoodState",
  bodyCellProps: {
    render: renderEnumerationValue("orderPositionGoodState", [
      "position",
      "good_state"
    ])
  }
};

const returnCountField = {
  id: "count",
  title: "Количество",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["position", "count"]);
    }
  }
};

const returnPriceField = {
  id: "price",
  title: "Цена",
  bodyCellProps: {
    render: ({ entity }) => {
      const price = entity.position && entity.position.price;
      return price || price === 0 ? `${price} руб.` : "";
    }
  }
};

const returnSumField = {
  id: "sum",
  title: "Сумма",
  bodyCellProps: {
    render: ({ entity }) => {
      const sum = entity.position && entity.position.sum;
      return sum || sum === 0 ? `${sum} руб.` : "";
    }
  }
};

const returnReasonField = {
  id: "reason",
  title: "Причина возврата",
  enumerationKey: "returnReason",
  bodyCellProps: {
    render: renderEnumerationValue("returnReason", "reason")
  }
};

const returnPurposeField = {
  id: "purpose",
  title: "Назначение возврата",
  enumerationKey: "returnPurpose",
  bodyCellProps: {
    render: renderEnumerationValue("returnPurpose", "purpose")
  }
};

const purchaseOrderStateField = {
  id: "state",
  title: "Статус",
  enumerationKey: "orderPositionGoodState",
  bodyCellProps: {
    render: renderEnumerationValue("orderPositionGoodState", "good_state")
  }
};

const purchaseOrderCountField = {
  id: "count",
  title: "Количество"
};

const purchaseOrderPriceField = {
  id: "price",
  title: "Цена",
  bodyCellProps: {
    render: renderSum("price")
  }
};

const purchaseOrderSumField = {
  id: "sum",
  title: "Сумма",
  bodyCellProps: {
    render: renderSum("sum")
  }
};

const purchaseOrderEstimatedTimeField = {
  id: "estimated_time",
  title: "Ожидаемая дата поступления",
  bodyCellProps: {
    render: renderDateTime("estimated_time")
  }
};

const priceListField = {
  id: "pricelist_code",
  title: "Прайс-лист"
};

export const fields = renderFormFields([
  brandField,
  articleField,
  nameField,
  statusField,
  priceField,
  countField,
  sumField,
  purposeField,
  reasonField
]);

export const purchaseOrdersFields = renderFormFields([
  brandField,
  articleField,
  nameField,
  purchaseOrderStateField,
  purchaseOrderPriceField,
  purchaseOrderCountField,
  purchaseOrderSumField,
  priceListField,
  purchaseOrderEstimatedTimeField
]);

export const returnFields = renderFormFields([
  returnBrandField,
  returnArticleField,
  returnNameField,
  returnGoodStateField,
  returnCountField,
  returnPriceField,
  returnSumField,
  returnReasonField,
  returnPurposeField
]);

export const purchaseReturnFields = renderFormFields([
  returnBrandField,
  returnArticleField,
  returnNameField,
  returnGoodStateField,
  returnCountField,
  returnPriceField,
  returnSumField,
  returnReasonField
]);
