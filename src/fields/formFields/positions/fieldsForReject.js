import React from "react";
import FormField from "../../../components/common/formControls/FieldContext";
import { typeMethods } from "../../../ducks/positions";

import { required, valueLessOrEqualThan } from "../../../helpers/validators";

import { renderFormFields } from "../../helpers/index";

import {
  brandField,
  articleField,
  nameField,
  returnBrandField,
  returnArticleField,
  returnNameField
} from "../../templates/tableCellTemplates/positionFields";

const bodyCellProps = {
  render: ({ entity, field }) => (
    <FormField name={`${field.id}__${entity._id}`} />
  )
};

const countField = {
  id: "count",
  title: "Количество",
  bodyCellProps
};

const rejectField = {
  id: "reject",
  title: "Отказаться от",
  bodyCellProps
};

const formCountField = {
  id: "count__",
  shouldNotBeInValue: true,
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "количество",
      disabled: true
    }
  },
  afterMap({ values, field }) {
    const value = values.count;
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        value,
        validators: [valueLessOrEqualThan(value), required]
      }
    };
  }
};

const formReturnCountField = {
  ...formCountField,
  afterMap({ values, field }) {
    const value = values.position && values.position.count;
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        value,
        validators: [valueLessOrEqualThan(value), required]
      }
    };
  }
};

const formRejectField = {
  id: "reject__",
  shouldNotBeInValue: true,
  formControlProps: {
    type: "text",
    attrs: { placeholder: "Отказаться от" }
  },
  afterMap({ values, field }) {
    const value = values.count;
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        value,
        validators: [valueLessOrEqualThan(value), required]
      }
    };
  }
};

const formReturnRejectField = {
  ...formRejectField,
  afterMap({ values, field }) {
    const value = values.position && values.position.count;
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        value,
        validators: [valueLessOrEqualThan(value), required]
      }
    };
  }
};

const fieldsForTable = {
  [typeMethods.orders]: renderFormFields([
    brandField,
    articleField,
    nameField,
    countField,
    rejectField
  ]),
  [typeMethods.returns]: renderFormFields([
    returnBrandField,
    returnArticleField,
    returnNameField,
    countField,
    rejectField
  ])
};

const fieldsForForm = {
  [typeMethods.orders]: renderFormFields([
    [
      { destructuring: true, id: "positions", name: null },
      [formCountField, formRejectField]
    ]
  ]),
  [typeMethods.returns]: renderFormFields([
    [
      { destructuring: true, id: "positions", name: null },
      [formReturnCountField, formReturnRejectField]
    ]
  ])
};

const proxyHandler = {
  get: function(obj, prop) {
    switch (prop) {
      case typeMethods.orders:
      case typeMethods.purchaseOrders:
        return obj[typeMethods.orders];
      case typeMethods.returns:
      case typeMethods.purchaseReturns:
        return obj[typeMethods.returns];
    }

    throw new Error(`Должен быть определён typeMethod. Сейчас: ${prop}`);
  }
};

export const fieldsForTableContainer = new Proxy(fieldsForTable, proxyHandler);
export const fieldsForFormContainer = new Proxy(fieldsForForm, proxyHandler);
