import React from "react";
import { renderFormFields } from "../../helpers/index";
import { getValueInDepth } from "../../../utils";
import {
  required,
  valueLessOrEqualThan,
  isRequiredNumericValue,
  valueGreaterOrEqualThan
} from "../../../helpers/validators";
import {
  brandField,
  articleField,
  nameField
} from "../../templates/tableCellTemplates/positionFields";
import FormField from "../../../components/common/formControls/FieldContext";
import { typeMethods } from "../../../ducks/positions";

const returnBrandField = {
  id: "brand",
  title: "Товар",
  bodyCellProps: {
    render: ({ entity }) =>
      getValueInDepth(entity, ["position", "product", "brand"])
  }
};

const returnArticleField = {
  id: "article",
  bodyCellProps: {
    render: ({ entity }) =>
      getValueInDepth(entity, ["position", "product", "article"])
  }
};

const returnNameField = {
  id: "name",
  bodyCellProps: {
    render: ({ entity }) =>
      getValueInDepth(entity, ["position", "product", "name"])
  }
};

const countField = {
  id: "count",
  title: "Количество",
  shouldNotBeInValue: true,
  bodyCellProps: {
    render: ({ entity }) => {
      return <FormField name={`count__${entity._id}`} />;
    }
  }
};

const devideField = {
  id: "devide",
  title: "Отделить",
  shouldNotBeInValue: true,
  bodyCellProps: {
    render: ({ entity }) => {
      return <FormField name={`devide__${entity._id}`} />;
    }
  }
};

// FORM FIELDS

const formCountField = {
  id: "count__",
  shouldNotBeInValue: true,
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "количество",
      disabled: true
    }
  },
  afterMap: ({ field, fields, values }) => {
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        value: values.count
      }
    };
  }
};

const formDevideField = {
  id: "devide__",
  shouldNotBeInValue: true,
  formControlProps: {
    type: "text",
    attrs: { placeholder: "отделить" },
    value: "1",
    validators: [isRequiredNumericValue]
  },
  afterMap: ({ field, values }) => {
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        validators: [
          valueGreaterOrEqualThan(1),
          valueLessOrEqualThan(values.count),
          required
        ]
      }
    };
  }
};

const returnFormDevideField = {
  id: "devide__",
  shouldNotBeInValue: true,
  formControlProps: {
    type: "text",
    attrs: { placeholder: "отделить" },
    value: "1",
    validators: [isRequiredNumericValue]
  },
  afterMap: ({ field, values }) => {
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        validators: [
          valueGreaterOrEqualThan(1),
          valueLessOrEqualThan(values.position.count),
          required
        ]
      }
    };
  }
};

const returnFormCountField = {
  id: "count__",
  shouldNotBeInValue: true,
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "количество",
      disabled: true
    }
  },
  afterMap: ({ field, fields, values }) => {
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        value: values.position && values.position.count
      }
    };
  }
};

const fieldsForDividingType = {
  orders: renderFormFields([
    brandField,
    articleField,
    nameField,
    countField,
    devideField
  ]),
  returns: renderFormFields([
    returnBrandField,
    returnArticleField,
    returnNameField,
    countField,
    devideField
  ])
};

const fieldsForFormType = {
  orders: renderFormFields([
    [
      { destructuring: true, id: "fields", name: null },
      [formCountField, formDevideField]
    ]
  ]),
  returns: renderFormFields([
    [
      { destructuring: true, id: "fields", name: null },
      [returnFormCountField, returnFormDevideField]
    ]
  ])
};

const getDevidingFieldsForType = (fields, typeMethod) => {
  switch (typeMethod) {
    case typeMethods.orders:
    case typeMethods.purchaseOrders:
      return fields.orders;
    case typeMethods.returns:
    case typeMethods.purchaseReturns:
      return fields.returns;
  }

  throw new Error(`Должен быть определён typeMethod. Сейчас: ${typeMethod}`);
};

export const getFieldsForDividingType = typeMethod =>
  getDevidingFieldsForType(fieldsForDividingType, typeMethod);

export const getFieldsForFormType = typeMethod =>
  getDevidingFieldsForType(fieldsForFormType, typeMethod);
