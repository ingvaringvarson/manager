import React from "react";
import { getDate, getValueInDepth } from "../../../utils";
import { renderFormFields } from "../../helpers";

import ItemQtyControl from "../../../components/blocks/baskets/ItemQtyControl";
import { clientCodeField } from "../../templates/formFieldTemplates/text";

export const fieldForClientBasketForm = renderFormFields([clientCodeField]);

export const brandKey = {
  id: "brandKey",
  title: "Товар",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["product", "brandName"]);
    }
  }
};

export const vendorCode = {
  id: "vendorCode",
  bodyCellProps: {
    render: ({ entity }) => {
      const vendorCodes = getValueInDepth(entity, ["product", "vendorCodes"]);

      if (!(Array.isArray(vendorCodes) && vendorCodes.length)) return null;

      const vendorInfo = vendorCodes.find(v => v.isActual) || vendorCodes[0];

      return vendorInfo.vendorCode;
    }
  }
};

export const productName = {
  id: "productName",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["product", "productName"]);
    }
  }
};

export const price = {
  id: "price",
  title: "Цена",
  bodyCellProps: {
    render: ({ entity }) => {
      return getValueInDepth(entity, ["offerInfo", "offer", "price"]);
    }
  }
};

export const qty = {
  id: "qty",
  title: "Количество",
  bodyCellProps: {
    render: ({ entity }) => {
      return <ItemQtyControl basketId={entity.basketId} item={entity} />;
    }
  }
};

export const total = {
  id: "total",
  title: "Сумма",
  bodyCellProps: {
    render: ({ entity }) => {
      const qty = getValueInDepth(entity, ["qty"]);
      const price = getValueInDepth(entity, ["offerInfo", "offer", "price"]);
      return `${qty * price} руб.`;
    }
  }
};

export const isPrepaid = {
  id: "isPrepaid",
  title: "Предоплата",
  bodyCellProps: {
    render: ({ entity }) => {
      const value = getValueInDepth(entity, [
        "offerInfo",
        "offer",
        "isPrepaid"
      ]);
      return value ? "Требуется" : "Не требуется";
    }
  }
};

export const deliveryDate = {
  id: "deliveryDate",
  title: "Срок",
  bodyCellProps: {
    render: ({ entity }) => {
      const value = getValueInDepth(entity, [
        "offerInfo",
        "offer",
        "deliveryDate"
      ]);
      return getDate(value);
    }
  }
};

export const fieldsForProducts = renderFormFields([
  brandKey,
  vendorCode,
  productName,
  price,
  qty,
  total,
  isPrepaid,
  deliveryDate
]);
