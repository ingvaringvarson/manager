import React from "react";
import { renderFormFields } from "../../helpers";
import { required, valueGreaterThan } from "../../../helpers/validators";
import {
  brandField,
  nameField,
  articleField
} from "../../templates/tableCellTemplates/positionFields";
import FormField from "../../../components/common/formControls/FieldContext";

const isCountGreaterThan = count => value => {
  return +count > +value
    ? 'Для уменьшения количества воспользуйтесь командой: "Отказ от товара"'
    : null;
};

export const tableFields = renderFormFields([
  brandField,
  nameField,
  articleField,
  {
    id: "count",
    title: "Количество",
    bodyCellProps: {
      render: ({ entity }) => <FormField name={`count__${entity._id}`} />
    }
  },
  {
    id: "price",
    title: "Цена закупки",
    bodyCellProps: {
      render: ({ entity }) => <FormField name={`price__${entity._id}`} />
    }
  }
]);

export const formFields = renderFormFields([
  [
    { destructuring: true, id: "fields", name: null },
    [
      {
        id: "count__",
        shouldNotBeInValue: true,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "Количество" }
        },
        afterMap: ({ field, values }) => {
          const { count } = values;
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              value: count,
              validators: [
                valueGreaterThan(0),
                isCountGreaterThan(count),
                required
              ]
            }
          };
        }
      },
      {
        id: "price__",
        shouldNotBeInValue: true,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "Цена закупки" }
        },
        afterMap: ({ field, values }) => {
          const { price } = values;
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              value: price,
              validators: [valueGreaterThan(0), required]
            }
          };
        }
      }
    ]
  ]
]);
