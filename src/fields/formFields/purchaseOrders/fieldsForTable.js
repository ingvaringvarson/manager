import React from "react";
import styled from "styled-components";
import { color } from "styled-system";
import {
  renderSum,
  renderFormFields,
  renderDateTime,
  renderEnumerationValue,
  maxDateAmongPositions,
  getNameCompanyShort,
  getIdForEntity
} from "../../helpers";
import { maskOnlyNumbers } from "../../../helpers/masks";
import { orderStateColors } from "../../../constants/colors";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForWarehouses
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

const StyledSpan = styled.span`
  ${color};
`;

const agentCodeField = {
  id: "agent_code",
  title: "Код поставщика",
  headCellProps: {
    sortBy: true,
    sortName: "purchase_orders_with_bills.agent_tech.code"
  },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.agent_tech && entity.agent_tech.code;
    }
  },
  formControlProps: {
    mask: maskOnlyNumbers,
    type: "text",
    attrs: { placeholder: "Поиск по номеру поставщика" }
  }
};

const agentNameField = {
  id: "company_name_full",
  title: "Наименование поставщика",
  queryKeys: ["agent_id"],
  headCellProps: { sortBy: true, sortName: "agent_name" },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.agent_tech && entity.agent_tech.company_name_full;
    }
  },
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Поиск по наименованию поставщика" },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      getName: getNameCompanyShort,
      getId: getIdForEntity,
      params: {
        roles: [1]
      }
    })
  }
};

const orderFirmNameField = {
  id: "firm_name",
  title: "Наименование заказчика",
  queryKeys: ["firm_id"],
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.order && entity.order.firm_name;
    }
  },
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Поиск по наименованию заказчика" },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      getName: getNameCompanyShort,
      getId: getIdForEntity,
      params: {
        roles: [2]
      }
    })
  }
};

const orderCodeField = {
  id: "code",
  title: "Номер заказа",
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.order && entity.order.code;
    }
  },
  formControlProps: {
    type: "text",
    attrs: { placeholder: "Поиск по номеру заказа" }
  }
};

const orderStateField = {
  id: "table_state",
  title: "Статус",
  queryKeys: ["state"],
  enumerationKey: "orderSupplierState",
  bodyCellProps: {
    render: ({ entity, enumerations, field }) => {
      const state = renderEnumerationValue(field.enumerationKey, [
        "order",
        "state_supplier_order"
      ])({ entity, enumerations });
      return (
        <StyledSpan color={orderStateColors[entity.order.state_supplier_order]}>
          {state}
        </StyledSpan>
      );
    }
  },
  formControlProps: {
    type: "select",
    attrs: { placeholder: "Поиск по статусу заказа" }
  }
};

const warehouseField = {
  id: "warehouse_name",
  title: "Склад приемки",
  queryKeys: ["warehouse_id"],
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.order && entity.order.warehouse_name;
    }
  },
  formControlProps: {
    type: "select_search_request",
    attrs: { placeholder: "Поиск по складу приемки" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};

const createdDateField = {
  id: "object_date_create",
  queryKeys: ["datefrom_created", "dateto_created"],
  title: "Дата создания",
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: renderDateTime(["order", "object_date_create"], true)
  },
  formControlProps: {
    type: "date",
    attrs: { placeholder: "Поиск по дате создания" }
  }
};

const estimatedDateField = {
  id: "estimated_time",
  queryKeys: ["estimated_time_start", "estimated_time_end"],
  title: "Ожидаемая дата поступления",
  bodyCellProps: {
    render: ({ entity }) =>
      maxDateAmongPositions("estimated_time")({ entity: entity.order })
  },
  formControlProps: {
    type: "date",
    attrs: { placeholder: "Поиск по ожидаемой дате поступления" }
  }
};

const totalSumField = {
  id: "sum_total",
  title: "Сумма",
  formControlProps: {
    attrs: { disabled: true, placeholder: "Поиск по сумме" }
  },
  bodyCellProps: {
    render: renderSum(["order", "sum_total"])
  }
};

const commentField = {
  id: "comment",
  title: "Комментарий",
  formControlProps: {
    attrs: { disabled: true, placeholder: "Поиск по комментарию" }
  },
  bodyCellProps: {
    render: ({ entity }) => entity.order.comment
  }
};

export const fields = renderFormFields([
  agentCodeField,
  agentNameField,
  orderFirmNameField,
  orderCodeField,
  orderStateField,
  estimatedDateField,
  warehouseField,
  totalSumField,
  createdDateField,
  commentField
]);
