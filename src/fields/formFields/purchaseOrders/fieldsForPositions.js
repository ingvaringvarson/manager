import {
  renderFormFields,
  renderDateTime,
  renderSum
} from "../../helpers/index";
import {
  brandField,
  articleField,
  nameField
} from "../../templates/tableCellTemplates/positionFields";

export const fields = renderFormFields([
  brandField,
  articleField,
  nameField,
  {
    id: "good_state",
    title: "Статус",
    enumerationKey: "orderPositionGoodState"
  },
  {
    id: "price",
    title: "Цена",
    bodyCellProps: {
      render: renderSum("price")
    }
  },
  {
    id: "count",
    title: "Кол-во"
  },
  {
    id: "sum",
    title: "Сумма",
    bodyCellProps: {
      render: renderSum("sum")
    }
  },
  {
    id: "pricelist_code",
    title: "Прайс-лист"
  },
  {
    id: "estimated_time",
    title: "Ожид. срок поставки",
    bodyCellProps: {
      render: renderDateTime("estimated_time", true)
    }
  }
]);
