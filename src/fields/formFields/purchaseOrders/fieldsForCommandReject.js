import React from "react";
import { renderFormFields } from "../../helpers";
import {
  valueGreaterOrEqualThan,
  valueLessOrEqualThan,
  required
} from "../../../helpers/validators";
import {
  brandField,
  nameField,
  articleField
} from "../../templates/tableCellTemplates/positionFields";
import FormField from "../../../components/common/formControls/FieldContext";
import { maskOnlyNumbers } from "../../../helpers/masks";

export const tableFields = renderFormFields([
  brandField,
  nameField,
  articleField,
  {
    id: "count",
    title: "Количество",
    bodyCellProps: {
      render: ({ entity }) => <FormField name={`count__${entity._id}`} />
    }
  },
  {
    id: "reject",
    title: "Отказаться от",
    bodyCellProps: {
      render: ({ entity }) => <FormField name={`reject__${entity._id}`} />
    }
  }
]);

export const formFields = renderFormFields([
  [
    { destructuring: true, id: "fields", name: null },
    [
      {
        id: "count__",
        shouldNotBeInValue: true,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "Количество", disabled: true }
        },
        afterMap: ({ field, values }) => {
          const { count } = values;
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              value: count
            }
          };
        }
      },
      {
        id: "reject__",
        shouldNotBeInValue: true,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "Отказаться от" }
        },
        afterMap: ({ field, values }) => {
          const { count } = values;
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              value: count,
              validators: [
                valueGreaterOrEqualThan(1),
                valueLessOrEqualThan(count),
                required
              ],
              mask: maskOnlyNumbers
            }
          };
        }
      }
    ]
  ]
]);
