import { renderFormFields } from "../../helpers";
import { searchRequestPropsForWarehouses } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

export const fields = renderFormFields([
  {
    id: "delivery_type",
    enumerationKey: "pricelistDeliveryType",
    shouldNotBeInValue: true,
    formControlProps: {
      attrs: { placeholder: "Условия доставки" },
      label: "Способ получения",
      type: "select",
      value: [],
      defaultValue: []
    },
    afterMap: ({ field, values }) => {
      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          value: values.order.delivery_type
        }
      };
    }
  },
  {
    id: "warehouse_id",
    shouldNotBeInValue: true,
    formControlProps: {
      type: "select_search_request",
      value: [],
      defaultValue: [],
      label: "Склад приемки",
      attrs: { placeholder: "Выбрать склад" },
      searchRequestProps: searchRequestPropsForWarehouses()
    },
    afterMap: ({ field, values }) => {
      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          value: values.order.warehouse_id
        }
      };
    }
  },
  {
    id: "estimated_time",
    shouldNotBeInValue: true,
    formControlProps: {
      type: "date",
      label: "Планируемая дата поступления заказа",
      attrs: { placeholder: "Выбрать дату поступления" }
    },
    afterMap: ({ field, values }) => {
      if (
        !(
          values.order &&
          values.order.positions &&
          Array.isArray(values.order.positions) &&
          values.order.positions[0]
        )
      )
        return field;

      const maxDate = values.order.positions.reduce((result, pos) => {
        if (result < pos.estimated_time) return pos.estimated_time;

        return result;
      }, values.order.positions[0].estimated_time);

      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          value: maxDate
        }
      };
    }
  }
]);
