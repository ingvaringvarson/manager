import {
  getIdForEntity,
  getNameCompanyShort,
  renderFormFields
} from "../../helpers";
import {
  maxDateRange,
  minDateRange,
  isTextSymbols,
  maxLength,
  minLength
} from "../../../helpers/validators";
import { maskDateBeginningDay, maskDateEndDay } from "../../../helpers/masks";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForWarehouses
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

const avaiableOrderPosGoodStates = [1, 10, 11, 13, 19, 20];

const createdDateFields = [
  { destructuring: false, id: null, name: "Поиск по дате создания" },
  [
    {
      id: "datefrom_created",
      formControlProps: {
        validators: [maxDateRange("dateto_created")],
        mask: maskDateBeginningDay,
        type: "date",
        attrs: { placeholder: "Выберите дату" }
      }
    },
    {
      id: "dateto_created",
      formControlProps: {
        validators: [minDateRange("datefrom_created")],
        mask: maskDateEndDay,
        type: "date",
        attrs: { placeholder: "Выберите дату" }
      }
    }
  ]
];

const acceptedDateFields = [
  { destructuring: false, id: null, name: "Поиск по ожидаемой дате приемки" },
  [
    {
      id: "estimated_time_start",
      formControlProps: {
        validators: [maxDateRange("estimated_time_end")],
        mask: maskDateBeginningDay,
        type: "date",
        attrs: { placeholder: "Выберите дату" }
      }
    },
    {
      id: "estimated_time_end",
      formControlProps: {
        validators: [minDateRange("estimated_time_start")],
        mask: maskDateEndDay,
        type: "date",
        attrs: { placeholder: "Выберите дату" }
      }
    }
  ]
];

const companyField = {
  id: "agent_id",
  queryKeys: ["agent_id"],
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select_search_request",
    label: "Наименование поставщика",
    attrs: { placeholder: "Поиск по наименованию поставщика" },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      getName: getNameCompanyShort,
      getId: getIdForEntity,
      params: {
        roles: [1]
      }
    })
  }
};

const firmField = {
  id: "firm_id",
  queryKeys: ["firm_id"],
  formControlProps: {
    label: "Организация",
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Выберите организацию" },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      getName: getNameCompanyShort,
      getId: getIdForEntity,
      params: {
        roles: [2]
      }
    })
  }
};

const warehouseField = {
  id: "warehouse_id",
  queryKeys: ["warehouse_id"],
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Выберите склад приемки" },
    label: "Склад приемки",
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};

const articleField = {
  id: "article",
  formControlProps: {
    type: "text",
    attrs: { placeholder: "Введите артикул" },
    label: "Артикул",
    validators: [isTextSymbols]
  }
};

const objectCreateField = {
  id: "_agent_code",
  formControlProps: {
    type: "text",
    attrs: { placeholder: "Введите код создателя заказа	поставщика" },
    label: "Код создателя заказа"
  },
  afterMap: ({ values, field }) => {
    const disabled =
      values.hasOwnProperty("object_id_create") && !!values.object_id_create;
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        attrs: {
          ...field.formControlProps.attrs,
          disabled
        }
      }
    };
  }
};

const oldClientIdField = {
  id: "history_id",
  formControlProps: {
    type: "text",
    label: "Старый id",
    attrs: { placeholder: "Введите старый id" },
    validators: [maxLength(13), minLength(1)]
  }
};

const forUserField = {
  id: "_for_user",
  title: "Мной созданные заказы поставщика ",
  formControlProps: {
    type: "checkbox",
    valuesByChecked: new Map([[false, ""], [true, "1"]]),
    checkedByValue: new Map([["", false], ["1", true]]),
    label: "Мной созданные заказы поставщика "
  }
};

const stateField = {
  id: "state",
  enumerationKey: "orderSupplierState",
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select",
    attrs: { placeholder: "Выберите статус заказа поставщика" },
    label: "Статус заказа поставщика",
    isMultiple: true
  }
};

const goodStateField = {
  id: "good_state",
  enumerationKey: "orderPositionGoodState",
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select",
    attrs: { placeholder: "Выберите статус позиции заказа поставщика" },
    label: "Статус позиции заказа поставщика",
    isMultiple: true
  },
  afterMap: ({ field, values, enumerations }) => {
    const options =
      enumerations.orderPositionGoodState &&
      Array.isArray(enumerations.orderPositionGoodState.list) &&
      enumerations.orderPositionGoodState.list.filter(stateObj => {
        return avaiableOrderPosGoodStates.some(
          avaiableState => stateObj.id === avaiableState
        );
      });
    if (!Array.isArray(options)) return field;

    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        options
      }
    };
  }
};

export const fieldsForFilters = renderFormFields([
  createdDateFields,
  acceptedDateFields,
  firmField,
  companyField,
  warehouseField,
  articleField,
  objectCreateField,
  oldClientIdField,
  forUserField,
  stateField,
  goodStateField
]);
