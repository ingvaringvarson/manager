import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { color } from "styled-system";

import {
  renderFormFields,
  renderEnumerationValue,
  getNameCompanyShort,
  getIdForEntity
} from "../../helpers";
import { getUrl } from "../../../helpers/urls";
import { getDate, getTime, getValueInDepth } from "../../../utils";
import { maskOnlyText } from "../../../helpers/masks";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForWarehouses
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";
import { orderStateColors } from "../../../constants/colors";

const ColoredSpan = styled.span`
  ${color};
`;

const StyledLink = styled(Link)`
  color: #336ea8;
  cursor: pointer;
  text-decoration: none;
`;

// MAIN --- START
const codeField = {
  id: "code",
  title: "Номер заказа",
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      const { code, id } = entity.order;
      return (
        <StyledLink to={getUrl("purchaseOrders", { id })}>{code}</StyledLink>
      );
    }
  },
  formControlProps: {
    mask: maskOnlyText,
    type: "text",
    attrs: { placeholder: "Поиск по номеру" }
  }
};

const stateField = {
  id: "state",
  title: "Статус",
  enumerationKey: "orderSupplierState",
  bodyCellProps: {
    render: ({ entity, enumerations, field }) => {
      if (orderStateColors[entity.order.state_supplier_order]) {
        return (
          <ColoredSpan
            color={orderStateColors[entity.order.state_supplier_order]}
          >
            {renderEnumerationValue(field.enumerationKey, [
              "order",
              "state_supplier_order"
            ])({ entity, enumerations })}
          </ColoredSpan>
        );
      }
      return renderEnumerationValue(field.enumerationKey, [
        "order",
        "state_supplier_order"
      ])({ entity, enumerations });
    }
  },
  formControlProps: {
    type: "select",
    attrs: { placeholder: "Поиск по статусу" },
    value: [],
    defaultValue: []
  }
};

const firmField = {
  id: "_agent_id_firm",
  title: "Фирма",
  queryKeys: ["firm_id"],
  headCellProps: { sortName: "firm_name", sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.order && entity.order.firm_name;
    }
  },
  formControlProps: {
    type: "select_search_request",
    value: [],
    defaultValue: [],
    attrs: { placeholder: "Введите название организации" },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      getName: getNameCompanyShort,
      getId: getIdForEntity,
      params: {
        roles: [2]
      }
    })
  }
};

const warehouseField = {
  id: "warehouse_name",
  title: "Склад приемки",
  queryKeys: ["warehouse_id"],
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.order && entity.order.warehouse_name;
    }
  },
  formControlProps: {
    type: "select_search_request",
    value: [],
    defaultValue: [],
    attrs: { placeholder: "Поиск по складу приемки" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};

const estimatedTimeField = {
  id: "estimated_time",
  title: "Ожидаемая дата поступления",
  queryKeys: ["estimated_time_start", "estimated_time_end"],
  bodyCellProps: {
    render: ({ field, fields, entity }) => {
      if (
        !(
          entity.order &&
          entity.order.positions &&
          Array.isArray(entity.order.positions) &&
          entity.order.positions.length
        )
      ) {
        return null;
      }

      const maxDateStr = entity.order.positions.reduce((result, element) => {
        const curDate = element.estimated_time;
        return result > curDate ? result : curDate;
      }, entity.order.positions[0].estimated_time);

      const maxDate = new Date(maxDateStr);

      return maxDateStr ? `${getDate(maxDate)}, ${getTime(maxDate)}` : "";
    }
  },
  formControlProps: {
    type: "date",
    attrs: { placeholder: "Поиск по ожидаемой дате поступления" }
  }
};

const sumTotalField = {
  id: "sum_total",
  title: "Сумма",
  bodyCellProps: {
    render: ({ entity }) => {
      const sum = entity.order && entity.order.sum_total;
      return sum || sum === 0 ? `${sum} руб.` : "";
    }
  },
  formControlProps: {
    type: "text",
    attrs: { disabled: true, placeholder: "Поиск по сумме" }
  }
};

const sumPaymentField = {
  id: "sum_payment",
  title: "Сумма к оплате",
  bodyCellProps: {
    render: ({ entity }) => {
      const sum = entity.order && entity.order.sum_payment;
      return sum || sum === 0 ? `${sum} руб.` : "";
    }
  },
  formControlProps: {
    type: "text",
    attrs: { disabled: true, placeholder: "Поиск по сумме к оплате" }
  }
};

const objectDateField = {
  id: "object_date_create",
  title: "Дата создания",
  queryKeys: ["datefrom_created", "dateto_created"],
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      if (!(entity.order && entity.order.object_date_create)) {
        return null;
      }
      const date = new Date(entity.order.object_date_create);
      return `${getDate(date)}, ${getTime(date)}`;
    }
  },
  formControlProps: {
    type: "date",
    attrs: { placeholder: "Поиск по дате создания" }
  }
};

const objectNameCreateField = {
  id: "object_name_create",
  title: "Автор",
  headCellProps: { sortBy: true },
  queryKeys: ["object_id_create"],
  bodyCellProps: {
    render: ({ entity }) =>
      getValueInDepth(entity, ["order", "object_name_create"])
  },
  formControlProps: {
    type: "select_search_request",
    value: [],
    defaultValue: [],
    attrs: { placeholder: "Поиск по автору" },
    searchRequestProps: searchRequestPropsForAgents({
      params: {
        roles: [5]
      }
    })
  }
};
// MAIN --- END

const commentField = {
  id: "comment",
  title: "Комментарий",
  formControlProps: {
    attrs: { disabled: true, placeholder: "Поиск по комментарию" }
  },
  bodyCellProps: {
    render: ({ entity }) => entity.order.comment
  }
};

export const fields = renderFormFields([
  codeField,
  stateField,
  firmField,
  warehouseField,
  estimatedTimeField,
  sumTotalField,
  sumPaymentField,
  objectDateField,
  objectNameCreateField,
  commentField
]);
