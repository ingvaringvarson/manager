import { renderFormFields, renderDateTime } from "../../helpers/index";

const codeFields = {
  id: "code",
  title: "Номер",
  headCellProps: { sortBy: true }
};

const typeField = {
  id: "type",
  title: "Тип",
  enumerationKey: "taskType"
};

const stateField = {
  id: "state",
  title: "Статус",
  enumerationKey: "taskState"
};

const executorNameField = {
  id: "executor_name",
  title: "Исполнитель"
};

const directorNameField = {
  id: "director_name",
  title: "Постановщик	"
};

const warehouseName = {
  id: "warehouse_name",
  title: "Склад отгрузки",
  bodyCellProps: {
    render: ({ entity }) => entity.warehouse_name || ""
  }
};

const warehouseToName = {
  id: "warehouse_to_name",
  title: "Склад приемки",
  bodyCellProps: {
    render: ({ entity }) => entity.warehouse_to_name || ""
  }
};

const objectDateCreateField = {
  id: "object_date_create",
  title: "Дата создания	",
  headCellProps: { sortBy: true, sortName: "object_date_create" },
  bodyCellProps: {
    render: renderDateTime("object_date_create")
  }
};

const dateField = {
  id: "date",
  title: "Дата требуемой реализации	",
  headCellProps: { sortBy: true, sortName: "date" },
  bodyCellProps: {
    render: renderDateTime("date")
  }
};

const reasonField = {
  id: "reason",
  title: "Причина",
  enumerationKey: "taskReason"
};

export const fields = renderFormFields([
  codeFields,
  typeField,
  stateField,
  executorNameField,
  directorNameField,
  warehouseName,
  warehouseToName,
  objectDateCreateField,
  dateField,
  reasonField
]);
