import React from "react";
import { renderFormFields } from "../../helpers";
import {
  brandField,
  nameField,
  articleField
} from "../../templates/tableCellTemplates/positionFields";
import FormField from "../../../components/common/formControls/FieldContext";
import { isRequiredDateValue } from "../../../helpers/validators";

export const tableFields = renderFormFields([
  brandField,
  nameField,
  articleField,
  {
    id: "estimated_time",
    title: "Дата прихода",
    bodyCellProps: {
      render: ({ entity }) => (
        <FormField name={`estimated_time__${entity._id}`} />
      )
    }
  }
]);

export const formFields = renderFormFields([
  [
    { destructuring: true, id: "fields", name: null },
    [
      {
        id: "estimated_time__",
        shouldNotBeInValue: true,
        formControlProps: {
          type: "date",
          attrs: { placeholder: "Дата прихода" },
          validators: [isRequiredDateValue]
        },
        afterMap: ({ field, values }) => {
          const { estimated_time } = values;
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              value: estimated_time
            }
          };
        }
      }
    ]
  ]
]);
