import React from "react";
import { getValueInDepth } from "../../../utils";
import {
  mapEntitiesToGroupedList,
  renderInfoFields,
  mapValuesToFields,
  mapEntitiesToList,
  mapValuesToGroupedList,
  mapClientCodeHeading
} from "../../helpers";
import { maxDateAmongEntities } from "../../../helpers/masks";
import { fields } from "../clients/fieldsForEditForms";
import InfoRowCommon from "../../../components/common/info/InfoRowCommon";

const renderMaxEstimatedTimeRow = ({ field, restHelperValues }) => {
  if (
    !(
      restHelperValues.purchaseOrder &&
      restHelperValues.purchaseOrder.order &&
      Array.isArray(restHelperValues.purchaseOrder.order.positions)
    )
  )
    return null;

  const maxDate = maxDateAmongEntities("estimated_time")(
    restHelperValues.purchaseOrder.order.positions
  );

  return maxDate ? <InfoRowCommon {...field} value={maxDate} /> : null;
};

const EntityRecord = {
  title: null,
  subtitle: "",
  formMain: null,
  formField: null,
  view: "default",
  infoFields: [],
  formFields: [],
  fieldsForAdd: [],
  mapValuesToFields,
  devidingFields: []
};

export const fieldGroups = {
  delivery_1: {
    ...EntityRecord,
    title: "Способ получения",
    subtitle: "Доставка поставщиком",
    formMain: {
      subtitle: "Способ получения",
      title: "Редактирование",
      type: "edit",
      icon: "edit"
    },
    infoFields: renderInfoFields([
      {
        key: "warehouse_name",
        title: "Склад получения"
      },
      {
        key: "estimated_time",
        title: "Ожид. дата получения",
        renderRow: renderMaxEstimatedTimeRow
      }
    ])
  },
  delivery_2: {
    ...EntityRecord,
    title: "Способ получения",
    subtitle: "Доставка  Laf24",
    formMain: {
      subtitle: "Способ получения",
      title: "Редактирование",
      type: "edit",
      icon: "edit"
    },
    infoFields: renderInfoFields([
      {
        key: "warehouse_name",
        title: "Склад получения"
      },
      {
        key: "estimated_time",
        title: "Ожид. дата получения",
        renderRow: renderMaxEstimatedTimeRow
      }
    ])
  },
  client: {
    ...EntityRecord,
    title: "Поставщик",
    formMain: {
      icon: "account_box"
    },
    infoFields: renderInfoFields([
      {
        key: "client",
        id: "person_surname",
        title: "Фамилия",
        mask: value => getValueInDepth(value, ["person", "person_surname"])
      },
      {
        key: "client",
        id: "person_name",
        title: "Имя",
        mask: value => getValueInDepth(value, ["person", "person_name"])
      },
      {
        key: "client",
        id: "person_second_name",
        title: "Отчество",
        mask: value => getValueInDepth(value, ["person", "person_second_name"])
      },
      {
        key: "client",
        id: "tags",
        title: "Теги",
        enumerationKey: "agentTag",
        mask: value => getValueInDepth(value, ["tags"])
      },
      {
        key: "client",
        id: "segments",
        title: "Сегменты",
        enumerationKey: "agentSegment",
        mask: value => getValueInDepth(value, ["segments"])
      },
      {
        key: "manager_id",
        title: "Аккаунт-менеджер",
        mask: value => getValueInDepth(value, ["agent_tech", "manager_name"])
      }
    ]),
    mapValuesToFields: mapClientCodeHeading
  },
  company: {
    ...EntityRecord,
    title: "Поставщик",
    formMain: {
      icon: "account_box"
    },
    infoFields: renderInfoFields([
      {
        key: "client",
        id: "company_name_full",
        title: "Имя",
        mask: value => getValueInDepth(value, ["company", "company_name_full"])
      },
      {
        key: "client",
        id: "company_name_short",
        title: "Сокращенное наименование",
        mask: value => getValueInDepth(value, ["company", "company_name_short"])
      },
      {
        key: "client",
        id: "tags",
        title: "Теги",
        enumerationKey: "agentTag",
        mask: value => getValueInDepth(value, ["tags"])
      },
      {
        key: "client",
        id: "segments",
        title: "Сегменты",
        enumerationKey: "agentSegment",
        mask: value => getValueInDepth(value, ["segments"])
      },
      {
        key: "manager_id",
        title: "Аккаунт-менеджер",
        mask: value => getValueInDepth(value, ["agent_tech", "manager_name"])
      }
    ]),
    mapValuesToFields: mapClientCodeHeading
  },
  contacts: {
    ...EntityRecord,
    title: "Контакты",
    formMain: {
      subtitle: "Контакты",
      title: "Создание контакта",
      type: "add",
      icon: "add"
    },
    formField: {
      subtitle: "Контакт",
      title: "Редактирование",
      type: "edit"
    },
    view: "groupedList",
    mapValuesToFields: mapEntitiesToGroupedList("contactType"),
    formFields: fields["contacts"]
  },
  contact_persons: {
    ...EntityRecord,
    title: "Контактные лица",
    formField: {
      subtitle: "Контактные лица",
      title: "Редактирование",
      type: "edit"
    },
    view: "list",
    infoFields: renderInfoFields([
      {
        key: "name",
        title: "Имя"
      },
      {
        key: "gender",
        title: "Пол",
        enumerationKey: "personGender"
      },
      {
        key: "position",
        title: "Должность"
      },
      {
        key: "contacts",
        mapValueToFields: mapValuesToGroupedList("contactType"),
        view: "groupedList"
      }
    ]),
    mapValuesToFields: mapEntitiesToList,
    formFields: fields["contact_persons"]
  }
};
