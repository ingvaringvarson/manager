import { renderFormFields } from "../../helpers/index";

export const tableFields = renderFormFields([
  {
    id: "brand",
    title: "Товар",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.brand : "";
      }
    }
  },
  {
    id: "vendorCode",
    title: "",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.article : "";
      }
    }
  },
  {
    id: "name",
    title: "",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.name : "";
      }
    }
  },
  {
    id: "price",
    title: "Цена",
    bodyCellProps: {
      render: ({ entity }) => entity.price
    }
  },
  {
    id: "discount",
    title: "Скидка",
    bodyCellProps: {
      render: ({ entity }) => entity.discount || ""
    }
  },
  {
    id: "count",
    title: "Количество"
  },
  {
    id: "sum",
    title: "Сумма"
  },
  {
    id: "prepaid_sum",
    title: "Предоплата",
    bodyCellProps: {
      render: ({ entity }) => entity.prepaid_sum || ""
    }
  }
]);
