import React from "react";
import { renderFormFields } from "../../helpers";
import { getDate } from "../../../utils";
import { valueGreaterThan, required } from "../../../helpers/validators";
import DummyBillTooltip from "../../../components/blocks/bills/DummyBillTooltip";

export const fields = renderFormFields([
  {
    id: "contract",
    formControlProps: {
      label: "Выберите договор",
      name: "contract",
      type: "select",
      validators: [required],
      attrs: { placeholder: "Выберите договор" },
      renderAfterField: ({
        field,
        fields,
        values,
        helperFieldData,
        enumerations
      }) => {
        const contractId =
          Array.isArray(values.contract) && values.contract.length
            ? values.contract[0]
            : null;
        if (contractId) {
          const contract = helperFieldData.contracts.find(contr => {
            return contr.id === contractId;
          });
          if (contract) {
            return (
              <DummyBillTooltip
                enumerations={enumerations}
                client={helperFieldData}
                contract={contract}
              />
            );
          }
        }

        return null;
      }
    },
    afterMap: ({ field, fields, values }) => {
      const options =
        values.client && values.client.contracts
          ? values.client.contracts.map(contract => {
              const date_end = contract.date_end
                ? getDate(contract.date_end)
                : "";
              return {
                ...contract,
                name: `${contract.number} | ${getDate(contract.date_start)}${
                  date_end ? ` - ${getDate(contract.date_end)}` : ""
                }`
              };
            })
          : [];

      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          options
        }
      };
    }
  },
  {
    id: "sum",
    formControlProps: {
      label: "Сумма счета",
      name: "sum",
      type: "text",
      attrs: { placeholder: "Введите сумму счета" },
      validators: [valueGreaterThan(0), required]
    }
  }
]);
