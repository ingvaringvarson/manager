import React from "react";
import ContractTooltip from "../../../components/blocks/clients/ContractTooltip";
import { getDate } from "../../../utils";
import { renderFormFields } from "../../helpers";
import { maskOnlyNumbers } from "../../../helpers/masks";
import { required } from "../../../helpers/validators";

export const fieldsForForm = renderFormFields([
  {
    id: "contract_id",
    formControlProps: {
      type: "select",
      label: "Выберите договор",
      attrs: { placeholder: "Выберите договор" },
      validators: [required],
      afterChange({ field, values }) {
        const { valuesToMap, enumerations } = field.formControlProps;
        const { contracts } = valuesToMap.agent;
        if (!(Array.isArray(values.contract_id) && values.contract_id.length)) {
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              renderIconAfter: null
            }
          };
        }

        const contract = contracts.find(c => c.id === values.contract_id[0]);
        return {
          ...field,
          formControlProps: {
            ...field.formControlProps,
            renderIconAfter: () => (
              <ContractTooltip
                contract={contract}
                enumerations={enumerations}
              />
            )
          }
        };
      }
    },
    afterMap({ values, field, enumerations }) {
      const contracts = values.agent.contracts;
      if (!Array.isArray(contracts)) return field;

      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          options: contracts.map(contract => {
            return {
              ...contract,
              name: `${contract.number} | ${getDate(
                contract.date_start
              )} - ${getDate(contract.date_end)}`
            };
          }),
          valuesToMap: values,
          enumerations
        }
      };
    }
  },
  {
    id: "sum",
    formControlProps: {
      type: "text",
      label: "Сумма счета",
      attrs: { placeholder: "Введите сумму счета" },
      mask: maskOnlyNumbers,
      validators: [required]
    }
  }
]);
