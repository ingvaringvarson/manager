import React from "react";
import { Link } from "react-router-dom";
import { renderFormFields } from "../../helpers/index";
import { getUrl } from "../../../helpers/urls";

export const fields = renderFormFields([
  {
    id: "payment",
    title: "Платеж",
    bodyCellProps: {
      render: ({ entity }) => {
        return (
          <Link
            target="_blank"
            to={getUrl("payments", { id: entity.payment_id })}
          >
            № {entity.payment_code}
          </Link>
        );
      }
    }
  },
  {
    id: "sum",
    title: "Сумма",
    bodyCellProps: {
      render: ({ entity }) => {
        return `${entity.sum} руб.`;
      }
    }
  }
]);
