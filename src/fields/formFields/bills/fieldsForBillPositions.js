import { renderEnumerationValue, renderFormFields } from "../../helpers/index";

export const fields = renderFormFields([
  {
    id: "brand",
    title: "Товар",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.brand : "";
      }
    }
  },
  {
    id: "article",
    title: "",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.article : "";
      }
    }
  },
  {
    id: "name",
    title: "",
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.product ? entity.product.name : "";
      }
    }
  },
  {
    id: "type",
    title: "Тип",
    bodyCellProps: {
      render: ({ entity, enumerations }) => {
        return entity.product
          ? renderEnumerationValue("productType", "type")({
              entity: entity.product,
              enumerations
            })
          : "";
      }
    }
  },
  {
    id: "sum",
    title: "Сумма"
  },
  { id: "discount_sum", title: "Скидка" }
]);
