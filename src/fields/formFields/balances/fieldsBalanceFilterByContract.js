import { renderFormFields } from "../../helpers/index";

export const fields = renderFormFields([
  {
    id: "contract_id",
    type: "select",
    title: "Договор",
    enumerationKey: "contract_id",
    isFilter: true,
    props: {
      type: "text",
      name: "contract_id",
      placeholder: "Выберите договор"
    }
  },
  {
    id: "account_type",
    type: "select",
    title: "Тип баланса",
    enumerationKey: "accountType",
    isFilter: true,
    props: {
      type: "text",
      name: "account_type",
      placeholder: "Выберите тип баланса"
    }
  }
]);
