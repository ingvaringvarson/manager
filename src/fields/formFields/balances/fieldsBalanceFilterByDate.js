import { renderFormFields } from "../../helpers/index";

export const fields = renderFormFields([
  {
    id: "begining_date",
    type: "date",
    props: {
      placeholder: "Выберите начало диапазона",
      type: "text",
      name: "begining_date"
    }
  },
  {
    id: "closing_date",
    type: "date",
    props: {
      placeholder: "Выберите окончание диапазона",
      type: "text",
      name: "closing_date"
    }
  }
]);
