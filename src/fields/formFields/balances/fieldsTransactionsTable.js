import { renderFormFields } from "../../helpers/index";
import { getDate, getTime } from "../../../utils";

export const fields = renderFormFields([
  {
    id: "object_date_create",
    title: "Дата",
    sortBy: false,
    renderCell: ({ entity }) => {
      const date = entity.object_date_create;
      return `${getDate(date)} ${getTime(date)}`;
    }
  },
  {
    id: "type",
    title: "Тип операции",
    sortBy: false,
    renderCell: ({ entity, enumerations }) => {
      const typeObj =
        enumerations.accountType &&
        enumerations.accountType.list &&
        enumerations.accountType.list.find(
          typeObject => typeObject.id === entity.type
        );
      return typeObj && typeObj !== -1 ? typeObj.name : entity.type;
    }
  },
  {
    id: "bill_code",
    sortBy: false,
    title: "Номер документа"
  },
  {
    id: "account_in_value",
    title: "Приход",
    sortBy: false,
    renderCell: ({ entity }) => {
      const value =
        entity.type === 1 ? entity.account_in_value : entity.account_out_value;
      return value > 0 ? value : "";
    }
  },
  {
    id: "account_out_value",
    title: "Расход",
    sortBy: false,
    renderCell: ({ entity }) => {
      const value =
        entity.type === 1 ? entity.account_in_value : entity.account_out_value;
      return value < 0 ? value : "";
    }
  }
]);
