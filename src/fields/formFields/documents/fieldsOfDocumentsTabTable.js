import { renderFormFields, renderEnumerationValue } from "../../helpers";

const nullToEmptyRender = func => input => func(input) || "";

export const fields = renderFormFields([
  {
    id: "name",
    title: "Наименование"
  },
  {
    id: "is_main",
    title: "Главный",
    bodyCellProps: {
      render: ({ entity }) => (entity.is_main ? "да" : "нет")
    }
  },
  {
    id: "templateId",
    enumerationKey: "",
    title: "Тип печатной формы",
    bodyCellProps: {
      render: nullToEmptyRender(
        renderEnumerationValue("documentTemplate", ["template", "id"])
      )
    }
  },
  {
    id: "firmId",
    title: "Организация",
    bodyCellProps: { render: () => ({}) },
    afterMap: ({ field, values }) => {
      const { getClientsFromSearch } = values;
      return {
        ...field,
        bodyCellProps: {
          render: ({ entity }) => {
            const firmId = entity.template && entity.template.firm_id;
            const firmClient =
              firmId && getClientsFromSearch().find(c => c.id === firmId);
            return firmClient ? firmClient.company.company_name_short : "";
          }
        }
      };
    }
  },
  {
    id: "templateDocType",
    title: "Расширение",
    bodyCellProps: {
      render: nullToEmptyRender(
        renderEnumerationValue("documentsType", ["template", "doc_type"])
      )
    }
  },
  {
    id: "object_name_create",
    title: "Автор"
  }
]);
