import React from "react";
import styled from "styled-components";

import { renderFormFields } from "../../helpers";

const EllipsisText = styled.p`
  text-overflow: ellipsis;
  width: 144px;
  overflow: hidden;
`;

export const fields = renderIsMain =>
  renderFormFields([
    {
      id: "key",
      title: "",
      isActive: false
    },
    {
      id: "name",
      title: "Наименование",
      bodyCellProps: {
        render: ({ entity }) => <EllipsisText>{entity.name}</EllipsisText>
      }
    },
    {
      id: "isMain",
      title: "Главный",
      bodyCellProps: { render: renderIsMain }
    }
  ]);
