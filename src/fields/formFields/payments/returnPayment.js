import React from "react";
import { renderFormFields } from "../../helpers";
import { maskOnlyNumbers } from "../../../helpers/masks";
import { required } from "../../../helpers/validators";
import { generateId, getValueInDepth } from "../../../utils";
import {
  PaymentIcon,
  PaymentKind,
  PaymentIconWrap,
  PaymentTitle,
  CheckBlock
} from "../../../components/common/styled/form";
import AgentContactsControl from "../../../components/common/form/AgentContactsControl";
import Checkbox from "../../../designSystem/molecules/Checkbox";

export const billIdField = {
  id: "bill_id",
  enumerationKey: "bills",
  formControlProps: {
    validators: [required],
    type: "select",
    value: [],
    defaultValue: [],
    attrs: { placeholder: "Выберите счет", iconClearProps: { hide: true } },
    afterChange: ({ values, prevValues, field }) => {
      if (
        values[field.id] &&
        prevValues[field.id] &&
        values[field.id][0] !== prevValues[field.id][0]
      ) {
        const selectedOption = field.formControlProps.options.find(
          f => f.id === values[field.id][0]
        );

        if (selectedOption && selectedOption.entity) {
          const newValues = {
            ...values,
            firm_id: selectedOption.entity.firm_id,
            firm_name: selectedOption.entity.firm_name,
            contract_id: selectedOption.entity.contract_id,
            sum: selectedOption.entity.bill_sum,
            receipt: selectedOption.entity.bill_sum,
            balance: 0
          };

          return { ...field, values: newValues };
        } else {
          const newValues = {
            ...values,
            firm_id: "",
            firm_name: "",
            contract_id: "",
            sum: 0,
            receipt: 0,
            balance: 0
          };

          return { ...field, values: newValues };
        }
      }

      return field;
    }
  },
  afterMap: ({ values, field }) => {
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        attrs: {
          ...field.formControlProps.attrs,
          disabled: !!(values.bill && !values.returnEntity)
        }
      }
    };
  }
};

export const billIdField_v2 = {
  ...billIdField,
  formControlProps: {
    ...billIdField.formControlProps,
    validators: [],
    attrs: { placeholder: "Выберите счет" },
    afterChange: ({ values, prevValues, field }) => {
      if (
        values[field.id] &&
        prevValues[field.id] &&
        values[field.id][0] !== prevValues[field.id][0]
      ) {
        const selectedOption = field.formControlProps.options.find(
          f => f.id === values[field.id][0]
        );

        const order = getValueInDepth(field, [
          "formControlProps",
          "data",
          "order"
        ]);

        let newValues = { ...values, balance: 0 };

        if (selectedOption && selectedOption.entity) {
          newValues.contract_id = selectedOption.entity.contract_id;
          newValues.sum = selectedOption.entity.bill_sum;
          newValues.receipt = selectedOption.entity.bill_sum;
        } else if (order) {
          newValues.contract_id = order.contract_id;
          newValues.sum = order.sum_payment;
          newValues.receipt = order.sum_payment;
        } else {
          newValues.contract_id = "";
          newValues.sum = 0;
          newValues.receipt = 0;
        }

        return { ...field, values: newValues };
      }

      return field;
    }
  },
  afterMap: ({ values, field }) => {
    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        data: {
          order: values.order
        }
      }
    };
  }
};

export const agentIdField = {
  id: "agent_id",
  formControlProps: {
    validators: [required]
  }
};

export const receiptAddressField = {
  id: "receipt_address",
  enumerationKey: "receipt_address",
  formControlProps: {
    type: "custom",
    render: ({ field, typeProps, values }) => {
      const agent = getValueInDepth(field, [
        "formControlProps",
        "data",
        "agent"
      ]);
      const onChange = getValueInDepth(typeProps, ["custom", "onChange"]);

      return (
        <AgentContactsControl
          name={field.id}
          value={values[field.id]}
          onChange={onChange}
          agent={agent}
        />
      );
    }
  },
  afterMap: ({ values, field }) => {
    const agent = getValueInDepth(values, ["client"]);

    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        data: {
          agent
        }
      }
    };
  }
};

export const paymentTypeField_1 = {
  id: "payment_type_1",
  queryKeys: ["payment_type"],
  formControlProps: {
    commonName: "payment_type",
    type: "radio",
    label: "100% предоплата",
    valuesByChecked: new Map([[false, ""], [true, "1"]]),
    checkedByValue: new Map([["", false], ["1", true]])
  }
};

export const paymentTypeField_2 = {
  id: "payment_type_2",
  queryKeys: ["payment_type"],
  formControlProps: {
    commonName: "payment_type",
    type: "radio",
    label: "Частичная предоплата",
    valuesByChecked: new Map([[false, ""], [true, "2"]]),
    checkedByValue: new Map([["", false], ["2", true]])
  }
};

export const paymentTypeField_3 = {
  id: "payment_type_3",
  queryKeys: ["payment_type"],
  formControlProps: {
    commonName: "payment_type",
    type: "radio",
    label: "Доплата",
    valuesByChecked: new Map([[false, ""], [true, "3"]]),
    checkedByValue: new Map([["", false], ["3", true]])
  }
};

export const paymentTypeField_4 = {
  id: "payment_type_4",
  queryKeys: ["payment_type"],
  formControlProps: {
    commonName: "payment_type",
    type: "radio",
    label: "100% оплата при выдаче",
    valuesByChecked: new Map([[false, ""], [true, "4"]]),
    checkedByValue: new Map([["", false], ["4", true]])
  }
};

export const paymentTypeField = {
  id: "payment_type",
  formControlProps: {
    validators: [required]
  }
};

export const sumField = {
  id: "sum",
  formControlProps: {
    mask: maskOnlyNumbers,
    type: "text",
    validators: [required]
  }
};

export const receiptField = {
  id: "receipt",
  formControlProps: {
    type: "text"
  }
};

export const balanceField = {
  id: "balance",
  formControlProps: {
    mask: (value, values) =>
      values.receipt && values.sum ? values.receipt - values.sum : 0,
    type: "text",
    attrs: { readOnly: true, iconClearProps: { hide: true } }
  }
};

export const orderIdField = {
  id: "order_id",
  formControlProps: {
    validators: [required]
  }
};

export const firmIdField = {
  id: "firm_id",
  formControlProps: {
    validators: [required]
  }
};

export const firmNameField = {
  id: "firm_name",
  formControlProps: {
    validators: [required]
  }
};

export const contractIdField = {
  id: "contract_id",
  formControlProps: {
    validators: [required]
  }
};

export const paymentMethodField = {
  id: "payment_method",
  formControlProps: {
    validators: [required]
  }
};

export const paymentMethodField_1 = {
  id: "payment_method_1",
  formControlProps: {
    type: "button",
    render: ({ typeProps, values }) => {
      return (
        <PaymentKind
          active={String(values.payment_method) === "1"}
          value="1"
          name="payment_method"
          onClick={typeProps.button.onClick}
          type="button"
        >
          <PaymentIconWrap>
            <PaymentIcon icon="local_atm" />
          </PaymentIconWrap>
          <PaymentTitle>Наличные</PaymentTitle>
        </PaymentKind>
      );
    }
  }
};

export const paymentMethodField_2 = {
  id: "payment_method_2",
  formControlProps: {
    type: "button",
    render: ({ typeProps, values }) => {
      return (
        <PaymentKind
          active={String(values.payment_method) === "2"}
          value="2"
          name="payment_method"
          onClick={typeProps.button.onClick}
          type="button"
        >
          <PaymentIconWrap>
            <PaymentIcon icon="local_atm" />
          </PaymentIconWrap>
          <PaymentTitle>Эквайринг</PaymentTitle>
        </PaymentKind>
      );
    }
  }
};

export const withoutCheckField = {
  id: "without_check",
  formControlProps: {
    type: "checkbox",
    valuesByChecked: new Map([[false, false], [true, true]]),
    checkedByValue: new Map([[false, false], [true, true]]),
    render: ({ typeProps, values, field }) => {
      return (
        <CheckBlock>
          <Checkbox
            id={generateId()}
            name={field.id}
            type="checkbox"
            disabled={!!values[internetAcquiringField.id]}
            checked={!!values[field.id]}
            {...typeProps.checkbox}
            text="НЕ ПЕЧАТАТЬ ЧЕК"
          />
        </CheckBlock>
      );
    }
  }
};

export const internetAcquiringField = {
  id: "internet_acquiring",
  formControlProps: {
    type: "checkbox",
    valuesByChecked: new Map([[false, false], [true, true]]),
    checkedByValue: new Map([[false, false], [true, true]]),
    render: ({ typeProps, values, field }) => {
      return (
        <CheckBlock>
          <Checkbox
            id={generateId()}
            name={field.id}
            type="checkbox"
            disabled={!!values[withoutCheckField.id]}
            checked={!!values[field.id]}
            {...typeProps.checkbox}
            text="ИНТЕРНЕТ-ЭКВАЙРИНГ"
          />
        </CheckBlock>
      );
    }
  }
};

export const paymentStateField = {
  id: "payment_state",
  formControlProps: {
    validators: [required]
  }
};

export const paymentPlaceField = {
  id: "payment_place",
  formControlProps: {
    validators: [required]
  }
};

export const fieldsForReturnPaymentForm = renderFormFields([
  billIdField,
  agentIdField,
  receiptAddressField,
  paymentTypeField_1,
  paymentTypeField_2,
  paymentTypeField_3,
  paymentTypeField_4,
  paymentTypeField,
  sumField,
  firmIdField,
  firmNameField,
  contractIdField,
  receiptField,
  balanceField,
  paymentMethodField,
  paymentMethodField_1,
  paymentMethodField_2,
  withoutCheckField,
  internetAcquiringField,
  orderIdField,
  paymentStateField,
  paymentPlaceField
]);

export const fieldsForCreatePaymentForm = renderFormFields([
  billIdField_v2,
  agentIdField,
  receiptAddressField,
  paymentTypeField_1,
  paymentTypeField_2,
  paymentTypeField_3,
  paymentTypeField_4,
  paymentTypeField,
  sumField,
  firmIdField,
  firmNameField,
  contractIdField,
  receiptField,
  balanceField,
  paymentMethodField,
  paymentMethodField_1,
  paymentMethodField_2,
  orderIdField,
  paymentStateField,
  paymentPlaceField
]);
