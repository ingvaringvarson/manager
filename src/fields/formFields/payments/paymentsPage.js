import React, { Fragment } from "react";
import { Link } from "react-router-dom";

import {
  renderDateTime,
  renderEnumerationValue,
  renderFormFields,
  disableFieldHandler,
  objectIdCreateForUserHandler,
  getNameCompanyShort,
  getIdForEntity
} from "../../helpers";
import {
  maskOnlyNumbers,
  maskDateEndDay,
  maskDateBeginningDay,
  maskOnlyText
} from "../../../helpers/masks";
import { maxDateRange, minDateRange } from "../../../helpers/validators";
import { sortByField } from "../../templates/serviceTemplates";
import { getUrl } from "../../../helpers/urls";
import { searchRequestPropsForAgents } from "../../../components/common/form/SearchRequestControl/searchRequestProps";
import { handleStopLifting } from "../../../components/common/tableControls";

export const agentCode = {
  id: "_agent_code",
  title: "ID клиента",
  headCellProps: { sortBy: true, sortName: "agent_code" },
  bodyCellProps: {
    render: ({ entity }) =>
      entity && entity.agent_code ? entity.agent_code : ""
  },
  formControlProps: {
    type: "text",
    mask: maskOnlyNumbers,
    attrs: { placeholder: "Введите код создателя платежа" },
    afterChange: objectIdCreateForUserHandler
  },
  afterMap: objectIdCreateForUserHandler
};
export const agentName = {
  id: "agent_name",
  title: "ФИО",
  formControlProps: {
    type: "text",
    attrs: {
      disabled: true
    }
  }
};
export const code = {
  id: "code",
  title: "Номер",
  headCellProps: { sortBy: true },
  formControlProps: {
    mask: maskOnlyText,
    type: "text",
    attrs: {
      placeholder: "Поиск по номеру"
    }
  }
};
export const paymentState = {
  id: "payment_state",
  title: "Тип транзакции",
  enumerationKey: "paymentState",
  bodyCellProps: {
    render: renderEnumerationValue("paymentState", "payment_state")
  },
  formControlProps: {
    type: "select",
    attrs: {
      placeholder: "Тип транзакции"
    }
  }
};
export const storeId = {
  id: "store_id",
  title: "Торговая точка",
  bodyCellProps: {
    render: ({ entity }) =>
      entity && entity.store_name ? entity.store_name : ""
  },
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "Поиск по ID торговой точки"
    }
  }
};
export const sum = {
  id: "sum",
  title: "Сумма",
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "Поиск по сумме"
    }
  }
};
export const objectDateCreate = {
  id: "object_date_create",
  title: "Дата создания",
  queryKeys: ["datefrom_created", "dateto_created"],
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: renderDateTime("object_date_create")
  },
  formControlProps: {
    type: "date",
    attrs: {
      placeholder: "Поиск по дате создания"
    }
  }
};
export const firmId = {
  id: "firm_id",
  title: "Торговая точка",
  bodyCellProps: {
    render: ({ entity }) => (entity && entity.firm_name ? entity.firm_name : "")
  },
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Введите название организации" },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      getName: getNameCompanyShort,
      getId: getIdForEntity,
      params: {
        roles: [2]
      }
    })
  }
};
export const receiptDate = {
  id: "receipt_date",
  title: "Дата поступления",
  bodyCellProps: {
    render: renderDateTime("receipt_date", false)
  },
  formControlProps: {
    type: "date",
    attrs: {
      placeholder: "Поиск по дате поступления"
    }
  }
};
export const billCode = {
  id: "_bill_code",
  title: "Счета",
  bodyCellProps: {
    render: ({ entity }) => {
      if (!entity.payments_inf) return null;

      return entity.payments_inf.reduce((bills, info) => {
        if (info.bill_code && info.bill_id) {
          bills.push(
            <Fragment key={info.bill_id}>
              {!!bills.length && <br />}№{" "}
              <Link
                to={getUrl("bills", { id: info.bill_id })}
                onClick={handleStopLifting}
              >
                {info.bill_code}
              </Link>
            </Fragment>
          );
        }

        return bills;
      }, []);
    }
  },
  formControlProps: {
    mask: maskOnlyNumbers,
    type: "text",
    attrs: { placeholder: "Введите код счёта" }
  }
};

export const fieldsForTableByType = {
  1: renderFormFields([
    agentCode,
    agentName,
    code,
    paymentState,
    firmId,
    receiptDate,
    sum,
    objectDateCreate,
    billCode,
    sortByField
  ]),
  2: renderFormFields([
    agentCode,
    agentName,
    code,
    paymentState,
    firmId,
    receiptDate,
    sum,
    objectDateCreate,
    billCode,
    sortByField
  ]),
  3: renderFormFields([
    agentCode,
    agentName,
    code,
    paymentState,
    storeId,
    sum,
    objectDateCreate,
    billCode,
    sortByField
  ])
};

const dateFromCreatedField = {
  id: "datefrom_created",
  formControlProps: {
    validators: [maxDateRange("dateto_created")],
    mask: maskDateBeginningDay,
    type: "date",
    attrs: { placeholder: "Выберите дату" }
  }
};
const dateToCreatedField = {
  id: "dateto_created",
  formControlProps: {
    validators: [minDateRange("datefrom_created")],
    mask: maskDateEndDay,
    type: "date",
    attrs: { placeholder: "Выберите дату" }
  }
};
const orderCode = {
  id: "_order_code",
  title: "Введите номер заказа",
  mask: maskOnlyText,
  formControlProps: {
    type: "text",
    label: "Заказ",
    attrs: { placeholder: "Введите номер заказа" }
  }
};
const forUser = {
  id: "_for_user",
  title: "Мной созданные платежи",
  formControlProps: {
    type: "checkbox",
    valuesByChecked: new Map([[false, ""], [true, "1"]]),
    checkedByValue: new Map([["", false], ["1", true]]),
    label: "Мной созданные платежи",
    afterChange: ({ field, values }) => {
      if (
        (values.hasOwnProperty("object_id_create") &&
          values.object_id_create.length) ||
        (values.hasOwnProperty("_agent_code") && values._for_user !== "")
      ) {
        return disableFieldHandler(field);
      }

      return field;
    }
  },
  afterMap: ({ field, values }) => {
    if (
      (values.hasOwnProperty("object_id_create") &&
        values.object_id_create.length) ||
      (values.hasOwnProperty("_agent_code") && values._for_user !== "")
    ) {
      return disableFieldHandler(field);
    }

    return field;
  }
};
export const fieldsForFilters = renderFormFields(
  [
    [
      { id: null, name: "Поиск по дате создания", destructuring: false },
      [dateFromCreatedField, dateToCreatedField]
    ],
    orderCode,
    forUser
  ],
  true
);

const comment = {
  id: "comment",
  title: "Комментарий",
  formControlProps: {
    type: "text",
    attrs: {
      disabled: true
    }
  }
};

const purpose = {
  id: "purpose",
  title: "Назначение",
  formControlProps: {
    type: "text",
    attrs: {
      disabled: true
    }
  }
};

export const fieldsForTableByTypeTab = {
  1: renderFormFields([
    code,
    paymentState,
    storeId,
    sum,
    objectDateCreate,
    billCode,
    comment,
    sortByField
  ]),
  2: renderFormFields([
    code,
    paymentState,
    storeId,
    sum,
    objectDateCreate,
    billCode,
    comment,
    sortByField
  ]),
  3: renderFormFields([
    code,
    paymentState,
    firmId,
    receiptDate,
    sum,
    purpose,
    objectDateCreate,
    billCode,
    comment,
    sortByField
  ])
};
