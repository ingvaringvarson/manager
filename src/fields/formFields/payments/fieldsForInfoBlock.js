import React from "react";
import { getValueInDepth } from "../../../utils";
import {
  renderInfoFields,
  mapValuesToFields,
  mapEntitiesToList,
  mapValuesToGroupedListByContacts,
  mapEntitiesToGroupedListByContacts
} from "../../helpers/index";
import { fields } from "../clients/fieldsForEditForms";
import { Link } from "react-router-dom";
import { getUrl } from "../../../helpers/urls";

const EntityRecord = {
  title: null,
  subtitle: "",
  formMain: null,
  formField: null,
  view: "default",
  infoFields: [],
  formFields: [],
  fieldsForAdd: [],
  mapValuesToFields
};

export const fieldGroups = {
  client: {
    ...EntityRecord,
    title: "Клиент",
    formMain: {
      icon: "account_box"
    },
    infoFields: renderInfoFields([
      {
        key: "person",
        id: "person_surname",
        title: "Фамилия",
        mask: value => getValueInDepth(value, ["person_surname"])
      },
      {
        key: "person",
        id: "person_name",
        title: "Имя",
        mask: value => getValueInDepth(value, ["person_name"])
      },
      {
        key: "person",
        id: "person_second_name",
        title: "Отчество",
        mask: value => getValueInDepth(value, ["person_second_name"])
      },
      {
        key: "tags",
        title: "Теги",
        enumerationKey: "agentTag"
      },
      {
        key: "segments",
        title: "Сегменты",
        enumerationKey: "agentSegment"
      },
      {
        key: "manager_id",
        title: "Аккаунт-менеджер"
      }
    ])
  },
  company: {
    ...EntityRecord,
    title: "Клиент",
    formMain: {
      icon: "account_box"
    },
    infoFields: renderInfoFields([
      {
        key: "company",
        id: "company_name_full",
        title: "Полное наименование",
        mask: value => getValueInDepth(value, ["company_name_full"])
      },
      {
        key: "company",
        id: "company_name_short",
        title: "Сокращенное наименование",
        mask: value => getValueInDepth(value, ["company_name_short"])
      },
      {
        key: "tags",
        title: "Теги",
        enumerationKey: "agentTag"
      },
      {
        key: "segments",
        title: "Сегменты",
        enumerationKey: "agentSegment"
      },
      {
        key: "manager_id",
        title: "Аккаунт-менеджер"
      }
    ])
  },
  contacts: {
    ...EntityRecord,
    title: "Контакты",
    formMain: {
      subtitle: "Контакты",
      title: "Создание контакта",
      type: "add",
      icon: "add"
    },
    formField: {
      subtitle: "Контакт",
      title: "Редактирование",
      type: "edit"
    },
    view: "groupedList",
    mapValuesToFields: mapEntitiesToGroupedListByContacts,
    formFields: fields["contacts"]
  },
  contact_persons: {
    ...EntityRecord,
    title: "Контактные лица",
    formField: {
      subtitle: "Контактные лица",
      title: "Редактирование",
      type: "edit"
    },
    view: "list",
    infoFields: renderInfoFields([
      {
        key: "name",
        title: "Имя"
      },
      {
        key: "gender",
        title: "Пол",
        enumerationKey: "personGender"
      },
      {
        key: "position",
        title: "Должность"
      },
      {
        key: "contacts",
        mapValueToFields: mapValuesToGroupedListByContacts,
        view: "groupedList"
      }
    ]),
    mapValuesToFields: mapEntitiesToList,
    formFields: fields["contact_persons"]
  },
  orders: {
    ...EntityRecord,
    title: "Информация о заказах",
    view: "list",
    infoFields: renderInfoFields([
      {
        key: "_root",
        title: "Заказ №",
        renderTitle: field => {
          if (
            field.value.order_code === null ||
            field.value.order_id === null
          ) {
            return null;
          }

          return (
            <Link to={getUrl("orders", { id: field.value.id })}>
              `№ ${field.value.bill_code}`
            </Link>
          );
        },
        renderValue: field => {
          if (
            field.value.order_code === null ||
            field.value.order_id === null
          ) {
            return null;
          }
          return field.title;
        }
      }
    ]),
    mapValuesToFields: mapEntitiesToList
  },
  bills: {
    ...EntityRecord,
    title: "Информация о счетах",
    view: "list",
    infoFields: renderInfoFields([
      {
        key: "_root",
        renderTitle: field => {
          return (
            <Link to={getUrl("bills", { id: field.value.id })}>
              `№ ${field.value.bill_code}`
            </Link>
          );
        },
        renderValue: field => `${field.value.sum} руб.`
      }
    ]),
    mapValuesToFields: mapEntitiesToList
  },
  agent_bank_details: {
    ...EntityRecord,
    title: "Банковские реквизиты плательщика",
    infoFields: renderInfoFields([
      {
        key: "settlement_account",
        title: "Расчетный счет"
      },
      {
        key: "correspondent_account",
        title: "Корреспондентский счет"
      },
      {
        key: "bik",
        title: "БИК"
      },
      {
        key: "bank_name",
        title: "Наименование банка"
      }
    ])
  },
  firm_bank_details: {
    ...EntityRecord,
    title: "Банковские реквизиты фирмы",
    infoFields: renderInfoFields([
      {
        key: "settlement_account",
        title: "Расчетный счет"
      },
      {
        key: "correspondent_account",
        title: "Корреспондентский счет"
      },
      {
        key: "bik",
        title: "БИК"
      },
      {
        key: "bank_name",
        title: "Наименование банка"
      }
    ])
  }
};
