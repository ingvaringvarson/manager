import { renderDateTime, renderFormFields } from "../../helpers/index";
import { renderEnumerationValue } from "../../helpers";

export const fieldsForTable_1 = renderFormFields([
  {
    id: "payment_place",
    title: "Место осуществления платежа",
    bodyCellProps: {
      render: renderEnumerationValue("paymentPlace", "payment_place")
    }
  },
  {
    id: "store_name",
    title: "Наименование торговой точки"
  },
  {
    id: "cashbox_name",
    title: "Наименование кассы"
  },
  {
    id: "firm_name",
    title: "Фирма"
  }
]);

export const fieldsForTable_2 = renderFormFields([
  {
    id: "payment_place",
    title: "Место осуществления платежа",
    bodyCellProps: {
      render: renderEnumerationValue("paymentPlace", "payment_place")
    }
  },
  {
    id: "receipt_num",
    title: "Номер п/п"
  },
  {
    id: "receipt_date",
    title: "Дата поступления",
    bodyCellProps: {
      render: renderDateTime("receipt_date", false)
    }
  },
  {
    id: "purpose",
    title: "Назначение"
  },
  {
    id: "firm_name",
    title: "Фирма"
  }
]);

export const fieldsForTable_3 = renderFormFields([
  {
    id: "payment_place",
    title: "Место осуществления платежа",
    bodyCellProps: {
      render: renderEnumerationValue("paymentPlace", "payment_place")
    }
  },
  {
    id: "store_name",
    title: "Наименование торговой точки, откуда перемещают средства"
  },
  {
    id: "cashbox_name",
    title: "Наименование кассы, откуда перемещают средства"
  },
  {
    id: "store_to_name",
    title: "Наименование торговой точки, куда перемещают средства"
  },
  {
    id: "cashbox_to_name",
    title: "Наименование кассы, куда перемещают средства"
  },
  {
    id: "firm_name",
    title: "Фирма"
  }
]);
