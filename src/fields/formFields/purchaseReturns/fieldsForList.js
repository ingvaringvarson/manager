import {
  renderDateTime,
  renderFormFields,
  renderEnumerationValue,
  getIdForEntity,
  getNameCompanyShort
} from "../../helpers";
import { maskOnlyText } from "../../../helpers/masks";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForWarehouses
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

const agentTechCodeField = {
  id: "agent_code",
  title: "Код",
  headCellProps: {
    sortBy: true,
    sortName: "agent_tech.code"
  },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.agent_tech ? entity.agent_tech.code : "";
    }
  },
  formControlProps: {
    type: "text",
    attrs: {
      placeholder: "Поиск по коду поставщика"
    }
  }
};

const agentTechNameField = {
  id: "list_agent_id",
  queryKeys: ["agent_id"],
  title: "Наименование поставщика",
  headCellProps: {
    sortBy: true,
    sortName: "agent_tech.company_name_full"
  },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.agent_tech ? entity.agent_tech.company_name_full : "";
    }
  },
  formControlProps: {
    type: "select_search_request",
    value: [],
    defaultValue: [],
    name: "list_agent_id",
    attrs: { placeholder: "Поиск по наименованию поставщика" },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      getId: getIdForEntity,
      getName: getNameCompanyShort,
      params: {
        roles: [1]
      }
    })
  }
};

const firmNameField = {
  id: "list_firm_id",
  queryKeys: ["firm_id"],
  title: "Наименование заказчика",
  headCellProps: {
    sortBy: true,
    sortName: "firm_name"
  },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.purchase_return ? entity.purchase_return.firm_name : "";
    }
  },
  formControlProps: {
    type: "select_search_request",
    value: [],
    defaultValue: [],
    name: "list_firm_id",
    attrs: { placeholder: "Поиск по наименованию заказчика" },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      getId: getIdForEntity,
      getName: getNameCompanyShort,
      params: {
        roles: [2]
      }
    })
  }
};

const returnCodeField = {
  id: "code",
  title: "Номер возврата",
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.purchase_return ? entity.purchase_return.code : "";
    }
  },
  formControlProps: {
    mask: maskOnlyText,
    type: "text",
    attrs: { placeholder: "Поиск по номеру возврата" }
  }
};

const returnStateField = {
  id: "list_state",
  enumerationKey: "purchaseReturnState",
  queryKeys: ["state"],
  title: "Статус",
  bodyCellProps: {
    render: renderEnumerationValue("purchaseReturnState", [
      "purchase_return",
      "state_return"
    ])
  },
  formControlProps: {
    type: "select",
    attrs: { placeholder: "Поиск по статусу возврата" }
  }
};

const returnWarehouseField = {
  id: "list_warehouse_id",
  queryKeys: ["warehouse_id"],
  title: "Склад отгрузки",
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.purchase_return
        ? entity.purchase_return.warehouse_name
        : "";
    }
  },
  formControlProps: {
    type: "select_search_request",
    value: [],
    defaultValue: [],
    name: "list_warehouse_id",
    attrs: { placeholder: "Поиск по наименованию заказчика" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};

const returnDateObjectField = {
  id: "object_date_create",
  queryKeys: ["datefrom_created", "dateto_created"],
  title: "Дата создания",
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: renderDateTime(["purchase_return", "object_date_create"], true)
  },
  formControlProps: {
    type: "date",
    attrs: {
      placeholder: "Поиск по дате создания возврата"
    }
  }
};

const returnAuthor = {
  id: "list_object_id_create",
  queryKeys: ["object_id_create"],
  title: "Автор",
  headCellProps: {
    sortBy: true,
    sortName: "object_name_create"
  },
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.purchase_return
        ? entity.purchase_return.object_name_create
        : "";
    }
  },
  formControlProps: {
    type: "select_search_request",
    value: [],
    defaultValue: [],
    name: "list_object_id_create",
    attrs: { placeholder: "Поиск по автору возврата" },
    searchRequestProps: searchRequestPropsForAgents({
      params: {
        roles: [5]
      }
    })
  }
};

export const fields = renderFormFields([
  agentTechCodeField,
  agentTechNameField,
  firmNameField,
  returnCodeField,
  returnStateField,
  returnWarehouseField,
  returnDateObjectField,
  returnAuthor
]);
