import React from "react";
import { renderFormFields } from "../../helpers";
import {
  brandField,
  articleField,
  nameField
} from "../../templates/tableCellTemplates/positionFields";
import {
  valueGreaterOrEqualThan,
  valueLessOrEqualThan,
  required
} from "../../../helpers/validators";
import FieldContext from "../../../components/common/formControls/FieldContext";
import { searchRequestPropsForWarehouses } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

export const fieldsForForm = renderFormFields([
  {
    id: "warehouse_id",
    formControlProps: {
      name: "warehouse_id",
      label: "Склад отгрузки",
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Выберите склад" },
      searchRequestProps: searchRequestPropsForWarehouses()
    },
    afterMap: ({ fields, field, values }) => {
      if (
        !(
          values &&
          values.positions &&
          Array.isArray(values.positions) &&
          values.positions.length
        )
      )
        return field;

      const posWithLowestCode = values.positions.reduce(
        (result, currentPosition) => {
          if (currentPosition.code < result.code) {
            return currentPosition;
          }
          return result;
        },
        values.positions[0]
      );

      return {
        ...field,
        formControlProps: {
          ...field.formControlProps,
          defaultValue: [posWithLowestCode.warehouse_id],
          value: [posWithLowestCode.warehouse_id]
        }
      };
    }
  }
]);

const bodyCellProps = {
  render: ({ entity, field }) => {
    const name = `${field.id}__${entity._id}`;
    return <FieldContext name={name} />;
  }
};

export const fieldsViewForTable = renderFormFields([
  brandField,
  articleField,
  nameField,
  {
    id: "count",
    title: "Количество",
    bodyCellProps
  },
  {
    id: "returnCount",
    title: "Вернуть",
    bodyCellProps
  },
  {
    id: "reason",
    title: "Причина возврата",
    bodyCellProps
  }
]);

export const fieldsFormForTable = renderFormFields([
  [
    { destructuring: true, id: "fields", name: null },
    [
      {
        id: "count__",
        shouldNotBeInValue: true,
        formControlProps: {
          type: "text",
          attrs: {
            placeholder: "Количество",
            disabled: true
          }
        }
      },
      {
        id: "returnCount__",
        shouldNotBeInValue: true,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "Вернуть" }
        },
        afterMap: ({ field, fields, values }) => {
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              value: values.count,
              validators: [
                valueGreaterOrEqualThan(1),
                valueLessOrEqualThan(values.count),
                required
              ]
            }
          };
        }
      },
      {
        id: "reason__",
        shouldNotBeInValue: true,
        enumerationKey: "returnReason",
        formControlProps: {
          type: "select",
          attrs: { placeholder: "Выберите причину" },
          defaultValue: [1],
          value: [1]
        }
      }
    ]
  ]
]);
