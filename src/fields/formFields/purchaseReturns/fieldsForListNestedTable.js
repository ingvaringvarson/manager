import { renderFormFields } from "../../helpers";
import {
  brandField,
  articleField,
  nameField,
  statusField,
  countField,
  priceField
} from "../../templates/tableCellTemplates/positionFields";

const bodyCellProps = {
  render: ({ entity, field }) => {
    return entity.position && entity.position.product
      ? entity.position.product[field.id]
      : "";
  }
};

export const fields = renderFormFields([
  {
    ...brandField,
    bodyCellProps
  },
  {
    ...articleField,
    bodyCellProps
  },
  {
    ...nameField,
    bodyCellProps
  },
  statusField,
  {
    ...countField,
    bodyCellProps: {
      render: ({ entity }) => {
        return entity.position ? entity.position.count : "";
      }
    }
  },
  priceField
]);
