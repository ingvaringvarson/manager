import React from "react";
import { renderFormFields } from "../../helpers";
import {
  valueGreaterOrEqualThan,
  valueLessOrEqualThan,
  required
} from "../../../helpers/validators";
import {
  brandField,
  articleField,
  nameField
} from "./../../templates/tableCellTemplates/positionFields";
import FormField from "../../../components/common/formControls/FieldContext";

export const tableFields = renderFormFields([
  brandField,
  articleField,
  nameField,
  {
    id: "count",
    title: "Количество",
    bodyCellProps: {
      render: ({ entity }) => <FormField name={`count__${entity._id}`} />
    }
  },
  {
    id: "return",
    title: "Отказаться от",
    bodyCellProps: {
      render: ({ entity }) => <FormField name={`return__${entity._id}`} />
    }
  }
]);

export const formFields = renderFormFields([
  [
    { destructuring: true, id: "fields", name: null },
    [
      {
        id: "count__",
        formControlProps: {
          type: "text",
          attrs: {
            disabled: true
          }
        },
        afterMap: ({ field, fields, values }) => {
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              value: values.count
            }
          };
        }
      },
      {
        id: "return__",
        shouldNotBeInValue: true,
        formControlProps: {
          type: "text",
          attrs: { placeholder: "Количество" }
        },
        afterMap: ({ field, values }) => {
          const { count } = values;
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              value: count,
              validators: [
                valueGreaterOrEqualThan(1),
                valueLessOrEqualThan(count),
                required
              ]
            }
          };
        }
      }
    ]
  ]
]);
