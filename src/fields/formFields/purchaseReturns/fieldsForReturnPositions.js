import { renderFormFields } from "../../helpers";
import {
  brandField,
  articleField,
  nameField,
  statusField,
  priceField,
  countField,
  sumField,
  purposeField,
  reasonField
} from "../../templates/tableCellTemplates/positionFields";

const bodyCellProps = {
  render: ({ entity, field }) => {
    return entity.position && entity.position.product
      ? entity.position.product[field.id]
      : "";
  }
};

export const fields = renderFormFields([
  {
    ...brandField,
    bodyCellProps
  },
  {
    ...articleField,
    bodyCellProps
  },
  {
    ...nameField,
    bodyCellProps
  },
  statusField,
  priceField,
  countField,
  sumField,
  {
    ...purposeField,
    bodyCellProps: {
      render: ({ entity, enumerations }) => {
        const returnPurpose =
          enumerations["returnPurpose"] && enumerations["returnPurpose"].map
            ? enumerations["returnPurpose"].map
            : null;
        const purpose = entity.position ? entity.position.purpose : null;

        if (!returnPurpose || purpose === null) return null;

        return returnPurpose[purpose] ? returnPurpose[purpose].name : "";
      }
    }
  },
  reasonField
]);
