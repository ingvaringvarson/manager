import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import {
  renderFormFields,
  renderEnumerationValue,
  getNameCompanyShort,
  getIdForEntity
} from "../../helpers";
import { getUrl } from "../../../helpers/urls";
import { getDate, getTime, getValueInDepth } from "../../../utils";
import { maskOnlyText } from "../../../helpers/masks";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForWarehouses
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

const StyledLink = styled(Link)`
  color: #336ea8;
  cursor: pointer;
  text-decoration: none;
`;

// MAIN TABLE --- START
const codeField = {
  id: "code",
  title: "Номер возврата",
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      return (
        <StyledLink
          to={getUrl("purchaseReturns", { id: entity.purchase_return.id })}
        >
          {entity.purchase_return.code}
        </StyledLink>
      );
    }
  },
  formControlProps: {
    mask: maskOnlyText,
    type: "text",
    attrs: { placeholder: "Поиск по номеру" }
  }
};

const stateField = {
  id: "state",
  title: "Статус",
  enumerationKey: "purchaseReturnState",
  bodyCellProps: {
    render: renderEnumerationValue("purchaseReturnState", [
      "purchase_return",
      "state_return"
    ])
  },
  formControlProps: {
    type: "select",
    attrs: { placeholder: "Поиск по статусу" }
  }
};

const firmField = {
  id: "firm_name",
  title: "Фирма",
  headCellProps: { sortBy: true },
  queryKeys: ["firm_id"],
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.purchase_return && entity.purchase_return.firm_name;
    }
  },
  formControlProps: {
    type: "select_search_request",
    value: [],
    defaultValue: [],
    attrs: { placeholder: "Введите название организации" },
    searchRequestProps: searchRequestPropsForAgents({
      keyById: "id",
      getName: getNameCompanyShort,
      getId: getIdForEntity,
      params: {
        roles: [2]
      }
    })
  }
};

const warehouseField = {
  id: "warehouse_name",
  title: "Склад отгрузки",
  headCellProps: { sortBy: true },
  queryKeys: ["warehouse_id"],
  bodyCellProps: {
    render: ({ entity }) => {
      return entity.purchase_return.warehouse_name;
    }
  },
  formControlProps: {
    type: "select_search_request",
    value: [],
    defaultValue: [],
    attrs: { placeholder: "Поиск по складу приемки" },
    searchRequestProps: searchRequestPropsForWarehouses()
  }
};

const sumTotalField = {
  id: "sum_total",
  title: "Сумма",
  bodyCellProps: {
    render: ({ entity }) => {
      const sum = entity.purchase_return && entity.purchase_return.sum_total;
      return sum || sum === 0 ? `${sum} руб.` : "";
    }
  },
  formControlProps: {
    type: "text",
    attrs: { placeholder: "Поиск по сумме", disabled: true }
  }
};

const sumToReturnField = {
  id: "sum_payment",
  title: "Сумма к возврату",
  bodyCellProps: {
    render: ({ entity }) => {
      const sum = entity.purchase_return && entity.purchase_return.sum_payment;
      return sum || sum === 0 ? `${sum} руб.` : "";
    }
  },
  formControlProps: {
    type: "text",
    attrs: { placeholder: "Поиск по сумме к возврату", disabled: true }
  }
};

const objectDateCreateField = {
  id: "object_date_create",
  queryKeys: ["datefrom_created", "dateto_created"],
  title: "Дата создания	",
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      if (!entity.purchase_return.object_date_create) return null;

      const date = new Date(entity.purchase_return.object_date_create);
      return `${getDate(date)}, ${getTime(date)}`;
    }
  },
  formControlProps: {
    type: "date",
    attrs: { placeholder: "Поиск по дате" }
  }
};

const authorField = {
  id: "object_name_create",
  queryKeys: ["object_id_create"],
  title: "Автор",
  headCellProps: { sortBy: true },
  bodyCellProps: {
    render: ({ entity }) => {
      return (
        entity.purchase_return && entity.purchase_return.object_name_create
      );
    }
  },
  formControlProps: {
    type: "select_search_request",
    value: [],
    defaultValue: [],
    attrs: { placeholder: "Поиск по автору" },
    searchRequestProps: searchRequestPropsForAgents({
      params: {
        roles: [5]
      }
    })
  }
};
// MAIN TABLE --- END

export const fields = renderFormFields([
  codeField,
  stateField,
  firmField,
  warehouseField,
  sumTotalField,
  sumToReturnField,
  objectDateCreateField,
  authorField
]);
