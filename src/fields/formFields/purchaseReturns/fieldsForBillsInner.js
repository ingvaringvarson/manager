import { renderFormFields } from "../../helpers";
import {
  brandField,
  articleField,
  nameField,
  typeField,
  sumBillPositionField
} from "../../templates/tableCellTemplates/positionFields";

export const billPositionFields = renderFormFields([
  brandField,
  articleField,
  nameField,
  typeField,
  sumBillPositionField
]);

export const billPaymentFields = renderFormFields([
  {
    id: "payment_code",
    title: "Платеж"
  },
  {
    id: "sum",
    title: "Сумма",
    bodyCellProps: {
      render: ({ entity, field }) => {
        return entity && entity[field.id] ? `${entity[field.id]} руб.` : "";
      }
    }
  }
]);
