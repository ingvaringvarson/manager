import {
  renderFormFields,
  getNameCompanyShort,
  getIdForEntity
} from "../../helpers";
import { maskDateBeginningDay, maskDateEndDay } from "../../../helpers/masks";
import {
  maxDateRange,
  minDateRange,
  maxLength,
  minLength
} from "../../../helpers/validators";
import {
  searchRequestPropsForAgents,
  searchRequestPropsForWarehouses
} from "../../../components/common/form/SearchRequestControl/searchRequestProps";

export const fields = renderFormFields([
  [
    { id: null, name: "Поиск по дате создания", destructuring: false },
    [
      {
        id: "datefrom_created",
        formControlProps: {
          validators: [maxDateRange("dateto_created")],
          mask: maskDateBeginningDay,
          type: "date",
          attrs: { placeholder: "Выберите дату" }
        }
      },
      {
        id: "dateto_created",
        formControlProps: {
          validators: [minDateRange("datefrom_created")],
          mask: maskDateEndDay,
          type: "date",
          attrs: { placeholder: "Выберите дату" }
        }
      }
    ]
  ],
  {
    id: "firm_id",
    formControlProps: {
      name: "firm_id",
      label: "Организация",
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Введите название организации" },
      searchRequestProps: searchRequestPropsForAgents({
        keyById: "id",
        getName: getNameCompanyShort,
        getId: getIdForEntity,
        params: {
          roles: [2]
        }
      })
    }
  },
  {
    id: "agent_id",
    formControlProps: {
      name: "agent_id",
      label: "Поставщик",
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Выберите поставщика" },
      searchRequestProps: searchRequestPropsForAgents({
        keyById: "id",
        getName: getNameCompanyShort,
        getId: getIdForEntity,
        params: {
          roles: [1]
        }
      })
    }
  },
  {
    id: "warehouse_id",
    formControlProps: {
      name: "warehouse_id",
      label: "Склад отгрузки",
      value: [],
      defaultValue: [],
      type: "select_search_request",
      attrs: { placeholder: "Выберите склад" },
      searchRequestProps: searchRequestPropsForWarehouses()
    }
  },
  {
    id: "article",
    formControlProps: {
      name: "article",
      label: "Артикул",
      type: "text",
      attrs: { placeholder: "Введите артикул" }
    }
  },
  {
    id: "object_id_create",
    queryKeys: ["object_id_create"],
    formControlProps: {
      value: [],
      defaultValue: [],
      type: "select_search_request",
      name: "object_id_create",
      attrs: { placeholder: "Введите код создателя возврата" },
      label: "Создатель возврата",
      searchRequestProps: searchRequestPropsForAgents({
        key: "code"
      })
    }
  },
  {
    id: "history_id",
    formControlProps: {
      type: "text",
      label: "Старый id",
      attrs: { placeholder: "Введите старый id" },
      validators: [maxLength(13), minLength(1)]
    }
  },
  {
    id: "_for_user",
    title: "Мной созданные возвраты",
    formControlProps: {
      type: "checkbox",
      valuesByChecked: new Map([[false, ""], [true, "1"]]),
      checkedByValue: new Map([["", false], ["1", true]]),
      label: "Мной созданные возвраты"
    }
  },
  {
    id: "state",
    queryKeys: ["state"],
    enumerationKey: "purchaseReturnState",
    formControlProps: {
      label: "Статус возврата",
      type: "select",
      isMultiple: true,
      value: [],
      defaultValue: [],
      name: "state",
      attrs: { placeholder: "Выберите один или несколько статусов" }
    }
  },
  {
    id: "good_state",
    enumerationKey: "orderPositionGoodState",
    formControlProps: {
      label: "Статус позиции возврата",
      type: "select",
      isMultiple: true,
      value: [],
      defaultValue: [],
      name: "good_state",
      attrs: { placeholder: "Выберите один или несколько статусов" }
    }
  },
  {
    id: "reason",
    enumerationKey: "returnReason",
    formControlProps: {
      label: "Причина возврата",
      type: "select",
      isMultiple: true,
      value: [],
      defaultValue: [],
      name: "reason",
      attrs: { placeholder: "Выберите одну или несколько причин" }
    }
  }
]);
