import { renderFormFields } from "../../helpers/index";

export const fieldsForTable = renderFormFields([
  {
    id: "brandName",
    title: "Бренд"
  },
  {
    id: "vendorCode",
    title: "Артикул",
    bodyCellProps: {
      render: ({ field, enumerations, entity }) => {
        if (!entity.vendorCodes.length) return null;

        const actualVendorCode = entity.vendorCodes.find(item => item.isActual);

        return actualVendorCode
          ? actualVendorCode.vendorCode
          : entity.vendorCodes[0].vendorCode;
      }
    }
  },
  {
    id: "productName",
    title: "Наименование"
  }
]);
