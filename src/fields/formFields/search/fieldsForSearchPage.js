import { renderFormFields } from "../../helpers";
import { clientCodeField } from "../../templates/formFieldTemplates/text";

export const fieldsForSearchForm = renderFormFields([
  {
    id: "searchString",
    formControlProps: {
      type: "text",
      attrs: { placeholder: "Введите артикул запчасти" }
    }
  }
]);

export const fieldsForBasketForm = renderFormFields([
  {
    id: "basketId",
    enumerationKey: "basketList",
    formControlProps: {
      type: "select",
      attrs: { placeholder: "Выберите корзину" }
    }
  }
]);

export const fieldsForClientForm = renderFormFields([clientCodeField]);
