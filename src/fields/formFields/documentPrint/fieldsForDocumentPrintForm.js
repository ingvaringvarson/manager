import React from "react";

import FormField from "../../../components/common/formControls/FieldContext";

import { renderFormFields } from "../../helpers";

const flexCellProps = {
  cellProps: {
    flex: "2"
  }
};

export const fields = renderFormFields([
  {
    id: "id",
    title: "Документ",
    enumerationKey: "documentTemplate",
    headCellProps: {
      ...flexCellProps
    },
    bodyCellProps: {
      ...flexCellProps
    }
  },
  {
    id: "doc_types",
    title: "Расширение",
    bodyCellProps: {
      render: ({ entity }) => <FormField name={`doc_types__${entity._id}`} />
    }
  },
  {
    id: "with_stamp",
    title: "С печатью фирмы",
    bodyCellProps: {
      render: ({ entity }) => <FormField name={`with_stamp__${entity._id}`} />
    }
  }
]);

const getDocumentsTypeOptions = (docTypes, enumerations) =>
  docTypes
    .filter(t => enumerations["documentsType"].map[t] !== undefined)
    .map(t => {
      let { id, name } = enumerations["documentsType"].map[t];
      name = `.${name.toLowerCase()}`;
      return { id, name };
    });

export const formFields = renderFormFields([
  [
    { destructuring: true, id: "fields", name: null },
    [
      {
        id: "id__",
        title: "Документ",
        isActive: false
      },
      {
        id: "doc_types__",
        formControlProps: {
          type: "select",
          attrs: {
            iconClearProps: {
              hide: true
            }
          }
        },
        afterMap: ({ field, enumerations, values }) => {
          if (!values.doc_types || !values.doc_types.length) {
            return field;
          }

          const docTypes = values.doc_types;
          const defaultValue = docTypes.includes(2) ? 2 : docTypes[0];
          return {
            ...field,
            formControlProps: {
              ...field.formControlProps,
              value: defaultValue,
              options: getDocumentsTypeOptions(docTypes, enumerations)
            }
          };
        }
      },
      {
        id: "with_stamp__",
        shouldNotBeInValue: true,
        formControlProps: {
          type: "select",
          options: [
            {
              id: "true",
              name: "да"
            },
            {
              id: "false",
              name: "нет"
            }
          ],
          value: "false",
          attrs: {
            iconClearProps: {
              hide: true
            }
          }
        }
      }
    ]
  ]
]);
