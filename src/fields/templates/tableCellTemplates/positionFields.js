import { renderEnumerationValue } from "./../../helpers/renderCell";
import { getValueInDepth } from "../../../utils";

const generateBody = keyPath => {
  return {
    render: ({ entity }) => getValueInDepth(entity, keyPath)
  };
};
export const brandField = {
  id: "brand",
  title: "Товар",
  bodyCellProps: generateBody(["product", "brand"])
};

export const returnBrandField = {
  id: "brand",
  title: "Товар",
  bodyCellProps: generateBody(["position", "product", "brand"])
};

export const articleField = {
  id: "article",
  bodyCellProps: generateBody(["product", "article"])
};

export const returnArticleField = {
  id: "article",
  bodyCellProps: generateBody(["position", "product", "article"])
};

export const nameField = {
  id: "name",
  bodyCellProps: generateBody(["product", "name"])
};

export const returnNameField = {
  id: "name",
  bodyCellProps: generateBody(["position", "product", "name"])
};

const renderServiceState = renderEnumerationValue(
  "orderPositionServiceState",
  "service_state"
);
const renderGoodState = renderEnumerationValue(
  "orderPositionGoodState",
  "good_state"
);

export const renderOrderPositionState = props => {
  const isServiceType = [2, 3, 5].includes(props.entity.product.type);
  return isServiceType ? renderServiceState(props) : renderGoodState(props);
};

export const statusField = {
  id: "good_state",
  title: "Статус",
  bodyCellProps: {
    render: props =>
      renderOrderPositionState({
        ...props,
        entity: props.entity.position
      })
  }
};

export const priceField = {
  id: "price",
  title: "Цена",
  bodyCellProps: {
    render: ({ entity }) => {
      const price =
        entity.position && entity.position.price && entity.position.price !== 0
          ? entity.position.price
          : null;
      return price !== null ? `${price} руб.` : "";
    }
  }
};

export const countField = {
  id: "count",
  title: "Кол-во",
  bodyCellProps: {
    render: ({ entity }) => {
      const count =
        entity.position && entity.position.count && entity.position.count !== 0
          ? entity.position.count
          : null;
      return count;
    }
  }
};

export const sumField = {
  id: "sum",
  title: "Сумма",
  bodyCellProps: {
    render: ({ entity }) => {
      const sum =
        entity.position && entity.position.sum && entity.position.sum !== 0
          ? entity.position.sum
          : null;
      return sum === null ? "" : `${sum} руб.`;
    }
  }
};

export const purposeField = {
  id: "purpose",
  title: "Назначение возврата",
  bodyCellProps: {
    render: ({ entity, enumerations }) => {
      const returnPurpose =
        enumerations["returnPurpose"] && enumerations["returnPurpose"].map
          ? enumerations["returnPurpose"].map
          : null;
      const purpose = entity ? entity.purpose : null;

      if (!returnPurpose || purpose === null) return null;

      return returnPurpose[purpose] ? returnPurpose[purpose].name : "";
    }
  }
};

export const reasonField = {
  id: "reason",
  title: "Причина возврата",
  bodyCellProps: {
    render: ({ entity, enumerations }) => {
      const returnReason =
        enumerations["returnReason"] && enumerations["returnReason"].map
          ? enumerations["returnReason"].map
          : null;
      const reason = entity ? entity.reason : null;

      if (!returnReason || reason === null) return null;

      return returnReason[reason] ? returnReason[reason].name : "";
    }
  }
};

export const typeField = {
  id: "type",
  title: "Тип",
  enumerationKey: "productType",
  bodyCellProps: {
    render: ({ entity, enumerations, field }) => {
      const productType =
        enumerations[field.enumerationKey] &&
        enumerations[field.enumerationKey].map
          ? enumerations[field.enumerationKey].map
          : null;
      const type = entity && entity.product ? entity.product.type : null;
      if (!productType || type === null) return null;

      return productType[type] ? productType[type].name : "";
    }
  }
};

export const sumBillPositionField = {
  id: "sum",
  title: "Сумма",
  bodyCellProps: {
    render: ({ entity }) => {
      const sum = entity && entity.sum && entity.sum !== 0 ? entity.sum : null;
      return sum === null ? "" : `${sum} руб.`;
    }
  }
};
