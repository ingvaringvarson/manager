import { maskOnlyNumbers, maskOnlyText } from "../../../helpers/masks";
import {
  renderOrderState,
  renderDateTime,
  forUserAgentCodeHandler
} from "../../helpers";
import { searchRequestPropsForAgents } from "../../../components/common/form/SearchRequestControl/searchRequestProps";

export const agentCodeField = {
  id: "agent_code",
  title: "ID клиента",
  formControlProps: {
    mask: maskOnlyNumbers,
    type: "text",
    attrs: { placeholder: "Введите ID клиента" }
  }
};

export const contactsValueField = {
  id: "contacts_value",
  title: "Телефон",
  formControlProps: {
    mask: maskOnlyNumbers,
    type: "text",
    attrs: { placeholder: "Введите телефон" }
  }
};

export const codeField = {
  id: "code",
  title: "Номер",
  headCellProps: {
    sortBy: true
  },
  formControlProps: {
    mask: maskOnlyText,
    type: "text",
    attrs: { placeholder: "Введите номер" }
  }
};

export const stateField = {
  id: "state",
  title: "Статус",
  bodyCellProps: {
    render: renderOrderState("orderState", "state")
  },
  enumerationKey: "orderState",
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select",
    attrs: { placeholder: "Выберите статус" }
  }
};

export const objectDateCreate = {
  id: "object_date_create",
  title: "Дата создания",
  queryKeys: ["datefrom_created", "dateto_created"],
  bodyCellProps: {
    render: renderDateTime("object_date_create")
  },
  headCellProps: {
    sortBy: true
  },
  formControlProps: {
    type: "date",
    attrs: { placeholder: "Введите дату создания" }
  }
};

export const objectIdCreateField = {
  id: "object_name_create",
  title: "Автор",
  bodyCellProps: {
    render: ({ entity, field }) => {
      return entity[field.id];
    }
  },
  formControlProps: {
    value: [],
    defaultValue: [],
    type: "select_search_request",
    attrs: { placeholder: "Введите наименование агента" },
    searchRequestProps: searchRequestPropsForAgents({
      params: {
        roles: [5]
      }
    }),
    afterChange: forUserAgentCodeHandler
  },
  afterMap: forUserAgentCodeHandler
};
