import { mapValuesToFields } from "../../helpers";

export const EntityRecord = {
  title: null,
  subtitle: "",
  formMain: null,
  formField: null,
  view: "default",
  infoFields: [],
  formFields: [],
  fieldsForAdd: [],
  mapValuesToFields,
  devidingFields: []
};
