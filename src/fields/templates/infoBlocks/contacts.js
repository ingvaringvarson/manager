import {
  mapEntitiesToGroupedList,
  renderInfoFields,
  mapValuesToGroupedList,
  mapEntitiesToList
} from "../../helpers";
import { fields } from "../../formFields/clients/fieldsForEditForms";
import { EntityRecord } from "./records";

export const contacts = {
  ...EntityRecord,
  title: "Контакты",
  formMain: {
    subtitle: "Контакты",
    title: "Создание контакта",
    type: "add",
    icon: "add"
  },
  formField: {
    subtitle: "Контакт",
    title: "Редактирование",
    type: "edit"
  },
  view: "groupedList",
  mapValuesToFields: (field, enumerations, values) => {
    const contacts = values.client && values.client.contacts;
    return mapEntitiesToGroupedList("contactType")(
      field,
      enumerations,
      contacts
    );
  },
  formFields: fields["contacts"]
};

export const contact_persons = {
  ...EntityRecord,
  title: "Контактные лица",
  formField: {
    subtitle: "Контактные лица",
    title: "Редактирование",
    type: "edit"
  },
  view: "list",
  infoFields: renderInfoFields([
    {
      key: "name",
      title: "Имя"
    },
    {
      key: "gender",
      title: "Пол",
      enumerationKey: "personGender"
    },
    {
      key: "position",
      title: "Должность"
    },
    {
      key: "contacts",
      mapValueToFields: mapValuesToGroupedList("contactType"),
      view: "groupedList"
    }
  ]),
  mapValuesToFields: (field, enumerations, values) => {
    const contact_persons = values.client && values.client.contact_persons;
    return mapEntitiesToList(field, enumerations, contact_persons);
  },
  formFields: fields["contact_persons"]
};
