import React from "react";
import styled from "styled-components";

import DiscountPriceRow from "../../../components/blocks/clients/infoBlockComponents/DiscountPriceRow";
import MultiValue from "../../../components/common/infoElementViews/MultiValue";
import InfoRowCommon from "../../../components/common/info/InfoRowCommon";
import Button from "../../../designSystem/atoms/Button";

const ButtonLinkStyled = styled(Button)`
  height: 18px;
`;

const renderRow = props => {
  const { entity, field, enumerations } = props;

  if (
    !(
      entity[field.key] &&
      Array.isArray(entity[field.key]) &&
      entity[field.key].length
    )
  ) {
    return null;
  }

  const value = (
    <MultiValue field={field} entity={entity} enumerations={enumerations} />
  );

  const title = field.title;

  return <InfoRowCommon title={title} value={value} />;
};

export const discountPriceViewField = {
  key: "discount_group",
  title: "Ценовая колонка",
  renderRow: props => {
    const { entity, field, enumerations } = props;
    return (
      <DiscountPriceRow
        key={field.id}
        field={field}
        entity={entity}
        enumerations={enumerations}
      />
    );
  }
};

export const mailingPriceViewField = {
  key: "mailing_price",
  title: "Рассылка прайса",
  renderRow: ({ field, entity }) => {
    const { code, segments } = entity;
    const b2bSegment = 2;

    if (!segments || !segments.includes(b2bSegment)) {
      return null;
    }

    const href = `/v1/redirect/admin?path=clientsettings/pricesendconfig/index?clientCode=${code}`;

    const value = (
      <ButtonLinkStyled secondary type="link" target="_blank" href={href}>
        Перейти в админку
      </ButtonLinkStyled>
    );
    return <InfoRowCommon title={field.title} value={value} />;
  }
};

export const clientTagsField = {
  key: "tags",
  title: "Теги",
  enumerationKey: "agentTag",
  renderRow
};

export const clientSegmentsField = {
  key: "segments",
  title: "Сегменты",
  enumerationKey: "agentSegment",
  renderRow
};
