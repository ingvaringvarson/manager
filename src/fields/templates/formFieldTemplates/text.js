import { getValueInDepth } from "../../../utils";
import { maskOnlyNumbers } from "../../../helpers/masks";

export const clientCodeField = {
  id: "clientCode",
  formControlProps: {
    type: "text",
    mask: maskOnlyNumbers,
    attrs: { placeholder: "Введите ID клиента" }
  },
  afterMap: ({ field, values }) => {
    const code = getValueInDepth(values, ["code"]);
    const person_name = getValueInDepth(values, ["person", "person_name"]);
    const person_surname = getValueInDepth(values, [
      "person",
      "person_surname"
    ]);
    let value = "";

    if (person_name) {
      value = person_name;
    }
    if (person_surname) {
      value = value ? `${value} ${person_surname}` : person_surname;
    }
    if (code) {
      value = value ? `${value} ID: ${code}` : `ID: ${code}`;
    }

    return {
      ...field,
      formControlProps: {
        ...field.formControlProps,
        value
      }
    };
  }
};
