import React, { Component } from "react";

export default ChildComponent => {
  class ToggleOpen extends Component {
    state = {
      isOpen: false
    };

    handleToggle = () => {
      this.setState(prevState => ({
        isOpen: !prevState.isOpen
      }));
    };

    render() {
      return (
        <ChildComponent
          {...this.props}
          {...this.state}
          handleToggle={this.handleToggle}
        />
      );
    }
  }

  return ToggleOpen;
};
