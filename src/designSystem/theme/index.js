const theme = {
  colors: {
    // common colors
    general: ["#3b3b3b", "#444444"],
    primary: ["#eb0028", "#c30028"],
    secondary: ["#787878"],

    // extra colors
    grayscale: ["#bcbcbc", "#e0e0e0", "#f1f1f1"],
    green: ["#56CB08", "#00CF34"],
    yellow: ["#FF9320", "#f0a01e", "#ffeb3b"],
    blue: ["#336EA8"],
    red: ["#ff7976", "#EF5350"],
    white: ["#ffffff"]
  },
  space: {
    small: "10px",
    middle: "16px"
  },
  get palette() {
    return this.colors;
  }
};

export default theme;
