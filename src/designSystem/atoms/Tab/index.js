import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp, palette } from "styled-tools";
import { space } from "styled-system";

import Title from "../../atoms/Title";

const StyledTab = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 16px;
  text-align: center;
  cursor: pointer;

  ::after {
    content: "";
    position: absolute;
    right: 0;
    bottom: 1px;
    left: 0;
    width: 100%;
    height: 2px;
    background: transparent;
    transform: translateY(0.5px);
    transition: background 0.2s;
  }

  ${ifProp(
    "active",
    css`
      ::after {
        background: ${palette("primary", 0)};
      }
    `
  )}

  ${space};
`;

const Tab = ({ title, ...props }) => {
  return (
    <StyledTab {...props}>
      <Title>{title}</Title>
    </StyledTab>
  );
};

Tab.propTypes = {
  title: PropTypes.string
};

export default Tab;
