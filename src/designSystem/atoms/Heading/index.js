import React from "react";
import styled, { css } from "styled-components";
import { ifProp, palette } from "styled-tools";
import { space, fontSize } from "styled-system";

const StyledHeading = styled.p`
  font-size: 24px;
  font-family: "Roboto", sans-serif;

  color: ${ifProp(
    "alter",
    css`
      ${palette("secondary", 0)}
    `,
    css`
      ${palette("general", 0)}
    `
  )};

  ${space};
  ${fontSize}
`;

const Heading = ({ ...props }) => {
  return <StyledHeading {...props} />;
};

export default Heading;
