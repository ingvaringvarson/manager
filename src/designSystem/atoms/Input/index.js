import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp, palette } from "styled-tools";
import { space } from "styled-system";

const styles = css`
  box-sizing: border-box;
  display: inline-block;
  width: ${ifProp("fullWidth", "100%", "auto")};
  height: ${ifProp({ type: "textarea" }, "60px", "36px")};
  margin: 0;
  padding: 8px 12px;
  font-size: 14px;
  color: ${palette("general", 0)};
  border: 1px solid ${ifProp("invalid", palette("primary", 0), "transparent")};
  border-radius: 5px;
  outline: none;
  background: ${palette("grayscale", 2)};
  opacity: ${ifProp("disabled", 0.5, 1)};
  transition: background 0.3s, border 0.3s;

  ::placeholder {
    color: ${palette("secondary", 0)};
  }

  :not(:disabled):hover {
    background: ${palette("grayscale", 1)};
  }

  :disabled {
    cursor: not-allowed;
  }

  :focus {
    border: 1px solid
      ${ifProp("invalid", palette("primary", 0), palette("secondary", 0))};
  }

  ${ifProp(
    { type: "textarea" },
    css`
      resize: none;
    `
  )};

  ${ifProp(
    "date",
    css`
      padding-left: 50px;
    `
  )}

  ${space};
`;

const StyledTextarea = styled.textarea`
  ${styles}
`;
const StyledInput = styled.input`
  ${styles}
`;

const Input = ({ type, ...props }) => {
  if (type === "textarea") {
    return <StyledTextarea {...props} type={type} />;
  }
  return <StyledInput {...props} type={type} />;
};

Input.propTypes = {
  type: PropTypes.string,
  disabled: PropTypes.bool,
  invalid: PropTypes.bool,
  fullWidth: PropTypes.bool
};

export default Input;
