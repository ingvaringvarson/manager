import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { space } from "styled-system";
import { palette } from "styled-tools";

const SeparatorWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 6px 0;
  overflow: hidden;
  text-align: center;
  ${space};
`;

const SeparatorText = styled.span`
  position: relative;
  padding: 0 15px;
  color: ${palette("secondary", 0)};
  font-size: 18px;
  text-transform: uppercase;

  ::before,
  ::after {
    content: "";
    position: absolute;
    top: calc(50% - 2px);
    display: block;
    width: 9999px;
    border-top: 1px solid ${palette("grayscale", 1)};
  }

  ::before {
    right: 100%;
  }
  ::after {
    left: 100%;
  }
`;

const Separator = ({ text, ...props }) => {
  return (
    <SeparatorWrap {...props}>
      <SeparatorText>{text}</SeparatorText>
    </SeparatorWrap>
  );
};

Separator.defaultProps = {
  text: "или"
};

Separator.propTypes = {
  text: PropTypes.string
};

export default Separator;
