import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { palette } from "styled-tools";

import ButtonMixin from "../../mixins/Button";

const Anchor = styled.a`
  ${ButtonMixin};
`;

const LinkWrapper = styled.div`
  ${ButtonMixin};
  color: ${palette("general")};
  background: ${palette("white")};

  :not(:disabled):hover {
    background: ${palette("grayscale", 1)};
  }
`;

const StyledLink = styled(Link)`
  text-decoration: none;

  :active,
  :visited {
    color: inherit;
  }
`;

const StyledButton = styled.button`
  ${ButtonMixin};
`;

const Button = ({ type, href, children, ...props }) => {
  switch (true) {
    case type === "link":
      return (
        <Anchor href={href} type={type} {...props}>
          {children}
        </Anchor>
      );
    case type === "spa-link":
      return (
        <LinkWrapper {...props}>
          <StyledLink to={href}>{children}</StyledLink>
        </LinkWrapper>
      );
    default:
      return (
        <StyledButton type={type} {...props}>
          {children}
        </StyledButton>
      );
  }
};

Button.propTypes = {
  children: PropTypes.any,
  disabled: PropTypes.bool,
  fullWidth: PropTypes.bool,
  palette: PropTypes.string,
  transparent: PropTypes.bool,
  secondary: PropTypes.bool,
  white: PropTypes.bool,
  simple: PropTypes.bool,
  noBorder: PropTypes.bool,
  height: PropTypes.number,
  type: PropTypes.string,
  href: PropTypes.string
};

export default Button;
