import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp, palette } from "styled-tools";
import { space, fontWeight, fontSize, color } from "styled-system";

const StyledText = styled.div`
  font-family: "Roboto", sans-serif;
  font-size: 14px;

  ${ifProp(
    "alter",
    css`
      color: ${palette("secondary", 0)};
    `
  )};

  ${space};
  ${color};
  ${fontWeight};
  ${fontSize};
`;

const Text = ({ children, ...props }) => {
  return <StyledText {...props}>{children}</StyledText>;
};

Text.propTypes = {
  children: PropTypes.any.isRequired
};

export default Text;
