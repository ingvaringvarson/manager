import React from "react";
import PropTypes from "prop-types";
import styled, { css, keyframes } from "styled-components";
import { ifProp, palette } from "styled-tools";
import { space } from "styled-system";

const rotateWrap = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const rotateCicle = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const StyledPreloader = styled.div`
  position: relative;
  animation: ${rotateWrap} 4.6s linear infinite;

  :after {
    content: "";
    position: absolute;
    border-radius: 50%;
    border: 2px solid ${palette("grayscale", 1)};
    border-top-color: ${palette("primary", 0)};
    animation: ${rotateCicle} 1.6s linear infinite;
  }

  ${ifProp(
    { size: "big" },
    css`
      width: 54px;
      height: 54px;

      :after {
        height: 54px;
        width: 54px;
      }
    `
  )}

  ${ifProp(
    { size: "medium" },
    css`
      width: 40px;
      height: 40px;

      :after {
        height: 40px;
        width: 40px;
      }
    `
  )}

  ${ifProp(
    { size: "small" },
    css`
      width: 24px;
      height: 24px;

      :after {
        height: 24px;
        width: 24px;
      }
    `
  )}

  ${space}
`;

const propTypes = {
  size: PropTypes.oneOf(["small", "medium", "big"])
};

const defaultProps = {
  size: "big"
};

const Preloader = props => {
  return <StyledPreloader {...props} />;
};

Preloader.propTypes = propTypes;
Preloader.defaultProps = defaultProps;

export default Preloader;
