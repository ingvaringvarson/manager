import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { space, width, height } from "styled-system";
import { ifProp } from "styled-tools";

const Wrapper = styled.span`
  display: inline-flex;

  ${ifProp(
    "flex",
    css`
      display: inline-flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
    `
  )}

  flex-direction: column;
  align-items: center;
  justify-content: center;

  & > svg {
    fill: currentcolor;
    width: inherit;
    height: inherit;
  }

  ${space};
  ${width};
  ${height};
`;

const Icon = ({ icon, ...props }) => {
  const svg = require(`!raw-loader!./icons/${icon}.svg`).default;
  return <Wrapper {...props} dangerouslySetInnerHTML={{ __html: svg }} />;
};

Icon.defaultProps = {
  width: "24px",
  height: "24px"
};

Icon.propTypes = {
  icon: PropTypes.string.isRequired,
  width: PropTypes.string
};

export default Icon;
