import React from "react";
import PropTypes from "prop-types";
import { palette, ifProp } from "styled-tools";
import styled, { css } from "styled-components";

const Item = styled.li`
  padding: 8px 12px;
  font-size: 14px;
  color: ${palette("general", 0)};
  background: ${palette("grayscale", 2)};
  transition: background 0.3s;
  cursor: pointer;

  :hover {
    background: ${palette("grayscale", 1)};
  }

  :not(:last-of-type) {
    border-bottom: 1px solid ${palette("grayscale", 1)};
  }

  ${ifProp(
    "inFocus",
    css`
      background: ${palette("grayscale", 1)};
    `
  )}
`;

const propTypes = {
  inFocus: PropTypes.bool,
  handlers: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element
  ]).isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

const defaultProps = {
  handlers: {}
};

function Option(props) {
  const { children, handlers, value, ...rest } = props;
  const itemHandlers = Object.keys(handlers).reduce((result, handlerName) => {
    return { ...result, [handlerName]: handlers[handlerName](value) };
  }, {});

  return (
    <Item {...itemHandlers} {...rest}>
      {children}
    </Item>
  );
}

Option.defaultProps = defaultProps;
Option.propTypes = propTypes;

export default Option;
