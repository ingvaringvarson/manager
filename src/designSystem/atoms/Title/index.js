import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp, palette } from "styled-tools";
import { space, fontWeight } from "styled-system";

const StyledTitle = styled.p`
  font-family: "Roboto", sans-serif;
  font-size: 18px;
  color: ${palette("general", 0)};

  ${ifProp(
    "alter",
    css`
      color: ${palette("secondary", 0)};
    `
  )};

  ${ifProp(
    "red",
    css`
      color: ${palette("primary", 0)};
    `
  )}

  ${space};
  ${fontWeight};
`;

const Title = ({ children, ...props }) => {
  return <StyledTitle {...props}>{children}</StyledTitle>;
};

Title.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.array
  ]).isRequired
};

export default Title;
