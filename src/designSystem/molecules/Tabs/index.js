import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { palette } from "styled-tools";
import { space } from "styled-system";

const StyledTabs = styled.div`
  display: flex;
  border-bottom: 1px solid ${palette("grayscale", 1)};
  overflow-x: auto;
  ${space};
`;

const Tabs = ({ children, ...props }) => {
  return <StyledTabs {...props}>{children}</StyledTabs>;
};

Tabs.propTypes = {
  children: PropTypes.any
};

export default Tabs;
