import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { space } from "styled-system";

const StyledAside = styled.aside`
  width: 64px;
  padding-bottom: 30px;
  background: #ffffff;
  ${space};
`;

const InnerAside = styled.div`
  height: 100%;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.05);
`;

const Aside = ({ children, ...props }) => {
  return (
    <StyledAside {...props}>
      <InnerAside>{children}</InnerAside>
    </StyledAside>
  );
};

Aside.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ])
};

export default Aside;
