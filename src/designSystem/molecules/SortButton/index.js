import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { space } from "styled-system";

import Icon from "../../atoms/Icon";

const StyledIcon = styled(Icon)`
  width: 16px;
`;

const StyledButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 0;
  padding: 0;
  border: 0 none;
  outline: none;
  background: none;
  cursor: pointer;
  width: 100%;
  ${space};
`;

const IconWrap = styled.span`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-left: 10px;

  ${StyledIcon} {
    width: 16px;
    height: 16px;
  }
`;

const SortButton = ({
  children,
  iconUpProps,
  iconDownProps,
  activeSortBy,
  ...props
}) => {
  return (
    <StyledButton {...props}>
      {children}
      <IconWrap>
        <StyledIcon
          {...iconUpProps}
          icon={activeSortBy === "desc" ? "sort-up-active" : "sort-up"}
        />
        <StyledIcon
          {...iconDownProps}
          icon={activeSortBy === "asc" ? "sort-down-active" : "sort-down"}
        />
      </IconWrap>
    </StyledButton>
  );
};

SortButton.propTypes = {
  children: PropTypes.element,
  iconUpProps: PropTypes.object,
  iconDownProps: PropTypes.object,
  activeSortBy: PropTypes.string
};

SortButton.defaultProps = {
  iconUpProps: {},
  iconDownProps: {}
};

export default SortButton;
