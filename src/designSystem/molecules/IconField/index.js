import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { palette } from "styled-tools";
import { ifProp } from "styled-tools";
import { space } from "styled-system";

import Icon from "../../atoms/Icon";

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  color: ${palette("general", 1)};
  transform: translateY(-50%);

  ${ifProp(
    "first",
    css`
      left: 10px;
    `
  )}

  ${ifProp(
    "last",
    css`
      right: 5px;
    `
  )}

  ${ifProp(
    "clear",
    css`
      width: 12px;
      height: 12px;
      right: 14px;
    `
  )};
`;

const StyledField = styled.div`
  position: relative;
  ${space};
`;

const IconField = ({
  children,
  icon,
  iconType,
  iconLast,
  iconClear,
  clearProps,
  ...props
}) => {
  switch (iconType) {
    case "iconLast":
      return (
        <StyledField {...props}>
          {children}
          <StyledIcon icon={icon} last />
        </StyledField>
      );
    case "iconClear":
      return (
        <StyledField {...props}>
          {children}
          <StyledIcon icon="clear" clear {...clearProps} />
        </StyledField>
      );
    default:
      return (
        <StyledField {...props}>
          <StyledIcon icon={icon} first />
          {children}
        </StyledField>
      );
  }
};

IconField.propTypes = {
  icon: PropTypes.string
};

export default IconField;
