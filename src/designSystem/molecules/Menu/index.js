import React from "react";
import styled, { css } from "styled-components";
import PropTypes from "prop-types";
import { ifProp } from "styled-tools";
import { space } from "styled-system";

import { Triangle, TriangleTop } from "../../mixins/Triangle";

const StyledMenu = styled.div`
  position: relative;
  padding: 8px 16px;
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
  border-radius: 5px;

  ${Triangle};
  ${TriangleTop};

  p {
    line-height: 32px;
  }

  a,
  button {
    margin: 0;
    padding: 0;
    line-height: inherit;
    font-weight: inherit;
    font-size: inherit;
    font-style: inherit;
    text-decoration: none;
    color: inherit;
    outline: none;
    border: 0 none;
    background: none;
  }
`;

const StyledWrap = styled.div`

  ${ifProp(
    "left",
    css`
      ${StyledMenu} {
        ::before,
        ::after {
          left: 16px;
        }
      }
    `
  )}

  ${ifProp(
    "center",
    css`
      ${StyledMenu} {
        ::before,
        ::after {
          left: calc(50% - 18px);
        }
      }
    `
  )}

  ${ifProp(
    "right",
    css`
      ${StyledMenu} {
        ::before,
        ::after {
          right: 16px;
        }
      }
    `
  )}

  ${ifProp(
    "user",
    css`
      ${StyledMenu} {
        padding: 16px;
      }
    `
  )}

  ${space};
`;

const MenuContent = styled.div`
  position: relative;
  z-index: 10;
`;

const Menu = ({ children, ...props }) => {
  return (
    <StyledWrap {...props}>
      <StyledMenu>
        <MenuContent>{children}</MenuContent>
      </StyledMenu>
    </StyledWrap>
  );
};

Menu.propTypes = {
  children: PropTypes.any
};

export default Menu;
