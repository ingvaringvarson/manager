import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { space } from "styled-system";

import Icon from "../../atoms/Icon";
import Button from "../../atoms/Button";

const StyledButton = styled(Button)`
  box-sizing: border-box;
  padding: 0 15px;
  ${space};
`;

const Text = styled.span`
  padding-left: 8px;
  white-space: pre;
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledIcon = styled(Icon)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  flex: none;
  width: 24px;
  height: 24px;
`;

const IconButton = ({ icon, children, ...props }) => {
  const iconElement = <StyledIcon icon={icon} />;
  return (
    <StyledButton hasText={!!children} {...props}>
      <Wrapper>
        {icon && iconElement}
        {children && (
          <Text ml="5px" className="text">
            {children}
          </Text>
        )}
      </Wrapper>
    </StyledButton>
  );
};

IconButton.propTypes = {
  icon: PropTypes.string,
  children: PropTypes.any
};

export default IconButton;
