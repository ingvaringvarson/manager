import React, { Component } from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { palette, ifProp } from "styled-tools";

import ToggleOpen from "../../decorators/ToggleOpen";
import Icon from "../../atoms/Icon";
import Text from "../../atoms/Text";
import Item from "../../mixins/Item";

const Box = styled.div`
  position: relative;
  margin-bottom: ${props => props.marginBottom};
  background: #ffffff;
  border: 1px solid ${palette("grayscale", 1)};
  border-radius: 5px;

  ${ifProp(
    "active",
    css`
      ::before {
        content: "";
        position: absolute;
        top: 0;
        bottom: 0;
        border-left: 2px solid #eb0028;
      }
    `
  )}
`;

const Placeholder = styled.div`
  ${Item};
  padding: 3px 0;
  cursor: pointer;
`;

const StyledText = styled(Text)`
  padding-right: 30px;
  padding-left: 12px;
  overflow: hidden;
  line-height: 24px;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  right: 16px;
  width: 12px;
  height: 6px;
  transform: translateY(-50%);
`;

const Body = styled.div`
  padding: 6px 0;
`;

class Accordion extends Component {
  static propTypes = {
    title: PropTypes.string,
    children: PropTypes.any,
    onToggle: PropTypes.func,
    active: PropTypes.bool
  };

  handleToggleOpen = () => {
    if (this.props.onToggle && !(!this.props.active && this.props.isOpen)) {
      this.props.onToggle(this.props);
    }

    this.props.handleToggle();
  };

  render() {
    const { marginBottom, title, children, isOpen, active } = this.props;
    return (
      <Box marginBottom={marginBottom} active={active}>
        <Placeholder onClick={this.handleToggleOpen} active={active}>
          <StyledText>{title}</StyledText>
          {isOpen ? (
            <StyledIcon flex icon="sort-up" />
          ) : (
            <StyledIcon flex icon="sort-down" />
          )}
        </Placeholder>
        {isOpen && <Body>{children}</Body>}
      </Box>
    );
  }
}

export default ToggleOpen(Accordion);
