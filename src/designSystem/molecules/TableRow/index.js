import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp, ifNotProp, palette } from "styled-tools";

const StyledRow = styled.div`
  display: flex;
  align-items: center;
  padding-top: 10px;
  padding-bottom: 10px;
  border-top: 1px solid ${palette("grayscale", 1)};
  background: transparent;
  transition: background 0.3s;

  :first-of-type {
    border-top: 0 none;
  }

  ${ifProp(
    "head",
    css`
      color: ${palette("secondary", 0)};
      cursor: default;
    `
  )}

  ${ifProp(
    "headFixed",
    css`
      position: sticky;
      z-index: 2;
      border-bottom: 1px solid ${palette("grayscale", 1)};
      background: #ffffff;
    `
  )}

  ${ifProp(
    "trigger",
    css`
      cursor: pointer;
    `
  )}

  ${ifNotProp(
    "head",
    css`
      :hover {
        background: ${palette("grayscale", 2)};
      }
    `
  )}

  ${ifProp(
    "active",
    css`
      background: rgba(235, 0, 40, 0.1);
      :hover {
        background: rgba(235, 0, 40, 0.1);
      }
    `
  )}
`;

const TableRow = ({ children, ...props }) => {
  return <StyledRow {...props}>{children}</StyledRow>;
};

TableRow.propTypes = {
  children: PropTypes.any,
  trigger: PropTypes.bool,
  head: PropTypes.bool,
  headFixed: PropTypes.bool
};

export default TableRow;
