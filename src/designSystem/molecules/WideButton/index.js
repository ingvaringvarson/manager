import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Icon from "../../atoms/Icon";
import Button from "../../atoms/Button";

const StyledIcon = styled(Icon)`
  width: 20px;
  height: 20px;
  float: left;
`;

const Wrapper = styled.div`
  padding: 8px 0 8px 0;
  width: 100%;
`;

const WideButton = React.memo(({ icon, ...props }) => (
  <Button fullWidth noBorder {...props}>
    <Wrapper>
      {icon && <StyledIcon icon={icon} />}
      {props.children}
    </Wrapper>
  </Button>
));

WideButton.propTypes = {
  icon: PropTypes.string
};

export default WideButton;
