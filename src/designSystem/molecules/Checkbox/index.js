import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp, palette } from "styled-tools";

import Hidden from "../../mixins/Hidden";

const StyledCheck = styled.div`
  display: flex;
  align-items: center;
`;

const Mark = styled.span`
  display: block;
  width: 18px;
  height: 18px;
  border: 1px solid transparent;
  background: transparent;
  transition: border 0.3s, background 0.2s;
  ::after {
    content: "";
    background: transparent;
    transition: background 0.2s;
  }
`;

const Input = styled.input`
  ${Hidden};
  position: absolute;

  :disabled {
    & ~ ${Mark} {
      opacity: 0.5;
      cursor: not-allowed;
    }
  }

  ${ifProp(
    "disabled",
    css`
      & ~ ${Mark} {
        opacity: 0.5;
        cursor: not-allowed;
      }
    `
  )}

  ${ifProp(
    "gray",
    css`
      & ~ ${Mark} {
        border: 1px solid ${palette("secondary", 0)};
      }
    `
  )}
`;

const Button = styled.label`
  position: relative;
  cursor: pointer;
  user-select: none;

  :hover {
    ${Input} {
      & ~ ${Mark} {
        border: ${palette("primary", 0)};
        background: ${palette("primary", 0)};
      }
      :disabled {
        & ~ ${Mark} {
          border: 1px solid ${palette("general", 1)};
          background: transparent;
        }
      }
    }
  }

  ${ifProp(
    "checkbox",
    css`
      ${Mark} {
        border: 1px solid ${palette("general", 1)};
        border-radius: 3px;
      }

      ${Input} {
        :checked {
          & ~ ${Mark} {
            border: 1px solid ${palette("primary", 0)};
            background: ${palette("primary", 0)};
            ::after {
              content: "";
              position: absolute;
              left: 50%;
              top: calc(50% - 1px);
              display: block;
              width: 6px;
              height: 10px;
              border: solid white;
              border-width: 0 2px 2px 0;
              transform: translate(-50%, -50%) rotate(45deg);
            }
          }
        }

        :indeterminate {
          & ~ ${Mark} {
            border: 1px solid ${palette("primary", 0)};
            background: ${palette("primary", 0)};
            ::after {
              content: "";
              position: absolute;
              left: 50%;
              top: 50%;
              display: block;
              width: 10px;
              height: 2px;
              background: white;
              transform: translate(-50%, -50%);
            }
          }
        }
      }
    `
  )}

  ${ifProp(
    "radio",
    css`
      ${Mark} {
        border: 1px solid ${palette("general", 1)};
        border-radius: 50%;
      }

      ${Input} {
        :checked {
          & ~ ${Mark} {
            border: 1px solid ${palette("primary", 0)};
            ::after {
              content: "";
              position: absolute;
              left: 50%;
              top: 50%;
              display: block;
              width: 10px;
              height: 10px;
              border-radius: 50%;
              background: ${palette("primary", 0)};
              transform: translate(-50%, -50%);
            }
          }
        }
      }
    `
  )};
`;

const StyledText = styled.label`
  margin-left: 5px;
  font-family: inherit;
  font-size: 14px;
  line-height: 1.2;
  cursor: pointer;
  color: ${ifProp(
    "alter",
    css`
      ${palette("secondary", 0)}
    `,
    css`
      ${palette("general", 0)}
    `
  )};
`;

const Notice = styled.span`
  display: block;
  margin-left: 42px;
  font-size: 10px;
  font-family: inherit;
  color: ${ifProp("invalid", palette("primary", 0), palette("grayscale", 0))};
`;

const Checkbox = ({
  disabled,
  name,
  notice,
  text,
  invalid,
  type,
  id,
  styledProps,
  ...props
}) => {
  if (type === "radio") {
    return (
      <div>
        <StyledCheck>
          <Button radio>
            <Input
              {...props}
              id={id}
              type="radio"
              name={name}
              disabled={disabled}
            />
            <Mark />
          </Button>
          {text && <StyledText htmlFor={id}>{text}</StyledText>}
        </StyledCheck>
        {notice && <Notice invalid={invalid}>{notice}</Notice>}
      </div>
    );
  }
  return (
    <div name={props.name}>
      <StyledCheck>
        <Button checkbox>
          <Input
            {...props}
            id={id}
            type="checkbox"
            name={name}
            disabled={disabled}
          />
          <Mark />
        </Button>
        {text && <StyledText htmlFor={id}>{text}</StyledText>}
      </StyledCheck>
      {notice && <Notice invalid={invalid}>{notice}</Notice>}
    </div>
  );
};

Checkbox.defaultProps = {
  id: ""
};

Checkbox.propTypes = {
  type: PropTypes.string.isRequired,
  text: PropTypes.string,
  notice: PropTypes.string,
  name: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string
};

export default Checkbox;
