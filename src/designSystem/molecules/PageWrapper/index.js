import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const StyledWrapper = styled.div`
  margin: 0 80px;
  padding-top: 80px;
  padding-bottom: 16px;
`;

const PageWrapper = ({ children, ...props }) => {
  return <StyledWrapper {...props}>{children}</StyledWrapper>;
};

PageWrapper.propTypes = {
  children: PropTypes.any
};

export default PageWrapper;
