import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp, withProp } from "styled-tools";

const StyledCell = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  flex: ${withProp("flex", flex => flex || "1 1 0")};
  max-width: 100%;
  margin-right: 10px;
  text-align: left;
  hyphens: auto;
  -webkit-box-orient: vertical;

  ${ifProp(
    "first",
    css`
      flex-shrink: 0;
      flex-basis: 40px;
      max-width: 40px;
    `
  )};

  ${ifProp(
    "control",
    css`
      flex-shrink: 0;
      flex-basis: 40px;
      max-width: 40px;
      padding-right: 0;
      color: initial;
    `
  )}
`;

const TableCell = ({ children, ...props }) => {
  return <StyledCell {...props}>{children}</StyledCell>;
};

TableCell.propTypes = {
  children: PropTypes.any,
  control: PropTypes.bool,
  first: PropTypes.bool
};

export default TableCell;
