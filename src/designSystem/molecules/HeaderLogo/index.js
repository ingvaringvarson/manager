import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { palette } from "styled-tools";
import { space } from "styled-system";

import LogoImage from "../../../designSystem/atoms/LogoImage";
import Title from "../../../designSystem/atoms/Title";

const Logo = styled.div`
  position: relative;
  display: flex;
  align-items: center;

  ::before {
    content: "";
    position: absolute;
    top: 50%;
    left: 120px;
    width: 3px;
    height: 30px;
    background-color: ${palette("primary", 0)};
    transform: translateY(-50%);
  }

  ${space};
`;

const StyledLogo = styled(LogoImage)`
  display: block;
  width: 110px;
`;

const StyledTitle = styled(Title)`
  margin-left: 30px;
`;

const HeaderLogo = props => {
  return (
    <Logo>
      <StyledLogo />
      {!!props.title && <StyledTitle alter>{props.title}</StyledTitle>}
    </Logo>
  );
};

HeaderLogo.propTypes = {
  title: PropTypes.string
};

export default HeaderLogo;
