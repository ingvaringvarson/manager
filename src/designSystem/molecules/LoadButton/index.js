import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp } from "styled-tools";

import Icon from "../../atoms/Icon";
import Button from "../../atoms/Button";

const StyledIcon = styled(Icon)`
  width: 20px;
  height: 20px;
  float: left;
  margin-right: 10px;

  ${ifProp(
    "size",
    css`
      width: ${props => props.size};
      height: ${props => props.size};
    `
  )}
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 8px 0 8px 0;
  width: 100%;

  ${ifProp(
    "height",
    css`
      height: ${props => props.height};
    `
  )}
`;

const StyledHiddenInput = styled.input`
  display: none;
  position: absolute;
`;

const uploadFileInputId = "upload-file-input";

const handleUploadRedirect = e => {
  if (e.target.id !== uploadFileInputId) {
    const el = e.target.querySelector(`#${uploadFileInputId}`);
    el && el.click();
  }
};

const LoadButton = React.memo(
  ({ onClick, iconSize, wrapperHeight, ...props }) => (
    <Button
      upload
      transparent
      defaultCase
      onClick={handleUploadRedirect}
      {...props}
    >
      <Wrapper height={wrapperHeight}>
        <StyledIcon size={iconSize} icon={"cloud_upload"} />
        <StyledHiddenInput
          id={uploadFileInputId}
          type="file"
          name="file"
          onChange={onClick}
        />
        {props.children}
      </Wrapper>
    </Button>
  )
);

LoadButton.propTypes = {
  icon: PropTypes.string
};

export default LoadButton;
