import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { palette } from "styled-tools";
import { space } from "styled-system";

import Title from "../../atoms/Title";
import Text from "../../atoms/Text";
import Icon from "../../atoms/Icon";

const Head = styled.div`
  position: relative;
  padding: 16px;
  border-bottom: 1px solid ${palette("grayscale", 1)};
  ${space};
`;

const SubTitle = styled(Text)`
  margin-top: 4px;
  color: ${palette("secondary", 0)};
`;

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 16px;
  right: 16px;
  width: 24px;
  cursor: pointer;
`;

const Heading = ({ title, subtitle, icon, iconProps, children, ...props }) => {
  return (
    <Head {...props}>
      {title && <Title>{title}</Title>}
      {subtitle && <SubTitle>{subtitle}</SubTitle>}
      {icon && <StyledIcon icon={icon} {...iconProps} />}
      {children}
    </Head>
  );
};

Heading.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  icon: PropTypes.string,
  children: PropTypes.any
};

export default Heading;
