import React from "react";
import styled, { css } from "styled-components";
import PropTypes from "prop-types";
import { ifProp } from "styled-tools";

import Hidden from "../../mixins/Hidden";
import ButtonMixin from "../../mixins/Button";

const CheckedStyles = css`
  ${ButtonMixin};

  ${ifProp(
    "secondary",
    css`
      ${ButtonMixin.secondary};
    `
  )};
`;

const UncheckedStyles = css`
  ${ButtonMixin.white};
`;

const Label = styled.label`
  border-radius: 0;
  cursor: pointer;
  user-select: none;

  :first-of-type {
    border-right: 0 none;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
  }

  :last-of-type {
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
  }

  ${props => (props.checked ? CheckedStyles : UncheckedStyles)};
`;

const Input = styled.input`
  ${Hidden};
`;

const ButtonGroup = ({ radio, name, value, checked, inputProps, ...props }) => {
  if (radio) {
    return (
      <>
        <Input {...inputProps} type="radio" name={name} checked={checked} />
        <Label {...props}>{value}</Label>
      </>
    );
  }

  return (
    <>
      <Input {...inputProps} type="checkbox" name={name} checked={checked} />
      <Label {...props}>{value}</Label>
    </>
  );
};

ButtonGroup.propTypes = {
  radio: PropTypes.bool,
  name: PropTypes.string,
  value: PropTypes.string,
  checked: PropTypes.bool
};

export default ButtonGroup;
