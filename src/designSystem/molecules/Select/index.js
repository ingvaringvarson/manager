import React, { PureComponent } from "react";
import styled, { css } from "styled-components";
import { ifProp, palette } from "styled-tools";
import PropTypes from "prop-types";
import ScrollArea from "react-scrollbar";
import Option from "../../atoms/Option";
import memoizeOne from "memoize-one";

import Icon from "../../atoms/Icon/index";
import Field from "../../molecules/Field";
import Input from "../../../designSystem/atoms/Input";
import Checkbox from "../../../designSystem/molecules/Checkbox";
import Preloader from "../../atoms/Spinner";

const SelectedBlock = styled.div`
  box-sizing: border-box;
  position: relative;
  display: flex;
  flex-wrap: wrap;
  grid-gap: 4px 8px;
  min-height: 36px;
  padding: 4px 30px 4px 6px;
  overflow: hidden;
  font-size: 14px;
  color: ${palette("secondary", 0)};
  border: 1px solid transparent;
  border-radius: 5px;
  background: ${palette("grayscale", 2)};
  transition: border 0.2s, background 0.3s;
  cursor: pointer;
`;

const SelectedItem = styled.span`
  position: relative;
  margin: 2px 4px;
  padding: 8px 28px 8px 12px;
  max-width: 108px;
  font-size: 12px;
  color: #ffffff;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  border-radius: 5px;
  background: ${palette("primary", 1)};
  cursor: pointer;

  ::before,
  ::after {
    content: "";
    position: absolute;
    top: 50%;
    right: 14px;
    width: 8px;
    height: 1px;
    background: #ffffff;
  }
  ::before {
    transform: translateY(-50%) rotate(45deg);
  }
  ::after {
    transform: translateY(-50%) rotate(-45deg);
  }
`;

const Overlay = styled.div`
  display: none;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

const PreloaderContainer = styled.div`
  position: absolute;
  top: calc(50% - 12px);
  right: 5px;
  bottom: 0;
`;

const StyledIcon = styled(Icon)`
  width: 24px;
  cursor: pointer;
  position: absolute;
  top: 50%;
  right: 5px;
  font-size: 34px;
  color: ${palette("general", 1)};
  opacity: 0.2;
  transform: translateY(-50%);
`;

const IconClearWrap = styled.button`
  position: absolute;
  top: 10px;
  right: 26px;
  width: 18px;
  height: 18px;
  cursor: pointer;
  border: none;
  background: transparent;
  padding: 0;

  ${ifProp(
    "hide",
    css`
      display: none;
    `
  )};
`;

const IconClear = styled(Icon)`
  width: 16px;
  height: 16px;
`;

const List = styled.ul`
  position: absolute;
  z-index: 3;
  display: none;
  overflow: hidden;
  border-radius: 5px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);

  ${ifProp(
    "isLoading",
    css`
      ::before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: rgba(255, 255, 255, 0.3);
        z-index: 4;
      }
    `
  )}
`;

const ReadOnlyInput = styled(Input)`
  padding-right: 45px;
  cursor: pointer;
`;

const SearchInput = styled(Input)`
  padding-right: 45px;
`;

const CustomSelect = styled.div`
  position: relative;

  ${ifProp(
    "disabled",
    css`
      opacity: 0.5;
    `
  )}

  ${ifProp(
    "active",
    css`
      ${Overlay} {
        display: block;
      }

      ${List} {
        min-width: 200px;
        display: block;
        width: 100%;
      }
    `
  )}
`;

const TitleCheckbox = styled(Checkbox)`
  margin-bottom: 5px;
`;

const StyleScrollArea = { maxHeight: "418px" };
const StyleVerticalScrollbarStyle = { borderRadius: "5px" };

class Select extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        id: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
          .isRequired,
        disabled: PropTypes.bool
      })
    ).isRequired,
    name: PropTypes.string.isRequired,
    selected: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.string,
      PropTypes.number
    ]),
    onChangeSearchValue: PropTypes.func,
    onToggleFocusSearchControl: PropTypes.func,
    iconClearProps: PropTypes.object,
    placeholder: PropTypes.string,
    isMultiple: PropTypes.bool,
    searchMode: PropTypes.oneOf([
      "none", //контро ввода не используется
      "custom", // контро ввода используется, но работа с вводимым значение происходит в родительском компоненте
      "searchRequestControl" // стандартное поведение с поиском по сущностям
    ]),
    isLoading: PropTypes.bool,
    disabled: PropTypes.bool,
    defaultSearchValue: PropTypes.string
  };

  static defaultProps = {
    placeholder: "",
    isMultiple: false,
    searchMode: "none",
    selected: [],
    defaultSearchValue: ""
  };

  state = {
    selected: [],
    selectedFromProps: [],
    focusedOptionId: null,
    selectedMultipleIsChange: false,
    isOpen: false,
    prevIsOpen: false,
    isFocus: false,
    prevIsFocus: false,
    searchValueIsChange: false,
    selectedSearchOptionName: "",
    searchValue: this.props.defaultSearchValue
  };

  static getDerivedStateFromProps(props, state) {
    const newState = {};

    if (state.isFocus !== state.prevIsFocus) {
      newState.prevIsFocus = state.isFocus;
    }

    if (state.isOpen && !state.prevIsOpen) {
      newState.prevIsOpen = state.isOpen;
      newState.selectedMultipleIsChange = false;
    } else if (!state.isOpen && state.prevIsOpen) {
      newState.focusedOptionId = null;
      newState.prevIsOpen = state.isOpen;
    }

    if (props.selected !== state.selectedFromProps) {
      let newSelected = [];

      if (Array.isArray(props.selected)) {
        newSelected = props.selected.map(item => String(item));
      } else if (!!props.selected || props.selected === 0) {
        newSelected = [String(props.selected)];
      }

      newState.selected = newSelected;
      newState.selectedFromProps = props.selected;
    }

    const selectedOptionId = newState.selected
      ? newState.selected[0]
      : state.selected[0];

    //определяем выбранный опшион при потере фокуса на поисковой строке и при первой загрузке props.options(асинхронно)
    if (
      props.searchMode === "searchRequestControl" &&
      selectedOptionId &&
      props.options.length &&
      (!state.selectedSearchOptionName || newState.prevIsFocus === false)
    ) {
      const selectedOption = props.options.find(
        op => String(op.id) === selectedOptionId
      );

      if (selectedOption) {
        newState.selectedSearchOptionName = selectedOption.name;
        newState.searchValue = selectedOption.name;
      } else if (state.selectedSearchOptionName) {
        newState.searchValue = state.selectedSearchOptionName;
      }
    }

    if (newState.prevIsFocus === false) {
      if (state.searchValueIsChange) {
        newState.searchValueIsChange = false;
      } else if (props.searchMode === "custom") {
        newState.searchValue = props.defaultSearchValue;
      }
    }

    return Object.keys(newState).length ? newState : null;
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      isOpen,
      selected,
      selectedMultipleIsChange,
      searchValue,
      isFocus,
      selectedFromProps
    } = this.state;
    const {
      onToggleFocusSearchControl,
      name,
      onChangeSearchValue,
      onChange,
      options
    } = this.props;

    if (
      !isOpen &&
      ((prevState.isOpen && selectedMultipleIsChange) ||
        (selected !== prevState.selected && selected !== selectedFromProps))
    ) {
      onChange(selected, name, options);
    }

    if (
      onChangeSearchValue &&
      isFocus &&
      prevState.isFocus &&
      searchValue !== prevState.searchValue
    ) {
      onChangeSearchValue(searchValue, name);
    } else if (onToggleFocusSearchControl && isFocus !== prevState.isFocus) {
      onToggleFocusSearchControl(isFocus, name);
    }
  }

  handleChangeSearchValue = event => {
    event.preventDefault();
    this.setState({
      searchValue: event.target.value,
      searchValueIsChange: true
    });
  };

  handleFocusSearchInput = () => {
    this.setState({ searchValue: "", isFocus: true, isOpen: true });
  };

  handleBlurSearchInput = () => {
    this.setState({
      isFocus: false
    });
  };

  handleToggleOpen = () => {
    this.setState(prevState => ({ isOpen: !prevState.isOpen }));
  };

  handleToggleSelectedAll = () => {
    const { options } = this.props;
    const { selected } = this.state;

    if (!!selected.length && options.length === selected.length) {
      this.setState({ selected: [] });
    } else {
      this.setState({ selected: options.map(option => option.id) });
    }
  };

  handleClose = () => {
    if (this.state.isOpen) this.setState({ isOpen: false });
  };

  handleClickCustomOption = value => () => {
    if (this.props.isMultiple) return false;

    this.setState({ selected: [String(value)], isOpen: false });
  };

  handleChangeCheckbox = event => {
    const { value } = event.target;
    let selected = [];

    const index = this.state.selected.findIndex(item => item === value);
    if (!~index) {
      selected = this.state.selected.concat(value);
    } else {
      selected = selected.concat(
        this.state.selected.slice(0, index),
        this.state.selected.slice(index + 1)
      );
    }

    this.setState({ selected, selectedMultipleIsChange: true });
  };

  handleRemoveSelected = id => event => {
    if (event) event.stopPropagation();
    const idStr = String(id);
    const index = this.state.selected.findIndex(item => item === idStr);
    if (~index) {
      const selected = [].concat(
        this.state.selected.slice(0, index),
        this.state.selected.slice(index + 1)
      );

      this.setState({ selected });
    }
  };

  navigationKeyCodes = [13, 38, 40];

  handleOptionNavigation = event => {
    const { keyCode } = event;
    const { options, isMultiple } = this.props;
    const { focusedOptionId, selected } = this.state;

    if (options.length && this.navigationKeyCodes.includes(keyCode)) {
      if (keyCode === 13 && focusedOptionId !== null) {
        const focusedOption = options.find(
          option => option.id === focusedOptionId && !option.disabled
        );

        if (focusedOption) {
          let selectedArr = [];
          if (isMultiple) {
            const index = selected.findIndex(item => item === focusedOptionId);

            if (!~index) {
              selectedArr = selected.concat(focusedOptionId);
            } else {
              selectedArr = selectedArr.concat(
                selected.slice(0, index),
                selected.slice(index + 1)
              );
            }
          } else {
            selectedArr.push(focusedOptionId);
          }

          event.preventDefault();
          event.stopPropagation();
          this.setState({ selected: selectedArr, isOpen: isMultiple });
        }
      } else if (keyCode === 38 || keyCode === 40) {
        const focusedIndex =
          focusedOptionId !== null
            ? options.findIndex(option => option.id === focusedOptionId)
            : -1;
        let newFocusedOption = null;

        if (keyCode === 38) {
          if (!~focusedIndex) {
            newFocusedOption = options[options.length - 1];
          } else if (focusedIndex > 0) {
            newFocusedOption = options[focusedIndex - 1];
          }
        } else if (keyCode === 40) {
          if (!~focusedIndex) {
            newFocusedOption = options[0];
          } else if (focusedIndex < options.length - 1) {
            newFocusedOption = options[focusedIndex + 1];
          }
        }

        if (newFocusedOption) {
          this.setState({ focusedOptionId: newFocusedOption.id });
        }
      }
    }
  };

  handlersForOption = {
    onClick: this.handleClickCustomOption
  };

  getTitle = memoizeOne((options, selected, placeholder) => {
    if (!selected.length) return placeholder;
    const selectedItem = options.find(
      option => String(option.id) === String(selected[0])
    );

    return selectedItem ? selectedItem.name : placeholder;
  });

  renderHead() {
    const {
      options,
      placeholder,
      isMultiple,
      searchMode,
      isLoading,
      iconClearProps
    } = this.props;
    const { selected, searchValue } = this.state;
    const title = !isMultiple && this.getTitle(options, selected, placeholder);

    let head = null;
    switch (true) {
      case searchMode === "searchRequestControl" || searchMode === "custom":
        head = (
          <SearchInput
            name={this.props.name}
            fullWidth
            placeholder={title}
            value={searchValue}
            onKeyDown={this.handleOptionNavigation}
            onChange={this.handleChangeSearchValue}
            onFocus={this.handleFocusSearchInput}
            onBlur={this.handleBlurSearchInput}
          />
        );
        break;
      case isMultiple && !!selected.length:
        head = (
          <SelectedBlock>
            {selected.map(id => {
              const option = options.find(opt => String(opt.id) === id);
              const value = option ? option.name : id;
              return (
                <SelectedItem
                  onClick={this.handleRemoveSelected(id)}
                  title={value}
                  key={id}
                >
                  {value}
                </SelectedItem>
              );
            })}
          </SelectedBlock>
        );
        break;
      default:
        head = (
          <ReadOnlyInput
            name={this.props.name}
            readOnly
            fullWidth
            value={title || placeholder}
          />
        );
    }

    return (
      <Field onClick={this.handleToggleOpen}>
        {head}
        {iconClearProps && !!selected.length && (
          <IconClearWrap name={this.props.name} {...iconClearProps}>
            <IconClear icon="clear" />
          </IconClearWrap>
        )}
        <StyledIcon icon="arrow_drop_down" />
        {isLoading && (
          <PreloaderContainer>
            <Preloader size="small" />
          </PreloaderContainer>
        )}
      </Field>
    );
  }

  render() {
    const {
      options,
      isMultiple,
      disabled,
      placeholder,
      selectedAll,
      isLoading
    } = this.props;
    const { isOpen, selected, focusedOptionId } = this.state;

    if (disabled) {
      return (
        <CustomSelect>
          <Field>
            <ReadOnlyInput disabled readOnly fullWidth value={placeholder} />
            <StyledIcon icon="arrow_drop_down" />
          </Field>
        </CustomSelect>
      );
    }

    return (
      <>
        {!!selectedAll && (
          <TitleCheckbox
            type="checkbox"
            onChange={this.handleToggleSelectedAll}
            checked={selected.length === options.length}
            text={selectedAll}
          />
        )}
        <CustomSelect active={isOpen}>
          <Overlay onClick={this.handleToggleOpen} />
          {this.renderHead()}
          <List isLoading={isLoading}>
            <ScrollArea
              horizontal={false}
              style={StyleScrollArea}
              verticalScrollbarStyle={StyleVerticalScrollbarStyle}
            >
              {options.map(option => {
                const { id, name, ...rest } = option;

                return (
                  <Option
                    {...rest}
                    value={id}
                    handlers={this.handlersForOption}
                    key={`${name}_${id}`}
                    inFocus={focusedOptionId === id}
                  >
                    {isMultiple ? (
                      <Checkbox
                        onChange={this.handleChangeCheckbox}
                        name={id.toString()}
                        type="checkbox"
                        value={id}
                        id={`${this.props.name}_${name}_${id}`}
                        checked={selected.includes(String(id))}
                        text={name}
                      />
                    ) : (
                      name
                    )}
                  </Option>
                );
              })}
            </ScrollArea>
          </List>
        </CustomSelect>
      </>
    );
  }
}

export default Select;
