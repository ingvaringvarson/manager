import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { ifProp, palette } from "styled-tools";
import { space } from "styled-system";

import TextMixin from "../../mixins/Text";

const Label = styled.label`
  ${TextMixin};
  display: block;
  margin-bottom: 4px;
  font-size: 12px;
`;

const Notice = styled.p`
  margin: 5px 0 0 10px;
  font-size: 10px;
  color: ${ifProp("invalid", palette("primary", 0), palette("grayscale", 0))};
`;

const StyledRow = styled.div`
  display: flex;
  align-items: center;
`;

const StyledField = styled.div`
  display: flex;
  flex-direction: column;
  ${space};
`;

const Field = ({
  label,
  notice,
  id,
  invalid,
  hasRowWrap,
  children,
  ...props
}) => {
  return (
    <div>
      {label && <Label htmlFor={id}>{label}</Label>}
      <StyledField {...props}>
        {!hasRowWrap ? children : <StyledRow>{children}</StyledRow>}
        {notice && <Notice invalid={invalid}>{notice}</Notice>}
      </StyledField>
    </div>
  );
};

Field.defaultProps = {
  id: ""
};

Field.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  invalid: PropTypes.bool,
  notice: PropTypes.string,
  children: PropTypes.any.isRequired // посмотреть, какие могут быть типы
};

export default Field;
