import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { space } from "styled-system";

const StyledBlock = styled.div`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.05);
  border-radius: 5px;
  ${space};
`;

const Block = ({ children, ...props }) => {
  return <StyledBlock {...props}>{children}</StyledBlock>;
};

Block.propTypes = {
  children: PropTypes.any
};

export default Block;
