import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { space } from "styled-system";
import { ifProp } from "styled-tools";

const Body = styled.div`
  position: relative;
  padding: 16px;

  ${ifProp(
    "table",
    css`
      padding-top: 0;
    `
  )}

  ${space};
`;

const BlockContent = ({ children, ...props }) => {
  return <Body {...props}>{children}</Body>;
};

BlockContent.propTypes = {
  children: PropTypes.any
};

export default BlockContent;
