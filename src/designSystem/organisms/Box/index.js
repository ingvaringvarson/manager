import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import {
  display,
  flexDirection,
  gridTemplateColumns,
  gridRowGap,
  gridColumnGap,
  gridColumn,
  alignItems,
  space,
  borderBottom,
  borderColor,
  justifyContent,
  color,
  textAlign,
  borderTop,
  position,
  background
} from "styled-system";

const StyledBox = styled.div`
  ${display};
  ${flexDirection};
  ${gridTemplateColumns};
  ${gridRowGap};
  ${gridColumnGap};
  ${gridColumn};
  ${alignItems};
  ${space};
  ${borderBottom};
  ${borderColor};
  ${color};
  ${justifyContent};
  ${space};
  ${textAlign};
  ${color};
  ${borderTop};
  ${borderBottom};
  ${borderColor};
  ${position};
  ${background};
`;

const Box = ({ children, ...props }) => {
  return <StyledBox {...props}>{children}</StyledBox>;
};

Box.propTypes = {
  children: PropTypes.any
};

export default Box;
