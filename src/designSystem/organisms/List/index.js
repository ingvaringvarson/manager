import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { palette } from "styled-tools";
import { space } from "styled-system";
import Text from "../../mixins/Text";

const StyledList = styled.div`
  ${space};
`;

export const ListRow = styled.div`
  display: flex;
  align-items: baseline;

  &:not(:last-of-type) {
    margin-bottom: 10px;
  }
`;

export const ListTitle = styled.div`
  ${Text}
  position: relative;
  flex-basis: 35%;
  max-width: 35%;
  padding-right: 16px;
  overflow: hidden;
  color: ${palette("secondary", 0)};

  :after {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    height: 100%;
    width: 16px;
    background: linear-gradient(90deg, transparent, #ffffff);
  }
`;

export const ListDesc = styled.div`
  ${Text}
  flex-basis: 65%;
  max-width: 65%;
`;

export const List = ({ children, ...props }) => {
  return <StyledList {...props}>{children}</StyledList>;
};

List.propTypes = {
  children: PropTypes.any
};
