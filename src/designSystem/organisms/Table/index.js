import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { space } from "styled-system";
import { ifProp, palette } from "styled-tools";

const StyledTable = styled.div`
  ${ifProp(
    "subTable",
    css`
      padding: 8px 16px;
      border-radius: 5px;
      background: ${ifProp("whiteBG", "#fff", palette("grayscale", 2))};
    `
  )}

  ${space};
`;

const Table = ({ children, ...props }) => {
  return <StyledTable {...props}>{children}</StyledTable>;
};

Table.propTypes = {
  children: PropTypes.any
};

export default Table;
