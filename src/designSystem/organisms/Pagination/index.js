import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { space } from "styled-system";

import Container from "../../templates/Container";
import Row from "../../templates/Row";
import Button from "../../atoms/Button";
import Input from "../../atoms/Input";
import Text from "../../atoms/Text";

const StyledPagination = styled.div`
  ${space};
`;

const Counter = styled.div`
  display: flex;
  align-items: center;
  padding: 0 16px;
`;

const StyledInput = styled(Input)`
  width: 50px;
  margin: 0 10px;
`;

const Pagination = ({
  count,
  inputProps,
  prevButtonProps,
  nextButtonProps,
  ...props
}) => {
  return (
    <StyledPagination {...props}>
      <Container>
        <Row justifyCenter>
          <Button {...prevButtonProps} secondary>
            Назад
          </Button>
          <Counter>
            <Text>Страница</Text>
            <StyledInput {...inputProps} />
            <Text>из {count}</Text>
          </Counter>
          <Button {...nextButtonProps} secondary>
            Вперед
          </Button>
        </Row>
      </Container>
    </StyledPagination>
  );
};

Pagination.propTypes = {
  count: PropTypes.number
};

export default Pagination;
