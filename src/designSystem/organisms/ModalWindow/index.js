import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Block from "../../organisms/Block";

const Overlay = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 9999;
  background: rgba(59, 59, 59, 0.5);
`;

const Window = styled(Block)`
  position: fixed;
  top: 80px;
  left: 50%;
  z-index: 10000;
  width: 400px;
  transform: translateX(-50%);
`;

const ModalWindow = ({ children, ...props }) => {
  return (
    <Overlay {...props}>
      <Window>{children}</Window>
    </Overlay>
  );
};

ModalWindow.propTypes = {
  children: PropTypes.any
};

export default ModalWindow;
