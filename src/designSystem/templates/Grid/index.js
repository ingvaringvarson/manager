import styled, { css } from "styled-components";
import { ifProp } from "styled-tools";
import { space } from "styled-system";

const Grid = styled.div`
  display: grid;
  grid-column-gap: 16px;
  align-items: start;
  grid-template-columns: var(--column-width);

  ${ifProp(
    "filterLeft",
    css`
      --column-width: 350px minmax(1200px, auto);
    `
  )}

  ${ifProp(
    "filterRight",
    css`
      --column-width: minmax(1200px, auto) 350px;
    `
  )}

  ${ifProp(
    "subGrid",
    css`
      --column-width: minmax(850px, auto) 350px;
    `
  )}

  ${space}
`;

export default Grid;
