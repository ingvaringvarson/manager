import React from "react";
import PropsTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp } from "styled-tools";
import { space, maxWidth } from "styled-system";

const StyledCol = styled.div`
  ${ifProp(
    "inlineFlex",
    css`
      display: inline-flex;
    `
  )};
  ${ifProp(
    "flex",
    css`
      display: flex;
    `
  )};
  ${ifProp(
    "order",
    css`
      order: ${props => props.order};
    `
  )};
  ${ifProp(
    "basis",
    css`
      flex-basis: ${props => props.basis};
    `
  )};
  ${ifProp(
    "grow",
    css`
      flex-grow: ${props => props.grow};
    `
  )};
  ${ifProp(
    "shrink",
    css`
      flex-shrink: ${props => props.shrink};
    `
  )};
  ${ifProp(
    "noShrink",
    css`
      flex-shrink: 0;
    `
  )};

  ${space};
  ${maxWidth};
`;

const Column = ({ children, ...props }) => {
  return <StyledCol {...props}>{children}</StyledCol>;
};

Column.propsTypes = {
  children: PropsTypes.any
};

export default Column;
