import React from "react";
import PropsTypes from "prop-types";
import styled, { css } from "styled-components";
import { ifProp } from "styled-tools";
import { space } from "styled-system";

const StyledRow = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-content: stretch;
  margin: 0 -8px;

  ${ifProp(
    "inline",
    css`
      display: inline-flex;
    `
  )};
  ${ifProp(
    "rowReverse",
    css`
      flex-direction: row-reverse;
    `
  )};
  ${ifProp(
    "column",
    css`
      flex-direction: column;
    `
  )};
  ${ifProp(
    "columnReverse",
    css`
      flex-direction: column-reverse;
    `
  )};
  ${ifProp(
    "nowrap",
    css`
      flex-wrap: nowrap;
    `
  )};
  ${ifProp(
    "wrap",
    css`
      flex-wrap: wrap;
    `
  )};
  ${ifProp(
    "wrapReverse",
    css`
      flex-wrap: wrap-reverse;
    `
  )};
  ${ifProp(
    "justifyStart",
    css`
      justify-content: flex-start;
    `
  )};
  ${ifProp(
    "justifyEnd",
    css`
      justify-content: flex-end;
    `
  )};
  ${ifProp(
    "justifyCenter",
    css`
      justify-content: center;
    `
  )};
  ${ifProp(
    "justifyBetween",
    css`
      justify-content: space-between;
    `
  )};
  ${ifProp(
    "justifyAround",
    css`
      justify-content: space-around;
    `
  )};
  ${ifProp(
    "contentStart",
    css`
      align-content: flex-start;
    `
  )};
  ${ifProp(
    "contentEnd",
    css`
      align-content: flex-end;
    `
  )};
  ${ifProp(
    "contentCenter",
    css`
      align-content: center;
    `
  )};
  ${ifProp(
    "contentSpaceBetween",
    css`
      align-content: space-between;
    `
  )};
  ${ifProp(
    "contentSpaceAround",
    css`
      align-content: space-around;
    `
  )};
  ${ifProp(
    "contentStretch",
    css`
      align-content: stretch;
    `
  )};
  ${ifProp(
    "alignStart",
    css`
      align-items: flex-start;
    `
  )};
  ${ifProp(
    "alignEnd",
    css`
      align-items: flex-end;
    `
  )};
  ${ifProp(
    "alignCenter",
    css`
      align-items: center;
    `
  )};
  ${ifProp(
    "alignBaseline",
    css`
      align-items: baseline;
    `
  )};
  ${ifProp(
    "alignStretch",
    css`
      align-items: stretch;
    `
  )};
  ${ifProp(
    "center",
    css`
      align-items: center;
      justify-content: center;
    `
  )};
  ${space};
`;

const Row = ({ children, ...props }) => {
  return <StyledRow {...props}>{children}</StyledRow>;
};

Row.propsTypes = {
  children: PropsTypes.any
};

export default Row;
