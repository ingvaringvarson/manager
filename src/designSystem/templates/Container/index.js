import React from "react";
import PropsTypes from "prop-types";
import styled from "styled-components";

const StyledConteiner = styled.div`
  width: 100%;
  margin-right: auto;
  margin-left: auto;
  padding: 0 8px;
`;

const Container = ({ children, ...props }) => {
  return <StyledConteiner {...props}>{children}</StyledConteiner>;
};

Container.propsTypes = {
  children: PropsTypes.any
};

export default Container;
