const ResetButton = `
  margin: 0;
  padding: 0;
  border: 0 none;
  background: none;
  cursor: pointer;
`;

export default ResetButton;
