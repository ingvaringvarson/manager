import { css } from "styled-components";
import { ifProp, palette } from "styled-tools";
import { space } from "styled-system";

const ButtonMixin = css`
  position: relative;
  display: inline-block;
  vertical-align: middle;
  box-sizing: border-box;
  width: ${ifProp("fullWidth", "100%", "auto")};
  height: 36px;
  padding: 0 15px;
  overflow: hidden;
  font-weight: normal;
  font-size: 14px;
  text-align: center;
  text-transform: ${ifProp("defaultCase", "none", "uppercase")};
  text-decoration: none;
  line-height: 18px;
  color: ${palette("white")};
  border: 1px solid transparent;
  border-radius: 5px;
  outline: none;
  background: ${palette("primary", 0)};
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0);
  cursor: pointer;
  transition: color 0.3s, background 0.3s, box-shadow 0.3s;
  transform: translate3d(0, 0, 0);

  :not(:disabled):hover {
    background: ${palette("primary", 1)};
  }

  :disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }

  :focus {
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.25);
  }

  ::after {
    content: '';
    display: block;
    position: absolute;
    z-index: -1;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    pointer-events: none;
    background-image: radial-gradient(
      circle,
      rgba(255, 255, 255, 0.5) 10%,
      transparent 10.01%
    );
    background-repeat: no-repeat;
    background-position: 50%;
    transform: scale(10, 10);
    opacity: 0;
    transition: transform 0.5s, opacity 1s;
  }

  :active::after {
    transform: scale(0, 0);
    opacity: 1;
    transition: 0s;
  }

  ${ifProp(
    "secondary",
    css`
      color: ${palette("primary", 0)};
      border: 1px solid ${palette("primary", 0)};
      background: white;

      :not(:disabled):hover {
        color: white;
        background: ${palette("primary", 0)};
      }
    `
  )}

  ${ifProp(
    "white",
    css`
      color: ${palette("general", 1)};
      border: 1px solid ${palette("general", 1)};
      background: white;

      :not(:disabled):hover {
        color: white;
        background: ${palette("general", 1)};
      }
    `
  )}

  ${ifProp(
    "transparent",
    css`
      color: ${palette("general", 1)};
      border: 1px solid ${palette("general", 1)};
      background: transparent;

      :not(:disabled):hover {
        color: white;
        background: ${palette("general", 1)};
      }
    `
  )}

  ${ifProp(
    "simple",
    css`
      font-weight: 500;
      color: ${palette("general", 1)};
      border: 1px solid transparent;
      background: transparent;

      :not(:disabled):hover {
        color: ${palette("general", 1)};
        background: ${palette("grayscale", 1)};
      }
    `
  )}

  ${ifProp(
    "noBorder",
    css`
      border: none;
    `
  )}

  ${ifProp(
    "upload",
    css`
      border: 1px dashed ${palette("general", 1)};
    `
  )}

  ${space};
`;

export default ButtonMixin;
