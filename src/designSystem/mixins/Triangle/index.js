export const Triangle = `
  ::before {
    content: "";
    position: absolute;
    z-index: 1;
    width: 0;
    height: 0;
    border-style: solid;
  }

  ::after {
    content: "";
    position: absolute;
    z-index: -1;
    width: 36px;
    height: 36px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
  }
`;

export const TriangleTop = `
  ::before {
    top: -18px;
    border-width: 0 18px 18px 18px;
    border-color: transparent transparent #ffffff transparent;
  }

  ::after {
    top: -10px;
    transform: rotate(45deg);
  }
`;

export const TriangleRight = `
  ::before {
    top: 28px;
    right: -18px;
    border-width: 18px 0 18px 18px;
    border-color: transparent transparent transparent #ffffff;
  }

  ::after {
    top: 28px;
    right: -10px;
    transform: rotate(45deg);
  }
`;
