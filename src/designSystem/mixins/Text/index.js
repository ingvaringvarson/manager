const TextMixin = `
  font-family: "Roboto", sans-serif;
  font-size: 14px;
  color: #3B3B3B;
`;

export default TextMixin;
