const Item = `
  position: relative;
  border-left: 2px solid transparent;

  :hover {
    background: rgba(241, 241, 241, 0.3);
  }
`;

export default Item;
