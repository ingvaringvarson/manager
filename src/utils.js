import nanoid from "nanoid";
import { customView } from "./helpers/productFilters";

export function serializationParamsToString(params = {}, onsetString = "") {
  const keys = Object.keys(params);

  if (!keys.length) return onsetString;

  return Object.keys(params).reduce((str, key, index) => {
    const paramString = `${key}=${encodeURIComponent(params[key])}`;

    if (!index && !str) {
      return `?${paramString}`;
    }

    return `${str}&${paramString}`;
  }, onsetString);
}

export function generateUrlWithQuery(
  data = {},
  resetOldQuery = false,
  ignoreEmptyOptions = false
) {
  const { pathname, oldQuery = {}, newQuery = {}, resetKeys = null } = data;
  let allQuery = resetOldQuery ? newQuery : { ...oldQuery, ...newQuery };
  if (ignoreEmptyOptions) {
    allQuery = removeEmptyOptionsFromObject(allQuery);
  }

  const queryString = Object.keys(allQuery).reduce((str, key, index) => {
    if (allQuery[key] === null || (resetKeys && resetKeys.includes(key))) {
      return str;
    }

    const paramsStr =
      Array.isArray(allQuery[key]) && allQuery[key].length
        ? allQuery[key].reduce((str, value, index) => {
            const paramStr = `${key}=${encodeURIComponent(value)}`;

            return `${str}${index ? "&" : ""}${paramStr}`;
          }, "")
        : `${key}=${encodeURIComponent(allQuery[key])}`;

    return `${str}${!index || !str ? "?" : "&"}${paramsStr}`;
  }, "");

  return `${pathname}${queryString}`;
}

export function arrayToObject(arr, id = "id", model) {
  if (!Array.isArray(arr) || !arr.length) return {};

  return arr.reduce(
    (acc, entity) => ({ ...acc, [entity[id]]: model ? model(entity) : entity }),
    {}
  );
}

export function objectToArray(obj) {
  return Object.keys(obj).map(key => obj[key]);
}

export function generateId() {
  return nanoid();
}

export function getWeekDay(day, mCase) {
  const days = {
    acc: [
      "воскресенье",
      "понедельник",
      "вторник",
      "среду",
      "четверг",
      "пятницу",
      "субботу"
    ],
    nom: [
      "воскресенье",
      "понедельник",
      "вторник",
      "среда",
      "четверг",
      "пятница",
      "суббота"
    ]
  };

  return mCase && days[mCase] ? days[mCase][day] : days.nom[day];
}

export function getDayRelativeToToday(day) {
  const days = ["сегодня", "завтра", "послезавтра"];

  return days[day];
}

export function getMonthName(month, mCase) {
  const days = {
    gen: [
      "января",
      "февраля",
      "марта",
      "апреля",
      "мая",
      "июня",
      "июля",
      "августа",
      "сентября",
      "октября",
      "ноября",
      "декабря"
    ],
    nom: [
      "январь",
      "февраль",
      "март",
      "апрель",
      "май",
      "июнь",
      "июль",
      "август",
      "сентябрь",
      "октябрь",
      "ноябрь",
      "декабрь"
    ]
  };

  return mCase && days[mCase] ? days[mCase][month] : days.nom[month];
}

export function getTime(arg) {
  const date = isType(arg, "object") ? arg : new Date(arg);
  const minutes =
    date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
  const hours = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();

  return `${hours}:${minutes}`;
}

export function getDate(arg) {
  if (!arg) {
    return "";
  }

  const date = isType(arg, "object") ? arg : new Date(arg);
  const month =
    date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
  const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();

  return `${day}.${month}.${date.getFullYear()}`;
}

export function getDateWithMonthName(arg) {
  const date = isType(arg, "object") ? arg : new Date(arg);
  const month = date.getMonth();
  const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();

  return `${day} ${getMonthName(month, "gen")} ${date.getFullYear()}`;
}

export function isValidDate(value) {
  const dateWrapper = new Date(String(value));
  return !isNaN(dateWrapper.getDate());
}

export function indexingEntity(entity = {}, index = null) {
  const initProps = isNumeric(index) ? { _index: index, _id: nanoid() } : {};
  return Object.keys(entity).reduce((indexedProps, propName) => {
    if (isType(entity[propName], "object")) {
      return {
        ...indexedProps,
        [propName]: indexingEntity(entity[propName])
      };
    } else if (
      Array.isArray(entity[propName]) &&
      isType(entity[propName][0], "object")
    ) {
      return {
        ...indexedProps,
        [propName]: entity[propName].map((item, index) => {
          const indexedPropsForItem = indexingEntity(item, index);
          const id = !item.id && item.Id ? item.Id : item.id;

          if (id) {
            return {
              ...indexedPropsForItem,
              id
            };
          }
          return {
            ...indexedPropsForItem
          };
        })
      };
    }

    const id = !entity.id && entity.Id ? entity.Id : entity.id;
    if (id) {
      return { ...indexedProps, id, [propName]: entity[propName] };
    }

    return { ...indexedProps, [propName]: entity[propName] };
  }, initProps);
}

export function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

export function removeIndexFromEntity(entity = {}) {
  return Object.keys(entity).reduce((indexedProps, propName) => {
    if (isType(entity[propName], "object")) {
      return {
        ...indexedProps,
        [propName]: removeIndexFromEntity(entity[propName])
      };
    } else if (
      Array.isArray(entity[propName]) &&
      isType(entity[propName][0], "object")
    ) {
      return {
        ...indexedProps,
        [propName]: entity[propName].map(removeIndexFromEntity)
      };
    } else if (propName === "_index" || propName === "_id") {
      return indexedProps;
    }

    return { ...indexedProps, [propName]: entity[propName] };
  }, {});
}

export function isType(value, typeName) {
  const regex = /^\[object (\S+?)\]$/;
  const matches = Object.prototype.toString.call(value).match(regex) || [];

  return (matches[1] || "undefined").toLowerCase() === typeName;
}

export function compareDateShapedStringFromArray(
  arr = [{}],
  dateOptionName = "",
  findLowest = false
) {
  return arr.reduce((result, currentObject) => {
    const prevDate = new Date(result);
    const currentDate = new Date(currentObject[dateOptionName]);

    if (findLowest) {
      if (prevDate > currentDate) {
        return currentObject[dateOptionName];
      }
      return result;
    }

    if (prevDate > currentDate) {
      return result;
    }
    return currentObject[dateOptionName];
  }, arr[0][dateOptionName]);
}

export function removeEmptyOptionsFromObject(entity = {}) {
  return Object.keys(entity).reduce((result, currentKey) => {
    if (entity[currentKey] === "" || entity[currentKey] === undefined) {
      return result;
    }
    result[currentKey] = entity[currentKey];
    return result;
  }, {});
}

export function getValueInDepth(entity, keys) {
  if (!Array.isArray(keys) || !entity) return null;

  return keys.reduce((values, key) => {
    if (isType(key, "number") && Array.isArray(values) && values.length > key) {
      return values[key] || null;
    }

    return isType(values, "object") && values.hasOwnProperty(key)
      ? values[key]
      : null;
  }, entity);
}

export function entitiesToOptions(entities, getName, getId) {
  return entities.reduce((options, entity) => {
    const name = getName(entity);
    const id = getId(entity);

    if (name === null || id === null) return options;

    return options.concat({
      id,
      name,
      entity
    });
  }, []);
}

export function digitsForNumbers(number = "", params = {}) {
  const { digit = 3, separator = " " } = params;
  const pattern = new RegExp(`(\\d)(?=(\\d{${digit}})+([^\\d]|$))`, "g");

  return String(number).replace(pattern, `$1${separator}`);
}

export function generateUrlWithFilters(params) {
  const { query = {}, pathname, filters = [], resetPage = false } = params;
  const newQuery = resetPage && !!query.page ? { ...query, page: null } : query;

  filters.forEach(filter => {
    const { urlProperty, type, value = {} } = filter;

    if (!value) {
      newQuery[urlProperty] = null;
    } else if (
      type.view === "parameters" ||
      (type.view === "minmax" &&
        customView[urlProperty] &&
        (type.ui === "select" || type.ui === "checkboxlist"))
    ) {
      if (Array.isArray(query[urlProperty])) {
        newQuery[urlProperty] = newQuery[urlProperty]
          .filter(entity => value[entity] !== null)
          .concat(Object.keys(value).filter(entity => value[entity] !== null));
      } else {
        const currentVal = newQuery[urlProperty];

        newQuery[urlProperty] = Object.keys(value).filter(
          entity => value[entity] !== null
        );
        if (currentVal && value[currentVal] !== null)
          newQuery[urlProperty].unshift(currentVal);
      }
    } else if (type.view === "minmax") {
      newQuery[urlProperty] = `${value[0]}__${value[1]}`;
    }
  });

  const newSearchStr = Object.keys(newQuery).reduce(
    (result, nameQuery, indexQuery) => {
      if (
        (Array.isArray(newQuery[nameQuery]) && !newQuery[nameQuery].length) ||
        newQuery[nameQuery] === null
      )
        return result;
      let queryStr = "";

      if (Array.isArray(newQuery[nameQuery])) {
        queryStr = newQuery[nameQuery].reduce(
          (resultQuery, valueQuery, indexValue) => {
            if (valueQuery === null) return resultQuery;
            return `${resultQuery}${
              !indexValue || !resultQuery ? "" : "&"
            }${nameQuery}=${valueQuery}`;
          },
          ""
        );
      } else {
        queryStr = `${nameQuery}=${newQuery[nameQuery]}`;
      }
      return `${result}${!indexQuery || !result ? "?" : "&"}${queryStr}`;
    },
    ""
  );

  return `${pathname}${newSearchStr}`;
}

export function getCurrentDateInISO() {
  const date = new Date();

  const str = date.toISOString();
  return str.split(".")[0];
}

export const mapArrOfIdsWithEnumerations = (field, entity, enumerations) => {
  const { id, enumerationKey } = field;
  const arr = entity[id];
  if (!arr) return null;

  const enums = enumerations[enumerationKey]
    ? enumerations[enumerationKey].map
    : {};

  return arr.reduce((res, elId) => {
    if (enums[elId]) res.push(enums[elId]);
    return res;
  }, []);
};

export const getStringWithEllipsis = (str, len) =>
  str.length > len && len > 4 ? str.substr(0, len - 4) + "..." : str;

export function convertDateToIsoWithoutMs(date) {
  return date.toISOString().split(".")[0];
}

export function getNumberWrappedWithRub(entity, keyPath) {
  const value = getValueInDepth(entity, keyPath);
  return value ? `${value} руб.` : "";
}

export function findEnumerationById(enumerations, enumKey, value) {
  return enumerations[enumKey].list.find(e => e.id === value);
}

export function mapInvalidFields(values, fields) {
  return Object.entries(fields).reduce((res, [key, field]) => {
    const { validators, revertMask } = field.formControlProps;
    if (!validators || validators.length === 0 || field.isActive === false) {
      return res;
    }

    const fieldErrors = [];

    validators.forEach(v => {
      const fieldValue = revertMask
        ? revertMask(values[key], values)
        : values[key];
      const error = v(fieldValue, values);

      if (error) fieldErrors.push(error);
    });

    if (fieldErrors.length > 0) res[key] = fieldErrors;
    return res;
  }, {});
}

export function getPrepaidDescription(value) {
  return value ? "Предоплата" : "Без предоплаты";
}

export function getMainVendorCodeInfo(vendorCodes = []) {
  if (!vendorCodes.length) return null;

  const actualVendor = vendorCodes.find(v => v.isActual);

  return actualVendor ? actualVendor : vendorCodes[0];
}

export function getMainVendorCode(vendorCodes = []) {
  const mainVendorCodeInfo = getMainVendorCodeInfo(vendorCodes);

  return mainVendorCodeInfo ? mainVendorCodeInfo.vendorCode : null;
}

export function getMainVendorCodeKey(vendorCodes = []) {
  const mainVendorCodeInfo = getMainVendorCodeInfo(vendorCodes);

  return mainVendorCodeInfo ? mainVendorCodeInfo.vendorCodeKey : null;
}

export function mapIdToSelectedIds(id) {
  return id === null ? [] : [id];
}
