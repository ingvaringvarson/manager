const createTaskPrepareToMoveCommand = {
  id: "createTaskPrepareToMove",
  title: "Подготовить к выдаче для перемещения",
  defaultValues: {
    type: [2],
    state: [1]
  }
};

export default {
  createTaskPrepareToMoveCommand
};
