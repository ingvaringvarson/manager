import { byAllEnitiesStates } from "./../helpers/filterMethods";

const waitingForApprovalCommand = {
  id: "waitingForApproval",
  title: "Ожидание согласования",
  availableStates: [1]
};

const agreedBySupplierCommand = {
  id: "agreedBySupplier",
  title: "Согласован поставщиком",
  availableStates: [1, 2]
};

const deliverGoodCommand = {
  id: "deliverGood",
  title: "Выдать товар",
  availableStates: [3]
};

const returnIsNotAgreedCommand = {
  id: "returnIsNotAgreed",
  title: "Возврат не согласован",
  availableStates: [1, 2]
};

const printDocumentsCommand = {
  id: "printDocuments",
  title: "Печать документов"
};

export default {
  waitingForApprovalCommand,
  agreedBySupplierCommand,
  deliverGoodCommand,
  returnIsNotAgreedCommand,
  printDocumentsCommand
};

export const filterPurchaseReturnsCommands = (commands, purchaseReturns) => {
  const statePropertyName = "state_return";
  const returns = purchaseReturns.map(pr => pr.purchase_return);
  const byAllPurchaseReturnsStates = byAllEnitiesStates(
    returns,
    statePropertyName
  );
  return returns.length ? commands.filter(byAllPurchaseReturnsStates) : [];
};

export const getMainCommandByPurchaseReturn = purchaseReturn => {
  switch (purchaseReturn.state_return) {
    case 1:
      return waitingForApprovalCommand.id;
    case 2:
      return agreedBySupplierCommand.id;
    case 3:
      return deliverGoodCommand.id;
  }

  return null;
};
