import { byAllPositionsStates } from "./../helpers/filterMethods";
import { pm_discount_create } from "../constants/permissions";

const availableStates = [1, 2, 4, 5, 6, 16, 17, 19, 28, 29];

const divideCommand = {
  id: "divide",
  title: "Разделить позицию",
  forList: true,
  availableStates,
  customFilter: position => position.count > 1
};

const discountCommand = {
  id: "discount",
  title: "Добавить скидку",
  forList: true,
  availableStates,
  permissionsMarker: [pm_discount_create]
};

const createBillCommand = {
  id: "createBill",
  title: "Создать счет",
  forList: true,
  availableStates
};

const rejectByProductCommand = {
  id: "rejectByProduct",
  title: "Отказаться от товара",
  forList: true,
  availableStates
};

const createReturnCommand = {
  id: "createReturn",
  title: "Создать возврат",
  availableStates: [7]
};

const returnPrepaymentCommand = {
  id: "returnPrepayment",
  title: "Вернуть предоплату",
  forList: true,
  availableStates: [10, 11, 12, 13, 14, 18]
};

export default {
  divideCommand,
  discountCommand,
  createBillCommand,
  rejectByProductCommand,
  createReturnCommand,
  returnPrepaymentCommand
};

export const filterOrderPositionsCommands = (commands, positions) =>
  positions.length ? commands.filter(byAllPositionsStates(positions)) : [];
