import { byAllEnitiesStates } from "./../helpers/filterMethods";
import { pm_purchase_order_reject } from "../constants/permissions";

const orderedFromSupplierCommand = {
  id: "orderedFromSupplier",
  title: "Заказан у поставщика",
  availableStates: [1]
};

const takeProductCommand = {
  id: "takeProduct",
  title: "Принять товар",
  availableStates: [2, 3]
};

// ToDo:
// * добавить работу с несколькими заказами постащику
const rejectCommand = {
  id: "reject",
  title: "Отказаться от заказа",
  forList: true,
  availableStates: [1, 2, 3],
  permissionsMarker: [pm_purchase_order_reject]
};

const changeDeliveryTypeCommand = {
  id: "changeDeliveryType",
  title: "Изменить способ получения",
  availableStates: [1, 2, 3]
};

function changePriceAndCountPositionsFilter(positions) {
  const availableStatesForPositions = [1, 19, 20];
  return positions.filter(p =>
    availableStatesForPositions.some(s => p.good_state === s)
  );
}

const changePriceAndCountCommand = {
  id: "changePriceAndCount",
  title: "Изменить цену и количество",
  customFilter(purchaseOrder) {
    return (
      Array.isArray(purchaseOrder.positions) &&
      changePriceAndCountPositionsFilter(purchaseOrder.positions).length !== 0
    );
  }
};

const printDocumentsCommand = {
  id: "printDocuments",
  title: "Печать документов",
  blockedStates: [5, 6]
};

const createPurchaseReturnCommand = {
  id: "createPurchaseReturn",
  title: "Создать возврат поставщику",
  forList: true,
  availableStates: [3, 4]
};

export default {
  orderedFromSupplierCommand,
  takeProductCommand,
  rejectCommand,
  changeDeliveryTypeCommand,
  changePriceAndCountCommand,
  printDocumentsCommand,
  createPurchaseReturnCommand
};

export const filters = {
  changePriceAndCountPositionsFilter
};

export const filterPurchaseOrdersCommands = (commands, purchaseOrders) => {
  const statePropertyName = "state_supplier_order";
  const orders = purchaseOrders.map(po => po.order);
  const byAllPurchaseOrdersStates = byAllEnitiesStates(
    orders,
    statePropertyName
  );
  return orders.length ? commands.filter(byAllPurchaseOrdersStates) : [];
};

export const getMainCommandByPurchaseOrder = purchaseOrder => {
  switch (purchaseOrder.state_supplier_order) {
    case 1:
      return orderedFromSupplierCommand.id;
    case 2:
    case 3:
      return takeProductCommand.id;
  }

  return null;
};
