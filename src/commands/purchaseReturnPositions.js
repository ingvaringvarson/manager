import { byAllPositionsStates } from "./../helpers/filterMethods";

const returnIsNotAgreedCommand = {
  id: "returnIsNotAgreed",
  title: "Возврат не согласован",
  availableStates: [21, 25]
};

const changePriceAndCountCommand = {
  id: "changePriceAndCount",
  title: "Изменить цену и количество",
  availableStates: [21, 22, 25, 27]
};

const divideCommand = {
  id: "divide",
  title: "Разделить позиции",
  availableStates: [21, 22, 25, 27],
  customFilter: position => position.count > 1
};

export default {
  returnIsNotAgreedCommand,
  changePriceAndCountCommand,
  divideCommand
};

export const filterPurchaseReturnPositionsCommands = (commands, positions) => {
  const positionPositions = positions.map(p => p.position);
  return positions.length
    ? commands.filter(byAllPositionsStates(positionPositions))
    : [];
};
