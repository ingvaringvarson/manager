import { byAllPositionsStates } from "./../helpers/filterMethods";
import { pm_purchase_order_reject } from "../constants/permissions";

const changeComingDateCommand = {
  id: "changeComingDate",
  title: "Изменить дату прихода позиции",
  availableStates: [1, 19]
};

// ToDo:
// * добавить работу с несколькими заказами постащику
const rejectByProductCommand = {
  id: "rejectByProduct",
  title: "Отказаться от товара",
  forList: true,
  availableStates: [1, 19],
  permissionsMarker: (permissions, positions) => {
    if (positions.every(p => p.good_state === 1)) {
      return true;
    }

    if (positions.some(p => p.good_state === 19)) {
      return permissions.includes(pm_purchase_order_reject);
    }

    return false;
  }
};

const divideCommand = {
  id: "divide",
  title: "Разделить позицию",
  forList: true,
  availableStates: [1, 19, 20],
  customFilter: position => position.count > 1
};

const changePriceAndCountCommand = {
  id: "changePriceAndCount",
  title: "Изменить цену и количество",
  availableStates: [1, 19, 20],
  forList: true
};

const createPurchaseReturnCommand = {
  id: "createPurchaseReturn",
  title: "Создать возврат поставщику",
  forList: true,
  availableStates: [3, 20]
};

export default {
  changeComingDateCommand,
  rejectByProductCommand,
  divideCommand,
  changePriceAndCountCommand,
  createPurchaseReturnCommand
};

export const filterPurchaseOrderPositionsCommands = (commands, positions) =>
  positions.length ? commands.filter(byAllPositionsStates(positions)) : [];
