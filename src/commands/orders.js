import { pm_discount_create } from "../constants/permissions";
import { byAllEnitiesStates } from "./../helpers/filterMethods";

const hasAnyPositionInGivenState = order =>
  order.positions.some(p => p.good_state === 7);

const hasOrderPositiveSumPayment = order => order.sum_payment > 0;
const hasOrderZeroSumPayment = order => order.sum_payment === 0;

const inWorkCommand = {
  id: "inWork",
  title: "В работу",
  availableStates: [12]
};

const createPaymentCommand = {
  id: "createPayment",
  title: "Оплатить",
  availableStates: [1, 2, 3, 4, 5, 6, 7],
  customFilter: order => hasOrderPositiveSumPayment(order)
};

const prepareToDeliveryCommand = {
  id: "prepareToDelivery",
  title: "Подготовить к выдаче",
  availableStates: [1, 2, 3, 4, 5, 6],
  customFilter: order => order.state !== 4 || hasOrderPositiveSumPayment(order)
};

const deliverGoodCommand = {
  id: "deliverGood",
  title: "Выдать товар",
  availableStates: [4],
  customFilter: order => hasOrderZeroSumPayment(order)
};

const createReturnCommand = {
  id: "createReturn",
  title: "Создать возврат",
  availableStates: [7],
  customFilter: order =>
    hasOrderPositiveSumPayment(order) || hasAnyPositionInGivenState(order)
};

const printDocumentsCommand = {
  id: "printDocuments",
  title: "Печать документов",
  blockedStates: [8]
};

const returnPrepaymentCommand = {
  id: "returnPrepayment",
  title: "Вернуть предоплату",
  blockedStates: [7, 8, 12],
  availableStatesForPositions: [10, 11, 12, 13, 14, 18],
  customFilter(order) {
    const posAvailableStates = this.availableStatesForPositions;

    return (
      order.sum_payment < order.sum_total &&
      order.positions.some(p =>
        posAvailableStates.some(s => p.good_state === s)
      )
    );
  },
  positionsFilter(positions) {
    const posAvailableStates = this.availableStatesForPositions;

    return positions.filter(p =>
      posAvailableStates.some(s => p.good_state === s)
    );
  }
};

const cancellationCommand = {
  id: "cancellation",
  title: "Аннулировать счет",
  availableStates: [1, 2, 3, 4, 5, 6]
};

const createBillCommand = {
  id: "createBill",
  title: "Создать счет",
  forList: true,
  blockedStates: [8, 9, 10, 11, 12, 13]
};

const discountCommand = {
  id: "discount",
  title: "Добавить скидку",
  forList: true,
  blockedStates: [7, 8, 9, 10, 11, 13],
  permissionsMarker: [pm_discount_create]
};

const extendReserveCommand = {
  id: "extendReserve",
  title: "Продлить резерв",
  blockedStates: [7, 8, 9, 10, 11, 13]
};

const rejectCommand = {
  id: "reject",
  title: "Отказаться от заказа",
  forList: true,
  blockedStates: [7, 8, 9, 10, 11, 13]
};

const changeClientCommand = {
  id: "changeClient",
  title: "Изменить клиента в заказе",
  blockedStates: [7]
};

const uploadMaxoptraCommand = {
  id: "uploadMaxoptra",
  title: "Выгрузить в Maxoptra",
  forList: true,
  blockedStates: [7, 8, 9, 10, 11, 13],
  customFilter: order => !!order.deliverylaf24
};

export default {
  inWorkCommand,
  createPaymentCommand,
  prepareToDeliveryCommand,
  deliverGoodCommand,
  createReturnCommand,
  printDocumentsCommand,
  returnPrepaymentCommand,
  cancellationCommand,
  createBillCommand,
  discountCommand,
  extendReserveCommand,
  rejectCommand,
  changeClientCommand,
  uploadMaxoptraCommand
};

export const filterOrdersCommands = (commands, orders) => {
  const statePropertyName = "state";
  const byAllOrdersStates = byAllEnitiesStates(orders, statePropertyName);
  return orders.length ? commands.filter(byAllOrdersStates) : [];
};

export const getMainCommandByOrder = order => {
  const orderPositiveSumPayment = hasOrderPositiveSumPayment(order);
  const orderZeroSumPayment = hasOrderZeroSumPayment(order);
  const anyPositionInGivenState = hasAnyPositionInGivenState(order);

  switch (true) {
    case order.state === 12:
      return inWorkCommand.id;
    case [1, 2, 3, 4, 5, 6].includes(order.state) && orderPositiveSumPayment:
      return createPaymentCommand.id;
    case [1, 2, 3, 5, 6].includes(order.state) && orderZeroSumPayment:
      return prepareToDeliveryCommand.id;
    case order.state === 4 && orderZeroSumPayment:
      return deliverGoodCommand.id;
    case order.state === 7 && orderPositiveSumPayment:
      return createPaymentCommand.id;
    case order.state === 7 && orderZeroSumPayment && anyPositionInGivenState:
      return createReturnCommand.id;
  }

  return null;
};
