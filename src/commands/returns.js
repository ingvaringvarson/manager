import { byAllEnitiesStates } from "./../helpers/filterMethods";

const acceptReturnCommand = {
  id: "acceptReturn",
  title: "Принять возврат",
  availableStates: [1]
};

const returnPaymentCommand = {
  id: "returnPayment",
  title: "Вернуть оплату",
  blockedStates: [1],
  customFilter: returnEntity => returnEntity.sum_payment > 0
};

const takeProductCommand = {
  id: "takeProduct",
  title: "Принять товар",
  blockedStates: [1]
};

const printDocumentsCommand = {
  id: "printDocuments",
  title: "Печать документов"
};

const createReturnCommand = {
  id: "returnReject",
  title: "Отказ в возврате",
  availableStates: [1]
};

export default {
  acceptReturnCommand,
  returnPaymentCommand,
  takeProductCommand,
  printDocumentsCommand,
  createReturnCommand
};

export const filterReturnsCommands = (commands, returnEntities) => {
  const statePropertyName = "state_return";
  const returns = returnEntities.map(pr => pr.return);
  const byAllReturnsStates = byAllEnitiesStates(returns, statePropertyName);
  return returns.length ? commands.filter(byAllReturnsStates) : [];
};

export const getMainCommandByReturn = returnEntity => {
  switch (returnEntity.state_return) {
    case 1:
      return acceptReturnCommand.id;
    case 2:
    case 3:
    case 4:
      return returnEntity.sum_payment > 0
        ? returnPaymentCommand.id
        : takeProductCommand.id;
  }

  return null;
};
