import { byAllPositionsStates } from "./../helpers/filterMethods";

const divideCommand = {
  id: "divide",
  title: "Разделить позицию",
  blockedStates: [22, 30],
  customFilter: position => position.count > 1
};

const rejectInReturnCommand = {
  id: "rejectInReturn",
  title: "Отказ в возврате",
  availableStates: [25]
};

export default {
  divideCommand,
  rejectInReturnCommand
};

export const filterReturnPositionsCommands = (commands, positions) => {
  const positionPositions = positions.map(p => p.position);
  return positions.length
    ? commands.filter(byAllPositionsStates(positionPositions))
    : [];
};
