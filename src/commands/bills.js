import { byAllEnitiesStates } from "./../helpers/filterMethods";

const cancellationCommand = {
  id: "cancellation",
  title: "Аннулировать счет",
  blockedStates: [4]
};

const printDocumentsCommand = {
  id: "printDocuments",
  title: "Печать документов",
  blockedStates: [4]
};

const returnPaymentCommand = {
  id: "returnPaymentCommand",
  title: "Вернуть оплату",
  customFilter: bill => bill.receipt_sum > 0 && bill.type === 5
};

const acceptPaymentCommand = {
  id: "acceptPaymentCommand",
  title: "Принять оплату"
};

export default {
  cancellationCommand,
  printDocumentsCommand,
  returnPaymentCommand,
  acceptPaymentCommand
};

export const filterBillsCommands = (commands, bills) => {
  const statePropertyName = "state";
  const byAllBillsStates = byAllEnitiesStates(bills, statePropertyName);
  return bills.length ? commands.filter(byAllBillsStates) : [];
};
