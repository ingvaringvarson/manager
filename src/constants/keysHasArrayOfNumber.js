const keysHasTypeArray = {
  orders: new Set(["state", "good_state", "service_state", "delivery_type"])
};

export default keysHasTypeArray;
