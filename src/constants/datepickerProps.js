export default {
  minDate: new Date("1919-01-01T00:00:00"),
  maxDate: new Date("2999-12-31T00:00:00")
};
