export const pm_payments_create_without_check =
  "pm_payments_create_without_check";
export const pm_payments_create_return_internet_acquiring =
  "pm_payments_create_return_internet_acquiring";
export const pm_autorization_add_warehouse = "pm_autorization_add_warehouse";
export const pm_registration_supplier = "pm_registration_supplier";
export const pm_discount_create = "pm_discount_create";
export const pm_payment_update = "pm_payment_update";
export const pm_purchase_order_reject = "pm_purchase_order_reject";
export const pm_orders_update_reserve_time = "pm_orders_update_reserve_time";
export const pm_agents_update_price_category =
  "pm_agents_update_price_category";
