// http://wiki.laf24.ru/display/1CAPI/AgentRole
export default {
  supplier: 1, // Поставщик
  ownFirm: 2, // Собственная организация
  transportCompany: 3, // Транспортная компания
  client: 4, // Клиент
  employee: 5 // Сотрудник
};
