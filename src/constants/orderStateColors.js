export const colors = [
  { id: 1, color: "#ccc500" },
  { id: 3, color: "#ff9320" },
  { id: 12, color: "#3B3B3B" },
  { id: 2, color: "#ff9320" },
  { id: 4, color: "#ff9320" },
  { id: 6, color: "#ff9320" },
  { id: 5, color: "#ff9320" },
  { id: 7, color: "#348e3d" },
  { id: 10, color: "#d24a45" },
  { id: 8, color: "#d24a45" },
  { id: 9, color: "#d24a45" },
  { id: 13, color: "#d24a45" }
];
