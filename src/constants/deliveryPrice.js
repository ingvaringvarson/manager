export const totalPriceByFreeDelivery = 3000;

const delivery_min_3000 = {
  id: "delivery_min_3000",
  name: "Доставка до 3000р",
  company: "LAF24",
  price: 250
};

const delivery_max_3000 = {
  id: "delivery_max_3000",
  name: "Доставка более 3000р",
  company: "LAF24",
  price: 0
};

const delivery_b2b = {
  id: "delivery_b2b",
  name: "Доставка B2B",
  company: "LAF24",
  price: 0
};

const delivery_free = {
  id: "delivery_free",
  name: "Доставка Бесплатная",
  company: "LAF24",
  price: 0
};

const delivery_sdek = {
  id: "delivery_sdek",
  name: "Доставка СДЕК",
  company: "LAF24",
  price: 0
};

const deliveryPrice = {
  info: {
    delivery_min_3000,
    delivery_max_3000,
    delivery_b2b,
    delivery_free,
    delivery_sdek
  },
  list: [delivery_min_3000, delivery_max_3000, delivery_b2b, delivery_free]
};

export default deliveryPrice;
