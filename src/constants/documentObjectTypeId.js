// http://wiki.laf24.ru/display/1CAPI/ObjectType
export default {
  agents: 1, // агенты
  contracts: 2, // контракты
  orders: 5, // заказы клиента
  bills: 6, // счета
  purchase_returns: 7, // возврат заказа поставщика
  tasks: 13, // задачи
  purchase_orders: 14, // заказ поставщика
  returns: 16 // возврат заказа
};
