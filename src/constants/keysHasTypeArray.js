const keysHasTypeArray = {
  agents: new Set([
    "id",
    "roles",
    "segments",
    "tags",
    "cars",
    "contacts",
    "car_keys",
    "pricelist_id",
    "pricelist_code",
    "contract_pricelist_id",
    "day_of_payment"
  ]),
  balances: new Set(["account_type"]),
  cars: new Set(["key"]),
  orders: new Set([
    "state",
    "good_state",
    "service_state",
    "delivery_type",
    "positions",
    "product_units_id",
    "product_units_ean_13"
  ]),
  bills: new Set(["state", "order_position_id", "payments_id", "return_id"]),
  payments: new Set([
    "payment_type",
    "payment_method",
    "payment_state",
    "payment_place",
    "return_id"
  ]),
  warehouse: new Set([
    "type",
    "city",
    "acquiring_terminal",
    "cashbox_id",
    "cashbox_name",
    "metro_id",
    "metro_name",
    "network_infrastructure_value",
    "price_list_code",
    "price_list_id"
  ]),
  files: new Set(["id"]),
  purchaseOrders: new Set([
    "state",
    "good_state",
    "service_state",
    "contacts",
    "delivery_type",
    "positions",
    "product_units_id",
    "product_units_ean_13",
    "pricelist_code"
  ]),
  tasks: new Set(["type", "state"]),
  returns: new Set(["reason", "state", "purpose", "good_state"]),
  productsInStock: new Set([
    //"product", особая обработка в mapRequestParamsToParamsByApiSaga
    "ean_13",
    "product_unit_id",
    "warehouse_id",
    "firm_id",
    "state",
    "pricelist_id"
  ]),
  purchaseReturns: new Set([
    "state",
    "good_state",
    "service_state",
    "contacts",
    "positions",
    "product_units_id",
    "product_units_ean_13",
    "purchase_order_id",
    "reason"
  ]),
  pricelists: new Set([
    "pricelist_id",
    "firm_id",
    "supplier_firm_id",
    "contract_id",
    "day_of_payment",
    "cities",
    "order_dispatch_method",
    "warehouse_id"
  ])
};

export default keysHasTypeArray;
