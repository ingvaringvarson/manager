import { combineReducers } from "redux";
import listForEntity from "./listForEntity";

export default combineReducers({
  listForEntity
});
