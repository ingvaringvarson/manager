import { all, call, put } from "redux-saga/effects";
import {
  FETCH_PRICELISTS_FOR_ENTITY_REQUEST,
  FETCH_PRICELISTS_FOR_ENTITY_SAFE_REQUEST,
  FETCH_PRICELISTS_FOR_ENTITY_SUCCESS,
  FETCH_PRICELISTS_FOR_ENTITY_ERROR
} from "./actions";
import { fetchApiSaga, watchingTwinActionsSaga } from "../logger";
import { getEntities } from "../../api/requestByTypeEntities";
import { mapRequestParamsToParamsByApiSaga } from "../../helpers/mapMethods";
import { moduleName } from "./config";

export function* fetchPricelistsSafeSaga(action) {
  return yield fetchPricelistsSaga(action, true);
}

export function* fetchPricelistsSaga(action, isSafe = false) {
  const paramsByApi = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    moduleName
  );

  const params = {
    params: JSON.stringify(paramsByApi)
  };

  const paramsRequest = getEntities(moduleName, params);
  const { response, error } = yield call(fetchApiSaga, paramsRequest, isSafe);
  const pricelists =
    response && response.payload && response.payload.pricelists;

  if (pricelists && pricelists.length > 0) {
    yield put({
      type: FETCH_PRICELISTS_FOR_ENTITY_SUCCESS,
      payload: {
        entities: pricelists,
        maxPages: response.payload.pages_num,
        ...action.payload
      }
    });
  } else if (error && !isSafe) {
    yield put({
      type: FETCH_PRICELISTS_FOR_ENTITY_ERROR,
      payload: {
        error,
        ...action.payload
      }
    });
  }
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_PRICELISTS_FOR_ENTITY_REQUEST]: [fetchPricelistsSaga, moduleName],
      [FETCH_PRICELISTS_FOR_ENTITY_SAFE_REQUEST]: [
        fetchPricelistsSafeSaga,
        moduleName
      ]
    })
  ]);
}
