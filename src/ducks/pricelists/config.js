import { appName } from "../../config";

export const moduleName = "pricelists";
export const prefix = `${appName}/${moduleName}`;
