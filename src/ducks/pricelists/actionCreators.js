import { fetchEntitiesForTypeEntity } from "../../helpers/actionCreators";
import { FETCH_PRICELISTS_FOR_ENTITY_SAFE_REQUEST } from "./actions";

export const fetchPriceLists = fetchEntitiesForTypeEntity(
  FETCH_PRICELISTS_FOR_ENTITY_SAFE_REQUEST
);
