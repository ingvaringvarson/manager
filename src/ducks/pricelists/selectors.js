import { createSelector } from "reselect/lib/index";

import { moduleName } from "./config";
import {
  listStateSelector,
  listForEntityStateSelector
} from "../../helpers/selectors";

export const pricelistsSelector = listStateSelector(moduleName);

const propsIdEntitySelector = (_, props) => props.idEntity;

export const pricelistOptionsSelector = createSelector(
  listForEntityStateSelector(moduleName),
  propsIdEntitySelector,
  (listForEntity, idEntity) => {
    const pricelistsByType = listForEntity.pricelists || {};
    const pricelistsById = pricelistsByType[idEntity] || {};
    const pricelists = pricelistsById.entities || [];

    if (pricelists.length !== 1) {
      return pricelists;
    }

    const pricelist = pricelists[0];

    return [
      {
        id: pricelist.id,
        name: `${pricelist.code} - ${pricelist.name}`
      }
    ];
  }
);
