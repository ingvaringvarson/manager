import { moduleName } from "../config";
import {
  entityListSelector,
  maxPagesSelector as maxPagesSelectorHelper
} from "../../../helpers/selectors";
import { createSelector } from "reselect/lib/index";
import { queryRouterSelector } from "../../../redux/selectors/router";

export const entitiesPaymentSelector = entityListSelector(moduleName);
export const paymentListSelector = entityListSelector(moduleName);
export const maxPagesSelector = maxPagesSelectorHelper(moduleName);

export const requestParamsSelector = createSelector(
  queryRouterSelector,
  query => ({
    page_size: 10,
    page: 1,
    sort_by: "object_date_create desc",
    ...query,
    payment_method: query.payment_method ? +query.payment_method : 1
  })
);
