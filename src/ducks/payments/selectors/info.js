import { moduleName } from "../config";
import { entityInfoSelector } from "../../../helpers/selectors";
import { entityIdForRouteSelector } from "../../../redux/selectors/router";
import { createSelector } from "reselect/lib/index";

export const paymentInfoSelector = entityInfoSelector(moduleName);

export const requestParamsForPaymentPageSelector = createSelector(
  entityIdForRouteSelector,
  paymentId => {
    return {
      id: paymentId,
      page: 1,
      page_size: 1
    };
  }
);
