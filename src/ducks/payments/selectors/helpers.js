import { createSelector } from "reselect";
import { queryRouterSelector } from "../../../redux/selectors/router";
import { clientIdSelector } from "../../../ducks/clients";
import {
  typeEntitySelector,
  idEntitySelector
} from "../../../helpers/selectors";

export const requestParamsByClientPageSelector = createSelector(
  queryRouterSelector,
  clientIdSelector,
  (query, id) => ({
    page_size: 10,
    page: 1,
    agent_id: id,
    payment_method: 1,
    ...query
  })
);

export const paymentMethodIdSelector = (state, props) => {
  return +requestParamsByClientPageSelector(state, props).payment_method;
};

export const requestParamsForPaymentsTabPageSelector = createSelector(
  queryRouterSelector,
  typeEntitySelector,
  idEntitySelector,
  ({ payment_method, ...query }, typeEntity, idEntity) => {
    return {
      page: 1,
      page_size: 10,
      ...query,
      payment_method: payment_method ? +payment_method : 1,
      [typeEntity]: idEntity
    };
  }
);
