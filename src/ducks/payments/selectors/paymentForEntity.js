import { createSelector } from "reselect";
import { moduleName } from "../config";
import {
  entitiesForTypeEntitySelector,
  typeEntitySelector as propsTypeEntitySelector,
  idEntitySelector
} from "../../../helpers/selectors";
import {
  entityIdForRouteSelector,
  queryRouterSelector
} from "../../../redux/selectors/router";

// helpers

export const paymentsForEntitySelector = entitiesForTypeEntitySelector(
  moduleName
);

export const requestParamsForPaymentsForBillSelector = createSelector(
  (state, props) => props.id,
  id => ({ bill_id: id })
);

export const requestParamsForOrderPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  ({ payment_method, ...query }, orderId) => {
    return {
      order_id: orderId,
      page: 1,
      page_size: 10,
      sort_by: "object_date_create desc",
      ...query,
      payment_method: payment_method ? +payment_method : 1
    };
  }
);

export const requestParamsForBillPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  ({ payment_method, ...query }, billId) => {
    return {
      bill_id: billId,
      page: 1,
      page_size: 10,
      sort_by: "object_date_create desc",
      ...query,
      payment_method: payment_method ? +payment_method : 1
    };
  }
);

export const requestParamsForClientPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  ({ payment_method, ...query }, clientId) => {
    return {
      agent_id: clientId,
      page: 1,
      pageSize: 10,
      sort_by: "object_date_create desc",
      ...query,
      payment_method: payment_method ? +payment_method : 1
    };
  }
);

export const requestParamsPaymentsForEntitySelector = createSelector(
  queryRouterSelector,
  propsTypeEntitySelector,
  idEntitySelector,
  (query, typeEntity, idEntity) => {
    const { payment_method } = query;
    return {
      page: 1,
      page_size: 10,
      ...query,
      payment_method: payment_method ? +payment_method : 1,
      [typeEntity]: idEntity
    };
  }
);
