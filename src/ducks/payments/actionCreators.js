import {
  FETCH_PAYMENTS_REQUEST,
  FETCH_PAYMENTS_FOR_ENTITY_REQUEST,
  FETCH_PAYMENT_REQUEST,
  UPDATE_PAYMENT_REQUEST,
  RETURN_PAYMENT_COMMAND,
  CREATE_PAYMENT_REQUEST,
  CREATE_PAYMENT_COMMAND
} from "./actions";
import {
  fetchEntities,
  fetchEntitiesForTypeEntity,
  fetchEntity,
  updateEntity,
  createEntity
} from "../../helpers/actionCreators";

export const fetchPayments = fetchEntities(FETCH_PAYMENTS_REQUEST);

export const fetchPayment = fetchEntity(FETCH_PAYMENT_REQUEST);

export const fetchPaymentsForEntity = fetchEntitiesForTypeEntity(
  FETCH_PAYMENTS_FOR_ENTITY_REQUEST
);

export const updatePayment = updateEntity(UPDATE_PAYMENT_REQUEST);

export const createPayment = createEntity(CREATE_PAYMENT_REQUEST);

export const returnPaymentCommand = (values, client) => {
  return {
    type: RETURN_PAYMENT_COMMAND,
    payload: {
      values,
      client
    }
  };
};

export const createPaymentCommand = (values, client, order, bills) => {
  return {
    type: CREATE_PAYMENT_COMMAND,
    payload: {
      values,
      client,
      order,
      bills
    }
  };
};
