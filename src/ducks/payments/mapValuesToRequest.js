import { getValueInDepth } from "../../utils";

const paymentTypes = {
  1: 1,
  2: 0,
  3: 2,
  4: 3
};

const paymentMethods = {
  1: 0,
  2: 1
};

export function mapFormValuesByCreatePaymentRequest(values, bill) {
  if (values.bill_id) return values;

  return {
    ...values,
    bill_id: bill.id,
    sum: bill.bill_sum,
    receipt: bill.bill_sum,
    firm_id: bill.firm_id,
    firm_name: bill.firm_name
  };
}

export function mapFormValuesByReturnPaymentKkmRequest(values) {
  return {
    orderId: values.order_id,
    billId: values.bill_id,
    receiptAddress: values.receipt_address,
    sum: +values.sum,
    paymentType: paymentTypes[values.payment_type],
    paymentMethod: paymentMethods[values.payment_method]
  };
}

export function mapFormValuesByPaymentRequest(values, warehouseInfo) {
  const params = {
    bills: [values.bill_id],
    sum: +values.sum,
    payment_type: +values.payment_type,
    payment_method: +values.payment_method,
    payment_state: values.payment_state,
    payment_place: values.payment_place,
    agent_id: values.agent_id,
    firm_id: values.firm_id,
    contract_id: values.contract_id
  };

  if (warehouseInfo) {
    const cashboxId = getValueInDepth(warehouseInfo, ["cashbox", 0, "id"]);
    const acquiringTerminalId = getValueInDepth(warehouseInfo, [
      "acquiring_terminal",
      0
    ]);

    if (+values.payment_method === 1 && cashboxId) {
      params.cashbox_id = cashboxId;
    } else if (+values.payment_method === 2 && acquiringTerminalId) {
      params.acquiring_terminal_id = acquiringTerminalId;
    }

    if (values.internet_acquiring) {
      params.payment_place = 1;
    }

    params.store_id = warehouseInfo.id;
  }

  return params;
}

export function mapSingleBillIdToValues(values, bill_id) {
  return {
    ...values,
    bill_id
  };
}
