import { appName } from "../../config";

export const moduleName = "payments";
export const commandsLoaderName = "payments_commands";
export const prefix = `${appName}/${moduleName}`;
