import { all, takeEvery, takeLatest } from "redux-saga/effects";
import {
  FETCH_PAYMENTS_FOR_ENTITY_REQUEST,
  FETCH_PAYMENTS_REQUEST,
  FETCH_PAYMENT_SUCCESS,
  FETCH_PAYMENT_REQUEST,
  UPDATE_PAYMENT_REQUEST,
  UPDATE_PAYMENT_SUCCESS,
  RETURN_PAYMENT_COMMAND,
  CREATE_PAYMENT_REQUEST,
  CREATE_PAYMENT_COMMAND
} from "../actions";

import {
  fetchPaymentsForEntitySaga,
  fetchPaymentsSaga,
  fetchPaymentSuccessSaga,
  fetchPaymentSaga,
  updatePaymentSaga,
  returnPaymentCommandSaga,
  createPaymentSaga,
  createPaymentCommandSaga
} from "./payments";
import { loggingRequestSaga, watchingTwinActionsSaga } from "../../logger";
import { moduleName, commandsLoaderName } from "../config";
import { responseToUpdateEntitySaga } from "../../../helpers/sagas";

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_PAYMENT_REQUEST]: [fetchPaymentSaga, moduleName],
      [FETCH_PAYMENTS_REQUEST]: [fetchPaymentsSaga, moduleName],
      [FETCH_PAYMENTS_FOR_ENTITY_REQUEST]: [
        fetchPaymentsForEntitySaga,
        moduleName
      ]
    }),
    takeEvery(FETCH_PAYMENT_SUCCESS, fetchPaymentSuccessSaga),
    takeEvery(
      UPDATE_PAYMENT_REQUEST,
      loggingRequestSaga,
      updatePaymentSaga,
      moduleName
    ),
    takeEvery(
      CREATE_PAYMENT_REQUEST,
      loggingRequestSaga,
      createPaymentSaga,
      moduleName
    ),
    takeLatest(UPDATE_PAYMENT_SUCCESS, responseToUpdateEntitySaga),
    takeEvery(
      RETURN_PAYMENT_COMMAND,
      loggingRequestSaga,
      returnPaymentCommandSaga,
      commandsLoaderName
    ),
    takeEvery(
      CREATE_PAYMENT_COMMAND,
      loggingRequestSaga,
      createPaymentCommandSaga,
      commandsLoaderName
    )
  ]);
}
