import { userFirmAndWarehouseSelector } from "./../../user/selectors";
import { call, put, select, all } from "redux-saga/effects";
import {
  FETCH_PAYMENT_SUCCESS,
  FETCH_PAYMENTS_SUCCESS,
  FETCH_PAYMENTS_ERROR,
  FETCH_PAYMENTS_FOR_ENTITY_SUCCESS,
  FETCH_PAYMENTS_FOR_ENTITY_ERROR,
  UPDATE_PAYMENT_SUCCESS,
  CREATE_PAYMENT_SUCCESS
} from "../actions";
import {
  fetchEntitiesForTypeEntitySaga,
  fetchEntitiesSaga,
  fetchEntitySaga,
  postEntitySaga,
  putEntitySaga
} from "../../../helpers/sagas";
import { mapRequestParamsToParamsByApiSaga } from "../../../helpers/mapMethods";
import { fetchClient, fetchClientSaga } from "../../clients";
import { fetchBillsForEntity } from "../../bills";
import { mapValuesToEntity } from "../../../helpers/dataMapping";
import { generateId, getValueInDepth } from "../../../utils";
import { addNotification } from "../../logger";
import {
  mapSingleBillIdToValues,
  mapFormValuesByPaymentRequest,
  mapFormValuesByReturnPaymentKkmRequest
} from "../mapValuesToRequest";
import {
  createReturnPaymentScenario,
  fetchKkmCommandSaga,
  createReturnPaymentWithoutCheck,
  createReturnPaymentWithCheck
} from "../../cash";
import { createPayment } from "../actionCreators";
import { getUrl } from "../../../helpers/urls";
import { push } from "connected-react-router";
import { fetchCurrentWarehouseInfoSaga } from "../../warehouses";
import {
  createPaymentSagaHelper,
  firmCheckSagaHelper,
  getNotificationMessageFirmCheckByCreatePayment
} from "../helpers";
import { getActionPayload } from "../../../helpers/mapValuesToRequest";

export function* fetchPaymentSaga(action) {
  const actions = {
    success: FETCH_PAYMENT_SUCCESS
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "payments"
  );

  yield call(
    fetchEntitySaga,
    {
      id: action.payload.id,
      params
    },
    actions,
    "payments",
    "payments"
  );
}

export function* fetchPaymentSuccessSaga(action) {
  if (!action.payload.forInfoPage) return;

  const idEntity = action.payload.id;
  const typeEntity = "payments";
  const params = { page: 1, page_size: 1 };
  const { agent_id, payment_state, id } = action.payload.entity;

  if (agent_id) {
    yield put(
      fetchClient(agent_id, {
        ...params,
        id: agent_id
      })
    );
  }
  if (payment_state !== 3) {
    yield put(
      fetchBillsForEntity(typeEntity, idEntity, {
        ...params,
        payments_id: [id]
      })
    );
  }
}

export function* fetchPaymentsSaga(action) {
  const actions = {
    success: FETCH_PAYMENTS_SUCCESS,
    error: FETCH_PAYMENTS_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "payments"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  yield call(fetchEntitiesSaga, payloadForApi, actions, "payments", "payments");
}

export function* fetchPaymentsForEntitySaga(action) {
  const actions = {
    success: FETCH_PAYMENTS_FOR_ENTITY_SUCCESS,
    error: FETCH_PAYMENTS_FOR_ENTITY_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "payments"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  yield call(
    fetchEntitiesForTypeEntitySaga,
    payloadForApi,
    actions,
    "payments",
    "payments"
  );
}

export function* updatePaymentSaga(action) {
  const actions = {
    success: UPDATE_PAYMENT_SUCCESS
  };

  const payloadForApi = {
    ...action.payload,
    body: mapValuesToEntity(action.payload.entity, action.payload.values)
  };

  yield call(putEntitySaga, payloadForApi, actions, "payments");
}

export function* createPaymentSaga(action) {
  const actions = {
    success: CREATE_PAYMENT_SUCCESS
  };

  const { mapSaga, values, ...props } = action.payload;

  const body = yield call(mapSaga ? mapSaga : mapValuesToEntity, values);

  const payloadForApi = {
    ...props,
    body
  };

  return yield call(postEntitySaga, payloadForApi, actions, "payments");
}

export function* returnPaymentCommandSaga(action) {
  const { values } = action.payload;
  const { userFirmId, userWarehouseId } = yield select(
    userFirmAndWarehouseSelector
  );

  if (!values.firm_id) {
    return yield put(
      addNotification({
        message: "Неизвестная ошибка"
      })
    );
  }

  if (values.firm_id !== userFirmId) {
    const { response } = yield call(fetchClientSaga, {
      payload: { params: { id: userFirmId } }
    });
    const userFirmName = getValueInDepth(response, [
      "payload",
      "agents",
      0,
      "company",
      "company_name_short"
    ]);

    return yield put(
      addNotification({
        message: `Документ возврата, для которого вы выдаете платеж, создан на организацию ${values.firm_name ||
          "Название неизвестно!"}. Вы создаете платеж от организации ${userFirmName ||
          "Название неизвестно!"}. Создайте новый возврат на вашу организацию или попросите клиента получить платеж в другом магазине.`
      })
    );
  }

  const currentWarehouseInfoParams = getActionPayload({
    userFirmId,
    userWarehouseId,
    paramsByWarehouseRequest: values.internet_acquiring
      ? { type: [6] }
      : { id: userWarehouseId }
  });
  const warehouseInfo = yield call(
    fetchCurrentWarehouseInfoSaga,
    currentWarehouseInfoParams
  );

  if (!warehouseInfo.id) {
    return warehouseInfo;
  }

  const paramsByKkmCommandRequest = mapFormValuesByReturnPaymentKkmRequest(
    values
  );

  switch (true) {
    case values.payment_method === "2" && values.internet_acquiring: {
      const resultScenario = yield call(
        fetchKkmCommandSaga,
        createReturnPaymentWithCheck(paramsByKkmCommandRequest)
      );

      if (
        getValueInDepth(resultScenario, ["response", "payload", "orderCode"])
      ) {
        break;
      }

      return resultScenario;
    }
    case !values.without_check: {
      const resultScenario = yield call(
        fetchKkmCommandSaga,
        createReturnPaymentScenario(paramsByKkmCommandRequest, generateId())
      );

      if (
        getValueInDepth(resultScenario, [
          "response",
          "payload",
          "completionStatus"
        ]) === 1
      ) {
        break;
      }

      return resultScenario;
    }
    case !!values.without_check: {
      const resultScenario = yield call(
        fetchKkmCommandSaga,
        createReturnPaymentWithoutCheck(paramsByKkmCommandRequest)
      );

      if (
        getValueInDepth(resultScenario, ["response", "payload", "orderCode"])
      ) {
        break;
      }

      return resultScenario;
    }
    default:
      return yield put(
        addNotification({
          message: "Неизвестная ошибка"
        })
      );
  }

  const params = mapFormValuesByPaymentRequest(values, warehouseInfo);

  const paymentResult = yield call(createPaymentSaga, createPayment(params));

  const payment = getValueInDepth(paymentResult, ["response", "payload"]);

  if (payment && payment.id) {
    yield put(push(getUrl("payments", { id: payment.id })));
  }

  return paymentResult;
}

export function* createPaymentCommandSaga(action) {
  yield put(
    addNotification({
      message: "Запрос на выполнение операции отправлен"
    })
  );
  const { values, order, bills } = action.payload;
  const { userFirmId, userWarehouseId } = yield select(
    userFirmAndWarehouseSelector
  );

  const firmCheckResult = yield call(
    firmCheckSagaHelper,
    values,
    userFirmId,
    getNotificationMessageFirmCheckByCreatePayment
  );

  if (!firmCheckResult.firmChecked) {
    return firmCheckResult;
  }

  const currentWarehouseInfoParams = getActionPayload({
    userFirmId,
    userWarehouseId,
    paramsByWarehouseRequest: {
      id: userWarehouseId
    }
  });
  const warehouseInfo = yield call(
    fetchCurrentWarehouseInfoSaga,
    currentWarehouseInfoParams
  );
  if (!warehouseInfo.id) {
    return warehouseInfo;
  }

  const singleBillId =
    values.bill_id ||
    (Array.isArray(bills) && bills.length === 1 && bills[0].id);

  if (singleBillId) {
    const valuesWithSingleBillId = mapSingleBillIdToValues(
      values,
      singleBillId
    );

    return yield call(
      createPaymentSagaHelper,
      valuesWithSingleBillId,
      warehouseInfo,
      null,
      order
    );
  } else if (Array.isArray(bills)) {
    if (!bills.length) {
      return yield put(
        addNotification({
          message: "Нет счетов связанных с заказом!"
        })
      );
    }

    return yield all(
      bills.map(b =>
        call(createPaymentSagaHelper, values, warehouseInfo, b, order)
      )
    );
  } else {
    return yield put(
      addNotification({
        message: "Неизвестная ошибка"
      })
    );
  }
}
