import React from "react";
import { call, put } from "redux-saga/effects";
import { addNotification } from "../logger";
import { fetchClientSaga } from "../clients";
import { generateId, getValueInDepth } from "../../utils";
import { createPaymentSaga } from "./sagas/payments";
import { getUrl } from "../../helpers/urls";
import { createPayment } from "./actionCreators";
import { createPaymentScenario, fetchKkmCommandSaga } from "../cash";
import {
  mapFormValuesByCreatePaymentRequest,
  mapFormValuesByPaymentRequest,
  mapFormValuesByReturnPaymentKkmRequest
} from "./mapValuesToRequest";
import { Link } from "react-router-dom";

export function getNotificationMessageFirmCheckByCreatePayment(
  oldFirmName,
  newFirmName
) {
  return `Заказ, для которого вы принимает оплату, создан на организацию ${oldFirmName ||
    "Название неизвестно!"}. Вы создаете платеж от организации ${newFirmName ||
    "Название неизвестно!"}. Создайте новый заказ на вашу организацию или попросите клиента оплатить в другом магазине или на сайте.`;
}

export function* firmCheckSagaHelper(
  values,
  userFirmId,
  getNotificationMessage
) {
  const result = {
    firmChecked: false,
    action: null
  };

  if (!values.firm_id) {
    result.action = yield put(
      addNotification({
        message: "Неизвестная ошибка"
      })
    );
  } else if (values.firm_id !== userFirmId) {
    const { response } = yield call(fetchClientSaga, {
      payload: { params: { id: userFirmId } }
    });
    const userFirmName = getValueInDepth(response, [
      "payload",
      "agents",
      0,
      "company",
      "company_name_short"
    ]);

    result.action = yield put(
      addNotification({
        message: getNotificationMessage(values.firm_name, userFirmName)
      })
    );
  } else {
    result.firmChecked = true;
  }

  return result;
}

export function* createPaymentSagaHelper(
  initValues,
  warehouseInfo,
  bill,
  order
) {
  const values = mapFormValuesByCreatePaymentRequest(initValues, bill);
  const result = {
    paymentCreated: false,
    action: null
  };

  const paramsByKkmCommandRequest = mapFormValuesByReturnPaymentKkmRequest(
    values
  );

  const resultScenario = yield call(
    fetchKkmCommandSaga,
    createPaymentScenario(paramsByKkmCommandRequest, generateId())
  );

  if (
    getValueInDepth(resultScenario, [
      "response",
      "payload",
      "completionStatus"
    ]) !== 1
  ) {
    result.action = resultScenario;
    return result;
  }

  const params = mapFormValuesByPaymentRequest(values, warehouseInfo);
  const notificationId = generateId();

  yield put(
    addNotification({
      id: notificationId,
      message: "Отправлен запрос на создание платежа"
    })
  );

  const paymentResult = yield call(createPaymentSaga, createPayment(params));

  const payment = getValueInDepth(paymentResult, ["response", "payload"]);

  result.action = paymentResult;

  if (payment && payment.id) {
    yield put(
      addNotification({
        id: notificationId,
        message: (
          <span>
            Заказ № {order.code} оплачен. Платеж №{" "}
            <Link to={getUrl("payments", { id: payment.id })}>
              {payment.code}
            </Link>
          </span>
        )
      })
    );
    result.paymentCreated = true;
  }

  return result;
}
