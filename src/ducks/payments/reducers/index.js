import { combineReducers } from "redux";

import info from "./info";
import list from "./list";
import listForEntity from "./listForEntity";

export default combineReducers({
  info,
  list,
  listForEntity
});
