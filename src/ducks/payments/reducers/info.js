import { FETCH_PAYMENT_SUCCESS, UPDATE_PAYMENT_SUCCESS } from "../actions";
import { InfoStateRecord, fetchEntitySuccess } from "../../../helpers/reducers";

export default (state = { ...InfoStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_PAYMENT_SUCCESS:
    case UPDATE_PAYMENT_SUCCESS:
      return fetchEntitySuccess(state, payload);
    default:
      return state;
  }
};
