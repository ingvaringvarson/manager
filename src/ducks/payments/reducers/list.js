import { FETCH_PAYMENTS_SUCCESS, FETCH_PAYMENTS_ERROR } from "../actions";
import {
  ListStateRecord,
  fetchEntitiesSuccess,
  fetchEntitiesError
} from "../../../helpers/reducers";

export default (state = { ...ListStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_PAYMENTS_SUCCESS:
      return fetchEntitiesSuccess(state, payload);
    case FETCH_PAYMENTS_ERROR:
      return fetchEntitiesError(state, payload);
    default:
      return state;
  }
};
