import { prefix } from "./config";

export const ADD_ERROR = `${prefix}/ADD_ERROR`;
export const CLEAR_ERROR_LOG = `${prefix}/CLEAR_ERROR_LOG`;

export const ADD_LOAD = `${prefix}/ADD_LOAD`;
export const REMOVE_LOAD = `${prefix}/REMOVE_LOAD`;

export const ADD_NOTIFICATION = `${prefix}/ADD_NOTIFICATION`;
export const REMOVE_NOTIFICATION = `${prefix}/REMOVE_NOTIFICATION`;
export const CLEAR_NOTIFICATION_LOG = `${prefix}/CLEAR_NOTIFICATION_LOG`;

export const ADD_ACTION_RESULT = `${prefix}/ADD_ACTION_RESULT`;
export const CLEAR_ACTION_RESULT_LOG = `${prefix}/CLEAR_ACTION_RESULT_LOG`;
