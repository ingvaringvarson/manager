import { moduleName } from "./config";
import { createSelector } from "reselect";

const stateSelector = state => state[moduleName];

export const errorListSelector = (state, props = {}) => {
  return stateSelector(state, props).errors;
};

export const entityIdSelector = (state, props = {}) => {
  return props.id;
};

export const notificationListSelector = (state, props = {}) => {
  return stateSelector(state, props).notifications;
};

export const notificationByIdSelector = createSelector(
  notificationListSelector,
  entityIdSelector,
  (notificationList, id) => notificationList.find(item => item.id === id)
);

export const actionResultListSelector = (state, props = {}) => {
  return stateSelector(state, props).actionResults;
};

export const actionResultsByIdSelector = createSelector(
  actionResultListSelector,
  entityIdSelector,
  (actionResultList, id) =>
    actionResultList.find(item => item.id === id) || null
);

const arrEmpty = [];
export const loadsStateSelector = (state, props = {}) => {
  return stateSelector(state, props).loads;
};
export const moduleNamesSelector = (state, props = {}) => {
  return props.moduleNames || arrEmpty;
};
export const modulesIsLoadingSelector = createSelector(
  loadsStateSelector,
  moduleNamesSelector,
  (loads, moduleNames) => moduleNames.some(name => loads.includes(name))
);

const actionKeySelector = (state, props = {}) => {
  return props.actionKey || "";
};
export const actionIsLoadingSelector = createSelector(
  loadsStateSelector,
  actionKeySelector,
  (loads, actionKey) => loads.includes(actionKey)
);
