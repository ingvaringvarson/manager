import {
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
  CLEAR_NOTIFICATION_LOG
} from "../actions";
import { generateId } from "../../../utils";

const StateRecord = [];

const NotificationRecord = {
  id: null,
  key: null,
  message: "",
  loading: false,
  payload: null
};

const notificationLogReducer = (state = StateRecord, action) => {
  const { type, payload } = action;

  switch (type) {
    case ADD_NOTIFICATION:
      return addNotification(state, payload);
    case REMOVE_NOTIFICATION:
      return removeNotification(state, payload);
    case CLEAR_NOTIFICATION_LOG:
      return clearNotificationLog();
    default:
      return state;
  }
};

function addNotification(state, payload) {
  const newNotification = {
    ...NotificationRecord,
    id: generateId(),
    key: generateId(),
    ...payload.notification
  };

  if (payload.notification.id && state.length) {
    const indexNotification = state.findIndex(item => {
      return item.id === payload.notification.id;
    });

    if (~indexNotification) {
      const copyState = state.slice();
      copyState.splice(indexNotification, 1, newNotification);

      return copyState;
    }
  }

  return state.concat(newNotification);
}

function removeNotification(state, payload) {
  if (payload.notification.id && state.length) {
    const indexNotification = state.findIndex(item => {
      return item.id === payload.notification.id;
    });

    if (~indexNotification) {
      const copyState = state.slice();
      copyState.splice(indexNotification, 1);

      return copyState;
    }
  }

  return state;
}

function clearNotificationLog() {
  return StateRecord;
}

export default notificationLogReducer;
