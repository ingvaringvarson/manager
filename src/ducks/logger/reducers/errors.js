import { ADD_ERROR, CLEAR_ERROR_LOG } from "../actions";
import nanoid from "nanoid";

const StateRecord = [];

const ErrorRecord = { id: null };

const errorLogReducer = (state = StateRecord, action) => {
  const { type, payload } = action;

  switch (type) {
    case ADD_ERROR:
      return addError(state, payload);
    case CLEAR_ERROR_LOG:
      return clearErrorLog();
    default:
      return state;
  }
};

function addError(state, payload) {
  return state.concat({ ...ErrorRecord, ...payload.error, id: nanoid() });
}

function clearErrorLog() {
  return StateRecord;
}

export default errorLogReducer;
