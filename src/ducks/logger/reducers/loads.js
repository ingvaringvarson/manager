import { ADD_LOAD, REMOVE_LOAD } from "../actions";

const StateRecord = [];

const errorLogReducer = (state = StateRecord, action) => {
  const { type, payload } = action;

  switch (type) {
    case ADD_LOAD:
      return addLoad(state, payload);
    case REMOVE_LOAD:
      return removeLoad(state, payload);
    default:
      return state;
  }
};

function addLoad(state, payload) {
  return state.concat(payload.load);
}

function removeLoad(state, payload) {
  const indexLoad = state.findIndex(item => {
    return item === payload.load;
  });
  return ~indexLoad
    ? state.filter((item, index) => {
        return index !== indexLoad;
      })
    : state;
}

export default errorLogReducer;
