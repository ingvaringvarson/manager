import { combineReducers } from "redux";
import errors from "./errors";
import loads from "./loads";
import notifications from "./notifications";
import actionResults from "./actionResults";

export default combineReducers({
  errors,
  loads,
  notifications,
  actionResults
});
