import { ADD_ACTION_RESULT, CLEAR_ACTION_RESULT_LOG } from "../actions";
import { generateId } from "../../../utils";

const StateRecord = [];

const ActionResultRecord = { id: null, payload: null };

const actionResultLogReducer = (state = StateRecord, action) => {
  const { type, payload } = action;

  switch (type) {
    case ADD_ACTION_RESULT:
      return addActionResult(state, payload);
    case CLEAR_ACTION_RESULT_LOG:
      return clearActionResultLog();
    default:
      return state;
  }
};

function addActionResult(state, payload) {
  const newResult = {
    ...ActionResultRecord,
    id: generateId(),
    ...payload.actionResult
  };

  if (payload.actionResult.id && state.length) {
    const indexResult = state.findIndex(item => {
      return item.id === payload.actionResult.id;
    });

    if (~indexResult) {
      const copyState = state.slice();
      copyState.splice(indexResult, 1, newResult);

      return copyState;
    }
  }

  return state.concat(newResult);
}

function clearActionResultLog() {
  return StateRecord;
}

export default actionResultLogReducer;
