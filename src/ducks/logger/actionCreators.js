import {
  ADD_ERROR,
  CLEAR_ERROR_LOG,
  ADD_LOAD,
  REMOVE_LOAD,
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
  CLEAR_NOTIFICATION_LOG,
  ADD_ACTION_RESULT,
  CLEAR_ACTION_RESULT_LOG
} from "./actions";

export function addError(error) {
  return {
    type: ADD_ERROR,
    payload: {
      error
    }
  };
}

export function clearErrorLog() {
  return {
    type: CLEAR_ERROR_LOG
  };
}

export function addLoad(load) {
  return {
    type: ADD_LOAD,
    payload: {
      load
    }
  };
}

export function removeLoad(load) {
  return {
    type: REMOVE_LOAD,
    payload: {
      load
    }
  };
}

export function addNotification(notification) {
  return {
    type: ADD_NOTIFICATION,
    payload: {
      notification
    }
  };
}

export function removeNotification(notification) {
  return {
    type: REMOVE_NOTIFICATION,
    payload: {
      notification
    }
  };
}

export function clearNotificationLog() {
  return {
    type: CLEAR_NOTIFICATION_LOG
  };
}

export function addActionResult(actionResult) {
  return {
    type: ADD_ACTION_RESULT,
    payload: {
      actionResult
    }
  };
}

export function clearActionResultLog() {
  return {
    type: CLEAR_ACTION_RESULT_LOG
  };
}
