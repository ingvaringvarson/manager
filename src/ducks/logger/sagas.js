import {
  put,
  call,
  all,
  takeLatest,
  take,
  cancel,
  fork
} from "redux-saga/effects";
import {
  addError,
  addLoad,
  removeLoad,
  addNotification
} from "./actionCreators";
import fetchApi from "../../helpers/fetchApi";
import { ADD_ERROR } from "./actions";
import { forceLogOut } from "../user";
import { getValueInDepth } from "../../utils";

export function* fetchApiSaga(paramsRequest, ignoreError = false) {
  const { error, ...rest } = yield call(fetchApi(paramsRequest));

  if (error && !ignoreError) {
    yield put(addError(error));
  }

  return { error, ...rest };
}

function* addLoadSaga(actionKey) {
  if (Array.isArray(actionKey)) {
    yield all(
      actionKey.map(modName => {
        return put(addLoad(modName));
      })
    );
  } else {
    yield put(addLoad(actionKey));
  }
}

function* removeLoadSaga(actionKey) {
  if (Array.isArray(actionKey)) {
    yield all(
      actionKey.map(modName => {
        return put(removeLoad(modName));
      })
    );
  } else {
    yield put(removeLoad(actionKey));
  }
}

//Нельзя использовать с redux-saga/effects takeLatest
export function* loggingRequestSaga(saga, moduleName, action) {
  const actionKey = action.actionKey || moduleName;

  try {
    if (!action.withoutLoading) {
      yield call(addLoadSaga, actionKey);
    }
    yield call(saga, action);
    if (!action.withoutLoading) {
      yield call(removeLoadSaga, actionKey);
    }
  } catch (e) {
    if (!action.withoutLoading) {
      yield call(removeLoadSaga, actionKey);
    }

    console.error(
      "Произошла ошибка в работе приложения. Обновите страницу!",
      e
    );

    yield put(
      addNotification({
        message: "Произошла ошибка в работе приложения. Обновите страницу!",
        exception: e
      })
    );
  }
}

function* errorWatcherSaga(action) {
  const errorCode = getValueInDepth(action.payload, ["error", "code"]);

  if (errorCode === 401) {
    yield put(forceLogOut());
  }
}

/*
 * сага в качесвте аргумента принимает объект,
 * ключи которого это тип экшэна (action.type),
 * а значения это массив,
 * первый элемент массива - сага, которую нужно вызвать
 * второй элемент массива - moduleName даксы
 * */
export function* watchingTwinActionsSaga(actions = {}) {
  const actionTypes = Object.keys(actions);
  const runningActions = {};

  while (true) {
    try {
      const action = yield take(actionTypes);
      const [sagaByCurrentAction, moduleNameByCurrentAction] = actions[
        action.type
      ];

      if (action.actionKey) {
        if (runningActions[action.actionKey]) {
          yield cancel(runningActions[action.actionKey]);
          yield put(removeLoad(action.actionKey));
          delete runningActions[action.actionKey];
        }

        runningActions[action.actionKey] = yield fork(
          loggingRequestSaga,
          sagaByCurrentAction,
          moduleNameByCurrentAction,
          action
        );
      } else {
        yield fork(
          loggingRequestSaga,
          sagaByCurrentAction,
          moduleNameByCurrentAction,
          action
        );
      }
    } catch (e) {
      console.error(
        "Произошла ошибка в работе приложения. Обновите страницу!",
        e
      );

      yield put(
        addNotification({
          message: "Произошла ошибка в работе приложения. Обновите страницу!",
          exception: e
        })
      );
    }
  }
}

export function* rootSaga() {
  yield all([takeLatest(ADD_ERROR, errorWatcherSaga)]);
}
