import { appName } from "../../config";

export const moduleName = "logger";
export const prefix = `${appName}/${moduleName}`;
