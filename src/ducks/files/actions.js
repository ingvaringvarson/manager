import { prefix } from "./config";

export const FETCH_FILE_REQUEST = `${prefix}/FETCH_FILE_REQUEST`;
export const FETCH_FILE_SUCCESS = `${prefix}/FETCH_FILE_SUCCESS`;

export const UPLOAD_FILE_REQUEST = `${prefix}/UPLOAD_FILE_REQUEST`;
export const UPLOAD_FILE_SUCCESS = `${prefix}/UPLOAD_FILE_SUCCESS`;
export const UPLOAD_FILE_ERROR = `${prefix}/UPLOAD_FILE_ERROR`;
