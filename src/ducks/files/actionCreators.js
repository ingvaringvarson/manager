import { FETCH_FILE_REQUEST, UPLOAD_FILE_REQUEST } from "./actions";

export const fetchFile = fileId => ({
  type: FETCH_FILE_REQUEST,
  payload: { fileId }
});
export const uploadFile = file => ({
  type: UPLOAD_FILE_REQUEST,
  payload: { file }
});
