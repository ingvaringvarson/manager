import { appName, apiUri } from "../../config";

export const moduleName = "files";
export const prefix = `${appName}/${moduleName}`;
export const apiPrefix = `${apiUri}${moduleName}`;
export const typeEntity = "files";
