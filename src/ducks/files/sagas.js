import { all, call, takeEvery } from "redux-saga/effects";
import {
  FETCH_FILE_REQUEST,
  UPLOAD_FILE_REQUEST,
  UPLOAD_FILE_SUCCESS,
  UPLOAD_FILE_ERROR
} from "./actions";
import { loggingRequestSaga } from "../logger";
import { moduleName } from "./config";
import { generateGetApiUrl, postApiUrl } from "./apiUrls";
import { postCommonSaga } from "../../helpers/sagas";

export function* fetchFileSaga(action) {
  const { fileId } = action.payload;
  const browserTab = window.open(generateGetApiUrl(fileId), "_blank");

  if (browserTab && browserTab.window) browserTab.window.print();
}

export function* uploadFileSaga(action) {
  const { file } = action.payload;
  const actions = {
    success: UPLOAD_FILE_SUCCESS,
    error: UPLOAD_FILE_ERROR
  };
  const formData = new FormData();
  formData.append("file", file);

  const payloadForApi = {
    body: formData
  };

  return yield call(postCommonSaga, payloadForApi, actions, postApiUrl);
}

export function* rootSaga() {
  yield all([
    takeEvery(FETCH_FILE_REQUEST, fetchFileSaga),
    takeEvery(
      UPLOAD_FILE_REQUEST,
      loggingRequestSaga,
      uploadFileSaga,
      moduleName
    )
  ]);
}
