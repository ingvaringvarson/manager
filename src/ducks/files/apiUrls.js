import { apiPrefix } from "./config";

export const generateGetApiUrl = fileId => {
  return `${apiPrefix}/${fileId}`;
};

export const postApiUrl = "files/upload";
