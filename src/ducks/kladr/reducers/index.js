import { combineReducers } from "redux";
import suggest from "./suggest";
import info from "./info";

export default combineReducers({
  suggest,
  info
});
