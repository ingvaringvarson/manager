import { FETCH_KLADR_SUGGEST_SUCCESS } from "../actions";

const StateRecord = {};

const EntityRecord = {
  entities: []
};

function reducer(state = StateRecord, action) {
  const { type, payload } = action;

  switch (type) {
    case FETCH_KLADR_SUGGEST_SUCCESS:
      return fetchKladrSuggestSuccess(state, payload);
    default:
      return state;
  }
}

function fetchKladrSuggestSuccess(state, payload) {
  return {
    ...state,
    [payload.id]: {
      ...EntityRecord,
      entities: payload.entities
    }
  };
}

export default reducer;
