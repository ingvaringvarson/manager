import { FETCH_KLADR_INFO_SUCCESS } from "../actions";

const StateRecord = {};

function reducer(state = StateRecord, action) {
  const { type, payload } = action;

  switch (type) {
    case FETCH_KLADR_INFO_SUCCESS:
      return fetchKladrInfoSuccess(state, payload);
    default:
      return state;
  }
}

function fetchKladrInfoSuccess(state, payload) {
  return {
    ...state,
    [payload.id]: payload.info
  };
}

export default reducer;
