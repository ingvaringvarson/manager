import { appName } from "../../config";

export const moduleName = "kladr";
export const prefix = `${appName}/${moduleName}`;
