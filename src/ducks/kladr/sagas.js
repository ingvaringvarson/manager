import { all, call, put } from "redux-saga/effects";
import {
  FETCH_KLADR_INFO_SUCCESS,
  FETCH_KLADR_INFO_REQUEST,
  FETCH_KLADR_SUGGEST_REQUEST,
  FETCH_KLADR_SUGGEST_SUCCESS
} from "./actions";
import { fetchApiSaga, watchingTwinActionsSaga } from "../logger";
import { moduleName } from "./config";
import { getDaDataInfo, getDaDataSuggest } from "../../api/kladr";

export function* fetchKladrSuggestSaga(action) {
  const paramsRequest = getDaDataSuggest(action.payload.params);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (response && response.payload && response.payload.suggestions) {
    yield put({
      type: FETCH_KLADR_SUGGEST_SUCCESS,
      payload: {
        id: action.payload.id,
        entities: response.payload.suggestions
      }
    });
  }

  return { response, error };
}

export function* fetchKladrInfoSaga(action) {
  const paramsRequest = getDaDataInfo(action.payload.params);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (response && response.payload && response.payload.suggestions) {
    yield put({
      type: FETCH_KLADR_INFO_SUCCESS,
      payload: {
        id: action.payload.id,
        info: response.payload.suggestions[0] || null
      }
    });
  }

  return { response, error };
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_KLADR_SUGGEST_REQUEST]: [fetchKladrSuggestSaga, moduleName],
      [FETCH_KLADR_INFO_REQUEST]: [fetchKladrInfoSaga, moduleName]
    })
  ]);
}
