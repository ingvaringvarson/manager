import { prefix } from "./config";

export const FETCH_KLADR_INFO_REQUEST = `${prefix}/FETCH_KLADR_INFO_REQUEST`;
export const FETCH_KLADR_INFO_SUCCESS = `${prefix}/FETCH_KLADR_INFO_SUCCESS`;

export const FETCH_KLADR_SUGGEST_REQUEST = `${prefix}/FETCH_KLADR_SUGGEST_REQUEST`;
export const FETCH_KLADR_SUGGEST_SUCCESS = `${prefix}/FETCH_KLADR_SUGGEST_SUCCESS`;
