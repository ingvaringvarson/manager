import {
  FETCH_KLADR_INFO_REQUEST,
  FETCH_KLADR_SUGGEST_REQUEST
} from "./actions";

export function fetchKladrInfo(id, params) {
  return {
    type: FETCH_KLADR_INFO_REQUEST,
    payload: { id, params }
  };
}

export function fetchKladrSuggest(id, params) {
  return {
    type: FETCH_KLADR_SUGGEST_REQUEST,
    payload: { id, params }
  };
}
