import { moduleName } from "./config";

export const stateSelector = state => state[moduleName];
export const stateInfoSelector = state => stateSelector(state).info;
export const stateSuggestSelector = state => stateSelector(state).suggest;
