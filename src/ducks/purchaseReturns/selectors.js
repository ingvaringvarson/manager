import { createSelector } from "reselect";
import { moduleName } from "./config";
import { queryRouterSelector } from "../../redux/selectors/router";
import {
  entityListSelector,
  maxPagesSelector as maxPagesSelectorHelper,
  entitiesForTypeEntitySelector,
  entityInfoSelector
} from "../../helpers/selectors";

export const purchaseReturnsSelector = entityListSelector(moduleName);
export const purchaseReturnInfoSelector = entityInfoSelector(moduleName);
export const purchaseReturnsForEntitySelector = entitiesForTypeEntitySelector(
  moduleName
);
export const maxPagesSelector = maxPagesSelectorHelper(moduleName);

export const requestParamsForListSelector = createSelector(
  queryRouterSelector,
  query => {
    return {
      page: 1,
      page_size: 20,
      sort_by: "object_date_create desc",
      ...query,
      _tab: query._tab ? +query._tab : 1
    };
  }
);
