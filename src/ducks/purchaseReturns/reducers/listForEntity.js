import {
  FETCH_PURCHASE_RETURNS_FOR_ENTITY_SUCCESS,
  FETCH_PURCHASE_RETURNS_FOR_ENTITY_ERROR
} from "../actions";
import {
  EmptyStateRecord,
  fetchEntitiesForTypeEntityError,
  fetchEntitiesForTypeEntitySuccess
} from "../../../helpers/reducers";

export default (state = { ...EmptyStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_PURCHASE_RETURNS_FOR_ENTITY_SUCCESS:
      return fetchEntitiesForTypeEntitySuccess(state, payload);
    case FETCH_PURCHASE_RETURNS_FOR_ENTITY_ERROR:
      return fetchEntitiesForTypeEntityError(state, payload);
    default:
      return state;
  }
};
