import { FETCH_PURCHASE_RETURN_SUCCESS } from "../actions";
import { InfoStateRecord, fetchEntitySuccess } from "../../../helpers/reducers";

export default (state = { ...InfoStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_PURCHASE_RETURN_SUCCESS:
      return fetchEntitySuccess(state, payload);
    default:
      return state;
  }
};
