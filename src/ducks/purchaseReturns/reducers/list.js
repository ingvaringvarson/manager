import {
  FETCH_PURCHASE_RETURNS_SUCCESS,
  FETCH_PURCHASE_RETURNS_ERROR
} from "../actions";
import {
  ListStateRecord,
  fetchEntitiesSuccess,
  fetchEntitiesError
} from "../../../helpers/reducers";

export default (state = { ...ListStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_PURCHASE_RETURNS_SUCCESS:
      return fetchEntitiesSuccess(state, payload);
    case FETCH_PURCHASE_RETURNS_ERROR:
      return fetchEntitiesError(state, payload);
    default:
      return state;
  }
};
