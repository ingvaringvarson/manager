import {
  FETCH_PURCHASE_RETURNS_REQUEST,
  FETCH_PURCHASE_RETURN_REQUEST,
  FETCH_PURCHASE_RETURNS_FOR_ENTITY_REQUEST,
  CREATE_PURCHASE_RETURN_REQUEST,
  POST_PURCHASE_RETURN_REQUEST,
  PUT_PURCHASE_RETURN_REQUEST,
  POST_PURCHASE_RETURN_POSITIONS_REQUEST
} from "./actions";
import {
  fetchEntities,
  fetchEntitiesForTypeEntity,
  fetchEntity,
  postEntity,
  putEntity
} from "../../helpers/actionCreators";

export const fetchPurchaseReturns = fetchEntities(
  FETCH_PURCHASE_RETURNS_REQUEST
);

export const fetchPurchaseReturn = fetchEntity(FETCH_PURCHASE_RETURN_REQUEST);

export const fetchPurchaseReturnsForEntity = fetchEntitiesForTypeEntity(
  FETCH_PURCHASE_RETURNS_FOR_ENTITY_REQUEST
);

export const createPurchaseReturn = (
  formValues,
  helperValues,
  normalizeCallback
) => {
  return {
    type: CREATE_PURCHASE_RETURN_REQUEST,
    payload: { formValues, helperValues, normalizeCallback }
  };
};

export const postPurchaseReturn = postEntity(POST_PURCHASE_RETURN_REQUEST);

export const putPurchaseReturn = putEntity(PUT_PURCHASE_RETURN_REQUEST);

export const postPurchaseReturnPositions = postEntity(
  POST_PURCHASE_RETURN_POSITIONS_REQUEST
);
