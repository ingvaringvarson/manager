export function normalizePurchaseReturnCreate(formValues, helperValues) {
  const { positions, entities, forMulti } = helperValues;

  if (forMulti) {
    return {
      firm_id: entities[0].order.firm_id,
      warehouse_id: formValues.warehouse_id,
      positions: positions.reduce((result, currentPosition) => {
        result.push({
          purchase_order_id: entities.find(curEntity => {
            return curEntity.order.positions.some(
              curPos => curPos.id === currentPosition.id
            );
          }).order.id,
          purchase_order_position_id: currentPosition.id,
          count: +formValues[`returnCount__${currentPosition._id}`],
          reason: formValues[`reason__${currentPosition._id}`]
        });
        return result;
      }, [])
    };
  }

  return {
    firm_id: entities[0].order.firm_id,
    warehouse_id: formValues.warehouse_id,
    positions: positions.reduce((result, currentPosition) => {
      result.push({
        purchase_order_id: entities[0].order.id,
        purchase_order_position_id: currentPosition.id,
        count: +formValues[`returnCount__${currentPosition._id}`],
        reason: formValues[`reason__${currentPosition._id}`]
      });
      return result;
    }, [])
  };
}
