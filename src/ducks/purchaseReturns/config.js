import { appName } from "../../config";

export const moduleName = "purchaseReturns";
export const prefix = `${appName}/${moduleName}`;
