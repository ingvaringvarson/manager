import { all, call, takeEvery, put } from "redux-saga/effects";
import { push } from "connected-react-router";
import {
  FETCH_PURCHASE_RETURNS_REQUEST,
  FETCH_PURCHASE_RETURNS_SUCCESS,
  FETCH_PURCHASE_RETURNS_ERROR,
  FETCH_PURCHASE_RETURN_REQUEST,
  FETCH_PURCHASE_RETURN_SUCCESS,
  FETCH_PURCHASE_RETURN_ERROR,
  FETCH_PURCHASE_RETURNS_FOR_ENTITY_REQUEST,
  FETCH_PURCHASE_RETURNS_FOR_ENTITY_SUCCESS,
  FETCH_PURCHASE_RETURNS_FOR_ENTITY_ERROR,
  CREATE_PURCHASE_RETURN_REQUEST,
  CREATE_PURCHASE_RETURN_SUCCESS,
  CREATE_PURCHASE_RETURN_ERROR,
  POST_PURCHASE_RETURN_REQUEST,
  POST_PURCHASE_RETURN_SUCCESS,
  POST_PURCHASE_RETURN_ERROR,
  PUT_PURCHASE_RETURN_REQUEST,
  PUT_PURCHASE_RETURN_SUCCESS,
  PUT_PURCHASE_RETURN_ERROR,
  POST_PURCHASE_RETURN_POSITIONS_REQUEST,
  POST_PURCHASE_RETURN_POSITIONS_SUCCESS,
  POST_PURCHASE_RETURN_POSITIONS_ERROR
} from "./actions";
import {
  fetchEntitiesForTypeEntitySaga,
  fetchEntitiesSaga,
  fetchEntitySaga,
  postEntitySaga,
  postCommonSaga,
  putEntitySaga,
  postEntitiesByFormSaga
} from "../../helpers/sagas";
import { mapRequestParamsToParamsByApiSaga } from "../../helpers/mapMethods";
import { loggingRequestSaga, watchingTwinActionsSaga } from "../logger";
import { moduleName } from "./config";
import { getUrl } from "../../helpers/urls";

function* purchaseReturnsSaga(action) {
  const actions = {
    success: FETCH_PURCHASE_RETURNS_SUCCESS,
    error: FETCH_PURCHASE_RETURNS_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "purchaseReturns"
  );

  if (params.article) {
    params.article = params.article.replace(/[^0-9a-zа-яё]/gim, "");
  }

  const payloadForApi = {
    ...action.payload,
    params: params
  };

  yield call(
    fetchEntitiesSaga,
    payloadForApi,
    actions,
    "purchaseReturns",
    "purchase_returns_with_bills"
  );
}

function* purchaseReturnSaga(action) {
  const actions = {
    success: FETCH_PURCHASE_RETURN_SUCCESS,
    error: FETCH_PURCHASE_RETURN_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "purchaseReturns"
  );

  yield call(
    fetchEntitySaga,
    {
      id: action.payload.id,
      params
    },
    actions,
    "purchaseReturns",
    "purchase_returns_with_bills"
  );
}

function* purchaseReturnsForEntitySaga(action) {
  const actions = {
    success: FETCH_PURCHASE_RETURNS_FOR_ENTITY_SUCCESS,
    error: FETCH_PURCHASE_RETURNS_FOR_ENTITY_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "purchaseReturns"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  yield call(
    fetchEntitiesForTypeEntitySaga,
    payloadForApi,
    actions,
    "purchaseReturns",
    "purchase_returns_with_bills"
  );
}

function* createPurchaseReturnSaga(action) {
  const actions = {
    success: CREATE_PURCHASE_RETURN_SUCCESS,
    error: CREATE_PURCHASE_RETURN_ERROR
  };
  const { formValues, helperValues, normalizeCallback } = action.payload;

  const payloadForApi = {
    body:
      typeof normalizeCallback === "function"
        ? yield call(normalizeCallback, formValues, helperValues)
        : formValues
  };

  const { response } = yield call(
    postEntitySaga,
    payloadForApi,
    actions,
    "purchaseReturns"
  );

  if (
    response &&
    response.payload &&
    response.payload.purchase_return &&
    response.payload.purchase_return.id
  ) {
    yield put(
      push(
        getUrl("purchaseReturns", { id: response.payload.purchase_return.id })
      )
    );
  }
}

function* postPurchaseReturnSaga(action) {
  const {
    formValues,
    helperValues,
    normalizeCallback,
    withoutFetching
  } = action.payload;

  const actions = {
    success: POST_PURCHASE_RETURN_SUCCESS,
    error: POST_PURCHASE_RETURN_ERROR
  };

  const payloadForApi = {
    body:
      typeof normalizeCallback === "function"
        ? normalizeCallback(formValues, helperValues)
        : formValues
  };

  const postPayloadData = yield call(
    postCommonSaga,
    payloadForApi,
    actions,
    "purchaseReturns"
  );

  if (!withoutFetching && helperValues.fetchEntities) {
    yield call(helperValues.fetchEntities);
  }

  return postPayloadData;
}

function* putPurchaseReturnSaga(action) {
  const {
    formValues,
    helperValues,
    normalizeCallback,
    withoutFetching
  } = action.payload;

  const actions = {
    success: PUT_PURCHASE_RETURN_SUCCESS,
    error: PUT_PURCHASE_RETURN_ERROR
  };

  const payloadForApi = {
    body:
      typeof normalizeCallback === "function"
        ? normalizeCallback(formValues, helperValues)
        : formValues
  };

  const putPayloadData = yield call(
    putEntitySaga,
    payloadForApi,
    actions,
    "purchaseReturns"
  );

  if (
    putPayloadData &&
    putPayloadData.response &&
    putPayloadData.response.payload &&
    helperValues &&
    helperValues.successCallback
  ) {
    const purchaseReturn = putPayloadData.response.payload;
    yield call(helperValues.successCallback, purchaseReturn);
  }

  if (!withoutFetching && helperValues && helperValues.fetchEntities) {
    yield call(helperValues.fetchEntities);
  }

  return putPayloadData;
}

function* postPurchaseReturnPositionsSaga(action) {
  const actions = {
    success: POST_PURCHASE_RETURN_POSITIONS_SUCCESS,
    error: POST_PURCHASE_RETURN_POSITIONS_ERROR
  };

  return yield call(
    postEntitiesByFormSaga,
    action.payload,
    actions,
    "purchaseReturnPositions"
  );
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_PURCHASE_RETURNS_REQUEST]: [purchaseReturnsSaga, moduleName],
      [FETCH_PURCHASE_RETURN_REQUEST]: [purchaseReturnSaga, moduleName],
      [FETCH_PURCHASE_RETURNS_FOR_ENTITY_REQUEST]: [
        purchaseReturnsForEntitySaga,
        moduleName
      ]
    }),
    takeEvery(
      CREATE_PURCHASE_RETURN_REQUEST,
      loggingRequestSaga,
      createPurchaseReturnSaga,
      moduleName
    ),
    takeEvery(
      POST_PURCHASE_RETURN_REQUEST,
      loggingRequestSaga,
      postPurchaseReturnSaga,
      moduleName
    ),
    takeEvery(
      PUT_PURCHASE_RETURN_REQUEST,
      loggingRequestSaga,
      putPurchaseReturnSaga,
      moduleName
    ),
    takeEvery(
      POST_PURCHASE_RETURN_POSITIONS_REQUEST,
      loggingRequestSaga,
      postPurchaseReturnPositionsSaga,
      moduleName
    )
  ]);
}
