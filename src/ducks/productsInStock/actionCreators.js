import {
  fetchEntities,
  fetchEntitiesForTypeEntity
} from "../../helpers/actionCreators";
import {
  FETCH_PRODUCTS_IN_STOCK_REQUEST,
  FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_REQUEST
} from "./actions";

export const fetchProductsInStockList = fetchEntities(
  FETCH_PRODUCTS_IN_STOCK_REQUEST
);

export const fetchProductsInStock = (idEntity, params) =>
  fetchEntitiesForTypeEntity(FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_REQUEST)(
    "",
    idEntity,
    params
  );
