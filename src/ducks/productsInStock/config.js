import { appName } from "../../config";

export const moduleName = "productsInStock";
export const prefix = `${appName}/${moduleName}`;

export const pathByApi = "products/instock";
export const searchLoad = `${moduleName}__searchLoad`;
