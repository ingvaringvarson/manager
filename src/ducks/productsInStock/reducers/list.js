import {
  FETCH_PRODUCTS_IN_STOCK_SUCCESS,
  FETCH_PRODUCTS_IN_STOCK_ERROR
} from "../actions";
import {
  ListStateRecord,
  fetchEntitiesSuccess,
  fetchEntitiesError
} from "../../../helpers/reducers";

const listReducer = (state = { ...ListStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_PRODUCTS_IN_STOCK_SUCCESS:
      return fetchEntitiesSuccess(state, payload);
    case FETCH_PRODUCTS_IN_STOCK_ERROR:
      return fetchEntitiesError(state, payload);
    default:
      return state;
  }
};

export default listReducer;
