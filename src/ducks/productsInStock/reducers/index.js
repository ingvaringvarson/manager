import { combineReducers } from "redux";
import listForEntity from "./listForEntity";
import list from "./list";

export default combineReducers({
  listForEntity,
  list
});
