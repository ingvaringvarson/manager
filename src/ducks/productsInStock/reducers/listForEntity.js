import {
  FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_SUCCESS,
  FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_ERROR
} from "../actions";
import {
  EmptyStateRecord,
  fetchEntitiesForTypeEntityError,
  fetchEntitiesForTypeEntitySuccess
} from "../../../helpers/reducers";

function listForEntity(state = { ...EmptyStateRecord }, action) {
  const { type, payload } = action;

  switch (type) {
    case FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_SUCCESS:
      return fetchEntitiesForTypeEntitySuccess(state, payload);
    case FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_ERROR:
      return fetchEntitiesForTypeEntityError(state, payload);
    default:
      return state;
  }
}

export default listForEntity;
