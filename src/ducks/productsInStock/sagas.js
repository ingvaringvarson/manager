import { all, call, put } from "redux-saga/effects";
import {
  FETCH_PRODUCTS_IN_STOCK_REQUEST,
  FETCH_PRODUCTS_IN_STOCK_SUCCESS,
  FETCH_PRODUCTS_IN_STOCK_ERROR,
  FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_REQUEST,
  FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_SUCCESS,
  FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_ERROR
} from "./actions";
import { fetchApiSaga, watchingTwinActionsSaga } from "../logger";
import { postEntities } from "../../api/requestByTypeEntities";
import { mapRequestParamsToParamsByApiSaga } from "../../helpers/mapMethods";
import { moduleName, pathByApi, searchLoad } from "./config";

export function* fetchProductsInStockSaga(action) {
  const reducerData = {
    reducerAction: {
      success: FETCH_PRODUCTS_IN_STOCK_SUCCESS,
      error: FETCH_PRODUCTS_IN_STOCK_ERROR
    }
  };
  return yield fetchProductsInStockBaseSaga(action, reducerData);
}

export function* fetchProductsInStockForEntitySaga(action) {
  const { idEntity, typeEntity } = action.payload;
  const reducerData = {
    reducerAction: {
      success: FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_SUCCESS,
      error: FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_ERROR
    },
    idEntity,
    typeEntity
  };
  return yield fetchProductsInStockBaseSaga(action, reducerData);
}

function* fetchProductsInStockBaseSaga(action, reducerData) {
  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    moduleName
  );

  const paramsRequest = postEntities(pathByApi, params);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);
  const { reducerAction, ...rest } = reducerData;

  if (response && response.payload) {
    const products_with_stocks = response.payload.products_with_stocks;
    const maxPages =
      products_with_stocks &&
      products_with_stocks.length > 0 &&
      products_with_stocks[0].pages_num;

    return yield put({
      type: reducerAction.success,
      payload: {
        maxPages,
        entities: products_with_stocks,
        ...rest
      }
    });
  } else if (error) {
    return yield put({
      type: reducerAction.error,
      payload: {
        error,
        ...rest
      }
    });
  }
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_PRODUCTS_IN_STOCK_REQUEST]: [
        fetchProductsInStockSaga,
        [moduleName, searchLoad]
      ],
      [FETCH_PRODUCTS_IN_STOCK_FOR_ENTITY_REQUEST]: [
        fetchProductsInStockForEntitySaga,
        moduleName
      ]
    })
  ]);
}
