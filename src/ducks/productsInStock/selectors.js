import { createSelector } from "reselect";
import { moduleName, searchLoad } from "./config";
import { loadsStateSelector } from "../logger/selectors";
import {
  listStateSelector,
  entitiesForTypeEntitySelector,
  maxPagesSelector as maxPagesSelectorHelper
} from "../../helpers/selectors";

import { employeeInfoByUserSelector } from "../user";

export const maxPagesSelector = maxPagesSelectorHelper(moduleName);
export const productsInStockSelector = (state, idEntity) => {
  const result = entitiesForTypeEntitySelector(moduleName)(state, {
    idEntity,
    typeEntity: ""
  });

  return result.entities;
};
export const productsInStockListSelector = listStateSelector(moduleName);

export { requestParamsInStockSelector } from "../../redux/selectors/router";

export const defaultValuesForFieldsSelector = createSelector(
  employeeInfoByUserSelector,
  employee => {
    const defaultParams = {
      page_product: 1,
      page_size_product: 20,
      page_product_unit: 1,
      page_size_product_unit: 20
    };

    if (employee && employee.warehouse_id) {
      defaultParams.warehouse_id = employee.warehouse_id;
    }

    if (employee && employee.firm_id) {
      defaultParams.firm_id = employee.firm_id;
    }

    return defaultParams;
  }
);

export const productsInStockUnitsSelector = createSelector(
  productsInStockSelector,
  productsArray => {
    if (
      !productsArray ||
      (Array.isArray(productsArray) && !productsArray.length)
    ) {
      return null;
    }

    try {
      const productInStocks = productsArray[0].product_in_stock[0];
      const productUnits = productInStocks.product_units;

      if (Array.isArray(productUnits) && !productUnits.length) {
        return null;
      }

      return productUnits[0];
    } catch (e) {
      return null;
    }
  }
);
export const productsInStockLoadingSelector = createSelector(
  loadsStateSelector,
  loads => {
    return loads.some(load => load === searchLoad);
  }
);
