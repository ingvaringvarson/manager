import { moduleName } from "./config";
import { EnumerationRecord } from "./reducers";
import createCachedSelector from "re-reselect";
import { createSelector } from "reselect";
import memoize from "memoize-state";

const stateSelector = state => state[moduleName];

export const enumerationStateSelector = (state, props = {}) => {
  return stateSelector(state)[props.name] || EnumerationRecord;
};

export const enumerationIsLoadedSelector = (state, props = {}) => {
  return enumerationStateSelector(state, props).isLoaded;
};

export const enumerationListSelector = (state, props = {}) => {
  return enumerationStateSelector(state, props).list;
};

export const enumerationMapSelector = (state, props = {}) => {
  return enumerationStateSelector(state, props).map;
};

export const enumerationIdListSelector = (state, props = {}) => {
  return enumerationListSelector(state, props).reduce((list, item) => {
    return list.concat(item.id);
  }, []);
};

const arrEmpty = [];
const enumerationNamesSelector = (state, props = {}) => {
  return props.enumerationNames || arrEmpty;
};

const enumerationCashedKeySelector = (state, props = {}) => {
  return props.enumerationNames ? props.enumerationNames.join("") : "empty";
};

const mapEnumerations = (enumerations, names) => {
  return names.reduce(
    (enumers, name) => ({
      ...enumers,
      [name]: enumerations[name] || EnumerationRecord
    }),
    {}
  );
};

const memoizeByKey = {};

const mapEnumerationsSelector = (enumerations, names, key) => {
  if (!memoizeByKey[key]) {
    memoizeByKey[key] = memoize(mapEnumerations);
  }

  return memoizeByKey[key](enumerations, names);
};

export const enumerationsSelector = createCachedSelector(
  stateSelector,
  enumerationNamesSelector,
  enumerationCashedKeySelector,
  mapEnumerationsSelector
)(enumerationCashedKeySelector);

export const enumerationListWithoutZeroIdSelector = createCachedSelector(
  enumerationListSelector,
  list => {
    return list.filter(item => item.id !== 0);
  }
)((state, props = {}) => props.name);

export const agentTypesByClientTabsSelector = createSelector(
  enumerationMapSelector,
  agentTypes => {
    const tabs = [];

    if (agentTypes[1]) {
      tabs.push({ ...agentTypes[1], name: "Физические" });
    }

    if (agentTypes[2]) {
      tabs.push({ ...agentTypes[2], name: "Юридические" });
    }

    return tabs;
  }
);
