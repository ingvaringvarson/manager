import { prefix } from "./config";

export const FETCH_ENUMERATIONS_REQUEST = `${prefix}/FETCH_ENUMERATIONS_REQUEST`;

export const FETCH_ENUMERATIONS_SUCCESS = `${prefix}/FETCH_ENUMERATIONS_SUCCESS`;
