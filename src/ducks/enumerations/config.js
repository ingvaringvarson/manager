import { appName } from "../../config";

export const moduleName = "enumerations";
export const prefix = `${appName}/${moduleName}`;
