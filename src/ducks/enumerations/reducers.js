import { FETCH_ENUMERATIONS_SUCCESS } from "./actions";
import { arrayToObject } from "../../utils";

const StateRecord = {};

export const EnumerationRecord = {
  list: [],
  map: {},
  isLoaded: false
};

const enumerationsReducer = (state = StateRecord, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_ENUMERATIONS_SUCCESS:
      return fetchEnumerationsSuccess(state, payload);
    default:
      return state;
  }
};

function fetchEnumerationsSuccess(state, payload) {
  return !payload.names
    ? state
    : payload.names.reduce(
        (newState, name) => {
          if (!payload.entities[name]) return newState;

          const entities = [...payload.entities[name]];

          if (name === "city") {
            entities.push({
              id: -1,
              name: "Дрйгой город"
            });
          }

          const list = entities.filter(entity => {
            return entity.id !== 0;
          });

          return {
            ...newState,
            [name]: {
              ...EnumerationRecord,
              list,
              map: arrayToObject(entities),
              isLoaded: true
            }
          };
        },
        { ...state }
      );
}

export default enumerationsReducer;
