import { all, call, put, select } from "redux-saga/effects";
import {
  FETCH_ENUMERATIONS_REQUEST,
  FETCH_ENUMERATIONS_SUCCESS
} from "./actions";
import { moduleName } from "./config";
import { getEnumeration } from "../../api/enumerations";
import { fetchApiSaga, watchingTwinActionsSaga } from "../logger";
import { enumerationsSelector } from "./selectors";

export function* fetchEnumerationsSaga(action) {
  const names = action.payload.names;

  if (!names) {
    throw "Должны быть заданы имена moduleNames";
  }

  const enumerations = yield select(enumerationsSelector, {
    enumerationNames: names
  });

  const enumerationsIsNotLoaded = names.filter(name => {
    return !(enumerations[name] && enumerations[name].isLoaded);
  });

  if (!enumerationsIsNotLoaded.length) return;

  const { ...responses } = yield all(
    enumerationsIsNotLoaded.reduce(
      (requestes, name) => ({
        ...requestes,
        [name]: call(fetchApiSaga, getEnumeration(name))
      }),
      {}
    )
  );

  if (responses) {
    yield put({
      type: FETCH_ENUMERATIONS_SUCCESS,
      payload: {
        names: enumerationsIsNotLoaded,
        entities: Object.keys(responses).reduce((entities, name) => {
          if (!responses[name].response) return entities;

          return {
            ...entities,
            [name]: responses[name].response.payload
          };
        }, {})
      }
    });
  }
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_ENUMERATIONS_REQUEST]: [fetchEnumerationsSaga, moduleName]
    })
  ]);
}
