import { FETCH_ENUMERATIONS_REQUEST } from "./actions";

export function fetchEnumerations(names) {
  return {
    type: FETCH_ENUMERATIONS_REQUEST,
    payload: {
      names
    }
  };
}
