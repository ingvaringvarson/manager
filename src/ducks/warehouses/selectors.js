import { moduleName } from "./config";
import {
  queryRouterSelector,
  entityIdForRouteSelector
} from "../../redux/selectors/router";
import { createSelector } from "reselect";
import {
  entityListSelector,
  maxPagesSelector as maxPagesSelectorHelper,
  typeEntityStateSelector as typeEntityStateSelectorHelper,
  entitiesForTypeEntitySelector,
  entityInfoSelector
} from "../../helpers/selectors";

import { entitiesToOptions } from "../../utils";

export const warehousesSelector = entityListSelector(moduleName);
export const maxPagesSelector = maxPagesSelectorHelper(moduleName);

export const typeEntityStateSelector = typeEntityStateSelectorHelper(
  moduleName
);

export const warehousesForEntitySelector = entitiesForTypeEntitySelector(
  moduleName
);
export const warehousesToOptionsSelector = (getName, getId) => (
  state,
  props
) => {
  const { entities = [] } = warehousesForEntitySelector(state, props);
  return entitiesToOptions(entities, getName, getId);
};

export const requestParamsSelector = createSelector(
  queryRouterSelector,
  query => ({ page_size: 10, page: 1, ...query })
);

export const requestParamsForClientSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  (query, id) => {
    return {
      page_size: 10,
      page: 1,
      agent_id: id,
      ...query
    };
  }
);

export const warehouseInfoSelector = entityInfoSelector(moduleName);
