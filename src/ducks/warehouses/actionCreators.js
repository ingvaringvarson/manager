import {
  FETCH_WAREHOUSE_REQUEST,
  FETCH_WAREHOUSES_REQUEST,
  FETCH_WAREHOUSES_FOR_ENTITY_REQUEST
} from "./actions";
import {
  fetchEntities,
  fetchEntitiesForTypeEntity,
  fetchEntity
} from "../../helpers/actionCreators";

export const fetchWarehouses = fetchEntities(FETCH_WAREHOUSES_REQUEST);

export const fetchWarehouse = fetchEntity(FETCH_WAREHOUSE_REQUEST);

export const fetchWarehousesForEntity = fetchEntitiesForTypeEntity(
  FETCH_WAREHOUSES_FOR_ENTITY_REQUEST
);
