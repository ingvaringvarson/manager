import { generateId, getValueInDepth } from "./../../utils";
import { addNotification } from "./../logger";
import { fetchWarehousesForEntity } from "./actionCreators";
import { all, call, put } from "redux-saga/effects";
import { watchingTwinActionsSaga } from "../logger";
import {
  fetchEntitiesForTypeEntitySaga,
  fetchEntitiesSaga,
  fetchEntitySaga
} from "../../helpers/sagas";
import { moduleName } from "./config";
import { mapRequestParamsToParamsByApiSaga } from "../../helpers/mapMethods";
import {
  FETCH_WAREHOUSES_REQUEST,
  FETCH_WAREHOUSES_SUCCESS,
  FETCH_WAREHOUSES_ERROR,
  FETCH_WAREHOUSE_REQUEST,
  FETCH_WAREHOUSE_SUCCESS,
  FETCH_WAREHOUSE_ERROR,
  FETCH_WAREHOUSES_FOR_ENTITY_REQUEST,
  FETCH_WAREHOUSES_FOR_ENTITY_SUCCESS,
  FETCH_WAREHOUSES_FOR_ENTITY_ERROR
} from "./actions";

export function* fetchWarehouseSaga(action) {
  const actions = {
    success: FETCH_WAREHOUSE_SUCCESS,
    error: FETCH_WAREHOUSE_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "warehouses"
  );

  return yield call(
    fetchEntitySaga,
    {
      id: action.payload.id,
      params
    },
    actions,
    "warehouses",
    "warehouses"
  );
}

export function* fetchWarehousesSaga(action) {
  const actions = {
    success: FETCH_WAREHOUSES_SUCCESS,
    error: FETCH_WAREHOUSES_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "warehouses"
  );

  const payloadForApi = {
    ...action.payload,
    params: params
  };

  return yield call(
    fetchEntitiesSaga,
    payloadForApi,
    actions,
    "warehouses",
    "warehouses"
  );
}

export function* fetchWarehousesForEntitySaga(action) {
  const actions = {
    success: FETCH_WAREHOUSES_FOR_ENTITY_SUCCESS,
    error: FETCH_WAREHOUSES_FOR_ENTITY_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "warehouses"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  return yield call(
    fetchEntitiesForTypeEntitySaga,
    payloadForApi,
    actions,
    "warehouses",
    "warehouses"
  );
}

export function* fetchCurrentWarehouseInfoSaga(action) {
  let {
    paramsByWarehouseRequest,
    userFirmId,
    userWarehouseId
  } = action.payload;

  // здесь происходит проверка, даже если поля не участвуют в запросе
  if (!userFirmId || !userWarehouseId) {
    return yield put(
      addNotification({
        message: "У работника нет firm_id и/или warehouse_id"
      })
    );
  }

  const resultWarehouse = yield call(
    fetchWarehousesForEntitySaga,
    fetchWarehousesForEntity("payments", generateId(), paramsByWarehouseRequest)
  );

  const warehouseInfo = getValueInDepth(resultWarehouse, [
    "response",
    "payload",
    "warehouses",
    0
  ]);

  if (warehouseInfo) {
    return warehouseInfo;
  }

  const warehouseInfoStr = JSON.stringify(paramsByWarehouseRequest);
  return yield put(
    addNotification({
      message: `Не найдена информация о складе по запросу ${warehouseInfoStr}`
    })
  );
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_WAREHOUSES_REQUEST]: [fetchWarehousesSaga, moduleName],
      [FETCH_WAREHOUSES_FOR_ENTITY_REQUEST]: [
        fetchWarehousesForEntitySaga,
        moduleName
      ],
      [FETCH_WAREHOUSE_REQUEST]: [fetchWarehouseSaga, moduleName]
    })
  ]);
}
