import { FETCH_WAREHOUSES_ERROR, FETCH_WAREHOUSES_SUCCESS } from "../actions";
import {
  ListStateRecord,
  fetchEntitiesSuccess,
  fetchEntitiesError
} from "../../../helpers/reducers";

const listReducer = (state = { ...ListStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_WAREHOUSES_SUCCESS:
      return fetchEntitiesSuccess(state, payload);
    case FETCH_WAREHOUSES_ERROR:
      return fetchEntitiesError(state, payload);
    default:
      return state;
  }
};

export default listReducer;
