import { appName } from "../../config";

export const moduleName = "warehouses";
export const prefix = `${appName}/${moduleName}`;
