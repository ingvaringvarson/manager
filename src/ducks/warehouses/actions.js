import { prefix } from "./config";

export const FETCH_WAREHOUSES_REQUEST = `${prefix}/FETCH_WAREHOUSES_REQUEST`;
export const FETCH_WAREHOUSES_SUCCESS = `${prefix}/FETCH_WAREHOUSES_SUCCESS`;
export const FETCH_WAREHOUSES_ERROR = `${prefix}/FETCH_WAREHOUSES_ERROR`;

export const FETCH_WAREHOUSE_REQUEST = `${prefix}/FETCH_WAREHOUSE_REQUEST`;
export const FETCH_WAREHOUSE_SUCCESS = `${prefix}/FETCH_WAREHOUSE_SUCCESS`;
export const FETCH_WAREHOUSE_ERROR = `${prefix}/FETCH_WAREHOUSE_ERROR`;

export const FETCH_WAREHOUSES_FOR_ENTITY_REQUEST = `${prefix}/FETCH_WAREHOUSES_FOR_ENTITY_REQUEST`;
export const FETCH_WAREHOUSES_FOR_ENTITY_SUCCESS = `${prefix}/FETCH_WAREHOUSES_FOR_ENTITY_SUCCESS`;
export const FETCH_WAREHOUSES_FOR_ENTITY_ERROR = `${prefix}/FETCH_WAREHOUSES_FOR_ENTITY_ERROR`;
