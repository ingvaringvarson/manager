import {
  FETCH_BILL_SUCCESS,
  PUT_PREPARED_BILL_SUCCESS,
  UPDATE_BILL_SUCCESS
} from "../actions";
import { InfoStateRecord, fetchEntitySuccess } from "../../../helpers/reducers";

const infoReducer = (state = { ...InfoStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case PUT_PREPARED_BILL_SUCCESS:
    case FETCH_BILL_SUCCESS:
    case UPDATE_BILL_SUCCESS:
      return fetchEntitySuccess(state, payload);
    default:
      return state;
  }
};

export default infoReducer;
