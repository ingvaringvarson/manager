import {
  FETCH_BILLS_FOR_ENTITY_ERROR,
  FETCH_BILLS_FOR_ENTITY_SUCCESS
} from "../actions";
import {
  EmptyStateRecord,
  fetchEntitiesForTypeEntityError,
  fetchEntitiesForTypeEntitySuccess
} from "../../../helpers/reducers";

const listForEntityReducer = (state = { ...EmptyStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_BILLS_FOR_ENTITY_ERROR:
      return fetchEntitiesForTypeEntityError(state, payload);
    case FETCH_BILLS_FOR_ENTITY_SUCCESS:
      return fetchEntitiesForTypeEntitySuccess(state, payload);
    default:
      return state;
  }
};

export default listForEntityReducer;
