import { appName } from "../../config";

export const moduleName = "bills";
export const prefix = `${appName}/${moduleName}`;
