import {
  all,
  call,
  takeEvery,
  select,
  put,
  takeLatest
} from "redux-saga/effects";
import {
  FETCH_BILLS_SUCCESS,
  FETCH_BILLS_REQUEST,
  FETCH_BILLS_ERROR,
  FETCH_BILLS_FOR_ENTITY_REQUEST,
  FETCH_BILLS_FOR_ENTITY_SUCCESS,
  FETCH_BILLS_FOR_ENTITY_ERROR,
  FETCH_BILL_AND_CLIENT_REQUEST,
  FETCH_BILL_REQUEST,
  FETCH_BILL_SUCCESS,
  FETCH_BILL_ERROR,
  FETCH_BILLS_WITH_STATE_FOR_ENTITY_REQUEST,
  FETCH_BILLS_WITH_STATE_FOR_ENTITY_SUCCESS,
  FETCH_BILLS_WITH_STATE_FOR_ENTITY_ERROR,
  POST_BILL_REQUEST,
  POST_BILL_SUCCESS,
  POST_BILL_ERROR,
  CREATE_BILL_REQUEST,
  PUT_PREPARED_BILL_REQUEST,
  PUT_PREPARED_BILL_SUCCESS,
  PUT_PREPARED_BILL_ERROR,
  CREATE_DUMMY_BILL_REQUEST,
  UPDATE_BILL_REQUEST,
  UPDATE_BILL_SUCCESS,
  CREATE_BILL_BY_FORM_REQUEST
} from "./actions";
import { loggingRequestSaga, watchingTwinActionsSaga } from "../logger";
import {
  fetchEntitiesForTypeEntitySaga,
  fetchEntitiesSaga,
  postEntitySaga,
  fetchEntitySaga,
  getApiMethodResponseSaga,
  putEntitySaga,
  responseToUpdateEntitySaga,
  postEntitiesByFormSaga
} from "../../helpers/sagas";
import { moduleName } from "./config";
import { mapRequestParamsToParamsByApiSaga } from "../../helpers/mapMethods";

import { fetchClient } from "../clients";
import { billInfoSelector } from "./selectors";
import { authIdSelector } from "../user";
import { mapValuesToEntity } from "../../helpers/dataMapping";
import { push } from "connected-react-router";
import { getUrl } from "../../helpers/urls";

export function* fetchBillSuccessSaga(action) {
  if (!action.payload.forInfoPage) return;

  const params = { page: 1, page_size: 1 };
  const { agent_id } = action.payload.entity;

  if (agent_id) {
    yield put(
      fetchClient(agent_id, {
        ...params,
        id: agent_id
      })
    );
  }
}

export function* fetchBillsSaga(action) {
  const actions = {
    success: FETCH_BILLS_SUCCESS,
    error: FETCH_BILLS_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "bills"
  );

  const payloadForApi = {
    ...action.payload,
    params: params
  };

  yield call(fetchEntitiesSaga, payloadForApi, actions, "bills", "bills");
}

export function* fetchBillsForEntitySaga(action) {
  const actions = {
    success: FETCH_BILLS_FOR_ENTITY_SUCCESS,
    error: FETCH_BILLS_FOR_ENTITY_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "bills"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  yield call(
    fetchEntitiesForTypeEntitySaga,
    payloadForApi,
    actions,
    "bills",
    "bills"
  );
}

export function* postBillSaga(action) {
  const actions = {
    success: POST_BILL_SUCCESS,
    error: POST_BILL_ERROR
  };

  return yield call(postEntitySaga, action.payload, actions, "bills");
}

export function* createBillSaga(action) {
  const { formData, typeEntity, values } = action.payload;

  let mainBody = {};
  let positionsByBillType = {
    prepayed: [],
    notPrepayed: []
  };

  // ToDo: use mapValuesToRequest
  mainBody.order_id = values.orders.map(order => order.id);
  mainBody.contract_id = values.orders[0].contract_id;
  mainBody.firm_id = values.orders[0].firm_id;
  mainBody.agent_id = values.orders[0].agent_id;
  mainBody.object_id_create = values.orders[0].object_id_create;

  values.orders.forEach(currentOrder =>
    currentOrder.positions
      .filter(pos => values.selectedAll || values.positionsIds.includes(pos.id))
      .forEach(pos => {
        positionsByBillType.prepayed.push({
          position_id: pos.id,
          sum: pos.prepaid_sum,
          order_id: currentOrder.id,
          discount_sum: pos.discount
        });
        positionsByBillType.notPrepayed.push({
          position_id: pos.id,
          sum: pos.sum,
          order_id: currentOrder.id,
          discount_sum: pos.discount
        });
      })
  );

  const getMainBodyByType = type => ({
    ...mainBody,
    type: type === "prepayed" ? 4 : 1,
    fact_sum: formData[type] ? +formData[type] : null,
    bill_sum: formData[type] ? +formData[type] : null,
    positions: positionsByBillType[type]
  });

  switch (formData.billType) {
    case "prepayed":
    case "notPrepayed":
      return yield call(postBillSaga, {
        payload: {
          body: getMainBodyByType(formData.billType),
          typeEntity
        }
      });
    case "twoBills":
      yield call(postBillSaga, {
        payload: {
          body: getMainBodyByType("prepayed"),
          typeEntity
        }
      });
      return yield call(postBillSaga, {
        payload: {
          body: getMainBodyByType("notPrepayed"),
          typeEntity
        }
      });
  }
}

export function* fetchBillSaga(
  action = {
    payload: {
      params: {},
      id: ""
    }
  }
) {
  const actions = {
    success: FETCH_BILL_SUCCESS,
    error: FETCH_BILL_ERROR
  };

  const payloadForApi = {
    ...action.payload,
    params: {
      ...action.payload.params,
      id: action.payload.id
    }
  };

  yield call(fetchEntitySaga, payloadForApi, actions, "bills", "bills");
}

export function* fetchBillAndClientSaga(
  action = {
    payload: {
      params: {},
      id: ""
    }
  }
) {
  yield call(fetchBillSaga, action);

  const bill = yield select(billInfoSelector, { id: action.payload.id });

  if (bill && bill.agent_id) {
    yield put(fetchClient(bill.agent_id, { id: bill.agent_id }));
  }
}

export function* fetchBillsWithStateForEntitySaga(
  action = {
    payload: {
      typeEntity: "",
      idEntity: "",
      params: {}
    }
  }
) {
  let payload = {
    idEntity: action.payload.idEntity,
    typeEntity: action.payload.typeEntity,
    entities: [],
    maxPages: 1
  };

  if (action.payload.params.billState === 2) {
    const params = {
      ...action.payload.params,
      state: 4
    };
    delete params.billState;
    const { response, error } = yield call(
      getApiMethodResponseSaga,
      { params },
      "bills"
    );

    if (response && response.payload) {
      payload.entities = response.payload["bills"] || [];
      payload.maxPages = response.payload.pages_num;

      yield put({
        type: FETCH_BILLS_WITH_STATE_FOR_ENTITY_SUCCESS,
        payload
      });
    } else {
      yield put({
        type: FETCH_BILLS_WITH_STATE_FOR_ENTITY_ERROR,
        payload: { error }
      });
    }
  } else {
    const states = [1, 2, 3];
    const responses = yield all(
      states.map(state => {
        const params = {
          ...action.payload.params,
          state
        };
        delete params.billState;
        return call(getApiMethodResponseSaga, { params }, "bills");
      })
    );

    for (let i = 0; i < responses.length; i++) {
      if (responses[i].response) {
        const resEntities =
          responses[i].response.payload && responses[i].response.payload.bills;
        payload.entities = resEntities
          ? [...payload.entities, ...resEntities]
          : payload.entities;
      } else if (responses[i].error) {
        yield put({
          type: FETCH_BILLS_WITH_STATE_FOR_ENTITY_ERROR,
          payload: { error: responses[i].error }
        });
      }
    }

    if (payload.entities.length) {
      yield put({
        type: FETCH_BILLS_WITH_STATE_FOR_ENTITY_SUCCESS,
        payload: {
          ...payload,
          maxPages: Math.ceil(
            payload.entities / action.payload.params.page_size
          )
        }
      });
    }
  }
}

export function* putPreparedBillSaga(action) {
  const actions = {
    success: PUT_PREPARED_BILL_SUCCESS,
    error: PUT_PREPARED_BILL_ERROR
  };

  const { idEntity, entity, typeEntity, successCallback } = action.payload;

  const payload = {
    id: idEntity,
    body: entity
  };

  const { response } = yield call(putEntitySaga, payload, actions, typeEntity);
  if (response && successCallback) {
    yield call(successCallback);
  }
}

export function* createDummyBillSaga(action) {
  const { formData, helperData } = action.payload;

  try {
    const { client, successCallback } = helperData;
    const sum = formData.sum ? +formData.sum : 0;
    const contractId = formData.contract;

    const contract = helperData.client.contracts.find(
      contr => contr.id === contractId
    );

    const userId = yield select(authIdSelector);

    const payload = {
      body: {
        type: 3,
        contract_id: contractId,
        agent_id: client.id,
        firm_id: contract.executor_id,
        fact_sum: sum,
        bill_sum: sum,
        object_id_create: userId
      }
    };

    const { response } = yield call(postBillSaga, { payload });

    if (response && successCallback) {
      yield call(successCallback);
    }
  } catch (error) {
    return error;
  }
}

export function* updateBillSaga(action) {
  const actions = {
    success: UPDATE_BILL_SUCCESS
  };

  const payloadForApi = {
    ...action.payload,
    body: mapValuesToEntity(action.payload.entity, action.payload.values)
  };

  yield call(putEntitySaga, payloadForApi, actions, "bills");
}

function* createBillByFormSaga(action) {
  const { response } = yield call(
    postEntitiesByFormSaga,
    action.payload,
    null,
    "bills"
  );

  if (
    response.payload &&
    response.payload.id &&
    action.payload.helperValues.shouldMoveToEntityPage
  ) {
    yield put(push(getUrl("bills", { id: response.payload.id })));
  }
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_BILLS_REQUEST]: [fetchBillsSaga, moduleName],
      [FETCH_BILLS_FOR_ENTITY_REQUEST]: [fetchBillsForEntitySaga, moduleName],
      [FETCH_BILL_REQUEST]: [fetchBillSaga, moduleName],
      [FETCH_BILL_AND_CLIENT_REQUEST]: [fetchBillAndClientSaga, moduleName],
      [FETCH_BILLS_WITH_STATE_FOR_ENTITY_REQUEST]: [
        fetchBillsWithStateForEntitySaga,
        moduleName
      ],
      [FETCH_BILL_SUCCESS]: [fetchBillSuccessSaga, moduleName]
    }),
    takeEvery(
      CREATE_BILL_REQUEST,
      loggingRequestSaga,
      createBillSaga,
      moduleName
    ),
    takeEvery(POST_BILL_REQUEST, loggingRequestSaga, postBillSaga, moduleName),
    takeEvery(
      PUT_PREPARED_BILL_REQUEST,
      loggingRequestSaga,
      putPreparedBillSaga,
      moduleName
    ),
    takeEvery(
      CREATE_DUMMY_BILL_REQUEST,
      loggingRequestSaga,
      createDummyBillSaga,
      moduleName
    ),
    takeLatest(
      UPDATE_BILL_REQUEST,
      loggingRequestSaga,
      updateBillSaga,
      moduleName
    ),
    takeLatest(UPDATE_BILL_SUCCESS, responseToUpdateEntitySaga),
    takeEvery(
      CREATE_BILL_BY_FORM_REQUEST,
      loggingRequestSaga,
      createBillByFormSaga,
      moduleName
    )
  ]);
}
