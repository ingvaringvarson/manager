import {
  FETCH_BILL_REQUEST,
  FETCH_BILLS_REQUEST,
  FETCH_BILLS_FOR_ENTITY_REQUEST,
  POST_BILL_REQUEST,
  CREATE_BILL_REQUEST,
  FETCH_BILL_AND_CLIENT_REQUEST,
  FETCH_BILLS_WITH_STATE_FOR_ENTITY_REQUEST,
  PUT_PREPARED_BILL_REQUEST,
  UPDATE_BILL_REQUEST,
  CREATE_BILL_BY_FORM_REQUEST,
  CREATE_DUMMY_BILL_REQUEST
} from "./actions";
import {
  fetchEntities,
  fetchEntitiesForTypeEntity,
  fetchEntity,
  postEntityPrepared,
  putEntityPrepared,
  updateEntity,
  postEntity
} from "../../helpers/actionCreators";

export const fetchBill = fetchEntity(FETCH_BILL_REQUEST);

export const fetchBills = fetchEntities(FETCH_BILLS_REQUEST);

export const fetchBillsForEntity = fetchEntitiesForTypeEntity(
  FETCH_BILLS_FOR_ENTITY_REQUEST
);

export const fetchBillAndClient = fetchEntity(FETCH_BILL_AND_CLIENT_REQUEST);
export const fetchBillsWithStateForEntity = fetchEntitiesForTypeEntity(
  FETCH_BILLS_WITH_STATE_FOR_ENTITY_REQUEST
);

export const postBill = postEntityPrepared(POST_BILL_REQUEST);

export const createBill = (
  typeEntity = "",
  formData = {},
  values = [],
  normalizeCallback
) => {
  return {
    type: CREATE_BILL_REQUEST,
    payload: {
      typeEntity,
      formData,
      values,
      normalizeCallback
    }
  };
};

export const createDummyBill = (formData = {}, helperData = {}) => {
  return {
    type: CREATE_DUMMY_BILL_REQUEST,
    payload: {
      formData,
      helperData
    }
  };
};

export const putBillPrepared = putEntityPrepared(PUT_PREPARED_BILL_REQUEST);
export const updateBill = updateEntity(UPDATE_BILL_REQUEST);
export const createBillByForm = postEntity(CREATE_BILL_BY_FORM_REQUEST);
