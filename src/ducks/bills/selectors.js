import { moduleName } from "./config";
import {
  entityIdForRouteSelector,
  queryRouterSelector
} from "../../redux/selectors/router";
import { createSelector } from "reselect";
import { clientIdSelector, clientInfoByIdSelector } from "../../ducks/clients";
import {
  entityListSelector,
  maxPagesSelector as maxPagesSelectorHelper,
  typeEntityStateSelector as typeEntityStateSelectorHelper,
  entitiesForTypeEntitySelector,
  entityInfoSelector,
  typeEntitySelector,
  idEntitySelector
} from "../../helpers/selectors";

const billIdSelector = (state, props = { id: "" }) => props.id;
export const billListSelector = entityListSelector(moduleName);
export const maxPagesSelector = maxPagesSelectorHelper(moduleName);

export const typeEntityStateSelector = typeEntityStateSelectorHelper(
  moduleName
);

export const billsForEntitySelector = entitiesForTypeEntitySelector(moduleName);

export const billInfoSelector = entityInfoSelector(moduleName);

// requestParams
export const requestParamsByClientPageSelector = createSelector(
  queryRouterSelector,
  clientIdSelector,
  (query, id) => ({ page_size: 10, page: 1, agent_id: id, ...query })
);

export const requestParamsForOrderPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  ({ payment_method, ...query }, orderId) => {
    const stateQuery =
      +query._tab_state_bill === 2
        ? { state: [4] }
        : { _without_state: [0, 4] };

    return {
      order_id: orderId,
      ...query,
      ...stateQuery
    };
  }
);

// helpers
export const billsForEntityTabParamsSelector = createSelector(
  queryRouterSelector,
  query => {
    return {
      ...query,
      _tab: query._tab ? +query._tab : 1,
      _tab_state_bill: query._tab_state_bill ? +query._tab_state_bill : 1
    };
  }
);

export const clientInfoByBillSelector = (
  state,
  props = {
    billId: null
  }
) => {
  const bill = billInfoSelector(state, { id: props.billId });
  if (!bill) {
    return null;
  }
  const client = clientInfoByIdSelector(state, { id: bill.agent_id });
  return client;
};

export const billRequestParamsSelector = createSelector(
  queryRouterSelector,
  query => {
    return {
      ...query,
      _tab: query._tab ? +query._tab : 1
    };
  }
);

export const billPaymentsRequestParamsSelector = createSelector(
  queryRouterSelector,
  billIdSelector,
  (query, billId) => {
    return {
      page_size: 10,
      page: 1,
      ...query,
      bill_id: billId,
      payment_method: query.payment_method ? +query.payment_method : 1,
      _tab: query._tab ? +query._tab : 1
    };
  }
);

export const requestParamsForBillPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  (query, billId) => {
    return {
      id: billId,
      page: 1,
      page_size: 1,
      _tab: query._tab ? +query._tab : 1
    };
  }
);

export const billForMappingSelector = (
  state,
  props = {
    idEntity: "",
    bill: {}
  }
) => {
  const billFromStore = billInfoSelector(state, props);
  return Object.keys(billFromStore).length ? billFromStore : props.bill;
};

export const requestParamsForBillEntitySelector = createSelector(
  queryRouterSelector,
  typeEntitySelector,
  idEntitySelector,
  (query, typeEntity, idEntity) => {
    return {
      page: 1,
      page_size: 10,
      ...query,
      _tab_state_bill: query._tab_state_bill ? +query._tab_state_bill : 1,
      [typeEntity]: [idEntity]
    };
  }
);
