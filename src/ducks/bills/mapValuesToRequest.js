export function mapCreationForReturnPrepaymentValues(formValues, helperValues) {
  const { order, positions } = helperValues;
  return {
    type: 2,
    contract_id: order.contract_id,
    fact_sum: +formValues.fact_sum,
    bill_sum: +formValues.fact_sum,
    firm_id: order.firm_id,
    agent_id: order.agent_id,
    order_id: [order.id],
    positions: positions.map(p => ({
      position_id: p.id,
      sum: p.sum,
      order_id: order.id
    }))
  };
}

export function mapCreationForReturnPrepaidValues(formValues, helperValues) {
  const { agent, bill } = helperValues;
  const contract = agent.contracts.find(c => c.id === formValues.contract_id);
  return {
    type: 2,
    contract_id: formValues.contract_id,
    fact_sum: +formValues.sum,
    bill_sum: +formValues.sum,
    agent_id: agent.id,
    firm_id: contract.executor_id,
    order_id: bill.order_id
  };
}
