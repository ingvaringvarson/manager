import { call, all, takeEvery, put } from "redux-saga/effects";
import { PRINT_BARCODES_REQUEST } from "./actions";
import { loggingRequestSaga, fetchApiSaga, addNotification } from "../logger";
import { moduleName } from "./config";
import { printBarcodes } from "../../api/print";
import { generateId } from "../../utils";

export function* printBarcodesSaga(action) {
  const id = generateId();

  yield put(
    addNotification({
      id,
      loading: true,
      message: `Печать штрих-кодов (${action.payload.count} шт.)`
    })
  );

  const paramsRequest = printBarcodes(action.payload);
  const { response } = yield call(fetchApiSaga, paramsRequest);

  if (!response) {
    return;
  }

  yield put(
    addNotification({
      id,
      message: `Закончена печать штрих-кодов (${action.payload.count} шт.)`
    })
  );
}

export function* rootSaga() {
  yield all([
    takeEvery(
      PRINT_BARCODES_REQUEST,
      loggingRequestSaga,
      printBarcodesSaga,
      moduleName
    )
  ]);
}
