import { appName } from "../../config";

export const moduleName = "print";
export const prefix = `${appName}/${moduleName}`;
