import { PRINT_BARCODES_REQUEST } from "./actions";

export function printBarcodes(count) {
  return {
    type: PRINT_BARCODES_REQUEST,
    payload: { count }
  };
}
