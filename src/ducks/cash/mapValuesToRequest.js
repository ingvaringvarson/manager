export const getOutcomePaymentRequest = paramsObj =>
  getPaymentEntityPayload(3, paramsObj);

export const getIncomePaymentRequest = paramsObj =>
  getPaymentEntityPayload(4, paramsObj);

const dummyMapSaga = values => values;

const getPaymentEntityPayload = (payment_state, params) => {
  const { amount, firm_id, cashbox_id, cashbox_to_id, store_id } = params;

  const values = {
    sum: amount,
    payment_method: 1,
    payment_state,
    payment_place: 2,
    firm_id,
    cashbox_id,
    cashbox_to_id,
    store_id
  };

  return {
    payload: {
      values,
      mapSaga: dummyMapSaga
    }
  };
};
