import { ADD_RESULT, RUNNING_COMMAND, COMPLETED_COMMAND } from "./actions";

const StateRecord = {
  commandId: null,
  results: {}
};

function reducers(state = StateRecord, action) {
  const { type, payload } = action;

  switch (type) {
    case ADD_RESULT:
      return addResult(state, payload);
    case RUNNING_COMMAND:
      return runningCommand(state, payload);
    case COMPLETED_COMMAND:
      return completeCommand(state, payload);
    default:
      return state;
  }
}

function addResult(state, payload) {
  return {
    ...state,
    commandId: payload.id === state.commandId ? null : state.commandId,
    results: { ...state.result, [payload.id]: payload.result }
  };
}

function runningCommand(state, payload) {
  return {
    ...state,
    commandId: payload.id
  };
}

function completeCommand(state) {
  return {
    ...state,
    commandId: null
  };
}
export default reducers;
