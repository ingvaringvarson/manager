import {
  RECONCILIATION_TOTALS_REQUEST,
  CLOSE_SHIFT_REQUEST,
  DEPOSIT_REQUEST,
  GET_STATE_REQUEST,
  GET_STATISTICS_REQUEST,
  OPEN_SHIFT_REQUEST,
  PRINT_XREPORT_REQUEST,
  PRINT_ZREPORT_REQUEST,
  WITHDRAW_REQUEST,
  WITHDRAW_TO_CENTRAL_OFFICE_REQUEST,
  ADD_RESULT,
  CREATE_RETURN_PAYMENT_SCENARIO,
  CREATE_RETURN_PAYMENT_WITH_CHECK,
  CREATE_RETURN_PAYMENT_WITHOUT_CHECK,
  CREATE_PAYMENT_SCENARIO
} from "./actions";

export function openShift(id) {
  return {
    type: OPEN_SHIFT_REQUEST,
    payload: { id }
  };
}

export function closeShift(id) {
  return {
    type: CLOSE_SHIFT_REQUEST,
    payload: { id }
  };
}

export function printXreport(id) {
  return {
    type: PRINT_XREPORT_REQUEST,
    payload: { id }
  };
}

export function printZreport(id) {
  return {
    type: PRINT_ZREPORT_REQUEST,
    payload: { id }
  };
}

export function deposit(id, params, onClose) {
  return {
    type: DEPOSIT_REQUEST,
    payload: { id, params, onClose }
  };
}

export function withdraw(id, params, onClose) {
  return {
    type: WITHDRAW_REQUEST,
    payload: { id, params, onClose }
  };
}

export function reconciliationTotals(id) {
  return {
    type: RECONCILIATION_TOTALS_REQUEST,
    payload: { id }
  };
}

export function withdrawToCentralOffice(id, params, onClose) {
  return {
    type: WITHDRAW_TO_CENTRAL_OFFICE_REQUEST,
    payload: { id, params, onClose }
  };
}

export function getState() {
  return {
    type: GET_STATE_REQUEST
  };
}

export function getStatistics() {
  return {
    type: GET_STATISTICS_REQUEST
  };
}

export function addResultCommand(commandId, result) {
  return {
    type: ADD_RESULT,
    payload: {
      commandId,
      result
    }
  };
}

export function createPaymentScenario(params, id) {
  return {
    type: CREATE_PAYMENT_SCENARIO,
    payload: { params, id }
  };
}

export function createReturnPaymentScenario(params, id) {
  return {
    type: CREATE_RETURN_PAYMENT_SCENARIO,
    payload: { params, id }
  };
}

export function createReturnPaymentWithCheck(params, id) {
  return {
    type: CREATE_RETURN_PAYMENT_WITH_CHECK,
    payload: { params, id }
  };
}

export function createReturnPaymentWithoutCheck(params, id) {
  return {
    type: CREATE_RETURN_PAYMENT_WITHOUT_CHECK,
    payload: { params, id }
  };
}
