import { appName } from "../../config";

export const moduleName = "cash";
export const prefix = `${appName}/${moduleName}`;
