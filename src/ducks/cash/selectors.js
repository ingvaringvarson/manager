import { moduleName } from "./config";

const stateSelector = state => state[moduleName];

export const successResultsSelector = (state, props) => {
  return stateSelector(state, props).successResults;
};

export const isDisabledSelector = (state, props) => {
  return stateSelector(state, props).commandId !== null;
};
