import { getValueInDepth, generateId } from "./../../utils";
import { userFirmAndWarehouseSelector } from "./../user/selectors";
import {
  getOutcomePaymentRequest,
  getIncomePaymentRequest
} from "./mapValuesToRequest";
import { put, call, select, all, takeLeading, delay } from "redux-saga/effects";
import {
  COMPLETED_COMMAND,
  RUNNING_COMMAND,
  PRINT_ZREPORT_REQUEST,
  PRINT_XREPORT_REQUEST,
  OPEN_SHIFT_REQUEST,
  DEPOSIT_REQUEST,
  CLOSE_SHIFT_REQUEST,
  RECONCILIATION_TOTALS_REQUEST,
  WITHDRAW_REQUEST,
  WITHDRAW_TO_CENTRAL_OFFICE_REQUEST,
  CREATE_RETURN_PAYMENT_WITHOUT_CHECK,
  CREATE_RETURN_PAYMENT_WITH_CHECK,
  CREATE_RETURN_PAYMENT_SCENARIO,
  CREATE_PAYMENT_SCENARIO
} from "./actions";
import { moduleName } from "./config";
import {
  printXreport,
  closeShift,
  deposit,
  openShift,
  printZreport,
  reconciliationTotals,
  withdraw,
  getScenarioInfo,
  createPaymentScenario,
  createReturnPaymentScenario,
  createReturnPaymentWithCheck,
  createReturnPaymentWithoutCheck
} from "../../api/cash";
import { addResultCommand } from "./actionCreators";
import {
  fetchApiSaga,
  addNotification,
  removeNotification,
  loggingRequestSaga,
  watchingTwinActionsSaga
} from "../logger";
import { fetchCurrentWarehouseInfoSaga } from "../warehouses";
import { createPaymentSaga } from "../payments/sagas/payments";
import { getActionPayload } from "../../helpers/mapValuesToRequest";

const kkmCommandApiMethods = {
  [PRINT_ZREPORT_REQUEST]: printZreport,
  [PRINT_XREPORT_REQUEST]: printXreport,
  [OPEN_SHIFT_REQUEST]: openShift,
  [DEPOSIT_REQUEST]: deposit,
  [CLOSE_SHIFT_REQUEST]: closeShift,
  [RECONCILIATION_TOTALS_REQUEST]: reconciliationTotals,
  [WITHDRAW_REQUEST]: withdraw,
  [CREATE_RETURN_PAYMENT_SCENARIO]: createReturnPaymentScenario,
  [CREATE_PAYMENT_SCENARIO]: createPaymentScenario,
  [CREATE_RETURN_PAYMENT_WITH_CHECK]: createReturnPaymentWithCheck,
  [CREATE_RETURN_PAYMENT_WITHOUT_CHECK]: createReturnPaymentWithoutCheck
};

export function* fetchKkmCommandSaga(action) {
  const { id, params, onClose } = action.payload;

  if (id) {
    yield put({
      type: RUNNING_COMMAND,
      payload: { id }
    });
  }

  const apiMethod = kkmCommandApiMethods[action.type];
  const paramsRequest = apiMethod(action.payload && params);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (response && response.payload && id) {
    if (onClose) {
      yield call(onClose);
    }

    return yield call(fetchScenarioInfoSaga, {
      payload: { id: response.payload.scenarioId, commandId: id }
    });
  } else {
    yield put({
      type: COMPLETED_COMMAND
    });

    return { response, error };
  }
}

function* fetchScenarioInfoSaga(action) {
  const paramsRequest = getScenarioInfo(action.payload.id);
  let status = 1;
  let result = {};

  while (status === 1 || status === 2) {
    const { response, error } = yield call(fetchApiSaga, paramsRequest);
    result = { response, error };

    if (response && response.payload) {
      switch (response.payload.status) {
        case 1: {
          status = 1;
          yield put(
            addNotification({
              id: response.payload.scenarioId,
              loading: true,
              message: "Операция в очереди"
            })
          );
          yield delay(3000);
          break;
        }
        case 2: {
          status = 2;
          yield put(
            addNotification({
              id: response.payload.scenarioId,
              loading: true,
              message: "Операция выполняется"
            })
          );
          yield delay(3000);
          break;
        }
        case 3: {
          status = 3;
          yield put(
            addNotification({
              id: response.payload.scenarioId,
              message: response.payload.hasError
                ? response.payload.message
                : "Операция выполнена"
            })
          );
          if (action.payload.commandId) {
            yield put(
              addResultCommand({
                commandId: action.payload.commandId,
                result: response.payload
              })
            );
          }
          break;
        }
        case 0: {
          status = 0;
          yield put(
            addNotification({
              id: response.payload.scenarioId,
              message: "Ошибка при выполнении операции"
            })
          );
          break;
        }
        default: {
          status = null;
        }
      }
    } else {
      status = null;
    }
  }

  yield put({
    type: COMPLETED_COMMAND
  });

  return result;
}

function* getPaymentRequestParams(amount) {
  const { userFirmId, userWarehouseId } = yield select(
    userFirmAndWarehouseSelector
  );

  const getWarehouseInfoAction = paramsByWarehouseRequest =>
    getActionPayload({
      userFirmId,
      userWarehouseId,
      paramsByWarehouseRequest
    });

  const warehouseInfoFromAction = getWarehouseInfoAction({
    id: userWarehouseId
  });
  const warehouseInfoFrom = yield call(
    fetchCurrentWarehouseInfoSaga,
    warehouseInfoFromAction
  );

  if (!warehouseInfoFrom.id) {
    return warehouseInfoFrom;
  }

  const warehouseInfoToAction = getWarehouseInfoAction({
    type: [1],
    firm_id: userFirmId
  });
  const warehouseInfoTo = yield call(
    fetchCurrentWarehouseInfoSaga,
    warehouseInfoToAction
  );

  if (!warehouseInfoTo.id) {
    return warehouseInfoTo;
  }

  const getCashboxId = warehouseInfo =>
    getValueInDepth(warehouseInfo, ["cashbox", 0, "id"]);

  return {
    amount,
    firm_id: userFirmId,
    cashbox_id: getCashboxId(warehouseInfoFrom),
    cashbox_to_id: getCashboxId(warehouseInfoTo),
    store_id: userWarehouseId
  };
}

function* withdrawToCentralOfficeSaga(action) {
  const {
    params: { amount },
    onClose
  } = action.payload;

  const startNotification = {
    id: generateId(),
    loading: true,
    message: "Операция выполняется"
  };

  yield put(addNotification(startNotification));

  const paymentRequestParams = yield call(getPaymentRequestParams, amount);

  // перемещение денежных средств Расход
  let nextParams = getOutcomePaymentRequest(paymentRequestParams);
  const paymentResult = yield call(createPaymentSaga, nextParams);
  const payment = getValueInDepth(paymentResult, ["response", "payload"]);

  if (!payment) {
    return yield put(
      addNotification({
        message: "Ошибка при перемещении денежных средств Расход"
      })
    );
  }

  // изъятия денежных средств
  nextParams = {
    type: WITHDRAW_REQUEST,
    payload: {
      params: {
        amount
      }
    }
  };

  const withdrawResponse = yield call(fetchKkmCommandSaga, nextParams);
  const withdrawResult = getValueInDepth(withdrawResponse, [
    "response",
    "payload"
  ]);

  if (!withdrawResult) {
    return yield put(
      addNotification({
        message: "Ошибка при изъятии денежных средств из кассы"
      })
    );
  }

  // перемещение денежных средств Приход
  nextParams = getIncomePaymentRequest(paymentRequestParams);
  const incomePaymentRequest = yield call(createPaymentSaga, nextParams);

  yield put(removeNotification(startNotification));

  if (incomePaymentRequest && !incomePaymentRequest.error) {
    if (onClose) {
      yield call(onClose);
    }

    return yield put(
      addNotification({
        message: "Операция выполнена"
      })
    );
  }

  return incomePaymentRequest;
}

const kkmCommands = [
  PRINT_ZREPORT_REQUEST,
  PRINT_XREPORT_REQUEST,
  OPEN_SHIFT_REQUEST,
  DEPOSIT_REQUEST,
  CLOSE_SHIFT_REQUEST,
  RECONCILIATION_TOTALS_REQUEST,
  WITHDRAW_REQUEST,
  CREATE_PAYMENT_SCENARIO,
  CREATE_RETURN_PAYMENT_SCENARIO,
  CREATE_RETURN_PAYMENT_WITH_CHECK,
  CREATE_RETURN_PAYMENT_WITHOUT_CHECK
];

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [WITHDRAW_TO_CENTRAL_OFFICE_REQUEST]: [
        withdrawToCentralOfficeSaga,
        moduleName
      ]
    }),
    takeLeading(
      kkmCommands,
      loggingRequestSaga,
      fetchKkmCommandSaga,
      moduleName
    )
  ]);
}
