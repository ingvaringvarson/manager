import { combineReducers } from "redux";
import list from "./list";
import listForEntity from "./listForEntity";
import groupsForAccount from "./groupsForAccount";

export default combineReducers({
  list,
  listForEntity,
  groupsForAccount
});
