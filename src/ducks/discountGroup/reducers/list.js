import {
  FETCH_DISCOUNT_GROUPS_SUCCESS,
  FETCH_DISCOUNT_GROUPS_ERROR
} from "../actions";
import {
  ListStateRecord,
  fetchEntitiesSuccess,
  fetchEntitiesError
} from "../../../helpers/reducers";

const listReducer = (state = { ...ListStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_DISCOUNT_GROUPS_SUCCESS:
      return fetchEntitiesSuccess(state, payload);
    case FETCH_DISCOUNT_GROUPS_ERROR:
      return fetchEntitiesError(state, payload);
    default:
      return state;
  }
};

export default listReducer;
