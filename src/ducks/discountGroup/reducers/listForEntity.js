import {
  FETCH_DISCOUNT_GROUPS_FOR_ENTITY_SUCCESS,
  FETCH_DISCOUNT_GROUPS_FOR_ENTITY_ERROR
} from "../actions";
import {
  EmptyStateRecord,
  fetchEntitiesForTypeEntityError,
  fetchEntitiesForTypeEntitySuccess
} from "../../../helpers/reducers";

const listForEntityReducer = (state = { ...EmptyStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_DISCOUNT_GROUPS_FOR_ENTITY_SUCCESS:
      return fetchEntitiesForTypeEntitySuccess(state, payload);
    case FETCH_DISCOUNT_GROUPS_FOR_ENTITY_ERROR:
      return fetchEntitiesForTypeEntityError(state, payload);
    default:
      return state;
  }
};

export default listForEntityReducer;
