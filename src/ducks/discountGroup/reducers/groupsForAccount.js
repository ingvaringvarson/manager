import {
  FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_SUCCESS,
  FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_ERROR
} from "../actions";

export default (state = {}, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_SUCCESS:
      return {
        ...state,
        [payload.idEntity]: {
          ...payload.apiData
        }
      };
    case FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_ERROR:
      return {
        ...state,
        [payload.idEntity]: {
          ...payload.apiData
        }
      };
    default:
      return state;
  }
};
