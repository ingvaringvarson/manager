import { createSelector } from "reselect";
import { moduleName } from "./config";
import {
  entitiesForTypeEntitySelector,
  entityListSelector,
  stateSelector
} from "../../helpers/selectors";

const emptyObj = {};

export const discountGroupListSelector = entityListSelector(moduleName);

export const groupsForAccountSelector = state => {
  return stateSelector(moduleName)(state).groupsForAccount;
};
const groupsForAccountByIdSelector = (state, { idEntity }) => {
  return groupsForAccountSelector(state)[idEntity] || emptyObj;
};
export const discountGroupIdForAccountSelector = createSelector(
  groupsForAccountByIdSelector,
  apiData => {
    return apiData.discountGroupId;
  }
);
export const discountGroupIdForAccountByListSelector = createSelector(
  discountGroupListSelector,
  discountGroupIdForAccountSelector,
  (list, discountGroupId) => {
    if (
      !(list && Array.isArray(list) && list.length) ||
      (!discountGroupId && discountGroupId !== 0)
    ) {
      return null;
    }
    return list.find(el => el.discountGroupId === discountGroupId);
  }
);

export const discountGroupListForAccountSelector = entitiesForTypeEntitySelector(
  moduleName
);
