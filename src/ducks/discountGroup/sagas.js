import { all, call, put, takeEvery, select } from "redux-saga/effects";
import {
  FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_REQUEST,
  FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_SUCCESS,
  FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_ERROR,
  FETCH_DISCOUNT_GROUPS_REQUEST,
  FETCH_DISCOUNT_GROUPS_SUCCESS,
  FETCH_DISCOUNT_GROUPS_ERROR,
  CHANGE_DISCOUNT_GROUPS_FOR_ACCOUNT_REQUEST,
  CHANGE_DISCOUNT_GROUPS_FOR_ACCOUNT_SUCCESS,
  CHANGE_DISCOUNT_GROUPS_FOR_ACCOUNT_ERROR
} from "./actions";
import { loggingRequestSaga, watchingTwinActionsSaga } from "../logger";
import {
  fetchEntitiesCommon,
  fetchDataCommonById,
  postCommonSaga
} from "../../helpers/sagas";
import { moduleName } from "./config";

import { discountGroupListSelector } from "./selectors";
import { fetchClient } from "../clients";

export function* fetchDiscountGroupsSaga() {
  const groups = yield select(discountGroupListSelector);
  // if length > 0 it means that groups are loaded
  if (groups.length) return null;

  const actions = {
    success: FETCH_DISCOUNT_GROUPS_SUCCESS,
    error: FETCH_DISCOUNT_GROUPS_ERROR
  };

  yield call(fetchEntitiesCommon, actions, "discountGroups", "discountGroups");
}

export function* fetchDiscountGroupsForAccountSaga(action) {
  const { clientId } = action.payload;
  const actions = {
    success: FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_SUCCESS,
    error: FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_ERROR
  };

  return yield call(
    fetchDataCommonById,
    actions,
    clientId,
    "discountGroups/forAccount"
  );
}

export function* changeDiscountGroupSaga(
  action = {
    payload: {
      formValues: {},
      clientId: "",
      restValues: {}
    }
  }
) {
  // ToDo: use mapValuesToRequest
  const { formValues, clientId, restValues } = action.payload;
  const actions = {
    success: CHANGE_DISCOUNT_GROUPS_FOR_ACCOUNT_SUCCESS,
    error: CHANGE_DISCOUNT_GROUPS_FOR_ACCOUNT_ERROR
  };
  const discountGroup = +formValues.discountGroup;

  const payloadForApi = {
    body: {
      agentId: clientId,
      discountGroupId: +discountGroup
    }
  };

  yield call(
    postCommonSaga,
    payloadForApi,
    actions,
    "discountGroups/forAccount"
  );

  yield put(fetchClient(restValues.entity.id, { id: restValues.entity.id }));
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_DISCOUNT_GROUPS_REQUEST]: [fetchDiscountGroupsSaga, moduleName],
      [FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_REQUEST]: [
        fetchDiscountGroupsForAccountSaga,
        moduleName
      ]
    }),
    takeEvery(
      CHANGE_DISCOUNT_GROUPS_FOR_ACCOUNT_REQUEST,
      loggingRequestSaga,
      changeDiscountGroupSaga,
      moduleName
    )
  ]);
}
