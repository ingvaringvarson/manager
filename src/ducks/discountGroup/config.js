import { appName } from "../../config";

export const moduleName = `discountGroups`;
export const prefix = `${appName}/${moduleName}`;
