import {
  FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_REQUEST,
  CHANGE_DISCOUNT_GROUPS_FOR_ACCOUNT_REQUEST,
  FETCH_DISCOUNT_GROUPS_REQUEST
} from "./actions";

export const fetchDiscountGroups = () => {
  return {
    type: FETCH_DISCOUNT_GROUPS_REQUEST
  };
};

export const fetchDiscountGroupsForAccount = clientId => {
  return {
    type: FETCH_DISCOUNT_GROUPS_FOR_ACCOUNT_REQUEST,
    payload: { clientId }
  };
};

export const changeDiscountGroupsForAccount = (
  formValues = {},
  clientId = null,
  restValues = {}
) => {
  return {
    type: CHANGE_DISCOUNT_GROUPS_FOR_ACCOUNT_REQUEST,
    payload: {
      formValues,
      clientId,
      restValues
    }
  };
};
