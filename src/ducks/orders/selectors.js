import { moduleName } from "./config";
import {
  queryRouterSelector,
  entityIdForRouteSelector,
  queryListWithoutServiceQueryRouterSelector
} from "../../redux/selectors/router";
import { createSelector } from "reselect";
import {
  entityListSelector,
  maxPagesSelector as maxPagesSelectorHelper,
  typeEntityStateSelector as typeEntityStateSelectorHelper,
  entitiesForTypeEntitySelector,
  entityInfoSelector,
  pageModByDefaultValuesSelector
} from "../../helpers/selectors";
import { employeeInfoByUserSelector } from "../user";
import { maskDateBeginningDay, maskDateEndDay } from "../../helpers/masks";
import { generateId, entitiesToOptions } from "../../utils";

const positionIdSelector = (state, props = { positionId: null }) =>
  props.positionId;
const emptyArr = [];

export const orderListSelector = entityListSelector(moduleName);
export const maxPagesSelector = maxPagesSelectorHelper(moduleName);

export const typeEntityStateSelector = typeEntityStateSelectorHelper(
  moduleName
);

export const ordersForEntitySelector = entitiesForTypeEntitySelector(
  moduleName
);

export const ordersWithBillIdForEntitySelector = createSelector(
  ordersForEntitySelector,
  ({ entities, ...rest }) => {
    return {
      ...rest,
      entities: entities.map(({ order, bill_id, ...restProps }) => ({
        ...order,
        bill_id,
        ...restProps
      }))
    };
  }
);

export const orderInfoSelector = entityInfoSelector(moduleName);
export const orderIdForRouteSelector = entityIdForRouteSelector;

export const orderPositionByIdArray = createSelector(
  orderInfoSelector,
  positionIdSelector,
  (orderData, positionId) => {
    if (
      !orderData ||
      !(orderData.order && Array.isArray(orderData.order.positions))
    ) {
      return emptyArr;
    }

    const { order } = orderData;
    return order.positions.filter(pos => pos.id === positionId);
  }
);

export const orderInfoWithBillIdSelector = createSelector(
  orderInfoSelector,
  info => {
    if (!info) return null;
    const { order, bill_id, ...restProps } = info;

    return { ...order, bill_id, ...restProps };
  }
);

export const orderListWithBillIdSelector = createSelector(
  orderListSelector,
  entities => {
    return entities.map(({ order, bill_id, ...restProps }) => ({
      ...order,
      bill_id,
      ...restProps
    }));
  }
);

export const requestParamsByClientPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  (query, id) => ({
    page_size: 10,
    page: 1,
    agent_id: id,
    sort_by: "object_date_create desc",
    ...query
  })
);

export const requestParamsOrderPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  (query, orderId) => {
    return {
      page_size: 10,
      page: 1,
      order_id: orderId,
      ...query,
      _tab: query._tab ? +query._tab : 1
    };
  }
);
export const requestParamsOrderBillsPageSelector = createSelector(
  requestParamsOrderPageSelector,
  query => {
    return {
      ...query,
      billState: query.billState ? +query.billState : 1
    };
  }
);

export const orderChangedSelector = createSelector(
  orderInfoSelector,
  () => generateId()
);

export const ordersToOptionsSelector = (getName, getId) => (state, props) => {
  const { entities = [] } = ordersForEntitySelector(state, props);
  return entitiesToOptions(entities, getName, getId);
};

export const orderEntitiesSelectorByStoreIdAndOwnId = (
  state,
  props = {
    storeId: "",
    orderId: ""
  }
) => {
  const entitiesInStore = ordersForEntitySelector(state, {
    idEntity: props.storeId,
    typeEntity: "orders"
  }).entities;
  return entitiesInStore;
};
export const orderIdSelector = (state, props = { orderId: "" }) => {
  return props.orderId;
};

export const orderSelectorByStoreIdAndOwnId = createSelector(
  orderEntitiesSelectorByStoreIdAndOwnId,
  orderIdSelector,
  (entities, orderId) => {
    const entityInStore = entities.find(entity => entity.order.id === orderId);
    return entityInStore ? entityInStore.order : null;
  }
);

export const defaultValuesForFieldsSelector = createSelector(
  employeeInfoByUserSelector,
  queryListWithoutServiceQueryRouterSelector,
  pageModByDefaultValuesSelector,
  (employee, queryList, pageMod) => {
    const defaultParams = {};

    if (queryList.length) return defaultParams;

    const currentDay = Date.now();

    defaultParams.datefrom_created = maskDateBeginningDay(
      currentDay - 1209600000
    );
    defaultParams.dateto_created = maskDateEndDay(currentDay);

    if (employee) {
      if (employee.warehouse_id && pageMod) {
        defaultParams.warehouse_id = employee.warehouse_id;
      }
      if (employee.firm_id) {
        defaultParams.firm_id = employee.firm_id;
      }
    }

    return defaultParams;
  }
);
