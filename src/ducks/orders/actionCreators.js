import {
  FETCH_ORDER_REQUEST,
  FETCH_ORDERS_REQUEST,
  FETCH_ORDERS_FOR_ENTITY_REQUEST,
  FETCH_ORDER_WITH_CLIENT_REQUEST,
  ADD_ORDER_POSITIONS_REQUEST,
  UPDATE_ORDER_REQUEST,
  UPDATE_ORDER_WITH_ENTITY_REQUEST,
  CREATE_ORDER_REQUEST,
  UPDATE_ORDER_POSITIONS_REQUEST,
  CHANGE_CLIENT_REQUEST
} from "./actions";
import {
  fetchEntities,
  fetchEntitiesForTypeEntity,
  fetchEntity,
  updateEntity,
  createEntity,
  putEntity
} from "../../helpers/actionCreators";

export const fetchOrder = fetchEntity(FETCH_ORDER_REQUEST);

export const fetchOrders = fetchEntities(FETCH_ORDERS_REQUEST);

export const fetchOrdersForEntity = fetchEntitiesForTypeEntity(
  FETCH_ORDERS_FOR_ENTITY_REQUEST
);

export const updateOrderWithEntity = updateEntity(
  UPDATE_ORDER_WITH_ENTITY_REQUEST
);

export const fetchOrderWithClient = orderId => {
  return {
    type: FETCH_ORDER_WITH_CLIENT_REQUEST,
    isForRepeat: true,
    payload: { orderId }
  };
};

export const addOrderPositions = putEntity(ADD_ORDER_POSITIONS_REQUEST);

export const updateOrder = updateEntity(UPDATE_ORDER_REQUEST);

export const createOrder = createEntity(CREATE_ORDER_REQUEST);

export const updateOrderPositions = updateEntity(
  UPDATE_ORDER_POSITIONS_REQUEST
);
export const changeClient = putEntity(CHANGE_CLIENT_REQUEST);
