import { appName } from "../../config";

export const moduleName = "orders";
export const prefix = `${appName}/${moduleName}`;
