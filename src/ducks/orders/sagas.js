import {
  all,
  takeLatest,
  call,
  takeEvery,
  select,
  fork,
  put
} from "redux-saga/effects";
import {
  FETCH_ORDERS_SUCCESS,
  FETCH_ORDERS_REQUEST,
  FETCH_ORDERS_ERROR,
  FETCH_ORDERS_FOR_ENTITY_REQUEST,
  FETCH_ORDERS_FOR_ENTITY_ERROR,
  FETCH_ORDERS_FOR_ENTITY_SUCCESS,
  FETCH_ORDER_SUCCESS,
  FETCH_ORDER_REQUEST,
  FETCH_ORDER_WITH_CLIENT_REQUEST,
  FETCH_ORDER_ERROR,
  ADD_ORDER_POSITIONS_REQUEST,
  ADD_ORDER_POSITIONS_SUCCESS,
  ADD_ORDER_POSITIONS_ERROR,
  UPDATE_ORDER_REQUEST,
  UPDATE_ORDER_SUCCESS,
  CREATE_ORDER_REQUEST,
  CREATE_ORDER_SUCCESS,
  UPDATE_ORDER_POSITIONS_REQUEST,
  UPDATE_ORDER_POSITIONS_SUCCESS,
  UPDATE_ORDER_WITH_ENTITY_REQUEST,
  UPDATE_ORDER_CLIENT_REQUEST,
  CHANGE_CLIENT_REQUEST
} from "./actions";
import { loggingRequestSaga, watchingTwinActionsSaga } from "../logger";
import {
  fetchEntitiesForTypeEntitySaga,
  fetchEntitiesSaga,
  fetchEntitySaga,
  putEntitiesSaga,
  putEntitySaga,
  postEntitySaga
} from "../../helpers/sagas";
import { mapRequestParamsToParamsByApiSaga } from "../../helpers/mapMethods";
import { moduleName } from "./config";
import {
  getEntitiesCommon,
  putEntities
} from "../../api/requestByTypeEntities";
import { fetchApiSaga, addNotification } from "../logger";

import { fetchClient } from "../clients";
import { orderInfoWithBillIdSelector } from "./selectors";
import { fetchOrder } from "./actionCreators";

import { mapValuesToEntity } from "../../helpers/dataMapping";

import { push, replace } from "connected-react-router";
import { getUrl } from "../../helpers/urls";
import { repeatLastActionSaga } from "../lastAction";
import { generateUrlWithQuery } from "../../utils";

export function* fetchOrdersSaga(action) {
  const actions = {
    success: FETCH_ORDERS_SUCCESS,
    error: FETCH_ORDERS_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "orders"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  return yield call(
    fetchEntitiesSaga,
    payloadForApi,
    actions,
    "orders",
    "orders_with_bills"
  );
}

export function* fetchOrdersForEntitySaga(action) {
  const actions = {
    success: FETCH_ORDERS_FOR_ENTITY_SUCCESS,
    error: FETCH_ORDERS_FOR_ENTITY_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "orders"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  return yield call(
    fetchEntitiesForTypeEntitySaga,
    payloadForApi,
    actions,
    "orders",
    "orders_with_bills"
  );
}

export function* fetchOrderSaga(action) {
  const actions = {
    success: FETCH_ORDER_SUCCESS,
    error: FETCH_ORDER_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "orders"
  );

  return yield call(
    fetchEntitySaga,
    {
      id: action.payload.id,
      params
    },
    actions,
    "orders",
    "orders_with_bills"
  );
}

export function* fetchOrderWithClientSaga(action) {
  yield call(fetchOrderSaga, {
    payload: {
      id: action.payload.orderId,
      params: { id: action.payload.orderId }
    }
  });

  const order = yield select(orderInfoWithBillIdSelector, {
    id: action.payload.orderId
  });

  if (order) {
    yield put(fetchClient(order.agent_id, { id: order.agent_id }));
  }
}

export function* addOrderPositionsSaga(
  action = {
    payload: {
      formValues: {},
      helperValues: {
        orders: []
      }
    }
  }
) {
  const actions = {
    success: ADD_ORDER_POSITIONS_SUCCESS,
    error: ADD_ORDER_POSITIONS_ERROR
  };

  const { formValues, helperValues, normalizeCallback } = action.payload;

  if (!normalizeCallback) {
    throw "normalizeCallback обязательный параметр для addOrderPositions";
  }

  const requestParamsArray = normalizeCallback(formValues, helperValues.orders);

  const effects = [];
  for (const body of requestParamsArray) {
    const effectsResult = yield call(
      postEntitySaga,
      { body },
      actions,
      "orderPositions"
    );
    effects.push(effectsResult);
  }

  if (effects.some(e => !!e.response)) {
    yield fork(repeatLastActionSaga);
  }

  return effects;
}

export function* updateOrderSaga(action) {
  const actions = {
    success: UPDATE_ORDER_SUCCESS
  };
  const { mapSaga, entity, values, ...props } = action.payload;

  const body = yield call(
    mapSaga ? mapSaga : mapValuesToEntity,
    entity,
    values
  );

  const payloadForApi = {
    ...props,
    body
  };

  return yield call(putEntitySaga, payloadForApi, actions, "orders");
}

function* updateOrderWithEntitySaga(
  action = {
    payload: {
      values: {},
      entity: {},
      id: ""
    }
  }
) {
  const actions = {
    success: UPDATE_ORDER_SUCCESS
  };

  const { entity, values, id } = action.payload;

  const newOrder = { ...entity, ...values };

  yield call(
    putEntitiesSaga,
    {
      id,
      body: { order: newOrder }
    },
    actions,
    "orders"
  );

  yield put(fetchOrder(id, { id }));
}

export function* createOrderSaga(action) {
  const actions = {
    success: CREATE_ORDER_SUCCESS
  };

  const { mapSaga, values, ...props } = action.payload;

  const body = yield call(mapSaga ? mapSaga : mapValuesToEntity, values);

  const payloadForApi = {
    ...props,
    body
  };

  return yield call(postEntitySaga, payloadForApi, actions, "orders");
}

function* responseToCreationOrderSaga(action) {
  if (!action.payload || !action.payload.successParams) return;

  if (action.payload.successParams.moveToEntityPage && action.payload.id) {
    yield put(
      push(getUrl("orders", { id: action.payload.id }), { isLoaded: true })
    );
  }
}

function* responseToUpdateOrderSaga(action) {
  if (!action.payload || !action.payload.successParams) return;

  if (action.payload.successParams.moveToEntityPage && action.payload.id) {
    yield put(replace(getUrl("orders", { id: action.payload.id })));
  }
}

export function* updateOrderPositionsSaga(action) {
  const actions = {
    success: UPDATE_ORDER_POSITIONS_SUCCESS
  };

  const { formValues, helperValues, normalizeCallback } = action.payload;

  const payload = normalizeCallback
    ? normalizeCallback(formValues, helperValues)
    : { body: formValues };

  const result = yield call(postEntitySaga, payload, actions, "orderPositions");

  if (
    result &&
    result.response &&
    result.response.payload &&
    helperValues &&
    helperValues.fetchEntities
  ) {
    helperValues.fetchEntities();
  }

  return result;
}

function* clientChangeSaga(action) {
  const {
    formValues,
    helperValues: { selectedClient, order, successCallback }
  } = action.payload;

  const getRequestParams = getEntitiesCommon(
    generateUrlWithQuery({
      pathname: `orders/agent/${order.id}/${selectedClient.id}`,
      newQuery: {
        contractId: formValues.contract_id
      }
    })
  );
  const { response, error } = yield call(fetchApiSaga, getRequestParams);

  if (error) {
    yield call(addNotification, error);
  }

  if (response) {
    const putRequestParams = putEntities(`orders/agent/${order.id}`, {
      agentId: formValues.new_agent_id,
      contractId: formValues.contract_id
    });
    const { response: putResponse } = yield call(
      fetchApiSaga,
      putRequestParams
    );

    if (putResponse) {
      yield call(successCallback);
    }
  }
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_ORDERS_REQUEST]: [fetchOrdersSaga, moduleName],
      [FETCH_ORDERS_FOR_ENTITY_REQUEST]: [fetchOrdersForEntitySaga, moduleName],
      [FETCH_ORDER_REQUEST]: [fetchOrderSaga, moduleName],
      [FETCH_ORDER_WITH_CLIENT_REQUEST]: [fetchOrderWithClientSaga, moduleName]
    }),
    takeEvery(
      ADD_ORDER_POSITIONS_REQUEST,
      loggingRequestSaga,
      addOrderPositionsSaga,
      moduleName
    ),
    takeEvery(
      UPDATE_ORDER_REQUEST,
      loggingRequestSaga,
      updateOrderSaga,
      moduleName
    ),
    takeEvery(
      UPDATE_ORDER_WITH_ENTITY_REQUEST,
      loggingRequestSaga,
      updateOrderWithEntitySaga,
      moduleName
    ),
    takeEvery(
      UPDATE_ORDER_POSITIONS_REQUEST,
      loggingRequestSaga,
      updateOrderPositionsSaga,
      moduleName
    ),
    takeLatest(UPDATE_ORDER_SUCCESS, responseToUpdateOrderSaga),
    takeEvery(
      CREATE_ORDER_SUCCESS,
      loggingRequestSaga,
      responseToCreationOrderSaga,
      moduleName
    ),
    takeEvery(
      CREATE_ORDER_REQUEST,
      loggingRequestSaga,
      createOrderSaga,
      moduleName
    ),
    takeEvery(
      UPDATE_ORDER_CLIENT_REQUEST,
      loggingRequestSaga,
      createOrderSaga,
      moduleName
    ),
    takeEvery(
      CHANGE_CLIENT_REQUEST,
      loggingRequestSaga,
      clientChangeSaga,
      moduleName
    )
  ]);
}
