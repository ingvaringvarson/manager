import { call } from "redux-saga/effects";
import deliveryPrice from "../../constants/deliveryPrice";
import { updateOrderPositionsSaga } from "./sagas";
import { generateId } from "../../utils";
import { fetchKladrInfoSaga } from "../kladr";

export function mapOrderPositionSpecialData(formValues = {}, position = {}) {
  return function(resultFromView = {}, valueKey = "") {
    const [posId] = valueKey.split("__");

    const result = { ...resultFromView };

    delete result._id;
    delete result._index;

    if (position._id !== posId) {
      return result;
    }

    switch (valueKey) {
      case `discount__${posId}`:
        const dicsValue = +formValues[valueKey];
        if (formValues[`discountValue__${posId}`] === "rub") {
          result["discount"] = dicsValue;
          result["discount_percent"] = null;
        } else if (formValues[`discountValue__${posId}`] === "percent") {
          result["discount_percent"] = dicsValue;
          result["discount"] = null;
        }
        return result;
      case `reject__${posId}`:
        const currentCount = result["count"];
        let rejectCount = +formValues[valueKey];
        rejectCount =
          currentCount > 0 && rejectCount > 0
            ? currentCount > rejectCount
              ? rejectCount
              : currentCount
            : null;

        if (rejectCount) {
          result["count"] = rejectCount;
          result["good_state"] = 10; // Отказ клиента
        }
      case `discountValue__${posId}`:
      case `count__${posId}`:
        return result;
      default:
        result[valueKey] = formValues[valueKey];
        return result;
    }
  };
}

export function mapOrderPositionsDiscount(formValues = {}, order = {}) {
  const discValue = formValues.discountValue;

  if (discValue === "percent") {
    return order.positions.map(pos => ({
      ...pos,
      discount_percent: formValues.discount,
      discount: null
    }));
  } else if (discValue === "rub") {
    let discountDecreasingSum = +formValues.discount;

    const positionsCount = order.positions.length;
    const getPositionSum = p => p.price * p.count;
    const bySumAsc = (p1, p2) => getPositionSum(p1) - getPositionSum(p2);

    // разбиваем скидку на все позиции поровну начиная
    // с той, у которой наименьшая сумма
    const getPositionDiscount = (positionSum, currentIndex) => {
      if (order.positions.length === currentIndex - 1) {
        return discountDecreasingSum;
      }

      const onePositionDiscountClean =
        discountDecreasingSum / (positionsCount - currentIndex);
      const onePositionDiscount = Math.floor(onePositionDiscountClean);

      const disc =
        onePositionDiscount > positionSum ? positionSum : onePositionDiscount;

      discountDecreasingSum -= disc;

      return disc;
    };

    return order.positions.sort(bySumAsc).map((position, i) => {
      if (position.product.type === 5) {
        // доставка
        return {
          ...position,
          discount: null,
          discount_percent: null
        };
      }

      const positionSum = getPositionSum(position);
      const disc = getPositionDiscount(positionSum, i);

      return {
        ...position,
        discount: disc,
        discount_percent: null
      };
    });
  }

  return null;
}

export function mapOrdersPositions(formValues = {}, orders = []) {
  const formValuesKeys = Object.keys(formValues);

  if (formValuesKeys.some(key => key === "discount")) {
    return orders.reduce((result, currentOrder) => {
      if (!currentOrder.positions) {
        return result;
      }

      const currPositions = mapOrderPositionsDiscount(formValues, currentOrder);

      result.push({
        order_id: currentOrder.id,
        positions: currPositions
      });

      return result;
    }, []);
  }

  return orders.reduce((result, currentOrder) => {
    if (!currentOrder.positions) {
      return result;
    }

    const isOrderValuesChanged = Object.keys(formValues).some(formKey => {
      return currentOrder.positions.some(pos => formKey.includes(pos._id));
    });
    if (!isOrderValuesChanged) {
      return result;
    }

    let currentPositions = currentOrder.positions.map(pos => {
      const newPos = formValuesKeys.reduce(
        mapOrderPositionSpecialData(formValues, pos),
        { ...pos }
      );

      return newPos;
    });

    return result.concat({
      order_id: currentOrder.id,
      positions: currentPositions
    });
  }, []);
}

export function normalizeOrdersPositionsForExtendReserve(
  formValues,
  { order }
) {
  const reserve_time = formValues["reserve_time__"];

  const byNormalize = position => ({
    ...position,
    reserve_time
  });

  return {
    body: {
      order_id: order.id,
      positions: order.positions.map(byNormalize)
    }
  };
}

export function* mapValuesToOrderByDeliveryChangeSaga(
  order,
  values,
  orderCreated = true
) {
  const delivery_type = +values.delivery_type;

  switch (delivery_type) {
    case 1: {
      let copyOrder = { ...order };

      if (orderCreated && delivery_type !== order.delivery_type) {
        const positions = copyOrder.positions.map(p => {
          if (p.product.type !== 5) return p;

          return {
            ...p,
            service_state: 5
          };
        });

        const requestParams = {
          payload: {
            id: generateId(),
            entity: { order_id: copyOrder.id, positions }
          }
        };
        const { response, error } = yield call(
          updateOrderPositionsSaga,
          requestParams
        );

        if (error || !(response && response.payload && response.payload.order))
          throw { message: "Ошибка в запросе!" };
        copyOrder = { ...response.payload.order, delivery_type: 1 };
      }

      if (copyOrder.deliverylaf24) delete copyOrder.deliverylaf24;
      if (copyOrder.deliveryDC) delete copyOrder.deliveryDC;

      const pickup = {
        warehouse_id: values.warehouse_id,
        min_date_delivery: values.min_date_delivery
      };

      if (values.warehouse_name) {
        pickup.warehouse_name = values.warehouse_name;
      }

      copyOrder.pickup = pickup;

      return { order: copyOrder };
    }
    case 2: {
      let copyOrder = { ...order };
      const selectedDeliveryPrice = deliveryPrice.info[values.delivery_price];
      const deliveryPosition = {
        product: {
          article: selectedDeliveryPrice.id,
          brand: selectedDeliveryPrice.company,
          name: selectedDeliveryPrice.name,
          type: 5
        },
        count: 1,
        price: selectedDeliveryPrice.price
      };

      let newPositions = copyOrder.positions;

      if (orderCreated && delivery_type !== order.delivery_type) {
        if (order.delivery_type === 3) {
          newPositions = copyOrder.positions.map(p => {
            if (p.product.type !== 5) return p;

            return {
              ...p,
              service_state: 5
            };
          });
        }
        newPositions = newPositions.concat(deliveryPosition);
      } else {
        const currentDeliveryPrice = copyOrder.positions.find(
          p => p.product.type === 5
        );

        if (
          currentDeliveryPrice &&
          currentDeliveryPrice.product &&
          currentDeliveryPrice.product.article !== selectedDeliveryPrice.id
        ) {
          newPositions = copyOrder.positions.map(p => {
            if (p.product.type !== 5) return p;

            return {
              ...deliveryPosition
            };
          });
        }
      }

      if (orderCreated && newPositions !== copyOrder.positions) {
        const requestParams = {
          payload: {
            id: generateId(),
            entity: { order_id: copyOrder.id, positions: newPositions }
          }
        };
        const { response, error } = yield call(
          updateOrderPositionsSaga,
          requestParams
        );

        if (error || !(response && response.payload && response.payload.order))
          throw { message: "Ошибка в запросе!" };
        copyOrder = { ...response.payload.order, delivery_type: 2 };
      }

      const deliverylaf24 = {
        warehouse_id: values.warehouse_id,
        address: {
          type: 5,
          location: values.location
        }
      };

      if (values.warehouse_name) {
        deliverylaf24.warehouse_name = values.warehouse_name;
      }

      if (values.min_date_delivery) {
        deliverylaf24.min_date_delivery = values.min_date_delivery;
      }

      if (values.kladr) {
        deliverylaf24.address.kladr = values.kladr;
      }

      if (values.date_fact) {
        deliverylaf24.date_fact = values.date_fact;
      }

      if (values.date_plan) {
        deliverylaf24.date_plan = values.date_plan;
      }

      if (values.period) {
        deliverylaf24.period = +values.period;
      }

      if (values.driver_id) {
        deliverylaf24.driver_id = values.driver_id;
      }

      if (values.driver_name) {
        deliverylaf24.driver_name = values.driver_name;
      }

      copyOrder.deliverylaf24 = deliverylaf24;

      if (copyOrder.pickup) delete copyOrder.pickup;
      if (copyOrder.deliveryDC) delete copyOrder.deliveryDC;

      return { order: copyOrder };
    }
    case 3: {
      let copyOrder = { ...order };
      const selectedDeliveryPrice = deliveryPrice.info.delivery_sdek;

      const deliveryPosition = {
        product: {
          article: selectedDeliveryPrice.id,
          brand: selectedDeliveryPrice.company,
          name: selectedDeliveryPrice.name,
          type: 5
        },
        count: 1,
        price: selectedDeliveryPrice.price
      };

      let newPositions = copyOrder.positions;

      if (orderCreated && delivery_type !== order.delivery_type) {
        if (order.delivery_type === 2) {
          newPositions = copyOrder.positions.map(p => {
            if (p.product.type !== 5) return p;

            return {
              ...p,
              service_state: 5
            };
          });
        }
        newPositions = newPositions.concat(deliveryPosition);
      }

      if (orderCreated && newPositions !== copyOrder.positions) {
        const requestParams = {
          payload: {
            id: generateId(),
            entity: { order_id: copyOrder.id, positions: newPositions }
          }
        };
        const { response, error } = yield call(
          updateOrderPositionsSaga,
          requestParams
        );

        if (error || !(response && response.payload && response.payload.order))
          throw { message: "Ошибка в запросе!" };
        copyOrder = { ...response.payload.order, delivery_type: 3 };
      }

      let address = {
        type: 6
      };

      if (
        !copyOrder.deliveryDC ||
        values.codedc !== copyOrder.deliveryDC.codedc
      ) {
        if (!values.location_cdek) throw { message: "Ошибка в запросе!" };

        const requestParams = {
          payload: {
            params: {
              query: values.location_cdek
            }
          }
        };
        const { response, error } = yield call(
          fetchKladrInfoSaga,
          requestParams
        );

        if (
          error ||
          !(
            response &&
            response.payload &&
            response.payload.suggestions &&
            response.payload.suggestions[0]
          )
        ) {
          address.kladr = "0";
          address.location = values.location_cdek;
        } else {
          const addressInfo = response.payload.suggestions[0];
          address.kladr = addressInfo.data.kladr_id;
          address.location = addressInfo.value;
        }
      } else {
        address = copyOrder.deliveryDC.address;
      }

      const deliveryDC = {
        warehouse_id: values.warehouse_id,
        address
      };

      if (values.warehouse_name) {
        deliveryDC.warehouse_name = values.warehouse_name;
      }

      if (values.min_date_delivery) {
        deliveryDC.min_date_delivery = values.min_date_delivery;
      }

      if (values.date_fact) {
        deliveryDC.date_fact = values.date_fact;
      }

      if (values.date_plan) {
        deliveryDC.date_plan = values.date_plan;
      }

      if (values.period) {
        deliveryDC.period = +values.period;
      }

      if (values.driver_id) {
        deliveryDC.driver_id = values.driver_id;
      }

      if (values.driver_name) {
        deliveryDC.driver_name = values.driver_name;
      }

      if (values.delivery_company) {
        deliveryDC.delivery_company = values.delivery_company;
      }

      if (values.delivery_company_name) {
        deliveryDC.delivery_company_name = values.delivery_company_name;
      }

      if (values.codedc) {
        deliveryDC.codedc = values.codedc;
      }

      copyOrder.deliveryDC = deliveryDC;

      if (copyOrder.pickup) delete copyOrder.pickup;
      if (copyOrder.deliverylaf24) delete copyOrder.deliverylaf24;

      return { order: copyOrder };
    }
    default:
      return { order };
  }
}
