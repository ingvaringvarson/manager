import { combineReducers } from "redux";
import list from "./list";
import info from "./info";
import listForEntity from "./listForEntity";

export default combineReducers({
  info,
  list,
  listForEntity
});
