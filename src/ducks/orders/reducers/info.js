import { FETCH_ORDER_SUCCESS, UPDATE_ORDER_SUCCESS } from "../actions";
import { InfoStateRecord, fetchEntitySuccess } from "../../../helpers/reducers";

const infoReducer = (state = { ...InfoStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_ORDER_SUCCESS:
    case FETCH_ORDER_SUCCESS:
      return fetchEntitySuccess(state, payload);
    default:
      return state;
  }
};

export default infoReducer;
