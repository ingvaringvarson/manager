import { call, all, takeEvery, put, select } from "redux-saga/effects";
import {
  loggingRequestSaga,
  fetchApiSaga,
  addNotification,
  watchingTwinActionsSaga
} from "../logger";
import {
  FETCH_PRODUCTS_BY_ARTICLE_ERROR,
  FETCH_PRODUCTS_BY_ARTICLE_SUCCESS,
  FETCH_PRODUCTS_BY_ARTICLE_REQUEST,
  ADD_PRODUCT_TO_BASKET_REQUEST,
  FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_REQUEST,
  FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_SUCCESS,
  FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_ERROR
} from "./actions";
import { moduleName } from "./config";
import { getProducts } from "../../api/search";
import {
  addItemsToBasket,
  addItemsToBasketSaga,
  basketLimitReachedSelector,
  createBasket,
  createBasketSaga
} from "../baskets";
import {
  fetchDiscountGroupsForAccountSaga,
  fetchDiscountGroupsForAccount
} from "../discountGroup";
import { pageFiltersSelector } from "../search";
import { getValueInDepth } from "../../utils";
import { mapDiscountGroupIdToRequestParams } from "./mapValuesToRequest";

export function* fetchProductsByArticleSaga(action) {
  const paramsRequest = getProducts(action.payload.params);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (response && response.payload) {
    yield put({
      type: FETCH_PRODUCTS_BY_ARTICLE_SUCCESS,
      payload: {
        ...response.payload,
        id: action.payload.id,
        query: action.payload.query
      }
    });
  } else if (error) {
    yield put({
      type: FETCH_PRODUCTS_BY_ARTICLE_ERROR,
      payload: {
        id: action.payload.id
      }
    });
  }
}

export function* fetchAdditionalOffersByBasketItemSaga(action) {
  const { id, basket, params } = action.payload;
  let discountGroupId = null;

  if (basket.clientAgentId) {
    const { response } = yield call(
      fetchDiscountGroupsForAccountSaga,
      fetchDiscountGroupsForAccount(basket.clientAgentId)
    );
    discountGroupId = getValueInDepth(response, ["payload", "discountGroupId"]);
  }

  const paramsApi = mapDiscountGroupIdToRequestParams(params, discountGroupId);
  const paramsRequest = getProducts(paramsApi);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (response && response.payload) {
    yield put({
      type: FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_SUCCESS,
      payload: {
        ...response.payload,
        id
      }
    });
  } else if (error) {
    yield put({
      type: FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_ERROR,
      payload: {
        id
      }
    });
  }
}

export function* addProductToBasketSaga(action) {
  const { items } = action.payload;
  const basketLimitReached = yield select(basketLimitReachedSelector);
  const { basketId, agent, discountGroupId, cityId } = yield select(
    pageFiltersSelector
  );

  if (basketId) {
    yield call(addItemsToBasketSaga, addItemsToBasket(basketId, items));
  } else if (agent) {
    if (!basketLimitReached) {
      const { id } = agent;

      yield call(
        createBasketSaga,
        createBasket({ cityId, clientAgentId: id, discountGroupId }, items)
      );
    } else {
      yield put(
        addNotification({
          message:
            "Создано максимальное количество корзин. Удалите существующую корзину или выберите в меню нужную корзину"
        })
      );
    }
  } else {
    if (!basketLimitReached) {
      const actionParams = { payload: { basket: { cityId }, items } };

      yield call(createBasketSaga, actionParams);
    } else {
      yield put(
        addNotification({
          message:
            "Создано максимальное количество корзин. Удалите существующую корзину или выберите в меню нужную корзину"
        })
      );
    }
  }
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_PRODUCTS_BY_ARTICLE_REQUEST]: [
        fetchProductsByArticleSaga,
        moduleName
      ],
      [FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_REQUEST]: [
        fetchAdditionalOffersByBasketItemSaga,
        moduleName
      ]
    }),
    takeEvery(
      ADD_PRODUCT_TO_BASKET_REQUEST,
      loggingRequestSaga,
      addProductToBasketSaga,
      moduleName
    )
  ]);
}
