import { appName } from "../../config";

export const moduleName = "products";
export const prefix = `${appName}/${moduleName}`;
