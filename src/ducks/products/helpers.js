import { isType } from "../../utils";

export function mapFilters(
  filters,
  availableFilters,
  query = {},
  presets = []
) {
  let usePresets = false;
  if (presets.length && !filters.some(filter => query[filter.urlProperty])) {
    usePresets = true;
  }

  return filters.reduce((entities, entity) => {
    let selectedItems = [];

    if (usePresets) {
      const presetFilter = presets.find(
        preset => preset[0] === entity.urlProperty
      );

      if (presetFilter) {
        if (
          entity.type.view === "minmax" &&
          entity.items[0] <= +presetFilter[1] &&
          entity.items[1] >= +presetFilter[1]
        ) {
          selectedItems = [presetFilter[1], presetFilter[1]];
        } else if (
          entity.items.find(item => item.valueUrl === presetFilter[1])
        ) {
          selectedItems.push(presetFilter[1]);
        }
      }
    } else {
      const queryFilter = query[entity.urlProperty];

      if (queryFilter) {
        if (isType(queryFilter, "object") && entity.type.view === "minmax") {
          selectedItems[0] = queryFilter.hasOwnProperty("min")
            ? queryFilter.min
            : entity.items[0];
          selectedItems[1] = queryFilter.hasOwnProperty("max")
            ? queryFilter.max
            : entity.items[1];
        } else if (Array.isArray(queryFilter)) {
          selectedItems = queryFilter;
        } else {
          selectedItems.push(queryFilter);
        }
      }
    }

    let availableItems = [];
    const availableFilter = availableFilters.find(
      entityAvailable => entityAvailable.urlProperty === entity.urlProperty
    );

    if (availableFilter) {
      if (availableFilter.type.view === "parameters") {
        availableItems = availableFilter.items.reduce(
          (itemList, item) => itemList.concat(item.valueUrl),
          []
        );
      } else if (availableFilter.type.view === "minmax") {
        availableItems = availableFilter.items;
      }
    }

    entities[entity.urlProperty] = {
      ...entity,
      selectedItems,
      availableItems
    };

    return entities;
  }, {});
}

export function mapProducts(products) {
  return products.map(product => {
    const copyProduct = { ...product, mainOffers: [], additionalOffers: [] };
    const actualVendorCode = product.vendorCodes.find(item => item.isActual);
    copyProduct.mainVendorCode =
      actualVendorCode || product.vendorCodes.length
        ? product.vendorCodes[0]
        : null;

    if (product.offers.length) {
      const indexOfferInStock = product.offers.findIndex(
        item => item.isInStock
      );

      if (~indexOfferInStock) {
        copyProduct.mainOffers = [product.offers[indexOfferInStock]];
        const copyOffers = product.offers.slice();
        copyOffers.splice(indexOfferInStock, 1);
        copyProduct.additionalOffers = copyOffers;
      } else {
        const offerCheap = product.offers.sort((offerA, offerB) => {
          return (
            offerA.price - offerB.price ||
            offerA.deliveryDays - offerB.deliveryDays
          );
        })[0];
        const offerFast = product.offers.sort((offerA, offerB) => {
          return (
            offerA.deliveryDays - offerB.deliveryDays ||
            offerA.price - offerB.price
          );
        })[0];

        if (offerCheap.id === offerFast.id) {
          copyProduct.mainOffers = [offerCheap];
          copyProduct.additionalOffers = product.offers.filter(
            offer => offer.id !== offerCheap.id
          );
        } else {
          copyProduct.mainOffers = [offerCheap, offerFast];
          copyProduct.additionalOffers = product.offers.filter(
            offer => offer.id !== offerCheap.id && offer.id !== offerFast.id
          );
        }
      }
    }

    return copyProduct;
  });
}
