import {
  FETCH_PRODUCTS_BY_ARTICLE_SUCCESS,
  FETCH_PRODUCTS_BY_ARTICLE_ERROR,
  FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_ERROR,
  FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_SUCCESS
} from "./actions";
import { mapFilters, mapProducts } from "./helpers";

const StateRecord = {};

const EntityRecord = {
  isLoaded: false,
  searchProduct: [],
  originalProducts: [],
  analogProducts: [],
  brandLevels: {},
  filters: [],
  availableFilters: [],
  pageProperties: {
    page: 0,
    totalProducts: 0,
    totalPages: 0
  }
};

function reducers(state = StateRecord, action) {
  const { type, payload } = action;

  switch (type) {
    case FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_SUCCESS:
    case FETCH_PRODUCTS_BY_ARTICLE_SUCCESS:
      return fetchProductsByArticleSuccess(state, payload);
    case FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_ERROR:
    case FETCH_PRODUCTS_BY_ARTICLE_ERROR:
      return fetchProductsByArticleError(state, payload);
    default:
      return state;
  }
}

function fetchProductsByArticleSuccess(state, payload) {
  const {
    id,
    query = {},
    filters,
    searchProduct,
    originalProducts,
    analogProducts,
    brandLevels,
    availableFilters,
    pageProperties
  } = payload;

  return {
    ...state,
    [id]: {
      ...EntityRecord,
      searchProduct: mapProducts(searchProduct),
      originalProducts: mapProducts(originalProducts),
      analogProducts: mapProducts(analogProducts),
      filters: mapFilters(filters, availableFilters, query),
      brandLevels,
      availableFilters,
      pageProperties,
      isLoaded: true
    }
  };
}

function fetchProductsByArticleError(state, payload) {
  const { id } = payload;

  return {
    ...state,
    [id]: {
      ...EntityRecord
    }
  };
}

export default reducers;
