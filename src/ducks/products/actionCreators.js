import {
  FETCH_PRODUCTS_BY_ARTICLE_REQUEST,
  ADD_PRODUCT_TO_BASKET_REQUEST,
  FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_REQUEST
} from "./actions";

export function fetchProductsByArticle(id, query, params) {
  return {
    type: FETCH_PRODUCTS_BY_ARTICLE_REQUEST,
    payload: {
      id,
      query,
      params
    }
  };
}

export function addToBasket(items) {
  return {
    type: ADD_PRODUCT_TO_BASKET_REQUEST,
    payload: {
      items
    }
  };
}

export function fetchAdditionalOffersByBasketItem(id, basket, params) {
  return {
    type: FETCH_ADDITIONAL_OFFERS_BY_BASKET_ITEM_REQUEST,
    payload: {
      id,
      params,
      basket
    }
  };
}
