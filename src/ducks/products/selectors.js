import { moduleName } from "./config";
import {
  queryRouterSelector,
  paramsForRouteSelector
} from "../../redux/selectors/router";
import { createSelector } from "reselect";
import { getMainVendorCodeKey, isType, getValueInDepth } from "../../utils";
import { pageFiltersSelector } from "../search";

const stateSelector = state => state[moduleName];

export const productsSelector = (state, props) => {
  return stateSelector(state, props);
};

export const productsInfoSelector = (state, props) => {
  return productsSelector(state, props)[props.id];
};

export const searchProductInfoSelector = (state, props) => {
  const productsInfo = productsInfoSelector(state, props);

  return getValueInDepth(productsInfo, ["searchProduct", 0]);
};

export const brandLevelsSelector = (state, props) => {
  const productsInfo = productsInfoSelector(state, props);

  return getValueInDepth(productsInfo, ["brandLevels"]);
};

export const requestParamsForProductListPageSelector = createSelector(
  queryRouterSelector,
  paramsForRouteSelector,
  pageFiltersSelector,
  ({ page = 1, pageSize = 20, ...query }, params, pageFilters) => {
    const { valueFilters, rangeFilters } = Object.keys(query).reduce(
      (filters, key) => {
        if (isType(query[key], "object")) {
          filters.rangeFilters.push({
            url: key,
            min: query[key].hasOwnProperty("min") ? query[key].min : 0,
            max: query[key].hasOwnProperty("max") ? query[key].max : 999999
          });
        } else if (Array.isArray(query[key])) {
          query[key].forEach(value => {
            filters.valueFilters.push({
              url: key,
              value,
              valueUrl: value
            });
          });
        } else {
          filters.valueFilters.push({
            url: key,
            value: query[key],
            valueUrl: query[key]
          });
        }

        return filters;
      },
      { valueFilters: [], rangeFilters: [] }
    );

    return {
      ...params,
      page,
      pageSize,
      valueFilters,
      rangeFilters,
      client: {
        companyId: 2,
        cityId: pageFilters.cityId,
        discountGroup: pageFilters.discountGroupId
      }
    };
  }
);

export const requestIdForAdditionalOffersSelector = (_, props) => {
  return getValueInDepth(props, ["item", "_id"]);
};

const getBasketItemBrandKey = (_, props) =>
  getValueInDepth(props, ["item", "product", "brandKey"]);
const getBasketItemVendorCodeKey = (_, props) =>
  getMainVendorCodeKey(
    getValueInDepth(props, ["item", "product", "vendorCodes"])
  );
const getBasketCityId = (_, props) =>
  getValueInDepth(props, ["basket", "cityId"]);

export const requestParamsForAdditionalOffersSelector = createSelector(
  getBasketItemBrandKey,
  getBasketItemVendorCodeKey,
  getBasketCityId,
  (brandKey, vendorCode, cityId) => {
    return {
      brandKey,
      vendorCode,
      page: 1,
      pageSize: 1,
      valueFilters: [],
      rangeFilters: [],
      client: {
        companyId: 2,
        cityId,
        discountGroup: null
      }
    };
  }
);
