export function mapDiscountGroupIdToRequestParams(params, discountGroupId) {
  return {
    ...params,
    client: {
      ...params.client,
      discountGroupId
    }
  };
}
