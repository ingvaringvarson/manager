import { combineReducers } from "redux";
import list from "./list";
import info from "./info";
import createBasket from "./createBasket";

export default combineReducers({
  info,
  list,
  createBasket
});
