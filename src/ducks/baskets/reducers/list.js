import { FETCH_BASKETS_SUCCESS } from "../actions";

const StateRecord = [];

function listReducer(state = StateRecord, action) {
  const { type, payload } = action;

  switch (type) {
    case FETCH_BASKETS_SUCCESS:
      return fetchBasketsSuccess(state, payload);
    default:
      return state;
  }
}

function fetchBasketsSuccess(state, payload) {
  return payload.entities;
}

export default listReducer;
