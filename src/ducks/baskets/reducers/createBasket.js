import { CREATE_BASKET_SUCCESS } from "../actions";

const StateRecord = { id: null };

function createBasketReducer(state = StateRecord, action) {
  const { type, payload } = action;

  switch (type) {
    case CREATE_BASKET_SUCCESS:
      return createBasketSuccess(state, payload);
    default:
      return state;
  }
}

function createBasketSuccess(state, payload) {
  return { id: payload.basketId };
}

export default createBasketReducer;
