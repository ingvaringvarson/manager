import {
  FETCH_BASKET_INFO_SUCCESS,
  ADD_ITEMS_TO_BASKET_SUCCESS,
  ATTACH_AGENT_TO_BASKET_SUCCESS,
  UPDATE_BASKET_COMMENT_SUCCESS,
  UPDATE_QTY_ITEM_IN_BASKET_SUCCESS,
  UPDATE_BASKET_CONTRACT_SUCCESS,
  UPDATE_BASKET_DELIVERY_SUCCESS,
  UPDATE_OFFER_ITEM_IN_BASKET_SUCCESS,
  REMOVE_BASKET_SUCCESS
} from "../actions";
import { indexingEntity } from "../../../utils";

const StateRecord = {};

function infoReducer(state = StateRecord, action) {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_QTY_ITEM_IN_BASKET_SUCCESS:
    case UPDATE_BASKET_COMMENT_SUCCESS:
    case ATTACH_AGENT_TO_BASKET_SUCCESS:
    case ADD_ITEMS_TO_BASKET_SUCCESS:
    case FETCH_BASKET_INFO_SUCCESS:
    case UPDATE_BASKET_CONTRACT_SUCCESS:
    case UPDATE_BASKET_DELIVERY_SUCCESS:
    case UPDATE_OFFER_ITEM_IN_BASKET_SUCCESS:
      return fetchBasketInfoSuccess(state, payload);
    case REMOVE_BASKET_SUCCESS:
      return removeBasketSuccess(state, payload);
    default:
      return state;
  }
}

function fetchBasketInfoSuccess(state, payload) {
  return { ...state, [payload.basketId]: indexingEntity(payload.basket) };
}

function removeBasketSuccess(state, payload) {
  return { ...state, [payload.basketId]: null };
}

export default infoReducer;
