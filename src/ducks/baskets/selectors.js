import { createSelector } from "reselect";
import { moduleName } from "./config";

const stateSelector = state => state[moduleName];

export const basketListSelector = (state, props) => {
  return stateSelector(state, props).list;
};

export const countBasketsSelector = (state, props) => {
  return basketListSelector(state, props).length;
};

export const basketLimitReachedSelector = (state, props) => {
  return countBasketsSelector(state, props) > 6;
};

const stateBasketInfoSelector = (state, props) => {
  return stateSelector(state, props).info;
};

export const basketInfoSelector = (state, props) => {
  return stateBasketInfoSelector(state, props)[props.id];
};

const stateCreateBasketSelector = (state, props) => {
  return stateSelector(state, props).createBasket;
};

export const createdBasketIdSelector = (state, props) => {
  return stateCreateBasketSelector(state, props).id;
};

export const basketOptionsSelector = createSelector(
  basketListSelector,
  baskets =>
    baskets.map(basket => ({
      id: basket.managerBasketId,
      name: basket.managerBasketId
    }))
);
