export function mapAttachAgentToRequestParamsByUpdateBasket(payload) {
  return {
    actionApiName: "client",
    basketId: payload.basketId,
    body: payload.params
  };
}

export function mapDetachAgentToRequestParamsByUpdateBasket(payload) {
  return {
    basketId: payload.basketId,
    query: payload.params
  };
}

export function mapCommentToRequestParamsByUpdateBasket(payload) {
  return {
    actionApiName: "comment",
    basketId: payload.basketId,
    body: { comment: payload.comment }
  };
}

export function mapBasketItemsToRequestParamsByUpdateItemsQty(payload) {
  return {
    actionApiName: "quantity",
    basketId: payload.basketId,
    itemId: payload.itemId,
    body: { qty: payload.qty }
  };
}

export function mapBasketItemToRequestParamsByUpdateItemOffer(payload) {
  return {
    actionApiName: "offer",
    basketId: payload.basketId,
    itemId: payload.itemId,
    body: payload.params
  };
}

export function mapDeliveryInfoToRequestParamsByUpdateBasket(payload) {
  const delivery_type = +payload.deliveryInfo.delivery_type;
  return {
    actionApiName: "delivery",
    basketId: payload.basketId,
    body: {
      delivery_type,
      warehouse_id: payload.deliveryInfo.warehouse_id,
      address: {
        location:
          delivery_type === 3
            ? payload.deliveryInfo.location_cdek
            : payload.deliveryInfo.location,
        kladr: payload.deliveryInfo.kladr,
        delivery_point_id: payload.deliveryInfo.codedc
      },
      period: {
        date_plan: payload.deliveryInfo.date_plan,
        period: payload.deliveryInfo.period
      }
    }
  };
}

export function mapContractToRequestParamsByUpdateBasket(payload) {
  return {
    actionApiName: "contract",
    basketId: payload.basketId,
    body: { contract_id: payload.contractId }
  };
}

export function mapBasketInfoToRequestParamsByCreateOrder(payload) {
  return {
    basketId: payload.basketId,
    body: {
      basket_items: payload.basket_items,
      creation_type: payload.creation_type,
      cleanup_basket: payload.cleanup_basket
    }
  };
}

export function mapAgentInfoToRequestParamsByCheckBasket(info) {
  return {
    agentId: info.id
  };
}
