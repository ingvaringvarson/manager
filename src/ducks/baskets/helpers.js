import { put, call } from "redux-saga/effects";
import { fetchApiSaga } from "../logger";
import { getValueInDepth } from "../../utils";

export function* requestBasketApiHelperSaga(
  action,
  mapParamsApi,
  mapParamsRequest,
  successActionType
) {
  const paramsApi = mapParamsApi
    ? mapParamsApi(action.payload)
    : action.payload;
  const paramsRequest = mapParamsRequest(paramsApi);
  const { response, ...rest } = yield call(fetchApiSaga, paramsRequest);

  if (response && response.payload && successActionType) {
    yield put({
      type: successActionType,
      payload: {
        basketId: action.payload.basketId,
        basket: response.payload
      }
    });
  }

  return { response, ...rest };
}

export function* requestBasketApiWithoutResponseBasketHelperSaga(
  action,
  mapParamsApi,
  mapParamsRequest,
  successActionType
) {
  const paramsApi = mapParamsApi
    ? mapParamsApi(action.payload)
    : action.payload;
  const paramsRequest = mapParamsRequest(paramsApi);
  const { response, ...rest } = yield call(fetchApiSaga, paramsRequest);

  const isSuccess = getValueInDepth(response, ["payload", "isSuccess"]);

  if (isSuccess && successActionType) {
    yield put({
      type: successActionType,
      payload: {
        basketId: action.payload.basketId
      }
    });
  }

  return { response, ...rest };
}
