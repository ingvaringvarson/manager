import {
  FETCH_BASKETS_REQUEST,
  FETCH_BASKET_INFO_REQUEST,
  CREATE_BASKET_REQUEST,
  REMOVE_BASKET_REQUEST,
  ADD_ITEMS_TO_BASKET_REQUEST,
  ATTACH_AGENT_TO_BASKET_REQUEST,
  UPDATE_BASKET_COMMENT_REQUEST,
  UPDATE_QTY_ITEM_IN_BASKET_REQUEST,
  UPDATE_BASKET_DELIVERY_REQUEST,
  UPDATE_BASKET_CONTRACT_REQUEST,
  CREATE_ORDER_BASED_ON_BASKET_REQUEST,
  MOVE_TO_PRODUCTS_PAGE,
  REMOVE_ITEMS_FROM_BASKET_REQUEST,
  UPDATE_OFFER_ITEM_IN_BASKET_REQUEST,
  ATTACH_AGENT_TO_BASKET_WITH_CHECK_BASKET,
  DETACH_AGENT_FROM_BASKET_REQUEST
} from "./actions";

export function fetchBaskets() {
  return {
    type: FETCH_BASKETS_REQUEST
  };
}

export function fetchBasketInfo(basketId) {
  return {
    type: FETCH_BASKET_INFO_REQUEST,
    payload: {
      basketId
    }
  };
}

export function createBasket(basket = {}, items = []) {
  return {
    type: CREATE_BASKET_REQUEST,
    payload: {
      basket,
      items
    }
  };
}

export function removeBasket(basketId) {
  return {
    type: REMOVE_BASKET_REQUEST,
    payload: {
      basketId
    }
  };
}

export function addItemsToBasket(basketId, items) {
  return {
    type: ADD_ITEMS_TO_BASKET_REQUEST,
    payload: {
      basketId,
      items
    }
  };
}

export function attachAgentToBasket(basketId, agent_id, copyBasket = false) {
  return {
    type: ATTACH_AGENT_TO_BASKET_REQUEST,
    payload: {
      basketId,
      params: {
        agent_id,
        copyBasket
      }
    }
  };
}

export function detachAgentFromBasket(
  basketId,
  mode = 0, // 0 or 1 or 2 http://wiki.laf24.ru/display/DEV/MAN+Basket+API
  cleanup = false
) {
  return {
    type: DETACH_AGENT_FROM_BASKET_REQUEST,
    payload: {
      basketId,
      params: {
        mode,
        cleanup
      }
    }
  };
}

export function updateBasketComment(basketId, comment) {
  return {
    type: UPDATE_BASKET_COMMENT_REQUEST,
    payload: {
      basketId,
      comment
    }
  };
}

export function updateQtyItemInBasket(basketId, itemId, qty) {
  return {
    type: UPDATE_QTY_ITEM_IN_BASKET_REQUEST,
    payload: {
      basketId,
      itemId,
      qty
    }
  };
}

export function removeItemsFromBasket(basketId, itemIds) {
  return {
    type: REMOVE_ITEMS_FROM_BASKET_REQUEST,
    payload: {
      basketId,
      itemIds
    }
  };
}

export function updateBasketDelivery(basketId, deliveryInfo) {
  return {
    type: UPDATE_BASKET_DELIVERY_REQUEST,
    payload: {
      basketId,
      deliveryInfo
    }
  };
}

export function updateBasketContract(basketId, contractId) {
  return {
    type: UPDATE_BASKET_CONTRACT_REQUEST,
    payload: {
      contractId
    }
  };
}

export function createOrderBasedOnBasket(
  basketId,
  basket_items,
  creation_type = 12,
  cleanup_basket = true
) {
  return {
    type: CREATE_ORDER_BASED_ON_BASKET_REQUEST,
    payload: {
      basketId,
      basket_items,
      creation_type,
      cleanup_basket
    }
  };
}

export function moveToProductsPage(
  basketInfo,
  brandKey = null,
  vendorCode = null
) {
  return {
    type: MOVE_TO_PRODUCTS_PAGE,
    payload: { basketInfo, brandKey, vendorCode }
  };
}

export function updateOfferItemInBasket(basketId, itemId, params) {
  return {
    type: UPDATE_OFFER_ITEM_IN_BASKET_REQUEST,
    payload: { basketId, itemId, params }
  };
}

export function attachAgentToBasketWithCheckBasket(basketId, agentCode) {
  return {
    type: ATTACH_AGENT_TO_BASKET_WITH_CHECK_BASKET,
    payload: { basketId, agentCode }
  };
}
