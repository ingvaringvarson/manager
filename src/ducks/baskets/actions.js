import { prefix } from "./config";

export const FETCH_BASKETS_REQUEST = `${prefix}/FETCH_BASKETS_REQUEST`;
export const FETCH_BASKETS_SUCCESS = `${prefix}/FETCH_BASKETS_SUCCESS`;

export const FETCH_BASKET_INFO_REQUEST = `${prefix}/FETCH_BASKET_INFO_REQUEST`;
export const FETCH_BASKET_INFO_SUCCESS = `${prefix}/FETCH_BASKET_INFO_SUCCESS`;

export const CREATE_BASKET_REQUEST = `${prefix}/CREATE_BASKET_REQUEST`;
export const CREATE_BASKET_SUCCESS = `${prefix}/CREATE_BASKET_SUCCESS`;

export const REMOVE_BASKET_REQUEST = `${prefix}/REMOVE_BASKET_REQUEST`;
export const REMOVE_BASKET_SUCCESS = `${prefix}/REMOVE_BASKET_SUCCESS`;

export const ADD_ITEMS_TO_BASKET_REQUEST = `${prefix}/ADD_ITEMS_TO_BASKET_REQUEST`;
export const ADD_ITEMS_TO_BASKET_SUCCESS = `${prefix}/ADD_ITEMS_TO_BASKET_SUCCESS`;

export const ATTACH_AGENT_TO_BASKET_REQUEST = `${prefix}/ATTACH_AGENT_TO_BASKET_REQUEST`;
export const ATTACH_AGENT_TO_BASKET_SUCCESS = `${prefix}/ATTACH_AGENT_TO_BASKET_SUCCESS`;

export const DETACH_AGENT_FROM_BASKET_REQUEST = `${prefix}/DETACH_AGENT_FROM_BASKET_REQUEST`;
export const DETACH_AGENT_FROM_BASKET_SUCCESS = `${prefix}/DETACH_AGENT_FROM_BASKET_SUCCESS`;

export const UPDATE_BASKET_COMMENT_REQUEST = `${prefix}/UPDATE_BASKET_COMMENT_REQUEST`;
export const UPDATE_BASKET_COMMENT_SUCCESS = `${prefix}/UPDATE_BASKET_COMMENT_SUCCESS`;

export const UPDATE_QTY_ITEM_IN_BASKET_REQUEST = `${prefix}/UPDATE_QTY_ITEM_IN_BASKET_REQUEST`;
export const UPDATE_QTY_ITEM_IN_BASKET_SUCCESS = `${prefix}/UPDATE_QTY_ITEM_IN_BASKET_SUCCESS`;

export const UPDATE_OFFER_ITEM_IN_BASKET_REQUEST = `${prefix}/UPDATE_OFFER_ITEM_IN_BASKET_REQUEST`;
export const UPDATE_OFFER_ITEM_IN_BASKET_SUCCESS = `${prefix}/UPDATE_OFFER_ITEM_IN_BASKET_SUCCESS`;

export const REMOVE_ITEMS_FROM_BASKET_REQUEST = `${prefix}/REMOVE_ITEMS_FROM_BASKET_REQUEST`;
export const REMOVE_ITEMS_FROM_BASKET_SUCCESS = `${prefix}/REMOVE_ITEMS_FROM_BASKET_SUCCESS`;

export const UPDATE_BASKET_DELIVERY_REQUEST = `${prefix}/UPDATE_BASKET_DELIVERY_REQUEST`;
export const UPDATE_BASKET_DELIVERY_SUCCESS = `${prefix}/UPDATE_BASKET_DELIVERY_SUCCESS`;

export const UPDATE_BASKET_CONTRACT_REQUEST = `${prefix}/UPDATE_BASKET_CONTRACT_REQUEST`;
export const UPDATE_BASKET_CONTRACT_SUCCESS = `${prefix}/UPDATE_BASKET_CONTRACT_SUCCESS`;

export const CREATE_ORDER_BASED_ON_BASKET_REQUEST = `${prefix}/CREATE_ORDER_BASED_ON_BASKET_REQUEST`;
export const CREATE_ORDER_BASED_ON_BASKET_SUCCESS = `${prefix}/CREATE_ORDER_BASED_ON_BASKET_SUCCESS`;

export const MOVE_TO_PRODUCTS_PAGE = `${prefix}/MOVE_TO_PRODUCTS_PAGE`;

export const ATTACH_AGENT_TO_BASKET_WITH_CHECK_BASKET = `${prefix}/ATTACH_AGENT_TO_BASKET_WITH_CHECK_BASKET`;
