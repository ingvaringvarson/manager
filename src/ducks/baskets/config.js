import { appName } from "../../config";

export const moduleName = "baskets";
export const prefix = `${appName}/${moduleName}`;
