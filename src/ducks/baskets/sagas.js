import { all, takeLeading, takeEvery, put, call } from "redux-saga/effects";
import {
  FETCH_BASKETS_REQUEST,
  FETCH_BASKETS_SUCCESS,
  CREATE_BASKET_REQUEST,
  CREATE_BASKET_SUCCESS,
  FETCH_BASKET_INFO_REQUEST,
  FETCH_BASKET_INFO_SUCCESS,
  REMOVE_BASKET_REQUEST,
  REMOVE_BASKET_SUCCESS,
  ADD_ITEMS_TO_BASKET_REQUEST,
  ADD_ITEMS_TO_BASKET_SUCCESS,
  ATTACH_AGENT_TO_BASKET_REQUEST,
  ATTACH_AGENT_TO_BASKET_SUCCESS,
  UPDATE_BASKET_COMMENT_REQUEST,
  UPDATE_BASKET_COMMENT_SUCCESS,
  UPDATE_QTY_ITEM_IN_BASKET_REQUEST,
  UPDATE_QTY_ITEM_IN_BASKET_SUCCESS,
  UPDATE_BASKET_CONTRACT_REQUEST,
  UPDATE_BASKET_CONTRACT_SUCCESS,
  UPDATE_BASKET_DELIVERY_REQUEST,
  UPDATE_BASKET_DELIVERY_SUCCESS,
  CREATE_ORDER_BASED_ON_BASKET_REQUEST,
  CREATE_ORDER_BASED_ON_BASKET_SUCCESS,
  MOVE_TO_PRODUCTS_PAGE,
  REMOVE_ITEMS_FROM_BASKET_REQUEST,
  REMOVE_ITEMS_FROM_BASKET_SUCCESS,
  UPDATE_OFFER_ITEM_IN_BASKET_REQUEST,
  UPDATE_OFFER_ITEM_IN_BASKET_SUCCESS,
  ATTACH_AGENT_TO_BASKET_WITH_CHECK_BASKET,
  DETACH_AGENT_FROM_BASKET_REQUEST,
  DETACH_AGENT_FROM_BASKET_SUCCESS
} from "./actions";
import {
  getBaskets,
  addBasket,
  getBasketInfo,
  deleteBasket,
  addItemsToBasket,
  updateBasketFabric,
  updateItemInBasketFabric,
  createOrder,
  removeItemsFromBasket,
  checkAgentBasket,
  detachAgentFromBasket
} from "../../api/baskets";
import { loggingRequestSaga, fetchApiSaga, addNotification } from "../logger";
import { moduleName } from "./config";
import {
  mapAttachAgentToRequestParamsByUpdateBasket,
  mapCommentToRequestParamsByUpdateBasket,
  mapBasketItemsToRequestParamsByUpdateItemsQty,
  mapDeliveryInfoToRequestParamsByUpdateBasket,
  mapContractToRequestParamsByUpdateBasket,
  mapBasketInfoToRequestParamsByCreateOrder,
  mapBasketItemToRequestParamsByUpdateItemOffer,
  mapAgentInfoToRequestParamsByCheckBasket,
  mapDetachAgentToRequestParamsByUpdateBasket
} from "./mapValuesToRequest";
import { generateId, getValueInDepth } from "../../utils";
import { push } from "connected-react-router";
import { getUrl } from "../../helpers/urls";
import {
  mapAgentCodeToActionParams,
  mapAgentIdToActionParams
} from "../../helpers/mapValuesToRequest";
import {
  fetchClientSaga,
  fetchCurrentAgentInfoWithDiscountGroupSaga,
  forceUpdateClientInfo
} from "../clients";
import { updatePageFilters } from "../search";
import {
  requestBasketApiHelperSaga,
  requestBasketApiWithoutResponseBasketHelperSaga
} from "./helpers";
import { openModal } from "../modals";
import { attachAgentToBasketWithCheckBasketModal } from "../../constants/modalNames";
import { attachAgentToBasket } from "./actionCreators";

export function* createBasketSaga(action) {
  const paramsRequest = addBasket(action.payload);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (response && response.payload) {
    yield call(fetchBasketsSaga);

    yield put({
      type: CREATE_BASKET_SUCCESS,
      payload: {
        basketId: response.payload.managerBasketId
      }
    });
  }

  return { response, error };
}

export function* fetchBasketsSaga() {
  const paramsRequest = getBaskets();
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (response && response.payload) {
    yield put({
      type: FETCH_BASKETS_SUCCESS,
      payload: {
        entities: response.payload.basketsInfos
      }
    });
  }

  return { response, error };
}

export function* createOrderBasedOnBasketSaga(action) {
  const paramsApi = mapBasketInfoToRequestParamsByCreateOrder(action.payload);
  const paramsRequest = createOrder(paramsApi);
  const { response, ...rest } = yield call(fetchApiSaga, paramsRequest);
  const orderInfo = getValueInDepth(response, [
    "payload",
    "orders_with_bills",
    0
  ]);

  if (orderInfo) {
    yield put({
      type: CREATE_ORDER_BASED_ON_BASKET_SUCCESS,
      payload: {
        basketId: action.payload.basketId,
        orderInfo: orderInfo
      }
    });
    yield put(push(getUrl("orders", { id: orderInfo.order.id })));
  }

  return { response, ...rest };
}

export function* moveToProductsPageSaga(action) {
  const {
    basketInfo: { clientAgentId, cityId, managerBasketId },
    brandKey,
    vendorCode
  } = action.payload;
  const pageFilters = {
    cityId,
    basketId: managerBasketId,
    agent: null,
    discountGroupId: null
  };

  if (clientAgentId) {
    const actionParams = mapAgentIdToActionParams(clientAgentId);
    const {
      agent: newAgentAttached,
      discountGroupId: newDiscountGroupIdAttached
    } = yield call(fetchCurrentAgentInfoWithDiscountGroupSaga, actionParams);

    if (newAgentAttached) {
      pageFilters.agent = newAgentAttached;
      pageFilters.discountGroupId = newDiscountGroupIdAttached;
    } else {
      yield put(
        addNotification({
          id: generateId(),
          message: `Привязанный к корзине клиент с ID ${clientAgentId} не найден.`
        })
      );
    }
  }
  yield put(updatePageFilters(pageFilters));
  yield put(push(getUrl("products", { brandKey, vendorCode })));
}

export function* attachAgentToBasketWithCheckBasketSaga(action) {
  const actionParams = mapAgentCodeToActionParams(action.payload.agentCode);

  const { response: agentResponse } = yield call(fetchClientSaga, actionParams);
  const agent = getValueInDepth(agentResponse, ["payload", "agents", 0]);

  if (!agent) {
    return yield put(
      addNotification({
        id: generateId(),
        message: `Клиент с ID ${action.payload.agentCode} не найден.`
      })
    );
  }

  yield put(forceUpdateClientInfo(agent.id, agent));
  const paramsApi = mapAgentInfoToRequestParamsByCheckBasket(agent);
  const paramsRequest = checkAgentBasket(paramsApi);
  const { response: agentBasketResponse } = yield call(
    fetchApiSaga,
    paramsRequest
  );
  const itemsCount = getValueInDepth(agentBasketResponse, [
    "payload",
    "itemsCount"
  ]);

  if (itemsCount) {
    return yield put(openModal(attachAgentToBasketWithCheckBasketModal, agent));
  }

  return yield call(
    attachAgentToBasketSaga,
    attachAgentToBasket(action.payload.basketId, agent.id)
  );
}

export function* updateBasketDeliverySaga(action) {
  return yield call(
    requestBasketApiHelperSaga,
    action,
    mapDeliveryInfoToRequestParamsByUpdateBasket,
    updateBasketFabric,
    UPDATE_BASKET_DELIVERY_SUCCESS
  );
}

export function* fetchBasketInfoSaga(action) {
  return yield call(
    requestBasketApiHelperSaga,
    action,
    null,
    getBasketInfo,
    FETCH_BASKET_INFO_SUCCESS
  );
}

export function* addItemsToBasketSaga(action) {
  return yield call(
    requestBasketApiHelperSaga,
    action,
    null,
    addItemsToBasket,
    ADD_ITEMS_TO_BASKET_SUCCESS
  );
}

export function* attachAgentToBasketSaga(action) {
  return yield call(
    requestBasketApiHelperSaga,
    action,
    mapAttachAgentToRequestParamsByUpdateBasket,
    updateBasketFabric,
    ATTACH_AGENT_TO_BASKET_SUCCESS
  );
}

export function* detachAgentFromBasketSaga(action) {
  return yield call(
    requestBasketApiHelperSaga,
    action,
    mapDetachAgentToRequestParamsByUpdateBasket,
    detachAgentFromBasket,
    DETACH_AGENT_FROM_BASKET_SUCCESS
  );
}

export function* updateBasketCommentSaga(action) {
  return yield call(
    requestBasketApiHelperSaga,
    action,
    mapCommentToRequestParamsByUpdateBasket,
    updateBasketFabric,
    UPDATE_BASKET_COMMENT_SUCCESS
  );
}

export function* updateQtyItemInBasketCommentSaga(action) {
  return yield call(
    requestBasketApiHelperSaga,
    action,
    mapBasketItemsToRequestParamsByUpdateItemsQty,
    updateItemInBasketFabric,
    UPDATE_QTY_ITEM_IN_BASKET_SUCCESS
  );
}

export function* updateBasketContractSaga(action) {
  return yield call(
    requestBasketApiHelperSaga,
    action,
    mapContractToRequestParamsByUpdateBasket,
    updateBasketFabric,
    UPDATE_BASKET_CONTRACT_SUCCESS
  );
}

export function* updateOfferItemInBasketSaga(action) {
  return yield call(
    requestBasketApiHelperSaga,
    action,
    mapBasketItemToRequestParamsByUpdateItemOffer,
    updateItemInBasketFabric,
    UPDATE_OFFER_ITEM_IN_BASKET_SUCCESS
  );
}

export function* removeBasketSaga(action) {
  return yield call(
    requestBasketApiWithoutResponseBasketHelperSaga,
    action,
    null,
    deleteBasket,
    REMOVE_BASKET_SUCCESS
  );
}

export function* removeItemsFromBasketSaga(action) {
  return yield call(
    requestBasketApiWithoutResponseBasketHelperSaga,
    action,
    null,
    removeItemsFromBasket,
    REMOVE_ITEMS_FROM_BASKET_SUCCESS
  );
}

export function* rootSaga() {
  yield all([
    takeLeading(
      [
        FETCH_BASKETS_REQUEST,
        REMOVE_BASKET_SUCCESS,
        ADD_ITEMS_TO_BASKET_SUCCESS,
        CREATE_ORDER_BASED_ON_BASKET_SUCCESS,
        REMOVE_ITEMS_FROM_BASKET_SUCCESS
      ],
      loggingRequestSaga,
      fetchBasketsSaga,
      moduleName
    ),
    takeEvery(
      [FETCH_BASKET_INFO_REQUEST, REMOVE_ITEMS_FROM_BASKET_SUCCESS],
      loggingRequestSaga,
      fetchBasketInfoSaga,
      moduleName
    ),
    takeLeading(
      CREATE_BASKET_REQUEST,
      loggingRequestSaga,
      createBasketSaga,
      moduleName
    ),
    takeLeading(
      REMOVE_BASKET_REQUEST,
      loggingRequestSaga,
      removeBasketSaga,
      moduleName
    ),
    takeEvery(
      ADD_ITEMS_TO_BASKET_REQUEST,
      loggingRequestSaga,
      addItemsToBasketSaga,
      moduleName
    ),
    takeEvery(
      ATTACH_AGENT_TO_BASKET_REQUEST,
      loggingRequestSaga,
      attachAgentToBasketSaga,
      moduleName
    ),
    takeEvery(
      DETACH_AGENT_FROM_BASKET_REQUEST,
      loggingRequestSaga,
      detachAgentFromBasketSaga,
      moduleName
    ),
    takeLeading(
      UPDATE_BASKET_COMMENT_REQUEST,
      loggingRequestSaga,
      updateBasketCommentSaga,
      moduleName
    ),
    takeLeading(
      UPDATE_QTY_ITEM_IN_BASKET_REQUEST,
      loggingRequestSaga,
      updateQtyItemInBasketCommentSaga,
      moduleName
    ),
    takeLeading(
      UPDATE_BASKET_DELIVERY_REQUEST,
      loggingRequestSaga,
      updateBasketDeliverySaga,
      moduleName
    ),
    takeLeading(
      UPDATE_BASKET_CONTRACT_REQUEST,
      loggingRequestSaga,
      updateBasketContractSaga,
      moduleName
    ),
    takeLeading(
      CREATE_ORDER_BASED_ON_BASKET_REQUEST,
      loggingRequestSaga,
      createOrderBasedOnBasketSaga,
      moduleName
    ),
    takeLeading(
      MOVE_TO_PRODUCTS_PAGE,
      loggingRequestSaga,
      moveToProductsPageSaga,
      moduleName
    ),
    takeLeading(
      REMOVE_ITEMS_FROM_BASKET_REQUEST,
      loggingRequestSaga,
      removeItemsFromBasketSaga,
      moduleName
    ),
    takeLeading(
      UPDATE_OFFER_ITEM_IN_BASKET_REQUEST,
      loggingRequestSaga,
      updateOfferItemInBasketSaga,
      moduleName
    ),
    takeLeading(
      ATTACH_AGENT_TO_BASKET_WITH_CHECK_BASKET,
      loggingRequestSaga,
      attachAgentToBasketWithCheckBasketSaga,
      moduleName
    )
  ]);
}
