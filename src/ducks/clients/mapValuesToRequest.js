export function normalizeAccountManagerEdit(formValues, helperValues) {
  const { client } = helperValues;

  return {
    ...client,
    manager_id: Array.isArray(formValues.manager_id)
      ? formValues.manager_id[0]
      : formValues.manager_id
  };
}
