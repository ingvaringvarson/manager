import {
  FETCH_CLIENT_REQUEST,
  FETCH_CLIENTS_REQUEST,
  FETCH_CLIENTS_FOR_ENTITY_REQUEST,
  UPDATE_CLIENT_REQUEST,
  CREATE_CLIENT_REQUEST,
  TEST_CLIENT_REQUEST,
  PUT_CLIENT_REQUEST,
  FORCE_UPDATE_CLIENT_INFO
} from "./actions";
import {
  fetchEntities,
  fetchEntitiesForTypeEntity,
  fetchEntity,
  putEntity
} from "../../helpers/actionCreators";

export const fetchClient = fetchEntity(FETCH_CLIENT_REQUEST);

export const fetchClients = fetchEntities(FETCH_CLIENTS_REQUEST);

export const fetchClientsForEntity = fetchEntitiesForTypeEntity(
  FETCH_CLIENTS_FOR_ENTITY_REQUEST
);

export function updateClient(
  id = "",
  params = {},
  clientParams = {},
  helperValues
) {
  return {
    type: UPDATE_CLIENT_REQUEST,
    payload: { id, params, clientParams, helperValues }
  };
}

export function createClient(params = {}) {
  return {
    type: CREATE_CLIENT_REQUEST,
    payload: { params }
  };
}

export const testClient = fetchEntity(TEST_CLIENT_REQUEST);

export const putClient = putEntity(PUT_CLIENT_REQUEST);

export function forceUpdateClientInfo(id, entity) {
  return {
    type: FORCE_UPDATE_CLIENT_INFO,
    payload: {
      id,
      entity
    }
  };
}
