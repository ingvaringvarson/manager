import { appName } from "../../config";

export const moduleName = "clients";
export const prefix = `${appName}/${moduleName}`;
