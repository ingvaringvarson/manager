import { moduleName } from "./config";
import {
  entityIdForRouteSelector,
  queryRouterSelector
} from "../../redux/selectors/router";
import { createSelector } from "reselect";
import { entitiesToOptions, generateId } from "../../utils";
import {
  entitiesForTypeEntitySelector,
  maxPagesSelector as maxPagesSelectorHelper,
  typeEntityStateSelector as typeEntityStateSelectorHelper,
  entityInfoSelector,
  entityListSelector
} from "../../helpers/selectors";

export const clientListSelector = entityListSelector(moduleName);

export const maxPagesSelector = maxPagesSelectorHelper(moduleName);

export const typeEntityStateSelector = typeEntityStateSelectorHelper(
  moduleName
);

export const clientsForEntitySelector = entitiesForTypeEntitySelector(
  moduleName
);

export const clientIdSelector = (state, props = {}) =>
  entityIdForRouteSelector(state, props);

export const clientInfoSelector = entityInfoSelector(moduleName);
export const clientIsProviderSelector = (state, props) => {
  const client = clientInfoSelector(state, props);
  // if client roles has element 1 return true
  return !!(
    client &&
    client.roles &&
    Array.isArray(client.roles) &&
    client.roles.length &&
    client.roles.some(role => role === 1)
  );
};

export const clientInfoByIdSelector = (state, props) => {
  const clients = clientInfoSelector(state, props);
  return clients ? clients[props.id] : null;
};

export const clientChangedSelector = createSelector(
  clientInfoSelector,
  () => generateId()
);

export const requestParamsSelector = createSelector(
  queryRouterSelector,
  query => ({ page_size: 20, page: 1, type: 1, ...query })
);

export const requestParamsForEntitySelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  (query, idEntity) => {
    return {
      page: 1,
      page_size: 20,
      agent_id: idEntity,
      sort_by: "object_date_create desc",
      ...query
    };
  }
);

export const requestParamsForClientPageSelector = createSelector(
  clientIdSelector,
  id => ({ id })
);

export const clientsToOptionsSelector = (getName, getId) => (state, props) => {
  const { entities = [] } = clientsForEntitySelector(state, props);
  return entitiesToOptions(entities, getName, getId);
};
