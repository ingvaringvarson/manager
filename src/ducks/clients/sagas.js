import {
  all,
  takeLatest,
  takeEvery,
  put,
  call,
  select,
  takeLeading
} from "redux-saga/effects";
import {
  FETCH_CLIENTS_SUCCESS,
  FETCH_CLIENTS_REQUEST,
  FETCH_CLIENT_REQUEST,
  FETCH_CLIENT_SUCCESS,
  FETCH_CLIENTS_FOR_ENTITY_ERROR,
  FETCH_CLIENTS_FOR_ENTITY_REQUEST,
  FETCH_CLIENTS_FOR_ENTITY_SUCCESS,
  UPDATE_CLIENT_REQUEST,
  UPDATE_CLIENT_SUCCESS,
  CREATE_CLIENT_RESULT,
  CREATE_CLIENT_REQUEST,
  FETCH_CLIENTS_ERROR,
  TEST_CLIENT_REQUEST,
  PUT_CLIENT_REQUEST,
  PUT_CLIENT_SUCCESS,
  PUT_CLIENT_ERROR
} from "./actions";
import { fetchClient } from "./actionCreators";
import { updateClient, createClient } from "../../api/clients";
import { authIdSelector } from "../user";
import {
  mapCreateClientFormParamsToRequestParams,
  mapEditClientFormParamsToRequestParams
} from "./helpers";
import { push } from "connected-react-router";
import {
  loggingRequestSaga,
  fetchApiSaga,
  addNotification,
  watchingTwinActionsSaga
} from "../logger";
import { moduleName } from "./config";
import { getValueInDepth, removeIndexFromEntity } from "../../utils";
import {
  fetchEntitiesForTypeEntitySaga,
  fetchEntitiesSaga,
  fetchEntitySaga,
  responseToUpdateEntitySaga,
  putEntitySaga
} from "../../helpers/sagas";
import { mapRequestParamsToParamsByApiSaga } from "../../helpers/mapMethods";
import { getUrl } from "../../helpers/urls";
import {
  fetchDiscountGroupsForAccountSaga,
  fetchDiscountGroupsForAccount
} from "../discountGroup";

export function* fetchClientsSaga(action) {
  const actions = {
    success: FETCH_CLIENTS_SUCCESS,
    error: FETCH_CLIENTS_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "agents"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  return yield call(
    fetchEntitiesSaga,
    payloadForApi,
    actions,
    "agents",
    "agents"
  );
}

export function* fetchClientsForEntitySaga(action) {
  const actions = {
    success: FETCH_CLIENTS_FOR_ENTITY_SUCCESS,
    error: FETCH_CLIENTS_FOR_ENTITY_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "agents"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  return yield call(
    fetchEntitiesForTypeEntitySaga,
    payloadForApi,
    actions,
    "agents",
    "agents"
  );
}

export function* fetchClientSaga(action) {
  const actions = {
    success: FETCH_CLIENT_SUCCESS
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "agents"
  );

  const payloadForApi = {
    ...action.payload,
    id: action.payload.id ? [action.payload.id] : null,
    params
  };

  return yield call(
    fetchEntitySaga,
    payloadForApi,
    actions,
    "agents",
    "agents"
  );
}

export function* updateClientSaga(action) {
  const clientParams = removeIndexFromEntity(
    mapEditClientFormParamsToRequestParams(
      action.payload.params,
      action.payload.clientParams
    )
  );
  const paramsRequest = updateClient(action.payload.id, {
    ...clientParams
  });
  const { response, ...rest } = yield call(fetchApiSaga, paramsRequest);

  if (response) {
    yield put({
      type: UPDATE_CLIENT_SUCCESS,
      payload: {
        id: action.payload.id,
        entity: response.payload
      }
    });

    if (
      action.payload.helperValues &&
      action.payload.helperValues.successCallback
    ) {
      yield call(action.payload.helperValues.successCallback);
    }
  }

  return { response, ...rest };
}

export function* createClientSaga(action) {
  const authId = yield select(authIdSelector);
  const clientParams = mapCreateClientFormParamsToRequestParams(
    action.payload.params
  );
  const paramsRequest = createClient({
    ...clientParams,
    manager_id: authId
  });
  const { response } = yield call(fetchApiSaga, paramsRequest);

  if (response && response.payload) {
    yield put({
      type: CREATE_CLIENT_RESULT,
      payload: {
        id: response.payload.id,
        entity: response.payload
      }
    });
  }
}

export function* responseToCreateClientSaga(action) {
  const { id } = action.payload;

  if (id) yield put(push(getUrl("clients", { id }), { isLoaded: true }));
}

export function* testClientSaga(action) {
  const { response, error } = yield call(fetchClientSaga, action);

  if (
    error ||
    !(
      response &&
      response.payload &&
      response.payload.agents &&
      response.payload.agents[0]
    )
  ) {
    yield put(addNotification({ message: "Клиент не найден!" }));
  }
}

export function* fetchCurrentAgentInfoWithDiscountGroupSaga(action) {
  const { response: agentResponse } = yield call(
    fetchClientSaga,
    fetchClient(action.payload.id, action.payload.params)
  );

  const agent = getValueInDepth(agentResponse, ["payload", "agents", 0]);

  let discountGroupId = null;

  if (agent) {
    const { response: discountGroupResponse } = yield call(
      fetchDiscountGroupsForAccountSaga,
      fetchDiscountGroupsForAccount(agent.id)
    );
    discountGroupId = getValueInDepth(discountGroupResponse, [
      "payload",
      "discountGroupId"
    ]);
  }

  return {
    agent,
    discountGroupId
  };
}

function* putClientSaga(action) {
  const {
    formValues,
    helperValues,
    normalizeCallback,
    withoutFetching
  } = action.payload;

  const actions = {
    success: PUT_CLIENT_SUCCESS,
    error: PUT_CLIENT_ERROR
  };

  const payloadForApi = {
    body:
      typeof normalizeCallback === "function"
        ? yield call(normalizeCallback, formValues, helperValues)
        : formValues
  };

  const putPayloadData = yield call(
    putEntitySaga,
    payloadForApi,
    actions,
    "agents"
  );

  if (!withoutFetching && helperValues.fetchEntities) {
    yield call(helperValues.fetchEntities);
  }

  return putPayloadData;
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_CLIENTS_REQUEST]: [fetchClientsSaga, moduleName],
      [FETCH_CLIENT_REQUEST]: [fetchClientSaga, moduleName],
      [FETCH_CLIENTS_FOR_ENTITY_REQUEST]: [
        fetchClientsForEntitySaga,
        moduleName
      ]
    }),
    takeEvery(
      UPDATE_CLIENT_REQUEST,
      loggingRequestSaga,
      updateClientSaga,
      moduleName
    ),
    takeLeading(
      CREATE_CLIENT_REQUEST,
      loggingRequestSaga,
      createClientSaga,
      moduleName
    ),
    takeLatest(CREATE_CLIENT_RESULT, responseToCreateClientSaga),
    takeLatest(UPDATE_CLIENT_SUCCESS, responseToUpdateEntitySaga),
    takeEvery(
      TEST_CLIENT_REQUEST,
      loggingRequestSaga,
      testClientSaga,
      moduleName
    ),
    takeEvery(PUT_CLIENT_REQUEST, loggingRequestSaga, putClientSaga, moduleName)
  ]);
}
