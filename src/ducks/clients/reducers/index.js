import { combineReducers } from "redux";
import list from "./list";
import info from "./info";
import listForEntity from "./listForEntity";
import createClient from "./createClient";

export default combineReducers({
  info,
  list,
  listForEntity,
  createClient
});
