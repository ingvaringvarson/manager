import { FETCH_CLIENTS_SUCCESS, FETCH_CLIENTS_ERROR } from "../actions";
import {
  ListStateRecord,
  fetchEntitiesSuccess,
  fetchEntitiesError
} from "../../../helpers/reducers";

const listReducer = (state = { ...ListStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_CLIENTS_SUCCESS:
      return fetchEntitiesSuccess(state, payload);
    case FETCH_CLIENTS_ERROR:
      return fetchEntitiesError(state, payload);
    default:
      return state;
  }
};

export default listReducer;
