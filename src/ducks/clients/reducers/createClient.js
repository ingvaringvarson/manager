import { CREATE_CLIENT_RESULT } from "../actions";

const StateRecord = {
  results: {}
};

const infoReducer = (state = StateRecord, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_CLIENT_RESULT:
      return createClientResult(state, payload);
    default:
      return state;
  }
};

function createClientResult(state, payload) {
  return {
    results: { ...state.results, [payload.id]: { ...payload.result } }
  };
}

export default infoReducer;
