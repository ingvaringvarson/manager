import {
  FETCH_CLIENT_SUCCESS,
  UPDATE_CLIENT_SUCCESS,
  CREATE_CLIENT_RESULT,
  FORCE_UPDATE_CLIENT_INFO
} from "../actions";
import { indexingEntity } from "../../../utils";
import { InfoStateRecord, fetchEntitySuccess } from "../../../helpers/reducers";

const infoReducer = (state = { ...InfoStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FORCE_UPDATE_CLIENT_INFO:
    case FETCH_CLIENT_SUCCESS:
    case UPDATE_CLIENT_SUCCESS:
      return fetchEntitySuccess(state, payload);
    case CREATE_CLIENT_RESULT:
      return createClientResult(state, payload);
    default:
      return state;
  }
};

function createClientResult(state, payload) {
  if (!payload.id) return state;

  return {
    ...state,
    entities: {
      ...state.entities,
      [payload.id]: { ...indexingEntity(payload.entity) }
    }
  };
}

export default infoReducer;
