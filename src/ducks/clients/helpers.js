import agentRole from "../../constants/agentRole";

export function mapCreateClientFormParamsToRequestParams(params) {
  const {
    type = null,
    roles = null,
    person_name = null,
    person_second_name = null,
    person_surname = null,
    company_name_full = null,
    company_inn = null,
    contacts_email = null,
    contacts_phone = null,
    create_supplier = false
  } = params;

  const typeNumber = +type;
  const requestParams = {
    type: typeNumber,
    roles: Array.isArray(roles) ? roles : [roles]
  };

  if (contacts_email || contacts_phone) {
    requestParams.contacts = [];
    if (contacts_email) {
      requestParams.contacts.push({ type: 2, value: contacts_email });
    }
    if (contacts_phone) {
      requestParams.contacts.push({ type: 7, value: contacts_phone });
    }
  }

  if (typeNumber === 1) {
    requestParams.person = {};
    if (person_name) {
      requestParams.person.person_name = person_name;
    }
    if (person_second_name) {
      requestParams.person.person_second_name = person_second_name;
    }
    if (person_surname) {
      requestParams.person.person_surname = person_surname;
    }
  } else if (typeNumber === 2) {
    if (company_name_full) {
      requestParams.company = { company_name_full };
    }
    if (company_inn) {
      requestParams.company_requisites = { company_inn };
    }
    if (create_supplier) {
      requestParams.roles = [agentRole.supplier, agentRole.client];
    }
  }

  return requestParams;
}

function mapNewValuesToClientParams(params, clientParams) {
  const { _root = null, _id = null, ...newValues } = params;

  const copyOldValues = Array.isArray(clientParams[_root])
    ? clientParams[_root].slice()
    : [];

  const index = copyOldValues.findIndex(item => item._id === _id);

  const mapValues = Object.keys(newValues).reduce((values, key) => {
    let value = newValues[key];

    if (!key.includes("__")) {
      return {
        ...values,
        [key]: key === "type" && Array.isArray(value) ? value[0] : value
      };
    }
    const arrKey = key.split("__");
    const contacts = values.contacts ? values.contacts.slice() : [];
    const indexContact = contacts.findIndex(item => item._id === arrKey[1]);

    value = arrKey[0] === "type" && Array.isArray(value) ? value[0] : value;

    if (~indexContact) {
      contacts[indexContact][arrKey[0]] = value;
    } else {
      contacts.push({ [arrKey[0]]: value, _id: arrKey[1] });
    }

    return { ...values, contacts };
  }, {});

  if (~index) {
    copyOldValues[index] = mapValues;
  } else {
    copyOldValues.push(mapValues);
  }

  return {
    ...clientParams,
    [_root]: copyOldValues
  };
}

export function mapClientInfoData(params, clientParams, _root) {
  const paramsCopy = { ...params };
  delete paramsCopy.tags;
  delete paramsCopy.segments;
  return {
    ...clientParams,
    tags: params.tags,
    segments: params.segments,
    [_root]: paramsCopy
  };
}

export function mapClientAddWorkPlace(params, clientInfo) {
  const newClient = { ...clientInfo };
  if (Array.isArray(clientInfo.employees) && clientInfo.employees[0]) {
    newClient.employees[0] = {
      ...newClient.employees[0],
      warehouse_id: params.warehouse_id,
      phone_extra: params.phone_extra
    };
  }
  return newClient;
}

function contractDecorator(c) {
  return Object.entries(c).reduce((res, [k, v]) => {
    switch (k) {
      case "terms":
        res.terms = v ? 2 : 1;
        break;
      case "_root":
        break;
      default:
        res[k] = v;
    }
    return res;
  }, {});
}

function mapClientContracts(params, clientParams, _root) {
  // в clientParams.contracts может быть либо массив, либо null
  let contracts = Array.isArray(clientParams.contracts)
    ? clientParams.contracts
    : [];

  if (contracts.some(c => c.id === params.id)) {
    contracts = contracts.map(c =>
      c.id === params.id ? contractDecorator(params) : c
    );
  } else {
    contracts = contracts.concat(contractDecorator(params));
  }

  return {
    ...clientParams,
    [_root]: contracts
  };
}

export function mapEditClientFormParamsToRequestParams(params, clientParams) {
  const { _root = null, _id = null, ...newValues } = params;

  switch (true) {
    case _root === "contracts":
      return mapClientContracts(params, clientParams, _root);
    case _root === "addWorkPlace": {
      return mapClientAddWorkPlace(params, clientParams);
    }
    case clientParams.hasOwnProperty(_root) && _id !== null: {
      return mapNewValuesToClientParams(params, clientParams);
    }
    case clientParams.hasOwnProperty(_root): {
      if (_root === "company" || _root === "person") {
        return mapClientInfoData(params, clientParams, _root);
      }
      return {
        ...clientParams,
        [_root]: { ...newValues }
      };
    }
    default:
      return clientParams;
  }
}
