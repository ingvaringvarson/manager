import { getValueInDepth } from "../../utils";

export function findPositionInOrders(position, orders, criterias) {
  // criterias is a array of strings, that means
  //that each criteria option must be same
  //for both order position and recieved positions
  let foundPos = null;
  let orderWithPosition = null;
  criterias.every(criteria => {
    return orders.some(order => {
      return order.order.positions.some(orderPos => {
        let isSame = false;
        if (Array.isArray(criteria)) {
          if (
            getValueInDepth(position, criteria) ===
            getValueInDepth(orderPos, criteria)
          ) {
            isSame = true;
            foundPos = orderPos;
            orderWithPosition = order;
          }
        } else {
          if (orderPos[criteria] === position[criteria]) {
            isSame = true;
            foundPos = orderPos;
            orderWithPosition = order;
          }
        }
        return isSame;
      });
    });
  });

  if (!foundPos) return null;

  return {
    position: foundPos,
    orderWithPosition
  };
}
