import { appName } from "../../config";

export const moduleName = "purchaseOrders";
export const prefix = `${appName}/${moduleName}`;
