export function normalizePositionChangePriceAndCount(formValues, helperValues) {
  const { positions, purchaseOrder, removedIds } = helperValues;

  return [
    {
      purchase_order_id: purchaseOrder.order.id,
      positions: positions.reduce((result, pos) => {
        if (!removedIds.some(id => id === pos._id)) {
          result.push({
            ...pos,
            count: +formValues[`count__${pos._id}`],
            price: +formValues[`price__${pos._id}`]
          });
        }
        return result;
      }, [])
    }
  ];
}

export function normalizeChangeComingDate(formValues, helperValues) {
  const { positions, purchaseOrder, removedIds } = helperValues;

  return {
    purchase_order_id: purchaseOrder.order.id,
    positions: positions.reduce((result, pos) => {
      if (!removedIds.some(id => id === pos._id)) {
        result.push({
          ...pos,
          estimated_time: formValues[`estimated_time__${pos._id}`]
        });
      }
      return result;
    }, [])
  };
}

export function normalizePositionsReject(formValues, helperValues) {
  const { positions, purchaseOrders, removedIds, good_state } = helperValues;

  return purchaseOrders.reduce((ordersResult, purchaseOrder) => {
    const resultPositions = purchaseOrder.order.positions.reduce(
      (positionsResult, position) => {
        if (!positions.some(pos => pos._id === position._id)) {
          return positionsResult;
        }

        if (!removedIds.some(id => id === position._id)) {
          positionsResult.push({
            ...position,
            count: +formValues[`reject__${position._id}`],
            good_state
          });
        }

        return positionsResult;
      },
      []
    );

    if (resultPositions.length) {
      ordersResult.push({
        purchase_order_id: purchaseOrder.order.id,
        positions: resultPositions
      });
    }
    return ordersResult;
  }, []);
}

function mapChangeDeliveryType(formValues, helperValues) {
  const { purchaseOrder } = helperValues;
  return [
    {
      purchase_order_id: purchaseOrder.id,
      positions: purchaseOrder.positions.map(pos => ({
        ...pos,
        estimated_time: formValues["estimated_time"],
        warehouse_id: formValues["warehouse_id"]
      }))
    }
  ];
}

export function mapChangeDeliveryTypeAction(action, purchaseOrder) {
  return {
    ...action,
    payload: {
      ...action.payload,
      normalizeCallback: mapChangeDeliveryType,
      withoutFetching: true,
      helperValues: {
        ...action.payload.helperValues,
        purchaseOrder
      }
    }
  };
}

export function mapPurchaseOrderUpdateComment(formValues, helperValues) {
  const { comment } = formValues;
  return {
    purchase_order: {
      ...helperValues.purchaseOrder,
      comment
    }
  };
}
