import { all, call, takeEvery, select, put, delay } from "redux-saga/effects";
import {
  FETCH_PURCHASE_ORDERS_REQUEST,
  FETCH_PURCHASE_ORDERS_SUCCESS,
  FETCH_PURCHASE_ORDERS_ERROR,
  FETCH_PURCHASE_ORDER_REQUEST,
  FETCH_PURCHASE_ORDER_SUCCESS,
  FETCH_PURCHASE_ORDER_ERROR,
  FETCH_PURCHASE_ORDERS_ENTITY_REQUEST,
  FETCH_PURCHASE_ORDERS_ENTITY_SUCCESS,
  FETCH_PURCHASE_ORDERS_ENTITY_ERROR,
  FETCH_PURCHASE_ORDER_CLIENT_REQUEST,
  POST_PURCHASE_ORDER_REQUEST,
  POST_PURCHASE_ORDER_SUCCESS,
  POST_PURCHASE_ORDER_ERROR,
  PUT_PURCHASE_ORDER_REQUEST,
  PUT_PURCHASE_ORDER_SUCCESS,
  PUT_PURCHASE_ORDER_ERROR,
  POST_PURCHASE_ORDER_POSITIONS_REQUEST,
  POST_PURCHASE_ORDER_POSITIONS_SUCCESS,
  POST_PURCHASE_ORDER_POSITIONS_ERROR,
  CHANGE_P_ORDER_POSITIONS_REQUEST,
  CHANGE_STATE_TO_ORDERED_FROM_PROVIDER_REQUEST,
  CHANGE_PO_DELIVERY_TYPE_REQUEST,
  UPDATE_PURCHASE_ORDER_REQUEST
} from "./actions";
import { loggingRequestSaga, watchingTwinActionsSaga } from "../logger";
import { moduleName } from "./config";
import { fetchClient } from "../clients";
import { mapRequestParamsToParamsByApiSaga } from "../../helpers/mapMethods";
import {
  fetchEntitiesForTypeEntitySaga,
  fetchEntitiesSaga,
  fetchEntitySaga,
  postCommonSaga,
  putEntitySaga,
  putEntitiesByFormSaga,
  postEntitySaga
} from "../../helpers/sagas";
import { purchaseOrdersBillIdEntitySelector } from "./selectors";
import { convertDateToIsoWithoutMs } from "../../utils";

import { findPositionInOrders } from "./helpers";
import { mapChangeDeliveryTypeAction } from "./mapValuesToRequest";

const typeEntity = "purchaseOrders";

export function* fetchPurchaseOrdersSaga(action) {
  const actions = {
    success: FETCH_PURCHASE_ORDERS_SUCCESS,
    error: FETCH_PURCHASE_ORDERS_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    typeEntity
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  yield call(
    fetchEntitiesSaga,
    payloadForApi,
    actions,
    typeEntity,
    "purchase_orders_with_bills"
  );
}

export function* fetchPurchaseOrdersEntitySaga(action) {
  const actions = {
    success: FETCH_PURCHASE_ORDERS_ENTITY_SUCCESS,
    error: FETCH_PURCHASE_ORDERS_ENTITY_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    typeEntity
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  yield call(
    fetchEntitiesForTypeEntitySaga,
    payloadForApi,
    actions,
    typeEntity,
    "purchase_orders_with_bills"
  );
}

export function* fetchPurchaseOrderSaga(action) {
  const actions = {
    success: FETCH_PURCHASE_ORDER_SUCCESS,
    error: FETCH_PURCHASE_ORDER_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    typeEntity
  );

  yield call(
    fetchEntitySaga,
    {
      id: action.payload.id,
      params
    },
    actions,
    typeEntity,
    "purchase_orders_with_bills"
  );
}

export function* fetchPurchaseOrderClientSaga(action) {
  yield call(fetchPurchaseOrderSaga, {
    payload: {
      id: action.payload.orderId,
      params: { id: action.payload.orderId }
    }
  });

  const order = yield select(purchaseOrdersBillIdEntitySelector, {
    id: action.payload.orderId
  });

  if (order) {
    yield put(fetchClient(order.agent_id, { id: order.agent_id }));
  }
}

function* postPurchaseOrderSaga(action) {
  const {
    formValues,
    helperValues,
    normalizeCallback,
    withoutFetching
  } = action.payload;

  const actions = {
    success: POST_PURCHASE_ORDER_SUCCESS,
    error: POST_PURCHASE_ORDER_ERROR
  };

  const payloadForApi = {
    body:
      typeof normalizeCallback === "function"
        ? normalizeCallback(formValues, helperValues)
        : formValues
  };

  const postPayloadData = yield call(
    postCommonSaga,
    payloadForApi,
    actions,
    "purchaseOrders"
  );

  if (postPayloadData.response && helperValues.successCallback) {
    yield call(helperValues.successCallback);
  }

  if (!withoutFetching && helperValues.fetchEntities) {
    yield call(helperValues.fetchEntities);
  }

  return postPayloadData;
}

function* putPurchaseOrderSaga(action) {
  const {
    formValues,
    helperValues,
    normalizeCallback,
    withoutFetching
  } = action.payload;

  const actions = {
    success: PUT_PURCHASE_ORDER_SUCCESS,
    error: PUT_PURCHASE_ORDER_ERROR
  };

  const payloadForApi = {
    body:
      typeof normalizeCallback === "function"
        ? normalizeCallback(formValues, helperValues)
        : formValues
  };

  const putPayloadData = yield call(
    putEntitySaga,
    payloadForApi,
    actions,
    "purchaseOrders"
  );

  if (putPayloadData.response && helperValues.successCallback) {
    yield call(helperValues.successCallback);
  }

  if (!withoutFetching && helperValues.fetchEntities) {
    yield call(helperValues.fetchEntities);
  }

  return putPayloadData;
}

function* postPurchaseOrderPositions(action) {
  const {
    formValues,
    helperValues,
    normalizeCallback,
    withoutFetching
  } = action.payload;

  const actions = {
    success: POST_PURCHASE_ORDER_POSITIONS_SUCCESS,
    error: POST_PURCHASE_ORDER_POSITIONS_ERROR
  };

  if (!normalizeCallback) {
    throw "normalizeCallback обязательный параметр для postPurchaseOrderPositions";
  }

  const requestParamsArray = normalizeCallback(formValues, helperValues);

  const effects = [];
  for (const body of requestParamsArray) {
    const effectsResult = yield call(
      postEntitySaga,
      { body },
      actions,
      "purchaseOrderPositions"
    );
    effects.push(effectsResult);
  }

  if (effects && helperValues.successCallback) {
    yield call(helperValues.successCallback);
  }

  if (!withoutFetching && helperValues.fetchEntities) {
    yield call(helperValues.fetchEntities);
  }

  return effects;
}

function* orderPositionsGetAndPostSaga(positions) {
  const { response } = yield call(
    fetchEntitiesSaga,
    { params: { positions: positions.map(pos => pos.order_position_id) } },
    null,
    "orders",
    "orders"
  );

  if (
    response &&
    response.payload &&
    Array.isArray(response.payload.orders_with_bills) &&
    response.payload.orders_with_bills.length
  ) {
    const orders = response.payload.orders_with_bills;

    // ToDo: скорее всего здесь блокировки
    // перенести маппинг в mapValuesToRequest
    yield all(
      positions.map(position => {
        const findResult = findPositionInOrders(position, orders, [
          ["product", "article"],
          ["product", "brand"],
          "pricelist_id"
        ]);

        if (findResult) {
          return call(
            postCommonSaga,
            {
              body: {
                order_id: findResult.orderWithPosition.order.id,
                positions: [
                  {
                    ...findResult.position,
                    estimated_time: position.estimated_time
                  }
                ]
              }
            },
            null,
            "orderPositions"
          );
        }

        return null;
      })
    );
  }
}

function* changePOrderPositionsComingDateSaga(action) {
  const {
    formValues,
    helperValues,
    normalizeCallback,
    withoutFetching
  } = action.payload;

  const payloadForApi = {
    body:
      typeof normalizeCallback === "function"
        ? normalizeCallback(formValues, helperValues)
        : formValues
  };

  const { response } = yield call(
    postCommonSaga,
    payloadForApi,
    null,
    "purchaseorderpositions"
  );

  if (!withoutFetching && helperValues.fetchEntities && response) {
    yield call(helperValues.fetchEntities);
  }

  if (helperValues.successCallback && response) {
    yield call(helperValues.successCallback);
  }

  if (response) {
    yield call(orderPositionsGetAndPostSaga, payloadForApi.body.positions);
  }
}

function* changePODeliveryTypeSaga(action) {
  const {
    formValues,
    helperValues: { purchaseOrder, maxEstimatedTime, successCallback },
    withoutFetching
  } = action.payload;

  const putPurchaseOrderResponse = yield call(
    tryPutPurchaseOrderSaga,
    purchaseOrder,
    formValues,
    action
  );

  if (
    putPurchaseOrderResponse &&
    maxEstimatedTime !== formValues["estimated_time"]
  ) {
    // ТУТ КОСЯК 1С!
    // При последовательном вызове методов второй выполнится с ошибкой
    // Изза их косяка, тут ставим костыль
    // Который добавить паузу между вызовами
    yield delay(2000);

    yield call(
      postPurchaseOrderPositions,
      mapChangeDeliveryTypeAction(
        action,
        putPurchaseOrderResponse.payload.purchase_order
      )
    );
  }

  if (!withoutFetching && successCallback) {
    yield call(successCallback);
  }
}

function* tryPutPurchaseOrderSaga(purchaseOrder, formValues, action) {
  const { delivery_type, warehouse_id } = purchaseOrder.order;
  if (
    delivery_type === +formValues["delivery_type"] &&
    warehouse_id === formValues["warehouse_id"]
  ) {
    return true;
  }

  // ToDo: перенести в mapValuesToRequest.js
  const body = {
    purchase_order: {
      ...purchaseOrder.order,
      delivery_type: +formValues["delivery_type"],
      warehouse_id: formValues["warehouse_id"]
    }
  };

  const { response } = yield call(putPurchaseOrderSaga, {
    ...action,
    payload: {
      ...action.payload,
      formValues: body
    },
    withoutFetching: true
  });

  return response;
}

function* setPurchaseOrderStateSaga(purchaseOrder) {
  const { response } = yield call(
    fetchEntitiesSaga,
    {
      params: {
        type: [10],
        state: [1],
        purchase_order_id: purchaseOrder.order.id
      }
    },
    null,
    "tasks",
    "tasks"
  );

  if (response && response.payload) {
    let linkedTask = null;

    if (
      Array.isArray(response.payload.tasks) &&
      response.payload.tasks.length
    ) {
      linkedTask = response.payload.tasks[0];
    } else {
      const createdTask = yield call(
        postCommonSaga,
        {
          body: {
            type: 10,
            purchase_order_id: purchaseOrder.order.id,
            date: convertDateToIsoWithoutMs(
              new Date(new Date().getTime() + 60000)
            )
          }
        },
        null,
        "tasks"
      );

      linkedTask = createdTask.response.payload.task;
    }

    yield call(
      putEntitySaga,
      {
        id: linkedTask.id,
        body: {
          ...linkedTask,
          state: 2
        }
      },
      null,
      "tasks"
    );
  }
}

function* changeStateToOrderedFromProviderSaga(action) {
  const { purchaseOrder, fetchEntities } = action.payload.helperValues;

  yield call(setPurchaseOrderStateSaga, purchaseOrder);

  if (!action.payload.withoutFetching && fetchEntities) {
    yield call(fetchEntities);
  }
}

function* updatePurchaseOrderSaga(action) {
  yield call(putEntitiesByFormSaga, action.payload, null, "purchaseOrders");
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_PURCHASE_ORDERS_REQUEST]: [fetchPurchaseOrdersSaga, moduleName],
      [FETCH_PURCHASE_ORDERS_ENTITY_REQUEST]: [
        fetchPurchaseOrdersEntitySaga,
        moduleName
      ],
      [FETCH_PURCHASE_ORDER_REQUEST]: [fetchPurchaseOrderSaga, moduleName],
      [FETCH_PURCHASE_ORDER_CLIENT_REQUEST]: [
        fetchPurchaseOrderClientSaga,
        moduleName
      ]
    }),
    takeEvery(
      POST_PURCHASE_ORDER_REQUEST,
      loggingRequestSaga,
      postPurchaseOrderSaga,
      moduleName
    ),
    takeEvery(
      PUT_PURCHASE_ORDER_REQUEST,
      loggingRequestSaga,
      putPurchaseOrderSaga,
      moduleName
    ),
    takeEvery(
      POST_PURCHASE_ORDER_POSITIONS_REQUEST,
      loggingRequestSaga,
      postPurchaseOrderPositions,
      moduleName
    ),
    takeEvery(
      CHANGE_P_ORDER_POSITIONS_REQUEST,
      loggingRequestSaga,
      changePOrderPositionsComingDateSaga,
      moduleName
    ),
    takeEvery(
      CHANGE_PO_DELIVERY_TYPE_REQUEST,
      loggingRequestSaga,
      changePODeliveryTypeSaga,
      moduleName
    ),
    takeEvery(
      CHANGE_STATE_TO_ORDERED_FROM_PROVIDER_REQUEST,
      loggingRequestSaga,
      changeStateToOrderedFromProviderSaga,
      moduleName
    ),
    takeEvery(
      UPDATE_PURCHASE_ORDER_REQUEST,
      loggingRequestSaga,
      updatePurchaseOrderSaga,
      moduleName
    )
  ]);
}
