import { moduleName } from "./config";
import {
  queryRouterSelector,
  entityIdForRouteSelector
} from "../../redux/selectors/router";
import { createSelector } from "reselect";
import { clientIdSelector } from "../../ducks/clients";
import {
  maxPagesSelector as maxPagesSelectorHelper,
  typeEntityStateSelector as typeEntityStateSelectorHelper,
  entitiesForTypeEntitySelector,
  entityInfoSelector,
  entityListSelector
} from "../../helpers/selectors";
import nanoid from "nanoid";

export const purchaseOrdersSelector = entityListSelector(moduleName);
export const maxPagesSelector = maxPagesSelectorHelper(moduleName);

export const typeEntityStateSelector = typeEntityStateSelectorHelper(
  moduleName
);

export const purchaseOrdersEntitySelector = entitiesForTypeEntitySelector(
  moduleName
);

export const purchaseOrderInfoSelector = entityInfoSelector(moduleName);

export const purchaseOrderIdRouteSelector = entityIdForRouteSelector;

export const purchaseOrdersBillIdEntitySelector = createSelector(
  purchaseOrdersEntitySelector,
  ({ entities, ...rest }) => {
    return {
      ...rest,
      entities: entities.map(({ order, bill_id, ...restProps }) => ({
        ...order,
        bill_id,
        ...restProps
      }))
    };
  }
);

export const orderInfoWithBillIdSelector = createSelector(
  purchaseOrderInfoSelector,
  info => {
    if (!info) return null;
    const { order, bill_id, ...restProps } = info;

    return { ...order, bill_id, ...restProps };
  }
);

export const requestParamsByClientPageSelector = createSelector(
  queryRouterSelector,
  clientIdSelector,
  (query, id) => ({
    page_size: 10,
    page: 1,
    agent_id: id,
    sort_by: "object_date_create desc",
    ...query
  })
);

export const requestParamsOrderPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  (query, orderId) => {
    return {
      page_size: 10,
      page: 1,
      order_id: orderId,
      ...query,
      _tab: query._tab ? +query._tab : 1
    };
  }
);

export const requestParamsOrderBillsPageSelector = createSelector(
  requestParamsOrderPageSelector,
  query => {
    return {
      ...query,
      billState: query.billState ? +query.billState : 1
    };
  }
);

export const orderChangedSelector = createSelector(
  purchaseOrderInfoSelector,
  () => nanoid()
);
