import { prefix } from "./config";

export const FETCH_PURCHASE_ORDERS_REQUEST = `${prefix}/FETCH_PURCHASE_ORDERS_REQUEST`;
export const FETCH_PURCHASE_ORDERS_SUCCESS = `${prefix}/FETCH_PURCHASE_ORDERS_SUCCESS`;
export const FETCH_PURCHASE_ORDERS_ERROR = `${prefix}/FETCH_PURCHASE_ORDERS_ERROR`;

export const FETCH_PURCHASE_ORDER_REQUEST = `${prefix}/FETCH_PURCHASE_ORDER_REQUEST`;
export const FETCH_PURCHASE_ORDER_SUCCESS = `${prefix}/FETCH_PURCHASE_ORDER_SUCCESS`;
export const FETCH_PURCHASE_ORDER_ERROR = `${prefix}/FETCH_PURCHASE_ORDER_ERROR`;

export const FETCH_PURCHASE_ORDERS_ENTITY_REQUEST = `${prefix}/FETCH_PURCHASE_ORDERS_ENTITY_REQUEST`;
export const FETCH_PURCHASE_ORDERS_ENTITY_SUCCESS = `${prefix}/FETCH_PURCHASE_ORDERS_ENTITY_SUCCESS`;
export const FETCH_PURCHASE_ORDERS_ENTITY_ERROR = `${prefix}/FETCH_PURCHASE_ORDERS_ENTITY_ERROR`;

export const FETCH_PURCHASE_ORDER_CLIENT_REQUEST = `${prefix}/FETCH_ORDER_WITH_CLIENT_REQUEST`;

export const POST_PURCHASE_ORDER_REQUEST = `${prefix}/POST_PURCHASE_ORDER_REQUEST`;
export const POST_PURCHASE_ORDER_SUCCESS = `${prefix}/POST_PURCHASE_ORDER_SUCCESS`;
export const POST_PURCHASE_ORDER_ERROR = `${prefix}/POST_PURCHASE_ORDER_ERROR`;

export const PUT_PURCHASE_ORDER_REQUEST = `${prefix}/PUT_PURCHASE_ORDER_REQUEST`;
export const PUT_PURCHASE_ORDER_SUCCESS = `${prefix}/PUT_PURCHASE_ORDER_SUCCESS`;
export const PUT_PURCHASE_ORDER_ERROR = `${prefix}/PUT_PURCHASE_ORDER_ERROR`;

export const POST_PURCHASE_ORDER_POSITIONS_REQUEST = `${prefix}/POST_PURCHASE_ORDER_POSITIONS_REQUEST`;
export const POST_PURCHASE_ORDER_POSITIONS_SUCCESS = `${prefix}/POST_PURCHASE_ORDER_POSITIONS_SUCCESS`;
export const POST_PURCHASE_ORDER_POSITIONS_ERROR = `${prefix}/POST_PURCHASE_ORDER_POSITIONS_ERROR`;

export const CHANGE_P_ORDER_POSITIONS_REQUEST = `${prefix}/CHANGE_P_ORDER_POSITIONS_REQUEST`;

export const CHANGE_PO_DELIVERY_TYPE_REQUEST = `${prefix}/CHANGE_PO_DELIVERY_TYPE_REQUEST`;

export const CHANGE_STATE_TO_ORDERED_FROM_PROVIDER_REQUEST = `${prefix}/CHANGE_STATE_TO_ORDERED_FROM_PROVIDER_REQUEST`;

export const UPDATE_PURCHASE_ORDER_REQUEST = `${prefix}/UPDATE_PURCHASE_ORDER_REQUEST`;
