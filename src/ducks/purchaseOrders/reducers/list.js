import {
  FETCH_PURCHASE_ORDERS_SUCCESS,
  FETCH_PURCHASE_ORDERS_ERROR
} from "../actions";

import {
  ListStateRecord,
  fetchEntitiesSuccess,
  fetchEntitiesError
} from "../../../helpers/reducers";

const listReducer = (state = { ...ListStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_PURCHASE_ORDERS_SUCCESS:
      return fetchEntitiesSuccess(state, payload);
    case FETCH_PURCHASE_ORDERS_ERROR:
      return fetchEntitiesError(state, payload);
    default:
      return state;
  }
};

export default listReducer;
