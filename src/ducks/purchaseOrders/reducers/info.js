import { FETCH_PURCHASE_ORDER_SUCCESS } from "../actions";
import { InfoStateRecord, fetchEntitySuccess } from "../../../helpers/reducers";

const infoReducer = (state = { ...InfoStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_PURCHASE_ORDER_SUCCESS:
      return fetchEntitySuccess(state, payload);
    default:
      return state;
  }
};

export default infoReducer;
