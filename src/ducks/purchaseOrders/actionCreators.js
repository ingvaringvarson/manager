import {
  FETCH_PURCHASE_ORDER_REQUEST,
  FETCH_PURCHASE_ORDERS_REQUEST,
  FETCH_PURCHASE_ORDERS_ENTITY_REQUEST,
  POST_PURCHASE_ORDER_POSITIONS_REQUEST,
  CHANGE_P_ORDER_POSITIONS_REQUEST,
  CHANGE_STATE_TO_ORDERED_FROM_PROVIDER_REQUEST,
  CHANGE_PO_DELIVERY_TYPE_REQUEST,
  UPDATE_PURCHASE_ORDER_REQUEST
} from "./actions";

import {
  fetchEntities,
  fetchEntitiesForTypeEntity,
  fetchEntity,
  postEntity,
  putEntity
} from "../../helpers/actionCreators";

export const fetchPurchaseOrder = fetchEntity(FETCH_PURCHASE_ORDER_REQUEST);

export const fetchPurchaseOrders = fetchEntities(FETCH_PURCHASE_ORDERS_REQUEST);

export const fetchPurchaseOrdersForEntity = fetchEntitiesForTypeEntity(
  FETCH_PURCHASE_ORDERS_ENTITY_REQUEST
);

export const postPurchaseOrderPositions = postEntity(
  POST_PURCHASE_ORDER_POSITIONS_REQUEST
);

export const changePurchaseOrderPositionsComingDate = postEntity(
  CHANGE_P_ORDER_POSITIONS_REQUEST
);

export const changePODeliveryType = postEntity(CHANGE_PO_DELIVERY_TYPE_REQUEST);

export const changeStateToOrderedFromProvider = postEntity(
  CHANGE_STATE_TO_ORDERED_FROM_PROVIDER_REQUEST
);

export const updatePurchaseOrder = putEntity(UPDATE_PURCHASE_ORDER_REQUEST);
