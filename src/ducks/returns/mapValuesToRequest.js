export const normalizeReturnCreate = (values, helperValues) => {
  const { client, positions, orderId, employee, removedIds } = helperValues;
  const newPositions =
    Array.isArray(positions) &&
    positions.reduce((result, currentPos) => {
      const { _id, id } = currentPos;
      if (!removedIds.some(curId => curId === _id)) {
        result.push({
          position_id: id,
          order_id: orderId,
          count: values[`count__${_id}`],
          reason: values[`reason__${_id}`],
          purpose: values[`purpose__${_id}`]
        });
      }
      return result;
    }, []);

  return {
    agent_id: client && client.id,
    delivery_type: 1,
    pickup: {
      warehouse_id: employee && employee.warehouse_id,
      min_date_delivery: new Date()
    },
    positions: newPositions
  };
};
