import { appName } from "../../config";

export const moduleName = "returns";
export const prefix = `${appName}/${moduleName}`;
