import { moduleName } from "./config";
import { createSelector } from "reselect";
import {
  entityListSelector,
  maxPagesSelector as maxPagesSelectorHelper,
  typeEntityStateSelector as typeEntityStateSelectorHelper,
  entitiesForTypeEntitySelector,
  entityInfoSelector
} from "../../helpers/selectors";
import {
  queryRouterSelector,
  entityIdForRouteSelector
} from "../../redux/selectors/router";

export const returnListSelector = entityListSelector(moduleName);
export const returnEntityListSelector = createSelector(
  returnListSelector,
  returns => {
    return returns.map(returnEntity => returnEntity.return);
  }
);
export const maxPagesSelector = maxPagesSelectorHelper(moduleName);

export const typeEntityStateSelector = typeEntityStateSelectorHelper(
  moduleName
);

export const returnsForEntitySelector = entitiesForTypeEntitySelector(
  moduleName
);

export const returnInfoSelector = entityInfoSelector(moduleName);

export const requestParamsReturnPageSelector = createSelector(
  queryRouterSelector,
  query => {
    return {
      page_size: 20,
      page: 1,
      sort_by: "object_date_create desc",
      ...query,
      _tab: query._tab ? +query._tab : 1
    };
  }
);

export const requestParamsForPaymentsTabPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  ({ payment_method, ...query }, return_id) => {
    return {
      page: 1,
      page_size: 10,
      return_id,
      sort_by: "object_date_create desc",
      ...query,
      payment_method: payment_method ? +payment_method : 1
    };
  }
);

export const requestParamsBillsForReturnPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  (query, return_id) => {
    const _tab_state_bill = query._tab_state_bill ? +query._tab_state_bill : 1;

    return {
      page: 1,
      page_size: 20,
      return_id,
      sort_by: "object_date_create desc",
      ...query,
      _tab_state_bill,
      state: _tab_state_bill === 1 ? [0, 1, 2, 3] : 4
    };
  }
);
