import {
  FETCH_RETURNS_FOR_ENTITY_REQUEST,
  FETCH_RETURNS_REQUEST,
  FETCH_RETURN_REQUEST,
  CREATE_RETURN_REQUEST,
  UPDATE_RETURN_REQUEST
} from "./actions";
import {
  fetchEntities,
  fetchEntitiesForTypeEntity,
  fetchEntity,
  updateEntity
} from "../../helpers/actionCreators";

export const fetchReturns = fetchEntities(FETCH_RETURNS_REQUEST);
export const fetchReturn = fetchEntity(FETCH_RETURN_REQUEST);
export const fetchReturnsForEntity = fetchEntitiesForTypeEntity(
  FETCH_RETURNS_FOR_ENTITY_REQUEST
);
export const updateReturn = updateEntity(UPDATE_RETURN_REQUEST);
export const createReturn = (
  formValues,
  helperValues,
  normalizeCallback,
  shouldNotMove = false
) => {
  return {
    type: CREATE_RETURN_REQUEST,
    payload: {
      formValues,
      helperValues,
      normalizeCallback,
      shouldNotMove
    }
  };
};
