import { all, call, takeEvery, put, select } from "redux-saga/effects";
import { push } from "connected-react-router";
import { loggingRequestSaga, watchingTwinActionsSaga } from "../logger";
import { employeeInfoByUserSelector } from "../user";
import { getUrl } from "../../helpers/urls";
import {
  fetchEntitiesForTypeEntitySaga,
  fetchEntitiesSaga,
  fetchEntitySaga,
  putEntitySaga,
  postEntitiesByFormSaga
} from "../../helpers/sagas";
import { moduleName } from "./config";
import { mapRequestParamsToParamsByApiSaga } from "../../helpers/mapMethods";
import {
  FETCH_RETURNS_FOR_ENTITY_REQUEST,
  FETCH_RETURNS_FOR_ENTITY_SUCCESS,
  FETCH_RETURNS_FOR_ENTITY_ERROR,
  FETCH_RETURNS_REQUEST,
  FETCH_RETURNS_SUCCESS,
  FETCH_RETURNS_ERROR,
  FETCH_RETURN_REQUEST,
  FETCH_RETURN_SUCCESS,
  FETCH_RETURN_ERROR,
  CREATE_RETURN_REQUEST,
  CREATE_RETURN_SUCCESS,
  CREATE_RETURN_ERROR,
  UPDATE_RETURN_REQUEST,
  UPDATE_RETURN_SUCCESS,
  UPDATE_RETURN_ERROR
} from "./actions";
import { mapValuesToEntity } from "../../helpers/dataMapping";

export function* fetchReturnsSaga(action) {
  const actions = {
    success: FETCH_RETURNS_SUCCESS,
    error: FETCH_RETURNS_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "returns"
  );

  const payloadForApi = {
    ...action.payload,
    params: params
  };

  if (
    action.payload.params.hasOwnProperty("_agent_code") &&
    action.payload.params["_agent_code"]
  ) {
    const agents = yield call(
      fetchEntitySaga,
      {
        params: {
          code: action.payload.params["_agent_code"]
        }
      },
      null,
      "agents"
    );

    if (
      agents.response &&
      agents.response.payload &&
      Array.isArray(agents.response.payload.agents) &&
      agents.response.payload.agents[0] &&
      Array.isArray(agents.response.payload.agents[0].employees) &&
      agents.response.payload.agents[0].employees[0]
    ) {
      params.object_id_create =
        agents.response.payload.agents[0].employees[0].id;
    }
  }

  yield call(
    fetchEntitiesSaga,
    payloadForApi,
    actions,
    "returns",
    "returns_with_bills"
  );
}

export function* fetchReturnSaga(action) {
  const actions = {
    success: FETCH_RETURN_SUCCESS,
    error: FETCH_RETURN_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "returns"
  );

  yield call(
    fetchEntitySaga,
    {
      id: action.payload.id,
      params
    },
    actions,
    "returns",
    "returns_with_bills"
  );
}

export function* fetchReturnForEntitySaga(action) {
  const actions = {
    success: FETCH_RETURNS_FOR_ENTITY_SUCCESS,
    error: FETCH_RETURNS_FOR_ENTITY_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "orders"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  yield call(
    fetchEntitiesForTypeEntitySaga,
    payloadForApi,
    actions,
    "returns",
    "returns_with_bills"
  );
}

export function* createReturnSaga(action) {
  const { helperValues, shouldNotMove } = action.payload;
  const actions = {
    success: CREATE_RETURN_SUCCESS,
    error: CREATE_RETURN_ERROR
  };

  const employee = yield select(employeeInfoByUserSelector);
  const newHelperValues = {
    ...helperValues,
    employee
  };

  const { response } = yield call(
    postEntitiesByFormSaga,
    {
      ...action.payload,
      helperValues: newHelperValues
    },
    actions,
    "returns"
  );

  if (response && !shouldNotMove) {
    yield call(moveToPayloadReturnPage, response.payload);
  }
}

function* updateReturnSaga(action) {
  const { values, entity } = action.payload;
  const actions = {
    success: UPDATE_RETURN_SUCCESS,
    error: UPDATE_RETURN_ERROR
  };

  const payloadForApi = {
    ...action.payload,
    body: mapValuesToEntity(entity, values)
  };

  yield call(putEntitySaga, payloadForApi, actions, "returns");
}

function* moveToPayloadReturnPage(payload) {
  if (payload && payload.return && payload.return.id) {
    const { id } = payload.return;
    yield put(push(getUrl("returns", { id })));
  }
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_RETURNS_REQUEST]: [fetchReturnsSaga, moduleName],
      [FETCH_RETURN_REQUEST]: [fetchReturnSaga, moduleName],
      [FETCH_RETURNS_FOR_ENTITY_REQUEST]: [fetchReturnForEntitySaga, moduleName]
    }),
    takeEvery(
      CREATE_RETURN_REQUEST,
      loggingRequestSaga,
      createReturnSaga,
      moduleName
    ),
    takeEvery(
      UPDATE_RETURN_REQUEST,
      loggingRequestSaga,
      updateReturnSaga,
      moduleName
    )
  ]);
}
