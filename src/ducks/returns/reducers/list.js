import { FETCH_RETURNS_SUCCESS, FETCH_RETURNS_ERROR } from "../actions";
import {
  ListStateRecord,
  fetchEntitiesSuccess,
  fetchEntitiesError
} from "../../../helpers/reducers";

const listReducer = (state = { ...ListStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_RETURNS_SUCCESS:
      return fetchEntitiesSuccess(state, payload);
    case FETCH_RETURNS_ERROR:
      return fetchEntitiesError(state, payload);
    default:
      return state;
  }
};

export default listReducer;
