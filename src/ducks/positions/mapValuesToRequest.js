import { typeMethods } from "./helpers";

export function mapPositionsDeviding(
  formValues,
  positions,
  entity,
  typeMethod
) {
  let body = null;

  const normalizedPositionValues = positions.reduce(
    (result, currentPosition) => {
      const { _id } = currentPosition;
      result[_id] = Object.keys(formValues).reduce(
        (formValueResult, formValueKey) => {
          if (formValueKey.includes(_id)) {
            const [valueKey, posId] = formValueKey.split("__");
            formValueResult[valueKey] = formValues[formValueKey];
          }
          return formValueResult;
        },
        {}
      );
      return result;
    },
    {}
  );

  switch (typeMethod) {
    case typeMethods.orders:
      body = {
        order_id: entity.id,
        positions: positions.map(position => {
          return {
            ...position,
            count: +normalizedPositionValues[position._id].devide
          };
        })
      };
      break;
    case typeMethods.purchaseOrders:
      body = {
        purchase_order_id: entity.order.id,
        positions: positions.map(position => {
          return {
            ...position,
            count: +normalizedPositionValues[position._id].devide
          };
        })
      };
      break;
    case typeMethods.returns:
      body = {
        return_id: entity.return.id,
        positions: positions.map(position => {
          return {
            ...position,
            position: {
              ...position.position,
              count: +normalizedPositionValues[position._id].devide
            }
          };
        })
      };
      break;
    case typeMethods.purchaseReturns:
      body = {
        purchase_return_id: entity.purchase_return.id,
        positions: positions.map(position => {
          return {
            ...position,
            position: {
              ...position.position,
              count: +normalizedPositionValues[position._id].devide
            }
          };
        })
      };
      break;
    default:
      break;
  }
  return body;
}

export function mapPositionsRejectForEntity(
  formValues,
  positions,
  entity,
  typeMethod
) {
  let body = null;

  switch (typeMethod) {
    case typeMethods.orders:
      body = {
        order_id: entity.id,
        positions: positions.map(p => ({
          ...p,
          good_state: 10,
          count: formValues[`reject__${p._id}`]
        }))
      };
      break;
    case typeMethods.returns:
      body = {
        return_id: entity.return.id,
        positions: positions.map(p => ({
          ...p,
          position: {
            ...p.position,
            good_state: 24,
            count: formValues[`reject__${p._id}`]
          }
        }))
      };
      break;
    case typeMethods.purchaseOrders:
      body = {
        purchase_order_id: entity.order.id,
        positions: positions.map(p => ({
          ...p,
          good_state: formValues.good_state,
          count: formValues[`reject__${p._id}`]
        }))
      };
      break;
    case typeMethods.purchaseReturns:
      body = {
        purchase_return_id: entity.purchase_return.id,
        positions: positions.map(p => ({
          ...p,
          position: {
            ...p.position,
            good_state: 24,
            count: formValues[`reject__${p._id}`]
          }
        }))
      };
    default:
      break;
  }

  return body;
}
