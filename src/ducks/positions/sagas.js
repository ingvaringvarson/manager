import { all, call, takeEvery, fork } from "redux-saga/effects";
import {
  POSITIONS_DIVIDE_REQUEST,
  POSITIONS_DIVIDE_SUCCESS,
  POSITIONS_DIVIDE_ERROR,
  POSITIONS_REJECT_REQUEST,
  POSITIONS_REJECT_SUCCESS,
  POSITIONS_REJECT_ERROR
} from "./actions";
import { moduleName } from "./config";
import { loggingRequestSaga } from "../logger";
import { postEntitySaga } from "../../helpers/sagas";
import { repeatLastActionSaga } from "../lastAction";

import {
  mapPositionsDeviding,
  mapPositionsRejectForEntity
} from "./mapValuesToRequest";

function* positionsDevidingSaga(
  action = {
    payload: {
      positions: [],
      typeMethod: "",
      entity: {},
      formValues: {}
    }
  }
) {
  const {
    positions,
    typeMethod,
    entity,
    formValues,
    fetchEntities
  } = action.payload;

  const actions = {
    success: POSITIONS_DIVIDE_SUCCESS,
    error: POSITIONS_DIVIDE_ERROR
  };

  const body = yield call(
    mapPositionsDeviding,
    formValues,
    positions,
    entity,
    typeMethod
  );

  if (body) {
    const { response } = yield call(
      postEntitySaga,
      { body },
      actions,
      typeMethod
    );
    yield fork(repeatLastActionSaga);

    if (response && fetchEntities) {
      fetchEntities();
    }
  }
}

function* positionsRejectSaga(action) {
  const {
    positions,
    typeMethod,
    entities,
    formValues,
    successCallback
  } = action.payload;

  const actions = {
    success: POSITIONS_REJECT_SUCCESS,
    error: POSITIONS_REJECT_ERROR
  };

  yield all(
    entities.map(entity => {
      const body = mapPositionsRejectForEntity(
        formValues,
        positions,
        entity,
        typeMethod
      );

      return body && call(postEntitySaga, { body }, actions, typeMethod);
    })
  );

  if (successCallback) {
    yield call(successCallback);
  }
}

export function* rootSaga() {
  yield all([
    takeEvery(
      POSITIONS_DIVIDE_REQUEST,
      loggingRequestSaga,
      positionsDevidingSaga,
      moduleName
    ),
    takeEvery(
      POSITIONS_REJECT_REQUEST,
      loggingRequestSaga,
      positionsRejectSaga,
      moduleName
    )
  ]);
}
