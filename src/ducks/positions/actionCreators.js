import { POSITIONS_DIVIDE_REQUEST, POSITIONS_REJECT_REQUEST } from "./actions";

export const positionsDeviding = (
  positions,
  typeMethod,
  entity,
  formValues,
  fetchEntities
) => {
  return {
    type: POSITIONS_DIVIDE_REQUEST,
    payload: {
      positions,
      typeMethod,
      entity,
      formValues,
      fetchEntities
    }
  };
};

export const positionsReject = (
  positions,
  typeMethod,
  entities,
  formValues,
  successCallback
) => {
  return {
    type: POSITIONS_REJECT_REQUEST,
    payload: {
      positions,
      typeMethod,
      entities,
      formValues,
      successCallback
    }
  };
};
