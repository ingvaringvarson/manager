export const typeMethods = {
  orders: "orderPositions",
  purchaseOrders: "purchaseOrderPositions",
  returns: "returnPositions",
  purchaseReturns: "purchaseReturnPositions"
};
