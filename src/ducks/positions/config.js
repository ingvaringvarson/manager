import { appName } from "../../config";

export const moduleName = "positions";
export const prefix = `${appName}/${moduleName}`;
