import { appName, apiUri } from "../../config";

export const moduleName = "maxoptra";
export const prefix = `${appName}/${moduleName}`;
export const apiPrefix = `${apiUri}${moduleName}`;
