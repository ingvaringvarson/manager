import { prefix } from "./config";

export const FETCH_MAXOPTRA_FILE_REQUEST = `${prefix}/FETCH_MAXOPTRA_FILE_REQUEST`;
export const FETCH_MAXOPTRA_FILE_SUCCESS = `${prefix}/FETCH_MAXOPTRA_FILE_SUCCESS`;
