import { all, takeEvery } from "redux-saga/effects";
import { FETCH_MAXOPTRA_FILE_REQUEST } from "./actions";
import { generateGetApiUrl } from "./apiUrls";

export function* fetchFileSaga(action) {
  const { orders } = action.payload;
  const orderIds = orders.map(o => o.id);
  window.open(generateGetApiUrl(orderIds), "_blank");
}

export function* rootSaga() {
  yield all([takeEvery(FETCH_MAXOPTRA_FILE_REQUEST, fetchFileSaga)]);
}
