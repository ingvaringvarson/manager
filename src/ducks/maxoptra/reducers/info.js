import { FETCH_MAXOPTRA_FILE_SUCCESS } from "../actions";
import { InfoStateRecord, fetchEntitySuccess } from "../../../helpers/reducers";

export default (state = { ...InfoStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_MAXOPTRA_FILE_SUCCESS:
      return fetchEntitySuccess(state, payload);
    default:
      return state;
  }
};
