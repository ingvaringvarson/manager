import { FETCH_MAXOPTRA_FILE_REQUEST } from "./actions";

export const fetchMaxoptraFile = orders => ({
  type: FETCH_MAXOPTRA_FILE_REQUEST,
  payload: { orders }
});
