import { apiPrefix } from "./config";

export const generateGetApiUrl = orderIds => {
  const query = orderIds.map(id => `orderIds=${id}`).join("&");
  return `${apiPrefix}/orders?${query}`;
};
