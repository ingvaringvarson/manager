import { appName } from "../../config";
import { all, put, select, takeEvery } from "redux-saga/effects";
import { combineReducers } from "redux";

// config
const moduleName = "lastActions";

// actions
const REPLACE_LAST_ACTION = `${appName}/${moduleName}/REPLACE_LAST_ACTION`;

// actionCreator
const replaceLastAction = action => ({
  type: REPLACE_LAST_ACTION,
  payload: action
});

// selectors
const stateSelector = state => state[moduleName];
const stateInfoSelector = state => stateSelector(state).info;

// sagas
function* replaceLastActionSaga(action) {
  action.isForRepeat && (yield put(replaceLastAction(action)));
}

function* repeatLastActionSaga() {
  const lastActionInfo = yield select(stateInfoSelector);
  lastActionInfo.type && (yield put(lastActionInfo));
}

function* rootSaga() {
  yield all([takeEvery("*", replaceLastActionSaga)]);
}

// reducers
const StateRecord = {};
function info(state = StateRecord, action) {
  const { type, payload } = action;

  switch (type) {
    case REPLACE_LAST_ACTION:
      return payload;
    default:
      return state;
  }
}

export default combineReducers({ info });

export { moduleName, rootSaga, repeatLastActionSaga };
