import { all, call, put, takeLeading, select } from "redux-saga/effects";
import { FETCH_CDEK_INFO_SUCCESS, FETCH_CDEK_INFO_REQUEST } from "./actions";
import { loggingRequestSaga, fetchApiSaga } from "../logger";
import { moduleName } from "./config";
import { isLoadedSelector } from "./selectors";
import { getCdekInfo } from "../../api/cdek";

export function* fetchCdekInfoSaga(action) {
  const isLoaded = yield select(isLoadedSelector);

  if (isLoaded) return;

  const paramsRequest = getCdekInfo(action.payload.params);
  const { response } = yield call(fetchApiSaga, paramsRequest);

  if (response && response.payload && response.payload.pvz) {
    yield put({
      type: FETCH_CDEK_INFO_SUCCESS,
      payload: {
        entities: response.payload.pvz
      }
    });
  }
}

export function* rootSaga() {
  yield all([
    takeLeading(
      FETCH_CDEK_INFO_REQUEST,
      loggingRequestSaga,
      fetchCdekInfoSaga,
      moduleName
    )
  ]);
}
