import { prefix } from "./config";

export const FETCH_CDEK_INFO_REQUEST = `${prefix}/FETCH_CDEK_INFO_REQUEST`;
export const FETCH_CDEK_INFO_SUCCESS = `${prefix}/FETCH_CDEK_INFO_SUCCESS`;
