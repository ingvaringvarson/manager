import { moduleName } from "./config";
import { createSelector } from "reselect";

export const stateSelector = state => state[moduleName];
export const pointsSelector = state => stateSelector(state).entities;
export const isLoadedSelector = state => stateSelector(state).isLoaded;
export const cdekInfoSelector = createSelector(
  pointsSelector,
  points =>
    points.reduce(
      (info, point) => {
        const entity = { ...point, id: point.code };
        if (info.pointsByCity.hasOwnProperty(entity.cityCode)) {
          info.pointsByCity[entity.cityCode].push(entity);
        } else {
          info.pointsByCity[entity.cityCode] = [entity];
          info.cities.push({ id: entity.cityCode, name: entity.city });
        }

        return info;
      },
      { cities: [], pointsByCity: {} }
    )
);

const requestParams = { countryId: 1 };

export const requestParamsSelector = () => requestParams;
