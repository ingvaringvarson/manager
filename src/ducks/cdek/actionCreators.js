import { FETCH_CDEK_INFO_REQUEST } from "./actions";

export function fetchCdekInfo(params) {
  return {
    type: FETCH_CDEK_INFO_REQUEST,
    payload: { params }
  };
}
