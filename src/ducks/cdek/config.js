import { appName } from "../../config";

export const moduleName = "cdek";
export const prefix = `${appName}/${moduleName}`;
