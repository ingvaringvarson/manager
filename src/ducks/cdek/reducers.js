import { FETCH_CDEK_INFO_SUCCESS } from "./actions";

const StateRecord = {
  isLoaded: false,
  entities: []
};

function reducer(state = StateRecord, action) {
  const { type, payload } = action;

  switch (type) {
    case FETCH_CDEK_INFO_SUCCESS:
      return fetchCdekInfoSuccess(state, payload);
    default:
      return state;
  }
}

function fetchCdekInfoSuccess(state, payload) {
  return {
    isLoaded: true,
    entities: payload.entities
  };
}

export default reducer;
