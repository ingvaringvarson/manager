import {
  FETCH_TEMPLATES_FOR_ENTITY_SUCCESS,
  FETCH_TEMPLATES_FOR_ENTITY_ERROR
} from "../actions";
import {
  ListStateRecord,
  fetchEntitiesForTypeEntitySuccess,
  fetchEntitiesForTypeEntityError
} from "../../../helpers/reducers";

export default (state = { ...ListStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_TEMPLATES_FOR_ENTITY_SUCCESS:
      return fetchEntitiesForTypeEntitySuccess(state, payload);
    case FETCH_TEMPLATES_FOR_ENTITY_ERROR:
      return fetchEntitiesForTypeEntityError(state, payload);
    default:
      return state;
  }
};
