import { appName } from "../../config";

export const moduleName = "templates";
export const prefix = `${appName}/${moduleName}`;
export const typeEntity = "templates";
