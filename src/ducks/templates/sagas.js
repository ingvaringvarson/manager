import { all, call, put, takeEvery } from "redux-saga/effects";
import {
  FETCH_TEMPLATES_FOR_ENTITY_REQUEST,
  FETCH_TEMPLATES_FOR_ENTITY_SUCCESS,
  FETCH_TEMPLATES_FOR_ENTITY_ERROR,
  CREATE_TEMPLATES_REQUEST,
  CREATE_TEMPLATES_SUCCESS,
  CREATE_TEMPLATES_ERROR
} from "./actions";
import { postCommonSaga, fetchEntitiesCommonById } from "../../helpers/sagas";
import {
  loggingRequestSaga,
  addNotification,
  removeNotification,
  watchingTwinActionsSaga
} from "../logger";
import { typeEntity, moduleName } from "./config";
import { generateApiUrlPrefix } from "./apiUrls";
import { fetchFileSaga } from "../files";
import { generateId } from "../../utils";
import { mapFileTofetchFileSagaAction } from "./mapValuesToRequest";

function* fetchTemplatesForEntitySaga(action) {
  const actions = {
    success: FETCH_TEMPLATES_FOR_ENTITY_SUCCESS,
    error: FETCH_TEMPLATES_FOR_ENTITY_ERROR
  };

  const { docType, docId } = action.payload;

  return yield call(
    fetchEntitiesCommonById,
    actions,
    docType,
    docId,
    "templates",
    generateApiUrlPrefix(docType, docId)
  );
}

function* createTemplatesSaga(action) {
  const actions = {
    success: CREATE_TEMPLATES_SUCCESS,
    error: CREATE_TEMPLATES_ERROR
  };
  const { formValues, helperValues, normalizeCallback } = action.payload;

  const payloadForApi = {
    body:
      typeof normalizeCallback === "function"
        ? yield call(normalizeCallback, formValues, helperValues)
        : formValues
  };

  const id = generateId();

  yield put(
    addNotification({
      id,
      loading: true,
      message: `Файлы подготавливаются для печати`
    })
  );

  const { response } = yield call(
    postCommonSaga,
    payloadForApi,
    actions,
    typeEntity
  );

  if (response) {
    const files = response.payload.files;
    for (let i = 0; i < files.length; i++) {
      yield call(fetchFileSaga, mapFileTofetchFileSagaAction(files[i]));
    }
  }

  yield put(removeNotification({ id }));

  return response;
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_TEMPLATES_FOR_ENTITY_REQUEST]: [
        fetchTemplatesForEntitySaga,
        moduleName
      ]
    }),
    takeEvery(
      CREATE_TEMPLATES_REQUEST,
      loggingRequestSaga,
      createTemplatesSaga,
      moduleName
    )
  ]);
}
