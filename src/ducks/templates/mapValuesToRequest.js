const mapFileTofetchFileSagaAction = file => ({
  payload: {
    fileId: file.id
  }
});

export { mapFileTofetchFileSagaAction };
