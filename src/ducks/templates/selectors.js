import { moduleName } from "./config";
import { entitiesForTypeEntitySelector } from "../../helpers/selectors";
import { createSelector } from "reselect";

export const templatesForEntitySelector = entitiesForTypeEntitySelector(
  moduleName
);

const availableIdsSelector = (state, props) =>
  Array.isArray(props.availableIds) ? props.availableIds : null;

export const availableTemplatesForEntitySelector = createSelector(
  templatesForEntitySelector,
  availableIdsSelector,
  (templates, availableIds) => {
    return availableIds && templates.entities.length
      ? templates.entities.filter(t => availableIds.includes(t.id))
      : templates.entities;
  }
);
