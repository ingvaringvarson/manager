import {
  FETCH_TEMPLATES_FOR_ENTITY_REQUEST,
  CREATE_TEMPLATES_REQUEST
} from "./actions";
import { postEntity } from "../../helpers/actionCreators";

export const fetchTemplates = (docType, docId) => {
  return {
    type: FETCH_TEMPLATES_FOR_ENTITY_REQUEST,
    payload: { docType, docId }
  };
};

export const createTemplates = postEntity(CREATE_TEMPLATES_REQUEST);
