import { moduleName } from "./config";
import {
  entitiesForTypeEntitySelector,
  entityInfoSelector
} from "../../helpers/selectors";

export const fileslinkInfoSelector = entityInfoSelector(moduleName);

export const fileslinkForObjectSelector = entitiesForTypeEntitySelector(
  moduleName
);
