import { appName } from "../../config";

export const moduleName = "fileslink";
export const prefix = `${appName}/${moduleName}`;
export const typeEntity = "fileslink";
export const payloadPropName = "files";
