import {
  FETCH_FILESLINK_FOR_OBJECT_SUCCESS,
  FETCH_FILESLINK_FOR_OBJECT_ERROR
} from "../actions";
import {
  EmptyStateRecord,
  fetchEntitiesForTypeEntityError,
  fetchEntitiesForTypeEntitySuccess
} from "../../../helpers/reducers";

export default (state = { ...EmptyStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_FILESLINK_FOR_OBJECT_SUCCESS:
      return fetchEntitiesForTypeEntitySuccess(state, payload);
    case FETCH_FILESLINK_FOR_OBJECT_ERROR:
      return fetchEntitiesForTypeEntityError(state, payload);
    default:
      return state;
  }
};
