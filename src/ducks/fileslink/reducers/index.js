import { combineReducers } from "redux";

import info from "./info";
import listForEntity from "./listForEntity";

export default combineReducers({
  info,
  listForEntity
});
