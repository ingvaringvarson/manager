import { FETCH_FILESLINK_FOR_ENTITY_SUCCESS } from "../actions";
import { InfoStateRecord, fetchEntitySuccess } from "../../../helpers/reducers";

export default (state = { ...InfoStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_FILESLINK_FOR_ENTITY_SUCCESS:
      return fetchEntitySuccess(state, payload);
    default:
      return state;
  }
};
