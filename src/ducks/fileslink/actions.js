import { prefix } from "./config";

export const FETCH_FILESLINK_FOR_OBJECT_REQUEST = `${prefix}/FETCH_FILESLINK_FOR_OBJECT_REQUEST`;
export const FETCH_FILESLINK_FOR_OBJECT_SUCCESS = `${prefix}/FETCH_FILESLINK_FOR_OBJECT_SUCCESS`;
export const FETCH_FILESLINK_FOR_OBJECT_ERROR = `${prefix}/FETCH_FILESLINK_FOR_OBJECT_ERROR`;

export const FETCH_FILESLINK_FOR_ENTITY_REQUEST = `${prefix}/FETCH_FILESLINK_FOR_ENTITY_REQUEST`;
export const FETCH_FILESLINK_FOR_ENTITY_SUCCESS = `${prefix}/FETCH_FILESLINK_FOR_ENTITY_SUCCESS`;
export const FETCH_FILESLINK_FOR_ENTITY_ERROR = `${prefix}/FETCH_FILESLINK_FOR_ENTITY_ERROR`;

export const CREATE_FILESLINK_FOR_OBJECT_REQUEST = `${prefix}/CREATE_FILESLINK_FOR_OBJECT_REQUEST`;
export const CREATE_FILESLINK_FOR_OBJECT_SUCCESS = `${prefix}/CREATE_FILESLINK_FOR_OBJECT_SUCCESS`;
export const CREATE_FILESLINK_FOR_OBJECT_ERROR = `${prefix}/CREATE_FILESLINK_FOR_OBJECT_ERROR`;

export const UPDATE_FILESLINK_REQUEST = `${prefix}/UPDATE_FILESLINK_REQUEST`;
export const UPDATE_FILESLINK_SUCCESS = `${prefix}/UPDATE_FILESLINK_SUCCESS`;
export const UPDATE_FILESLINK_ERROR = `${prefix}/UPDATE_FILESLINK_ERROR`;

export const DELETE_FILESLINK_REQUEST = `${prefix}/DELETE_FILESLINK_REQUEST`;
export const DELETE_FILESLINK_SUCCESS = `${prefix}/DELETE_FILESLINK_SUCCESS`;
export const DELETE_FILESLINK_ERROR = `${prefix}/DELETE_FILESLINK_ERROR`;

export const UPLOAD_AND_LINK_FILES_REQUEST = `${prefix}/UPLOAD_AND_LINK_FILES_REQUEST`;
export const UPLOAD_AND_LINK_FILES_SUCCESS = `${prefix}/UPLOAD_AND_LINK_FILES_SUCCESS`;
export const UPLOAD_AND_LINK_FILES_ERROR = `${prefix}/UPLOAD_AND_LINK_FILES_ERROR`;
