export function fileslinkByCreatedFileId(fileMeta, createdFileId) {
  const { docId, docType, isMain } = fileMeta;

  return {
    id: docId,
    helperValues: {
      docId,
      docType
    },
    formValues: {
      id: createdFileId,
      is_main: isMain
    },
    normalizeCallback: null
  };
}

export function getClientsRequestParams(filesLinksPayload, idEntity) {
  const setOfagentIds = filesLinksPayload.files.reduce((set, f) => {
    const firmId = f.template && f.template.firm_id;
    if (firmId) {
      set.add(firmId);
    }
    return set;
  }, new Set());

  if (!setOfagentIds.size) {
    return null;
  }

  return {
    payload: {
      idEntity,
      typeEntity: "agents",
      params: {
        id: Array.from(setOfagentIds)
      }
    }
  };
}
