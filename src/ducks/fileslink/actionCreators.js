import {
  FETCH_FILESLINK_FOR_ENTITY_REQUEST,
  FETCH_FILESLINK_FOR_OBJECT_REQUEST,
  CREATE_FILESLINK_FOR_OBJECT_REQUEST,
  UPDATE_FILESLINK_REQUEST,
  DELETE_FILESLINK_REQUEST,
  UPLOAD_AND_LINK_FILES_REQUEST
} from "./actions";
import { postEntity, putEntity } from "../../helpers/actionCreators";

export const fetchFileslinkForEntity = fileslinkId => ({
  type: FETCH_FILESLINK_FOR_ENTITY_REQUEST,
  payload: { fileslinkId }
});
export const fetchFileslinkForObject = (docType, docId) => {
  return {
    type: FETCH_FILESLINK_FOR_OBJECT_REQUEST,
    payload: { docType, docId }
  };
};
export const createFileslinkForObject = postEntity(
  CREATE_FILESLINK_FOR_OBJECT_REQUEST
);
export const updateFileslink = putEntity(UPDATE_FILESLINK_REQUEST);
export const deleteFileslink = fileslinkId => {
  return {
    type: DELETE_FILESLINK_REQUEST,
    payload: { fileslinkId }
  };
};
export const uploadAndLinkFiles = filesToLink => ({
  type: UPLOAD_AND_LINK_FILES_REQUEST,
  payload: { filesToLink }
});
