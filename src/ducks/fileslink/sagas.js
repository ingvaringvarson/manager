import { all, call, takeEvery, put } from "redux-saga/effects";
import {
  FETCH_FILESLINK_FOR_OBJECT_REQUEST,
  FETCH_FILESLINK_FOR_OBJECT_SUCCESS,
  FETCH_FILESLINK_FOR_OBJECT_ERROR,
  FETCH_FILESLINK_FOR_ENTITY_REQUEST,
  FETCH_FILESLINK_FOR_ENTITY_SUCCESS,
  FETCH_FILESLINK_FOR_ENTITY_ERROR,
  CREATE_FILESLINK_FOR_OBJECT_REQUEST,
  CREATE_FILESLINK_FOR_OBJECT_SUCCESS,
  CREATE_FILESLINK_FOR_OBJECT_ERROR,
  UPDATE_FILESLINK_REQUEST,
  UPDATE_FILESLINK_SUCCESS,
  UPDATE_FILESLINK_ERROR,
  DELETE_FILESLINK_REQUEST,
  DELETE_FILESLINK_SUCCESS,
  DELETE_FILESLINK_ERROR,
  UPLOAD_AND_LINK_FILES_REQUEST,
  UPLOAD_AND_LINK_FILES_SUCCESS,
  UPLOAD_AND_LINK_FILES_ERROR
} from "./actions";
import {
  postEntitiesByFormSaga,
  putEntitiesByFormSaga,
  deleteCommonSaga
} from "../../helpers/sagas";
import { uploadFileSaga } from "../files";
import { loggingRequestSaga, watchingTwinActionsSaga } from "../logger";
import { moduleName, payloadPropName } from "./config";
import {
  fileslinkByCreatedFileId,
  getClientsRequestParams
} from "./mapValuesToRequest";
import {
  generateApiUrlForObject,
  generateApiUrlForEntity,
  commonApiUrl,
  generateDeleteApiUrl
} from "./apiUrls";
import { getEntitiesCommon } from "../../api/requestByTypeEntities";
import { fetchApiSaga } from "../logger";
import { fetchClientsForEntitySaga } from "../clients";

function* fetchFileslinkForObjectSaga(action) {
  const actions = {
    success: FETCH_FILESLINK_FOR_OBJECT_SUCCESS,
    error: FETCH_FILESLINK_FOR_OBJECT_ERROR
  };
  const { docType, docId } = action.payload;
  let requestParams = getEntitiesCommon(
    generateApiUrlForObject(docType, docId)
  );
  const { response, error } = yield call(fetchApiSaga, requestParams);

  if (response && response.payload) {
    requestParams = getClientsRequestParams(response.payload, docId);

    if (requestParams) {
      yield call(fetchClientsForEntitySaga, requestParams);
    }

    yield put({
      type: actions.success,
      payload: {
        idEntity: docId,
        typeEntity: docType,
        entities: response.payload && response.payload[payloadPropName]
      }
    });
  } else if (error) {
    yield put({
      type: actions.error,
      payload: { error }
    });
  }
}

function* fetchFileslinkForEntitySaga(action) {
  const actions = {
    success: FETCH_FILESLINK_FOR_ENTITY_SUCCESS,
    error: FETCH_FILESLINK_FOR_ENTITY_ERROR
  };
  const { fileslinkId } = action.payload;
  const requestParams = getEntitiesCommon(generateApiUrlForEntity(fileslinkId));
  const { response, error } = yield call(fetchApiSaga, requestParams);

  if (response && response.payload) {
    yield put({
      type: actions.success,
      payload: {
        id: fileslinkId,
        entity:
          response.payload &&
          response.payload[payloadPropName] &&
          response.payload[payloadPropName][0]
      }
    });
  } else if (error) {
    yield put({
      type: actions.error,
      payload: { error }
    });
  }
}

function* createFileslinkForObjectSaga(action) {
  const { docId, docType } = action.payload.helperValues;
  const actions = {
    success: CREATE_FILESLINK_FOR_OBJECT_SUCCESS,
    error: CREATE_FILESLINK_FOR_OBJECT_ERROR
  };

  return yield call(
    postEntitiesByFormSaga,
    action.payload,
    actions,
    generateApiUrlForObject(docType, docId)
  );
}

function* updateFileslinkSaga(action) {
  const actions = {
    success: UPDATE_FILESLINK_SUCCESS,
    error: UPDATE_FILESLINK_ERROR
  };

  yield call(putEntitiesByFormSaga, action.payload, actions, commonApiUrl);
}

function* deleteFileslinkSaga(action) {
  const actions = {
    success: DELETE_FILESLINK_SUCCESS,
    error: DELETE_FILESLINK_ERROR
  };
  const { fileslinkId } = action.payload;

  yield call(
    deleteCommonSaga,
    { idEntity: fileslinkId },
    actions,
    generateDeleteApiUrl(fileslinkId)
  );
}

const getActionfromFilesToLink = filesToLink => {
  const firstFile = filesToLink[0];
  const { docId, docType } = firstFile;
  return {
    payload: {
      docId,
      docType
    }
  };
};

function* uploadAndLinkFilesSaga(action) {
  const { filesToLink } = action.payload;
  const requestResult = yield uploadFileByFile(filesToLink);
  yield logRequestResult(requestResult);
  yield fetchFileslinkForObjectSaga(getActionfromFilesToLink(filesToLink));
  return requestResult;
}

function* uploadFileByFile(filesToLink) {
  let requestResult, payload;

  for (const fileMeta of filesToLink) {
    payload = { file: fileMeta.file };
    requestResult = yield uploadFileSaga({ payload });

    const createdFileId =
      requestResult.response &&
      requestResult.response.payload &&
      requestResult.response.payload.id;
    if (!createdFileId) {
      break;
    }

    payload = fileslinkByCreatedFileId(fileMeta, createdFileId);
    requestResult = yield createFileslinkForObjectSaga({ payload });
    if (!requestResult.response) {
      break;
    }
  }

  return requestResult;
}

function* logRequestResult(requestResult) {
  const { response, error } = requestResult;
  if (response) {
    yield put({
      type: UPLOAD_AND_LINK_FILES_SUCCESS,
      payload: { lastResponse: response }
    });
  } else if (error) {
    yield put({
      type: UPLOAD_AND_LINK_FILES_ERROR,
      payload: { error }
    });
  }
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_FILESLINK_FOR_OBJECT_REQUEST]: [
        fetchFileslinkForObjectSaga,
        moduleName
      ],
      [FETCH_FILESLINK_FOR_ENTITY_REQUEST]: [
        fetchFileslinkForEntitySaga,
        moduleName
      ]
    }),
    takeEvery(
      CREATE_FILESLINK_FOR_OBJECT_REQUEST,
      loggingRequestSaga,
      createFileslinkForObjectSaga,
      moduleName
    ),
    takeEvery(
      UPDATE_FILESLINK_REQUEST,
      loggingRequestSaga,
      updateFileslinkSaga,
      moduleName
    ),
    takeEvery(
      DELETE_FILESLINK_REQUEST,
      loggingRequestSaga,
      deleteFileslinkSaga,
      moduleName
    ),
    takeEvery(
      UPLOAD_AND_LINK_FILES_REQUEST,
      loggingRequestSaga,
      uploadAndLinkFilesSaga,
      moduleName
    )
  ]);
}
