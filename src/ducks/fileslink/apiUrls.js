export const generateApiUrlForObject = (objType, objId) => {
  return `fileslink/${objType}/${objId}`;
};
export const generateApiUrlForEntity = linkId => {
  return `fileslink/${linkId}`;
};
export const generateDeleteApiUrl = linkId => {
  return `fileslink/by?id=${linkId}`;
};
export const commonApiUrl = "fileslink";
