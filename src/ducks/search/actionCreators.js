import {
  SEARCH_BRANDS_REQUEST,
  UPDATE_PAGE_FILTERS,
  RESET_PAGE_FILTERS,
  CHANGE_BASKET_ATTACHED_PAGE,
  CHANGE_AGENT_ATTACHED_PAGE
} from "./actions";

export function searchBrands(searchString, id) {
  return {
    type: SEARCH_BRANDS_REQUEST,
    payload: {
      searchString,
      id
    }
  };
}

export function updatePageFilters(filters) {
  return {
    type: UPDATE_PAGE_FILTERS,
    payload: filters
  };
}

export function resetPageFilters() {
  return {
    type: RESET_PAGE_FILTERS
  };
}

export function changeAgentAttachedPage(agentCode) {
  return {
    type: CHANGE_AGENT_ATTACHED_PAGE,
    payload: {
      agentCode
    }
  };
}

export function changeBasketAttachedPage(basketId) {
  return {
    type: CHANGE_BASKET_ATTACHED_PAGE,
    payload: {
      basketId
    }
  };
}
