import { createSelector } from "reselect";
import memoizeOne from "memoize-one";

import { moduleName } from "./config";

import { queryRouterSelector } from "../../redux/selectors/router";
import { generateId } from "../../utils";
import { basketInfoSelector } from "../baskets";

const stateSelector = state => state[moduleName];

export const brandsSelector = (state, props) => {
  return stateSelector(state, props).brands;
};

const getBrandRequestId = memoizeOne(() => generateId());

export const brandRequestIdSelector = (state, props) => {
  const query = queryRouterSelector(state, props);

  return getBrandRequestId(query.searchString);
};

const brandsByIdSelector = (state, props) => {
  const brands = brandsSelector(state, props);
  const brandsResult = brands[props.idEntity];
  const brandsEntities = brandsResult && brandsResult.entities;
  return brandsEntities && brandsEntities.length ? brandsEntities : [];
};

export const brandsVendorCodesSelector = createSelector(
  brandsByIdSelector,
  brands => {
    return brands.map(b => ({
      id: b.id,
      brandKey: b.brandKey,
      vendorCode: b.mainVendorCode.vendorCodeKey,
      name: b.id
    }));
  }
);

export const pageFiltersSelector = (state, props) => {
  return stateSelector(state, props).pageFilters;
};

export const basketAttachedInfoSelector = (state, props) => {
  const { basketId } = pageFiltersSelector(state, props);

  return basketId
    ? basketInfoSelector(state, { ...props, id: basketId })
    : null;
};
