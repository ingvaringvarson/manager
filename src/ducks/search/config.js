import { appName } from "../../config";

export const moduleName = "search";
export const prefix = `${appName}/${moduleName}`;
