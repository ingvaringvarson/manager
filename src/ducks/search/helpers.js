export function normalizeBrands(entities) {
  return entities.map(entity => {
    const actualVendorCode = entity.vendorCodes.find(item => item.isActual);

    return {
      mainVendorCode:
        actualVendorCode || entity.vendorCodes.length
          ? entity.vendorCodes[0]
          : null,
      ...entity
    };
  });
}
