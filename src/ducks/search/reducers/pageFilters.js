import { UPDATE_PAGE_FILTERS, RESET_PAGE_FILTERS } from "../actions";

const StateRecord = {
  cityId: 2,
  agent: null,
  basketId: null,
  discountGroupId: null
};

function reducer(state = StateRecord, action) {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_PAGE_FILTERS:
      return updatePageFilters(state, payload);
    case RESET_PAGE_FILTERS:
      return resetPageFilters(state, payload);
    default:
      return state;
  }
}

function updatePageFilters(state, payload) {
  return {
    ...state,
    ...payload
  };
}

function resetPageFilters() {
  return StateRecord;
}

export default reducer;
