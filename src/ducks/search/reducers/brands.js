import { SEARCH_BRANDS_SUCCESS, SEARCH_BRANDS_ERROR } from "../actions";
import { normalizeBrands } from "../helpers";

const StateRecord = {};

const EntityRecord = {
  entities: [],
  isLoaded: false
};

function reducerBrands(state = { ...StateRecord }, action) {
  const { type, payload } = action;

  switch (type) {
    case SEARCH_BRANDS_SUCCESS:
      return searchBrandsSuccess(state, payload);
    case SEARCH_BRANDS_ERROR:
      return searchBrandsError(state, payload);
    default:
      return state;
  }
}

function searchBrandsSuccess(state, payload) {
  return {
    ...state,
    [payload.id]: {
      ...EntityRecord,
      entities: normalizeBrands(payload.entities),
      isLoaded: true
    }
  };
}

function searchBrandsError(state, payload) {
  return {
    ...state,
    [payload.id]: {
      ...EntityRecord
    }
  };
}

export default reducerBrands;
