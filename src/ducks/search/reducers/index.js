import { combineReducers } from "redux";

import brands from "./brands";
import pageFilters from "./pageFilters";

export default combineReducers({
  brands,
  pageFilters
});
