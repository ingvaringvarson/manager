import { prefix } from "./config";

export const SEARCH_BRANDS_REQUEST = `${prefix}/SEARCH_BRANDS_REQUEST`;
export const SEARCH_BRANDS_SUCCESS = `${prefix}/SEARCH_BRANDS_SUCCESS`;
export const SEARCH_BRANDS_ERROR = `${prefix}/SEARCH_BRANDS_ERROR`;

export const UPDATE_PAGE_FILTERS = `${prefix}/UPDATE_PAGE_FILTERS`;
export const RESET_PAGE_FILTERS = `${prefix}/RESET_PAGE_FILTERS`;

export const CHANGE_BASKET_ATTACHED_PAGE = `${prefix}/CHANGE_BASKET_ATTACHED_PAGE`;

export const CHANGE_AGENT_ATTACHED_PAGE = `${prefix}/CHANGE_AGENT_ATTACHED_PAGE`;
