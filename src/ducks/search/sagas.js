import { call, all, takeLatest, put, select } from "redux-saga/effects";
import {
  addNotification,
  fetchApiSaga,
  loggingRequestSaga,
  watchingTwinActionsSaga
} from "../logger";
import {
  SEARCH_BRANDS_REQUEST,
  SEARCH_BRANDS_ERROR,
  SEARCH_BRANDS_SUCCESS,
  CHANGE_AGENT_ATTACHED_PAGE,
  CHANGE_BASKET_ATTACHED_PAGE
} from "./actions";
import { getBrandsForVendorCode } from "../../api/search";
import { moduleName } from "./config";
import { urlRouterSelector } from "../../redux/selectors/router";
import { fetchCurrentAgentInfoWithDiscountGroupSaga } from "../clients";
import {
  basketListSelector,
  attachAgentToBasket,
  attachAgentToBasketSaga
} from "../baskets";
import { pageFiltersSelector } from "./selectors";
import { updatePageFilters } from "./actionCreators";
import {
  mapAgentCodeToActionParams,
  mapAgentIdToActionParams
} from "../../helpers/mapValuesToRequest";

export function* searchBrandsSaga(action) {
  const paramsRequest = getBrandsForVendorCode(action.payload.searchString);

  if (paramsRequest) {
    const { response, error } = yield call(fetchApiSaga, paramsRequest);

    if (response && response.payload) {
      yield put({
        type: SEARCH_BRANDS_SUCCESS,
        payload: {
          id: action.payload.id,
          entities: response.payload.items
        }
      });
    } else if (error) {
      yield put({
        type: SEARCH_BRANDS_ERROR,
        payload: {
          id: action.payload.id
        }
      });
    }
  } else {
    yield put({
      type: SEARCH_BRANDS_ERROR,
      payload: {
        id: action.payload.id,
        error: "Невалидный артикул"
      }
    });
  }
}

export function* changeAgentAttachedPageSaga(action) {
  const { agentCode } = action.payload;
  const { basketId, agent } = yield select(pageFiltersSelector);
  const currentUrl = yield select(urlRouterSelector);

  if (!agentCode) {
    if (agent) {
      return yield put(
        updatePageFilters({ agent: null, discountGroupId: null })
      );
    }
    return yield put(
      addNotification({ id: currentUrl, message: "Введите код клиента!" })
    );
  }

  const actionParams = mapAgentCodeToActionParams(agentCode);
  const {
    agent: newAgentAttached,
    discountGroupId: newDiscountGroupIdAttached
  } = yield call(fetchCurrentAgentInfoWithDiscountGroupSaga, actionParams);

  if (newAgentAttached) {
    const newPageFilters = {
      agent: newAgentAttached,
      discountGroupId: newDiscountGroupIdAttached
    };
    const baskets = yield select(basketListSelector);

    if (baskets.length) {
      if (!basketId) {
        const basketsAttached = baskets.filter(
          b => b.clientAgentId === newAgentAttached.id
        );

        if (basketsAttached.length === 1) {
          newPageFilters.basketId = basketsAttached[0].managerBasketId;
        }
      } else {
        const basket = baskets.find(b => b.managerBasketId === +basketId);

        if (!basket) {
          return yield put(
            addNotification({
              id: currentUrl,
              message: "Установленная корзина не найдена!"
            })
          );
        } else if (!basket.clientAgentId) {
          const { response } = yield call(
            attachAgentToBasketSaga,
            attachAgentToBasket(basket.managerBasketId, newAgentAttached.id)
          );

          if (response) {
            yield put(
              addNotification({
                id: currentUrl,
                message: `Указанная вами корзина привязана к клиенту ${newAgentAttached.id}.`
              })
            );
          }
        } else if (basket.clientAgentId !== newAgentAttached.id) {
          return yield put(
            addNotification({
              id: currentUrl,
              message: `Указанная вами корзина принадлежит клиенту ${basket.clientAgentId}. Укажите этого клиента или создайте новую корзину`
            })
          );
        }
      }
    }

    return yield put(updatePageFilters(newPageFilters));
  } else {
    return yield put(
      addNotification({ message: "Клиент не найден. Введите другой код" })
    );
  }
}

export function* changeBasketAttachedPageSaga(action) {
  const { basketId } = action.payload;
  const { agent: currentAgent } = yield select(pageFiltersSelector);
  const baskets = yield select(basketListSelector);
  const currentUrl = yield select(urlRouterSelector);
  const basket = baskets.find(b => b.managerBasketId === +basketId);

  if (!basket) {
    return yield put(updatePageFilters({ basketId: null }));
  }

  const newPageFilters = { basketId: basket.managerBasketId };

  if (currentAgent) {
    if (!basket.clientAgentId) {
      const { response } = yield call(
        attachAgentToBasketSaga,
        attachAgentToBasket(basket.managerBasketId, currentAgent.id)
      );

      if (response) {
        yield put(
          addNotification({
            id: currentUrl,
            message: `Указанная вами корзина привязана к клиенту ${currentAgent.id}.`
          })
        );
      }
    } else if (basket.clientAgentId !== currentAgent.id) {
      return yield put(
        addNotification({
          id: currentUrl,
          message: `Указанная вами корзина принадлежит клиенту ${basket.clientAgentId}. Укажите этого клиента или создайте новую корзину`
        })
      );
    }
  } else if (basket.clientAgentId) {
    const actionParams = mapAgentIdToActionParams(basket.clientAgentId);
    const {
      agent: newAgentAttached,
      discountGroupId: newDiscountGroupIdAttached
    } = yield call(fetchCurrentAgentInfoWithDiscountGroupSaga, actionParams);

    if (newAgentAttached) {
      newPageFilters.agent = newAgentAttached;
      newPageFilters.discountGroupId = newDiscountGroupIdAttached;
    }
  }

  return yield put(updatePageFilters(newPageFilters));
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [SEARCH_BRANDS_REQUEST]: [searchBrandsSaga, moduleName]
    }),
    takeLatest(
      CHANGE_AGENT_ATTACHED_PAGE,
      loggingRequestSaga,
      changeAgentAttachedPageSaga,
      moduleName
    ),
    takeLatest(
      CHANGE_BASKET_ATTACHED_PAGE,
      loggingRequestSaga,
      changeBasketAttachedPageSaga,
      moduleName
    )
  ]);
}
