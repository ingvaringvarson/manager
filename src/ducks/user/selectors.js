import { moduleName } from "./config";
import { createSelector } from "reselect";
import createCachedSelector from "re-reselect";
import { pm_autorization_add_warehouse } from "../../constants/permissions";
import { entitiesToOptions, getValueInDepth } from "../../utils";

export const stateSelector = state => state[moduleName];
export const authStateSelector = state => stateSelector(state).auth;
export const isAuthSelector = state => authStateSelector(state).isAuth;
export const autoLoginSelector = state => authStateSelector(state).autoLogin;
export const isLoadingAuthSelector = state => {
  return authStateSelector(state).isLoading;
};
export const errorAuthSelector = state => authStateSelector(state).error;
export const authIdSelector = state => authStateSelector(state).employeeId;
export const userInfoStateSelector = state => stateSelector(state).info;
export const workPlaceChangedSelector = state =>
  userInfoStateSelector(state).workPlaceChanged;
export const agentInfoByUserSelector = state =>
  userInfoStateSelector(state).agent;
export const employeeInfoByUserSelector = state => {
  const agentInfo = agentInfoByUserSelector(state);
  const employeeInfo = getValueInDepth(agentInfo, ["employees", 0]);

  return employeeInfo || null;
};
export const contractsByUserSelector = state => {
  const agentInfo = agentInfoByUserSelector(state);
  const contacts = getValueInDepth(agentInfo, ["contracts"]);

  return contacts || null;
};
export const contractsToOptionsByUserSelector = createSelector(
  contractsByUserSelector,
  contracts => {
    if (!contracts) return [];

    return entitiesToOptions(
      contracts,
      entity => getValueInDepth(entity, ["number"]),
      entity => getValueInDepth(entity, ["id"])
    );
  }
);

export const userFirmAndWarehouseSelector = state => {
  const employee = employeeInfoByUserSelector(state);
  return {
    userFirmId: getValueInDepth(employee, ["firm_id"]),
    userWarehouseId: getValueInDepth(employee, ["warehouse_id"])
  };
};

export const userNameSelector = state => {
  const agentInfo = agentInfoByUserSelector(state);

  let name = "";

  if (agentInfo && agentInfo.person) {
    if (agentInfo.person.person_name) {
      name = agentInfo.person.person_name;
    }
    if (agentInfo.person.person_second_name) {
      name = name
        ? `${name} ${agentInfo.person.person_second_name}`
        : agentInfo.person.person_second_name;
    }
  }

  return name || "Неизвестно";
};
export const permissionAppSelector = state => stateSelector(state).permissions;
export const permissionsArrSelector = state =>
  permissionAppSelector(state).permissionsToApp;
export const permissionsIsLoadedSelector = state =>
  permissionAppSelector(state).isLoaded;
export const showWorkPlaceFormSelector = createSelector(
  permissionsArrSelector,
  workPlaceChangedSelector,
  autoLoginSelector,
  agentInfoByUserSelector,
  (haspermission, workPlaceChanged, autoLogin, agentInfo) =>
    !!(
      haspermission.includes(pm_autorization_add_warehouse) &&
      !autoLogin &&
      !workPlaceChanged &&
      agentInfo
    )
);

export const permissionNameSelector = (state, { name }) => name;
export const hasPermissionSelector = createCachedSelector(
  permissionsArrSelector,
  permissionNameSelector,
  (permissions, name) => permissions.includes(name)
)((state, { name }) => name);
