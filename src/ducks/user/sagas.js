import { all, call, put, takeLeading, select } from "redux-saga/effects";
import {
  TEST_AUTH_ERROR,
  TEST_AUTH_REQUEST,
  TEST_AUTH_START,
  TEST_AUTH_SUCCESS,
  LOG_IN_REQUEST,
  LOG_IN_SUCCESS,
  LOG_IN_ERROR,
  LOG_IN_START,
  LOG_OUT_SUCCESS,
  LOG_OUT_ERROR,
  LOG_OUT_REQUEST,
  LOG_OUT_START,
  FETCH_PERMISSION_APP_REQUEST,
  FETCH_PERMISSION_APP_SUCCESS
} from "./actions";
import { getInfoByUser, logIn, logOut, getPermissions } from "../../api/user";
import fetchApi from "../../helpers/fetchApi";
import { loggingRequestSaga, fetchApiSaga } from "../logger";
import { moduleName } from "./config";
import { isAuthSelector } from "./selectors";
import { fetchEntitySaga } from "../../helpers/sagas";
import { mapRequestParamsToParamsByApiSaga } from "../../helpers/mapMethods";

function* fetchAgentInfoByUser(id) {
  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    { page: 1, page_size: 1, id },
    "agents"
  );

  const { response } = yield call(
    fetchEntitySaga,
    { params },
    {},
    "agents",
    "agents"
  );

  if (response && response.payload && response.payload.agents.length) {
    return response.payload.agents[0];
  }

  return null;
}

export function* testAuthSaga() {
  yield put({ type: TEST_AUTH_START });

  const paramsRequest = getInfoByUser();
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (response) {
    let info = null;

    if (response.payload.agentId) {
      const agentInfo = yield call(
        fetchAgentInfoByUser,
        response.payload.agentId
      );

      if (agentInfo) {
        info = agentInfo;
      }
    }

    yield put({
      type: TEST_AUTH_SUCCESS,
      payload: {
        ...response.payload,
        info
      }
    });
  } else if (error) {
    yield put({
      type: TEST_AUTH_ERROR,
      payload: {
        error
      }
    });
  }
}

export function* logInSaga(action) {
  yield put({ type: LOG_IN_START });

  const { login, password } = action.payload;
  // ToDo: use mapValuesToRequest
  const paramsRequest = logIn(login, password);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (response) {
    let info = null;

    if (response.payload.agentId) {
      const agentInfo = yield call(
        fetchAgentInfoByUser,
        response.payload.agentId
      );

      if (agentInfo) {
        info = agentInfo;
      }
    }
    yield put({
      type: LOG_IN_SUCCESS,
      payload: {
        ...response.payload,
        info
      }
    });
  } else if (error) {
    yield put({
      type: LOG_IN_ERROR,
      payload: {
        error
      }
    });
  }
}

export function* logOutSaga() {
  const isAuth = yield select(isAuthSelector);

  if (!isAuth) return;

  yield put({ type: LOG_OUT_START });

  const paramsRequest = logOut();
  const fetchRequest = fetchApi(paramsRequest);
  const { error, status } = yield call(fetchRequest);

  if (status === 200) {
    yield put({
      type: LOG_OUT_SUCCESS
    });
  } else if (error) {
    yield put({
      type: LOG_OUT_ERROR,
      payload: {
        error
      }
    });
  }
}

export function* permissionAppSaga() {
  const paramsRequest = getPermissions();
  const { response } = yield call(fetchApiSaga, paramsRequest);

  if (response && response.payload) {
    return yield put({
      type: FETCH_PERMISSION_APP_SUCCESS,
      payload: {
        permissionsToApp: response.payload
      }
    });
  }
}

export function* rootSaga() {
  yield all([
    takeLeading(
      TEST_AUTH_REQUEST,
      loggingRequestSaga,
      testAuthSaga,
      moduleName
    ),
    takeLeading(LOG_IN_REQUEST, loggingRequestSaga, logInSaga, moduleName),
    takeLeading(LOG_OUT_REQUEST, loggingRequestSaga, logOutSaga, moduleName),
    takeLeading(
      [TEST_AUTH_SUCCESS, LOG_IN_SUCCESS, FETCH_PERMISSION_APP_REQUEST],
      loggingRequestSaga,
      permissionAppSaga,
      moduleName
    )
  ]);
}
