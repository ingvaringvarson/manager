import {
  FETCH_PERMISSION_APP_SUCCESS,
  LOG_OUT_SUCCESS,
  FORCE_LOG_OUT
} from "../actions";

const StateRecord = {
  permissionsToApp: [],
  isLoaded: false
};

const permissionAppReducer = (state = StateRecord, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_PERMISSION_APP_SUCCESS:
      return permissionAppSuccess(state, payload);
    case LOG_OUT_SUCCESS:
    case FORCE_LOG_OUT:
      return clearPermissionApp(state, payload);
    default:
      return state;
  }
};

function permissionAppSuccess(state, payload) {
  return {
    ...state,
    permissionsToApp: payload.permissionsToApp,
    isLoaded: true
  };
}

function clearPermissionApp() {
  return {
    ...StateRecord
  };
}

export default permissionAppReducer;
