import {
  TEST_AUTH_SUCCESS,
  LOG_IN_SUCCESS,
  LOG_OUT_SUCCESS,
  WORK_PLACE_CHANGED
} from "../actions";
import { UPDATE_CLIENT_SUCCESS } from "../../clients/actions";
import { getValueInDepth } from "../../../utils";

const StateRecord = {
  agent: null,
  agentIsLoaded: false,
  workPlaceChanged: false
};

const infoReducer = (state = StateRecord, action) => {
  const { type, payload } = action;

  switch (type) {
    case TEST_AUTH_SUCCESS:
    case LOG_IN_SUCCESS:
      return logInSuccess(state, payload);
    case LOG_OUT_SUCCESS:
      return logOutSuccess(state);
    case WORK_PLACE_CHANGED:
      return workPlaceChanged(state);
    case UPDATE_CLIENT_SUCCESS:
      return updateClientSuccess(state, payload);
    default:
      return state;
  }
};

function logOutSuccess() {
  return StateRecord;
}

function logInSuccess(state, payload) {
  return payload.info
    ? {
        agent: payload.info,
        agentIsLoaded: true
      }
    : StateRecord;
}

function workPlaceChanged(state) {
  return {
    ...state,
    workPlaceChanged: true
  };
}

function updateClientSuccess(state, payload) {
  const idByClient = getValueInDepth(payload, ["entity", "id"]);
  const idByAgent = getValueInDepth(state, ["agent", "id"]);

  if (idByClient && idByAgent && idByAgent === idByClient) {
    return {
      ...state,
      agent: payload.entity
    };
  }

  return state;
}

export default infoReducer;
