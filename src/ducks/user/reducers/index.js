import { combineReducers } from "redux";
import auth from "./auth";
import info from "./info";
import permissions from "./permissions";

export default combineReducers({
  auth,
  info,
  permissions
});
