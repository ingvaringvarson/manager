import {
  TEST_AUTH_ERROR,
  TEST_AUTH_START,
  TEST_AUTH_SUCCESS,
  LOG_IN_SUCCESS,
  LOG_OUT_SUCCESS,
  FORCE_LOG_OUT
} from "../actions";

const StateRecord = {
  agentId: null,
  employeeId: null,
  isAuth: false,
  testAuth: false,
  isLoading: false,
  autoLogin: false,
  error: null
};

const authReducer = (state = StateRecord, action) => {
  const { type, payload } = action;

  switch (type) {
    case TEST_AUTH_START:
      return testAuthStart(state, payload);
    case TEST_AUTH_SUCCESS:
      return testAuthSuccess(state, payload);
    case TEST_AUTH_ERROR:
      return testAuthError(state, payload);
    case LOG_IN_SUCCESS:
      return logInSuccess(state, payload);
    case FORCE_LOG_OUT:
    case LOG_OUT_SUCCESS:
      return logOutSuccess(state, payload);
    default:
      return state;
  }
};

function testAuthStart(state) {
  return {
    ...state,
    isLoading: true
  };
}

function testAuthSuccess(state, payload) {
  return {
    ...state,
    agentId: payload.agentId,
    employeeId: payload.employeeId,
    isAuth: !!payload.employeeId,
    autoLogin: !!payload.employeeId,
    isLoading: false,
    error: null
  };
}

function testAuthError(state, payload) {
  return {
    ...state,
    isAuth: false,
    isLoading: false,
    error: payload.error
  };
}

function logOutSuccess() {
  return {
    ...StateRecord,
    isAuth: false
  };
}

function logInSuccess(state, payload) {
  return {
    ...state,
    agentId: payload.agentId,
    employeeId: payload.employeeId,
    isAuth: !!payload.employeeId,
    autoLogin: false
  };
}

export default authReducer;
