import {
  LOG_IN_REQUEST,
  TEST_AUTH_REQUEST,
  LOG_OUT_REQUEST,
  FORCE_LOG_OUT,
  FETCH_PERMISSION_APP_REQUEST,
  WORK_PLACE_CHANGED
} from "./actions";

export function testAuth() {
  return {
    type: TEST_AUTH_REQUEST
  };
}

export function logIn(login, password) {
  return {
    type: LOG_IN_REQUEST,
    payload: { login, password }
  };
}

export function logOut() {
  return {
    type: LOG_OUT_REQUEST
  };
}

export function forceLogOut() {
  return {
    type: FORCE_LOG_OUT
  };
}

export function fetchPermissionApp() {
  return {
    type: FETCH_PERMISSION_APP_REQUEST
  };
}

export function workPlaceChanged() {
  return {
    type: WORK_PLACE_CHANGED
  };
}
