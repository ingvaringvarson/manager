import { prefix } from "./config";

export const LOG_IN_REQUEST = `${prefix}/LOG_IN_REQUEST`;
export const LOG_IN_START = `${prefix}/LOG_IN_START`;
export const LOG_IN_SUCCESS = `${prefix}/LOG_IN_SUCCESS`;
export const LOG_IN_ERROR = `${prefix}/LOG_IN_ERROR`;

export const LOG_OUT_REQUEST = `${prefix}/LOG_OUT_REQUEST`;
export const LOG_OUT_START = `${prefix}/LOG_OUT_START`;
export const LOG_OUT_SUCCESS = `${prefix}/LOG_OUT_SUCCESS`;
export const LOG_OUT_ERROR = `${prefix}/LOG_OUT_ERROR`;

export const TEST_AUTH_REQUEST = `${prefix}/TEST_AUTH_REQUEST`;
export const TEST_AUTH_START = `${prefix}/TEST_AUTH_START`;
export const TEST_AUTH_SUCCESS = `${prefix}/TEST_AUTH_SUCCESS`;
export const TEST_AUTH_ERROR = `${prefix}/TEST_AUTH_ERROR`;

export const FETCH_PERMISSION_APP_REQUEST = `${prefix}/FETCH_PERMISSION_APP_REQUEST`;
export const FETCH_PERMISSION_APP_SUCCESS = `${prefix}/FETCH_PERMISSION_APP_SUCCESS`;

export const FORCE_LOG_OUT = `${prefix}/FORCE_LOG_OUT`;

export const WORK_PLACE_CHANGED = `${prefix}/WORK_PLACE_CHANGED`;
