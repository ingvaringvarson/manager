import { all, call, takeEvery, put, select } from "redux-saga/effects";
import {
  addActionResult,
  addNotification,
  loggingRequestSaga,
  watchingTwinActionsSaga
} from "../logger";
import {
  fetchEntitiesForTypeEntitySaga,
  fetchEntitiesSaga,
  putEntitySaga,
  postCommonSaga,
  putEntitiesSaga
} from "../../helpers/sagas";
import { moduleName } from "./config";
import { mapRequestParamsToParamsByApiSaga } from "../../helpers/mapMethods";
import {
  FETCH_TASKS_REQUEST,
  FETCH_TASKS_SUCCESS,
  FETCH_TASKS_ERROR,
  FETCH_TASKS_FOR_ENTITY_REQUEST,
  FETCH_TASKS_FOR_ENTITY_SUCCESS,
  FETCH_TASKS_FOR_ENTITY_ERROR,
  UPDATE_PRODUCT_TASKS_REQUEST,
  CREATE_TASK_REQUEST,
  UPDATE_TASK_SUCCESS,
  UPDATE_TASK_ERROR,
  UPDATE_TASK_REQUEST
} from "./actions";
import { getValueInDepth } from "../../utils";
import { mapTasksToUpdateCalls } from "./helpers";
import { updateProductTasks } from "./actionCreators";
import { authIdSelector } from "../user";

export function* fetchTasksSaga(action) {
  const actions = {
    success: FETCH_TASKS_SUCCESS,
    error: FETCH_TASKS_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "tasks"
  );

  const payloadForApi = {
    ...action.payload,
    params: params
  };

  return yield call(
    fetchEntitiesSaga,
    payloadForApi,
    actions,
    "tasks",
    "tasks"
  );
}

export function* fetchTasksForEntitySaga(action) {
  const actions = {
    success: FETCH_TASKS_FOR_ENTITY_SUCCESS,
    error: FETCH_TASKS_FOR_ENTITY_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "tasks"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  return yield call(
    fetchEntitiesForTypeEntitySaga,
    payloadForApi,
    actions,
    "tasks",
    "tasks"
  );
}

function* createTaskSaga(action) {
  const { formValues, helperValues } = action.payload;
  const { _product } = formValues;

  if (_product) {
    // при создании задачи, если у формы имеется филд _product
    // вызвать столько апи методов, сколько было создано задач для товара

    yield all(
      Object.values(_product).reduce((res, v) => {
        const body = {
          ...formValues
        };

        // товар может быть притянут от позиции через заказ
        // или через артикул
        if ("order" in v) {
          body.order_position_id = v.order_position_id;
        } else {
          body.product = {
            article: v.article,
            brand: v.brand,
            type: v.type,
            name: v.name
          };
        }

        delete body._product;

        for (let i = 0; i < v.count_add; i++) {
          res.push(call(postCommonSaga, { body }, null, "tasks"));
        }

        return res;
      }, [])
    );
  } else {
    yield call(postCommonSaga, { body: formValues }, null, "tasks");
  }

  if (helperValues.successCallback) {
    yield call(helperValues.successCallback);
  }
}

export function* updateTasksBySelectedGroupSaga(action) {
  const { actionParams, tasks, newParams: newParamsDefault } = action.payload;

  if (newParamsDefault && !actionParams.needKey) {
    return yield call(updateTasksSaga, action, false);
  }

  if (actionParams.selectedGroup) {
    let key = "id_realization";

    if (actionParams.selectedGroup === "11") key = "id_goods_receipt";

    const taskWithIdRealization = tasks.find(task => task[key]);

    if (taskWithIdRealization) {
      const tasksUpdate = tasks.filter(t => !t[key]);
      const newParams = {
        state: 2,
        id_realization: taskWithIdRealization[key]
      };

      return yield call(
        updateTasksSaga,
        updateProductTasks(tasksUpdate, newParams),
        false
      );
    }

    const [firstTask, ...restTask] = tasks;
    const taskCompleted = {
      ...firstTask,
      state: 2
    };

    const { response } = yield call(
      putEntitySaga,
      { body: taskCompleted },
      null,
      "tasks"
    );

    if (!restTask.length) return [{ response }];

    const keyValue = getValueInDepth(response, ["payload", "task", key]);
    if (keyValue) {
      const newParams = {
        state: 2,
        [key]: keyValue
      };

      return yield call(
        updateTasksSaga,
        updateProductTasks(restTask, newParams),
        false
      );
    }

    return yield put(addNotification({ message: "Что-то пошло не так" }));
  }

  return [];
}

export function* updateTasksSaga(action, parallel = true) {
  const { tasks, newParams } = action.payload;
  const updateCalls = mapTasksToUpdateCalls(tasks, newParams);

  if (!updateCalls.length) {
    return yield put(addNotification({ message: "Что-то пошло не так" }));
  }

  // ToDo: проверить несколько запросов, могут быть блокировки
  if (parallel) {
    return yield all(updateCalls);
  } else {
    const results = [];

    for (const c of updateCalls) {
      results.push(yield c);
    }

    return results;
  }
}

export function* updateProductTasksSaga(action) {
  let responses = null;
  const { actionParams } = action.payload;

  if (actionParams && actionParams.selectedGroup) {
    responses = yield updateTasksBySelectedGroupSaga(action);
  } else {
    responses = yield updateTasksSaga(action, false);
  }

  if (!Array.isArray(responses) || !responses.length) {
    return yield put(addNotification({ message: "Что-то пошло не так" }));
  }

  if (responses.length) {
    if (actionParams) {
      let taskListRequestResult = null;
      if (actionParams.fetchTasks) {
        if (actionParams.fetchTasks.typeEntity) {
          taskListRequestResult = yield call(fetchTasksForEntitySaga, {
            payload: actionParams.fetchTasks
          });
        } else {
          taskListRequestResult = yield call(fetchTasksSaga, {
            payload: actionParams.fetchTasks
          });
        }
      }

      if (actionParams.notification && responses.some(r => r.response)) {
        const notification = {
          id: actionParams.notification.id,
          message: "Изменён статус для задач"
        };

        yield put(addNotification(notification));
      }

      const taskList = getValueInDepth(taskListRequestResult, [
        "response",
        "payload",
        "tasks"
      ]);

      if (
        actionParams.actionResult &&
        Array.isArray(taskList) &&
        taskList.length
      ) {
        const payload = responses.reduce(
          (result, { response, error }) => {
            if (response) {
              const oldTask = getValueInDepth(response, ["payload", "task"]);
              const newTask = taskList.find(
                t =>
                  t.order_id === oldTask.order_id &&
                  t.product_unit_id === oldTask.product_unit_id
              );

              if (newTask) {
                result.ids.push(newTask.id);
                result.tasks.push(newTask);
              } else {
                result.tasks.push(oldTask);
              }
            } else if (error || !response) {
              result.hasErrors = true;
            }

            return result;
          },
          {
            ...actionParams.actionResult.payload,
            ids: [],
            tasks: [],
            hasErrors: false,
            openForm: actionParams.openForm
          }
        );

        const actionResult = {
          id: actionParams.actionResult.id,
          payload
        };

        yield put(addActionResult(actionResult));
      }
    }
  }
}

export function* updateTaskSaga(
  action = {
    payload: {
      entityId: "",
      valuesForm: {},
      restValues: {}
    }
  }
) {
  const actions = {
    success: UPDATE_TASK_SUCCESS,
    error: UPDATE_TASK_ERROR
  };

  const userId = yield select(authIdSelector);

  const { type } = action.payload.valuesForm;

  switch (type) {
    case 1:
    case 8:
      yield call(updatePrepareToIssueTask, action, userId, actions);
      break;
    default:
      yield call(
        putEntitiesSaga,
        {
          ...action.payload,
          body: {
            ...action.payload.valuesForm,
            object_id_edit: userId
          }
        },
        actions,
        "tasks"
      );
  }
}

export function* updatePrepareToIssueTask(
  action = {
    payload: {
      valuesForm: {},
      restValues: {}
    }
  },
  object_id_edit,
  actions
) {
  const { valuesForm, restValues } = action.payload;
  const body = {
    ...restValues.task,
    ...valuesForm,
    product_unit_id: restValues.productUnit ? restValues.productUnit.id : null
  };

  yield call(
    putEntitiesSaga,
    {
      ...action.payload,
      body
    },
    actions,
    "tasks"
  );
}

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_TASKS_REQUEST]: [fetchTasksSaga, moduleName],
      [FETCH_TASKS_FOR_ENTITY_REQUEST]: [fetchTasksForEntitySaga, moduleName]
    }),
    takeEvery(
      UPDATE_PRODUCT_TASKS_REQUEST,
      loggingRequestSaga,
      updateProductTasksSaga,
      moduleName
    ),
    takeEvery(
      CREATE_TASK_REQUEST,
      loggingRequestSaga,
      createTaskSaga,
      moduleName
    ),
    takeEvery(
      UPDATE_TASK_REQUEST,
      loggingRequestSaga,
      updateTaskSaga,
      moduleName
    )
  ]);
}
