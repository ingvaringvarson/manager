import {
  FETCH_TASK_REQUEST,
  FETCH_TASKS_REQUEST,
  FETCH_TASKS_FOR_ENTITY_REQUEST,
  UPDATE_PRODUCT_TASKS_REQUEST,
  CREATE_TASK_REQUEST,
  UPDATE_TASK_REQUEST
} from "./actions";
import {
  fetchEntities,
  fetchEntitiesForTypeEntity,
  fetchEntity,
  postEntity
} from "../../helpers/actionCreators";

export const fetchTasks = fetchEntities(FETCH_TASKS_REQUEST);

export const fetchTask = fetchEntity(FETCH_TASK_REQUEST);

export const fetchTasksForEntity = fetchEntitiesForTypeEntity(
  FETCH_TASKS_FOR_ENTITY_REQUEST
);

export const updateProductTasks = (tasks, newParams, actionParams) => {
  return {
    type: UPDATE_PRODUCT_TASKS_REQUEST,
    payload: {
      tasks,
      newParams,
      actionParams
    }
  };
};

export const createTask = postEntity(CREATE_TASK_REQUEST);

export const updateTask = (entityId, valuesForm, restValues) => {
  return {
    type: UPDATE_TASK_REQUEST,
    payload: {
      entityId,
      valuesForm,
      restValues
    }
  };
};
