import { moduleName } from "./config";
import {
  entityIdForRouteSelector,
  queryListWithoutServiceQueryRouterSelector,
  queryRouterSelector,
  stateRouterSelector
} from "../../redux/selectors/router";
import { createSelector } from "reselect";
import {
  entityListSelector,
  maxPagesSelector as maxPagesSelectorHelper,
  typeEntityStateSelector as typeEntityStateSelectorHelper,
  entitiesForTypeEntitySelector,
  typeEntitySelector,
  idEntitySelector,
  pageModByDefaultValuesSelector
} from "../../helpers/selectors";
import { maskDateBeginningDay, maskDateEndDay } from "../../helpers/masks";
import { employeeInfoByUserSelector } from "../user";
import { generateId } from "../../utils";
import { tasksTabs } from "../../constants/tabs";
import { taskTypesBySecondGroup } from "../../fields/formFields/tasks/fieldsForWarehousePage";

export const tasksSelector = entityListSelector(moduleName);
export const maxPagesSelector = maxPagesSelectorHelper(moduleName);

export const typeEntityStateSelector = typeEntityStateSelectorHelper(
  moduleName
);

function stateForEntityDecorator({ tasks_state, ...query }, f) {
  return f({
    ...query,
    state: (
      tasksTabs.find(i => i.id === +(tasks_state ? tasks_state : 1)) ||
      tasksTabs.find(i => i.id === 1)
    ).values,
    tasks_state
  });
}

export const tasksForEntitySelector = entitiesForTypeEntitySelector(moduleName);

export const requestParamsSelector = createSelector(
  queryRouterSelector,
  query => ({
    page_size: 20,
    page: 1,
    sort_by: "date desc",
    ...query
  })
);
export const requestParamsForEntitySelector = createSelector(
  queryRouterSelector,
  typeEntitySelector,
  idEntitySelector,
  (query, typeEntity, idEntity) =>
    stateForEntityDecorator(query, newQuery => ({
      page_size: 20,
      page: 1,
      sort_by: "date desc",
      ...newQuery,
      [typeEntity]: idEntity
    }))
);
export const maxPagesForEntitySelector = createSelector(
  tasksForEntitySelector,
  entity => {
    return entity.maxPages || 99;
  }
);

export const tasksWithProductForEntitySelector = createSelector(
  tasksForEntitySelector,
  tasks => {
    if (!tasks.entities.length) return tasks;
    const entities = tasks.entities.filter(entity => entity.product);

    return { ...tasks, entities };
  }
);

export const requestParamsByClientPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  (query, id) =>
    stateForEntityDecorator(query, newQuery => ({
      page_size: 10,
      page: 1,
      agent_id: id,
      sort_by: "date desc",
      ...newQuery
    }))
);

export const requestParamsByOrderPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  (query, id) =>
    stateForEntityDecorator(query, newQuery => ({
      page_size: 10,
      page: 1,
      order_id: id,
      sort_by: "date desc",
      ...newQuery
    }))
);

export const requestParamsByWarehousePageSelector = createSelector(
  queryRouterSelector,
  employeeInfoByUserSelector,
  (query, employee) => {
    const defaultParams = {};

    if (!query.hasOwnProperty("_tab") || !query.hasOwnProperty("_type_group")) {
      if (!query.hasOwnProperty("_type_group")) {
        const currentDay = Date.now();

        defaultParams.date_from = maskDateBeginningDay(currentDay);
        defaultParams.date_to = maskDateEndDay(currentDay);

        if (employee && employee.warehouse_id) {
          defaultParams.warehouse_id = employee.warehouse_id;
        }

        if (!query.hasOwnProperty("_tab") || query._tab === "1") {
          defaultParams._tab = "1";
          defaultParams._type_group = "1_4";
        } else if (query._tab === "2") {
          defaultParams._type_group = "3_5";
        }
      } else if (taskTypesBySecondGroup.includes(query._type_group)) {
        defaultParams._tab = "2";
      } else {
        defaultParams._tab = "1";
      }
    }

    const requestParams = { ...defaultParams, ...query };
    const state = [];

    if (requestParams._tab === "1") {
      if (requestParams._completed_tasks === "1") {
        state.splice(0, 0, 2, 5);
      } else {
        state.splice(0, 0, 1, 3, 6, 7);
      }
    } else if (requestParams._tab === "2") {
      if (requestParams._completed_tasks !== "1") {
        state.splice(0, 0, 1);
      }
    }

    if (requestParams._canceled_tasks === "1") {
      state.splice(0, 0, 4);
    }

    if (state.length) {
      requestParams.state = state;
    } else if (requestParams.hasOwnProperty("state")) {
      delete requestParams.state;
    }

    return requestParams;
  }
);

export const requestParamsForTasksEntitySelector = createSelector(
  queryRouterSelector,
  typeEntitySelector,
  idEntitySelector,
  (query, typeEntity, idEntity) => {
    return {
      page: 1,
      page_size: 10,
      sort_by: "date desc",
      ...query,
      [typeEntity]: idEntity
    };
  }
);

export const entityIdByWarehousePageSelector = createSelector(
  employeeInfoByUserSelector,
  employee =>
    employee && employee.warehouse_id ? employee.warehouse_id : generateId()
);

export const defaultValuesForFieldsSelector = createSelector(
  employeeInfoByUserSelector,
  queryListWithoutServiceQueryRouterSelector,
  pageModByDefaultValuesSelector,
  (employee, queryList, pageMod) => {
    const defaultParams = {};

    if (queryList.length) return defaultParams;

    const currentDay = Date.now();

    defaultParams.datefrom_created = maskDateBeginningDay(
      currentDay - 604800000
    );
    defaultParams.dateto_created = maskDateEndDay(currentDay);

    if (employee) {
      if (employee.id && pageMod) {
        defaultParams.executor_id = employee.id;
      }
    }

    return defaultParams;
  }
);

const emptySelectedFilterGroup = [];
export const defaultSelectedFilterGroupSelector = createSelector(
  stateRouterSelector,
  stateRouter => {
    if (Array.isArray(stateRouter.selectedFilterGroup)) {
      return stateRouter.selectedFilterGroup;
    }

    return emptySelectedFilterGroup;
  }
);
