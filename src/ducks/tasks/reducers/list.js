import { FETCH_TASKS_ERROR, FETCH_TASKS_SUCCESS } from "../actions";
import {
  ListStateRecord,
  fetchEntitiesSuccess,
  fetchEntitiesError
} from "../../../helpers/reducers";

const listReducer = (state = { ...ListStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_TASKS_SUCCESS:
      return fetchEntitiesSuccess(state, payload);
    case FETCH_TASKS_ERROR:
      return fetchEntitiesError(state, payload);
    default:
      return state;
  }
};

export default listReducer;
