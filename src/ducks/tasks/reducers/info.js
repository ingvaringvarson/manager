import {
  FETCH_TASK_SUCCESS,
  CREATE_TASK_SUCCESS,
  UPDATE_TASK_SUCCESS
} from "../actions";
import { InfoStateRecord, fetchEntitySuccess } from "../../../helpers/reducers";

const infoReducer = (state = { ...InfoStateRecord }, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_TASK_SUCCESS:
    case UPDATE_TASK_SUCCESS:
    case FETCH_TASK_SUCCESS:
      return fetchEntitySuccess(state, payload);
    default:
      return state;
  }
};

export default infoReducer;
