import { prefix } from "./config";

export const FETCH_TASKS_REQUEST = `${prefix}/FETCH_TASKS_REQUEST`;
export const FETCH_TASKS_SUCCESS = `${prefix}/FETCH_TASKS_SUCCESS`;
export const FETCH_TASKS_ERROR = `${prefix}/FETCH_TASKS_ERROR`;

export const FETCH_TASKS_FOR_ENTITY_REQUEST = `${prefix}/FETCH_TASKS_FOR_ENTITY_REQUEST`;
export const FETCH_TASKS_FOR_ENTITY_SUCCESS = `${prefix}/FETCH_TASKS_FOR_ENTITY_SUCCESS`;
export const FETCH_TASKS_FOR_ENTITY_ERROR = `${prefix}/FETCH_TASKS_FOR_ENTITY_ERROR`;

export const FETCH_TASK_REQUEST = `${prefix}/FETCH_TASK_REQUEST`;
export const FETCH_TASK_SUCCESS = `${prefix}/FETCH_TASK_SUCCESS`;
export const FETCH_TASK_ERROR = `${prefix}/FETCH_TASK_ERROR`;

export const CREATE_TASK_REQUEST = `${prefix}/CREATE_TASK_REQUEST`;
export const CREATE_TASK_SUCCESS = `${prefix}/CREATE_TASK_SUCCESS`;
export const CREATE_TASK_ERROR = `${prefix}/CREATE_TASK_ERROR`;

export const UPDATE_TASK_REQUEST = `${prefix}/UPDATE_TASK_REQUEST`;
export const UPDATE_TASK_SUCCESS = `${prefix}/UPDATE_TASK_SUCCESS`;
export const UPDATE_TASK_ERROR = `${prefix}/UPDATE_TASK_ERROR`;

export const UPDATE_PRODUCT_TASKS_REQUEST = `${prefix}/UPDATE_PRODUCT_TASKS_REQUEST`;
