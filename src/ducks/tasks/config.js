import { appName } from "../../config";

export const moduleName = "tasks";
export const prefix = `${appName}/${moduleName}`;
