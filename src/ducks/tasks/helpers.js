import { call } from "redux-saga/effects";
import { putEntitySaga } from "../../helpers/sagas";
import { isType } from "../../utils";

export function mapTasksToUpdateCalls(tasks, newParams) {
  let updateCalls = null;

  if (Array.isArray(newParams)) {
    updateCalls = tasks.map(task => {
      const newParamsByTask = newParams.find(item => item.id === task.id);
      const body = { ...task, ...newParamsByTask };

      return call(putEntitySaga, { body }, null, "tasks");
    });
  } else if (isType(newParams, "object")) {
    updateCalls = tasks.map(task => {
      const body = { ...task, ...newParams };

      return call(putEntitySaga, { body }, null, "tasks");
    });
  } else {
    updateCalls = tasks.map(task => {
      return call(putEntitySaga, { body: task }, null, "tasks");
    });
  }

  return updateCalls;
}
