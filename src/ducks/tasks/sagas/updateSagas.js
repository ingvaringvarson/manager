import { call } from "redux-saga/effects";
import { putEntitiesSaga } from "../../../helpers/sagas";

export function* updatePrepareToIssueTask(
  action = {
    payload: {
      valuesForm: {},
      restValues: {}
    }
  },
  object_id_edit,
  actions
) {
  const { valuesForm, restValues } = action.payload;
  const body = {
    ...restValues.task,
    ...valuesForm,
    product_unit_id: restValues.productUnit ? restValues.productUnit.id : null
  };

  yield call(
    putEntitiesSaga,
    {
      ...action.payload,
      body
    },
    actions,
    "tasks"
  );
}
