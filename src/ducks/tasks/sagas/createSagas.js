import { all, call } from "redux-saga/effects";
import { postEntitySaga } from "../../../helpers/sagas";
import { getCurrentPositionToAdd } from "./sagaHelpers";

export function* createPrepareToIssueTask(
  action = {
    payload: {
      valuesForm: {},
      restValues: {}
    }
  },
  object_id_create,
  actions
) {
  const { valuesForm, restValues } = action.payload;
  yield all(
    Object.keys(restValues.orderValues).reduce((result, currentOrderId) => {
      const currentOrderData = restValues.orderValues[currentOrderId];
      const { order, formValues, removedIds } = currentOrderData;

      // CHECK if there are no positions
      // and return if no
      if (
        !(order && Array.isArray(order.positions) && order.positions.length)
      ) {
        return result;
      }

      // return array of call sagas
      const listOfTasks = order.positions.reduce(
        (posArrayResult, currentPos) => {
          // check if position was removed
          // dont handle it if true
          if (
            Array.isArray(removedIds) &&
            removedIds.some(id => id === currentPos.id)
          ) {
            return posArrayResult;
          }

          const count = getCurrentPositionToAdd(formValues, currentPos);

          for (let i = 0; i < count; i++) {
            posArrayResult.push(
              call(
                postEntitySaga,
                {
                  ...action.payload,
                  body: {
                    ...valuesForm,
                    order_position_id: currentPos.id,
                    order_id: order.id
                  }
                },
                actions,
                "tasks"
              )
            );
          }

          return posArrayResult;
        },
        []
      );

      return result.concat(listOfTasks);
    }, [])
  );
}
