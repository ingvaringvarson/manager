import { all, call, takeEvery, select } from "redux-saga/effects";
import { loggingRequestSaga } from "../../logger";
import {
  fetchEntitiesForTypeEntitySaga,
  fetchEntitiesSaga,
  putEntitiesSaga,
  postEntitySaga
} from "../../../helpers/sagas";
import { moduleName } from "../config";
import { mapRequestParamsToParamsByApiSaga } from "../../../helpers/mapMethods";
import {
  FETCH_TASKS_REQUEST,
  FETCH_TASKS_SUCCESS,
  FETCH_TASKS_ERROR,
  FETCH_TASKS_FOR_ENTITY_REQUEST,
  FETCH_TASKS_FOR_ENTITY_SUCCESS,
  FETCH_TASKS_FOR_ENTITY_ERROR,
  CREATE_TASK_REQUEST,
  CREATE_TASK_SUCCESS,
  CREATE_TASK_ERROR,
  UPDATE_TASK_REQUEST,
  UPDATE_TASK_SUCCESS,
  UPDATE_TASK_ERROR
} from "../actions";

import { authIdSelector } from "../../user";
import { createPrepareToIssueTask } from "./createSagas";
import { updatePrepareToIssueTask } from "./updateSagas";

export function* fetchTasksSaga(action) {
  const actions = {
    success: FETCH_TASKS_SUCCESS,
    error: FETCH_TASKS_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "tasks"
  );

  const payloadForApi = {
    ...action.payload,
    params: params
  };

  yield call(fetchEntitiesSaga, payloadForApi, actions, "tasks", "tasks");
}

export function* fetchTasksForEntitySaga(action) {
  const actions = {
    success: FETCH_TASKS_FOR_ENTITY_SUCCESS,
    error: FETCH_TASKS_FOR_ENTITY_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "tasks"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  yield call(
    fetchEntitiesForTypeEntitySaga,
    payloadForApi,
    actions,
    "tasks",
    "tasks"
  );
}

export function* createTaskSaga(
  action = {
    payload: {
      valuesForm: {},
      restValues: {}
    }
  }
) {
  const actions = {
    success: CREATE_TASK_SUCCESS,
    error: CREATE_TASK_ERROR
  };
  const userId = yield select(authIdSelector);

  const { type } = action.payload.valuesForm;

  switch (+type) {
    case 1:
    case 8:
      yield call(createPrepareToIssueTask, action, userId, actions);
      break;
    default:
      yield call(
        postEntitySaga,
        {
          ...action.payload,
          body: {
            ...action.payload.valuesForm,
            object_id_create: userId
          }
        },
        actions,
        "tasks"
      );
      break;
  }
}

export function* updateTaskSaga(
  action = {
    payload: {
      entityId: "",
      valuesForm: {},
      restValues: {}
    }
  }
) {
  const actions = {
    success: UPDATE_TASK_SUCCESS,
    error: UPDATE_TASK_ERROR
  };

  const userId = yield select(authIdSelector);

  const { type } = action.payload.valuesForm;

  switch (type) {
    case 1:
    case 8:
      yield call(updatePrepareToIssueTask, action, userId, actions);
      break;
    default:
      yield call(
        putEntitiesSaga,
        {
          ...action.payload,
          body: {
            ...action.payload.valuesForm,
            object_id_edit: userId
          }
        },
        actions,
        "tasks"
      );
  }
}

export function* rootSaga() {
  yield all([
    takeEvery(
      FETCH_TASKS_REQUEST,
      loggingRequestSaga,
      fetchTasksSaga,
      moduleName
    ),
    takeEvery(
      FETCH_TASKS_FOR_ENTITY_REQUEST,
      loggingRequestSaga,
      fetchTasksForEntitySaga,
      moduleName
    ),
    takeEvery(
      CREATE_TASK_REQUEST,
      loggingRequestSaga,
      createTaskSaga,
      moduleName
    ),
    takeEvery(
      UPDATE_TASK_REQUEST,
      loggingRequestSaga,
      updateTaskSaga,
      moduleName
    )
  ]);
}
