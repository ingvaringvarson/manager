export function getCurrentPositionToAdd(formValues = {}, curPos = {}) {
  return formValues &&
    (+formValues[`add__${curPos._id}`] ||
      +formValues[`add__${curPos._id}`] === 0)
    ? +formValues[`add__${curPos._id}`]
    : curPos.count;
}
