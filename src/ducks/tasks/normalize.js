export const normalizingTaskCreation = payload => {
  delete payload.body.state;
  return payload;
};

export const normalizingTaskUpdate = payload => {
  return {
    ...payload,
    body: {
      ...payload.body,
      ...payload.values
    }
  };
};
