import { all } from "redux-saga/effects";
import {
  FETCH_BALANCES_FOR_ENTITY_REQUEST,
  FETCH_BALANCES_REQUEST,
  FETCH_BALANCE_REQUEST
} from "../actions";

import {
  fetchBalancesForEntitySaga,
  fetchBalancesSaga,
  fetchBalanceSaga
} from "./balances";
import { watchingTwinActionsSaga } from "../../logger";
import { moduleName } from "../config";

export function* rootSaga() {
  yield all([
    watchingTwinActionsSaga({
      [FETCH_BALANCES_REQUEST]: [fetchBalancesSaga, moduleName],
      [FETCH_BALANCES_FOR_ENTITY_REQUEST]: [
        fetchBalancesForEntitySaga,
        moduleName
      ],
      [FETCH_BALANCE_REQUEST]: [fetchBalanceSaga, moduleName]
    })
  ]);
}
