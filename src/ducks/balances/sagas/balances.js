import { call } from "redux-saga/effects";
import {
  FETCH_BALANCES_SUCCESS,
  FETCH_BALANCES_ERROR,
  FETCH_BALANCES_FOR_ENTITY_SUCCESS,
  FETCH_BALANCES_FOR_ENTITY_ERROR,
  FETCH_BALANCE_SUCCESS,
  FETCH_BALANCE_ERROR
} from "../actions";
import {
  fetchEntitiesForTypeEntitySaga,
  fetchEntitiesSaga,
  fetchEntitySaga
} from "../../../helpers/sagas";
import { mapRequestParamsToParamsByApiSaga } from "../../../helpers/mapMethods";

export function* fetchBalancesSaga(action) {
  const actions = {
    success: FETCH_BALANCES_SUCCESS,
    error: FETCH_BALANCES_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "balances"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  yield call(fetchEntitiesSaga, payloadForApi, actions, "balance", "balances");
}

export function* fetchBalancesForEntitySaga(action) {
  const actions = {
    success: FETCH_BALANCES_FOR_ENTITY_SUCCESS,
    error: FETCH_BALANCES_FOR_ENTITY_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "balances"
  );

  const payloadForApi = {
    ...action.payload,
    params
  };

  yield call(
    fetchEntitiesForTypeEntitySaga,
    payloadForApi,
    actions,
    "balance",
    "balances"
  );
}

export function* fetchBalanceSaga(action) {
  const actions = {
    success: FETCH_BALANCE_SUCCESS,
    error: FETCH_BALANCE_ERROR
  };

  const params = yield call(
    mapRequestParamsToParamsByApiSaga,
    action.payload.params,
    "balances"
  );

  const payloadForApi = {
    id: action.payload.id,
    params
  };

  yield call(fetchEntitySaga, payloadForApi, actions, "balance", "balances");
}
