import { moduleName } from "./config";
import {
  queryRouterSelector,
  entityIdForRouteSelector
} from "../../redux/selectors/router";
import { createSelector } from "reselect";
import {
  entityListSelector,
  maxPagesSelector as maxPagesSelectorHelper,
  entitiesForTypeEntitySelector
} from "../../helpers/selectors";
import { clientInfoSelector } from "../clients";

import { transactionsMapping } from "./helpers";

export const balanceListSelector = entityListSelector(moduleName);
export const maxPagesSelector = maxPagesSelectorHelper(moduleName);

export const balancesForEntitySelector = entitiesForTypeEntitySelector(
  moduleName
);
const balanceTransactionsSelector = (state, props = { balance: null }) => {
  const { balance } = props;
  return balance && balance.balance && balance.balance.transactions;
};

export const mappedBalanceTransactionsSelector = createSelector(
  balanceTransactionsSelector,
  transactions => {
    if (!Array.isArray(transactions)) return null;

    return transactionsMapping(transactions);
  }
);

export const requestParamsByClientPageSelector = createSelector(
  queryRouterSelector,
  entityIdForRouteSelector,
  clientInfoSelector,
  (query, agentId, client) => {
    const contractDefaultProps = {
      account_type: 2
    };

    const contract =
      client && Array.isArray(client.contracts)
        ? client.contracts.find(c => c.state === 2 && c.type === 1)
        : null;

    if (contract) {
      contractDefaultProps.contract_id = contract.id;
    }

    return {
      page_size: 20,
      page: 1,
      agent_id: agentId,
      ...contractDefaultProps,
      ...query
    };
  }
);
