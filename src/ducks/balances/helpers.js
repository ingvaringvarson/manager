export function transactionsMapping(transactions) {
  const transactionsComposition = transactions.reduce(
    (result, currentTransaction) => {
      const foundTrans = result.find(block =>
        block.some(trans => {
          const transDate = new Date(trans.object_date_create);
          const currentTransactionDate = new Date(
            currentTransaction.object_date_create
          );
          const dateCreateMinutesDiff = Math.abs(
            (transDate - currentTransactionDate) / 60000
          );
          return (
            trans.order_code === currentTransaction.order_code &&
            trans.type === currentTransaction.type &&
            trans.contract_id === currentTransaction.contract_id &&
            dateCreateMinutesDiff < 10
          );
        })
      );

      if (result.indexOf(foundTrans) !== -1) {
        result[result.indexOf(foundTrans)].push(currentTransaction);
      } else {
        result.push([currentTransaction]);
      }

      return result;
    },
    []
  );

  return transactionsComposition.reduce((result, currentBlock) => {
    if (currentBlock.length === 1) {
      result.push(currentBlock[0]);
    } else {
      const composedTransaction = currentBlock.reduce(
        (res, curTrans) => {
          if (curTrans.object_date_create < res.object_date_create) {
            res.object_date_create = curTrans.object_date_create;
          }
          res.account_out_value =
            res.account_out_value + curTrans.account_out_value;
          return {
            ...curTrans,
            ...res
          };
        },
        {
          account_out_value: 0,
          object_date_create: currentBlock[0].object_date_create
        }
      );
      result.push({
        ...composedTransaction,
        blockTransactions: currentBlock
      });
    }
    return result;
  }, []);
}
