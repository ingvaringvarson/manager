import {
  FETCH_BALANCES_FOR_ENTITY_REQUEST,
  FETCH_BALANCE_REQUEST,
  FETCH_BALANCES_REQUEST
} from "./actions";
import {
  fetchEntities,
  fetchEntitiesForTypeEntity,
  fetchEntity
} from "../../helpers/actionCreators";

export const fetchBalance = fetchEntity(FETCH_BALANCE_REQUEST);

export const fetchBalancesList = fetchEntities(FETCH_BALANCES_REQUEST);

export const fetchBalancesForEntity = fetchEntitiesForTypeEntity(
  FETCH_BALANCES_FOR_ENTITY_REQUEST
);
