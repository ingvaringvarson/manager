import { appName } from "../../config";

export const moduleName = "balances";
export const prefix = `${appName}/${moduleName}`;
