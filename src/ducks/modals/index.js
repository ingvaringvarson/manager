import { appName } from "../../config";
import { combineReducers } from "redux";

// config
export const moduleName = "modals";

// actions
export const OPEN_MODAL = `${appName}/${moduleName}/OPEN_MODAL`;
export const CLOSE_MODAL = `${appName}/${moduleName}/CLOSE_MODAL`;

// actionCreator
export function openModal(modalName, payloadModal) {
  return {
    type: OPEN_MODAL,
    payload: { modalName, payloadModal }
  };
}

export function closeModal(modalName) {
  return {
    type: CLOSE_MODAL,
    payload: { modalName }
  };
}

// selectors
const stateSelector = state => state[moduleName];

export const openedModalsSelector = state => stateSelector(state).openedModals;
export const modalIsOpenSelector = (state, { modalName }) =>
  openedModalsSelector(state).includes(modalName);

export const payloadModalsSelector = state =>
  stateSelector(state).payloadModals;
export const payloadModalSelector = (state, { modalName }) =>
  payloadModalsSelector(state)[modalName];

// reducers
function openedModals(state = [], action) {
  const { type, payload } = action;

  switch (type) {
    case OPEN_MODAL: {
      if (!state.includes(payload.modalName)) {
        return state.concat(payload.modalName);
      }

      return state;
    }
    case CLOSE_MODAL: {
      return state.filter(n => n !== payload.modalName);
    }
    default:
      return state;
  }
}

function payloadModals(state = {}, action) {
  const { type, payload } = action;

  switch (type) {
    case OPEN_MODAL: {
      if (!state.hasOwnProperty(payload.modalName)) {
        return {
          ...state,
          [payload.modalName]: payload.payloadModal
        };
      }

      return state;
    }
    case CLOSE_MODAL: {
      const copyState = { ...state };

      delete copyState[payload.modalName];

      return copyState;
    }
    default:
      return state;
  }
}

export default combineReducers({ openedModals, payloadModals });
