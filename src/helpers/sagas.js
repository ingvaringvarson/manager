import { put, call, select } from "redux-saga/effects";
import {
  getEntities,
  postEntities,
  putEntities,
  getEntitiesCommon,
  getEntitiesCommonById,
  postCommon,
  postUploadCommon,
  deleteCommon
} from "../api/requestByTypeEntities";
import { fetchApiSaga } from "../ducks/logger";
import { push } from "connected-react-router";
import {
  pathnameRouterSelector,
  searchRouterSelector
} from "../redux/selectors/router";

export function* fetchEntitiesSaga(
  payload,
  actions,
  typeEntity,
  payloadPropName
) {
  const params = {
    params: JSON.stringify(payload.params)
  };
  const paramsRequest = getEntities(typeEntity, params);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (actions && actions.success && response && response.payload) {
    yield put({
      type: actions.success,
      payload: {
        entities: payloadPropName
          ? response.payload[payloadPropName] || []
          : response.payload,
        maxPages: payloadPropName ? response.payload.pages_num : null
      }
    });
  } else if (actions && error && actions.error) {
    yield put({
      type: actions.error,
      payload: {
        error
      }
    });
  }

  return { response, error };
}

export function* fetchEntitiesForTypeEntitySaga(
  payload,
  actions,
  typeEntity,
  payloadPropName
) {
  const params = {
    params: JSON.stringify(payload.params)
  };
  const paramsRequest = getEntities(typeEntity, params);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (actions.success && response && response.payload) {
    yield put({
      type: actions.success,
      payload: {
        idEntity: payload.idEntity,
        typeEntity: payload.typeEntity,
        entities: payloadPropName
          ? response.payload[payloadPropName] || []
          : response.payload,
        maxPages: payloadPropName ? response.payload.pages_num : null
      }
    });
  } else if (error && actions.error) {
    yield put({
      type: actions.error,
      payload: {
        error
      }
    });
  }

  return { response, error };
}

export function* fetchEntitySaga(
  payload,
  actions,
  typeEntity,
  payloadPropName
) {
  const { forInfoPage, ...restParams } = payload.params;

  const params = {
    params: JSON.stringify(restParams)
  };
  const paramsRequest = getEntities(typeEntity, params);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (actions && actions.success && response && response.payload) {
    yield put({
      type: actions.success,
      payload: {
        id: payload.id,
        forInfoPage,
        entity: payloadPropName
          ? response.payload[payloadPropName] &&
            response.payload[payloadPropName][0]
          : response.payload[0]
      }
    });
  } else if (error && actions && actions.error) {
    yield put({
      type: actions.error,
      payload: { error }
    });
  }

  return { response, error };
}

export function* getApiMethodResponseSaga(
  payload = {
    params: {}
  },
  entityName = ""
) {
  const params = {
    params: JSON.stringify(payload.params)
  };

  const paramsRequest = getEntities(entityName, params);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  return {
    response,
    error
  };
}

export function* postEntitySaga(
  payload = {
    body: {},
    resultParams: {
      successParams: {},
      errorParams: {}
    }
  },
  actions = {
    success: "",
    error: ""
  },
  typeEntity = ""
) {
  const { body, resultParams = {} } = payload;
  const paramsRequest = postEntities(typeEntity, body);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (actions && actions.success && response && response.payload) {
    const { successParams = {} } = resultParams;

    yield put({
      type: actions.success,
      payload: {
        id: response.payload.id,
        entity: response.payload,
        successParams
      }
    });
  } else if (actions && actions.error && error) {
    const { errorParams = {} } = resultParams;

    yield put({
      type: actions.error,
      payload: { error },
      errorParams
    });
  }

  return { response, error };
}

export function* putEntitiesSaga(
  payload = {
    id: "",
    body: {}
  },
  actions = {
    success: "",
    error: ""
  },
  typeEntity = ""
) {
  const paramsRequest = putEntities(typeEntity, payload.body);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (actions && actions.success && response && response.payload) {
    yield put({
      type: actions.success,
      payload: {
        id: payload.id,
        entity: response.payload
      }
    });
  } else if (actions && actions.error && error) {
    yield put({
      type: actions.error,
      payload: { error }
    });
  }

  return { response, error };
}

export function* putEntitySaga(
  payload = {
    id: "",
    body: {},
    resultParams: {
      successParams: {},
      errorParams: {}
    }
  },
  actions = {
    success: "",
    error: ""
  },
  typeEntity = ""
) {
  const { id, body, resultParams = {} } = payload;
  const paramsRequest = putEntities(typeEntity, body);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (actions && actions.success && response && response.payload) {
    const { successParams = {} } = resultParams;

    yield put({
      type: actions.success,
      payload: {
        id,
        entity: response.payload,
        successParams
      }
    });
  } else if (actions && actions.error && error) {
    const { errorParams = {} } = resultParams;

    yield put({
      type: actions.error,
      payload: { error },
      errorParams
    });
  }

  return { response, error };
}

export function* fetchEntitiesCommon(
  actions = {},
  typeEntity = "",
  payloadPropName = "",
  urlPath = ""
) {
  const paramsRequest = getEntitiesCommon(urlPath || typeEntity);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (actions.success && response && response.payload) {
    yield put({
      type: actions.success,
      payload: {
        entities: payloadPropName
          ? response.payload[payloadPropName] || []
          : response.payload
      }
    });
  } else if (error && actions.error) {
    yield put({
      type: actions.error,
      payload: {
        error
      }
    });
  }

  return { response, error };
}

export function* fetchEntitiesCommonById(
  actions,
  typeEntity,
  entityId,
  payloadPropName,
  urlPath
) {
  const paramsRequest = getEntitiesCommonById(urlPath || typeEntity, entityId);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (actions.success && response && response.payload) {
    yield put({
      type: actions.success,
      payload: {
        idEntity: entityId,
        typeEntity: typeEntity,
        entities: payloadPropName
          ? response.payload[payloadPropName] || []
          : response.payload
      }
    });
  } else if (error && actions.error) {
    yield put({
      type: actions.error,
      payload: {
        error
      }
    });
  }

  return { response, error };
}

export function* fetchDataCommonById(actions, entityId, urlPath) {
  const paramsRequest = getEntitiesCommonById(urlPath, entityId);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (actions.success && response && response.payload) {
    yield put({
      type: actions.success,
      payload: {
        idEntity: entityId,
        apiData: response.payload
      }
    });
  } else if (error && actions.error) {
    yield put({
      type: actions.error,
      payload: {
        error
      }
    });
  }

  return { response, error };
}

export function* postCommonSaga(
  payload = {
    body: {}
  },
  actions = {
    success: "",
    error: ""
  },
  url = ""
) {
  const paramsRequest =
    payload.body instanceof FormData
      ? postUploadCommon(url, payload.body)
      : postCommon(url, payload.body);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (actions && actions.success && response && response.payload) {
    yield put({
      type: actions.success,
      payload: {
        id: response.payload.id,
        entity: response.payload
      }
    });
  } else if (actions && actions.error && error) {
    yield put({
      type: actions.error,
      payload: { error }
    });
  }

  return { response, error };
}

export function* responseToUpdateEntitySaga(action) {
  const { id } = action.payload;
  const pathname = yield select(pathnameRouterSelector);
  const search = yield select(searchRouterSelector);

  if (id) yield put(push(`${pathname}${search}`, { isLoaded: true }));
}

function* callMethodByFormSaga(
  generateParamsRequestCallback,
  payload,
  actions,
  apiUrl
) {
  const { formValues, helperValues, normalizeCallback } = payload;

  const bodyForApi =
    typeof normalizeCallback === "function"
      ? normalizeCallback(formValues, helperValues)
      : formValues;

  const paramsRequest = generateParamsRequestCallback(apiUrl, bodyForApi);
  const resultResponse = yield call(fetchApiSaga, paramsRequest);
  return yield call(handleResult, resultResponse, actions, payload);
}

function* handleResult(resultResponse, actions, payload) {
  const { helperValues, withoutFetching } = payload;
  const { response, error } = resultResponse;

  if (!response && !error) {
    return resultResponse;
  }

  if (error) {
    yield put({
      type: actions.error,
      payload: { error }
    });

    return resultResponse;
  }

  if (helperValues) {
    const { successCallback, fetchEntities } = helperValues;

    successCallback && (yield call(successCallback));

    !withoutFetching && fetchEntities && (yield call(fetchEntities));
  }

  if (actions && actions.success) {
    yield put({
      type: actions.success,
      payload: { ...payload, entity: response.payload }
    });
  }

  return resultResponse;
}

export function* postEntitiesByFormSaga(payload, actions, apiUrl) {
  return yield call(
    callMethodByFormSaga,
    postEntities,
    payload,
    actions,
    apiUrl
  );
}

export function* putEntitiesByFormSaga(payload, actions, apiUrl) {
  return yield call(
    callMethodByFormSaga,
    putEntities,
    payload,
    actions,
    apiUrl
  );
}

export function* deleteCommonSaga(payload, actions, apiUrl) {
  const { idEntity } = payload;
  const paramsRequest = deleteCommon(apiUrl);
  const { response, error } = yield call(fetchApiSaga, paramsRequest);

  if (response && actions && actions.success) {
    yield put({
      type: actions.success,
      payload: { idEntity }
    });
  } else if (error) {
    yield put({
      type: actions.error,
      payload: { error }
    });
  }

  return { response, error };
}
