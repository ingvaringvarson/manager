import { createSelector } from "reselect";
import { EmptyStateRecord, ListStateRecord, InfoStateRecord } from "./reducers";
import { employeeInfoByUserSelector } from "../ducks/user";
import { queryListWithoutServiceQueryRouterSelector } from "../redux/selectors/router";
import { maskDateBeginningDay, maskDateEndDay } from "./masks";

export function stateSelector(moduleName) {
  return function(state) {
    return state[moduleName];
  };
}

export function listStateSelector(moduleName) {
  return function(state, props) {
    return stateSelector(moduleName)(state, props).list;
  };
}

export function listForEntityStateSelector(moduleName) {
  return function(state, props) {
    return stateSelector(moduleName)(state, props).listForEntity;
  };
}

export function entityListSelector(moduleName) {
  return function(state, props) {
    return listStateSelector(moduleName)(state, props).entities;
  };
}

export function maxPagesSelector(moduleName) {
  return function(state, props) {
    return +listStateSelector(moduleName)(state, props).maxPages;
  };
}

const EmptyListForEntityStateRecord = { ...EmptyStateRecord };
export function typeEntityStateSelector(moduleName) {
  return function(state, props) {
    const listForEntityState = listForEntityStateSelector(moduleName)(
      state,
      props
    )[props.typeEntity];

    return listForEntityState || EmptyListForEntityStateRecord;
  };
}

const EmptyTypeEntityStateRecord = { ...ListStateRecord };
export function entitiesForTypeEntitySelector(moduleName) {
  return function(state, props) {
    const typeEntityState = typeEntityStateSelector(moduleName)(state, props)[
      props.idEntity
    ];

    return typeEntityState || EmptyTypeEntityStateRecord;
  };
}

const EmptyInfoStateRecord = { ...InfoStateRecord };
export function entitySelector(moduleName) {
  return function(state, props) {
    const infoState = infoStateSelector(moduleName)(state, props)[
      props.idEntity
    ];
    return infoState || EmptyInfoStateRecord;
  };
}

export function infoStateSelector(moduleName) {
  return function(state, props) {
    const info = stateSelector(moduleName)(state, props).info;

    return info || InfoStateRecord;
  };
}
export function entityInfoEntitiesSelector(moduleName) {
  return function(state, props) {
    const entities = infoStateSelector(moduleName)(state, props).entities;

    return entities || {};
  };
}
export function entityInfoSelector(moduleName) {
  return function(state, props) {
    return props.id
      ? entityInfoEntitiesSelector(moduleName)(state, props)[props.id]
      : null;
  };
}

export function entityInfoForRouteSelector(moduleName) {
  return function(state, props) {
    const id = entityInfoForRouteSelector(state, props);
    return id ? entityInfoEntitiesSelector(moduleName)(state, props)[id] : null;
  };
}

export const typeEntitySelector = (state, props = { typeEntity: "" }) =>
  props.typeEntity;
export const idEntitySelector = (state, props = { idEntity: "" }) =>
  props.idEntity;

export const pageModByDefaultValuesSelector = (state, props = {}) => {
  const { pageMod = true } = props;

  return pageMod;
};

export const defaultValuesForFieldsSelector = createSelector(
  employeeInfoByUserSelector,
  queryListWithoutServiceQueryRouterSelector,
  pageModByDefaultValuesSelector,
  (employee, queryList, pageMod) => {
    const defaultParams = {};

    if (queryList.length) return defaultParams;

    const currentDay = Date.now();

    defaultParams.datefrom_created = maskDateBeginningDay(
      currentDay - 1209600000
    );
    defaultParams.dateto_created = maskDateEndDay(currentDay);

    if (employee) {
      if (employee.warehouse_id && pageMod) {
        defaultParams.warehouse_id = employee.warehouse_id;
      }
      if (employee.firm_id) {
        defaultParams.firm_id = employee.firm_id;
      }
    }

    return defaultParams;
  }
);
