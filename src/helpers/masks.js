import { getDate, getTime, isValidDate, isNumeric } from "../utils";
import moment from "moment";

export function maskOnlyNumbers(str) {
  const arrNumbers = String(str).match(/[0-9]/g);

  if (!arrNumbers) return "";

  return arrNumbers.join("");
}

export function maskUppercaseText(str) {
  const arrNumbers = String(str).match(/[0-9A-ZА-ЯЁ]/g);

  if (!arrNumbers) return "";

  return arrNumbers.join("");
}

export function maskOnlyText(str) {
  const arrNumbers = String(str).match(/[0-9a-zа-яё]/gi);

  if (!arrNumbers) return "";

  return arrNumbers.join("");
}

export function maskCustom(str) {
  const arrNumbers = String(str).match(/[-\s0-9]/g);

  if (!arrNumbers) return "";

  return arrNumbers.join("");
}

export function maskPhoneNumberWithCode(str) {
  const arrNumbers = String(str)
    .replace(/(\+7)/, "")
    .match(/[0-9]/g);

  if (!str || !arrNumbers) return "";

  return `+7 ${arrNumbers.slice(0, 10).join("")}`;
}

export function maskNumberSNILS(str) {
  const arrNumbers = String(str).match(/[0-9]/g);

  if (!arrNumbers) return "";

  return `${arrNumbers.slice(0, 3).join("")}-${arrNumbers
    .slice(3, 7)
    .join("")}-${arrNumbers.slice(7, 10).join("")} ${arrNumbers
    .slice(10, 12)
    .join("")}`;
}

export function maskDatePicker(str) {
  const arrNumbers = String(str).match(/[0-9]/g);

  switch (true) {
    case !arrNumbers:
      return "";
    case arrNumbers.length <= 2:
      return arrNumbers.slice(0, 2).join("");
    case arrNumbers.length <= 4:
      return `${arrNumbers.slice(0, 2).join("")}.${arrNumbers
        .slice(2, 4)
        .join("")}`;
    case arrNumbers.length > 4:
      return `${arrNumbers.slice(0, 2).join("")}.${arrNumbers
        .slice(2, 4)
        .join("")}.${arrNumbers.slice(4, 8).join("")}`;
    default:
      return "";
  }
}

export function maskDatePickerRevertMonthDay(str) {
  const arrNumbers = String(str).match(/[0-9]/g);

  switch (true) {
    case !arrNumbers:
      return "";
    case arrNumbers.length <= 4:
      return `${arrNumbers.slice(2, 4).join("")}.${arrNumbers
        .slice(0, 2)
        .join("")}`;
    case arrNumbers.length > 4:
      return `${arrNumbers.slice(2, 4).join("")}.${arrNumbers
        .slice(0, 2)
        .join("")}.${arrNumbers.slice(4, 8).join("")}`;
    default:
      return "";
  }
}

export function masksByNumberOnPersonRequisites(fieldName = "type") {
  return function(value, values = {}) {
    switch (+values[fieldName]) {
      case 3:
        return maskNumberSNILS(value, values);
      default:
        return maskCustom(value, values);
    }
  };
}

export function revertMasksByNumberOnPersonRequisites(fieldName = "type") {
  return function(value, values = {}) {
    switch (+values[fieldName]) {
      case 3:
        return maskOnlyNumbers(value, values);
      default:
        return value;
    }
  };
}

export function masksByContacts(fieldName = "type") {
  return function(value, values = {}) {
    switch (true) {
      case +values[fieldName] === 7 &&
        isNumeric(+value) &&
        (value.length === 10 || (value.length === 11 && value.startsWith(7))):
        return value.startsWith("+")
          ? maskPhoneNumberWithCode(value)
          : maskPhoneNumberWithCode("+" + value);
      default:
        return value;
    }
  };
}

export function revertMasksByContacts(fieldName = "type") {
  return function(value, values = {}) {
    switch (+values[fieldName]) {
      case 7:
        return maskOnlyNumbers(value, values);
      default:
        return value;
    }
  };
}

export function maxDateAmongEntities(key) {
  return function(value) {
    if (!Array.isArray(value) || !value.length || !key) return null;

    const maxDate = value.reduce((currentMaxDate, entity) => {
      const currentDate = new Date(entity[key]);

      if (!currentMaxDate || +currentMaxDate < +currentDate) {
        return currentDate;
      }

      return currentMaxDate;
    }, "");

    return maxDate ? `${getDate(maxDate)} ${getTime(maxDate)}` : null;
  };
}

export function maskDateTime(value) {
  if (!isValidDate(value)) return null;

  const date = new Date(value);

  return `${getDate(date)} ${getTime(date)}`;
}
export function maskDate(value) {
  if (!isValidDate(value)) return null;

  const date = new Date(value);

  return `${getDate(date)}`;
}

export function maskTime(value) {
  if (!isValidDate(value)) return null;

  const date = new Date(value);

  return `${getTime(date)}`;
}

export function maskDateEndDay(value) {
  return value ? `${moment(value).format("YYYY-MM-DD")}T23:59:59` : value;
}

export function maskDateBeginningDay(value) {
  return value ? `${moment(value).format("YYYY-MM-DD")}T00:00:00` : value;
}
