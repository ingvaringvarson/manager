import PropTypes from "prop-types";

export default {
  name: PropTypes.string,
  onLogOut: PropTypes.func.isRequired
};
