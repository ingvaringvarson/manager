import PropTypes from "prop-types";

export default {
  client: PropTypes.shape({
    person: PropTypes.object,
    company: PropTypes.object,
    contacts: PropTypes.arrayOf(PropTypes.object),
    addresses: PropTypes.arrayOf(PropTypes.object),
    contact_persons: PropTypes.arrayOf(PropTypes.object),
    bank_details: PropTypes.arrayOf(PropTypes.object),
    person_requisites: PropTypes.arrayOf(PropTypes.object),
    company_requisites: PropTypes.object
  }).isRequired,
  enumerations: PropTypes.shape({
    personGender: PropTypes.object,
    contactType: PropTypes.object,
    addressType: PropTypes.object,
    documentType: PropTypes.object,
    companyType: PropTypes.object,
    companySno: PropTypes.object
  }).isRequired
};
