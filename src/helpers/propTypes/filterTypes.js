import PropTypes from "prop-types";

export const filterPropTypes = {
  items: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.number])
  ).isRequired,
  nameProperty: PropTypes.string.isRequired,
  urlProperty: PropTypes.string.isRequired,
  selectedItems: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  ).isRequired,
  availableItems: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  ).isRequired,
  type: PropTypes.shape({
    top: PropTypes.number.isRequired,
    view: PropTypes.string.isRequired,
    ui: PropTypes.string.isRequired
  }).isRequired,
  titleProperty: PropTypes.string.isRequired
};

export const filterLayoutPropTypes = {
  filter: PropTypes.shape({
    ...filterPropTypes
  }).isRequired,
  onChangeFilter: PropTypes.func.isRequired,
  onToggleFilterBlock: PropTypes.func.isRequired
};
