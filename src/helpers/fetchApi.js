const fetchApi = (params = {}) => async () => {
  const result = { response: null, error: null };
  let body = {};

  if (!params || !params.action) {
    result.error = { message: "Empty params by fetch" };

    return result;
  }

  const headers = new Headers(params.options.headers || {});

  headers.append("Accept", params.type || "application/json;charset=utf-8");
  if (params.type !== "multipart/form-data") {
    headers.append("Content-Type", params.type || "application/json");
  }

  const response = await fetch(
    params.action,
    Object.assign({}, params.options, { headers })
  );

  const contentType = response.headers.get("content-type");
  result.status = response.status;
  result.headers = response.headers;

  if (!response.ok) {
    if (result.status === 401) {
      result.error = { code: 401 };
    } else if (contentType && contentType.includes("application/json")) {
      body = await response.json();
      if (body.message) result.error = { ...body };
      if (body.error) {
        result.error = { ...body.error };
      } else if (body.message) {
        result.error = { ...body };
      }
    }

    return result;
  }

  if (contentType && contentType.includes("application/json")) {
    body = await response.json();
    result.response = { ...body };
  }

  return result;
};

export default fetchApi;
