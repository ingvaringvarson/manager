import { getUrl } from "./urls";

export const routesToMove = {
  order(orderEntity) {
    return getUrl("orders", { id: orderEntity.id });
  },
  return(returnEntity) {
    return getUrl("returns", { id: returnEntity.return.id });
  },
  purchaseReturn(purchaseReturnEntity) {
    return getUrl("purchaseReturns", {
      id: purchaseReturnEntity.purchase_return.id
    });
  },
  purchaseOrder(purchaseOrderEntity) {
    return getUrl("purchaseOrders", { id: purchaseOrderEntity.order.id });
  },
  client(clientEntity) {
    return getUrl("clients", { id: clientEntity.id });
  },
  payment(payment) {
    return getUrl("payments", { id: payment.id });
  },
  bill(bill) {
    return getUrl("bills", { id: bill.id });
  },
  billPayment(billPayment) {
    return getUrl("payments", { id: billPayment.payment_id });
  }
};
