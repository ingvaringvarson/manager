import memoize from "memoize-state";

// ToDo: к сожалению, этот кэш не решает проблему изменения
// поля _id в объекте. Предположительно будем решать через селекторы
//
// для увеличения производительности можно исследовать
// свойство метода cacheStatistics и объём кэша
export const memoizeToArray = memoize(el => [el], {
  cacheSize: 128,
  shallowCheck: false,
  safe: true
});
