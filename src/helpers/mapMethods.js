import { select, call } from "redux-saga/effects";
import { authIdSelector } from "../ducks/user";
import keysHasTypeArray from "../constants/keysHasTypeArray";
import keysHasArrayOfNumber from "../constants/keysHasArrayOfNumber";
import { fetchApiSaga } from "../ducks/logger";
import { getEntities } from "../api/requestByTypeEntities";
import {
  enumerationIdListSelector,
  fetchEnumerationsSaga
} from "../ducks/enumerations";
import { mapStringToProductId } from "../helpers/dataMapping";
import { isType } from "../utils";

export function* mapRequestParamsToParamsByApiSaga(params, typeEntity) {
  const objectIdUser = yield select(authIdSelector);
  let objectIdForAgentCode = null;
  let order_id = null;
  let bill_id = null;
  let agent_id = null;
  const newParams = {};

  if (params.hasOwnProperty("_agent_code")) {
    const paramsFetch = {
      params: JSON.stringify({
        code: params["_agent_code"],
        page: 1,
        page_size: 1
      })
    };
    const paramsRequest = getEntities("agents", paramsFetch);
    const { response } = yield call(fetchApiSaga, paramsRequest);

    const agent =
      response &&
      response.payload &&
      response.payload.agents &&
      response.payload.agents[0];

    if (agent) {
      agent_id = agent.id;
      objectIdForAgentCode =
        Array.isArray(agent.employees) && agent.employees.length
          ? agent.employees[0].id
          : null;
    }
  }

  if (params.hasOwnProperty("_order_code")) {
    const paramsFetch = {
      params: JSON.stringify({
        code: params["_order_code"],
        page: 1,
        page_size: 1
      })
    };
    const paramsRequest = getEntities("orders", paramsFetch);
    const { response } = yield call(fetchApiSaga, paramsRequest);

    const order =
      response &&
      response.payload &&
      response.payload.orders_with_bills &&
      response.payload.orders_with_bills[0] &&
      response.payload.orders_with_bills[0].order;

    if (order) {
      order_id = order.id;
    }
  }

  if (params.hasOwnProperty("_bill_code")) {
    const paramsFetch = {
      params: JSON.stringify({
        code: params["_bill_code"],
        page: 1,
        page_size: 1
      })
    };
    const paramsRequest = getEntities("bills", paramsFetch);
    const { response } = yield call(fetchApiSaga, paramsRequest);

    const bill =
      response &&
      response.payload &&
      response.payload.bills &&
      response.payload.bills[0];

    if (bill) {
      bill_id = bill.id;
    }
  }

  let listWithoutState = null;

  if (typeEntity === "bills" && params.hasOwnProperty("_without_state")) {
    yield call(fetchEnumerationsSaga, { payload: { names: ["billState"] } });
    const stateIdList = yield select(enumerationIdListSelector, {
      name: "billState"
    });
    listWithoutState = stateIdList.filter(
      id => !params["_without_state"].includes(id)
    );
  }

  if (typeEntity === "bills" && params.hasOwnProperty("_tab_state_bill")) {
    newParams.state = params._tab_state_bill === 1 ? [0, 1, 2, 3] : [4];
  }

  return Object.keys(params).reduce(
    (newParams, key) => {
      switch (true) {
        case key.includes("_contacts_") && !!params[key]:
          const contactType = key.split("_")[2];

          if (!contactType) return newParams;

          const newContact = { type: +contactType, value: params[key] };

          if (Array.isArray(newParams.contacts)) {
            return {
              ...newParams,
              contacts: newParams.contacts.concat(newContact)
            };
          }
          return {
            ...newParams,
            contacts: [newContact]
          };
        case key === "_for_user" && params[key] === "1":
          return {
            ...newParams,
            object_id_create: objectIdUser
          };
        case key === "_created_for_user" && params[key] === "1":
          return {
            ...newParams,
            executor_id: objectIdUser
          };
        case key === "_executed_by_user" && params[key] === "1":
          return {
            ...newParams,
            object_id_edit: objectIdUser
          };
        case key === "_without_state" && !!listWithoutState:
          return {
            ...newParams,
            state: listWithoutState
          };
        case key === "_order_code":
          return {
            ...newParams,
            order_id
          };
        case key === "_bill_code":
          return {
            ...newParams,
            bill_id
          };
        case key === "_type_group":
          const types = params[key].split("_").map(item => +item);

          return {
            ...newParams,
            type: types
          };
        case key === "_agent_code":
          if (typeEntity === "tasks") {
            return {
              ...newParams,
              agent_id
            };
          }

          return {
            ...newParams,
            object_id_create: objectIdForAgentCode
          };
        case key === "page" || key === "page_size":
          return {
            ...newParams,
            [key]: +params[key]
          };
        case (typeEntity === "productInStock" || key === "product") &&
          isType(params[key], "string"):
          const productId = mapStringToProductId(params[key]);
          return {
            ...newParams,
            [key]: [
              {
                article: productId.vendorCode,
                brand: productId.brandKey
              }
            ]
          };
        case keysHasTypeArray[typeEntity] &&
          keysHasTypeArray[typeEntity].has(key):
          let values = Array.isArray(params[key]) ? params[key] : [params[key]];

          if (
            keysHasArrayOfNumber[typeEntity] &&
            keysHasTypeArray[typeEntity].has(key)
          ) {
            values = values.map(item => +item);
          }
          return {
            ...newParams,
            [key]: values
          };
        case key === "article" && isType(params[key], "string"): {
          return {
            ...newParams,
            [key]: params[key].replace(/[^0-9a-zа-яё]/gim, "")
          };
        }
        default:
          return newParams;
      }
    },
    { ...params, ...newParams }
  );
}
