export function mapValuesToEntity(entity = {}, values = {}) {
  return {
    ...entity,
    ...values
  };
}

export const mapStringToProductId = s =>
  s
    ? {
        brandKey: s.split("/")[0],
        vendorCode: s.split("/")[1]
      }
    : {
        brandKey: "",
        vendorCode: ""
      };
