import { isNumeric, isValidDate, getDate } from "../../utils";

export const maxLength = max => value => {
  return value && value.length > max
    ? `Должно быть символов максимум ${max}`
    : null;
};

export const minLength = min => value => {
  return value && value.length < min
    ? `Должно быть символов минимум ${min}`
    : null;
};

export const required = value => {
  const selectedValue = Array.isArray(value) ? value[0] : value;

  return selectedValue === "" ||
    selectedValue === null ||
    selectedValue === undefined
    ? "Обязательное поле для заполннения"
    : null;
};

export const requiredKladrId = value => {
  return !value || value === "0" ? "Адрес указан неверно" : null;
};

export const requiredOneOf = (twins = []) => (value, values = {}) => {
  if (twins.some(twin => values[twin])) return null;

  return !value ? "Обязательное поле для заполннения" : null;
};

export const maxNotRequiredValue = maxVal => value => {
  return !!value && value > maxVal
    ? `Максимально возможное значение: ${maxVal}`
    : null;
};
export const minNotRequiredValue = minVal => value => {
  return !!value && value < minVal
    ? `Минимально возможное значение: ${minVal}`
    : null;
};

// dates

export const notLaterCurrentDate = value => {
  const isNotDateError = isDateValue(value);
  if (!!isNotDateError) {
    return isNotDateError;
  }
  const date = new Date(value);

  return +date > Date.now() ? "Родился в будущем" : null;
};

export const requiredNotEarlierCurrentDate = value => {
  const isNotDateError = isRequiredDateValue(value);
  if (!!isNotDateError) {
    return isNotDateError;
  }
  const date = new Date(value);

  return +date < Date.now()
    ? "Выбранная дата не должна быть ранее текущей"
    : null;
};

export const maxDateRange = key => (value, values = {}) => {
  const isNotDateError = isDateValue(value);
  if (!!isNotDateError) {
    return isNotDateError;
  }
  const date = new Date(value);

  const maxDate = new Date(values[key]);

  if (!isValidDate(maxDate)) return null;

  return +date > +maxDate
    ? `Дата должна быть меньше или равна ${getDate(maxDate)}`
    : null;
};

export const minDateRange = key => (value, values = {}) => {
  const isNotDateError = isDateValue(value);
  if (!!isNotDateError) {
    return isNotDateError;
  }
  const date = new Date(value);

  const minDate = new Date(values[key]);

  if (!isValidDate(minDate)) return null;

  return +date < +minDate
    ? `Дата должна быть больше чем ${getDate(minDate)}`
    : null;
};

// numeric
export const valueGreaterThan = min => value =>
  +value > min ? null : `Значение должно быть больше: ${min}`;
export const valueGreaterOrEqualThan = min => value =>
  +value >= min ? null : `Значение должно быть больше или равно: ${min}`;

export const valueLessThan = max => value =>
  +value < max ? null : `Значение должно быть меньше: ${max}`;
export const valueLessOrEqualThan = max => value =>
  +value <= max ? null : `Значение должно быть меньше или равно: ${max}`;

export function maxNumericDiscountValue(
  maxCount,
  maxPersent,
  discountIdEntity
) {
  return function(value, formValues) {
    const discValue = Array.isArray(formValues[discountIdEntity])
      ? formValues[discountIdEntity][0]
      : formValues[discountIdEntity];

    if (discValue === "rub") {
      return +value > maxCount
        ? `Максимально возможное значение: ${maxCount}`
        : null;
    } else if (discValue === "percent") {
      return +value > maxPersent
        ? `Максимально возможное значение в процентах: ${maxPersent}`
        : null;
    }

    return null;
  };
}

// common

export const isRequiredNumericValue = value => {
  const isNoDataError = required(value);
  if (!!isNoDataError) {
    return isNoDataError;
  }

  return isNumeric(value) ? "" : "Значение может быть только численным";
};

const isDateValue = value => (value === "" ? "" : isRequiredDateValue(value));

export const isRequiredDateValue = value => {
  const isNoDataError = required(value);
  if (!!isNoDataError) {
    return isNoDataError;
  }

  return isValidDate(value) ? "" : "Значение может быть только датой";
};

export const isTextSymbols = value => {
  if (!value) return null;
  const regexp = new RegExp(/[a-zA-Z0-9а-яА-ЯёЁ]+$/, "i");
  return regexp.test(value) ? null : "Можно использовать только символы";
};
