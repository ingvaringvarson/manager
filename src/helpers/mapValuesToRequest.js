import { generateId } from "../utils";
export const getActionPayload = params => ({ payload: { ...params } });

export function mapAgentCodeToActionParams(code) {
  return { payload: { id: generateId(), params: { code } } };
}

export function mapAgentIdToActionParams(id) {
  return { payload: { id: generateId(), params: { id: [id] } } };
}
