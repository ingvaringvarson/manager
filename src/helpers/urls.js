export function getUrl(type, params = {}) {
  const {
    id = null,
    brandKey = null,
    vendorCode = null,
    queryString = ""
  } = params;

  let pathString = "";

  switch (true) {
    case type === "products" && brandKey !== null && vendorCode !== null:
      pathString = `/search/products/${brandKey}/${vendorCode}`;
      break;
    case type === "products":
      pathString = `/search/products`;
      break;
    case !!type && id === null:
      pathString = `/${type === "clients" ? "" : type}`;
      break;
    case !!type && id !== null:
      pathString = `/${type}/${id}`;
      break;
    default:
      pathString = "/";
  }

  return `${pathString}${queryString}`;
}
