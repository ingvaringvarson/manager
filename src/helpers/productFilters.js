export const customView = {
  delivery: arrRange => {
    const filterItems = [
      {
        name: "Сегодня",
        id: "1",
        value: [0, 0]
      },
      {
        name: "Завтра",
        id: "2",
        value: [1, 1]
      },
      {
        name: "Послезавтра",
        id: "3",
        value: [2, 2]
      },
      {
        name: "3-5 дней",
        id: "4",
        value: [3, 5]
      },
      {
        name: "5-10 дней",
        id: "5",
        value: [5, 10]
      },
      {
        name: "> 10 дней",
        id: "6",
        value: [11, 365]
      }
    ];

    if (!arrRange) return filterItems;

    return filterItems.filter(item => {
      return !(arrRange[1] < item.value[0] || arrRange[0] > item.value[1]);
    });
  },
  volume: arrRange => {
    const filterItems = [
      {
        name: "0...3",
        id: "1",
        value: [0, 3]
      },
      {
        name: "3,1...10",
        id: "2",
        value: [3.1, 10]
      },
      {
        name: "11...1000",
        id: "3",
        value: [11, 1000]
      }
    ];

    if (!arrRange) return filterItems;

    return filterItems.filter(item => {
      return !(arrRange[1] < item.value[0] || arrRange[0] > item.value[1]);
    });
  }
};
