const areInByPropertyName = statePropertyName => states => entity =>
  states.includes(entity[statePropertyName]);

const isValidByCustomFilter = command => entity => command.customFilter(entity);

const byStates = (entities, statePropertyName) => command => {
  const areIn = areInByPropertyName(statePropertyName);

  // либо blockedStates, либо availableStates
  // ToDo: обработать ситуацию, если статуса нет?
  if (
    (command.blockedStates && entities.some(areIn(command.blockedStates))) ||
    (command.availableStates && !entities.every(areIn(command.availableStates)))
  ) {
    return false;
  }

  return true;
};

const byCustomFilter = entities => command =>
  !command.customFilter || entities.every(isValidByCustomFilter(command));

export const byAllEnitiesStates = (entities, statePropertyName) => command =>
  byStates(entities, statePropertyName)(command) &&
  byCustomFilter(entities)(command);

export const byAllPositionsStates = positions =>
  byAllEnitiesStates(positions, "good_state");
