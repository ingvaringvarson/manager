import { indexingEntity } from "../utils";

export const InfoStateRecord = {
  entities: {}
};

export const ListStateRecord = {
  entities: [],
  maxPages: 0
};

export const EmptyStateRecord = {};

export const EmptyListRecord = [];

export function fetchEntitySuccess(state, payload = { id: "", entity: {} }) {
  return {
    ...state,
    entities: {
      ...state.entities,
      [payload.id]: !!payload.entity
        ? { ...indexingEntity(payload.entity, 0) }
        : null
    }
  };
}

export function fetchEntitiesSuccess(state, payload) {
  return {
    ...state,
    entities: payload.entities.map(indexingEntity),
    maxPages: payload.maxPages
  };
}

export function fetchEntitiesError(state) {
  return {
    ...state,
    entities: [],
    maxPages: 0
  };
}

export function fetchEntitiesForTypeEntitySuccess(state, payload) {
  return {
    ...state,
    [payload.typeEntity]: {
      ...state[payload.typeEntity],
      [payload.idEntity]: {
        ...ListStateRecord,
        entities: payload.entities.map(indexingEntity),
        maxPages: payload.maxPages
      }
    }
  };
}

export function fetchEntitiesForTypeEntityError(state, payload) {
  return {
    ...state,
    [payload.typeEntity]: {
      ...state[payload.typeEntity],
      [payload.idEntity]: {
        ...ListStateRecord,
        entities: [],
        maxPages: 0
      }
    }
  };
}
