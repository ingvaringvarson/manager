export function getStateByRequestInfo(props, state, initState = null) {
  let newState = initState;

  if (state.isLoading !== props.isLoading) {
    newState = { ...newState, isLoading: props.isLoading };
    if (state.isLoading && !props.isLoading) {
      newState.isLoaded = true;
    }
  }

  return newState;
}

export function getStateByDefaultValuesForFields(
  props,
  state,
  initState = null
) {
  let newState = initState;

  if (
    state.valuesForFields === null &&
    props.defaultValuesForFields &&
    props.valuesForFields !== state.valuesForFields
  ) {
    newState = {
      ...newState,
      prevValuesForFields: props.valuesForFields,
      valuesForFields: {
        ...props.defaultValuesForFields,
        ...props.valuesForFields
      }
    };
  } else if (
    state.valuesForFields !== null &&
    props.valuesForFields !== state.prevValuesForFields
  ) {
    newState = {
      ...newState,
      prevValuesForFields: props.valuesForFields,
      valuesForFields: props.valuesForFields
    };
  }

  return newState;
}
