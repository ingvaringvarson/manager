import { generateId, generateUrlWithQuery, getValueInDepth } from "../../utils";
import {
  mapFormFieldsToQuery,
  reduceValuesToFormFields
} from "../../fields/helpers";
import { getUrl } from "../urls";

export function handleChangeTab(id) {
  const {
    pathname,
    push,
    queryKeyByTab,
    queryKeysHold,
    requestParams
  } = this.props;

  if (!pathname || !push || !queryKeyByTab) return;

  let queryString = id === 1 ? "" : `?${queryKeyByTab}=${id}`;

  if (queryKeysHold) {
    queryString = queryKeysHold.reduce((str, key) => {
      let newStr = str;
      if (requestParams.hasOwnProperty(key)) {
        const strByKey = `${[key]}=${requestParams[key]}`;

        if (newStr) {
          newStr = `${newStr}&${strByKey}`;
        } else {
          newStr = `?${strByKey}`;
        }
      }

      return newStr;
    }, queryString);
  }

  push(`${pathname}${queryString}`);
}

export function handleTableSort(query, fields, values) {
  const { pathname, requestParams, push } = this.props;

  if (!pathname || !push) return;

  const params = mapFormFieldsToQuery(fields, values, {
    ...requestParams,
    page: 1
  });

  const newQuery = { ...params, ...query };

  push(generateUrlWithQuery({ pathname, newQuery }));
}

export function handleSubmitForm(values, fields) {
  const { pathname, requestParams, push } = this.props;

  if (!pathname || !push) return;

  const newQuery = mapFormFieldsToQuery(fields, values, {
    ...requestParams,
    page: 1
  });

  push(generateUrlWithQuery({ pathname, newQuery }));
}

export function handleMoveToEntityPage({ entity }) {
  return () => {
    const { push, typeEntity } = this.props;

    if (typeEntity && push && entity)
      push(getUrl(typeEntity, { id: entity.id }));
  };
}

export function mapValuesToFields() {
  const {
    fieldsForTable = [],
    fieldsForFilters = [],
    valuesForFields,
    requestParams = {},
    enumerations
  } = this.props;
  const stateValuesForFields = getValueInDepth(this.state, ["valuesForFields"]);

  const values = stateValuesForFields || valuesForFields || requestParams;

  const fieldsForTableFilled = fieldsForTable.reduce(
    reduceValuesToFormFields(values, enumerations),
    []
  );
  const fieldsForFiltersFilled = fieldsForFilters.reduce(
    reduceValuesToFormFields(values, enumerations),
    []
  );
  const allFields = fieldsForTableFilled.concat(fieldsForFiltersFilled);

  return {
    fieldsForTable: fieldsForTableFilled,
    fieldsForFilters: fieldsForFiltersFilled,
    allFields,
    key: generateId()
  };
}

export function getSelectedEntities(entities = [], selectedIds = []) {
  if (!Array.isArray(entities) || !Array.isArray(selectedIds))
    throw "both arguments must be array!";

  return entities.filter(entity => selectedIds.some(id => id === entity._id));
}
