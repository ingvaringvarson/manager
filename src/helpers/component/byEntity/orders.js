import React from "react";

import TableCell from "../../../designSystem/molecules/TableCell";
import {
  RenderContext,
  handleStopLifting
} from "../../../components/common/tableControls";
import OrderMenu from "../../../components/blocks/orders/OrderMenu";

import { memoizeToArray } from "../../cache";

export function renderOrderControl(props) {
  return ({ entity }) => (
    <TableCell control onClick={handleStopLifting}>
      <OrderMenu
        orders={memoizeToArray(entity)}
        fetchEntities={props.fetchEntities}
      />
    </TableCell>
  );
}

function renderContextByHeadOrderControl(context, props) {
  if (!props.orders) {
    return <TableCell control />;
  }

  const orders = props.orders.filter(order => {
    return context.selected.find(id => order._id === id);
  });

  return (
    <TableCell control onClick={handleStopLifting}>
      <OrderMenu forList orders={orders} fetchEntities={props.fetchEntities} />
    </TableCell>
  );
}

export function renderHeadOrderControl(props) {
  return (
    <TableCell control onClick={handleStopLifting}>
      <RenderContext {...props}>
        {renderContextByHeadOrderControl}
      </RenderContext>
    </TableCell>
  );
}
