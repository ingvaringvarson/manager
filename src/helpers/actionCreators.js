export function fetchEntity(action, withoutLoading = false) {
  return function(id = "", params = {}) {
    const { isForRepeat, ...restParams } = params;
    return {
      type: action,
      isForRepeat,
      payload: { id, params: restParams },
      withoutLoading
    };
  };
}

export function fetchEntities(action, withoutLoading = false) {
  return function(params = {}) {
    const { isForRepeat, ...restParams } = params;
    return {
      type: action,
      isForRepeat,
      payload: { params: restParams },
      withoutLoading
    };
  };
}

export function fetchEntitiesForTypeEntity(action, withoutLoading = false) {
  return function(typeEntity = "", idEntity = "", params = {}) {
    const { isForRepeat, ...restParams } = params;
    return {
      type: action,
      isForRepeat,
      payload: { typeEntity, idEntity, params: restParams },
      withoutLoading
    };
  };
}

export function putEntityPrepared(action, withoutLoading = false) {
  return function(
    typeEntity = "",
    idEntity = "",
    entity = {},
    successCallback
  ) {
    return {
      type: action,
      payload: {
        typeEntity,
        idEntity,
        entity,
        successCallback
      },
      withoutLoading
    };
  };
}

export function postEntityPrepared(action, withoutLoading = false) {
  return function(typeEntity = "", entity = {}) {
    return {
      type: action,
      payload: {
        typeEntity,
        entity
      },
      withoutLoading
    };
  };
}

export function postEntity(action, withoutLoading = false) {
  return function(
    formValues,
    helperValues,
    normalizeCallback,
    withoutFetching = false
  ) {
    return {
      type: action,
      payload: {
        formValues,
        helperValues,
        normalizeCallback,
        withoutFetching
      },
      withoutLoading
    };
  };
}

export function updateEntity(action, withoutLoading = false) {
  return function(
    id = "",
    entity = {},
    values = {},
    mapSaga = null,
    resultParams = { successParams: {}, errorParams: {} }
  ) {
    return {
      type: action,
      payload: {
        id,
        entity,
        values,
        mapSaga,
        resultParams
      },
      withoutLoading
    };
  };
}

export function createEntity(action, withoutLoading = false) {
  return function(
    values = {},
    mapSaga,
    resultParams = { successParams: {}, errorParams: {} }
  ) {
    return {
      type: action,
      payload: {
        values,
        mapSaga,
        resultParams
      },
      withoutLoading
    };
  };
}

export function putEntity(action, withoutLoading = false) {
  return function(
    formValues,
    helperValues,
    normalizeCallback,
    withoutFetching = false
  ) {
    return {
      type: action,
      payload: {
        formValues,
        helperValues,
        normalizeCallback,
        withoutFetching
      },
      withoutLoading
    };
  };
}
