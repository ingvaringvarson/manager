import { all } from "redux-saga/effects";
import { rootSaga as clientsSagas } from "../ducks/clients";
import { rootSaga as userSagas } from "../ducks/user";
import { rootSaga as enumerationsSagas } from "../ducks/enumerations";
import { rootSaga as ordersSagas } from "../ducks/orders";
import { rootSaga as billsSagas } from "../ducks/bills";
import { rootSaga as paymentsSaga } from "../ducks/payments";
import { rootSaga as basketsSaga } from "../ducks/baskets";
import { rootSaga as cashSaga } from "../ducks/cash";
import { rootSaga as kladrSaga } from "../ducks/kladr";
import { rootSaga as balanceSaga } from "../ducks/balances";
import { rootSaga as searchSaga } from "../ducks/search";
import { rootSaga as productsSaga } from "../ducks/products";
import { rootSaga as tasksSaga } from "../ducks/tasks";
import { rootSaga as warehousesSaga } from "../ducks/warehouses";
import { rootSaga as productsInStockSaga } from "../ducks/productsInStock";
import { rootSaga as pricelistsSaga } from "../ducks/pricelists";
import { rootSaga as printSaga } from "../ducks/print";
import { rootSaga as loggerSaga } from "../ducks/logger";
import { rootSaga as purchaseReturnsSaga } from "../ducks/purchaseReturns";
import { rootSaga as returnsSaga } from "../ducks/returns";
import { rootSaga as purchaseOrdersSaga } from "../ducks/purchaseOrders";
import { rootSaga as discountGroupSaga } from "../ducks/discountGroup";
import { rootSaga as positionsSaga } from "../ducks/positions";
import { rootSaga as cdekSaga } from "../ducks/cdek";
import { rootSaga as templatesSaga } from "../ducks/templates";
import { rootSaga as filesSaga } from "../ducks/files";
import { rootSaga as fileslinkSaga } from "../ducks/fileslink";
import { rootSaga as lastActionSaga } from "../ducks/lastAction";
import { rootSaga as maxoptraSaga } from "../ducks/maxoptra";

export default function*() {
  yield all([
    clientsSagas(),
    userSagas(),
    enumerationsSagas(),
    ordersSagas(),
    billsSagas(),
    balanceSaga(),
    paymentsSaga(),
    basketsSaga(),
    cashSaga(),
    kladrSaga(),
    tasksSaga(),
    warehousesSaga(),
    searchSaga(),
    productsSaga(),
    productsInStockSaga(),
    pricelistsSaga(),
    printSaga(),
    purchaseReturnsSaga(),
    returnsSaga(),
    loggerSaga(),
    discountGroupSaga(),
    purchaseOrdersSaga(),
    positionsSaga(),
    cdekSaga(),
    templatesSaga(),
    filesSaga(),
    fileslinkSaga(),
    lastActionSaga(),
    maxoptraSaga()
  ]);
}
