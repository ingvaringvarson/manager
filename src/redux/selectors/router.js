import { createSelector } from "reselect";
import { generateId, isType } from "../../utils";

const stateSelector = state => state.router;

export const actionRouterSelector = state => stateSelector(state).action;
export const locationRouterSelector = state => {
  return stateSelector(state).location;
};
export const pathnameRouterSelector = state => {
  return locationRouterSelector(state).pathname;
};
export const searchRouterSelector = state => {
  return locationRouterSelector(state).search;
};

export const urlRouterSelector = state => {
  const pathname = pathnameRouterSelector(state);
  const search = searchRouterSelector(state);

  return `${pathname}${search}`;
};
const initStateRouter = {};
export const stateRouterSelector = state => {
  return locationRouterSelector(state).state || initStateRouter;
};
export const queryRouterSelector = createSelector(
  searchRouterSelector,
  search => {
    if (
      search.length < 3 ||
      !search.includes("=") ||
      search.charAt(0) !== "?"
    ) {
      return {};
    }

    return search
      .slice(1)
      .split("&")
      .reduce((result, entity) => {
        const field = entity.split("=");
        const value = decodeURIComponent(field[1]);
        const newResult = { ...result };

        switch (true) {
          case field[0].includes("."): {
            const keys = field[0].split(".");

            if (isType(result[keys[0]], "object")) {
              result[keys[0]][keys[1]] = value;
            } else {
              result[keys[0]] = { [keys[1]]: value };
            }

            break;
          }
          case result.hasOwnProperty(field[0]): {
            newResult[field[0]] = [].concat(value, result[field[0]]);
            break;
          }
          default: {
            newResult[field[0]] = value;
          }
        }

        return newResult;
      }, {});
  }
);

export const queryListRouterSelector = createSelector(
  queryRouterSelector,
  query => Object.keys(query).map(key => ({ key, value: query[key] }))
);

export const queryListWithoutServiceQueryRouterSelector = createSelector(
  queryListRouterSelector,
  queryList => queryList.filter(item => !item.key.startsWith("_"))
);

export const keyRouterSelector = createSelector(
  locationRouterSelector,
  () => generateId()
);

export const keyRouterByPathnameSelector = createSelector(
  pathnameRouterSelector,
  () => generateId()
);

export const requestParamsSelector = createSelector(
  queryRouterSelector,
  query => ({
    page_size: 20,
    page: 1,
    sort_by: "object_date_create desc",
    ...query
  })
);

export const requestParamsInStockSelector = createSelector(
  queryRouterSelector,
  query => ({
    page_size_product: 20,
    page_product: 1,
    page_size_product_unit: 20,
    page_product_unit: 1,
    ...query
  })
);

export const entityIdForRouteSelector = (state, props = {}) => {
  return props.match && props.match.params ? props.match.params.id : null;
};

export const paramsForRouteSelector = (state, props = {}) => {
  return props.match && props.match.params ? props.match.params : {};
};
