import history from "../history";
import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import userReducer, { moduleName as userModule } from "../ducks/user";
import clientsReducer, { moduleName as clientsModule } from "../ducks/clients";
import ordersReducer, { moduleName as ordersModule } from "../ducks/orders";
import enumerationsReducer, {
  moduleName as enumerationsModule
} from "../ducks/enumerations";
import errorLoggerReducer, {
  moduleName as errorLoggerModule
} from "../ducks/logger";
import billsReducer, { moduleName as billsModule } from "../ducks/bills";
import paymentsReducer, {
  moduleName as paymentsModule
} from "../ducks/payments";
import basketsReducer, { moduleName as basketsModule } from "../ducks/baskets";
import cashReducer, { moduleName as cashModule } from "../ducks/cash";
import balanceReducer, { moduleName as balanceModule } from "../ducks/balances";
import kladrReducer, { moduleName as kladrModule } from "../ducks/kladr";
import searchReducer, { moduleName as searchModule } from "../ducks/search";
import productsReducer, {
  moduleName as productsModule
} from "../ducks/products";
import tasksReducer, { moduleName as tasksModule } from "../ducks/tasks";
import warehouseReducer, {
  moduleName as warehousesModule
} from "../ducks/warehouses";
import productsInStockReducer, {
  moduleName as productsInStockModule
} from "../ducks/productsInStock";
import pricelistsReducer, {
  moduleName as pricelistsModule
} from "../ducks/pricelists";
import purchaseReturnsReducer, {
  moduleName as purchaseReturnsModule
} from "../ducks/purchaseReturns";
import discountGroupReducer, {
  moduleName as discountGroupModule
} from "../ducks/discountGroup";
import returnsReducer, { moduleName as returnsModule } from "../ducks/returns";
import purchaseOrdersReducer, {
  moduleName as purchaseOrdersModule
} from "../ducks/purchaseOrders";
import cdekReducer, { moduleName as cdekModule } from "../ducks/cdek";
import templatesReducer, {
  moduleName as templatesModule
} from "../ducks/templates";
import filesReducer, { moduleName as filesModule } from "../ducks/files";
import fileslinkReducer, {
  moduleName as fileslinkModule
} from "../ducks/fileslink";
import lastActionReducer, {
  moduleName as lastActionModule
} from "../ducks/lastAction";
import modalsReducer, { moduleName as modalsModule } from "../ducks/modals";

export default combineReducers({
  router: connectRouter(history),
  [clientsModule]: clientsReducer,
  [userModule]: userReducer,
  [enumerationsModule]: enumerationsReducer,
  [errorLoggerModule]: errorLoggerReducer,
  [ordersModule]: ordersReducer,
  [billsModule]: billsReducer,
  [balanceModule]: balanceReducer,
  [paymentsModule]: paymentsReducer,
  [basketsModule]: basketsReducer,
  [cashModule]: cashReducer,
  [kladrModule]: kladrReducer,
  [warehousesModule]: warehouseReducer,
  [searchModule]: searchReducer,
  [productsModule]: productsReducer,
  [tasksModule]: tasksReducer,
  [productsInStockModule]: productsInStockReducer,
  [pricelistsModule]: pricelistsReducer,
  [purchaseReturnsModule]: purchaseReturnsReducer,
  [returnsModule]: returnsReducer,
  [purchaseOrdersModule]: purchaseOrdersReducer,
  [discountGroupModule]: discountGroupReducer,
  [cdekModule]: cdekReducer,
  [templatesModule]: templatesReducer,
  [filesModule]: filesReducer,
  [fileslinkModule]: fileslinkReducer,
  [lastActionModule]: lastActionReducer,
  [modalsModule]: modalsReducer
});
