import { createStore, applyMiddleware } from "redux";
import { routerMiddleware } from "connected-react-router";
import logger from "redux-logger";
import createSagaMiddleware from "redux-saga";
import reducer from "./reducer";
import saga from "./saga";
import history from "../history";

const sagaMiddleware = createSagaMiddleware();
const initialState = {};

const enhancer = applyMiddleware(
  routerMiddleware(history),
  sagaMiddleware,
  logger
);

const store = createStore(reducer, initialState, enhancer);

sagaMiddleware.run(saga);

export default store;
